import moment from 'moment';
import { get } from 'lodash';
import { config } from 'config';

export const renderDataFormatted = (data, col) => {
  if (typeof data === undefined || !col) return '---';
  else if (col.format === 'int') return parseInt(data[col.column]);
  else if (col.format === 'float') return parseFloat(data[col.column]);
  else if (col.format === 'date') {
    if (!data[col.column]) return '---';
    return moment(data[col.column]).utc(0).format(config.DATE_FORMAT_SHORT);
  } else if (col.format === 'datetime') {
    if (!data[col.column]) return '---';
    return moment(data[col.column]).utc(0).format(config.SHOW_DATE_FORMAT);
  } else if (col.format === 'image') return `${config.API_URL}/${data[col.column]}`;
  else if (col.format === 'custom') {
    const item = get(col, 'options', []).find((it) => it.input == data[col.column]);
    return item ? item.output : '---';
  } else return data[col.column] ? data[col.column] : '---';
};

export const formatDataExport = (data, fieldConfig) => {
  if (fieldConfig && fieldConfig.meta && fieldConfig.meta.options && data) {
    const value = fieldConfig.meta.options.find((item) => {
      const [key, val] = item.split(':');
      return data === key;
    });
    if (value) {
      return value.split(':')[1];
    }
    return data;
  }
  return data;
};

export const recursiveTableModule = (categories, originCategories, characters = ' --- ', countLoops = 0, currentArr = []) => {
  countLoops++;
  let char = '';
  for (let index = 0; index < countLoops; index++) {
    char += characters;
  }
  if (countLoops === 1) {
    const roots = categories.filter((cate) => cate.module_parent === 'root');
    roots.forEach((category) => {
      currentArr.push({ ...category, titleLevel: category.module_name });
      const subs = [];
      categories.forEach((item) => {
        if (item.module_parent === category.module_id) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.module_name}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveTableModule(subs, originCategories, ' --- ', countLoops, currentArr);
    });
  } else {
    categories.forEach((category) => {
      const subs = [];
      originCategories.forEach((item) => {
        if (item.module_parent === category.module_id) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.module_name}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveTableModule(subs, originCategories, ' --- ', countLoops, currentArr);
    });
  }
  return currentArr;
};
