import VI_LANG from 'constants/languages/vi';
import EN_LANG from 'constants/languages/en';

import * as configActions from 'redux/actions/configure';

export const getCurrentLanguage = () => {
  return localStorage.getItem('w5dLanguage') || 'vi';
};

export const setCurrentLanguage = (lang, dispatch) => {
  if (localStorage.getItem('w5dLanguage') !== lang) {
    dispatch(configActions.editGlobal('language', lang));
    return localStorage.setItem('w5dLanguage', lang);
  }
};

export const getLangText = (key) => {
  const currentLang = getCurrentLanguage();
  const LANG = currentLang === 'en' ? EN_LANG : VI_LANG;
  const arrayKeys = key.split('.');
  if (arrayKeys[0] && arrayKeys[1] && LANG[arrayKeys[0]] && LANG[arrayKeys[0]][arrayKeys[1]]) return LANG[arrayKeys[0]][arrayKeys[1]];
  return key;
};
