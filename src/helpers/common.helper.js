// import crypto from 'crypto';
import { config } from 'config';
import defaultImg from 'assets/images/default.jpg';
import wordImg from 'assets/images/word.png';
import videoImg from 'assets/images/video.jpg';
import excelImg from 'assets/images/excel.png';
import pdfImg from 'assets/images/pdf.png';
import fileImg from 'assets/images/file.png';

export const buildUrl = (url, parameters) => {
  let qs = '';
  for (let key in parameters) {
    let value = parameters[key];
    if (value !== '') {
      qs += encodeURIComponent(key) + '=' + encodeURIComponent(value) + '&';
    }
  }
  if (qs.length > 0) {
    qs = qs.substring(0, qs.length - 1);
    url = url + '?' + qs;
  }
  return url;
};

export const getUserInformation = () => {
  return JSON.parse(localStorage.getItem('userInfo'));
};

export const shouldHaveAccessPermission = (key, sub) => {
  const user = getUserInformation();
  if (!user || !user.phan_quyen_nhom) {
    return false;
  }

  const permissions = JSON.parse(user.phan_quyen_nhom);
  if (sub) {
    if (permissions[key] && permissions[key].length > 0 && permissions[key].includes(sub)) {
      return true;
    }
  } else {
    if (permissions[key] && permissions[key].length > 0) {
      return true;
    }
  }
  return false;
};

export const cryptoUnHash = (key) => {
  try {
    // let mykey = crypto.createDecipher('aes-128-cbc', 'demo-app');
    // let mystr = mykey.update(key, 'hex', 'utf8');
    // mystr += mykey.final('utf8');
    // return mystr;
    return key;
  } catch (error) {
    console.log(error);
  }
};

export const recursiveTableData = (categories, originCategories, characters = ' --- ', countLoops = 0, currentArr = []) => {
  countLoops++;
  let char = '';
  for (let index = 0; index < countLoops; index++) {
    char += characters;
  }
  if (countLoops === 1) {
    const roots = categories.filter((cate) => !cate.parent);
    roots.forEach((category) => {
      currentArr.push({ ...category, titleLevel: category.title });
      const subs = [];
      categories.forEach((item) => {
        if (item.parent === category._id) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.title}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveTableData(subs, originCategories, ' --- ', countLoops, currentArr);
    });
  } else {
    categories.forEach((category) => {
      const subs = [];
      originCategories.forEach((item) => {
        if (item.parent === category._id) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.title}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveTableData(subs, originCategories, ' --- ', countLoops, currentArr);
    });
  }
  return currentArr;
};

export const stringToSlug = (str) => {
  let from = 'àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ',
    to = 'aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy';
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(RegExp(from[i], 'gi'), to[i]);
  }

  str = str
    .toLowerCase()
    .trim()
    .replace(/[^a-z0-9\-]/g, '-')
    .replace(/-+/g, '-');

  return str;
};

export const recursiveFolders = (categories, originCategories, countLoops = 0, currentArr = []) => {
  countLoops++;
  if (countLoops === 1) {
    const roots = categories.filter((cate) => !cate.parent || cate.parent === '');
    roots.forEach((category) => {
      const subs = [];
      categories.forEach((item) => {
        if (item.parent === category.id) {
          // currentArr.push({ ...item, titleLevel: `${char} ${item.name}` });
          subs.push(item);
        }
      });
      currentArr.push({ ...category, titleLevel: category.name, children: subs });

      if (subs.length > 0) {
        recursiveFolders(subs, originCategories, countLoops, currentArr);
      }
    });
  } else {
    categories.forEach((category) => {
      const subs = [];
      originCategories.forEach((item) => {
        if (item.parent === category.id) {
          subs.push(item);
        }
      });
      // currentArr.push({ ...category, children: subs });

      if (subs.length > 0) recursiveFolders(subs, originCategories, countLoops, currentArr);
    });
  }
  return currentArr;
};

export const recursiveUserCates = (categories, originCategories, characters = ' --- ', countLoops = 0, currentArr = []) => {
  countLoops++;
  let char = '';
  for (let index = 0; index < countLoops; index++) {
    char += characters;
  }
  if (countLoops === 1) {
    const roots = categories.filter((cate) => !cate.nhomnhanviencha_id || cate.nhomnhanviencha_id === 'null');
    roots.forEach((category) => {
      currentArr.push({ ...category, titleLevel: category.ten_nhom });
      const subs = [];
      categories.forEach((item) => {
        if (item.nhomnhanviencha_id === category.nhom_nhan_vien_id) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.ten_nhom}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveUserCates(subs, originCategories, ' --- ', countLoops, currentArr);
    });
  } else {
    categories.forEach((category) => {
      const subs = [];
      originCategories.forEach((item) => {
        if (item.nhomnhanviencha_id === category.nhom_nhan_vien_id) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.ten_nhom}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveUserCates(subs, originCategories, ' --- ', countLoops, currentArr);
    });
  }
  const sortedArray = currentArr.reduce((accumulator, currentValue) => {
    let item = accumulator.find((x) => x.nhom_nhan_vien_id === currentValue.nhomnhanviencha_id);
    let index = accumulator.indexOf(item);
    index = index !== -1 ? index + 1 : accumulator.length;
    accumulator.splice(index, 0, currentValue);
    return accumulator;
  }, []);

  return sortedArray.map((item) => {
    return { ...item, ten_nhom: item.titleLevel };
  });
};

export const recursiveCates = (categories, originCategories, cateField, cateParentField, characters = ' --- ', countLoops = 0, currentArr = []) => {
  countLoops++;
  let char = '';
  for (let index = 0; index < countLoops; index++) {
    char += characters;
  }
  if (countLoops === 1) {
    const roots = categories.filter((cate) => !cate[cateParentField] || cate[cateParentField] === 'null');
    roots.forEach((category) => {
      currentArr.push({ ...category, titleLevel: category.ten_nhom });
      const subs = [];
      categories.forEach((item) => {
        if (item[cateParentField] === category[cateField]) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.ten_nhom}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveCates(subs, originCategories, cateField, cateParentField, ' --- ', countLoops, currentArr);
    });
  } else {
    categories.forEach((category) => {
      const subs = [];
      originCategories.forEach((item) => {
        if (item[cateParentField] === category[cateField]) {
          currentArr.push({ ...item, titleLevel: `${char} ${item.ten_nhom}` });
          subs.push(item);
        }
      });
      if (subs.length > 0) recursiveCates(subs, originCategories, cateField, cateParentField, ' --- ', countLoops, currentArr);
    });
  }
  const sortedArray = currentArr.reduce((accumulator, currentValue) => {
    let item = accumulator.find((x) => x[cateField] === currentValue[cateParentField]);
    let index = accumulator.indexOf(item);
    index = index !== -1 ? index + 1 : accumulator.length;
    accumulator.splice(index, 0, currentValue);
    return accumulator;
  }, []);

  return sortedArray;
};

/**
 *
 * @param {*} server: upload server(minio or default)
 * @param {*} pathImg: path of image in database
 * @param {*} minioPath: minio image url
 * @param {*} defaultImg: default image url
 * @returns
 */
export const getImagePath = (server = '', pathImg, minioPath, defaultImg = '') => {
  if (pathImg) {
    if (server === 'minio') {
      return minioPath;
    }
    if (pathImg.includes('base64')) {
      return pathImg;
    }
    return `${config.UPLOAD_API_URL}/${pathImg}`;
  }
  return defaultImg;
};

export const getMediaInfo = (image = {}) => {
  image = image ? image : {};
  let fileType = 'image';
  let imgSrc = image.tep_tin_url ? `${config.UPLOAD_API_URL}/${image.tep_tin_url}` : defaultImg;
  if (['.docx', '.doc'].includes(image.loai_tep_tin)) {
    imgSrc = wordImg;
    fileType = 'doc';
  } else if (image.loai_tep_tin && ['.jpg', '.png', '.gif', '.bmp', '.tiff', '.jpeg', '.eps', '.raw', '.cr2'].includes(image.loai_tep_tin.toLowerCase())) {
    fileType = 'image';
  } else if (image.loai_tep_tin && ['.xlsx', '.xlsm', '.xls', '.xltx'].includes(image.loai_tep_tin.toLowerCase())) {
    imgSrc = excelImg;
    fileType = 'excel';
  } else if (image.loai_tep_tin && ['.mp4', '.flv', '.ogg', '.avi', '.avi'].includes(image.loai_tep_tin.toLowerCase())) {
    imgSrc = videoImg;
    fileType = 'video';
  } else if (['.pdf'].includes(image.loai_tep_tin)) {
    imgSrc = pdfImg;
    fileType = 'pdf';
  } else {
    imgSrc = fileImg;
    fileType = 'unknown';
  }
  return {
    src: imgSrc,
    type: fileType,
  };
};
