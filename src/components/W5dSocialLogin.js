// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { useHistory } from 'react-router-dom';
import GoogleLogin from 'react-google-login';
import { Button, Row, Col, notification } from 'antd';
import { FacebookOutlined, GooglePlusOutlined } from '@ant-design/icons';
import { get } from 'lodash';

// import internal libs
import 'assets/style/auth.css';
import { getLangText } from 'helpers/language.helper';
import * as userAction from 'redux/actions/user';
import constants from 'constants/global.constants';

const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_USER;

function W5dSocailLogin(props) {
  const history = useHistory();

  const [state, setState] = useState({
    isDisableFacebook: false,
    isDisableGoogle: false,
    appFacebookKey: '',
    appGoogleKey: '',
  });
  const onLogin = (values, oauth) => {
    let data = {};
    if (oauth === 'facebook') {
      data = {
        oauth,
        info: {
          email: values.email,
          fullname: values.name,
          image: get(values, 'picture.data.url', ''),
          accessToken: values.accessToken,
        },
      };
    }
    if (oauth === 'google') {
      data = {
        oauth,
        info: {
          email: values.profileObj.email,
          fullname: values.profileObj.name,
          image: values.profileObj.imageUrl,
          accessToken: values.accessToken,
        },
      };
    }
    const callback = () => {
      notification.success({
        message: getLangText('MESSAGE.USER_LOGIN_SUCCESS'),
      });
      window.location.reload();
    };
    props.dispatch(userAction.loginUser(data, callback));
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        isDisableFacebook: data.enable_loginfb,
        isDisableGoogle: data.enable_logingg,
        appFacebookKey: data.app_facebook_key,
        appGoogleKey: data.app_google_key,
      }));
    }
    const token = localStorage.getItem('userToken');
    if (token) {
      history.push('/');
    }
  }, [props]);

  if (state.isDisableFacebook || state.isDisableGoogle) {
    return (
      <Row className="social-login">
        <Col xxl={12} xl={24} lg={24} md={24} sm={24} xs={24} className="box">
          {state.isDisableFacebook && (
            <FacebookLogin
              appId={state.appFacebookKey}
              fields="name,email,picture"
              render={(renderProps) => (
                <Button block onClick={renderProps.onClick} shape="round" type="dashed" icon={<FacebookOutlined />}>
                  {getLangText('FORM.LOGIN_FACEBOOK')}
                </Button>
              )}
              callback={(data) => onLogin(data, 'facebook')}
            />
          )}
        </Col>
        <Col xxl={12} xl={24} lg={24} md={24} sm={24} xs={24} className="box">
          {state.isDisableGoogle && (
            <GoogleLogin
              clientId={state.appGoogleKey}
              render={(renderProps) => (
                <Button block onClick={renderProps.onClick} shape="round" danger icon={<GooglePlusOutlined />}>
                  {getLangText('FORM.LOGIN_GOOGLE')}
                </Button>
              )}
              onSuccess={(data) => onLogin(data, 'google')}
              onFailure={(error) => () => console.log('Error', error)}
            />
          )}
        </Col>
      </Row>
    );
  }
  return null;
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(W5dSocailLogin);
