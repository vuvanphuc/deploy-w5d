import React, { useState, useEffect } from 'react';
import { Modal, Button, Row, Col, Form, Input, Select, notification } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { get } from 'lodash';

import { config } from 'config';
import { getLangText } from 'helpers/language.helper';
import * as mediaAction from 'redux/actions/media';
import { getMediaInfo } from 'helpers/common.helper';

const { TextArea } = Input;
const { Option } = Select;

const W5dPreviewImage = (props) => {
  const [form] = Form.useForm();
  const [imagePreview, setImagePreview] = useState({});

  const handleCancel = () => {
    props.setIsPreviewVisible(false);
  };

  const onSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        setImagePreview(res.data);
        notification.success({
          message: 'Cập nhật tệp tin thành công.',
        });
        form.setFieldsValue(res.data);
        props.dispatch(mediaAction.getMedias({ folder: props.folder }));
      }
    };
    props.dispatch(mediaAction.editMedia({ ...imagePreview, ...values }, callback));
  };

  const renderCates = () => {
    const cates = get(props, 'media.listCates.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.nhom_tep_tin_id}>
          {cate.ten_nhom}
        </Option>
      );
    });
    return <Select placeholder="Chọn thư mục">{catesOpts}</Select>;
  };

  useEffect(() => {
    form.setFieldsValue(props.imagePreview);
    setImagePreview(props.imagePreview);
  }, [props.imagePreview]);

  const imgInfo = getMediaInfo(imagePreview);

  return (
    <>
      <Modal title="Chi tiết tệp tin" visible={props.isPreviewVisible} onCancel={handleCancel} wrapClassName="w5d-modal-large" maskClosable={true} footer={null}>
        <Row gutter={[20, 20]}>
          <Col xl={18} sm={18} xs={24}>
            {(imgInfo.type === 'image' || imgInfo.type === 'doc' || imgInfo.type === 'excel') && (
              <div className="preview-thumbnail-image">
                {imagePreview.minioUrl && <img className="details-image" src={`${imagePreview.minioUrl}`} />}
                {!imagePreview.minioUrl && <img className="details-image" src={`${config.UPLOAD_API_URL}/${imagePreview.tep_tin_url}`} />}
              </div>
            )}
            {imgInfo.type === 'video' && (
              <div className="preview-thumbnail-image">
                <video controls src={`${config.UPLOAD_API_URL}/${imagePreview.tep_tin_url}`}></video>
              </div>
            )}
            {imgInfo.type === 'pdf' && (
              <div className="preview-thumbnail-image">
                <iframe src={`${config.UPLOAD_API_URL}/${imagePreview.tep_tin_url}`}  type="application/pdf"></iframe>
              </div>
            )}
          </Col>
          <Col xl={6} sm={6} xs={24} className="attachment-info">
            <div className="file-information">
              <h4>Thông tin tệp tin</h4>
              <p className="infor-item">
                <b>{getLangText('GLOBAL.TITLE')}:</b> {imagePreview.tieu_de ? imagePreview.tieu_de : '---'}
              </p>
              <p className="infor-item">
                <b>{getLangText('GLOBAL.DESCRIPTION')}:</b> {imagePreview.mo_ta ? imagePreview.mo_ta : '---'}
              </p>
              <p className="infor-item">
                <b>{getLangText('GLOBAL.AUTHOR')}:</b>{' '}
                <Link target="_blank" to={`/user/detail/${imagePreview.nguoi_tao}`}>
                  {' '}
                  {imagePreview.ten_tai_khoan}
                </Link>
              </p>
              <p className="infor-item">
                <b>Loại tệp tin:</b> {imagePreview.loai_tep_tin ? imagePreview.loai_tep_tin : '---'}
              </p>
              <p className="infor-item">
                <b>Thư mục:</b> {imagePreview.ten_nhom ? imagePreview.ten_nhom : imagePreview.nhom_tep_tin_id}
              </p>
              <p className="infor-item">
                <b>{getLangText('GLOBAL.CREATE_DATE')}:</b> {imagePreview.ngay_tao ? imagePreview.ngay_tao : '---'}
              </p>
              <p className="infor-item">
                <b>{getLangText('GLOBAL.URL')}:</b> {imagePreview.tep_tin_url ? imagePreview.tep_tin_url : '---'}
              </p>
            </div>
            <div className="file-update">
              <h4>Cập nhật tệp tin</h4>
              <Form layout="vertical" className="category-form" form={form} onFinish={onSubmit}>
                <Form.Item
                  className="input-col"
                  label={getLangText('GLOBAL.TITLE')}
                  name="tieu_de"
                  rules={[
                    {
                      required: true,
                      message: getLangText('GLOBAL.TITLE_REQUIRE'),
                    },
                  ]}
                >
                  <Input placeholder={getLangText('GLOBAL.TITLE')} />
                </Form.Item>
                <Form.Item className="input-col" label="Thư mục" name="nhom_tep_tin_id" rules={[]}>
                  {renderCates()}
                </Form.Item>
                <Form.Item className="input-col" label={getLangText('GLOBAL.DESCRIPTION')} name="mo_ta" rules={[]}>
                  <TextArea rows={2} placeholder={getLangText('GLOBAL.DESCRIPTION')} />
                </Form.Item>
                <Form.Item className="button-col">
                  <Button shape="round" type="primary" htmlType="submit">
                    Cập nhật
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </Col>
        </Row>
      </Modal>
    </>
  );
};
const mapStateToProps = (state) => {
  return {
    media: state.media,
    config: state.config,
  };
};

export default connect(mapStateToProps)(W5dPreviewImage);
