import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Form, Button, Avatar } from 'antd';
import constants from 'constants/global.constants';
import { getFileMinio } from 'redux/services/api';
import { getImagePath } from 'helpers/common.helper';
import { config } from 'config';
import defaultImage from 'assets/images/default.jpg';
function W5dImageMinIO(props) {
  const [images, setImages] = useState({});
  const [state, setState] = useState({
    configData: {},
  });

  const { image, size, shape } = props;

  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
      }));
    }
  }, [props]);

  useEffect(() => {
    if (image && image !== '' && state.configData.site_upload_server === 'minio') {
      const getData = async () => {
        const img = await getImage(image, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, img: img.data });
      };
      getData();
    }
  }, [state.configData]);
  if (!image || image === '') {
    return <Avatar src={defaultImage} size={size ? size : 50} shape={shape ? shape : 'circle'} />;
  }
  return <Avatar src={getImagePath(state.configData.site_upload_server, image, images.img, '')} size={size ? size : 50} shape={shape ? shape : 'circle'} />;
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(W5dImageMinIO);
