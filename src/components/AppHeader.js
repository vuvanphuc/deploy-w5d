import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Link } from 'react-router-dom';
import { Avatar, Layout, Row, Col, Dropdown, Menu, Button, Badge, Tooltip } from 'antd';
import moment from 'moment';
import { NotificationOutlined, WechatOutlined, UserOutlined, BellOutlined } from '@ant-design/icons';
import AutoLaTeX from 'react-autolatex';
import logoImage from '../assets/images/logo.png';
import { getUserInformation, getImagePath } from '../helpers/common.helper';
import { config } from '../config';
import { setCurrentLanguage, getLangText } from '../helpers/language.helper';
import constants from 'constants/global.constants';
import { getFileMinio } from 'redux/services/api';
import W5dImageMinIO from 'components/W5dImageMinIO';

const { Header } = Layout;
const CONFIG_CODE = constants.CONFIG_LIST.CONFIG_COMMON;

function AppHeader(props) {
  let notifications = get(props, 'notification.list.result.data', []);
  const configProps = get(props, 'config.list.result.data', []);
  const commonConfig = configProps.find((item) => item.ma_cau_hinh === CONFIG_CODE);
  const commonData = commonConfig ? JSON.parse(commonConfig.du_lieu) : {};
  const userInfo = getUserInformation();
  const [state, setState] = useState({
    configData: {},
  });
  const [images, setImages] = useState({});
  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  const totalNoti = `${get(props, 'notification.list.result.data', []).length} thông báo`;
  const menu = (
    <Menu>
      <Menu.Item key="1" className="info">
        <Link to={userInfo && userInfo.nhan_vien_id ? `/user/detail/${userInfo.nhan_vien_id}` : ''}>
          <W5dImageMinIO image={userInfo && userInfo.anh_dai_dien ? userInfo.anh_dai_dien : ''} />
          {userInfo && userInfo.ten_nhan_vien ? userInfo.ten_nhan_vien : 'Nhân viên'}
        </Link>
      </Menu.Item>
      <Menu.Item key="2" style={{ display: 'block' }}>
        <Row>
          <Col span={11}>
            <Button block type="default" onClick={props.logout}>
              {getLangText('GLOBAL.LOGOUT')}
            </Button>
          </Col>
          <Col span={2}></Col>
          <Col span={11}>
            <Link to={userInfo && userInfo.nhan_vien_id ? `/user/detail/${userInfo.nhan_vien_id}` : ''}>
              <Button block type="default">
                {getLangText('GLOBAL.PROFILE')}
              </Button>
            </Link>
          </Col>
        </Row>
      </Menu.Item>
    </Menu>
  );

  const languages = (
    <Menu>
      <Menu.Item>
        <Button type="link" onClick={() => setCurrentLanguage('en', props.dispatch)}>
          {getLangText('GLOBAL.ENGLISH')}
        </Button>
      </Menu.Item>
      <Menu.Item>
        <Button type="link" onClick={() => setCurrentLanguage('vi', props.dispatch)}>
          {getLangText('GLOBAL.VIETNAMESE')}
        </Button>
      </Menu.Item>
    </Menu>
  );

  const renderNotification = () => {
    const menu = (
      <Menu>
        <Menu.Item className="itemGroup">{totalNoti}</Menu.Item>
        {renderMenu()}
        <Menu.Item className="itemGroup">
          Xem tất cả
          <a href="/notifications/view"></a>
        </Menu.Item>
      </Menu>
    );
    return (
      <Dropdown overlay={menu} trigger="click" className="notification-header" overlayClassName="notification" placement="bottom">
        <a className="ant-dropdown-link">
          <Badge count={notifications.length} offset={[6, 0]} size="small">
            <NotificationOutlined />
          </Badge>
        </a>
      </Dropdown>
    );
  };
  const renderMenu = () => {
    if (notifications.length > 3) notifications = notifications.slice(0, 3);
    const listNotification = notifications.map((cate, idx) => {
      return (
        <Menu.Item className="item" key={cate.thong_bao_id} value={cate.thong_bao_id}>
          <Row>
            <Col xs={2} md={2} xl={2}>
              <BellOutlined />
            </Col>
            <Col xs={20} md={20} xl={20}>
              <Tooltip title={<AutoLaTeX>{`${cate.mo_ta} <br/> - Gửi lúc: ${moment(cate.ngay_tao).isValid() ? moment(new Date(cate.ngay_tao)).utc().format('HH:ss DD-MM-YYYY') : '-'}`}</AutoLaTeX>} color="#2db7f5" placement="bottom">
                {cate.tieu_de}
              </Tooltip>
            </Col>
          </Row>
        </Menu.Item>
      );
    });
    return <Menu>{listNotification}</Menu>;
  };
  const renderChats = () => {
    const menu = (
      <Menu>
        <Menu.Item>
          <a>Không có tin nhắn</a>
        </Menu.Item>
      </Menu>
    );
    return (
      <Dropdown overlay={menu} trigger="click" className="chat-header" placement="bottom">
        <a className="ant-dropdown-link">
          <Badge dot>
            <WechatOutlined />
          </Badge>
        </a>
      </Dropdown>
    );
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
      }));
    }
  }, [props]);

  useEffect(() => {
    if (state.configData.site_upload_server === 'minio') {
      const getData = async () => {
        const logo = await getImage(state.configData.site_logo, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, logo: logo.data });
      };
      getData();
    }
  }, [state.configData]);
  const APP_VERSION = process.env.REACT_APP_VERSION || 'admin_v.1.0.1';
  return (
    <Header className="header-content">
      <Row gutter={0}>
        <Col xs={6} md={4} xl={4}>
          <div className={props.collapsed ? 'unfold logo' : 'fold logo'}>
            <Link to="/">
              <Avatar src={getImagePath(state.configData.site_upload_server, state.configData.site_logo, images.logo, logoImage)} size="small" className="my-logo" />
              <span className="app-name">{commonData.site_name || 'Admin App'}</span>
            </Link>
          </div>
        </Col>
        <Col xs={6} md={12} xl={8} className="main-header-content"></Col>
        <Col xs={12} md={8} xl={12} className="content-user-menu">
          {/* {renderChats()} */}
          <div className="user-avatar">
            <Dropdown overlay={menu} placement="bottom" overlayClassName="w5d-user-dropdown" trigger={['click']}>
              <span className="app-name">
                <span className="user-label">
                  {getLangText('GLOBAL.HOWDY')},<i> {userInfo ? userInfo.ten_nhan_vien : ''}</i>
                </span>
                <UserOutlined />
              </span>
            </Dropdown>
          </div>
          {renderNotification()}
          <div className="app-version">{APP_VERSION}</div>
        </Col>
      </Row>
    </Header>
  );
}

const mapStateToProps = (state) => {
  return {
    config: state.config,
    user: state.user,
    notification: state.notification,
  };
};

export default connect(mapStateToProps)(AppHeader);
