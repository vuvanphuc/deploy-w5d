// import external libs
import React, { useEffect } from 'react';
import { Button } from 'antd';
import { get } from 'lodash';
import { connect } from 'react-redux';
import moment from 'moment';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { useParams } from 'react-router-dom';

// import internal libs
import { config } from 'config';
import * as moduleAction from 'redux/actions/moduleApp';
import { stringToSlug } from 'helpers/common.helper';
import { formatDataExport } from 'helpers/module.helper';

function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}

function ExportToWordData(props) {
  const params = useParams();
  const { moduleExport, dataId } = props;
  const temp = moduleExport.module_export_template;
  const url_temp = `${config.UPLOAD_API_URL}/${temp[0]}`;
  const formatData = (data) => {
    let formatedData = {};
    const formData = get(props, 'form.list.result.data', []).find((item) => item.bieu_mau_id === moduleExport.module_form);
    const formConfigs = JSON.parse(formData.truong_du_lieu);

    const extractWords = (str) => {
      const words = [];
      for (let i = 0; i < str.length; i++) {
        if (str.charAt(i) === '{') {
          const stopIndex = str.indexOf('}', i);
          if (stopIndex !== -1) words.push(str.substring(i + 1, stopIndex));
        }
      }
      return words;
    };

    for (const [key, value] of Object.entries(data)) {
      const configFormat = formConfigs.find((item) => item.name === key);
      const newVal = configFormat ? formatDataExport(value, configFormat) : value;
      formatedData = { ...formatedData, [key]: newVal };
    }
    moduleExport.module_export_custom_fields.forEach((item) => {
      let newVal = item.module_export_custom_field_val;
      const listItems = extractWords(newVal);
      listItems.forEach((it) => {
        const val = formatedData[it];
        newVal = newVal.replaceAll(`{${it}}`, val);
      });

      formatedData = { ...formatedData, [item.module_export_custom_field_key]: newVal };
    });
    return formatedData;
  };
  const onExport = (dataExport) => {
    try {
      loadFile(url_temp, function (error, content) {
        if (error) {
          throw error;
        }
        const zip = new PizZip(content);
        const doc = new Docxtemplater().loadZip(zip);
        const data = formatData(dataExport);
        doc.setData(data);
        try {
          doc.render();
        } catch (error) {
          function replaceErrors(key, value) {
            if (value instanceof Error) {
              return Object.getOwnPropertyNames(value).reduce(function (error, key) {
                error[key] = value[key];
                return error;
              }, {});
            }
            return value;
          }
          console.log(JSON.stringify({ error: error }, replaceErrors));

          if (error.properties && error.properties.errors instanceof Array) {
            const errorMessages = error.properties.errors
              .map(function (error) {
                return error.properties.explanation;
              })
              .join('\n');
            console.log('errorMessages', errorMessages);
          }
          throw error;
        }
        const out = doc.getZip().generate({
          type: 'blob',
          mimeType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        });
        const exportFilename = stringToSlug(moduleExport.module_export_title);
        const currentDate = moment().format(config.DATE_FORMAT_SHORT);
        saveAs(out, `${exportFilename}-${currentDate}.docx`);
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleExport = () => {
    const callback = (res) => {
      if (res.success) {
        onExport(res.data);
      }
    };

    props.dispatch(moduleAction.exportModule({ ...params, module_data_id: dataId }, callback));
  };

  return (
    <Button className="view act" type="link" onClick={() => handleExport()}>
      {moduleExport.module_export_title}
    </Button>
  );
}

const mapStateToProps = (state) => {
  return {
    module: state.module,
    config: state.config,
    form: state.form,
  };
};

export default connect(mapStateToProps)(ExportToWordData);
