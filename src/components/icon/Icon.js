import React from 'react';
import { IconPicker } from 'react-fa-icon-picker';

export default (props) => {
  return (
    <IconPicker
      value={props.value}
      onChange={(v) => props.onChangeIcon(v)}
      containerStyles={{
        borderStyle: 'solid',
        borderWidth: 1,
        height: 500,
        width: 300,
        paddingRight: 5,
        paddingLeft: 8,
      }}
      searchInputStyles={{
        borderStyle: 'solid',
        borderWidth: 1,
      }}
    />
  );
};
