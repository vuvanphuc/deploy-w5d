import React, { useState, useEffect } from 'react';
import { Form, Button, Modal } from 'antd';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { getLangText } from 'helpers/language.helper';
import { DeleteOutlined, EyeOutlined, UploadOutlined } from '@ant-design/icons';

import { config } from 'config';
import * as mediaAction from 'redux/actions/media';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dPreviewImage from 'components/W5dPreviewImage';
import fileImg from 'assets/images/file.png';

function W5dGalleryBrowser(props) {
  const [isPreviewVisible, setIsPreviewVisible] = useState(false);
  const [imagePreview, setImagePreview] = useState({});

  useEffect(() => {
    props.dispatch(mediaAction.getMedias());
  }, []);
  return (
    <div className="image-box-list">
      {/* <h2>{props.title || getLangText('GLOBAL.ALBUM_IMAGES')}</h2> */}
      <Form.Item className="button-col">
        {props.images && props.images.length ? (
          <div className="upload-imgs">
            {props.images.map((img, k) => {
              return (
                <div className="image-item" key={k}>
                  <div className="attachment-preview" onClick={() => props.openMediaLibrary(props.field)}>
                    <div className="thumbnail">
                      <div className="centered">
                        <img src={props.type === 'file' ? fileImg : `${config.UPLOAD_API_URL}/${img}`} alt={props.title} />
                      </div>
                    </div>
                  </div>

                  <Button onClick={() => props.removeImage(img)} className="btn-remove-album-image" type="link" icon={<DeleteOutlined />} />

                  <Button
                    onClick={() => {
                      const images = get(props, 'media.list.result.data', []);
                      const image = images.find((item) => item.tep_tin_url === img);
                      setIsPreviewVisible(true);
                      setImagePreview(image);
                    }}
                    className="btn-view-album-image"
                    type="link"
                    icon={<EyeOutlined />}
                  />
                </div>
              );
            })}
          </div>
        ) : (
          <Button type="dashed" onClick={() => props.openMediaLibrary(props.field)} icon={<UploadOutlined />}>
            {props.setImageLabel || getLangText('GLOBAL.SET_ALBUM_IMAGES')}
          </Button>
        )}
      </Form.Item>
      {props.visible && (
        <Modal wrapClassName="w5d-modal-media-library" visible={props.visible} onCancel={() => props.onCancel()} footer={null}>
          <W5dMediaLibrary visible={props.openMediaLibrary} onSelectImages={(images) => props.onSelectImages(images)} selected={props.images} />
        </Modal>
      )}
      <W5dPreviewImage isPreviewVisible={isPreviewVisible} imagePreview={imagePreview} folder={imagePreview.nhom_tep_tin_id} setIsPreviewVisible={(value) => setIsPreviewVisible(value)} />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    media: state.media,
    config: state.config,
  };
};

export default connect(mapStateToProps)(W5dGalleryBrowser);
