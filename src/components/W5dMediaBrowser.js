import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Form, Button, Tooltip } from 'antd';
import constants from 'constants/global.constants';
import { getFileMinio } from 'redux/services/api';
import { getImagePath } from 'helpers/common.helper';
import { getLangText } from '../helpers/language.helper';
import { config } from 'config';
import defaultImage from 'assets/images/default.jpg';

function W5dMediaBrowser(props) {
  const [images, setImages] = useState({});
  const [state, setState] = useState({
    configData: {},
  });
  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
      }));
    }
  }, [props]);

  useEffect(() => {
    if (state.configData.site_upload_server === 'minio' && props.image && props.image !== '') {
      const getData = async () => {
        const img = await getImage(props.image, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, img: img.data });
      };
      getData();
    }
  }, [state.configData]);

  return (
    <div className="border-box image-box">
      <h2>{props.title || getLangText('GLOBAL.FEATURED_IMAGE')}</h2>
      <Form.Item className="button-col">
        {props.image ? (
          <div className="upload-img">
            <img
              onClick={() => props.openMediaLibrary(props.field)}
              src={props.image === '' ? defaultImage : getImagePath(state.configData.site_upload_server, props.image, images.img, defaultImage)}
              alt={props.title}
              style={{
                width: '100%',
                cursor: 'pointer',
              }}
            />
          </div>
        ) : (
          <Button block type="link" onClick={() => props.openMediaLibrary(props.field)}>
            {props.setImageLabel || getLangText('GLOBAL.SET_FEATURED_IMAGE')}
          </Button>
        )}

        {props.image && props.image !== '' ? (
          <Tooltip>
            <Button onClick={() => props.removeImage(props.field)} className="btn-remove-image" type="link">
              {props.removeImageLabel || getLangText('GLOBAL.REMOVE_FEATURED_IMAGE')}
            </Button>
          </Tooltip>
        ) : (
          ''
        )}
      </Form.Item>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(W5dMediaBrowser);
