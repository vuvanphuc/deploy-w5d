import React from 'react';
import { Tag } from 'antd';
import constants from '../../constants/global.constants';

function W5dStatusTag(props) {
  if (props.status === 'active') return <Tag color="#87d068">{constants.COMMON_STATUS[0].title}</Tag>;
  return <Tag color="red">{constants.COMMON_STATUS[1].title}</Tag>;
}

export default W5dStatusTag;
