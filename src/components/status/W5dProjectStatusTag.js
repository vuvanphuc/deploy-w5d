import React from 'react';
import { Tag } from 'antd';
import constants from '../../constants/global.constants';

function W5dProjectStatusTag(props) {
  if (props.status === 'inprogress') return <Tag color="#108ee9">{constants.PROJECT_STATUS[0].title}</Tag>;
  if (props.status === 'active') return <Tag color="#87d068">{constants.PROJECT_STATUS[1].title}</Tag>;
  else if (props.status === 'finished') return <Tag color="#f50">{constants.PROJECT_STATUS[2].title}</Tag>;
  return <Tag color="#999">{constants.PROJECT_STATUS[3].title}</Tag>;
}

export default W5dProjectStatusTag;
