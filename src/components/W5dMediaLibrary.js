// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Upload, Button, Select, Input, Checkbox, Modal, List, Form, notification } from 'antd';
import { UploadOutlined, SendOutlined } from '@ant-design/icons';
import { get } from 'lodash';
import { useHistory } from 'react-router-dom';
import { FaRegTrashAlt } from 'react-icons/fa';
import FolderTree from 'react-folder-tree';
import queryString from 'query-string';
import { Link } from 'react-router-dom';

// import internal libs
import * as mediaAction from '../redux/actions/media';
import { config } from '../config';
import { getLangText } from '../helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from '../helpers/common.helper';
import constants from 'constants/global.constants';
import { getMediaInfo } from 'helpers/common.helper';
const { Dragger } = Upload;
const { confirm } = Modal;
const { Option } = Select;

function W5dMediaLibrary(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(history.location.search);
  const [state, setState] = useState({
    previewImage: null,
    fileList: [],
    selected: props.selected,
  });
  const [appConfig, setAppConfig] = useState({});
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(40);
  const [folders, setFolders] = useState({
    name: 'Thư viện ảnh',
    isOpen: true,
    children: [],
  });

  const [folder, setFolder] = useState(urlQuery.folder);

  const handlePreview = async (file) => {
    setState((state) => ({
      ...state,
      previewImage: file,
    }));
  };

  const onSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        setState({ ...state, previewImage: res.data });
        notification.success({
          message: 'Cập nhật tệp tin thành công.',
        });
        form.setFieldsValue(res.data);
        props.dispatch(mediaAction.getMedias({ folder: props.folder }));
      }
    };
    props.dispatch(mediaAction.editMedia({ ...previewImage, ...values }, callback));
  };

  const customRequest = async ({ onSuccess, onError, file }) => {
    const callback = () => {
      const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
      if (folder) {
        params.category = folder;
      }
      props.dispatch(
        mediaAction.getMedias(params, (res) => {
          if (res.data && res.data.length > 0) {
            props.onSelectImage(res.data[0]);
          }
        })
      );
    };
    props.dispatch(mediaAction.createMedia({ fileUpload: file, folder, server: appConfig.site_upload_server, minio_bucket: appConfig.site_minio_bucket }, callback));
  };

  const selectImage = (image) => {
    if (props.type === 'single') {
      setState((state) => ({
        ...state,
        selected: [image.tep_tin_url],
        previewImage: image,
      }));
      form.setFieldsValue(image);
    } else {
      setState((state) => ({
        ...state,
        previewImage: image,
        selected: state.selected && state.selected.includes(image.tep_tin_url) ? state.selected.filter((img) => img !== image.tep_tin_url) : state.selected.concat([image.tep_tin_url]),
      }));
    }
  };

  const renderImage = () => {
    return (
      <List
        header={null}
        footer={null}
        pagination={{
          hideOnSinglePage: false,
          responsive: true,
          showLessItems: true,
          pageSize: pageSize,
          showSizeChanger: false,
          showQuickJumper: false,
          pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
          onChange: (page, pageSize) => {
            changeMutilUrlParams({ page, pageSize });
            setPageSize(pageSize);
            setPage(page);
          },
          current: Number(page),
          showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
          showSizeChanger: true,
        }}
        dataSource={get(props, 'media.list.result.data', [])}
        renderItem={(image, idx) => {
          const imgInfo = getMediaInfo(image);
          let imgSrc = imgInfo.src;
          if (appConfig.site_upload_server === 'minio') {
            imgSrc = image.minioUrl;
          }
          return (
            <div className="image-item" key={idx} title={image.tieu_de}>
              <Checkbox checked={state.selected && state.selected.includes(image.tep_tin_url)} onChange={() => selectImage(image)} />
              <div className="attachment-preview" onClick={() => selectImage(image)}>
                <div className="thumbnail">
                  <div className="centered">
                    <img src={imgSrc} title={image.tieu_de} alt={image.tieu_de} />
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      />
    );
  };
  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(history.location.search);
    let url = history.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const renderCates = () => {
    const cates = get(props, 'media.listCates.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.nhom_tep_tin_id}>
          {cate.ten_nhom}
        </Option>
      );
    });
    return <Select placeholder="Chọn thư mục">{catesOpts}</Select>;
  };

  useEffect(() => {
    setState((state) => ({
      ...state,
      selected: props.selected,
    }));
  }, [props.selected]);

  useEffect(() => {
    const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
    props.dispatch(mediaAction.getMedias(params));
  }, []);

  useEffect(() => {
    if (props.visible) {
      const fileList =
        props.media.list.result && props.media.list.result.data && props.media.list.result.data.length > 0
          ? props.media.list.result.data.map((item) => {
              return {
                ...item,
                uid: item.tep_tin_id,
                name: item.tep_tin,
                status: 'done',
                url: `${config.UPLOAD_API_URL}/${item.tep_tin_url}`,
                imgUrl: item.tep_tin_id,
                thumbUrl: `${config.UPLOAD_API_URL}/${item.tep_tin_url}`,
              };
            })
          : [];

      setState((state) => ({
        ...state,
        fileList,
        previewImage: fileList[0] ? fileList[0] : {},
      }));
    }
  }, [props]);

  const onChangeFolder = (defaultOnClick, nodeData) => {
    // defaultOnClick();
    const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
    if (nodeData.id) {
      params.category = nodeData.id;
      setFolder(nodeData.id);
      setFolders({
        ...folders,
        children: folders.children.map((item) => ({ ...item, isOpen: item.id === nodeData.id })),
      });
      // changeMutilUrlParams({ folder: nodeData.id });
    } else {
      setFolder('');
    }
    props.dispatch(mediaAction.getMedias(params));
  };

  const onChangeFolderInfo = (newFolders) => {
    if (newFolders.children.length > folders.children.length) {
      const callback = (res) => {
        if (res.success) {
          const params = {
            server: appConfig.site_upload_server,
            bucket: appConfig.site_minio_bucket,
            category: res.data.nhom_tep_tin_id,
          };
          setFolder(res.data.nhom_tep_tin_id);
          props.dispatch(mediaAction.getMediaCates());
          props.dispatch(mediaAction.getMedias(params));
        }
      };
      props.dispatch(mediaAction.createMediaCate({ ten_nhom: 'new folder' }, callback));
    } else if (newFolders.children.length === folders.children.length) {
      for (const newNode of newFolders.children) {
        let isFinded = false;
        for (const oldNode of folders.children) {
          if (newNode.name === oldNode.name) {
            isFinded = true;
          }
        }
        if (!isFinded) {
          const callback = (res) => {
            if (res.success) {
              const params = {
                server: appConfig.site_upload_server,
                bucket: appConfig.site_minio_bucket,
                category: res.data.nhom_tep_tin_id,
              };
              setFolder(res.data.nhom_tep_tin_id);
              props.dispatch(mediaAction.getMediaCates());
              props.dispatch(mediaAction.getMedias(params));
            }
          };
          props.dispatch(mediaAction.editMediaCate({ nhom_tep_tin_id: newNode.id, ten_nhom: newNode.name }, callback));
        }
      }
    } else {
      console.log('Delete folder');
    }
  };

  const DeleteIcon = ({ onClick: defaultOnClick, nodeData }) => {
    const { name } = nodeData;
    const handleClick = () => {
      const callback = (res) => {
        if (res.success) {
          const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
          props.dispatch(mediaAction.getMedias(params));
          props.dispatch(mediaAction.getMediaCates());
          setFolder('');
          notification.success({
            message: 'Thực hiện xóa thư mục ảnh thành công.',
          });
        }
      };
      confirm({
        title: 'Bạn chắc chắn muốn xóa thư mục ' + name + ' không?',
        content: '',
        onOk() {
          props.dispatch(mediaAction.deleteMediaCate({ id: nodeData.id }, callback));
          defaultOnClick();
        },
      });
    };

    return <FaRegTrashAlt className="delete-folder-icon" onClick={handleClick} />;
  };

  const iconComponents = {
    DeleteIcon,
  };

  useEffect(() => {
    const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
    if (folder) {
      params.category = folder;
    }
    if (appConfig.site_upload_server) {
      props.dispatch(mediaAction.getMedias(params));
      props.dispatch(mediaAction.getMediaCates());
    }
  }, [appConfig]);

  useEffect(() => {
    const cates = get(props, 'media.listCates.result.data', []);
    const subFolders = cates
      .filter((item) => item.nhom_cha_id !== 'MODULE_FILE_PRIVATE')
      .map((cate) => ({
        id: cate.nhom_tep_tin_id,
        checked: false,
        isOpen: false,
        name: cate.ten_nhom,
        children: [],
      }));
    setFolders({ ...folders, children: subFolders });
  }, [props.media.listCates.result.data]);

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    const dataConfig = JSON.parse(config.du_lieu);
    setAppConfig(dataConfig);
  }, [props.config]);

  const { previewImage } = state;
  let imagePreview = previewImage;

  const imgInfo = getMediaInfo(imagePreview);

  return (
    <Row className="app-library">
      <h2>{getLangText('GLOBAL.MEDIA_LIBRARY')}</h2>
      <Col span={24} className="body-content">
        <div className="clearfix">
          <Row>
            <Col span={4}>
              <Dragger
                multiple={true}
                listType="picture"
                customRequest={(data) => customRequest(data)}
                fileList={[]}
                onPreview={(file) => handlePreview(file)}
                className="upload-list-inline"
                disabled={!shouldHaveAccessPermission('thu_vien', 'thu_vien/them')}
              >
                <p className="ant-upload-drag-icon">
                  <UploadOutlined />
                </p>
                <p className="ant-upload-text">Click hoặc kéo thả một hoặc nhiều ảnh vào đây</p>
              </Dragger>
              <FolderTree
                data={folders}
                showCheckbox={false}
                initOpenStatus="custom"
                iconComponents={iconComponents}
                onChange={(info) => onChangeFolderInfo(info)}
                onNameClick={(defaultOnClick, nodeData) => onChangeFolder(defaultOnClick, nodeData)}
              />
            </Col>
            <Col span={16}>
              <Row className="library-image-list" gutter={10}>
                {renderImage()}
              </Row>
            </Col>
            <Col span={4}>
              {previewImage && (
                <div>
                  <Row>
                    <Col span="24" style={{ textAlign: 'center' }}>
                      <Button
                        key="submit"
                        type="primary"
                        shape="round"
                        icon={<SendOutlined />}
                        disabled={!state.selected || state.selected.length <= 0}
                        onClick={() => {
                          if (props.type === 'single') {
                            props.onSelectImage(state.previewImage);
                          } else {
                            props.onSelectImages(state.selected);
                          }
                          setState({
                            previewImage: null,
                            fileList: [],
                            selected: [],
                          });
                        }}
                      >
                        {props.type === 'single' ? 'Chọn ảnh này' : 'Chọn những ảnh này'}
                      </Button>
                    </Col>
                    <Col span="24" className="preview-box">
                      {(imgInfo.type === 'image' || imgInfo.type === 'doc' || imgInfo.type === 'excel') && (
                        <div className="preview-thumbnail">
                          {imagePreview.minioUrl && <img className="details-image" src={`${imagePreview.minioUrl}`} />}
                          {!imagePreview.minioUrl && <img className="details-image" src={`${config.UPLOAD_API_URL}/${imagePreview.tep_tin_url}`} />}
                        </div>
                      )}
                      {imgInfo.type === 'video' && (
                        <div className="preview-thumbnail">
                          <video controls autoPlay src={`${config.UPLOAD_API_URL}/${imagePreview.tep_tin_url}`}></video>
                        </div>
                      )}
                      {imgInfo.type === 'pdf' && (
                        <div className="preview-thumbnail">
                          <iframe src={`${config.UPLOAD_API_URL}/${imagePreview.tep_tin_url}`}></iframe>
                        </div>
                      )}

                      <div className="file-information">
                        <h4>Thông tin tệp tin</h4>
                        <p className="infor-item">
                          <b>{getLangText('GLOBAL.TITLE')}:</b> {previewImage.tieu_de ? previewImage.tieu_de : '---'}
                        </p>
                        <p className="infor-item">
                          <b>{getLangText('GLOBAL.DESCRIPTION')}:</b> {previewImage.mo_ta ? previewImage.mo_ta : '---'}
                        </p>
                        <p className="infor-item">
                          <b>{getLangText('GLOBAL.AUTHOR')}:</b>{' '}
                          <Link target="_blank" to={`/user/detail/${previewImage.nguoi_tao}`}>
                            {' '}
                            {previewImage.ten_tai_khoan}
                          </Link>
                        </p>
                        <p className="infor-item">
                          <b>Loại tệp tin:</b> {previewImage.loai_tep_tin ? previewImage.loai_tep_tin : '---'}
                        </p>
                        <p className="infor-item">
                          <b>Thư mục:</b> {previewImage.ten_nhom ? previewImage.ten_nhom : '---'}
                        </p>
                        <p className="infor-item">
                          <b>{getLangText('GLOBAL.CREATE_DATE')}:</b> {previewImage.ngay_tao ? previewImage.ngay_tao : '---'}
                        </p>
                        <p className="infor-item">
                          <b>{getLangText('GLOBAL.URL')}:</b> {previewImage.tep_tin_url ? previewImage.tep_tin_url : '---'}
                        </p>
                      </div>
                      {/* <div className="file-update">
                        <h4>Cập nhật tệp tin</h4>
                        <Form layout="vertical" className="category-form" form={form} onFinish={onSubmit}>
                          <Form.Item
                            className="input-col"
                            label={getLangText('GLOBAL.TITLE')}
                            name="tieu_de"
                            rules={[
                              {
                                required: true,
                                message: getLangText('GLOBAL.TITLE_REQUIRE'),
                              },
                            ]}
                          >
                            <Input placeholder={getLangText('GLOBAL.TITLE')} />
                          </Form.Item>
                          <Form.Item className="input-col" label="Thư mục" name="nhom_tep_tin_id" rules={[]}>
                            {renderCates()}
                          </Form.Item>
                          <Form.Item className="input-col" label={getLangText('GLOBAL.DESCRIPTION')} name="mo_ta" rules={[]}>
                            <TextArea rows={2} placeholder={getLangText('GLOBAL.DESCRIPTION')} />
                          </Form.Item>
                          <Form.Item className="button-col">
                            <Button shape="round" type="primary" htmlType="submit">
                              Cập nhật
                            </Button>
                          </Form.Item>
                        </Form>
                      </div> */}
                    </Col>
                  </Row>
                </div>
              )}
            </Col>
          </Row>
        </div>
      </Col>
    </Row>
  );
}

const mapStateToProps = (state) => {
  return {
    media: state.media,
    config: state.config,
  };
};

export default connect(mapStateToProps)(W5dMediaLibrary);
