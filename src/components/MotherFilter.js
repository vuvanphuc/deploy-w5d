import React from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Col, Select, Input, Badge } from 'antd';
import queryString from 'query-string';
import { getLangText } from '../helpers/language.helper';
import constants from '../constants/global.constants';
import { get } from 'lodash';

const { Option } = Select;
const { Search } = Input;

function MotherFilter(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(history.location.search);

  const renderUsers = () => {
    const defaultUser = urlQuery.user ? urlQuery.user.split('_') : [];
    const users = get(props, 'users.data', []);
    const userOpts = users.map((user, idx) => {
      return (
        <Option key={idx} value={user.nhan_vien_id}>
          {user.ten_nhan_vien}
        </Option>
      );
    });
    return (
      <Select
        mode="multiple"
        maxTagCount="responsive"
        defaultValue={defaultUser}
        onChange={(value) => {
          props.onFilterChange('user', value.join('_'));
        }}
        placeholder="Người tạo"
      >
        {userOpts}
      </Select>
    );
  };

  const renderStatus = () => {
    const status = props.status ? props.status : constants.COMMON_REVIEW_STATUS;
    return status.map((label) => (
      <Option key={label.value} value={label.value}>
        <Badge color={label.color || 'black'} text={label.title} />
      </Option>
    ));
  };

  return (
    <Row>
      <Col span={24} className="filter-todo">
        <Row>
          <Col xl={6} sm={24} xs={24}>
            <h2>{props.title || ''} </h2>
          </Col>
          <Col xl={18} sm={24} xs={24}>
            <Row>
              <Col md={8} xs={12}>
                <Select
                  defaultValue={props.search && props.search.status ? props.search.status : ''}
                  placeholder="Trạng thái kiểm duyệt"
                  value={urlQuery.status}
                  onChange={(value) => props.onFilterChange('status', value)}
                >
                  <Option key="all" value="">
                    <Badge color="pink" text="Trạng thái kiểm duyệt" />
                  </Option>
                  {renderStatus()}
                </Select>
              </Col>
              <Col md={8} xs={12}>
                {renderUsers()}
              </Col>
              <Col md={8} xs={12}>
                <Search placeholder={getLangText('GLOBAL.SEARCH_TEXT')} value={urlQuery.keyword} onChange={(e) => props.onFilterChange('keyword', e.target.value)} />
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default MotherFilter;
