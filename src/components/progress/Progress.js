import React from 'react';
import 'components/progress/Progress.css';
import { Progress } from 'antd';

// import internal libs

function ProgressBar(props) {
  return (
    <div className="progress-global">
      <div className="progress">
        <Progress
          type="circle"
          strokeColor={{
            '0%': '#108ee9',
            '100%': '#87d068',
          }}
          percent={props.percent}
        />
      </div>
    </div>
  );
}

export default ProgressBar;
