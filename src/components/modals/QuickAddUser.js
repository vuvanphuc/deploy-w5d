// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Form, Input, Row, Col, Select, Modal, Switch, notification } from 'antd';
import { UnorderedListOutlined, PlusOutlined } from '@ant-design/icons';

// import internal libs
import { config } from '../../config';
import constants from '../../constants/global.constants';
import { getLangText } from '../../helpers/language.helper';
import * as userAction from '../../redux/actions/user';

const { Option } = Select;

function QuickAddUser(props) {
  const [form] = Form.useForm();

  const defaultForm = {
    fullname: '',
    username: '',
    email: '',
    phone: '',
    image: '',
    password: 'kh123456',
    categories: '',
    status: 'active',
    address: '',
    website: '',
    isVerified: true,
    birthday: null,
    uploadLoading: false,
    introduction: '',
    isChanged: false,
    isChangePass: false,
    openMediaLibrary: false,
    insertMedia: {
      open: false,
      editor: null,
    },
  };

  // const openMediaLibrary = () => setState(state => ({ ...state, openMediaLibrary: true }));

  // const handleChangeDate = date => setState({ ...state, birthday: date, isChanged: true });

  const handleSubmit = (values) => {
    //add a new user
    const callback = (data) => {
      notification.success({
        message: getLangText('MESSAGE.CREATE_USER_SUCCESS'),
      });
      setState((state) => ({
        ...state,
        isChanged: false,
      }));
      props.onCancel();
    };
    props.dispatch(userAction.createUser({ ...state, ...values }, callback));
  };

  const showField = (field) => (field ? field : '');

  const removeImage = () => setState((state) => ({ ...state, image: '' }));

  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('newPassword')) {
      callback(getLangText('MESSAGE.PASSWORD_NOT_MATCH'));
    } else {
      callback();
    }
  };

  const validateToNextPassword = (rule, value, callback) => {
    if (value && state.confirmDirty) {
      form.validateFields(['confirm-pass'], { force: true });
    }
    callback();
  };

  const renderUserCategories = () => {
    let options = [];
    if (props.user.listCates.result && props.user.listCates.result.data) {
      options = props.user.listCates.result.data.map((cate) => (
        <Option key={cate._id} value={cate._id}>
          {cate.title}
        </Option>
      ));
      return (
        <Select showSearch loading={props.user.listCates.loading} onChange={(category) => setState({ ...state, category, isChanged: true })} placeholder={getLangText('GLOBAL.SELECT_CATEGORIES')}>
          {options}
        </Select>
      );
    }
  };

  const renderUserStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select onChange={(status) => setState({ ...state, status, isChanged: true })} placeholder={getLangText('GLOBAL.SELECT_STATUS')}>
        {options}
      </Select>
    );
  };

  const isEdit = props.match && props.match.params && props.match.params.id;

  const [state, setState] = useState(defaultForm);

  const openInsertMedia = (editor) =>
    setState((state) => ({
      ...state,
      insertMedia: {
        open: true,
        editor,
      },
      isChanged: true,
    }));

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      image: file.imgUrl,
    }));
  };

  const onInsertImage = (file) => {
    state.insertMedia.editor.insertContent('<img class="image-editor" src="' + `${config.UPLOAD_API_URL}/${file.imgUrl}` + '"/>');
    setState((state) => ({
      ...state,
      isChanged: true,
      insertMedia: {
        open: false,
      },
    }));
  };

  const onResetForm = () => {
    if (props.user.item.result && isEdit) {
      form.setFieldsValue(props.user.item.result.data);
      setState((state) => ({ ...state, ...props.user.item.result.data }));
    } else {
      form.resetFields();
      setState((state) => ({ ...state, ...defaultForm }));
    }
  };

  useEffect(() => {
    props.dispatch(userAction.getUserCates());
    if (isEdit) {
      props.dispatch(
        userAction.getUser({
          id: props.match.params.id,
        })
      );
    }
    return () => (window.onbeforeunload = null);
  }, []);

  useEffect(() => {
    if (isEdit && props.user.item.result) {
      setState((state) => ({ ...state, ...props.user.item.result.data }));
    }
    onResetForm();
    window.onbeforeunload = null;
  }, [props]);

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }
  return (
    <Row>
      <Col span={24} className="body-content">
        <div className="w5d-form">
          <Form layout="vertical" className="" onFinish={handleSubmit} form={form}>
            <Row gutter={25}>
              <Col span={18}>
                <h2 className="header-form-title">{isEdit ? getLangText('USER.EDIT_USER') : getLangText('USER.ADD_NEW_USER')}</h2>
              </Col>
              <Col span={6}>
                <Col span={4}></Col>
                <Col span={20}>
                  <Link to={isEdit ? '/user/add' : '/users'}>
                    <Button block shape="round" type="primary" icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />} className="btn-create-todo">
                      {isEdit ? getLangText('USER.ADD_NEW_USER') : getLangText('USER.LIST_USERS')}
                    </Button>
                  </Link>
                </Col>
              </Col>
              <Col span={18} className="left-content">
                <div className="border-box">
                  <Row gutter={25}>
                    <Col span={12}>
                      <Form.Item
                        className="input-col"
                        label={getLangText('GLOBAL.FULLNAME')}
                        name="fullname"
                        rules={[
                          {
                            required: true,
                            message: getLangText('GLOBAL.FULLNAME_REQUIRE'),
                          },
                        ]}
                      >
                        <Input
                          placeholder={getLangText('GLOBAL.FULLNAME')}
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                      <Form.Item
                        className="input-col"
                        label={getLangText('GLOBAL.USERNAME')}
                        name="username"
                        rules={[
                          {
                            required: true,
                            message: getLangText('GLOBAL.USERNAME_REQUIRE'),
                          },
                        ]}
                      >
                        <Input
                          disabled={isEdit}
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                          placeholder={getLangText('GLOBAL.USERNAME')}
                        />
                      </Form.Item>
                      <Form.Item
                        className="input-col"
                        label={getLangText('GLOBAL.EMAIL')}
                        name="email"
                        rules={[
                          {
                            required: true,
                            message: getLangText('GLOBAL.EMAIL_REQUIRE'),
                          },
                        ]}
                      >
                        <Input
                          disabled={isEdit}
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                          placeholder={getLangText('GLOBAL.EMAIL')}
                        />
                      </Form.Item>
                      <Form.Item className="input-col" label={getLangText('GLOBAL.PHONE')} name="phone" rules={[]}>
                        <Input
                          placeholder={getLangText('GLOBAL.PHONE')}
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      {isEdit && (
                        <Form.Item name="isChangePass" label="Đổi mật khẩu" valuePropName="checked">
                          <Switch
                            onClick={() => {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                                isChangePass: !state.isChangePass,
                              }));
                            }}
                          />
                        </Form.Item>
                      )}
                      {isEdit && state.isChangePass && (
                        <>
                          <Form.Item
                            className="input-col"
                            name="newPassword"
                            label={getLangText('GLOBAL.PASSWORD')}
                            rules={[
                              {
                                required: true,
                                message: getLangText('GLOBAL.PASSWORD_REQUIRE'),
                              },
                              {
                                min: 6,
                                message: getLangText('GLOBAL.PASSWORD_VALIDATE_MIN'),
                              },
                              { validator: validateToNextPassword },
                            ]}
                          >
                            <Input.Password
                              value=""
                              onChange={() => {
                                if (!state.isChanged) {
                                  setState((state) => ({
                                    ...state,
                                    isChanged: true,
                                  }));
                                }
                              }}
                            />
                          </Form.Item>
                          <Form.Item
                            className="input-col"
                            name="confirm-pass"
                            label={getLangText('GLOBAL.CONFIRM_PASSWORD')}
                            rules={[
                              {
                                required: true,
                                message: getLangText('GLOBAL.CONFIRM_PASSWORD_REQUIRE'),
                              },
                              {
                                validator: compareToFirstPassword,
                              },
                            ]}
                          >
                            <Input.Password
                              onChange={() => {
                                if (!state.isChanged) {
                                  setState((state) => ({
                                    ...state,
                                    isChanged: true,
                                  }));
                                }
                              }}
                            />
                          </Form.Item>
                        </>
                      )}
                      {!isEdit && (
                        <Form.Item
                          className="input-col"
                          label={getLangText('GLOBAL.PASSWORD')}
                          name="password"
                          rules={[
                            {
                              required: true,
                              message: getLangText('GLOBAL.PASSWORD_REQUIRE'),
                            },
                          ]}
                        >
                          <Input.Password
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                      )}
                      <Form.Item className="input-col" label={getLangText('GLOBAL.ADDRESS')} name="address" rules={[]}>
                        <Input placeholder={getLangText('GLOBAL.ADDRESS')} />
                      </Form.Item>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={6} className="right-content">
                <div className="box">
                  <div className="box-body">
                    <div className="border-box">
                      <h2>{getLangText('FORM.PUBLISH')}</h2>
                      <Form.Item className="input-col">
                        <Row>
                          <Col span="11">
                            <Button block type="danger" htmlType="reset" onClick={() => onResetForm()}>
                              {getLangText('FORM.RESET')}
                            </Button>
                          </Col>
                          <Col span="2"></Col>
                          <Col span="11">
                            <Button block type="primary" htmlType="submit">
                              {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                            </Button>
                          </Col>
                        </Row>
                      </Form.Item>
                    </div>
                    <div className="border-box">
                      <h2>{getLangText('GLOBAL.CATEGORIES')}</h2>
                      <Form.Item className="input-col" name="categories">
                        {renderUserCategories()}
                      </Form.Item>
                    </div>
                    <div className="border-box">
                      <h2>{getLangText('GLOBAL.STATUS')}</h2>
                      <Form.Item className="input-col" name="status">
                        {renderUserStatus()}
                      </Form.Item>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </Col>
      <Modal
        wrapClassName="w5d-modal-large"
        visible={state.isOpenFormAddUser}
        onCancel={() =>
          setState((state) => ({
            ...state,
            isOpenFormAddUser: false,
          }))
        }
        footer={null}
      >
        <QuickAddUser />
      </Modal>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </Row>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    todo: state.todo,
  };
};

export default connect(mapStateToProps)(QuickAddUser);
