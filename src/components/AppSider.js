import React, { useState, useEffect } from 'react';
import { IconPickerItem } from 'react-fa-icon-picker';

import { connect } from 'react-redux';
import { get } from 'lodash';
import { useHistory } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { PlusSquareOutlined, BarsOutlined, DeploymentUnitOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { getUserInformation } from '../helpers/common.helper';
import { ROUTES } from '../routers';
import { shouldHaveAccessPermission } from '../helpers/common.helper';
import constants from 'constants/global.constants';

const { Sider } = Layout;
const { SubMenu } = Menu;

const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_MODULE;
const CONFIG_DEDUCE_KEY = constants.CONFIG_LIST.CONFIG_DEDUCE;
const CONFIG_CATEGORY_KEY = constants.CONFIG_LIST.CONFIG_CATEGORY;

function AppSider(props) {
  const currentOpenKey = localStorage.getItem('currentOpenKey');
  const [routers, setRouters] = useState(ROUTES);
  const [openKeys, setOpenKeys] = React.useState(currentOpenKey ? JSON.parse(currentOpenKey) : []);
  const history = useHistory();
  const magicTypes = ['/list/', '/view/', '/edit/', '/detail/'];
  const userInfo = getUserInformation();

  const findAllOpenKey = (mainMenus) => {
    let allKeys = [];
    mainMenus.forEach((main, key) => {
      if (main.sub) {
        allKeys = [...allKeys, `sub${key}`];
      }
    });
    return allKeys;
  };

  const onOpenChange = (keys) => {
    const rootSubmenuKeys = findAllOpenKey(routers);
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
      localStorage.setItem('currentOpenKey', JSON.stringify(keys));
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
      localStorage.setItem('currentOpenKey', JSON.stringify(latestOpenKey ? [latestOpenKey] : []));
    }
  };

  const onOpenClick = (menu) => {
    if (!menu.key.includes('sub')) {
      setOpenKeys([]);
      localStorage.setItem('currentOpenKey', JSON.stringify([]));
    }
  };
  const renderMenu2Levels = (mainMenus, root, magicKey) => {
    return mainMenus.map((main, key) => {
      if (main.hidden) return;
      const listSubPaths = main.sub ? main.sub.map((item) => item.path.replace('/:id', '')) : [];
      let isActive = listSubPaths.find((pathStr) => history.location.pathname.includes(pathStr));
      if (main.sub) {
        const isMagicCase = magicTypes.find((item) => history.location.pathname.includes(item));
        if (isMagicCase && history.location.pathname.includes(main.id)) {
          isActive = true;
        }
        return (
          <SubMenu
            key={`sub${key}`}
            className={`${isActive ? 'm-active' : ''} m-sub`}
            title={
              <a>
                {main.icon ? main.icon : <PlusSquareOutlined />}
                <span className="nav-text">{main.name}</span>
              </a>
            }
          >
            {renderMenu2Levels(main.sub, false, key)}
          </SubMenu>
        );
      }
      return (
        <Menu.Item key={root ? key : `sub-${magicKey}-${key}`} className={`${history.location.pathname === main.path || isActive ? 'm-active ' : ''} m-item`}>
          <Link to={main.path}>
            {main.icon ? main.icon : <PlusSquareOutlined />}
            <span className="nav-text">{main.name}</span>
          </Link>
        </Menu.Item>
      );
    });
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    const configDeduce = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_DEDUCE_KEY);
    const configCategory = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_CATEGORY_KEY);
    let currentRouters = ROUTES;
    if (config) {
      let data = {};
      try {
        data = JSON.parse(config.du_lieu);
      } catch (e) {
        console.log('Error: ', e);
        data = {};
      }

      const mainItems = get(data, 'modules', []).filter((item) => {
        return item.module_location === 'root';
      });
      const modules_routers = mainItems.map((mode) => {
        return {
          exact: true,
          hidden: !shouldHaveAccessPermission(mode.module_id),
          icon: (
            <span className="anticon">
              <IconPickerItem
                icon={mode.module_icon}
                color={'#FFFFFF'}
                size={16}
                containerStyles={{
                  display: 'inline-block',
                  paddingLeft: 0,
                  paddingRight: 0,
                  marginLeft: 0,
                }}
              />
            </span>
          ),
          id: mode.module_id,
          name: mode.module_name,
          path: `/module/${mode.module_id}`,
          sub: [
            {
              exact: true,
              hidden: !shouldHaveAccessPermission(mode.module_id, `${mode.module_id}/xem`),
              icon: <BarsOutlined />,
              id: mode.module_id,
              name: 'Danh sách',
              path: `/module/${mode.module_id}`,
            },
            {
              exact: true,
              hidden: !shouldHaveAccessPermission(mode.module_id, `${mode.module_id}/them`),
              icon: <PlusSquareOutlined />,
              id: mode.module_id,
              name: 'Thêm mới',
              path: `/module/${mode.module_id}/add`,
            },
          ],
        };
      });
      currentRouters = [...currentRouters].concat(modules_routers);
      setRouters(currentRouters);
    }

    // if (configCategory) {
    //   const data = JSON.parse(configCategory.du_lieu);
    //   let category_routers = {
    //     exact: true,
    //     hidden: !(data.modules.length > 0),
    //     icon: <BarsOutlined />,
    //     id: 'category',
    //     name: 'Danh mục',
    //     path: '/category',
    //     sub: [],
    //   };
    //   data.modules.map((mode) => {
    //     category_routers.sub.push({
    //       exact: true,
    //       hidden: !shouldHaveAccessPermission(mode.module_id, `${mode.module_id}/xem`),
    //       icon: (
    //         <IconPickerItem
    //           icon={mode.module_icon}
    //           color={'#FFFFFF'}
    //           size={16}
    //           containerStyles={{
    //             display: 'inline-block',
    //             paddingLeft: 0,
    //             paddingRight: 9,
    //             marginLeft: 0,
    //           }}
    //         />
    //       ),
    //       id: mode.module_id,
    //       name: mode.module_name,
    //       path: `/category/${mode.module_id}`,
    //     });
    //   });
    //   currentRouters = [...currentRouters].concat(category_routers);
    //   setRouters(currentRouters);
    // }

    // if (configDeduce) {
    //   const data = JSON.parse(configDeduce.du_lieu);
    //   const deduce_routers = data.modules.map((mode) => {
    //     return {
    //       exact: true,
    //       hidden: !shouldHaveAccessPermission(mode.module_id),
    //       icon: (
    //         <span className="anticon">
    //           <IconPickerItem
    //             icon={mode.module_icon}
    //             color={'#FFFFFF'}
    //             size={16}
    //             containerStyles={{
    //               display: 'inline-block',
    //               paddingLeft: 0,
    //               paddingRight: 0,
    //               marginLeft: 0,
    //             }}
    //           />
    //         </span>
    //       ),
    //       id: mode.module_id,
    //       name: mode.module_name,
    //       path: `/deduction/${mode.module_id}`,
    //       sub: [
    //         {
    //           exact: true,
    //           hidden: !shouldHaveAccessPermission(mode.module_id, `${mode.module_id}/xem`),
    //           icon: <BarsOutlined />,
    //           id: mode.module_id,
    //           name: 'Danh sách',
    //           path: `/deduction/${mode.module_id}/list`,
    //         },
    //         {
    //           exact: true,
    //           hidden: !shouldHaveAccessPermission(mode.module_id, `${mode.module_id}/xem`),
    //           icon: <DeploymentUnitOutlined />,
    //           id: mode.module_id,
    //           name: 'Suy diễn',
    //           path: `/deduction/${mode.module_id}/detail`,
    //         },
    //       ],
    //     };
    //   });
    //   currentRouters = [...currentRouters].concat(deduce_routers);
    //   setRouters(currentRouters);
    // }
  }, [props.config.list.result.data]);

  useEffect(() => {
    if (userInfo && userInfo.nhom && userInfo.nhom.hien_thi_menu_con) {
      const rootSubmenuKeys = findAllOpenKey(routers);
      setOpenKeys(rootSubmenuKeys);
    }
  }, [routers]);

  

  return (
    <Sider className="app-sider" trigger={null} width={250} collapsible collapsed={props.collapsed} collapsedWidth={55} breakpoint="md">
      <BarsOutlined onClick={props.toggle} className="mobile-bar" />
      <Menu className="app-menu" mode="inline" onOpenChange={onOpenChange} openKeys={openKeys} onClick={onOpenClick}>
        {renderMenu2Levels(routers, true)}
      </Menu>
      
    </Sider>
  );
}

const mapStateToProps = (state) => {
  return {
    config: state.config,
  };
};

export default connect(mapStateToProps)(AppSider);
