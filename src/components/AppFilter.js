import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Col, Select, Input, Badge, DatePicker } from 'antd';
import queryString from 'query-string';
import { getLangText } from '../helpers/language.helper';
import constants from '../constants/global.constants';
import { get } from 'lodash';
import moment from 'moment';

const { Option } = Select;
const { Search } = Input;
const { RangePicker } = DatePicker;
function AppFilter(props) {
  const dateFormat = 'DD-MM-YYYY';
  const history = useHistory();
  const urlQuery = queryString.parse(history.location.search);
  const [date, setDate] = useState(urlQuery.date ? moment(urlQuery.date, dateFormat) : moment(props.search.date, dateFormat));
  const renderStatus = () => {
    const status = props.status ? props.status : constants.COMMON_STATUS;
    return status.map((label) => (
      <Option key={label.value} value={label.value}>
        <Badge color={label.color || 'black'} text={label.title} />
      </Option>
    ));
  };

  const renderCates = () => {
    const defaultCate = urlQuery.categories ? urlQuery.categories.split('_') : [];
    const cates = get(props, 'categories', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.id}>
          {cate.name}
        </Option>
      );
    });
    return (
      <Select
        mode="multiple"
        disabled={props.isDisableCategories}
        maxTagCount="responsive"
        value={defaultCate}
        onChange={(value) => {
          props.onFilterChange('categories', value.join('_'));
        }}
        placeholder="Danh mục dữ liệu"
      >
        {catesOpts}
      </Select>
    );
  };

  return (
    <Row>
      <Col span={24} className="filter-todo">
        <Row>
          <Col xl={8} sm={8} xs={8}>
            <h2>{props.title || ''} </h2>
          </Col>
          <Col xl={16} sm={16} xs={16}>
            <Row>
              {props.isShowCategories && (
                <Col xl={6} md={6} xs={12}>
                  {renderCates()}
                </Col>
              )}
              {props.isShowStatus && (
                <Col xl={6} md={6} xs={12}>
                  <Select
                    defaultValue={props.search && props.search.status ? props.search.status : ''}
                    placeholder={getLangText('GLOBAL.STATUS')}
                    value={urlQuery.status}
                    onChange={(value) => props.onFilterChange('status', value)}
                  >
                    <Option key="all" value="">
                      <Badge color="black" text={getLangText('GLOBAL.ALL_STATUS')} />
                    </Option>
                    {renderStatus()}
                  </Select>
                </Col>
              )}
              {props.isShowDatePicker && (
                <Col xl={6} md={6} xs={24}>
                  {props.isRangeDatePicker ? (
                    <RangePicker
                      locale={{
                        lang: {
                          locale: 'en_US',
                          rangePlaceholder: ['Từ ngày', 'Đến ngày'],
                        },
                      }}
                      style={{ width: '95%' }}
                      onChange={(date) => props.onDateChange(date ? date : '', '')}
                      defaultValue={[urlQuery.startDate ? moment(urlQuery.startDate, dateFormat) : '', urlQuery.endDate ? moment(urlQuery.endDate, dateFormat) : '']}
                      format={dateFormat}
                      allowEmpty={[[false, false]]}
                      ranges={{
                        'Hôm nay': [moment(), moment()],
                        'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                        'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                        'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                      }}
                    />
                  ) : (
                    <DatePicker
                      placeholder={getLangText('GLOBAL.SEARCH_DATE')}
                      defaultValue={urlQuery.date ? moment(urlQuery.date, dateFormat) : ''}
                      value={date ? date : ''}
                      onChange={(value) => {
                        if (value) {
                          setDate(moment(value, dateFormat).utc(0));
                          props.onFilterChange('date', moment(value, dateFormat).utc(0).format(dateFormat));
                        } else props.onFilterChange('date', null);
                      }}
                      showToday
                      style={{
                        width: '95%',
                      }}
                      format={dateFormat}
                    />
                  )}
                </Col>
              )}
              {props.isShowSearchBox && (
                <Col md={6} xs={24}>
                  <Search placeholder={getLangText('GLOBAL.SEARCH_TEXT')} value={urlQuery.keyword} onChange={(e) => props.onFilterChange('keyword', e.target.value)} />
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default AppFilter;
