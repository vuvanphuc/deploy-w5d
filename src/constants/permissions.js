// Danh sách các chức năng cho các mô đun

export const PERMISSIONS = [
  // DEFINE ROLES
  {
    name: 'Lịch hẹn',
    key: 'lich_hen',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem chi tiết',
        key: 'chi_tiet',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Bình luận',
    key: 'binh_luan',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem chi tiết',
        key: 'chi_tiet',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Khóa học',
    key: 'khoa_hoc',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách ',
        key: 'xem',
      },
      {
        name: 'Xóa ',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Danh mục khóa học',
    key: 'nhom_khoa_hoc',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Bài học',
    key: 'bai_hoc',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách ',
        key: 'xem',
      },
      {
        name: 'Xóa ',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Danh mục bài học',
    key: 'nhom_bai_hoc',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả ',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Đề thi',
    key: 'de_thi',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Danh mục đề thi',
    key: 'nhom_de_thi',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Chủ',
    key: 'chu_de_thi',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa chủ đề',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả ',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Lịch sử thi',
    key: 'lich_su_thi',
    subs: [
      {
        name: 'Thêm mới ',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa ',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả ',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Câu hỏi',
    key: 'cau_hoi',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Danh mục câu hỏi',
    key: 'nhom_cau_hoi',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Tin tức',
    key: 'tin_tuc',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Danh mục  tin tức',
    key: 'nhom_tin_tuc',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Thực đơn',
    key: 'thuc_don',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Danh mục thực đơn',
    key: 'nhom_thuc_don',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Nhắc nhở',
    key: 'nhac_nho',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Xem chi tiết',
        key: 'chi_tiet',
      },
    ],
  },
  {
    name: 'Danh mục nhắc nhở',
    key: 'nhom_nhac_nho',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Thông báo',
    key: 'thong_bao',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Danh mục thông báo',
    key: 'nhom_thong_bao',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
    ],
  },
  {
    name: 'Trang chủ',
    key: 'trang_chu',
    subs: [
      {
        name: 'Truy cập trang chủ',
        key: 'truy_cap',
      },
    ],
  },
  {
    name: 'Nhân viên',
    key: 'nhan_vien',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem chi tiết',
        key: 'chi_tiet',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
    ],
  },
  {
    name: 'Danh mục nhân viên',
    key: 'nhom_nhan_vien',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Phân quyền',
        key: 'phan_quyen',
      },
      {
        name: 'Truy cập trang quản trị',
        key: 'truy_cap',
      },
      {
        name: 'Truy cập app bệnh nhân',
        key: 'truy_cap_app_benh_nhan',
      },
      {
        name: 'Truy cập app bác sĩ',
        key: 'truy_cap_app_bac_si',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Truy cập trang khách',
        key: 'truy_cap_trang_khach',
      },
    ],
  },
  {
    name: 'Trường dữ liệu',
    key: 'truong_du_lieu',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
    ],
  },
  {
    name: 'Bảng dữ liệu',
    key: 'bang_nghiep_vu',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
      {
        name: 'Thêm trường dữ liệu từ bảng khác',
        key: 'add_column',
      },
      {
        name: 'Xóa bảng CSDL',
        key: 'xoa_bang_csdl',
      },
      {
        name: 'Tạo bảng CSDL',
        key: 'tao_bang_csdl',
      },
    ],
  },
  {
    name: 'Biểu mẫu',
    key: 'bieu_mau',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem chi tiết',
        key: 'chi_tiet',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
    ],
  },
  {
    name: 'Danh mục biểu mẫu',
    key: 'nhom_bieu_mau',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
    ],
  },

  {
    name: 'Thư viện',
    key: 'thu_vien',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
    ],
  },
  {
    name: 'Cấu hình',
    key: 'cau_hinh',
    subs: [
      {
        name: 'Cấu hình chung',
        key: 'chung',
      },
      {
        name: 'Cấu hình mobile',
        key: 'mobile',
      },
      {
        name: 'Cấu hình trang khách',
        key: 'khach',
      },
      {
        name: 'Cấu hình thành viên',
        key: 'user',
      },
      {
        name: 'Cấu hình email',
        key: 'email',
      },
      {
        name: 'Cấu hình mô đun',
        key: 'module',
      },
      {
        name: 'Cấu hình mô đun hệ suy diễn',
        key: 'deduce-module',
      },
      {
        name: 'Cấu hình mô đun',
        key: 'category-module',
      },
      {
        name: 'Cấu hình quản lý CronJob',
        key: 'configCronjob',
      },
    ],
  },
  {
    name: 'Tập luật',
    key: 'tap_luat',
    subs: [
      {
        name: 'Thêm mới',
        key: 'them',
      },
      {
        name: 'Cập nhật',
        key: 'sua',
      },
      {
        name: 'Xem danh sách',
        key: 'xem',
      },
      {
        name: 'Xem tất cả',
        key: 'xem_tat_ca',
      },
      {
        name: 'Xóa',
        key: 'xoa',
      },
    ],
  },
];
