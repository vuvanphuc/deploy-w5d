export const FIELD_TYPES = [
  // trường cơ bản
  {
    name: 'Cơ bản',
    isSupportRepeater: true,
    items: [
      {
        key: 'text',
        name: 'Text',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Giới hạn số kí tự', type: 'text', name: 'max_length' },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
        isSupportRepeater: true,
      },
      {
        key: 'text-area',
        name: 'Text area',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Giới hạn số kí tự', type: 'text', name: 'max_length' },
          {
            title: 'Số dòng',
            type: 'number',
            name: 'rows',
            min: 1,
            default_value: 3,
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
        isSupportRepeater: true,
      },
      {
        key: 'text-editor',
        name: 'Text editor',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
        isSupportRepeater: false,
      },
      {
        key: 'number',
        name: 'Number',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Giá trị tối thiểu', type: 'number', name: 'min' },
          { title: 'Giá trị tối đa', type: 'number', name: 'max' },
          {
            title: 'Bước nhảy',
            type: 'number',
            name: 'step',
            min: 0,
            default_value: 1,
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
        isSupportRepeater: true,
      },
      {
        key: 'email',
        name: 'E-mail',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'url',
        name: 'Url',
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'password',
        name: 'Password',
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
    ],
  },
  // trường lựa chọn
  {
    name: 'Lựa chọn',
    isSupportRepeater: true,
    items: [
      {
        key: 'select',
        name: 'Select box',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          {
            title: 'Lựa chọn',
            type: 'options',
            name: 'options',
            default_value: [],
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
        isSupportRepeater: true,
      },
      {
        key: 'checkbox',
        name: 'Checkbox',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'options', name: 'default_value' },
          {
            title: 'Lựa chọn',
            type: 'options',
            name: 'options',
            default_value: [],
          },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
        ],
        isSupportRepeater: false,
      },
      {
        key: 'radio',
        name: 'Radio button',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          {
            title: 'Lựa chọn',
            type: 'options',
            name: 'options',
            default_value: [],
          },
        ],
        isSupportRepeater: true,
      },
    ],
  },
  {
    name: 'Nâng cao',
    items: [
      {
        key: 'label',
        name: 'Label',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Mô tả', type: 'textarea', name: 'description' },
          {
            title: 'Căn chỉnh',
            type: 'select',
            name: 'align',
            options: [
              { value: 'Căn trái', key: 'left' },
              { value: 'Căn phải', key: 'right' },
              { value: 'Căn giữa', key: 'center' },
            ],
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'repeater',
        name: 'Form list',
        hasCondition: false,
        conditions: [],
        metaFields: [],
      },
      {
        key: 'image-picker',
        name: 'Image picker',
        hasCondition: false,
        conditions: [],
        metaFields: [
          {
            title: 'Hình thức',
            type: 'select',
            name: 'type',
            options: [
              { value: 'Trực tiếp', key: 'direct' },
              { value: 'Chọn ảnh trong thư viện', key: 'lib' },
            ],
          },
        ],
      },
      {
        key: 'date-time',
        name: 'Date time picker',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Hiển thị chọn giờ', type: 'switch', name: 'show_time' },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'time-picker',
        name: 'Time picker',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'color-picker',
        name: 'Color picker',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          { title: 'Gợi ý nhập liệu', type: 'text', name: 'placeholder' },
        ],
      },
    ],
  },
  {
    name: 'Dữ liệu ẩn',
    items: [
      {
        key: 'hidden',
        name: 'Hidden input',
        hasCondition: false,
        conditions: [],
        metaFields: [{ title: 'Giá trị mặc định', type: 'text', name: 'default_value' }],
      },
      {
        key: 'url-variable',
        name: 'URL variable',
        hasCondition: false,
        conditions: [],
        metaFields: [
          {
            title: 'Giá trị mặc định',
            type: 'text',
            name: 'default_value',
            placeholder: 'Ví dụ: /benhnhan/{{value}}/khambenh/',
          },
        ],
      },
      {
        key: 'short-unique-id',
        name: 'Short ID',
        hasCondition: false,
        conditions: [],
        metaFields: [],
      },
      {
        key: 'long-unique-id',
        name: 'Long ID',
        hasCondition: false,
        conditions: [],
        metaFields: [],
      },
    ],
  },
  {
    name: 'Dữ liệu mô đun',
    items: [
      {
        key: 'module-url-params',
        name: 'Url params',
        hasCondition: false,
        conditions: [],
        metaFields: [
          {
            title: 'Thuộc tính',
            type: 'select',
            name: 'params',
            options: [
              { value: 'Mô đun cha', key: 'module_parent' },
              { value: 'ID mô đun cha', key: 'module_parent_data_id' },
              { value: 'Mô đun hiện tại', key: 'module_id' },
              { value: 'ID Mô đun hiện tại', key: 'id' },
            ],
          },
        ],
      },
    ],
  },
  {
    name: 'Dữ liệu liên kết',
    items: [
      {
        key: 'user-data',
        name: 'Thành viên',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Cho phép chọn nhiều không?', type: 'switch', name: 'multiple', default_value: false },
          {
            title: 'Lọc theo nhóm',
            type: 'userCates',
            name: 'categories',
            default_value: [],
            placeholder: 'Tất cả các nhóm',
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'form-data',
        name: 'Biểu mẫu',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Cho phép chọn nhiều không?', type: 'switch', name: 'multiple', default_value: false },
          {
            title: 'Lọc theo nhóm',
            type: 'formCates',
            name: 'categories',
            default_value: [],
            placeholder: 'Tất cả các nhóm',
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'module-data',
        name: 'Mô đun',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Cho phép chọn nhiều không?', type: 'switch', name: 'multiple', default_value: false },
          {
            title: 'Lọc theo mô đun',
            type: 'module',
            name: 'module',
            default_value: [],
            placeholder: 'Tất cả các mô đun',
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
      },
      {
        key: 'category-data',
        name: 'Danh mục',
        hasCondition: false,
        conditions: [],
        metaFields: [
          { title: 'Giá trị mặc định', type: 'text', name: 'default_value' },
          {
            title: 'Chọn danh mục',
            type: 'category',
            name: 'category',
            default_value: [],
            placeholder: 'Chọn nhóm danh mục',
            key_column: 'key_column',
            value_column: 'value_column',
          },
          { title: 'Tùy chỉnh độ rộng thẻ', type: 'text', name: 'input_width', placeholder: 'Ví dụ: 100%, 100px' },
        ],
        isSupportRepeater: true,
      },
    ],
  },
];
