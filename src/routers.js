import React from 'react';
import { Redirect } from 'react-router-dom';
import { getLangText } from './helpers/language.helper';
import { shouldHaveAccessPermission, getUserInformation } from './helpers/common.helper';

import {
  FormOutlined,
  HomeOutlined,
  TeamOutlined,
  SoundOutlined,
  FolderViewOutlined,
  CalendarOutlined,
  NotificationOutlined,
  ContainerOutlined,
  DatabaseOutlined,
  OrderedListOutlined,
  FolderOutlined,
  SettingOutlined,
  SnippetsOutlined,
  MenuOutlined,
  SolutionOutlined,
  AppstoreOutlined,
  FileSearchOutlined,
} from '@ant-design/icons';

// IMPORT PAGE MODULE
const Notification = React.lazy(() => import('containers/notification/Notification'));
const NotificationCategory = React.lazy(() => import('containers/notification/NotificationCategory'));

// auth module
const Login = React.lazy(() => import('containers/auth/Login'));
const Register = React.lazy(() => import('containers/auth/Register'));
//const Registed = React.lazy(() => import('containers/auth/Registed'));
const Verify = React.lazy(() => import('containers/auth/Verify'));
const ForgotPassword = React.lazy(() => import('containers/auth/ForgotPassword'));

// user module
const User = React.lazy(() => import('containers/user/User'));
const UserForm = React.lazy(() => import('containers/user/UserForm'));
const UserDetail = React.lazy(() => import('containers/user/UserDetail'));
const UserCategory = React.lazy(() => import('containers/user/UserCategory'));

//post module
const Post = React.lazy(() => import('containers/post/Post'));
const PostForm = React.lazy(() => import('containers/post/PostForm'));
const PostCategory = React.lazy(() => import('containers/post/PostCategory'));
//cook module
const Cook = React.lazy(() => import('containers/cook/Cook'));
const CookForm = React.lazy(() => import('containers/cook/CookForm'));
const CookCategory = React.lazy(() => import('containers/cook/CookCategory'));
// remind module
const Remind = React.lazy(() => import('containers/reminds/Remind'));
const RemindCategory = React.lazy(() => import('containers/reminds/RemindCategory'));

// appoinetment module
const Appointment = React.lazy(() => import('containers/appointments/Appointment'));

// form builder module
const FormList = React.lazy(() => import('containers/form/FormList'));
const FormBuilder = React.lazy(() => import('containers/form/FormBuilder'));
const FormBuilderCategory = React.lazy(() => import('containers/form/FormBuilderCategory'));

// config module
const Config = React.lazy(() => import('containers/config/Config'));
const ConfigUser = React.lazy(() => import('containers/config/ConfigUser'));
const ConfigEmail = React.lazy(() => import('containers/config/ConfigEmail'));
const ConfigModules = React.lazy(() => import('containers/config/ConfigModules'));
const ConfigDeduceModules = React.lazy(() => import('containers/config/ConfigDeduceModules'));
const ConfigCategoryModules = React.lazy(() => import('containers/config/ConfigCategoryModules'));
const ConfigCronjob = React.lazy(() => import('containers/config/ConfigCronjob'));
const ConfigMobile = React.lazy(() => import('containers/config/ConfigMobile'));
const ConfigClient = React.lazy(() => import('containers/config/ConfigClient'));
// Table and Column module
const Column = React.lazy(() => import('containers/table/Column'));
const Table = React.lazy(() => import('containers/table/Table'));

// modules
const Module = React.lazy(() => import('containers/module/Module'));
const ModuleForm = React.lazy(() => import('containers/module/ModuleForm'));

// deduce modules
const Deduction = React.lazy(() => import('containers/deduction/Deduction'));
const DeductionForm = React.lazy(() => import('containers/deduction/DeductionForm'));

// category modules
const Category = React.lazy(() => import('containers/category/Category'));

// rule module
const RuleEditor = React.lazy(() => import('containers/rule/RuleEditor'));

// exam module
const Exam = React.lazy(() => import('containers/exam/Exam'));
const ExamForm = React.lazy(() => import('containers/exam/ExamForm/ExamForm'));
const ExamCategory = React.lazy(() => import('containers/exam/ExamCategory'));
const ExamTopic = React.lazy(() => import('containers/exam/ExamTopic'));
const ExamHistory = React.lazy(() => import('containers/exam/ExamHistory'));

// question module
const Question = React.lazy(() => import('containers/question/Question'));
const QuestionForm = React.lazy(() => import('containers/question/QuestionForm'));
const QuestionCategory = React.lazy(() => import('containers/question/QuestionCategory'));

// course module
const Course = React.lazy(() => import('containers/course/Course'));
const CourseForm = React.lazy(() => import('containers/course/CourseForm/CourseForm'));
const CourseCategory = React.lazy(() => import('containers/course/CourseCategory'));

// lesson module
// const Lesson = React.lazy(() => import('containers/lesson/Lesson'));
// const LessonForm = React.lazy(() => import('containers/lesson/LessonForm'));
// const LessonCategory = React.lazy(() => import('containers/lesson/LessonCategory'));

// other module
const Home = React.lazy(() => import('containers/home/Home'));
const MediaLibrary = React.lazy(() => import('containers/common/MediaLibrary'));
const NotFound = React.lazy(() => import('containers/common/NotFound'));
const Protected = React.lazy(() => import('containers/common/Protected'));
const Installer = React.lazy(() => import('containers/installer/Installer'));

const userInfo = getUserInformation();

export const ROUTES = [
  {
    id: 'home',
    path: '/',
    name: getLangText('MENU.HOME'),
    icon: <HomeOutlined />,
    exact: true,
    hidden: !shouldHaveAccessPermission('trang_chu', 'trang_chu/truy_cap'),
    render: (props) => (shouldHaveAccessPermission('trang_chu', 'trang_chu/truy_cap') ? <Home {...props} /> : <Redirect to="/protected" />),
  },

  {
    id: 'module',
    path: '/module/:module_id',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <Module {...props} />,
  },
  {
    id: 'module_form_add',
    path: '/module/:module_id/:module_action',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <ModuleForm {...props} />,
  },
  {
    id: 'module_form_edit',
    path: '/module/:module_id/:module_action/:id',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <ModuleForm {...props} />,
  },

  {
    id: 'sub_module_list',
    path: '/module/sub/:module_parent/:module_parent_data_id/:module_id',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <Module {...props} />,
  },
  {
    id: 'sub_module_form_add',
    path: '/module/sub/:module_parent/:module_parent_data_id/:module_id/:module_action',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <ModuleForm {...props} />,
  },
  {
    id: 'sub_module_form_edit',
    path: '/module/sub/:module_parent/:module_parent_data_id/:module_id/:module_action/:id',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <ModuleForm {...props} />,
  },
  {
    id: 'grant_module_form_add',
    path: '/module/sub/:module_grant/:module_grant_data_id/:module_parent/:module_parent_data_id/:module_id/:module_action',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <ModuleForm {...props} />,
  },
  {
    id: 'grant_module_form_edit',
    path: '/module/sub/:module_grant/:module_grant_data_id/:module_parent/:module_parent_data_id/:module_id/:module_action/:id',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <ModuleForm {...props} />,
  },
  {
    id: 'category',
    path: '/category/:module_id',
    name: 'category',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <Category {...props} />,
  },
  {
    id: 'deduction',
    path: '/deduction/:module_id/list',
    name: 'deduction',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <Deduction {...props} />,
  },
  {
    id: 'deduction_form',
    path: '/deduction/:module_id/detail/:id',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <DeductionForm {...props} />,
  },
  {
    id: 'deduction_form',
    path: '/deduction/:module_id/detail',
    name: 'Module',
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: true,
    render: (props) => <DeductionForm {...props} />,
  },
  // RENDER PAGE MODULE
  {
    id: 'media',
    path: '/media',
    name: getLangText('MENU.MEDIA_LIBRARY'),
    icon: <FolderViewOutlined />,
    exact: true,
    hidden: !shouldHaveAccessPermission('thu_vien', 'thu_vien/xem'),
    render: (props) => (shouldHaveAccessPermission('thu_vien', 'thu_vien/xem') ? <MediaLibrary {...props} /> : <Redirect to="/protected" />),
  },
  {
    id: 'users',
    hidden: !shouldHaveAccessPermission('nhan_vien'),
    name: getLangText('MENU.USERS'),
    icon: <TeamOutlined />,
    sub: [
      {
        path: '/users',
        name: 'Tất cả',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'users',
        hidden: !(shouldHaveAccessPermission('nhan_vien', 'nhan_vien/xem') || shouldHaveAccessPermission('nhan_vien', 'nhan_vien/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhan_vien', 'nhan_vien/xem') || shouldHaveAccessPermission('nhan_vien', 'nhan_vien/xem_tat_ca') ? <User {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/user/add',
        name: getLangText('MENU.ADD_NEW_USER'),
        exact: true,
        parent: 'users',
        hidden: !shouldHaveAccessPermission('nhan_vien', 'nhan_vien/them'),
        render: (props) => (shouldHaveAccessPermission('nhan_vien', 'nhan_vien/them') ? <UserForm {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/user/edit/:id',
        hidden: true,
        name: getLangText('MENU.EDIT_USER'),
        exact: true,
        parent: 'user',
        render: (props) => <UserForm {...props} />,
        /*  shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') ? (
            <UserForm {...props} />
          ) : (
            <Redirect to="/protected" />
          ), */
      },
      {
        path: '/user/detail/:id',
        hidden: true,
        name: getLangText('MENU.DETAIL_USER'),
        exact: true,
        parent: 'user',
        render: (props) => <UserDetail {...props} />,
        /*   shouldHaveAccessPermission('nhan_vien', 'nhan_vien/chi_tiet') ? (
            <UserDetail {...props} />
          ) : (
            <Redirect to="/protected" />
          ), */
      },
      {
        path: '/user_category',
        name: getLangText('MENU.USER_CATEGORIES'),
        exact: true,
        icon: <FolderOutlined />,
        parent: 'users',
        hidden: !(shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/xem') || shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/xem') || shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/xem_tat_ca') ? (
            <UserCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
    ],
  },
  {
    id: 'courses',
    hidden: !shouldHaveAccessPermission('khoa_hoc'),
    name: 'Khóa học',
    icon: <AppstoreOutlined />,
    sub: [
      {
        path: '/courses',
        name: 'Khóa học',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'courses',
        hidden: !(shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/xem') || shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/xem') || shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/xem_tat_ca') ? <Course {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/course/add',
        name: 'Thêm mới',
        exact: true,
        parent: 'courses',
        hidden: true,
        render: (props) => (shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/them') ? <CourseForm {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/course/edit/:id',
        hidden: true,
        name: 'Sửa khóa học',
        exact: true,
        parent: 'courses',
        render: (props) => <CourseForm {...props} />,
      },
      {
        path: '/course_category',
        name: 'Danh mục',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'courses',
        hidden: !(shouldHaveAccessPermission('nhom_khoa_hoc', 'nhom_khoa_hoc/xem') || shouldHaveAccessPermission('nhom_khoa_hoc', 'nhom_khoa_hoc/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_khoa_hoc', 'nhom_khoa_hoc/xem') || shouldHaveAccessPermission('nhom_khoa_hoc', 'nhom_khoa_hoc/xem_tat_ca') ? (
            <CourseCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
      // {
      //   path: '/lessons',
      //   name: 'Bài học',
      //   exact: true,
      //   icon: <FolderOutlined />,
      //   parent: 'courses',
      //   hidden: true,
      //   // hidden: !(shouldHaveAccessPermission('bai_hoc', 'bai_hoc/xem') || shouldHaveAccessPermission('bai_hoc', 'bai_hoc/xem_tat_ca')),
      //   render: (props) =>
      //     shouldHaveAccessPermission('bai_hoc', 'bai_hoc/xem') || shouldHaveAccessPermission('bai_hoc', 'bai_hoc/xem_tat_ca') ? <Question {...props} /> : <Redirect to="/protected" />,
      // },
      // {
      //   path: '/question/add',
      //   name: 'Thêm mới',
      //   exact: true,
      //   parent: 'courses',
      //   hidden: true,
      //   render: (props) => (shouldHaveAccessPermission('bai_hoc', 'bai_hoc/them') ? <QuestionForm {...props} /> : <Redirect to="/protected" />),
      // },
      // {
      //   path: '/question/add/:examId',
      //   name: 'Thêm mới',
      //   exact: true,
      //   parent: 'courses',
      //   hidden: true,
      //   render: (props) => (shouldHaveAccessPermission('bai_hoc', 'bai_hoc/them') ? <QuestionForm {...props} /> : <Redirect to="/protected" />),
      // },
      // {
      //   path: '/question/edit/:id',
      //   hidden: true,
      //   name: 'Sửa câu hỏi',
      //   exact: true,
      //   parent: 'courses',
      //   render: (props) => <LessonForm {...props} />,
      // },
      // {
      //   path: '/question_category',
      //   name: 'Danh mục',
      //   exact: true,
      //   icon: <FolderOutlined />,
      //   parent: 'courses',
      //   hidden: true,
      //   render: (props) =>
      //     shouldHaveAccessPermission('nhom_bai_hoc', 'nhom_bai_hoc/xem') || shouldHaveAccessPermission('nhom_bai_hoc', 'nhom_bai_hoc/xem_tat_ca') ? (
      //       <LessonCategory {...props} />
      //     ) : (
      //       <Redirect to="/protected" />
      //     ),
      // },
    ],
  },
  {
    id: 'exams',
    hidden: !shouldHaveAccessPermission('de_thi'),
    name: 'Trắc nghiệm',
    icon: <FileSearchOutlined />,
    sub: [
      {
        path: '/exams',
        name: 'Đề thi',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'exams',
        hidden: !(shouldHaveAccessPermission('de_thi', 'de_thi/xem') || shouldHaveAccessPermission('de_thi', 'de_thi/xem_tat_ca')),
        render: (props) => (shouldHaveAccessPermission('de_thi', 'de_thi/xem') || shouldHaveAccessPermission('de_thi', 'de_thi/xem_tat_ca') ? <Exam {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/exam/add',
        name: 'Thêm mới',
        exact: true,
        parent: 'exams',
        hidden: true,
        render: (props) => (shouldHaveAccessPermission('de_thi', 'de_thi/them') ? <ExamForm {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/exam/edit/:id',
        hidden: true,
        name: 'Sửa đề thi',
        exact: true,
        parent: 'exams',
        render: (props) => <ExamForm {...props} />,
      },
      {
        path: '/exam_category',
        name: 'Danh mục',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'exams',
        hidden: !(shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem') || shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem') || shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem_tat_ca') ? (
            <ExamCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
      {
        path: '/exam_topic',
        name: 'Chủ đề',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'exams',
        hidden: true,
        // hidden: !(shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem') || shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem') || shouldHaveAccessPermission('nhom_de_thi', 'nhom_de_thi/xem_tat_ca') ? <ExamTopic {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/exam_history',
        name: 'Lịch sử thi',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'exams',
        hidden: true,
        // hidden: !(shouldHaveAccessPermission('lich_su_thi', 'nhom_de_thi/xem') || shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/xem') || shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/xem_tat_ca') ? (
            <ExamHistory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
      {
        path: '/questions',
        name: 'Câu hỏi',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'exams',
        hidden: true,
        // hidden: !(shouldHaveAccessPermission('cau_hoi', 'cau_hoi/xem') || shouldHaveAccessPermission('cau_hoi', 'cau_hoi/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('cau_hoi', 'cau_hoi/xem') || shouldHaveAccessPermission('cau_hoi', 'cau_hoi/xem_tat_ca') ? <Question {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/question/add',
        name: 'Thêm mới',
        exact: true,
        parent: 'exams',
        hidden: true,
        render: (props) => (shouldHaveAccessPermission('cau_hoi', 'cau_hoi/them') ? <QuestionForm {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/question/add/:examId',
        name: 'Thêm mới',
        exact: true,
        parent: 'exams',
        hidden: true,
        render: (props) => (shouldHaveAccessPermission('cau_hoi', 'cau_hoi/them') ? <QuestionForm {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/question/edit/:id',
        hidden: true,
        name: 'Sửa câu hỏi',
        exact: true,
        parent: 'exams',
        render: (props) => <QuestionForm {...props} />,
      },
      {
        path: '/question_category',
        name: 'Danh mục',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'exams',
        hidden: true,
        render: (props) =>
          shouldHaveAccessPermission('nhom_cau_hoi', 'nhom_cau_hoi/xem') || shouldHaveAccessPermission('nhom_cau_hoi', 'nhom_cau_hoi/xem_tat_ca') ? (
            <QuestionCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
    ],
  },
  {
    id: 'posts',
    hidden: !shouldHaveAccessPermission('tin_tuc'),
    name: 'Bài viết',
    icon: <SnippetsOutlined />,
    sub: [
      {
        path: '/posts',
        name: 'Danh sách',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'posts',
        hidden: !(shouldHaveAccessPermission('tin_tuc', 'tin_tuc/xem') || shouldHaveAccessPermission('tin_tuc', 'tin_tuc/xem_tat_ca')),
        render: (props) => (shouldHaveAccessPermission('tin_tuc', 'tin_tuc/xem') || shouldHaveAccessPermission('tin_tuc', 'tin_tuc/xem_tat_ca') ? <Post {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/post/add',
        name: 'Thêm mới',
        exact: true,
        parent: 'posts',
        hidden: !shouldHaveAccessPermission('tin_tuc', 'tin_tuc/them'),
        render: (props) => (shouldHaveAccessPermission('tin_tuc', 'tin_tuc/them') ? <PostForm {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/post/edit/:id',
        hidden: true,
        name: 'Sửa bài viết',
        exact: true,
        parent: 'post',
        render: (props) => <PostForm {...props} />,
      },
      {
        path: '/post_category',
        name: 'Danh mục',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'posts',
        hidden: !(shouldHaveAccessPermission('nhom_tin_tuc', 'nhom_tin_tuc/xem') || shouldHaveAccessPermission('nhom_tin_tuc', 'nhom_tin_tuc/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_tin_tuc', 'nhom_tin_tuc/xem') || shouldHaveAccessPermission('nhom_tin_tuc', 'nhom_tin_tuc/xem_tat_ca') ? (
            <PostCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
    ],
  },
  {
    id: 'cooks',
    hidden: !shouldHaveAccessPermission('thuc_don'),
    name: 'Thực đơn',
    icon: <SolutionOutlined />,
    sub: [
      {
        path: '/cooks',
        name: 'Danh sách',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'cooks',
        hidden: !(shouldHaveAccessPermission('thuc_don', 'thuc_don/xem') || shouldHaveAccessPermission('thuc_don', 'thuc_don/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('thuc_don', 'thuc_don/xem') || shouldHaveAccessPermission('thuc_don', 'thuc_don/xem_tat_ca') ? <Cook {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/cook/add',
        name: 'Thêm mới',
        exact: true,
        parent: 'cooks',
        hidden: !shouldHaveAccessPermission('thuc_don', 'thuc_don/them'),
        render: (props) => (shouldHaveAccessPermission('thuc_don', 'thuc_don/them') ? <CookForm {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/cook/edit/:id',
        hidden: true,
        name: 'Sửa thực đơn',
        exact: true,
        parent: 'cook',
        render: (props) => <CookForm {...props} />,
      },
      {
        path: '/cook_category',
        name: 'Danh mục',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'cooks',
        hidden: !(shouldHaveAccessPermission('nhom_thuc_don', 'nhom_thuc_don/xem') || shouldHaveAccessPermission('nhom_thuc_don', 'nhom_thuc_don/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_thuc_don', 'nhom_thuc_don/xem') || shouldHaveAccessPermission('nhom_thuc_don', 'nhom_thuc_don/xem_tat_ca') ? (
            <CookCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
    ],
  },
  {
    id: 'appointment',
    path: '/appointments',
    name: 'Lịch hẹn',
    icon: <CalendarOutlined />,
    exact: true,
    hidden: !shouldHaveAccessPermission('lich_hen', 'lich_hen/xem'),
    render: (props) => (shouldHaveAccessPermission('lich_hen', 'lich_hen/xem') ? <Appointment {...props} /> : <Redirect to="/protected" />),
  },

  {
    id: 'reminds',
    hidden: !shouldHaveAccessPermission('nhac_nho'),
    name: 'Nhắc nhở',
    icon: <SoundOutlined />,
    sub: [
      {
        path: '/reminds',
        name: 'Nhắc nhở',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'reminds',
        hidden: !(shouldHaveAccessPermission('nhac_nho', 'nhac_nho/xem') || shouldHaveAccessPermission('nhac_nho', 'nhac_nho/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhac_nho', 'nhac_nho/xem') || shouldHaveAccessPermission('nhac_nho', 'nhac_nho/xem_tat_ca') ? <Remind {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/remind_category',
        name: 'Danh mục',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'users',
        hidden: !(shouldHaveAccessPermission('nhom_nhac_nho', 'nhom_nhac_nho/xem') || shouldHaveAccessPermission('nhom_nhac_nho', 'nhom_nhac_nho/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_nhac_nho', 'nhom_nhac_nho/xem') || shouldHaveAccessPermission('nhom_nhac_nho', 'nhom_nhac_nho/xem_tat_ca') ? (
            <RemindCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
    ],
  },
  {
    id: 'notifications',
    hidden: !shouldHaveAccessPermission('thong_bao'),
    name: 'Thông báo',
    icon: <NotificationOutlined />,
    sub: [
      {
        path: '/notifications',
        name: 'Thông báo',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'notifications',
        hidden: !(shouldHaveAccessPermission('thong_bao', 'thong_bao/xem') || shouldHaveAccessPermission('thong_bao', 'thong_bao/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('thong_bao', 'thong_bao/xem') || shouldHaveAccessPermission('thong_bao', 'thong_bao/xem_tat_ca') ? <Notification {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/notifications/view',
        name: 'Thông báo',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'notifications',
        hidden: true,
        render: (props) =>
          shouldHaveAccessPermission('thong_bao', 'thong_bao/xem') || shouldHaveAccessPermission('thong_bao', 'thong_bao/xem_tat_ca') ? <Notification {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/notification_category',
        name: 'Danh mục',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'notifications',
        hidden: !(shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/xem') || shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/xem') || shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/xem_tat_ca') ? (
            <NotificationCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
    ],
  },
  {
    id: 'forms',
    // hidden: !shouldHaveAccessPermission('bieu_mau'),
    hidden: true,
    name: getLangText('MENU.FORMS'),
    icon: <FormOutlined />,
    sub: [
      {
        path: '/forms',
        name: getLangText('MENU.ALL_FORMS'),
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'forms',
        hidden: !(shouldHaveAccessPermission('bieu_mau', 'bieu_mau/xem') || shouldHaveAccessPermission('bieu_mau', 'bieu_mau/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('bieu_mau', 'bieu_mau/xem') || shouldHaveAccessPermission('bieu_mau', 'bieu_mau/xem_tat_ca') ? <FormList {...props} /> : <Redirect to="/protected" />,
      },
      {
        path: '/form/add',
        name: getLangText('MENU.ADD_NEW_FORM'),
        exact: true,
        parent: 'forms',
        hidden: !shouldHaveAccessPermission('bieu_mau', 'bieu_mau/them'),
        render: (props) => (shouldHaveAccessPermission('bieu_mau', 'bieu_mau/them') ? <FormBuilder {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/form/edit/:id',
        hidden: true,
        name: getLangText('MENU.EDIT_FORM'),
        exact: true,
        parent: 'forms',
        render: (props) => (shouldHaveAccessPermission('bieu_mau', 'bieu_mau/sua') ? <FormBuilder {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/form_category',
        name: getLangText('MENU.FORM_CATEGORIES'),
        exact: true,
        icon: <FolderOutlined />,
        parent: 'forms',
        hidden: !(shouldHaveAccessPermission('nhom_bieu_mau', 'nhom_bieu_mau/xem') || shouldHaveAccessPermission('nhom_bieu_mau', 'nhom_bieu_mau/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('nhom_bieu_mau', 'nhom_bieu_mau/xem') || shouldHaveAccessPermission('nhom_bieu_mau', 'nhom_bieu_mau/xem') ? (
            <FormBuilderCategory {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
    ],
  },
  {
    id: 'columns',
    hidden: !shouldHaveAccessPermission('truong_du_lieu'),
    name: 'Bảng dữ liệu',
    icon: <DatabaseOutlined />,
    sub: [
      {
        path: '/tables',
        name: 'Danh sách',
        exact: true,
        icon: <FolderOutlined />,
        parent: 'dbcolumns',
        hidden: !(shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xem') || shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xem') || shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xem_tat_ca') ? (
            <Table {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
      {
        path: '/columns',
        name: 'Trường dữ liệu',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'columns',
        hidden: !(shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/xem') || shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/xem_tat_ca')),
        render: (props) =>
          shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/xem') || shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/xem_tat_ca') ? (
            <Column {...props} />
          ) : (
            <Redirect to="/protected" />
          ),
      },
      {
        path: '/columns/select-columns/:bangid',
        name: 'Chọn trường dữ liệu',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'columns',
        hidden: true,
        render: (props) => (shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/add_column') ? <Column {...props} /> : <Redirect to="/protected" />),
      },
    ],
  },
  {
    id: 'rule-editor',
    path: '/rule-editor',
    name: 'Quản lý tập luật',
    icon: <ContainerOutlined />,
    exact: true,
    hidden: !shouldHaveAccessPermission('tap_luat', 'tap_luat/xem'),
    render: (props) => (shouldHaveAccessPermission('tap_luat', 'tap_luat/xem') ? <RuleEditor {...props} /> : <Redirect to="/protected" />),
  },
  {
    id: 'config',
    path: '/config',
    name: getLangText('MENU.CONFIG'),
    icon: <SettingOutlined />,
    exact: true,
    hidden: !shouldHaveAccessPermission('cau_hinh'),
    sub: [
      {
        path: '/config/common',
        name: 'Cấu hình chung',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/chung'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/chung') ? <Config {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/configClient',
        name: 'Trang khách',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/khach'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/khach') ? <ConfigClient {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/configMobile',
        name: 'Ứng dụng ĐT',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/mobile'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/mobile') ? <ConfigMobile {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/user',
        name: 'Thành viên',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/user'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/user') ? <ConfigUser {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/email',
        name: 'E-mail',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/email'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/email') ? <ConfigEmail {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/modules',
        name: 'Mô đun',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/module'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/module') ? <ConfigModules {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/deduce-modules',
        name: 'MD suy diễn',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/deduce-module'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/deduce-module') ? <ConfigDeduceModules {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/category-modules',
        name: 'MD danh mục',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh', 'cau_hinh/category-module'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh', 'cau_hinh/category-module') ? <ConfigCategoryModules {...props} /> : <Redirect to="/protected" />),
      },
      {
        path: '/config/configCronjob',
        name: 'Quản lý CronJob',
        exact: true,
        icon: <OrderedListOutlined />,
        parent: 'config',
        hidden: !shouldHaveAccessPermission('cau_hinh'),
        render: (props) => (shouldHaveAccessPermission('cau_hinh') ? <ConfigCronjob {...props} /> : <Redirect to="/protected" />),
      },
    ],
  },
  {
    id: 'login',
    path: '/auth/login',
    exact: true,
    hidden: true,
    render: (props) => <Login {...props} />,
  },
  {
    id: 'register',
    path: '/auth/register',
    exact: true,
    hidden: true,
    render: (props) => <Register {...props} />,
  },
  {
    id: 'verify',
    path: '/auth/verify',
    exact: true,
    hidden: true,
    render: (props) => <Verify {...props} />,
  },
  {
    id: 'forgot-password',
    path: '/auth/forgot-password',
    exact: true,
    hidden: true,
    render: (props) => <ForgotPassword {...props} />,
  },
  {
    id: 'installer',
    path: '/installer',
    exact: true,
    hidden: true,
    render: (props) => (userInfo ? <Redirect to="/protected" /> : <Installer {...props} />),
  },
  {
    id: 'protected',
    path: '/protected',
    exact: true,
    hidden: true,
    render: (props) => <Protected {...props} />,
  },
  {
    id: 'any',
    path: '*',
    exact: true,
    hidden: true,
    render: (props) => <NotFound {...props} />,
  },
];
