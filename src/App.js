// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Layout, BackTop, Modal, notification } from 'antd';
import { useHistory } from 'react-router-dom';
import { get } from 'lodash';
import { io as socketIOClient } from 'socket.io-client';
import Helmet from 'react-helmet';
// import internal libs
import 'assets/style/style.css';
import 'assets/style/responsive.css';

import { setCurrentLanguage } from 'helpers/language.helper';
import { getLangText } from 'helpers/language.helper';

import AppSider from 'components/AppSider';
import AppHeader from 'components/AppHeader';

import constants from 'constants/global.constants';
import { config } from 'config';

import * as configAction from 'redux/actions/configure';
import * as categoryAction from 'redux/actions/category';
import * as userAction from 'redux/actions/user';
import * as formAction from 'redux/actions/form';
import * as notificationAction from 'redux/actions/notification';
import * as statisticAction from 'redux/actions/statistic';
import Loading from 'components/loading/Loading';
import { getImagePath } from 'helpers/common.helper';
import { getFileMinio } from 'redux/services/api';

const defaultLogo = require('assets/images/logo.png');

const { Content } = Layout;
const { confirm } = Modal;

function App(props) {
  const [images, setImages] = useState({});
  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  const { innerWidth: width } = window;
  const cf_pros = get(props, 'config.list.result.data', []);
  const cm_config = cf_pros.find((item) => item.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
  const theme_type = cm_config ? constants.THEME_TYPES.find((item) => item.value === JSON.parse(cm_config.du_lieu).site_theme) : {};

  const favicon_img = cm_config && cm_config.du_lieu ? `${config.UPLOAD_API_URL}/${JSON.parse(cm_config.du_lieu).site_favicon}` : defaultLogo;

  const [state, setState] = useState({
    collapsed: width < 768,
    configData: {},
  });

  const history = useHistory();

  const toggle = () => setState((state) => ({ ...state, collapsed: !state.collapsed }));

  const logout = () => {
    confirm({
      title: 'Bạn chắc chắn muốn đăng xuất không?',
      content: '',
      cancelText: 'Không',
      okText: 'Có',
      onOk() {
        notification.success({
          message: getLangText('MESSAGE.USER_LOGOUT_SUCCESS'),
        });
        history.push('/auth/login');

        props.dispatch(userAction.logoutUser({}));
      },
    });
  };

  useEffect(() => {
    const token = localStorage.getItem('userToken');
    if (!token) {
      history.push('/auth/login');
    }
    if (props.config.language) {
      setCurrentLanguage(props.config.language, props.dispatch);
    }
  }, [props]);

  useEffect(() => {
    const configs = get(props, 'config.list.result', []);
    if (configs.data.length > 0 && configs.success) {
      localStorage.setItem('configInfo', JSON.stringify(configs.data));
    }
  }, [props.config]);

  useEffect(() => {
    const forms = get(props, 'form.list.result', {});
    if (forms && forms.data && forms.data.length > 0 && forms.success) {
      localStorage.setItem('formInfo', JSON.stringify(forms.data));
    }
  }, [props.form]);

  useEffect(() => {
    const categories = get(props, 'category.categoryData.result', {});
    if (categories && categories.data && categories.success) {
      localStorage.setItem('categoryInfo', JSON.stringify(categories.data));
    }
  }, [props.category.categoryData]);

  useEffect(() => {
    const configs = get(props, 'config.list.result.data', []);
    const forms = get(props, 'form.list.result.data', []);
    const categories = get(props, 'category.categoryData.result.data', {});
    const token = localStorage.getItem('userToken');
    if (forms.length <= 0 && token) {
      props.dispatch(formAction.getForms());
    }
    if (configs.length <= 0) {
      props.dispatch(configAction.getConfigs());
    }
    if (Object.keys(categories).length <= 0) {
      props.dispatch(categoryAction.getAllCategoryData());
    }
    const socket = socketIOClient(config.UPLOAD_API_URL, config.SOCKET_CONFIG);
    socket.on('event://configs-changed', () => {
      props.dispatch(configAction.getConfigs());
    });
    socket.on('event://forms-changed', () => {
      props.dispatch(formAction.getForms());
    });
    socket.on('event://categories-changed', () => {
      props.dispatch(categoryAction.getAllCategoryData());
    });

    return () => {
      socket.disconnect();
    };
  }, []);

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const configData = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    if (configData) {
      const data = JSON.parse(configData.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
      }));
    }
  }, [props]);

  useEffect(() => {
    if (state.configData.site_upload_server === 'minio') {
      const getData = async () => {
        const siteBackground = await getImage(state.configData.site_background, state.configData.site_upload_server, state.configData.site_minio_bucket);
        const siteFavicon = await getImage(state.configData.site_favicon, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, siteFavicon: siteFavicon.data, siteBackground: siteBackground.data });
      };
      getData();
    }
  }, [state.configData]);

  useEffect(() => {
    props.dispatch(notificationAction.getNotifications({ status: 'active' }));
    props.dispatch(statisticAction.getGlobalStatistic());
    props.dispatch(userAction.getUserCates());
    props.dispatch(userAction.getUsers({ loading: false }));
    props.dispatch(notificationAction.getNotificationCates());
  }, []);

  return (
    <Layout className={theme_type ? theme_type.value : 'blue-theme'}>
      <Helmet>
        <link rel="icon" href={getImagePath(state.configData.site_upload_server, state.configData.site_favicon, images.siteFavicon, '')} />
      </Helmet>
      {props.user.logout.loading && <Loading />}
      <AppSider collapsed={state.collapsed} toggle={toggle} />
      <Content className="app-content">
        <AppHeader collapsed={state.collapsed} toggle={toggle} logout={logout} />
        {props.children}
      </Content>
      <BackTop />
    </Layout>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
    form: state.form,
    category: state.category,
  };
};

export default connect(mapStateToProps)(App);
