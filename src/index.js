import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, HashRouter, Switch, BrowserRouter as Router } from 'react-router-dom';
import katex from 'katex';
import 'katex/dist/katex.min.css';
import * as serviceWorker from 'serviceWorker';
import store from 'redux/store';
import { ROUTES } from 'routers';
window.katex = katex;

const Root = () => {
  const myRouters = [];
  ROUTES.forEach((router) => {
    if (router.path && router.render) {
      myRouters.push(router);
    }
    if (router.sub && router.sub.length > 0) {
      router.sub.forEach((subRouter) => {
        if (subRouter.path && subRouter.render) {
          myRouters.push(subRouter);
        }
      });
    }
  });
  return (
    <Provider store={store}>
      <HashRouter hashType="noslash">
        <Suspense fallback={null}>
          <Router>
            <Switch>
              {myRouters.map((route, key) => {
                return <Route key={key} path={route.path} exact={route.exact} render={route.render} />;
              })}
            </Switch>
          </Router>
        </Suspense>
      </HashRouter>
    </Provider>
  );
};

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
