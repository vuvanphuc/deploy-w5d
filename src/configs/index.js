const ENV = 'prod';
const API_URL_DEV = 'https://localhost:5001';
const API_URL_PROD = 'https://exam-dev-api.web5days.com:5001';
const DATE_FORMAT = 'HH:mm DD/MM/YYYY';
const DATE_FORMAT_SHORT = 'DD/MM/YYYY';
const SHOW_DATE_FORMAT = 'D MMMM YYYY, HH:mm';
const LANGUAGES = [
  {
    title: 'English',
    value: 'en',
  },
  {
    title: 'Vietnamese',
    value: 'vi',
  },
];
const AUTH_SOCIAL = {
  FACEBOOK_APP_ID: '226264828440675',
  GOOOGLE_APP_ID: '246536234255-k1gi5d5pvo1ovifgt36jebdaivmhrdff.apps.googleusercontent.com',
};
const config = {
  ENV,
  API_URL: ENV === 'dev' ? API_URL_DEV : API_URL_PROD,
  RULE_SET_APP: ENV === 'dev' ? 'http://localhost:4005' : 'http://localhost:4005',
  UPLOAD_API_URL: 'https://exam-dev-api.web5days.com:5001',
  DATE_FORMAT,
  SHOW_DATE_FORMAT,
  DATE_FORMAT_SHORT,
  LANGUAGES,
  AUTH_SOCIAL,
  SOCKET_CONFIG: { transports: ['websocket', 'polling', 'flashsocket'] },
  PAGE_SIZE: 100,
};

export default config;
