// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory, useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Button, Form, Row, Col, notification, List, Dropdown, Menu, Space, Modal, Checkbox } from 'antd';
import { DeleteOutlined, DownOutlined, QuestionCircleOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons';
import queryString from 'query-string';

// import internal libs
import App from 'App';
import { getLangText } from 'helpers/language.helper';
import Loading from 'components/loading/Loading';
import FormBuilderRender from 'containers/form/FormBuilderRender';
import * as categoryAction from 'redux/actions/category';
import { shouldHaveAccessPermission, buildUrl } from '../../helpers/common.helper';
import { config } from 'config';
import AppFilter from 'components/AppFilter';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import W5dStatusTag from '../../components/status/W5dStatusTag';

import { renderDataFormatted } from 'helpers/module.helper';
const { confirm } = Modal;

function Category(props) {
  const params = useParams();
  const [form] = Form.useForm();

  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const [module, setModule] = useState({});

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  let allOptions = [];

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    search: props.category.list.search,
  });

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
    props.dispatch(categoryAction.getCategories({ ...state.search, [field]: value, module_id: module.module_id }));
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
    props.dispatch(categoryAction.getCategories({ ...state.search, ...fields, module_id: module.module_id }));
  };

  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(id) ? state.checkedList.filter((it) => it !== id) : state.checkedList.concat([id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const editCategory = (item) => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    setState((state) => ({
      ...state,
      isEdit: true,
      category: item,
    }));
    form.setFieldsValue(item);
  };

  const renderHeaderFooterColumns = () => {
    if (module && module.module_fields) {
      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} className="text-left">
            {item.title} {item.isSortable && <Sorting urlQuery={urlQuery} field={item.column} onSortChange={(fields) => onSortChange(fields)} />}
          </Col>
        );
      });
      return (
        <Row>
          <Col xs={1} xl={1} className="text-left">
            <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
          </Col>
          {columns}
          {module.module_show_filter_status && (
            <Col xs={3} xl={3}>
              {getLangText('GLOBAL.STATUS')}
              <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
            </Col>
          )}
        </Row>
      );
    }
  };

  const renderRows = (data) => {
    if (module && module.module_fields) {
      //lấy id của từng bản ghi, đẩy vào allOptions
      allOptions.push(data[module.module_fields.find((field) => field.key === true).column]);

      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} className="text-left">
            {renderDataFormatted(data, item)}
            {item.key && (
              <div className="actions">
                {shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) && (
                  <Button className="edit act" type="link" onClick={() => editCategory(data)}>
                    {getLangText('GLOBAL.EDIT')}
                  </Button>
                )}
                {shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) && (
                  <Button className="delete act" type="link" onClick={() => deleteCategory(data[item.column], data.trang_thai)}>
                    {module.module_show_filter_status && data.trang_thai === 'active' ? getLangText('GLOBAL.BLOCK') : getLangText('GLOBAL.DELETE')}
                  </Button>
                )}
              </div>
            )}
          </Col>
        );
      });
      return (
        <Row className="full">
          <Col xs={1} xl={1} className="text-left">
            <Checkbox
              checked={state.checkedList.includes(data[module.module_fields.find((field) => field.key === true).column])}
              value={data[module.module_fields.find((field) => field.key === true).column]}
              onChange={() => onChangeCheck(data[module.module_fields.find((field) => field.key === true).column])}
            />
          </Col>
          {columns}
          {module.module_show_filter_status && (
            <Col xs={3} xl={3}>
              <W5dStatusTag status={data.trang_thai} />
            </Col>
          )}
        </Row>
      );
    }
  };

  const searchData = (module_id) => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.category.list.search,
      ...urlQuery,
      module_id,
    };
    delete searchParams.page;
    props.dispatch(categoryAction.getCategories(searchParams));
  };

  const handleSubmit = (values) => {
    let formatValues = {};
    for (const [key, value] of Object.entries(values)) {
      const newVal = Array.isArray(value) ? JSON.stringify(value) : value;
      formatValues = { ...formatValues, [key]: newVal };
    }
    if (state.isEdit === true) {
      // edit a data
      const callback = (res) => {
        if (res.success) {
          searchData(module.module_id);
          changeUrlParams('edit', '');
          notification.success({
            message: 'Cập nhật dữ liệu thành công.',
          });
          setState((state) => ({
            ...state,
            isEdit: false,
          }));
        }
      };
      const newFormData = {
        data: {
          ...formatValues,
          urlParams: params,
        },
        id: formatValues[module.module_table_key],
        module,
      };
      props.dispatch(categoryAction.editCategory(newFormData, callback));
    } else {
      //add a new data
      const callback = (res) => {
        if (res.success) {
          searchData(module.module_id);
          changeUrlParams('add', '');
          notification.success({
            message: 'Thêm mới dữ liệu thành công',
          });
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
        }
      };
      const newFormData = {
        data: {
          ...formatValues,
          urlParams: params,
        },
        module,
      };
      props.dispatch(categoryAction.createCategory(newFormData, callback));
    }
    form.resetFields();
  };

  const onResetForm = () => {
    setState((state) => ({
      ...state,
      isEdit: false,
    }));
    form.resetFields();
  };

  const renderForm = () => {
    if (!module.module_id) return null;
    return (
      <FormBuilderRender formId={module.module_form} table={module.module_table} formConfig={module.module_table ? props.form.list[module.module_table] : {}} formObj={form} isEdit={state.isEdit} />
    );
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_CATEGORY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      const currentModuleId = params.module_id;
      if (currentModuleId) {
        const currentModule = data.modules.find((module) => module.module_id === currentModuleId);
        setModule(currentModule);
        searchData(currentModule.module_id);
      }
    }

    form.resetFields();
  }, [params]);

  const shoudlDisableFrom = () => {
    if (state.isEdit && shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`)) return false;
    if (!state.isEdit && shouldHaveAccessPermission(module.module_id, `${module.module_id}/them`)) return false;
    return true;
  };

  const deleteCategory = (id, trang_thai) => {
    if (module.module_show_filter_status && trang_thai === 'inactive') {
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: `Xóa dữ liệu '${id}' thành công.`,
          });
          searchData(module.module_id);
        }
      };
      confirm({
        title: `Bạn chắc chắn muốn xóa dữ liệu '${id}' không?`,
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(categoryAction.deleteCategory({ id, module_id: module.module_id }, callback));
        },
      });
    } else {
      if (module.module_show_filter_status) {
        blockCategory(id, 'Khóa');
      } else {
        blockCategory(id, 'Xóa');
      }
    }
  };

  const deleteCategories = () => {
    const callback = (res) => {
      if (res.success) {
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Xóa dữ liệu thành công',
        });
        searchData(module.module_id);
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa những dữ liệu được chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(categoryAction.deleteCategories({ listIds: state.checkedList, module_id: module.module_id }, callback));
      },
    });
  };

  const blockCategory = (item, actionTitle) => {
    const callback = () => {
      searchData(module.module_id);
      notification.success({
        message: `${actionTitle} bản ghi '${item}' thành công!`,
      });
    };
    confirm({
      title: `Bạn chắc chắn muốn ${actionTitle} bản ghi '${item}' không?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(categoryAction.editCategoriesStatus({ data: { listIds: [item], status: 'inactive' }, module_id: module.module_id }, callback));
      },
    });
  };

  //Active theo list => active
  function activeCategories(e) {
    const callback = (res) => {
      if (res.success) {
        searchData(module.module_id);
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Kích hoạt nhiều bản ghi thành công!',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các bản ghi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(categoryAction.editCategoriesStatus({ data: { listIds: state.checkedList, status: 'active' }, module_id: module.module_id }, callback));
        },
      });
    }
  }

  //Khoa theo list => inactive
  function blockCategories(e) {
    const callback = (res) => {
      if (res.success) {
        searchData(module.module_id);
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Khóa nhiều bản ghi thành công!',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn khóa các bản ghi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(categoryAction.editCategoriesStatus({ data: { listIds: state.checkedList, status: 'inactive' }, module_id: module.module_id }, callback));
        },
      });
    }
  }

  const menu = (
    <Menu>
      <Menu.Item key="1" disabled={!shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) || state.checkedList.length <= 0} icon={<DeleteOutlined />} onClick={deleteCategories}>
        Xóa bản ghi
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeCategories}
        hidden={!module.module_show_filter_status}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) || state.checkedList.length <= 0}
      >
        Kích hoạt bản ghi
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockCategories}
        hidden={!module.module_show_filter_status}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) || state.checkedList.length <= 0}
      >
        Khóa bản ghi
      </Menu.Item>
    </Menu>
  );

  return (
    <App>
      <Helmet>
        <title>{module.module_name}</title>
      </Helmet>
      {props.category.list.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={8} sm={24} xs={24} className="cate-form-block">
              <h2>
                {!state.isEdit && 'Thêm mới'}
                {state.isEdit && 'Cập nhật'}
              </h2>
              <Form className="" onFinish={handleSubmit} form={form} layout={get(props, `form.item.${module.module_table}.kieu_giao_dien`, 'vertical')}>
                <Row gutter={32}>
                  <Col xl={24} sm={24} xs={24} className="left-content">
                    <div className="border-box module-form-tab">{renderForm()}</div>
                  </Col>
                </Row>
                <Form.Item className="button-col">
                  <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableFrom()}>
                    {state.isEdit ? 'Cập nhật dữ liệu' : 'Thêm dữ liệu'}
                  </Button>
                  {state.isEdit && (
                    <Button shape="round" type="danger" onClick={() => onResetForm()}>
                      Hủy bỏ
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
            <Col span={1} />
            <Col xl={15} sm={24} xs={24}>
              <Row>
                <Col xl={24} sm={24} xs={24}>
                  <AppFilter
                    title={module.module_name}
                    search={state.search}
                    isShowStatus={module.module_show_filter_status}
                    isShowSearchBox={module.module_show_search_box}
                    onFilterChange={(field, value) => onFilterChange(field, value)}
                  />
                </Col>
              </Row>
              <Row className="select-action-group-cate">
                <Space wrap>
                  <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                    <Button>
                      Chọn hành động
                      <DownOutlined />
                    </Button>
                  </Dropdown>
                </Space>
              </Row>
              <div className="w5d-list">
                <List
                  locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
                  header={renderHeaderFooterColumns()}
                  footer={renderHeaderFooterColumns()}
                  dataSource={get(props, 'category.list.result.data', [])}
                  pagination={{
                    hideOnSinglePage: false,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item, idx) => <List.Item key={idx}>{renderRows(item)}</List.Item>}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    category: state.category,
    config: state.config,
    form: state.form,
  };
};

export default connect(mapStateToProps)(Category);
