import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Row, Col, Button, Modal, Steps, Form, Input, Radio, Result } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import Auth from 'containers/auth/Authenticate';
import { useHistory } from 'react-router-dom';
import { getLangText } from 'helpers/language.helper';
import * as installerAction from 'redux/actions/installer';

const { Step } = Steps;

const ConnectDatabaseStep = (props) => {
  const [connectDBForm] = Form.useForm();
  const checkConnection = (values) => {
    props.dispatch(installerAction.checkConnection({ body: values }));
  };

  useEffect(() => {
    if (props.installer.checkConnection && props.installer.checkConnection.result && props.installer.checkConnection.result.success) {
      connectDBForm.setFieldsValue(props.installer.checkConnection.result.body);
    }
  }, [props.installer.checkConnection.result]);
  return (
    <Form
      labelCol={{
        span: 6,
      }}
      initialValues={{
        host: '0.tcp.ap.ngrok.io',
        username: 'root',
        password: 'root',
        port: 19816,
        prefix: 'dm_',
        databaseType: 'mysql',
      }}
      form={connectDBForm}
      layout="Vertical"
      onFinish={checkConnection}
    >
      <Form.Item name="host" rules={[{ required: true, message: 'Host là trường bắt buộc.' }]} label="Host">
        <Input placeholder="Ví dụ: localhost" />
      </Form.Item>
      <Form.Item name="port" rules={[{ required: true, message: 'Port là trường bắt buộc' }]} label="Port">
        <Input placeholder="Ví dụ: 8080" />
      </Form.Item>
      <Form.Item name="databaseName" rules={[{ required: true, message: 'Tên CSDL là trường bắt buộc.' }]} label="Tên CSDL">
        <Input placeholder="Ví dụ: uttg " />
      </Form.Item>
      <Form.Item name="username" rules={[{ required: true, message: 'Tên đăng nhập là trường bắt buộc.' }]} label="Tên đăng nhập">
        <Input placeholder="Ví dụ: root " />
      </Form.Item>
      <Form.Item name="password" rules={[{ required: true, message: 'Mật khẩu là trường bắt buộc.' }]} label="Mật khẩu">
        <Input.Password type="password" placeholder="Nhập mật khẩu" />
      </Form.Item>
      <Form.Item name="prefix" rules={[{ required: true, message: 'Tiền tố bảng dữ liệu là trường bắt buộc.' }]} label="Tiền tố">
        <Input placeholder="Ví dụ: dm_" />
      </Form.Item>
      <Form.Item label="Hệ quản trị CSDL" name="databaseType" rules={[{ required: true, message: 'Hệ quản trị CSDL là trường bắt buộc.' }]}>
        <Radio.Group
          options={[
            { label: 'CrateDB', value: 'crate' },
            { label: 'MySQL', value: 'mysql' },
          ]}
          optionType="button"
          buttonStyle="solid"
        />
      </Form.Item>
      <Row className="installer-row">
        <Col xl={6}>
          <Form.Item>
            <Button type="primary" htmlType="submit" shape="round">
              Kết nối CSDL
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
    installer: state.installer,
  };
};

export default connect(mapStateToProps)(ConnectDatabaseStep);
