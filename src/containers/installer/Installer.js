import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Row, Col, Button, Modal, Steps, Form, Input, Radio, Result } from 'antd';
import Auth from 'containers/auth/Authenticate';
import { useHistory } from 'react-router-dom';
import * as installerAction from 'redux/actions/installer';
import Loading from 'components/loading/Loading';
import ConnectDatabaseStep from 'containers/installer/components/ConnectDatabaseStep';

const { Step } = Steps;

const ResultStep = () => {
  return <Result status="success" title="Bạn đã cài đặt thông tin thành công!" subTitle="Vui lòng chọn 'đi tới Trang đăng nhập' để kết thúc" />;
};
const steps = [
  {
    title: 'Kết nối CSDL ',
    content: <ConnectDatabaseStep />,
  },
  {
    title: 'Hoàn Thành',
    content: <ResultStep />,
  },
];

function Installer(props) {
  const [current, setCurrent] = React.useState(0);
  const [body, setBody] = React.useState({});
  const [isEnableInitData, setIsEnableInitData] = useState(false);

  const history = useHistory();

  const next = () => {
    if (current === 0) {
      Modal.confirm({
        title: 'Tiến hành cài đặt dữ liệu',
        content: 'Chúng tôi sẽ tiến hành tạo bảng dữ liệu và thêm dữ liệu mặc định. Bạn có chắc chắn muốn thực hiện không?',
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        onOk: () => {
          const callback = () => {
            setCurrent(current + 1);
          };
          props.dispatch(installerAction.initData({ body }, callback));
        },
      });
    }
  };

  const prev = () => {
    setIsEnableInitData(false);
    setCurrent(current - 1);
  };

  const redirect = () => {
    history.push('/auth/login');
  };

  useEffect(() => {
    if (props.installer.checkConnection && props.installer.checkConnection.result && props.installer.checkConnection.result.success) {
      setIsEnableInitData(true);
      setBody(props.installer.checkConnection.result.body);
      Modal.success({
        title: 'Kết nối thành công.',
        content: 'Bạn đã kết nối đến CSDL thành công.',
      });
    } else if (props.installer.checkConnection && props.installer.checkConnection.result && props.installer.checkConnection.result.msg && !props.installer.checkConnection.result.success) {
      setIsEnableInitData(false);
      Modal.error({
        title: 'Kết nối thất bại.',
        content: 'Bạn chưa kết nối được đến CSDL.',
      });
    }
  }, [props.installer.checkConnection.result]);

  useEffect(() => {
    if (props.installer.check && props.installer.check.result.installed) {
      Modal.warning({
        title: 'Thông tin kết nối đã tồn tại.',
        content: 'Thông tin kết nối CSDL đã tồn tại, bạn không thể cài đặt lại CSDL.',
        onOk: () => history.push('/auth/login'),
      });
    }
  }, [props.installer.check.result]);

  useEffect(() => {
    props.dispatch(installerAction.check());
  }, []);
  return (
    <Auth>
      <Helmet>
        <title>Kết nối CSDL</title>
      </Helmet>
      {(props.installer.check.loading || props.installer.checkConnection.loading || props.installer.initData.loading) && <Loading />}
      <div className="installer-block">
        <Steps current={current}>
          {steps.map((item) => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">{steps[current].content}</div>
        <div className="steps-action">
          {current < steps.length - 1 && (
            <Button disabled={!isEnableInitData} type="primary" shape="round" onClick={() => next()}>
              Tiếp theo
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type="primary" shape="round" onClick={redirect}>
              Đi tới Trang đăng nhập
            </Button>
          )}
        </div>
      </div>
    </Auth>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
    installer: state.installer,
  };
};

connect(mapStateToProps)(ConnectDatabaseStep);
export default connect(mapStateToProps)(Installer);
