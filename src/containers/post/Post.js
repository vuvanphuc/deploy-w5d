// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
// import InfiniteScroll from 'react-infinite-scroller';
import { get } from 'lodash';
import { Row, Col, List, Avatar, Button, Checkbox, notification, Menu, Dropdown, Space, Modal, Upload, Image, Switch, Form } from 'antd';
import { DownOutlined, PlusOutlined, LockOutlined, DeleteOutlined, UnlockOutlined, QuestionCircleOutlined, DownloadOutlined, UploadOutlined } from '@ant-design/icons';
import { io as socketIOClient } from 'socket.io-client';

// import internal libs
import * as postAction from 'redux/actions/post';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl, getUserInformation } from 'helpers/common.helper';
import { config } from 'config';
import userDefaultImage from 'assets/images/defaultUser.jpg';
import newNews from 'assets/images/ico_news.png';
import hotNews from 'assets/images/ico_hot.png';
import templateFile from 'assets/templates/User_temp.xlsx';
import App from 'App';
import constants from '../../constants/global.constants';
import AppFilter from 'components/AppFilter';
import W5dStatusTag from 'components/status/W5dStatusTag';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import { ExportToExcel } from 'helpers/ExportToExcel';
import W5dImageMinIO from 'components/W5dImageMinIO';

const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';

function Post(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);
  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };
  const searchPosts = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.post.list.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(postAction.getPosts(searchParams));
    //form.setFieldsValue(state.body);
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.post.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });
  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };
  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
      checkAll: 0,
      checkedList: [],
    }));
    changeUrlParams(field, value);
  };
  const onDelete = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Block bài viết thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa bài viết thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        searchPosts();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(postAction.deletePost({ id }, callback));
      },
    });
  };

  const onSwitch = (body) => {
    const callback = (res) => {
      if (res.success) {
        searchPosts();
        notification.success({
          message: 'Đổi trạng thái thành công',
        });
      }
    };
    props.dispatch(postAction.editPost(body, callback));
  };
  const allOptions = get(props, 'post.list.result.data', []).map((item) => item.tin_tuc_id);

  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (tin_tuc_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(tin_tuc_id) ? state.checkedList.filter((it) => it !== tin_tuc_id) : state.checkedList.concat([tin_tuc_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  function deleteListPosts(e) {
    const callback = (res) => {
      if (res.success) {
        searchPosts();
        notification.success({
          message: 'Xóa nhiều bài viết thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các bài viết được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(postAction.deletePosts({ listIds: state.checkedList }, callback));
      },
    });
  }
  //Active thanh vien theo list => active
  function activeListPosts(e) {
    const callback = (res) => {
      if (res.success) {
        searchPosts();
        notification.success({
          message: 'Kích hoạt bài viết thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các bài viết được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(postAction.editStatusPost({ listIds: state.checkedList, status: 'active' }, callback));
        },
      });
    }
  }
  //Khoa thanh vien theo list => inactive
  function blockListPosts(e) {
    const callback = (res) => {
      if (res.success) {
        searchPosts();
        notification.success({
          message: 'Khóa bài viết thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn chắc chắn muốn khóa những bài viết được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            postAction.editStatusPost(
              {
                listIds: state.checkedList,
                status: 'inactive',
              },
              callback
            )
          );
        },
      });
    }
  }

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListPosts} disabled={!shouldHaveAccessPermission('tin_tuc', 'tin_tuc/xoa') || state.checkedList.length <= 0}>
        Xóa bài viết
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeListPosts}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('tin_tuc', 'tin_tuc/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt bài viết
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockListPosts}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('tin_tuc', 'tin_tuc/sua') || state.checkedList.length <= 0}
      >
        Khóa bài viết
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.post.list.search,
      ...urlQuery,
      loading: true,
    };
    props.dispatch(postAction.getPosts(searchParams));
  }, [props.location]);
  const findCate = (id) => {
    const cates = get(props, 'post.listCates.result.data', []);
    const cate = cates.find((ct) => ct.nhom_tin_tuc_id === id);
    return cate ? cate.ten_nhom : '';
  };
  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.post.list.search,
      ...urlQuery,
      loading: false,
    };
    props.dispatch(postAction.getPostCates());
    const socket = socketIOClient(config.API_URL, config.SOCKET_CONFIG);
    socket.on('event://posts-changed', () => {
      props.dispatch(postAction.getPosts(searchParams));
    });
  }, []);
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const saveFile = () => {
    FileSaver.saveAs(templateFile, 'tempUser.xlsx');
  };

  function processExcel(data) {
    const workbook = XLSX.read(data, { type: 'binary' });
    const firstSheet = workbook.SheetNames[0];
    const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    const onSuccess = () => {
      notification.success({
        message: 'Import biểu mẫu thành công',
      });
    };
    const onError = () => {
      const urlQuery = queryString.parse(props.location.search);
      const searchParams = {
        ...props.post.list.search,
        ...urlQuery,
        loading: false,
      };
      props.dispatch(postAction.getPosts(searchParams));
    };
    //props.dispatch(postAction.importPosts({ listUsers: excelRows }, onSuccess, onError));
  }

  const customRequest = async ({ file }) => {
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      if (reader.readAsBinaryString) {
        reader.onload = (e) => {
          processExcel(reader.result);
        };
        reader.readAsBinaryString(file);
      }
    } else {
      notification.error({
        message: 'Không đọc được file',
      });
    }
  };

  const menuImport = (
    <Menu>
      <Menu.Item key="1" icon={<DownloadOutlined />} onClick={saveFile}>
        Tải xuống file mẫu
      </Menu.Item>
      <Menu.Item key="2" icon={<UploadOutlined />}>
        <Upload id="uploadFile" multiple={false} accept=".xls,.xlsx" showUploadList={false} customRequest={(data) => customRequest(data)}>
          Đẩy lên dữ liệu
        </Upload>
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý bài viết</title>
      </Helmet>
      {props.post.list.loading && <Loading />}
      <Row className="app-main">
        <Col xl={24} className="body-content">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                title="Bài viết"
                isShowCategories={true}
                isShowStatus={true}
                isShowSearchBox={true}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                categories={get(props, 'post.listCates.result.data', []).map((cate) => ({
                  name: cate.ten_nhom,
                  id: cate.nhom_tin_tuc_id,
                }))}
                search={state.search}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group" gutter={[8, 8]}>
            <Col xl={12} sm={12} xs={24}>
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
            </Col>
            <Col xl={12} sm={12} xs={24} className="right-actions">
              {/* <Space wrap>
                <Dropdown overlay={menuImport} trigger="click">
                  <Button>
                    Import file dữ liệu
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
              <ExportToExcel
                apiData={get(props, 'post.list.result.data', [])
                  .map((item) => {
                    return {
                      'Mã bài viết': item.tin_tuc_id,
                      'Tên bài viết': item.tieu_de,
                      'Mô tả': item.mo_ta,
                      'Nội dung': item.noi_dung,
                      'Mã nhóm': item.nhom_tin_tuc_id,
                      'Tên nhóm': findCate(item.nhom_tin_tuc_id),
                      'Bài viết mới': item.tin_moi,
                      'Bài viết nổi bật': item.tin_noi_bat,
                      'Ngày tạo': moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT),
                    };
                  })
                  .sort((a, b) => (a.Nhom > b.Nhom ? 1 : -1))}
                fileName="DanhSach"
              /> */}
              {shouldHaveAccessPermission('tin_tuc', 'tin_tuc/them') && (
                <Link to="/post/add">
                  <Button shape="round" type="primary" icon={<PlusOutlined />} className="btn-action">
                    Thêm mới bài viết
                  </Button>
                </Link>
              )}
            </Col>
          </Row>

          <div className="w5d-list">
            <List
              locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
              header={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={2} xl={3}>
                    Ảnh bài viết
                  </Col>
                  <Col xs={4} xl={6} className="text-left">
                    Tiêu đề
                    <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={3} className="text-left">
                    Nhóm bài viết
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={3} className="text-left">
                    Tin nổi bật
                    <Sorting urlQuery={urlQuery} field={'tin_noi_bat'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={2} className="text-left">
                    Tin mới
                    <Sorting urlQuery={urlQuery} field={'tin_moi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              footer={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={2} xl={3}>
                    Ảnh bài viết
                  </Col>
                  <Col xs={4} xl={6} className="text-left">
                    Tiêu đề
                    <Sorting urlQuery={urlQuery} field={'email'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={3} className="text-left">
                    Nhóm bài viết
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={3} className="text-left">
                    Tin nổi bật
                    <Sorting urlQuery={urlQuery} field={'tin_noi_bat'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={2} className="text-left">
                    Tin mới
                    <Sorting urlQuery={urlQuery} field={'tin_moi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              dataSource={get(props, 'post.list.result.data', [])}
              pagination={{
                hideOnSinglePage: false,
                responsive: true,
                showLessItems: true,
                pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                pageSize: pageSize,
                onChange: (page, pageSize) => {
                  changeMutilUrlParams({ pageSize, page });
                  setPageSize(pageSize);
                  setPage(page);
                },
                current: Number(page),
                showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                showSizeChanger: true,
              }}
              renderItem={(item) => (
                <List.Item key={item.tin_tuc_id} id="listUsers">
                  <Row className="full">
                    <Col xs={1} xl={1}>
                      <Checkbox checked={state.checkedList.includes(item.tin_tuc_id)} value={item.tin_tuc_id} onChange={() => onChangeCheck(item.tin_tuc_id)} />
                    </Col>
                    <Col xs={2} xl={3}>
                      <Link className="view" to={shouldHaveAccessPermission('tin_tuc', 'tin_tuc/chi_tiet') ? `/post/detail/${item.tin_tuc_id}` : '#'}>
                        <W5dImageMinIO image={item.anh_dai_dien} />
                      </Link>
                    </Col>
                    <Col xs={4} xl={6} className="text-left">
                      <Link className="edit" to={shouldHaveAccessPermission('tin_tuc', 'tin_tuc/sua') ? `/post/edit/${item.tin_tuc_id}` : '#'}>
                        {item.tieu_de}
                      </Link>
                      <div className="actions">
                        {shouldHaveAccessPermission('tin_tuc', 'tin_tuc/sua') && (
                          <Link className="edit act" to={`/post/edit/${item.tin_tuc_id}`}>
                            {getLangText('GLOBAL.EDIT')}
                          </Link>
                        )}
                        {shouldHaveAccessPermission('tin_tuc', 'tin_tuc/xoa') && (
                          <Button
                            className="delete act"
                            type="link"
                            onClick={() => {
                              onDelete(item.tin_tuc_id, item.trang_thai);
                            }}
                          >
                            {item.trang_thai === 'active' ? 'Khóa' : 'Xóa'}
                          </Button>
                        )}
                        {/* {(shouldHaveAccessPermission('tin_tuc', 'tin_tuc/chi_tiet') &&
                            <Link className="view act" to={`/post/detail/${item.tin_tuc_id}`}>
                              {getLangText('GLOBAL.VIEW')}
                            </Link>
                          )} */}
                      </div>
                    </Col>
                    <Col xs={4} xl={3} className="text-left">
                      {findCate(item.nhom_tin_tuc_id)}
                    </Col>
                    <Col xs={4} xl={3} className="text-left">
                      <Switch
                        checkedChildren="Có"
                        unCheckedChildren="Không"
                        defaultChecked={item.tin_noi_bat === 1}
                        onChange={() => {
                          const isNew = item.tin_noi_bat === 1 ? 0 : 1;
                          onSwitch({ ...item, tin_noi_bat: isNew });
                        }}
                      />
                    </Col>
                    <Col xs={3} xl={2} className="text-left">
                      <Switch
                        checkedChildren="Có"
                        unCheckedChildren="Không"
                        defaultChecked={item.tin_moi === 1}
                        onChange={() => {
                          const isNew = item.tin_moi === 1 ? 0 : 1;
                          onSwitch({ ...item, tin_moi: isNew });
                        }}
                      />
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      <W5dStatusTag status={item.trang_thai} />
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    post: state.post,
  };
};
export default connect(mapStateToProps)(Post);
