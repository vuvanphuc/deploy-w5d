// import external libs
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import { get } from 'lodash';
// import { v4 as uuidv4 } from 'uuid';
import { Button, Row, Col, Steps, notification } from 'antd';
import { UnorderedListOutlined, PlusOutlined, SolutionOutlined, BuildOutlined } from '@ant-design/icons';

// import internal libs
import App from 'App';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import * as formAction from 'redux/actions/form';
import * as tableAction from 'redux/actions/table';

import FormBuilderInfo from './components/FormBuilderInfo';
import FormBuilderBuilding from './components/FormBuilderBuilding';
import Loading from 'components/loading/Loading';
const { Step } = Steps;

function FormBuilderMaster(props) {
  // const [form] = Form.useForm();
  const urlQuery = queryString.parse(props.location.search);
  const history = useHistory();

  let step = urlQuery.step ? Number(urlQuery.step) : 1;
  step = step > 2 ? 2 : step;
  step = step < 1 ? 1 : step;

  const [current, setCurrent] = React.useState(step);

  const defaultField = {
    label: 'Tên trường dữ liệu',
    name: 'text_name',
    type: 'text',
    require: false,
    meta: [],
    subs: [],
  };

  const defaultForm = {
    ten_bieu_mau: '',
    bang_du_lieu: '',
    mo_ta: '',
    nhom_bieu_mau_id: '',
    trang_thai: '',
    listFields: [],
    isChanged: false,
    columns: [],
  };

  const [state, setState] = useState(defaultForm);

  const next = () => {
    if (current === 1) {
      if (state.ten_bieu_mau === '') {
        notification.error({
          message: 'Tên trường dữ liệu chưa được nhập',
        });
      } else if (state.bang_du_lieu === '') {
        notification.error({
          message: 'Bảng dữ liệu chưa được chọn',
        });
      } else {
        changeUrlParams('step', current + 1);
        setCurrent(current + 1);
        //props.dispatch(tableAction.getColumns({ categories: state.bang_du_lieu }));
        return true;
      }
    } else if (current === 2) {
      const nameArr = state.listFields.map((item) => item.name);
      const isDuplicate = nameArr.some((item, idx) => {
        return nameArr.indexOf(item) !== idx;
      });
      if (isDuplicate) {
        notification.error({
          message: 'Tên trường dữ liệu không được trùng nhau',
        });
      } else {
        // changeUrlParams('step', current + 1);
        // setCurrent(current + 1);
        return true;
      }
    } else {
      changeUrlParams('step', current + 1);
      setCurrent(current + 1);
      return true;
    }
    return false;
  };

  const prev = () => {
    changeUrlParams('step', current - 1);
    setCurrent(current - 1);
  };

  const resetFromDatabase = () => {
    const columns = get(props, 'table.list.result.data', []).filter((item) => item.bangid === state.bang_du_lieu);
    const getFieldType = (field) => {
      if (field.kieudulieu === 'datetime') return 'date-time';
      else if (field.kieudulieu === 'integer' || field.kieudulieu === 'smallint' || field.kieudulieu === 'real') return 'number';
      else return 'text';
    };
    const listFields = columns.map((field) => {
      return {
        id: field.truongid,
        name: field.csdl_tentruong,
        label: field.tentruong,
        type: getFieldType(field),
        require: field.dulieu_batbuoc,
        isActive: true,
      };
    });
    setState((state) => ({
      ...state,
      listFields,
      columns,
    }));
  };

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    history.push(url);
  };

  const handleSubmit = () => {
    if (props.match.params.id) {
      // edit a form
      const callback = () => {
        notification.success({
          message: getLangText('MESSAGE.UPDATE_FORM_BUILDER_SUCCESS'),
        });
        setState((state) => ({
          ...state,
          isChanged: false,
        }));
      };
      props.dispatch(
        formAction.editForm(
          {
            ...state,
            truong_du_lieu: JSON.stringify(state.listFields),
          },
          callback
        )
      );
    } else {
      //add a new form

      const callback = (data) => {
        notification.success({
          message: getLangText('MESSAGE.CREATE_FORM_BUILDER_SUCCESS'),
        });
        setState((state) => ({
          ...state,
          isChanged: false,
        }));
        const routeLink = shouldHaveAccessPermission('forms', 'edit') ? `/form/edit/${data.data.bieu_mau_id}` : '/forms';
        history.push(routeLink);
      };
      props.dispatch(
        formAction.createForm(
          {
            ...state,
            truong_du_lieu: JSON.stringify(state.listFields),
          },
          callback
        )
      );
    }
  };

  const showField = (field) => (field ? field : '');

  const isEdit = props.match && props.match.params && props.match.params.id;

  const changeField = (field, value) => setState((state) => ({ ...state, [field]: value }));

  const addFormField = (index) => {
    if (typeof index !== 'undefined') {
      const listFields = state.listFields;
      const field = state.listFields[index];
      if (field.subs && field.subs.length > 0) {
        field.subs = [
          ...field.subs,
          {
            ...defaultField,
            id: `${Math.random().toString(36).substring(7)}-${new Date().getTime()}`,
          },
        ];
      } else {
        field.subs = [
          {
            ...defaultField,
            id: `${Math.random().toString(36).substring(7)}-${new Date().getTime()}`,
          },
        ];
      }
      listFields[index] = field;
      setState((state) => ({
        ...state,
        listFields,
      }));
    } else {
      setState((state) => ({
        ...state,
        listFields: state.listFields.concat({
          ...defaultField,
          id: `${Math.random().toString(36).substring(7)}-${new Date().getTime()}`,
        }),
      }));
    }
  };

  const removeFormField = (index, indexParent) => {
    if (typeof indexParent !== 'undefined') {
      const listFields = state.listFields;
      const field = state.listFields[indexParent];
      field.subs = field.subs.filter((item, idx) => idx !== index);
      listFields[indexParent] = { ...field };
      setState((state) => {
        return {
          ...state,
          listFields,
        };
      });
    } else {
      setState((state) => ({
        ...state,
        listFields: state.listFields.filter((item, idx) => idx !== index),
      }));
    }
  };

  const cloneFormField = (field, index, indexParent) => {
    if (typeof indexParent !== 'undefined') {
      const listFields = state.listFields;
      const field = state.listFields[indexParent];
      field.subs.splice(index, 0, {
        ...field.subs[index],
        id: `${Math.random().toString(36).substring(7)}-${new Date().getTime()}`,
      });
      listFields[indexParent] = { ...field };

      setState((state) => {
        return {
          ...state,
          listFields,
        };
      });
    } else {
      setState((state) => {
        const listFields = state.listFields;
        listFields.splice(index, 0, {
          ...field,
          id: `${Math.random().toString(36).substring(7)}-${new Date().getTime()}`,
        });
        return {
          ...state,
          listFields,
        };
      });
    }
  };

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    let step = urlQuery.step ? Number(urlQuery.step) : 1;
    step = step > 2 ? 2 : step;
    step = step < 1 ? 1 : step;
    setCurrent(step);
    props.dispatch(formAction.getFormCates());
    props.dispatch(tableAction.getTables());
    // if (urlQuery.table) {
    //   props.dispatch(tableAction.getColumns({ categories: urlQuery.table }));
    // }
    props.dispatch(tableAction.getColumns());

    if (isEdit) {
      props.dispatch(
        formAction.getForm({
          bieu_mau_id: props.match.params.id,
        })
      );
    }
    return () => (window.onbeforeunload = null);
  }, []);

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    if (isEdit && urlQuery.table && props.form.item[urlQuery.table]) {
      setState((state) => ({
        ...state,
        ...props.form.item[urlQuery.table],
        listFields: props.form.item[urlQuery.table] && props.form.item[urlQuery.table].truong_du_lieu ? JSON.parse(props.form.item[urlQuery.table].truong_du_lieu) : [],
      }));
    }
  }, [props.form.item[urlQuery.table]]);

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }
  const steps = [
    {
      title: 'Thông tin cơ bản',
      description: 'Bước 1',
      content: <FormBuilderInfo {...state} changeField={(field, value) => changeField(field, value)} />,
      icon: <SolutionOutlined />,
    },
    {
      title: 'Tạo biểu mẫu',
      description: 'Bước 2',
      content: (
        <FormBuilderBuilding
          {...state}
          addFormField={(field) => addFormField(field)}
          removeFormField={(index, indexParent) => removeFormField(index, indexParent)}
          cloneFormField={(field, index, indexParent) => cloneFormField(field, index, indexParent)}
          changeField={(field, value) => changeField(field, value)}
          handleSubmit={() => handleSubmit()}
          columns={state.columns}
          so_cot={state.so_cot}
        />
      ),
      icon: <BuildOutlined />,
    },
  ];
  return (
    <App>
      <Helmet>
        <title>{isEdit ? `${getLangText('FORM_BUILDER.EDIT_FORM_BUILDER')}: ${showField(state.fullname)}` : getLangText('FORM_BUILDER.ADD_NEW_FORM_BUILDER')}</title>
      </Helmet>
      {props.form.item.loading && <Loading />}
      <Row className="app-main form-builder-main">
        <Col span={24} className="body-content">
          <div className="w5d-form">
            <Row gutter={25}>
              <Col xl={18} sm={16} xs={24}>
                <h2 className="header-form-title">{isEdit ? getLangText('FORM_BUILDER.EDIT_FORM_BUILDER') : getLangText('FORM_BUILDER.ADD_NEW_FORM_BUILDER')}</h2>
              </Col>
              <Col xl={6} sm={8} xs={0}>
                <Col xl={4} sm={24} xs={24}></Col>
                <Col xl={20} sm={24} xs={24}>
                  <Link to={isEdit ? '/form/add' : '/forms'}>
                    <Button block shape="round" type="primary" icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />} className="btn-create-todo">
                      {isEdit ? getLangText('FORM_BUILDER.ADD_NEW_FORM_BUILDER') : getLangText('FORM_BUILDER.LIST_FORM_BUILDERS')}
                    </Button>
                  </Link>
                </Col>
              </Col>
              <Col xl={24} sm={24} xs={24}>
                <div className="steps-content">
                  {' '}
                  <Steps current={current} initial={1}>
                    {steps.map((item) => (
                      <Step key={item.title} title={item.title} icon={item.icon} subTitle={item.subTitle} description={item.description} />
                    ))}
                  </Steps>
                  {steps[current - 1].content}
                </div>
                <div className="steps-action">
                  <br />
                  {current > 1 && (
                    <>
                      <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
                        Quay lại
                      </Button>
                      <Button type="danger" style={{ margin: '0 8px' }} onClick={() => resetFromDatabase()}>
                        Reset dữ liệu từ CSDL
                      </Button>
                      <Button
                        type="primary"
                        onClick={() => {
                          if (next()) {
                            handleSubmit();
                          }
                        }}
                      >
                        Hoàn thành
                      </Button>
                    </>
                  )}
                  {current === 1 && (
                    <>
                      <Button type="primary" onClick={() => next()}>
                        Tiếp theo
                      </Button>
                    </>
                  )}
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.form,
    table: state.table,
  };
};

export default connect(mapStateToProps)(FormBuilderMaster);
