// import external libs
import React, { useState, useEffect } from 'react';
import AntFormBuilder from 'antd-form-builder';
import { get } from 'lodash';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { useLocation, useParams } from 'react-router-dom';
import moment from 'moment';
import { Form } from 'antd';
import { MailOutlined } from '@ant-design/icons';

// import internal libs
import * as formAction from 'redux/actions/form';
import * as userAction from 'redux/actions/user';
import * as moduleAction from 'redux/actions/moduleApp';
//import * as categoryAction from 'redux/actions/category';

import {
  ColorPickerWidget,
  DatePickerWidget,
  //SwitchWidget,
  RadioGroupWidget,
  ImagePickerWidget,
  UrlWidget,
  HiddenWidget,
  RepeaterWidget,
  TextEditorWidget,
} from 'containers/form/components/widgets';
import Loading from 'components/loading/Loading';

function FormBuilderRender(props) {
  const { formObj } = props;
  const params = useParams();
  const location = useLocation();
  //const defaultColumnsCount = props.formConfig && props.formConfig.so_cot ? props.formConfig.so_cot : 1;
  //const [column, setColumn] = useState(defaultColumnsCount);
  const [column, setColumn] = useState();
  const [meta, setMeta] = useState([]);
  const [currentForm, setCurrentForm] = useState();

  const [viewMode, setViewMode] = useState(false);
  const urlQuery = queryString.parse(location.search);
  const isEdit = urlQuery.table && urlQuery.field && urlQuery.id;

  const isHasData = (metas) => {
    let isBuildMetas = false;

    if (isEdit) {
      if (props.record.item[props.table]) {
        isBuildMetas = true;
      }
    } else {
      isBuildMetas = metas.length > 0;
    }

    return isBuildMetas;
  };

  const checkCondition = (con, value) => {
    if (con.operator === 'c1') {
      return !!value;
    } else if (con.operator === 'c2') {
      return !value;
    } else if (con.operator === 'c3') {
      return con.value === value;
    } else if (con.operator === 'c4') {
      return con.value !== value;
    } else if (con.operator === 'c5') {
      return value > con.value;
    } else if (con.operator === 'c6') {
      return value < con.value;
    } else return false;
  };

  const buildMetas = (metas, defaultColumns) => {
    const initialValues = props.body ? props.body : {};

    const filterMetas = metas.filter((field) => {
      if (field.hasCondition) {
        const conditions = field.conditions;
        let isVisible = false;
        conditions.forEach((con) => {
          const check = checkCondition(con, initialValues[con.field]);
          if (conditions.length === 1) {
            isVisible = check;
          } else {
            console.log('Check condition', check);
          }
        });

        return isVisible;
      }
      return true;
    });

    const migrateMetas = filterMetas.map((field) => {
      field.meta = field.meta ? field.meta : {};
      const migrateField = {
        key: field.name,
        label: field.label,
        disabled: field.disableEdit && props.isEdit,
        extra: field.instruction,
        colSpan: field.colSpan ? Number(field.colSpan) : defaultColumns,
        initialValue: initialValues[field.name] ? initialValues[field.name] : field.meta.default_value,
        placeholder: field.meta.placeholder ? field.meta.placeholder : '',
        rules: [{ required: field.require, message: 'Bạn chưa nhập trường này' }],
      };

      if (field.type === 'text') {
        return {
          ...migrateField,
          widgetProps: {
            maxLength: field.meta.max_length ? field.meta.max_length : undefined,
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
          },
        };
      }
      if (field.type === 'text-area') {
        const defaultVal = field.meta.default_value ? field.meta.default_value.toString() : '';
        return {
          ...migrateField,
          initialValue: initialValues[field.name] ? initialValues[field.name].toString() : defaultVal,
          widget: 'textarea',
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
            rows: field.meta.rows ? field.meta.rows : 3,
            maxLength: field.meta.max_length ? field.meta.max_length : undefined,
          },
        };
      }
      if (field.type === 'text-editor') {
        const defaultVal = field.meta.default_value ? field.meta.default_value.toString() : '';
        return {
          ...migrateField,
          initialValue: initialValues[field.name] ? initialValues[field.name].toString() : defaultVal,
          widget: TextEditorWidget,
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
            rows: field.meta.rows ? field.meta.rows : 3,
            maxLength: field.meta.max_length ? field.meta.max_length : undefined,
          },
        };
      }
      if (field.type === 'number') {
        return {
          ...migrateField,
          widget: 'number',
          widgetProps: {
            step: field.meta.step ? field.meta.step : 1,
            style: { width: get(field, 'meta.input_width', '100%') },
            min: field.meta.min ? field.meta.min : undefined,
            max: field.meta.max ? field.meta.max : undefined,
            maxLength: field.meta.max_length ? field.meta.max_length : undefined,
          },
        };
      }
      if (field.type === 'email') {
        return {
          ...migrateField,
          widgetProps: {
            maxLength: field.meta.max_length ? field.meta.max_length : undefined,
            prefix: <MailOutlined />,
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
          },
          rules: migrateField.rules.concat({
            type: 'email',
            message: 'Không đúng định dạng e-mail',
          }),
        };
      }

      if (field.type === 'url') {
        return {
          ...migrateField,
          widget: UrlWidget,
        };
      }
      if (field.type === 'password') {
        return {
          ...migrateField,
          widget: 'password',
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
          },
        };
      }

      // trường lựa chọn
      if (field.type === 'select') {
        const formatedOptions =
          field.meta.options &&
          field.meta.options.map((opt) => {
            return opt.indexOf(':') > -1 ? { value: Number(opt.split(':')[0]), label: opt.split(':')[1] } : { value: Number(opt), label: opt };
          });
        let defaultValue = field.meta.default_value ? Number(field.meta.default_value) : '';
        if (initialValues[field.name]) {
          defaultValue = Number(initialValues[field.name]);
        }

        return {
          ...migrateField,
          initialValue: defaultValue,
          widget: 'select',
          options: field.meta.options ? formatedOptions : [],
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
          },
        };
      }

      if (field.type === 'checkbox') {
        const formatedOptions =
          field.meta.options &&
          field.meta.options.map((opt) => {
            return opt.indexOf(':') > -1 ? { value: opt.split(':')[0], label: opt.split(':')[1] } : { value: opt, label: opt };
          });
        return {
          ...migrateField,
          initialValue: field.meta.default_value ? field.meta.default_value : [],
          widget: 'checkbox-group',
          options: field.meta.options ? formatedOptions : [],
        };
      }
      if (field.type === 'radio') {
        const formatedOptions =
          field.meta.options &&
          field.meta.options.map((opt) => {
            return opt.indexOf(':') > -1 ? { value: opt.split(':')[0], label: opt.split(':')[1] } : { value: opt, label: opt };
          });
        return {
          ...migrateField,
          initialValue: field.meta.default_value,
          widgetProps: {
            options: field.meta.options ? formatedOptions : [],
          },
          widget: RadioGroupWidget,
        };
      }

      // trường dữ liệu nâng cao

      if (field.type === 'repeater') {
        const initialValue = initialValues[field.name] ? JSON.parse(initialValues[field.name]) : [];
        props.formObj.setFieldsValue({ ...props.body, [field.name]: initialValue });
        return {
          ...migrateField,
          widget: RepeaterWidget,
          initialValue: initialValue,
          widgetProps: {
            format: field.meta.show_time ? 'HH:mm:ss DD/MM/YYYY' : 'DD/MM/YYYY',
            subFields: field.subs,
            meta: field.meta,
          },
        };
      }
      if (field.type === 'date-time') {
        const dateValue = initialValues[field.name] ? initialValues[field.name] : null;
        const dateValueFormatted = dateValue ? moment(dateValue).format('DD/MM/YYYY') : null;
        return {
          ...migrateField,
          value: dateValueFormatted,
          widget: DatePickerWidget,
          widgetProps: {
            format: field.meta.show_time ? 'HH:mm:ss DD/MM/YYYY' : 'DD/MM/YYYY',
            showTime: field.meta.show_time ? field.meta.show_time : '',
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
          },
        };
      }
      if (field.type === 'image-picker') {
        return {
          ...migrateField,
          widget: ImagePickerWidget,
          widgetProps: {
            meta: field.meta,
            urlParams: params,
          },
        };
      }
      if (field.type === 'date-time') {
        const dateValue = initialValues[field.name] ? initialValues[field.name] : null;
        const dateValueFormatted = dateValue ? moment(dateValue).format('DD/MM/YYYY') : null;
        return {
          ...migrateField,
          value: dateValueFormatted,
          widget: DatePickerWidget,
          widgetProps: {
            format: field.meta.show_time ? 'HH:mm:ss DD/MM/YYYY' : 'DD/MM/YYYY',
            showTime: field.meta.show_time ? field.meta.show_time : '',
            style: {
              width: get(field, 'meta.input_width', '100%'),
              meta: field.meta,
            },
          },
        };
      }

      if (field.type === 'time-picker') {
        return {
          ...migrateField,
          widget: 'date-picker',
          widgetProps: {
            format: 'HH:mm:ss',
            mode: 'time',
          },
        };
      }

      if (field.type === 'color-picker') {
        return {
          ...migrateField,
          widget: ColorPickerWidget,
          forwardRef: true,
        };
      }

      // trường dữ liệu giao diện
      if (field.type === 'label') {
        return {
          key: field.name,
          colSpan: defaultColumns,
          render() {
            return (
              <Form.Item className="input-col">
                <fieldset className={`fber-label align-${field.meta.align || 'left'}`} style={{ width: get(field, 'meta.input_width', '100%') }}>
                  <legend className="fber-label-title">{field.label}</legend>
                  {field.meta.description && <p className="fber-label-description">{field.meta.description}</p>}
                </fieldset>
              </Form.Item>
            );
          },
        };
      }

      // dữ liệu ẩn
      if (field.type === 'hidden') {
        return {
          ...migrateField,
          widget: HiddenWidget,
          formItemProps: { hidden: true },
        };
      }

      if (field.type === 'url-variable') {
        const [beforeText, afterText] = field.meta.default_value.split('{{value}}');
        let initialValue = '';
        if (beforeText && afterText) {
          initialValue = location.pathname.split(beforeText).pop().split(afterText)[0];
        }
        return {
          ...migrateField,
          widget: HiddenWidget,
          initialValue: initialValue,
          formItemProps: { hidden: true },
        };
      }

      if (field.type === 'short-unique-id') {
        return {
          ...migrateField,
          widget: HiddenWidget,
          formItemProps: { hidden: true },
          initialValue: 'SHORT_UNIQUE_ID',
        };
      }

      if (field.type === 'long-unique-id') {
        return {
          ...migrateField,
          widget: HiddenWidget,
          formItemProps: { hidden: true },
          initialValue: 'LONG_UNIQUE_ID',
        };
      }

      // dữ liệu mô đun
      if (field.type === 'module-url-params') {
        const defaultParam = field.meta && field.meta.params ? field.meta.params : undefined;
        let defaultVal = '';
        if (defaultParam && params[defaultParam]) {
          defaultVal = params[defaultParam];
        }
        return {
          ...migrateField,
          widget: HiddenWidget,
          formItemProps: { hidden: true },
          initialValue: defaultVal,
        };
      }

      // dữ liệu liên kết
      if (field.type === 'user-data') {
        const options = get(props, 'user.list.result.data', [])
          .filter((otp) => {
            if (get(field, 'meta.categories', []).length > 0) {
              return get(field, 'meta.categories', []).includes(otp.nhom_nhan_vien_id);
            }
            return otp;
          })
          .map((otp) => ({
            value: otp.nhan_vien_id,
            label: otp.ten_nhan_vien,
          }));
        return {
          ...migrateField,
          initialValue: field.meta.default_value,
          widget: 'select',
          options: options,
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
            mode: field.meta.multiple ? 'multiple' : '',
            showSearch: true,
            onSearch: (keyword) => {
              props.dispatch(userAction.getUsers({ status: 'active', keyword }));
            },
          },
        };
      }

      if (field.type === 'form-data') {
        const options = get(props, 'form.list.result.data', [])
          .filter((otp) => {
            if (get(field, 'meta.categories', []).length > 0) {
              return get(field, 'meta.categories', []).includes(otp.nhom_bieu_mau_id);
            }
            return otp;
          })
          .map((otp) => ({
            value: otp.bieu_mau_id,
            label: otp.ten_bieu_mau,
          }));
        return {
          ...migrateField,
          initialValue: field.meta.default_value,
          widget: 'select',
          options: options,
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
            mode: field.meta.multiple ? 'multiple' : '',
            showSearch: true,
            onSearch: (keyword) => {
              props.dispatch(formAction.getForms({ status: 'active', keyword }));
            },
          },
        };
      }

      if (field.type === 'module-data') {
        const moduleConfig = get(props, 'module.list.result.moduleInfo', {});
        const options = get(props, 'module.list.result.data', []).map((otp) => ({
          value: otp[moduleConfig.key],
          label: otp[moduleConfig.key],
        }));
        return {
          ...migrateField,
          initialValue: field.meta.default_value,
          widget: 'select',
          options: options,
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
            mode: field.meta.multiple ? 'multiple' : '',
            showSearch: true,
            onSearch: (keyword) => {
              props.dispatch(moduleAction.getModules({ status: 'active', module_id: get(field, 'meta.module', ''), keyword }));
            },
          },
        };
      }

      if (field.type === 'category-data') {
        const cateData = get(props, 'category.categoryData.result.data', []);
        let options = [];
        if (cateData[field.meta.category]) {
          options = cateData[field.meta.category].map((otp) => ({
            value: Number(otp[field.meta.key_column]),
            label: otp[field.meta.value_column],
          }));
        }
        let defaultValue = field.meta.default_value ? Number(field.meta.default_value) : '';
        if (initialValues[field.name]) {
          defaultValue = Number(initialValues[field.name]);
        }

        return {
          ...migrateField,
          initialValue: defaultValue,
          widget: 'select',
          options: options ? options : [],
          widgetProps: {
            style: {
              width: get(field, 'meta.input_width', '100%'),
            },
          },
        };
      }

      return {
        ...migrateField,
        widgetProps: {
          maxLength: field.meta.max_length ? field.meta.max_length : undefined,
        },
      };
    });
    return {
      columns: defaultColumns,
      formItemLayout: [6, 18],
      fields: migrateMetas,
    };
  };

  useEffect(() => {
    const forms = get(props, 'form.list.result.data', []);
    const currentForm = forms.find((frm) => frm.bieu_mau_id === props.formId);
    const defaultColumnsCount = currentForm && currentForm.so_cot ? currentForm.so_cot : 1;
    setColumn(defaultColumnsCount);
    setViewMode(currentForm && currentForm.che_do_chi_xem);
    const mainMeta = currentForm && currentForm.truong_du_lieu ? JSON.parse(currentForm.truong_du_lieu) : [];
    setCurrentForm(currentForm);

    if (isHasData(mainMeta)) {
      // const bangidList = mainMeta
      //   .filter((field) => field.type === 'category-data')
      //   .map((field) => {
      //     return field.meta.category;
      //   });
      // if (bangidList && bangidList.length > 0) {
      //   props.dispatch(categoryAction.getCategoryData({ bangidlist: bangidList.join('_') }));
      // } else {
      const metaMigrated = buildMetas(mainMeta, defaultColumnsCount);
      setMeta(metaMigrated);
      // }
    }
  }, [props.form, props.body, props.user, props.module, props.formID, props.table, props.formConfig, props.formObj, props.isEdit]);

  // useEffect(() => {
  //   const mainMeta = currentForm && currentForm.truong_du_lieu ? JSON.parse(currentForm.truong_du_lieu) : [];

  //   if (isHasData(mainMeta)) {
  //     const metaMigrated = buildMetas(mainMeta, column);
  //     setMeta(metaMigrated);
  //   }
  // }, [props.category.categoryData, props.isEdit]);

  return (
    <div className={`${viewMode ? 'viewMode' : ''} form-builder-gennerated`}>
      {props.form.item.loading && <Loading />}
      <div className="form-generate">
        <AntFormBuilder form={formObj} meta={meta} />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.form,
    record: state.record,
    user: state.user,
    module: state.module,
    category: state.category,
  };
};

export default connect(mapStateToProps)(FormBuilderRender);
