// import external libs
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Form, Input, Row, Col, Select, Radio } from 'antd';

// import internal libs
import constants from '../../../constants/global.constants';
import { getLangText } from '../../../helpers/language.helper';

const { Option } = Select;
const { TextArea } = Input;

function FormBuilderInfo(props) {
  const [form] = Form.useForm();
  const renderCategories = () => {
    let options = [];
    if (props.form.listCates.result && props.form.listCates.result.data) {
      options = props.form.listCates.result.data.map((cate) => (
        <Option key={cate.nhom_bieu_mau_id} value={cate.nhom_bieu_mau_id}>
          {cate.ten_nhom}
        </Option>
      ));
    }

    return (
      <Select showSearch onChange={(cat) => props.changeField('nhom_bieu_mau_id', cat)} value={props.nhom_bieu_mau_id} placeholder="Please select">
        {options}
      </Select>
    );
  };

  const renderTables = () => {
    let options = [];
    if (props.table.listCates.result && props.table.listCates.result.data) {
      options = props.table.listCates.result.data.map((cate) => (
        <Option key={cate.bangid} value={cate.bangid}>
          {cate.tenbang}
        </Option>
      ));
    }

    return (
      <Select showSearch onChange={(table) => props.changeField('bang_du_lieu', table)} value={props.bangid} placeholder="Please select">
        {options}
      </Select>
    );
  };

  const renderStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select onChange={(trang_thai) => props.changeField('trang_thai', trang_thai)} value={props.trang_thai} placeholder={getLangText('GLOBAL.SELECT_STATUS')}>
        {options}
      </Select>
    );
  };

  const renderLayoutTypes = () => {
    const options = constants.LAYOUT_TYPES.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select
        onChange={(kieu_giao_dien) => props.changeField('kieu_giao_dien', kieu_giao_dien)}
        value={props.kieu_giao_dien}
        placeholder={'Kiểu giao diện'}
        defaultValue={constants.LAYOUT_TYPES[0].value}
      >
        {options}
      </Select>
    );
  };

  const renderColumnNumber = () => {
    const options = constants.COLUMN_NUMBER.map((col) => (
      <Option key={col} value={col}>
        {col}
      </Option>
    ));
    return (
      <Select onChange={(so_cot) => props.changeField('so_cot', so_cot)} value={props.so_cot} placeholder={'Số cột hiển thị'} defaultValue={constants.COLUMN_NUMBER[0]}>
        {options}
      </Select>
    );
  };

  useEffect(() => {
    form.setFieldsValue(props);
  }, [props]);

  return (
    <div>
      <Row className="form-main">
        <Col span={24}>
          <div className="w5d-form">
            <Form layout="vertical" form={form}>
              <Row gutter={25}>
                <Col xl={24} sm={24} xs={24} className="left-content">
                  <div className="border-box">
                    <Row gutter={25}>
                      <Col span={12}>
                        {' '}
                        <Form.Item
                          className="input-col"
                          label={'Tên biểu mẫu'}
                          name="ten_bieu_mau"
                          rules={[
                            {
                              required: true,
                              message: 'Bạn chưa nhập tên biểu mẫu',
                            },
                          ]}
                        >
                          <Input
                            placeholder="Nhập tên biểu mẫu"
                            defaultValue={props.ten_bieu_mau}
                            onBlur={(e) => {
                              props.changeField('ten_bieu_mau', e.target.value);
                            }}
                          />
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item
                          className="input-col"
                          label={'Bảng dữ liệu'}
                          name="bang_du_lieu"
                          rules={[
                            {
                              required: true,
                              message: 'Bạn chưa nhập bảng dữ liệu',
                            },
                          ]}
                        >
                          {renderTables()}
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item className="input-col" label={'Trạng thái'}>
                          {renderStatus()}
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item label={'Danh mục'} className="input-col">
                          {renderCategories()}
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item className="input-col" label={'Kiểu giao diện'}>
                          {renderLayoutTypes()}
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item className="input-col" label={'Số cột tối đa'}>
                          {renderColumnNumber()}
                        </Form.Item>
                      </Col>
                      <Col span={24}>
                        <Form.Item className="input-col" label="Kích hoạt chế độ chỉ xem dữ liệu không?" name="che_do_chi_xem" valuePropName="checked">
                          <Radio.Group
                            value={props.che_do_chi_xem}
                            onChange={(e) => {
                              props.changeField('che_do_chi_xem', e.target.value);
                            }}
                            options={[
                              { label: 'Không', value: false },
                              { label: 'Có', value: true },
                            ]}
                            optionType="button"
                            buttonStyle="solid"
                          />
                        </Form.Item>
                      </Col>
                    </Row>

                    <Form.Item className="input-col" label="Mô tả" name="mo_ta">
                      <TextArea
                        rows={6}
                        defaultValue={props.mo_ta}
                        onBlur={(e) => {
                          props.changeField('mo_ta', e.target.value);
                        }}
                        placeholder="Nhập mô tả"
                      />
                    </Form.Item>
                  </div>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.form,
    table: state.table,
  };
};
const FormBuilderInfoMemo = React.memo(FormBuilderInfo);
export default connect(mapStateToProps)(FormBuilderInfoMemo);
