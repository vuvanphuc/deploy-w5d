import React from 'react';
import { Row, Col, Input } from 'antd';

const HiddenWidget = () => {
  return (
    <Row gutter={10} className="hidden-input">
      <Col span={16}>
        <Input type="hidden" />
      </Col>
    </Row>
  );
};

export default HiddenWidget;
