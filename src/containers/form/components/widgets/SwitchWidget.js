import React from 'react';
import { Row, Col, Switch } from 'antd';

const SwitchWidget = (props) => {
  return (
    <Row gutter={10}>
      <Col span={16}>
        <Switch
          disabled={props.disabled}
          onChange={(value) => {
            props.onChange(value ? props.checkedValue : props.unCheckedValue);
          }}
          defaultValue={props.default_value}
          checked={Number(props.value) === Number(props.checkedValue) ? true : false}
          checkedChildren={props.checkedChildren}
          unCheckedChildren={props.unCheckedChildren}
        />
      </Col>
    </Row>
  );
};

export default SwitchWidget;
