import React from 'react';
import moment from 'moment';
import { get } from 'lodash';
import { DatePicker } from 'antd';

const DatePickerWidget = (props) => {
  const dateFormat = 'DD-M-YYYY';
  const dateValueFormatted = props.value ? moment(props.value) : null;
  const showTime = props.format === 'HH:mm:ss DD/MM/YYYY' ? { format: 'HH:mm' } : false;
  return (
    <DatePicker
      showTime={showTime}
      disabled={props.disabled}
      placeholder="Ví dụ: 15-7-2020"
      onChange={(value) => {
        props.onChange(moment(value).utc(7));
      }}
      style={{
        width: get(props, 'style.width', '100%'),
      }}
      showToday
      format={props.format}
      value={props.value && moment(dateValueFormatted).isValid() ? moment(dateValueFormatted, dateFormat).utc(0) : null}
    />
  );
};

export default DatePickerWidget;
