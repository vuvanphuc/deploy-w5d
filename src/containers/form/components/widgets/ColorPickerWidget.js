import React from 'react';
import { CirclePicker } from 'react-color';
import { Row, Col } from 'antd';
const ColorPickerWidget = ({ value, onChange, disabled }) => (
  <Row gutter={10}>
    <Col span={24}>
      <CirclePicker disabled={disabled} color={value} onChangeComplete={(color) => onChange(color.hex)} />
    </Col>
  </Row>
);

export default ColorPickerWidget;
