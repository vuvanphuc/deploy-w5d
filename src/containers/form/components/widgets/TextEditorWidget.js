import React, { useEffect, useState, useRef, useMemo } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import ImageResize from 'quill-image-resize-module-react';
import 'react-quill/dist/quill.snow.css';
import * as PropTypes from '@redux-saga/is';
import katex from 'katex';
import 'katex/dist/katex.min.css';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import { Button, Modal, notification } from 'antd';
import { FileImageOutlined } from '@ant-design/icons';
import configs from 'configs';
window.katex = katex;
Quill.register('modules/imageResize', ImageResize);
const TextEditorWidget = (props) => {
  const [state, setState] = useState({
    editorHtml: props.value,
    theme: 'snow',
    isChanged: false,
  });

  const quillRef = useRef();

  const [media, setmedia] = useState({
    visible: false,
  });

  const handleChange = (html) => {
    setState({ ...state, editorHtml: html, isChanged: true });
    props.onChange(html);
  };

  const handleChangeProps = () => {
    props.onChange(state.editorHtml);
  };

  const formats = ['header', 'font', 'color', 'align', 'size', 'bold', 'italic', 'underline', 'strike', 'blockquote', 'list', 'bullet', 'indent', 'link', 'image', 'video', 'formula'];

  const modules = {
    toolbar: [
      [{ header: '1' }, { header: '2' }, { font: [] }, { color: [] }, { align: [] }],
      [{ size: [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
      ['link', 'image', 'video', 'formula'],
      ['clean'],
    ],
    imageResize: {
      parchment: Quill.import('parchment'),
      modules: ['Resize', 'DisplaySize', 'Toolbar'],
    },
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false,
    },
  };

  const onSelectImage = (file) => {
    setmedia({
      ...media,
      visible: false,
    });
    const editor = quillRef.current.getEditor();
    editor.insertEmbed(editor.getSelection(), 'image', `${configs.UPLOAD_API_URL}/${file.tep_tin_url}`);
    // setState({
    //   ...state,
    //   isChanged: true,
    //   editorHtml: `${state.editorHtml} <img src="${configs.UPLOAD_API_URL}/${file.tep_tin_url}" />`,
    // });
    // setTimeout(() => {
    //   handleChangeProps();
    // }, 300);
  };

  useEffect(() => {
    if (JSON.stringify(props.value) !== JSON.stringify(state.editorHtml)) {
      setState({ ...state, editorHtml: props.value });
    }
  }, [props.value]);

  if (props.disabled) {
    return (
      <div className="viewable-editor editor-new disabled">
        <span dangerouslySetInnerHTML={{ __html: props.value }} />
      </div>
    );
  }

  return (
    <div className="text-quill-custom">
      <Button
        className="add-image"
        type="link"
        icon={<FileImageOutlined />}
        onClick={() =>
          setmedia({
            ...media,
            visible: true,
          })
        }
      >
        Thêm hình ảnh
      </Button>
      <ReactQuill
        ref={quillRef}
        theme={state.theme}
        onChange={handleChange}
        // onBlur={handleChangeProps}
        value={state.editorHtml}
        formats={formats}
        modules={modules}
        bounds={'.app'}
        placeholder={props.placeholder}
      />
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={media.visible}
        onCancel={() => {
          setmedia({
            ...media,
            visible: false,
          });
        }}
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>
    </div>
  );
};

export default TextEditorWidget;
