import React from 'react';
import { get } from 'lodash';
import { Row, Col, Form, Select, Button, Input, InputNumber, Radio } from 'antd';
import { PlusOutlined, DeleteOutlined } from '@ant-design/icons';
const { TextArea } = Input;
const { Option } = Select;

const RepeaterWidget = (props) => {
  if (!Array.isArray(props.value)) return null;
  if (get(props, 'subFields', []).length === 0) return <p>Không có trường dữ liệu con.</p>;
  const defaultValue = {};
  get(props, 'subFields', []).forEach((item) => {
    defaultValue[item.name] = item.meta.default_value;
  });
  const renderFields = (index, name, fieldKey, restField) => {
    return get(props, 'subFields', []).map((field, idx) => {
      let selectOpts = [];
      let radioOpts = [];
      const value = get(props, `value[${name}].${field.name}`, get(field, 'meta.default_value', ''));
      get(field, 'meta.options', []).forEach((opt, optIdx) => {
        if (opt.indexOf(':') > -1) {
          radioOpts.push({ label: opt.split(':')[1], value: opt.split(':')[0] });
          selectOpts.push(
            <Option key={optIdx} value={opt.split(':')[0]}>
              {opt.split(':')[1]}
            </Option>
          );
        } else {
          radioOpts.push({ label: opt, value: opt });
          selectOpts.push(
            <Option key={optIdx} value={opt}>
              {opt}
            </Option>
          );
        }
      });

      return (
        <Col span={isNaN(Number(field.colSpan)) ? 4 : Number(field.colSpan)} key={idx}>
          <Form.Item {...restField} label={index === 0 ? field.label : ''} fieldKey={[fieldKey, field.name]} rules={[{ required: field.require }]} extra={get(field, 'instruction', '')}>
            {field.type === 'text' && (
              <Input
                onChange={(e) => {
                  let current = get(props, 'value', []);
                  current[name] = {
                    ...current[name],
                    [field.name]: e.target.value,
                  };
                  props.onChange(current);
                }}
                placeholder={get(field, 'meta.placeholder', '')}
                maxLength={get(field, 'meta.max_length', '')}
                style={{ width: get(field, 'meta.input_width', '') }}
                value={value}
              />
            )}
            {field.type === 'text-area' && (
              <TextArea
                onChange={(e) => {
                  let current = get(props, 'value', []);
                  current[name] = {
                    ...current[name],
                    [field.name]: e.target.value,
                  };
                  props.onChange(current);
                }}
                placeholder={get(field, 'meta.placeholder', '')}
                style={{ width: get(field, 'meta.input_width', '') }}
                value={value}
              />
            )}
            {field.type === 'number' && (
              <InputNumber
                onChange={(e) => {
                  let current = get(props, 'value', []);
                  current[name] = {
                    ...current[name],
                    [field.name]: e.target.value,
                  };
                  props.onChange(current);
                }}
                value={value}
                placeholder={get(field, 'meta.placeholder', '')}
                step={get(field, 'meta.step', 1)}
                style={{ width: get(field, 'meta.input_width', '') }}
                min={field.meta.min ? Number(get(field, 'meta.min', 0)) : undefined}
                max={field.meta.max ? Number(get(field, 'meta.max', 0)) : undefined}
              />
            )}

            {field.type === 'select' && (
              <Select
                onChange={(val) => {
                  let current = get(props, 'value', []);
                  current[name] = {
                    ...current[name],
                    [field.name]: val,
                  };
                  props.onChange(current);
                }}
                value={value}
              >
                {selectOpts}
              </Select>
            )}
            {/* {field.type === 'Checkbox' && (
              <Select defaultValue="lucy" allowClear>
                <Option value="lucy">Lucy</Option>
              </Select>
            )} */}
            {field.type === 'radio' && (
              <Radio.Group
                onChange={(e) => {
                  let current = get(props, 'value', []);
                  current[name] = {
                    ...current[name],
                    [field.name]: e.target.value,
                  };
                  props.onChange(current);
                }}
                value={value}
                options={radioOpts}
                optionType="button"
                buttonStyle="solid"
              />
            )}
          </Form.Item>
        </Col>
      );
    });
  };
  return (
    <Row gutter={10}>
      <Col span={24} className="list-fields">
        <Form.List name={props.id}>
          {(fields, { add, remove }) => (
            <Row gutter={10} className="list-field-item">
              {fields.map(({ key, name, fieldKey, ...restField }) => (
                <Row key={key} className="full-width">
                  {renderFields(key, name, fieldKey, restField)}
                  <DeleteOutlined
                    onClick={() => {
                      remove(name);
                      let current = get(props, 'value', []);
                      current = current.filter((it, idx) => idx !== name);
                      props.onChange(current);
                    }}
                    className={`${key === 0 ? 'first-remove-sub-field' : 'remove-sub-field'}`}
                  />
                </Row>
              ))}
              <Form.Item>
                <Button
                  type="dashed"
                  onClick={() => {
                    add();
                    const current = get(props, 'value', []);
                    current.push(defaultValue);
                    props.onChange(current);
                  }}
                  icon={<PlusOutlined />}
                >
                  Thêm mới
                </Button>
              </Form.Item>
            </Row>
          )}
        </Form.List>
      </Col>
    </Row>
  );
};

export default RepeaterWidget;
