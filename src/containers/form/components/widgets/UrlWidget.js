import React, { useState } from 'react';
import { Row, Col, Input, Select } from 'antd';

const { Option } = Select;

const UrlWidget = (props) => {
  const [type, setType] = useState('http://');
  const selectBefore = (
    <Select defaultValue="http://" onChange={(value) => setType(value)}>
      <Option value="http://">http://</Option>
      <Option value="https://">https://</Option>
    </Select>
  );
  return (
    <Row gutter={10}>
      <Col span={16}>
        <Input addonBefore={selectBefore} onChange={(e) => props.onChange(`${type}${e.target.value}`)} />
      </Col>
    </Row>
  );
};

export default UrlWidget;
