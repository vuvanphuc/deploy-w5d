import React, { useState, useEffect } from 'react';
import { get } from 'lodash';
import { Row, Col, Upload, Modal } from 'antd';
import W5dGalleryBrowser from 'components/W5dGalleryBrowser';
import { PlusOutlined } from '@ant-design/icons';
import * as mediaAction from 'redux/actions/media';
import { connect } from 'react-redux';
import { config } from 'config';
import { shouldHaveAccessPermission } from 'helpers/common.helper';

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

const ImagePickerWidget = (props) => {
  const [openMediaLibrary, setOpenMediaLibrary] = useState(false);
  const [folder, setFolder] = useState();

  const [state, setState] = useState({
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: [],
  });

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setState({
      ...state,
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
    });
  };

  const handleRemove = (file) => {
    Modal.confirm({
      title: 'Bạn có chắc muốn xóa không?',
      iconType: 'close-circle',
      okText: 'Có',
      cancelText: 'Không',
      onOk() {
        const callback = () => {
          props.dispatch(mediaAction.getMedias({ category: folder }));
        };
        props.dispatch(mediaAction.deleteMedia({ id: file.tep_tin_id }, callback));
      },
    });
  };

  const customRequest = async ({ onSuccess, onError, file }) => {
    const callback = () => {
      props.dispatch(mediaAction.getMedias({ category: folder }));
    };
    props.dispatch(mediaAction.createMedia({ fileUpload: file, folder: folder }, callback));
  };

  const isDirect = get(props, 'meta.type', '') === 'direct';

  let defaultImages = [];
  if (!isDirect && Array.isArray(props.value)) {
    defaultImages = props.value ? props.value : [];
  }

  useEffect(() => {
    props.dispatch(mediaAction.getMediaCates());
  }, []);

  useEffect(() => {
    const folders = get(props, 'media.listCates.result.data', []);
    if (props.value && folders.length > 0 && get(props, 'urlParams.id', '')) {
      const cFolder = `${get(props, 'urlParams.module_parent_data_id', '')}_${get(props, 'urlParams.id', '')}`;
      setFolder(cFolder);
    }
  }, [props.media.listCates.result, props.value, props.urlParams]);

  useEffect(() => {
    const images = get(props, 'media.list.result.data', []);
    const fileList = images.map((item) => ({
      ...item,
      uid: item.tep_tin_id,
      name: item.tieu_de,
      status: 'done',
      url: `${config.UPLOAD_API_URL}/${item.tep_tin_url}`,
    }));

    setState({ ...state, fileList });
  }, [props.media.list.result, props.value]);

  useEffect(() => {
    if (folder && folder !== '') {
      props.dispatch(mediaAction.getMedias({ category: folder }));
    }
  }, [folder]);

  useEffect(() => {
    if ((!props.value || props.value === '') && folder) {
      if (get(props, 'urlParams.id', '') !== '') {
        // const callback = res => {
        //   if (res.success) {
        //     const params = {
        //       category: res.data.nhom_tep_tin_id,
        //     };
        //     props.onChange(folder);
        //   }
        // };
        // props.dispatch(mediaAction.createMediaCate({ ten_nhom: folder }, callback));
      }
    }
  }, [props.urlParams]);

  return (
    <Row gutter={10} className={props.className}>
      <Col span={24} className="upload-images-list">
        {isDirect && (
          <>
            <Upload
              listType="picture"
              multiple={true}
              customRequest={(data) => customRequest(data)}
              listType="picture-card"
              fileList={state.fileList}
              onPreview={(file) => handlePreview(file)}
              onRemove={(file) => handleRemove(file)}
              className="upload-list-inline"
              showUploadList={{
                showRemoveIcon: shouldHaveAccessPermission('thu_vien', 'thu_vien/xoa'),
              }}
              disabled={!shouldHaveAccessPermission('thu_vien', 'thu_vien/them') || !props.urlParams.id}
            >
              <div>
                <PlusOutlined />
                <div style={{ marginTop: 8 }}>Tải lên</div>
              </div>
            </Upload>
            <Modal visible={state.previewVisible} title={state.previewTitle} footer={null} onCancel={() => setState({ ...state, previewVisible: false })}>
              <img alt={state.previewTitle} style={{ width: '100%' }} src={state.previewImage} />
            </Modal>
          </>
        )}

        {!isDirect && (
          <W5dGalleryBrowser
            title={props.title || 'Hình ảnh'}
            setImageLabel={props.setImageLabel}
            type={props.type}
            images={defaultImages}
            field="image"
            onSelectImages={(images) => {
              props.onChange(images);
              setOpenMediaLibrary(false);
            }}
            visible={openMediaLibrary}
            openMediaLibrary={() => setOpenMediaLibrary(true)}
            removeImage={(image) => {
              const imgs = defaultImages.filter((img) => img !== image);
              props.onChange(JSON.stringify(imgs));
            }}
            onCancel={() => setOpenMediaLibrary(false)}
          />
        )}
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => {
  return {
    media: state.media,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ImagePickerWidget);
