import React from 'react';
import { Row, Col, Radio } from 'antd';

const RadioGroupWidget = (props) => {
  const rdValue = props.value === 0 ? '0' : props.value;
  return (
    <Radio.Group
      disabled={props.disabled}
      options={props.options}
      onChange={(value) => {
        props.onChange(value);
      }}
      value={rdValue && typeof rdValue !== 'undefined' ? rdValue.toString() : ''}
      optionType="button"
      buttonStyle="solid"
    />
  );
};

export default RadioGroupWidget;
