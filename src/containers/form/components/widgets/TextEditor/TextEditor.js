import React, { useEffect, useState, useRef } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import ImageResize from 'quill-image-resize-module-react';
import 'react-quill/dist/quill.snow.css';
import * as PropTypes from '@redux-saga/is';
import katex from 'katex';
import 'katex/dist/katex.min.css';
import './TextEditor.css';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import AutoLaTeX from 'react-autolatex';
import { Button, Modal, Tabs } from 'antd';
import { FileImageOutlined, FunctionOutlined, EditOutlined, CameraOutlined, EyeOutlined } from '@ant-design/icons';
import 'quill-paste-smart';

import icon1 from 'assets/images/math-icons/1.png';
import icon2 from 'assets/images/math-icons/2.png';
import icon3 from 'assets/images/math-icons/3.png';

import icon4 from 'assets/images/math-icons/4.png';
import icon5 from 'assets/images/math-icons/5.png';
import icon6 from 'assets/images/math-icons/6.png';

import icon7 from 'assets/images/math-icons/7.png';
import icon8 from 'assets/images/math-icons/8.png';
import icon9 from 'assets/images/math-icons/9.png';

import icon10 from 'assets/images/math-icons/10.png';
import icon11 from 'assets/images/math-icons/11.png';
import icon12 from 'assets/images/math-icons/12.png';

import icon13 from 'assets/images/math-icons/13.png';
import icon14 from 'assets/images/math-icons/14.png';
import icon15 from 'assets/images/math-icons/15.png';

import configs from 'configs';
window.katex = katex;
const { TabPane } = Tabs;
// Register the module
Quill.register('modules/imageResize', ImageResize);
const Block = Quill.import('blots/block');
Block.tagName = 'div';
Quill.register(Block);

const TextEditorWidget = (props) => {
  const [state, setState] = useState({
    editorHtml: props.value,
    theme: 'snow',
    isChanged: false,
  });

  const [viewable, setViewable] = useState(false);
  const [text, setText] = useState('');
  const [textHtml, setTextHtml] = useState('');

  const quillRef = useRef();

  const [media, setMedia] = useState({
    visible: false,
  });

  const [math, setMath] = useState({
    visible: false,
  });

  const handleChange = (html) => {
    setState({ ...state, editorHtml: html, isChanged: true });
  };

  const formats = ['header', 'font', 'color', 'align', 'size', 'bold', 'italic', 'underline', 'strike', 'blockquote', 'list', 'bullet', 'indent', 'link', 'image', 'video', 'formula', 'width'];

  const modules = {
    toolbar: [
      [{ header: '1' }, { header: '2' }, { font: [] }, { color: [] }, { align: [] }],
      [{ size: [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
      ['link', 'image', 'video', 'formula'],
      ['clean'],
    ],
    imageResize: {
      parchment: Quill.import('parchment'),
      modules: ['Resize', 'DisplaySize', 'Toolbar'],
    },
    clipboard: {
      matchVisual: false,
    },
  };

  const onSelectImage = (file) => {
    setMedia({
      ...media,
      visible: false,
    });
    const editor = quillRef.current.getEditor();
    const url = `${configs.UPLOAD_API_URL}/${file.tep_tin_url}`;
    editor.insertEmbed(editor.getSelection(), 'image', url);
    editor.pasteHTML(editor.getSelection(), <img src={url} class="exam-image" alt="exam image" />);
  };

  const setFormula = (type) => {
    if (type === 1) setText(`$^{2}$`);
    if (type === 2) setText(`$\\sqrt[n]{x}$`);
    if (type === 3) setText('$\\frac{x}{y}$');
    if (type === 4) setText('$x_{123}$');
    if (type === 5) setText('$\\leq$');
    if (type === 6) setText('$\\geq$');
    if (type === 7) setText('$\\neq$');
    if (type === 8) setText('$\\pi$');
    if (type === 9) setText('$\\alpha$');
    if (type === 10) setText('$\\left \\{ {{y=2} \\atop {x=2}} \\right.$');
    if (type === 11) setText('\\(\\left[ \\begin{array}{l}x=2\\\\x=-1\\end{array} \\right.\\)');
    if (type === 12) setText('$\\int\\limits^a_b {x} \\, dx$');
    if (type === 13) setText('$\\lim_{n \\to \\infty} a_n$');
    if (type === 14) setText('$\\left[\\begin{array}{ccc}1&2&3\\\\4&5&6\\\\7&8&9\\end{array}\\right]$');
  };

  useEffect(() => {
    setState({ ...state, editorHtml: props.value, isChanged: false });
    setViewable(false);
    setMedia({
      ...media,
      visible: false,
    });
  }, [props.value]);

  useEffect(() => {
    setViewable(false);
  }, [props.viewable]);

  useEffect(() => {
    if (text !== '') {
      setState({ ...state, editorHtml: `${state.editorHtml} ${text}`, isChanged: true });
      setText('');
    }
  }, [text]);

  if (viewable && !props.value && props.isOverlay) {
    return (
      <div
        className="viewable-editor editor-new"
        onClick={() => {
          if (!props.disabled) {
            setViewable(false);
            props.openEditor(true);
          }
        }}
      >
        <span>{props.placeholder}</span>
      </div>
    );
  }

  if (viewable && props.value && props.value.trim() !== '' && props.value.trim() !== '<div><br></div>') {
    return (
      <div className="viewable-editor">
        <div className="viewable-content">
          <AutoLaTeX>{props.value}</AutoLaTeX>
        </div>
        <Button
          type="link"
          size="small"
          onClick={() => {
            if (!props.disabled) {
              setViewable(false);
              props.openEditor(true);
            }
          }}
        >
          <EditOutlined title="Chỉnh sửa" />
        </Button>
      </div>
    );
  }

  return (
    <div
      className={`text-quill-custom 
        ${props.isOverlay ? 'overlay-editor' : ''} 
        ${props.isSimple ? 'simple-editor' : ''} 
        ${props.showToolbar ? 'show-toolbar-editor' : ''} 
        ${props.isMinHeight200 ? 'min-height-200' : ''}
        ${props.isMinHeight300 ? 'min-height-300' : ''}
        ${props.isMinHeight500 ? 'min-height-500' : ''}
      `}
      onMouseLeave={() => {
        props.onChange(state.editorHtml);
      }}
    >
      {props.isOverlay && (
        <div className="toolbar-extra-actions">
          <Button
            className="add-image"
            type="dash"
            size="small"
            icon={<FileImageOutlined />}
            onClick={() =>
              setMedia({
                ...media,
                visible: true,
              })
            }
          >
            Thêm hình ảnh
          </Button>
          <Button
            className="add-image"
            type="dash"
            size="small"
            icon={<FileImageOutlined />}
            onClick={() =>
              setMath({
                ...math,
                visible: !math.visible,
              })
            }
          >
            Thêm công thức
          </Button>
        </div>
      )}
      {!props.disabled && (
        <ReactQuill
          ref={quillRef}
          theme={state.theme}
          onChange={handleChange}
          value={state.editorHtml || ''}
          formats={formats}
          modules={modules}
          bounds={'.app'}
          disabled={props.disabled}
          placeholder={props.placeholder}
        />
      )}

      {props.isSimple && !props.disabled && (
        <div className="simple-extra-actions">
          <Button
            className="add-image"
            type="link"
            size="small"
            title="Chèn hình ảnh"
            onClick={() =>
              setMedia({
                ...media,
                visible: true,
              })
            }
          >
            <CameraOutlined />
          </Button>
          <Button
            className={math.visible ? 'active add-image' : 'add-image'}
            type="link"
            title="Chèn công thức"
            size="small"
            onClick={() =>
              setMath({
                ...math,
                visible: !math.visible,
              })
            }
          >
            <FunctionOutlined />
          </Button>
          {props.value && props.value.trim() !== '' && props.value.trim() !== '<div><br></div>' && (
            <Button
              className={viewable ? 'active add-image' : 'add-image'}
              type="link"
              title="Chế độ xem"
              size="small"
              onClick={() => {
                setViewable(!viewable);
              }}
            >
              <EyeOutlined />
            </Button>
          )}
        </div>
      )}
      {props.isOverlay && (
        <div className="footer-editor">
          <Button
            type="dash"
            danger
            ghost
            onClick={() => {
              props.openEditor(false);
              setViewable(false);
            }}
          >
            Hủy bỏ
          </Button>
          <Button
            type="primary"
            onClick={() => {
              props.onChange(state.editorHtml);
              props.openEditor(false);
              setViewable(false);
            }}
          >
            Cập nhật
          </Button>
        </div>
      )}

      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={media.visible}
        onCancel={() => {
          setMedia({
            ...media,
            visible: false,
          });
        }}
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>

      {(math.visible || props.disabled) && (
        <div className="math-area">
          {math.visible && (
            <div className="math-options">
              <div className="body-math">
                <Tabs defaultActiveKey="1">
                  <TabPane tab="Công thức toán" key="1">
                    <img src={icon1} onClick={() => setFormula(1)} />
                    <img src={icon2} onClick={() => setFormula(2)} />
                    <img src={icon3} onClick={() => setFormula(3)} />
                    <img src={icon4} onClick={() => setFormula(4)} />
                    <img src={icon5} onClick={() => setFormula(5)} />
                    <img src={icon6} onClick={() => setFormula(6)} />
                    <img src={icon7} onClick={() => setFormula(7)} />
                    <img src={icon8} onClick={() => setFormula(8)} />
                    <img src={icon9} onClick={() => setFormula(9)} />
                    <img src={icon10} onClick={() => setFormula(10)} />
                    <img src={icon11} onClick={() => setFormula(11)} />
                    <img src={icon12} onClick={() => setFormula(12)} />
                    <img src={icon13} onClick={() => setFormula(13)} />
                    <img src={icon14} onClick={() => setFormula(14)} />
                  </TabPane>
                  <TabPane tab="Ký tự đặc biệt" key="2">
                    <ul className="math-symbols">
                      <li onClick={() => setText('²')}>
                        <span>²</span>
                      </li>
                      <li onClick={() => setText('³')}>
                        <span>³</span>
                      </li>
                      <li onClick={() => setText('√')}>
                        <span>√</span>
                      </li>
                      <li onClick={() => setText('∛')}>
                        <span>∛</span>
                      </li>
                      <li onClick={() => setText('·')}>
                        <span>·</span>
                      </li>
                      <li onClick={() => setText('×')}>
                        <span>×</span>
                      </li>
                      <li onClick={() => setText('÷')}>
                        <span>÷</span>
                      </li>
                      <li onClick={() => setText('±')}>
                        <span>±</span>
                      </li>
                      <li onClick={() => setText('≈')}>
                        <span>≈</span>
                      </li>
                      <li onClick={() => setText('≤')}>
                        <span>≤</span>
                      </li>
                      <li onClick={() => setText('≥')}>
                        <span>≥</span>
                      </li>
                      <li onClick={() => setText('≡')}>
                        <span>≡</span>
                      </li>
                      <li onClick={() => setText('⇒')}>
                        <span>⇒</span>
                      </li>
                      <li onClick={() => setText('⇔')}>
                        <span>⇔</span>
                      </li>
                      <li onClick={() => setText('∈')}>
                        <span>∈</span>
                      </li>
                      <li onClick={() => setText('∉')}>
                        <span>∉</span>
                      </li>
                      <li onClick={() => setText('∧')}>
                        <span>∧</span>
                      </li>
                      <li onClick={() => setText('∨')}>
                        <span>∨</span>
                      </li>
                      <li onClick={() => setText('∞')}>
                        <span>∞</span>
                      </li>
                      <li onClick={() => setText('Δ')}>
                        <span>Δ</span>
                      </li>
                      <li onClick={() => setText('π')}>
                        <span>π</span>
                      </li>
                      <li onClick={() => setText('Ф')}>
                        <span>Ф</span>
                      </li>
                      <li onClick={() => setText('ω')}>
                        <span>ω</span>
                      </li>
                      <li onClick={() => setText('↑')}>
                        <span>↑</span>
                      </li>
                      <li onClick={() => setText('↓')}>
                        <span>↓</span>
                      </li>
                      <li onClick={() => setText('∵')}>
                        <span>∵</span>
                      </li>
                      <li onClick={() => setText('∴')}>
                        <span>∴</span>
                      </li>
                      <li onClick={() => setText('↔')}>
                        <span>↔</span>
                      </li>
                      <li onClick={() => setText('→')}>
                        <span>→</span>
                      </li>
                      <li onClick={() => setText('←')}>
                        <span>←</span>
                      </li>
                      <li onClick={() => setText('⇵')}>
                        <span>⇵</span>
                      </li>
                      <li onClick={() => setText('⇅')}>
                        <span>⇅</span>
                      </li>
                      <li onClick={() => setText('⇄')}>
                        <span>⇄</span>
                      </li>
                      <li onClick={() => setText('⇆')}>
                        <span>⇆</span>
                      </li>
                      <li onClick={() => setText('∫')}>
                        <span>∫</span>
                      </li>
                      <li onClick={() => setText('∑')}>
                        <span>∑</span>
                      </li>
                      <li onClick={() => setText('⊂')}>
                        <span>⊂</span>
                      </li>
                      <li onClick={() => setText('⊃')}>
                        <span>⊃</span>
                      </li>
                      <li onClick={() => setText('⊆')}>
                        <span>⊆</span>
                      </li>
                      <li onClick={() => setText('⊇')}>
                        <span>⊇</span>
                      </li>
                      <li onClick={() => setText('⊄')}>
                        <span>⊄</span>
                      </li>
                      <li onClick={() => setText('⊅')}>
                        <span>⊅</span>
                      </li>
                      <li onClick={() => setText('∀')}>
                        <span>∀</span>
                      </li>
                      <li onClick={() => setText('∠')}>
                        <span>∠</span>
                      </li>
                      <li onClick={() => setText('∡')}>
                        <span>∡</span>
                      </li>
                      <li onClick={() => setText('⊥')}>
                        <span>⊥</span>
                      </li>
                      <li onClick={() => setText('∪')}>
                        <span>∪</span>
                      </li>
                      <li onClick={() => setText('∩')}>
                        <span>∩</span>
                      </li>
                      <li onClick={() => setText('∅')}>
                        <span>∅</span>
                      </li>
                      <li onClick={() => setText('¬')}>
                        <span>¬</span>
                      </li>
                      <li onClick={() => setText('⊕')}>
                        <span>⊕</span>
                      </li>
                      <li onClick={() => setText('║')}>
                        <span>║</span>
                      </li>
                      <li onClick={() => setText('∦')}>
                        <span>∦</span>
                      </li>
                      <li onClick={() => setText('∝')}>
                        <span>∝</span>
                      </li>
                      <li onClick={() => setText('㏒')}>
                        <span>㏒</span>
                      </li>
                      <li onClick={() => setText('㏑')}>
                        <span>㏑</span>
                      </li>
                    </ul>
                  </TabPane>
                  <TabPane tab="Phiên âm tiếng anh" key="3">
                    <ul className="math-symbols">
                      <li onClick={() => setText('i:')}>
                        <span>i:</span>
                      </li>
                      <li onClick={() => setText('ɪ')}>
                        <span>ɪ</span>
                      </li>
                      <li onClick={() => setText('ʊ')}>
                        <span>ʊ</span>
                      </li>
                      <li onClick={() => setText('u:')}>
                        <span>u:</span>
                      </li>
                      <li onClick={() => setText('ɪə')}>
                        <span>ɪə</span>
                      </li>
                      <li onClick={() => setText('eɪ')}>
                        <span>eɪ</span>
                      </li>
                      <li onClick={() => setText('e')}>
                        <span>e</span>
                      </li>
                      <li onClick={() => setText('ə')}>
                        <span>ə</span>
                      </li>
                      <li onClick={() => setText('3:')}>
                        <span>3:</span>
                      </li>
                      <li onClick={() => setText('ɔ:')}>
                        <span>ɔ:</span>
                      </li>
                      <li onClick={() => setText('ʊə')}>
                        <span>ʊə</span>
                      </li>
                      <li onClick={() => setText('ɔɪ')}>
                        <span>ɔɪ</span>
                      </li>
                      <li onClick={() => setText('əʊ')}>
                        <span>əʊ</span>
                      </li>
                      <li onClick={() => setText('æ')}>
                        <span>æ</span>
                      </li>
                      <li onClick={() => setText('ʌ')}>
                        <span>ʌ</span>
                      </li>
                      <li onClick={() => setText('ɑ:')}>
                        <span>ɑ:</span>
                      </li>
                      <li onClick={() => setText('ɒ')}>
                        <span>ɒ</span>
                      </li>
                      <li onClick={() => setText('eə')}>
                        <span>eə</span>
                      </li>
                      <li onClick={() => setText('aɪ')}>
                        <span>aɪ</span>
                      </li>
                      <li onClick={() => setText('aʊ')}>
                        <span>aʊ</span>
                      </li>
                      <li onClick={() => setText('p')}>
                        <span>p</span>
                      </li>
                      <li onClick={() => setText('b')}>
                        <span>b</span>
                      </li>
                      <li onClick={() => setText('t')}>
                        <span>t</span>
                      </li>
                      <li onClick={() => setText('d')}>
                        <span>d</span>
                      </li>
                      <li onClick={() => setText('tʃ')}>
                        <span>tʃ</span>
                      </li>
                      <li onClick={() => setText('dʒ')}>
                        <span>dʒ</span>
                      </li>
                      <li onClick={() => setText('k')}>
                        <span>k</span>
                      </li>
                      <li onClick={() => setText('g')}>
                        <span>g</span>
                      </li>
                      <li onClick={() => setText('f')}>
                        <span>f</span>
                      </li>
                      <li onClick={() => setText('v')}>
                        <span>v</span>
                      </li>
                      <li onClick={() => setText('θ')}>
                        <span>θ</span>
                      </li>
                      <li onClick={() => setText('ð')}>
                        <span>ð</span>
                      </li>
                      <li onClick={() => setText('s')}>
                        <span>s</span>
                      </li>
                      <li onClick={() => setText('z')}>
                        <span>z</span>
                      </li>
                      <li onClick={() => setText('ʃ')}>
                        <span>ʃ</span>
                      </li>
                      <li onClick={() => setText('ʒ')}>
                        <span>ʒ</span>
                      </li>
                      <li onClick={() => setText('m')}>
                        <span>m</span>
                      </li>
                      <li onClick={() => setText('n')}>
                        <span>n</span>
                      </li>
                      <li onClick={() => setText('ŋ')}>
                        <span>ŋ</span>
                      </li>
                      <li onClick={() => setText('h')}>
                        <span>h</span>
                      </li>
                      <li onClick={() => setText('l')}>
                        <span>l</span>
                      </li>
                      <li onClick={() => setText('r')}>
                        <span>r</span>
                      </li>
                      <li onClick={() => setText('w')}>
                        <span>w</span>
                      </li>
                      <li onClick={() => setText('j')}>
                        <span>j</span>
                      </li>
                    </ul>
                  </TabPane>
                </Tabs>
              </div>
            </div>
          )}

          <div className="form-math">
            <div className="math-item">
              <AutoLaTeX>{state.editorHtml}</AutoLaTeX>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default TextEditorWidget;
