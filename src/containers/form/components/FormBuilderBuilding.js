// import external libs
import React, { useEffect } from 'react';
import { get } from 'lodash';
import { connect } from 'react-redux';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import lodash from 'lodash';
import { Button, Form, Input, InputNumber, Row, Col, Select, Modal, Switch, List, Collapse } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

// import internal libs
import constants from 'constants/global.constants';
import { getLangText } from 'helpers/language.helper';
import * as formAction from 'redux/actions/form';
import * as userAction from 'redux/actions/user';
import TagsOption from './fields/TagsOption';

const { Option, OptGroup } = Select;
const { TextArea } = Input;
const { Panel } = Collapse;

function FormBuilderBuilding(props) {
  const handleOnDragEnd = (result) => {
    if (!result.destination) return;
    const items = Array.from(props.listFields);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    props.changeField('listFields', items);
  };

  const renderSubFields = (indexParent) => {
    return (
      <>
        <DragDropContext onDragEnd={(result) => handleOnDragEnd(result)}>
          <Droppable droppableId="fieldId">
            {(provided) => (
              <div className="characters" {...provided.droppableProps} ref={provided.innerRef}>
                <List
                  locale={{
                    emptyText: getLangText('GLOBAL.NO_ITEMS'),
                  }}
                  header={
                    <Row>
                      <Col span={4} className="text-left">
                        Thứ tự
                      </Col>
                      <Col span={8} className="text-left">
                        Tiêu đề nhãn
                      </Col>
                      <Col span={8} className="text-left">
                        Tên trường
                      </Col>
                      <Col span={4} className="text-left">
                        Kiểu dữ liệu
                      </Col>
                    </Row>
                  }
                  footer={null}
                  dataSource={props.listFields && props.listFields[indexParent] && props.listFields[indexParent].subs ? props.listFields[indexParent].subs : []}
                  renderItem={(item, index) => (
                    <Draggable key={item.id} draggableId={item.id} index={index}>
                      {(provided) => (
                        <List.Item key={item.id} className={item.type === 'label' ? 'label-panel-item' : ''}>
                          <Collapse defaultActiveKey={[]} expandIconPosition="right" ghost={true} collapsible="icon">
                            <Panel
                              header={
                                <Row className="full">
                                  <Col span={4} className="text-left">
                                    <span className="nbr" ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                      {index + 1}
                                    </span>
                                  </Col>
                                  <Col span={8} className="text-left">
                                    <span className="label-text">{item.label}</span>
                                    <div className="actions">
                                      <Button onClick={(e) => e.preventDefault()} className="edit act" type="link">
                                        Sửa
                                      </Button>
                                      <Button
                                        onClick={(e) => {
                                          e.stopPropagation();
                                          Modal.confirm({
                                            title: 'Bạn có chắc muốn xóa không?',
                                            iconType: 'close-circle',
                                            okText: 'Có',
                                            cancelText: 'Không',
                                            onOk() {
                                              props.removeFormField(index, indexParent);
                                            },
                                          });
                                        }}
                                        className="delete act"
                                        type="link"
                                      >
                                        {getLangText('GLOBAL.DELETE')}
                                      </Button>

                                      <Button
                                        className="view act"
                                        type="link"
                                        onClick={(e) => {
                                          e.stopPropagation();
                                          props.cloneFormField(item, index, indexParent);
                                        }}
                                      >
                                        Nhân bản
                                      </Button>
                                    </div>
                                  </Col>
                                  <Col span={8} className="text-left">
                                    {item.name}
                                  </Col>
                                  <Col span={4} className="text-left">
                                    {item.type}
                                  </Col>
                                </Row>
                              }
                              key="1"
                            >
                              <Row className="full field-setting">
                                <Col span={4} className="field-label">
                                  <span className="label-txt">Tiêu đề nhãn</span>
                                </Col>
                                <Col span={20} className="field-value">
                                  <Form.Item className="input-col">
                                    <Input
                                      placeholder="Nhập tiêu đề nhãn"
                                      defaultValue={props.listFields[indexParent].subs[index].label}
                                      onBlur={(e) => {
                                        const fields = props.listFields;
                                        const currentSub = {
                                          ...props.listFields[indexParent].subs[index],
                                          label: e.target.value,
                                        };
                                        const subs = props.listFields[indexParent].subs;
                                        subs[index] = currentSub;

                                        fields[indexParent] = {
                                          ...fields[indexParent],
                                          subs,
                                        };
                                        props.changeField('listFields', fields);
                                      }}
                                    />
                                  </Form.Item>
                                </Col>
                                <Col span={4} className="field-label">
                                  <span className="label-txt">Tên trường dữ liệu</span>
                                </Col>
                                <Col span={20} className="field-value">
                                  <Form.Item className="input-col">
                                    <Input
                                      placeholder="Nhập tên trường dữ liệu"
                                      defaultValue={props.listFields[indexParent].subs[index].name}
                                      onBlur={(e) => {
                                        const fields = props.listFields;
                                        const currentSub = {
                                          ...props.listFields[indexParent].subs[index],
                                          name: e.target.value,
                                        };
                                        const subs = props.listFields[indexParent].subs;
                                        subs[index] = currentSub;

                                        fields[indexParent] = {
                                          ...fields[indexParent],
                                          subs,
                                        };
                                        props.changeField('listFields', fields);
                                      }}
                                    />
                                  </Form.Item>
                                </Col>
                                <Col span={4} className="field-label">
                                  <span className="label-txt">Kiểu trường dữ liệu</span>
                                </Col>
                                <Col span={20} className="field-value">
                                  <Form.Item className="input-col">{renderTypes(index, indexParent)}</Form.Item>
                                </Col>
                                <Col span={4} className="field-label">
                                  <span className="label-txt">Độ rộng</span>
                                </Col>
                                <Col span={20} className="field-value">
                                  <Form.Item className="input-col left-align">{renderColSpan(index, indexParent)}</Form.Item>
                                </Col>

                                <Col span={4} className="field-label">
                                  <span className="label-txt">Hướng dẫn</span>
                                </Col>
                                <Col span={20} className="field-value">
                                  <Form.Item className="input-col">
                                    <TextArea
                                      rows={6}
                                      defaultValue={props.listFields[indexParent].subs[index].instruction}
                                      onBlur={(e) => {
                                        const fields = props.listFields;
                                        const currentSub = {
                                          ...props.listFields[indexParent].subs[index],
                                          instruction: e.target.value,
                                        };
                                        const subs = props.listFields[indexParent].subs;
                                        subs[index] = currentSub;

                                        fields[indexParent] = {
                                          ...fields[indexParent],
                                          subs,
                                        };
                                        props.changeField('listFields', fields);
                                      }}
                                      placeholder="Nhập hướng dẫn cho quản trị viên"
                                    />
                                  </Form.Item>
                                </Col>
                                <Col span={4} className="field-label">
                                  <span className="label-txt">Trường bắt buộc</span>
                                </Col>
                                <Col span={20} className="field-value">
                                  <Form.Item className="input-col">
                                    <Switch
                                      checkedChildren="Có"
                                      unCheckedChildren="Không"
                                      checked={props.listFields[indexParent].subs[index].require}
                                      onChange={(checked) => {
                                        const fields = props.listFields;
                                        const currentSub = {
                                          ...props.listFields[indexParent].subs[index],
                                          require: checked,
                                        };
                                        const subs = props.listFields[indexParent].subs;
                                        subs[index] = currentSub;

                                        fields[indexParent] = {
                                          ...fields[indexParent],
                                          subs,
                                        };
                                        props.changeField('listFields', fields);
                                      }}
                                    />
                                  </Form.Item>
                                </Col>
                                <Col span={4} className="field-label">
                                  <span className="label-txt">Không cho sửa</span>
                                </Col>
                                <Col span={20} className="field-value">
                                  <Form.Item className="input-col">
                                    <Switch
                                      checkedChildren="Có"
                                      unCheckedChildren="Không"
                                      checked={props.listFields[indexParent].subs[index].disableEdit}
                                      onChange={(checked) => {
                                        const fields = props.listFields;
                                        const currentSub = {
                                          ...props.listFields[indexParent].subs[index],
                                          disableEdit: checked,
                                        };
                                        const subs = props.listFields[indexParent].subs;
                                        subs[index] = currentSub;

                                        fields[indexParent] = {
                                          ...fields[indexParent],
                                          subs,
                                        };
                                        props.changeField('listFields', fields);
                                      }}
                                    />
                                  </Form.Item>
                                </Col>
                                {renderMetaFields(item, index, indexParent)}
                                {/* <Col span={4} className="field-label">
                                  <span className="label-txt">Điều kiện xuất hiện</span>
                                </Col> */}
                                {/* <Col span={20} className="field-value">
                                  <Form.Item className="input-col">
                                    <Switch
                                      checkedChildren="Có"
                                      unCheckedChildren="Không"
                                      defaultChecked={props.listFields[index].hasCondition}
                                      onChange={hasCondition => {
                                        const fields = props.listFields;
                                        fields[index] = {
                                          ...fields[index],
                                          hasCondition,
                                        };
                                        props.changeField('listFields', fields);
                                      }}
                                    />
                                    {renderConditions(item, index)}
                                  </Form.Item>
                                </Col> */}
                              </Row>
                            </Panel>
                          </Collapse>
                        </List.Item>
                      )}
                    </Draggable>
                  )}
                />
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
        <div className="add-sub-field">
          <Button type="dashed" icon={<PlusOutlined />} onClick={() => props.addFormField(indexParent)}>
            Thêm trường
          </Button>
        </div>
      </>
    );
  };

  const renderColSpan = (index, indexParent) => {
    let defaultColSpan = props.so_cot ? props.so_cot : 1;
    if (indexParent) {
      if (props.listFields[indexParent] && props.listFields[indexParent].subs[index] && props.listFields[indexParent].subs[index].colSpan) {
        defaultColSpan = Number(props.listFields[indexParent].subs[index].colSpan);
      }
    } else {
      if (props.listFields[index] && props.listFields[index].colSpan) {
        defaultColSpan = Number(props.listFields[index].colSpan);
      }
    }

    return (
      <InputNumber
        min={1}
        max={props.so_cot}
        defaultValue={defaultColSpan}
        onChange={(value) => {
          const fields = props.listFields;
          if (indexParent) {
            const currentSub = {
              ...props.listFields[indexParent].subs[index],
              colSpan: value,
            };
            const subs = props.listFields[indexParent].subs;
            subs[index] = currentSub;

            fields[indexParent] = {
              ...fields[indexParent],
              subs,
            };
          } else {
            fields[index] = {
              ...fields[index],
              colSpan: value,
            };
          }

          props.changeField('listFields', fields);
        }}
      />
    );
  };

  const renderTypes = (index, indexParent) => {
    let types = constants.FIELD_TYPES;
    if (indexParent) {
      types = constants.FIELD_TYPES.filter((item) => item.isSupportRepeater).map((item) => {
        return {
          ...item,
          items: item.items.filter((it) => it.isSupportRepeater),
        };
      });
    }
    const options = types.map((type, idx) => (
      <OptGroup label={type.name} key={idx}>
        {type.items.map((subField) => (
          <Option key={subField.key} value={subField.key}>
            {subField.name}
          </Option>
        ))}
      </OptGroup>
    ));
    return (
      <Select
        onChange={(type) => {
          const fields = props.listFields;
          if (indexParent) {
            const currentSub = {
              ...props.listFields[indexParent].subs[index],
              type,
            };
            const subs = props.listFields[indexParent].subs;
            subs[index] = currentSub;

            fields[indexParent] = {
              ...fields[indexParent],
              subs,
            };
          } else {
            fields[index] = {
              ...fields[index],
              type,
            };
          }

          props.changeField('listFields', fields);
        }}
        value={indexParent ? props.listFields[indexParent].subs[index].type : props.listFields[index].type}
      >
        {options}
      </Select>
    );
  };

  const renderConditions = (field, index) => {
    if (!field.hasCondition) return null;
    const conditions = get(field, 'conditions', []);
    if (conditions.length === 0) {
      conditions.push({
        field: '',
        operator: 'c1',
        value: '',
        type: 'and',
      });
    }

    const fieldSelect = (currentCondition, idx) => {
      const options = props.listFields.map((f, i) => {
        return (
          <Option key={i} disabled={field.name === f.name} value={f.name}>
            {f.label}
          </Option>
        );
      });
      return (
        <Col span={6}>
          <Select
            defaultValue={currentCondition.field}
            onChange={(val) => {
              const fields = props.listFields;
              conditions[idx] = { ...conditions[idx], field: val };
              fields[index] = {
                ...fields[index],
                conditions,
              };
              props.changeField('listFields', fields);
            }}
          >
            {options}
          </Select>
        </Col>
      );
    };
    const dataConditions = conditions.map((con, idx) => {
      return (
        <div key={idx}>
          <Row gutter={[10, 30]}>
            {fieldSelect(con, idx)}
            <Col span={6}>
              <Select
                defaultValue={con.operator}
                onChange={(val) => {
                  const fields = props.listFields;
                  conditions[idx] = { ...conditions[idx], operator: val };
                  fields[index] = {
                    ...fields[index],
                    conditions,
                  };
                  props.changeField('listFields', fields);
                }}
              >
                <Option value="c1">Có giá trị</Option>
                <Option value="c2">Không có giá trị</Option>
                <Option value="c3">Giá trị bằng</Option>
                <Option value="c4">Giá trị khác</Option>
                <Option value="c5">Giá trị lớn hơn</Option>
                <Option value="c6">Giá trị nhỏ hơn</Option>
              </Select>
            </Col>
            <Col span={5}>
              <Input
                defaultValue={con.value}
                onChange={(e) => {
                  const fields = props.listFields;
                  conditions[idx] = { ...conditions[idx], value: e.target.value };
                  fields[index] = {
                    ...fields[index],
                    conditions,
                  };
                  props.changeField('listFields', fields);
                }}
                placeholder="Giá trị"
                disabled={idx === 0 && (con.operator === 'c1' || con.operator === 'c2')}
              />
            </Col>
            {/* <Col span={5}>
              <Select
                defaultValue="and"
                onChange={val => {
                  const fields = props.listFields;
                  conditions[idx] = { ...conditions[idx], type: val };
                  fields[index] = {
                    ...fields[index],
                    conditions,
                  };
                  props.changeField('listFields', fields);
                }}
              >
                <Option value="and">Và</Option>
                <Option value="or">Hoặc</Option>
              </Select>
            </Col> */}
            <Col span={2}>
              {idx > 0 && (
                <Button
                  block
                  type="danger"
                  onClick={() => {
                    const fields = props.listFields;
                    conditions.splice(idx, 1);
                    fields[index] = {
                      ...fields[index],
                      conditions,
                    };
                    props.changeField('listFields', fields);
                  }}
                >
                  Xóa
                </Button>
              )}
            </Col>
          </Row>
        </div>
      );
    });
    return (
      <>
        <br />
        <h4 className="field-value">Hiển thị nếu: </h4>
        <Row gutter={[10, 30]} className="condition-items">
          <Col span={24}>
            {dataConditions}
            {/* <Row gutter={[10, 30]}>
              {' '}
              <Col span={4}>
                <Button
                  type="primary"
                  onClick={() => {
                    const fields = props.listFields;
                    fields[index] = {
                      ...fields[index],
                      conditions: conditions.concat({
                        field: '',
                        operator: 'c1',
                        value: '',
                        type: 'and',
                      }),
                    };
                    props.changeField('listFields', fields);
                  }}
                >
                  Thêm điều kiện
                </Button>
              </Col>
            </Row> */}
          </Col>
        </Row>
      </>
    );
  };
  const renderMetaFields = (field, index, indexParent) => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_MODULE);
    const configModule = config ? JSON.parse(config.du_lieu) : {};
    const modules = configModule.modules ? configModule.modules : [];

    const userCates = get(props, 'user.listCates.result.data', []);
    const formCates = get(props, 'form.listCates.result.data', []);
    const tableList = get(props, 'table.listCates.result.data', []).filter((item) => item.loai_bang === 'category');
    const columnList = get(props, 'table.list.result.data', []);

    const metaFieldGroup = constants.FIELD_TYPES.map((item) => item.items);

    const metaField = lodash.flatten(metaFieldGroup).find((fieldItem) => fieldItem.key === field.type);
    if (metaField && metaField.metaFields) {
      return metaField.metaFields.map((metaField, idx) => (
        <React.Fragment key={idx}>
          <Col span={4} className="field-label">
            <span className="label-txt">{metaField.title}</span>
          </Col>
          {!metaField.type ||
            (metaField.type === 'text' && (
              <Col span={20} className="field-value">
                <Form.Item className="input-col">
                  <Input
                    placeholder={metaField.placeholder ? metaField.placeholder : `Vui lòng nhập ${metaField.title.toLowerCase()}`}
                    defaultValue={
                      indexParent
                        ? get(props, `listFields[${indexParent}].subs[${index}].meta[${metaField.name}]`, metaField.default_value)
                        : get(props, 'listFields[${index}].meta[${metaField.name}]', metaField.default_value)
                    }
                    onBlur={(e) => {
                      const fields = props.listFields;
                      if (indexParent) {
                        const currentSub = {
                          ...props.listFields[indexParent].subs[index],
                          meta: {
                            ...props.listFields[indexParent].subs[index].meta,
                            [metaField.name]: e.target.value,
                          },
                        };
                        const subs = props.listFields[indexParent].subs;
                        subs[index] = currentSub;

                        fields[indexParent] = {
                          ...fields[indexParent],
                          subs,
                        };
                      } else {
                        fields[index] = {
                          ...fields[index],
                          meta: {
                            ...props.listFields[index].meta,
                            [metaField.name]: e.target.value,
                          },
                        };
                      }

                      props.changeField('listFields', fields);
                    }}
                  />
                </Form.Item>
              </Col>
            ))}
          {metaField.type === 'textarea' && (
            <Col span={20} className="field-value">
              <Form.Item className="input-col">
                <TextArea
                  placeholder={`Vui lòng nhập ${metaField.title.toLowerCase()}`}
                  defaultValue={
                    indexParent
                      ? get(props, `listFields[${indexParent}].subs[${index}].meta[${metaField.name}]`, metaField.default_value)
                      : get(props, 'listFields[${index}].meta[${metaField.name}]', metaField.default_value)
                  }
                  onBlur={(e) => {
                    const fields = props.listFields;
                    if (indexParent) {
                      const currentSub = {
                        ...props.listFields[indexParent].subs[index],
                        meta: {
                          ...props.listFields[indexParent].subs[index].meta,
                          [metaField.name]: e.target.value,
                        },
                      };
                      const subs = props.listFields[indexParent].subs;
                      subs[index] = currentSub;

                      fields[indexParent] = {
                        ...fields[indexParent],
                        subs,
                      };
                    } else {
                      fields[index] = {
                        ...fields[index],
                        meta: {
                          ...props.listFields[index].meta,
                          [metaField.name]: e.target.value,
                        },
                      };
                    }
                    props.changeField('listFields', fields);
                  }}
                />
              </Form.Item>
            </Col>
          )}
          {metaField.type === 'select' && (
            <Col span={20} className="field-value">
              <Form.Item className="input-col">
                <Select
                  placeholder={`Vui lòng chọn ${metaField.title.toLowerCase()}`}
                  defaultValue={
                    indexParent
                      ? get(props, `listFields[${indexParent}].subs[${index}].meta[${metaField.name}]`, metaField.default_value)
                      : get(props, 'listFields[${index}].meta[${metaField.name}]', metaField.default_value)
                  }
                  onChange={(e) => {
                    const fields = props.listFields;
                    if (indexParent) {
                      const currentSub = {
                        ...props.listFields[indexParent].subs[index],
                        meta: {
                          ...props.listFields[indexParent].subs[index].meta,
                          [metaField.name]: e,
                        },
                      };
                      const subs = props.listFields[indexParent].subs;
                      subs[index] = currentSub;

                      fields[indexParent] = {
                        ...fields[indexParent],
                        subs,
                      };
                    } else {
                      fields[index] = {
                        ...fields[index],
                        meta: {
                          ...props.listFields[index].meta,
                          [metaField.name]: e,
                        },
                      };
                    }
                    props.changeField('listFields', fields);
                  }}
                >
                  {metaField.options.map((it) => (
                    <Option key={it.key} value={it.key}>
                      {it.value}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          )}
          {metaField.type === 'userCates' && (
            <Col span={20} className="field-value">
              <Form.Item className="input-col">
                <Select
                  mode="multiple"
                  placeholder={metaField.placeholder ? metaField.placeholder : `Vui lòng nhập ${metaField.title.toLowerCase()}`}
                  defaultValue={props.listFields[index].meta && props.listFields[index].meta[metaField.name] ? props.listFields[index].meta[metaField.name] : metaField.default_value}
                  onChange={(e) => {
                    const fields = props.listFields;
                    fields[index] = {
                      ...fields[index],
                      meta: {
                        ...props.listFields[index].meta,
                        [metaField.name]: e,
                      },
                    };
                    props.changeField('listFields', fields);
                  }}
                >
                  {userCates.map((it) => (
                    <Option key={it.nhom_nhan_vien_id} value={it.nhom_nhan_vien_id}>
                      {it.ten_nhom}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          )}
          {metaField.type === 'category' && (
            <Col span={20}>
              <Row>
                <Col span={24} className="field-value">
                  <Form.Item className="input-col">
                    <Select
                      mode="single"
                      placeholder={metaField.placeholder ? metaField.placeholder : `Vui lòng nhập ${metaField.title.toLowerCase()}`}
                      defaultValue={props.listFields[index].meta && props.listFields[index].meta[metaField.name] ? props.listFields[index].meta[metaField.name] : metaField.default_value}
                      onChange={(e) => {
                        const fields = props.listFields;
                        fields[index] = {
                          ...fields[index],
                          meta: {
                            ...props.listFields[index].meta,
                            [metaField.name]: e,
                          },
                        };
                        props.changeField('listFields', fields);
                      }}
                    >
                      {tableList.map((it) => (
                        <Option key={it.bangid} value={it.bangid}>
                          {it.tenbang}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={4} className="field-label">
                  <span className="label-txt">Trường khóa</span>
                </Col>
                <Col span={8} className="field-value">
                  <Form.Item className="input-col">
                    <Select
                      mode="single"
                      placeholder="Chọn trường khóa"
                      defaultValue={props.listFields[index].meta && props.listFields[index].meta[metaField.key_column] ? props.listFields[index].meta[metaField.key_column] : metaField.key_column}
                      onChange={(e) => {
                        const fields = props.listFields;
                        fields[index] = {
                          ...fields[index],
                          meta: {
                            ...props.listFields[index].meta,
                            [metaField.key_column]: e,
                          },
                        };
                        props.changeField('listFields', fields);
                      }}
                    >
                      {columnList
                        .filter((item) => item.bangid === (props.listFields[index].meta && props.listFields[index].meta[metaField.name] ? props.listFields[index].meta[metaField.name] : ''))
                        .map((it) => (
                          <Option key={it.csdl_tentruong} value={it.csdl_tentruong}>
                            {it.tentruong}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={4} className="field-label">
                  <span className="label-txt">Trường hiển thị</span>
                </Col>
                <Col span={8} className="field-value">
                  <Form.Item className="input-col">
                    <Select
                      mode="single"
                      placeholder="Chọn trường hiển thị"
                      defaultValue={
                        props.listFields[index].meta && props.listFields[index].meta[metaField.value_column] ? props.listFields[index].meta[metaField.value_column] : metaField.value_column
                      }
                      onChange={(e) => {
                        const fields = props.listFields;
                        fields[index] = {
                          ...fields[index],
                          meta: {
                            ...props.listFields[index].meta,
                            [metaField.value_column]: e,
                          },
                        };
                        props.changeField('listFields', fields);
                      }}
                    >
                      {columnList
                        .filter((item) => item.bangid === (props.listFields[index].meta && props.listFields[index].meta[metaField.name] ? props.listFields[index].meta[metaField.name] : ''))
                        .map((it) => (
                          <Option key={it.csdl_tentruong} value={it.csdl_tentruong}>
                            {it.tentruong}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          )}
          {metaField.type === 'formCates' && (
            <Col span={20} className="field-value">
              <Form.Item className="input-col">
                <Select
                  mode="multiple"
                  placeholder={metaField.placeholder ? metaField.placeholder : `Vui lòng nhập ${metaField.title.toLowerCase()}`}
                  defaultValue={props.listFields[index].meta && props.listFields[index].meta[metaField.name] ? props.listFields[index].meta[metaField.name] : metaField.default_value}
                  onChange={(e) => {
                    const fields = props.listFields;
                    fields[index] = {
                      ...fields[index],
                      meta: {
                        ...props.listFields[index].meta,
                        [metaField.name]: e,
                      },
                    };
                    props.changeField('listFields', fields);
                  }}
                >
                  {formCates.map((it) => (
                    <Option key={it.nhom_nhan_vien_id} value={it.nhom_nhan_vien_id}>
                      {it.ten_nhom}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          )}
          {metaField.type === 'module' && (
            <Col span={20} className="field-value">
              <Form.Item className="input-col">
                <Select
                  mode="single"
                  placeholder={metaField.placeholder ? metaField.placeholder : `Vui lòng nhập ${metaField.title.toLowerCase()}`}
                  defaultValue={props.listFields[index].meta && props.listFields[index].meta[metaField.name] ? props.listFields[index].meta[metaField.name] : metaField.default_value}
                  onChange={(e) => {
                    const fields = props.listFields;
                    fields[index] = {
                      ...fields[index],
                      meta: {
                        ...props.listFields[index].meta,
                        [metaField.name]: e,
                      },
                    };
                    props.changeField('listFields', fields);
                  }}
                >
                  {modules.map((it) => (
                    <Option key={it.module_id} value={it.module_id}>
                      {it.module_name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          )}
          {metaField.type === 'number' && (
            <Col span={20} className="field-value">
              <Form.Item className="input-col">
                <Input
                  placeholder={`Vui lòng nhập ${metaField.title.toLowerCase()}`}
                  type="number"
                  min={metaField.min}
                  max={metaField.max}
                  defaultValue={
                    indexParent
                      ? get(props, `listFields[${indexParent}].subs[${index}].meta[${metaField.name}]`, metaField.default_value)
                      : get(props, 'listFields[${index}].meta[${metaField.name}]', metaField.default_value)
                  }
                  onBlur={(e) => {
                    const fields = props.listFields;
                    if (indexParent) {
                      const currentSub = {
                        ...props.listFields[indexParent].subs[index],
                        meta: {
                          ...props.listFields[indexParent].subs[index].meta,
                          [metaField.name]: e.target.value,
                        },
                      };
                      const subs = props.listFields[indexParent].subs;
                      subs[index] = currentSub;

                      fields[indexParent] = {
                        ...fields[indexParent],
                        subs,
                      };
                    } else {
                      fields[index] = {
                        ...fields[index],
                        meta: {
                          ...props.listFields[index].meta,
                          [metaField.name]: e.target.value,
                        },
                      };
                    }
                    props.changeField('listFields', fields);
                  }}
                />
              </Form.Item>
            </Col>
          )}
          {metaField.type === 'switch' && (
            <Col span={20} className="field-value">
              <Form.Item className="input-col">
                <Switch
                  checkedChildren="Có"
                  unCheckedChildren="Không"
                  defaultValue={
                    indexParent
                      ? get(props, `listFields[${indexParent}].subs[${index}].meta[${metaField.name}]`, metaField.default_value)
                      : get(props, 'listFields[${index}].meta[${metaField.name}]', metaField.default_value)
                  }
                  onChange={(checked) => {
                    const fields = props.listFields;
                    fields[index] = {
                      ...fields[index],
                      meta: {
                        ...props.listFields[index].meta,
                        [metaField.name]: checked,
                      },
                    };
                  }}
                />
              </Form.Item>
            </Col>
          )}
          {metaField.type === 'options' && (
            <Col span={18} className="field-value field-options">
              <Form.Item className="input-col text-left" extra="Cú pháp: value: lable. Ví dụ: 1:Đúng, 2:Sai">
                <TagsOption
                  options={
                    indexParent
                      ? get(props, `listFields[${indexParent}].subs[${index}].meta[${metaField.name}]`, metaField.default_value)
                      : get(props, `listFields[${index}].meta[${metaField.name}]`, metaField.default_value)
                  }
                  onChange={(options) => {
                    const fields = props.listFields;
                    if (indexParent) {
                      const currentSub = {
                        ...props.listFields[indexParent].subs[index],
                        meta: {
                          ...props.listFields[indexParent].subs[index].meta,
                          [metaField.name]: options,
                        },
                      };
                      const subs = props.listFields[indexParent].subs;
                      subs[index] = currentSub;

                      fields[indexParent] = {
                        ...fields[indexParent],
                        subs,
                      };
                    } else {
                      fields[index] = {
                        ...fields[index],
                        meta: {
                          ...props.listFields[index].meta,
                          [metaField.name]: options,
                        },
                      };
                    }
                    props.changeField('listFields', fields);
                  }}
                />
              </Form.Item>
            </Col>
          )}
        </React.Fragment>
      ));
    }
    return null;
  };

  useEffect(() => {
    props.dispatch(userAction.getUserCates({ status: 'active' }));
    props.dispatch(formAction.getFormCates({ status: 'active' }));
  }, []);

  return (
    <Row>
      <Col span={24} className="">
        <div className="w5d-form form-list-fields">
          <Form layout="vertical" className="">
            <Row gutter={25}>
              <Col span={24} className="w5d-list">
                <Row gutter={25}>
                  <Col span={24}>
                    <DragDropContext onDragEnd={(result) => handleOnDragEnd(result)}>
                      <Droppable droppableId="fieldId">
                        {(provided) => (
                          <div className="characters" {...provided.droppableProps} ref={provided.innerRef}>
                            <List
                              locale={{
                                emptyText: getLangText('GLOBAL.NO_ITEMS'),
                              }}
                              header={
                                <Row>
                                  <Col span={4} className="text-left">
                                    Thứ tự
                                  </Col>
                                  <Col span={8} className="text-left">
                                    Tiêu đề nhãn
                                  </Col>
                                  <Col span={8} className="text-left">
                                    Tên trường
                                  </Col>
                                  <Col span={4} className="text-left">
                                    Kiểu dữ liệu
                                  </Col>
                                </Row>
                              }
                              footer={null}
                              dataSource={props.listFields ? props.listFields : []}
                              renderItem={(item, index) => (
                                <Draggable key={item.id} draggableId={item.id} index={index}>
                                  {(provided) => (
                                    <List.Item key={item.id} className={item.type === 'label' ? 'label-panel-item' : ''}>
                                      <Collapse defaultActiveKey={[]} expandIconPosition="right" ghost={true} collapsible="icon">
                                        <Panel
                                          header={
                                            <Row className="full">
                                              <Col span={4} className="text-left">
                                                <span className="nbr" ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                  {index + 1}
                                                </span>
                                              </Col>
                                              <Col span={8} className="text-left">
                                                <span className="label-text">{item.label}</span>
                                                <div className="actions">
                                                  <Button onClick={(e) => e.preventDefault()} className="edit act" type="link">
                                                    Sửa
                                                  </Button>
                                                  <Button
                                                    onClick={(e) => {
                                                      e.stopPropagation();
                                                      Modal.confirm({
                                                        title: 'Bạn có chắc muốn xóa không?',
                                                        iconType: 'close-circle',
                                                        okText: 'Có',
                                                        cancelText: 'Không',
                                                        onOk() {
                                                          props.removeFormField(index);
                                                        },
                                                      });
                                                    }}
                                                    className="delete act"
                                                    type="link"
                                                  >
                                                    {getLangText('GLOBAL.DELETE')}
                                                  </Button>

                                                  <Button
                                                    className="view act"
                                                    type="link"
                                                    onClick={(e) => {
                                                      e.stopPropagation();
                                                      props.cloneFormField(item, index);
                                                    }}
                                                  >
                                                    Nhân bản
                                                  </Button>
                                                </div>
                                              </Col>
                                              <Col span={8} className="text-left">
                                                {item.name}
                                              </Col>
                                              <Col span={4} className="text-left">
                                                {item.type}
                                              </Col>
                                            </Row>
                                          }
                                          key="1"
                                        >
                                          <Row className="full field-setting">
                                            {/* <Col span={4} className="field-label">
                                              <span className="label-txt">Có hoạt động hay không?</span>
                                            </Col>
                                            <Col span={20} className="field-value">
                                              <Form.Item className="input-col">
                                                <Switch
                                                  checkedChildren="Có"
                                                  unCheckedChildren="Không"
                                                  checked={props.listFields[index].isActive}
                                                  onChange={checked => {
                                                    const fields = props.listFields;
                                                    fields[index] = {
                                                      ...fields[index],
                                                      isActive: checked,
                                                    };
                                                    props.changeField('listFields', fields);
                                                  }}
                                                />
                                              </Form.Item>
                                            </Col> */}
                                            <Col span={4} className="field-label">
                                              <span className="label-txt">Tiêu đề nhãn</span>
                                            </Col>
                                            <Col span={20} className="field-value">
                                              <Form.Item className="input-col">
                                                <Input
                                                  placeholder="Nhập tiêu đề nhãn"
                                                  defaultValue={props.listFields[index].label}
                                                  onBlur={(e) => {
                                                    const fields = props.listFields;
                                                    fields[index] = {
                                                      ...fields[index],
                                                      label: e.target.value,
                                                    };
                                                    props.changeField('listFields', fields);
                                                  }}
                                                />
                                              </Form.Item>
                                            </Col>
                                            <Col span={4} className="field-label">
                                              <span className="label-txt">Tên trường dữ liệu</span>
                                            </Col>
                                            <Col span={20} className="field-value">
                                              <Form.Item className="input-col">
                                                <Input
                                                  placeholder="Nhập tên trường dữ liệu"
                                                  defaultValue={props.listFields[index].name}
                                                  onBlur={(e) => {
                                                    const fields = props.listFields;
                                                    fields[index] = {
                                                      ...fields[index],
                                                      name: e.target.value,
                                                    };
                                                    props.changeField('listFields', fields);
                                                  }}
                                                />
                                              </Form.Item>
                                            </Col>
                                            <Col span={4} className="field-label">
                                              <span className="label-txt">Kiểu trường dữ liệu</span>
                                            </Col>
                                            <Col span={20} className="field-value">
                                              <Form.Item className="input-col">{renderTypes(index)}</Form.Item>
                                            </Col>
                                            <Col span={4} className="field-label">
                                              <span className="label-txt">Độ rộng</span>
                                            </Col>
                                            <Col span={20} className="field-value">
                                              <Form.Item className="input-col left-align">{renderColSpan(index)}</Form.Item>
                                            </Col>
                                            {props.listFields[index].type === 'repeater' && (
                                              <>
                                                <Col span={4} className="field-label">
                                                  <span className="label-txt">Trường dữ liệu con</span>
                                                </Col>
                                                <Col span={20} className="field-value">
                                                  <Form.Item className="input-col left-align">{renderSubFields(index)}</Form.Item>
                                                </Col>
                                              </>
                                            )}

                                            {props.listFields[index].type !== 'label' &&
                                              props.listFields[index].type !== 'hidden' &&
                                              props.listFields[index].type !== 'url-variable' &&
                                              props.listFields[index].type !== 'short-unique-id' &&
                                              props.listFields[index].type !== 'long-unique-id' && (
                                                <>
                                                  <Col span={4} className="field-label">
                                                    <span className="label-txt">Hướng dẫn</span>
                                                  </Col>
                                                  <Col span={20} className="field-value">
                                                    <Form.Item className="input-col">
                                                      <TextArea
                                                        rows={6}
                                                        defaultValue={props.listFields[index].instruction}
                                                        onBlur={(e) => {
                                                          const fields = props.listFields;
                                                          fields[index] = {
                                                            ...fields[index],
                                                            instruction: e.target.value,
                                                          };
                                                          props.changeField('listFields', fields);
                                                        }}
                                                        placeholder="Nhập hướng dẫn cho quản trị viên"
                                                      />
                                                    </Form.Item>
                                                  </Col>
                                                  <Col span={4} className="field-label">
                                                    <span className="label-txt">Trường bắt buộc</span>
                                                  </Col>
                                                  <Col span={20} className="field-value">
                                                    <Form.Item className="input-col">
                                                      <Switch
                                                        checkedChildren="Có"
                                                        unCheckedChildren="Không"
                                                        checked={props.listFields[index].require}
                                                        onChange={(checked) => {
                                                          const fields = props.listFields;
                                                          fields[index] = {
                                                            ...fields[index],
                                                            require: checked,
                                                          };
                                                          props.changeField('listFields', fields);
                                                        }}
                                                      />
                                                    </Form.Item>
                                                  </Col>
                                                  <Col span={4} className="field-label">
                                                    <span className="label-txt">Không cho sửa</span>
                                                  </Col>
                                                  <Col span={20} className="field-value">
                                                    <Form.Item className="input-col">
                                                      <Switch
                                                        checkedChildren="Có"
                                                        unCheckedChildren="Không"
                                                        checked={props.listFields[index].disableEdit}
                                                        onChange={(checked) => {
                                                          const fields = props.listFields;
                                                          fields[index] = {
                                                            ...fields[index],
                                                            disableEdit: checked,
                                                          };
                                                          props.changeField('listFields', fields);
                                                        }}
                                                      />
                                                    </Form.Item>
                                                  </Col>
                                                </>
                                              )}
                                            {renderMetaFields(item, index)}
                                            <Col span={4} className="field-label">
                                              <span className="label-txt">Điều kiện xuất hiện</span>
                                            </Col>
                                            <Col span={20} className="field-value">
                                              <Form.Item className="input-col">
                                                <Switch
                                                  checkedChildren="Có"
                                                  unCheckedChildren="Không"
                                                  defaultChecked={props.listFields[index].hasCondition}
                                                  onChange={(hasCondition) => {
                                                    const fields = props.listFields;
                                                    fields[index] = {
                                                      ...fields[index],
                                                      hasCondition,
                                                    };
                                                    props.changeField('listFields', fields);
                                                  }}
                                                />
                                                {renderConditions(item, index)}
                                              </Form.Item>
                                            </Col>
                                          </Row>
                                        </Panel>
                                      </Collapse>
                                    </List.Item>
                                  )}
                                </Draggable>
                              )}
                            />
                            {provided.placeholder}
                          </div>
                        )}
                      </Droppable>
                    </DragDropContext>
                  </Col>
                </Row>
              </Col>
              <Col md={18} sm={0} xs={0}></Col>
              <Col md={6} sm={12} xs={12} className="add-sub-field">
                <br />
                <Button type="dashed" icon={<PlusOutlined />} onClick={() => props.addFormField()}>
                  Thêm trường mới
                </Button>
                <br />
              </Col>
            </Row>
          </Form>
        </div>
      </Col>
    </Row>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.form,
    user: state.user,
    config: state.config,
    table: state.table,
  };
};

const FormBuilderBuildingMemo = React.memo(FormBuilderBuilding);
export default connect(mapStateToProps)(FormBuilderBuildingMemo);
