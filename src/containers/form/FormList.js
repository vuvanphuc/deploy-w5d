// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Row, Col, List, Button, Checkbox, notification, Menu, Dropdown, Space, Modal, Upload } from 'antd';
import { DownOutlined, PlusOutlined, LockOutlined, DeleteOutlined, UnlockOutlined, QuestionCircleOutlined, DownloadOutlined, UploadOutlined } from '@ant-design/icons';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
// import internal libs
import * as formAction from 'redux/actions/form';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import { config } from 'config';
import App from 'App';
import AppFilter from 'components/AppFilter';
import W5dStatusTag from 'components/status/W5dStatusTag';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import { ExportToExcel } from 'helpers/ExportToExcel';
import templateFile from 'assets/templates/Form_temp.xlsx';
const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';
function FormList(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);
  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };
  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
      checkAll: false,
      checkedList: [],
    }));
    changeUrlParams(field, value);
  };

  const allOptions = get(props, 'form.list.result.data', []).map((item) => item.bieu_mau_id);
  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (item) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(item) ? state.checkedList.filter((it) => it !== item) : state.checkedList.concat([item]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.form.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });

  const getListForms = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.form.list.search,
      ...urlQuery,
    };
    delete searchParams.page;
    setState({
      ...state,
      checkedList: [],
      checkAll: false,
    });
    props.dispatch(formAction.getForms(searchParams));
  };

  const deleteForm = (item) => {
    const callback = () => {
      getListForms();
      notification.success({
        message: `Xóa biểu mẫu '${item.ten_bieu_mau}' thành công`,
      });
    };
    confirm({
      title: `Bạn chắc chắn muốn xóa biểu mẫu '${item.ten_bieu_mau}' không ?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(formAction.deleteForm({ id: item.bieu_mau_id }, callback));
      },
    });
  };

  const blockForm = (item) => {
    const callback = () => {
      getListForms();
      notification.success({
        message: `Khóa biểu mẫu '${item.ten_bieu_mau}' thành công`,
      });
    };
    confirm({
      title: `Bạn chắc chắn muốn khóa biểu mẫu '${item.ten_bieu_mau}' không ?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(
          formAction.editFormsStatus(
            {
              listIds: [item.bieu_mau_id],
              status: 'inactive',
            },
            callback
          )
        );
      },
    });
  };

  function deleteListForms(e) {
    const callback = (res) => {
      if (res.success) {
        getListForms();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Xóa nhiều biểu mẫu thành công',
        });
      }
    };
    confirm({
      title: 'Bạn có chắc chắn muốn xóa các biểu mẫu được chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(formAction.deleteForms({ listIds: state.checkedList }, callback));
      },
    });
  }

  const renderCategory = (nhom_bieu_mau_id) => {
    const cate = get(props, 'form.listCates.result.data', []).find((cate) => cate.nhom_bieu_mau_id === nhom_bieu_mau_id);
    return cate && cate.ten_nhom ? cate.ten_nhom : '-';
  };

  //Active Forms theo list => active
  function activeListForms(e) {
    const callback = (res) => {
      if (res.success) {
        getListForms();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Kích hoạt nhiều biểu mẫu thành công',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các biểu mẫu được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            formAction.editFormsStatus(
              {
                listIds: state.checkedList,
                status: 'active',
              },
              callback
            )
          );
        },
      });
    }
  }

  //Khoa Forms theo list => inactive
  function blockListForms(e) {
    const callback = (res) => {
      if (res.success) {
        getListForms();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Khóa nhiều biểu mẫu thành công',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn khóa các biểu mẫu được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            formAction.editFormsStatus(
              {
                listIds: state.checkedList,
                status: 'inactive',
              },
              callback
            )
          );
        },
      });
    }
  }

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListForms} disabled={!shouldHaveAccessPermission('bieu_mau', 'bieu_mau/xoa') || state.checkedList.length <= 0}>
        Xóa mẫu biểu
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeListForms}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('bieu_mau', 'bieu_mau/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt mẫu biểu
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockListForms}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('bieu_mau', 'bieu_mau/sua') || state.checkedList.length <= 0}
      >
        Khóa mẫu biểu
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.form.list.search,
      ...urlQuery,
      loading: false,
    };
    props.dispatch(formAction.getForms(searchParams));
  }, [props.location]);

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.form.list.search,
      ...urlQuery,
      loading: false,
    };
    props.dispatch(formAction.getFormCates());
  }, []);
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const saveFile = () => {
    FileSaver.saveAs(templateFile, 'tempForm.xlsx');
  };
  function processExcel(data) {
    const workbook = XLSX.read(data, { type: 'binary' });
    const firstSheet = workbook.SheetNames[0];
    const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    const onSuccess = () => {
      notification.success({
        message: 'Import biểu mẫu thành công',
      });
    };
    const onError = () => {
      const urlQuery = queryString.parse(props.location.search);
      const searchParams = {
        ...props.form.list.search,
        ...urlQuery,
        loading: false,
      };
      props.dispatch(formAction.getForms(searchParams));
    };
    props.dispatch(formAction.importForms({ listForms: excelRows }, onSuccess, onError));
  }
  const customRequest = async ({ file }) => {
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      if (reader.readAsBinaryString) {
        reader.onload = (e) => {
          processExcel(reader.result);
        };
        reader.readAsBinaryString(file);
      }
    } else {
      notification.error({
        message: 'Không đọc được file',
      });
    }
  };
  const menuImport = (
    <Menu>
      <Menu.Item key="1" icon={<DownloadOutlined />} onClick={saveFile}>
        Tải xuống file mẫu
      </Menu.Item>
      <Menu.Item key="2" icon={<UploadOutlined />}>
        <Upload id="uploadFile" multiple={false} accept=".xls,.xlsx" showUploadList={false} customRequest={(data) => customRequest(data)}>
          Đẩy lên dữ liệu
        </Upload>
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý biểu mẫu nhập liệu</title>
      </Helmet>
      {props.form.list.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                title="Quản lý biểu mẫu"
                isShowStatus={true}
                isShowSearchBox={true}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                isShowCategories={true}
                categories={get(props, 'form.listCates.result.data', []).map((cate) => ({
                  name: cate.ten_nhom,
                  id: cate.nhom_bieu_mau_id,
                }))}
                search={state.search}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group" gutter={[8, 8]}>
            <Col xl={12} sm={13} xs={24}>
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
            </Col>

            <Col xl={12} sm={12} xs={24} className="right-actions">
              <Space wrap>
                <Dropdown overlay={menuImport} trigger="click" className="btn-action">
                  <Button>
                    Import file dữ liệu
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
              <ExportToExcel
                apiData={get(props, 'form.list.result.data', [])
                  .map((item) => {
                    return {
                      'Mã biểu mẫu': item.bieu_mau_id,
                      'Tên biểu mẫu': item.ten_bieu_mau,
                      'Bảng dữ liệu': item.bang_du_lieu,
                      'Mô tả': item.mo_ta,
                      'Trường dữ liệu': item.truong_du_lieu,
                      'Số cột': item.so_cot,
                      'Kiểu giao diện': item.kieu_giao_dien,
                      'Chế độ chỉ xem': item.che_do_chi_xem,
                      'Mã nhóm biểu mẫu': item.nhom_bieu_mau_id,
                      'Trạng thái': item.trang_thai,
                      'Ngày tạo': moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT),
                    };
                  })
                  .sort((a, b) => (a.Nhom > b.Nhom ? 1 : -1))}
                fileName="DanhSach"
              />
              {shouldHaveAccessPermission('bieu_mau', 'bieu_mau/them') && (
                <Link to="/form/add">
                  <Button shape="round" type="primary" icon={<PlusOutlined />} className="btn-action">
                    Thêm mới biểu mẫu
                  </Button>
                </Link>
              )}
            </Col>
          </Row>

          <div className="w5d-list">
            <List
              locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
              header={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={5} xl={5} className="text-left">
                    Tên biểu mẫu
                    <Sorting urlQuery={urlQuery} field={'ten_bieu_mau'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Mô tả
                    <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Danh mục
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>

                  <Col xs={3} xl={4} className="text-left">
                    {getLangText('GLOBAL.UPDATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_sua'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={4} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              footer={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={5} xl={5} className="text-left">
                    Tên biểu mẫu
                    <Sorting urlQuery={urlQuery} field={'ten_bieu_mau'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Mô tả
                    <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Danh mục
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={4} className="text-left">
                    {getLangText('GLOBAL.UPDATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_sua'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={4} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              dataSource={get(props, 'form.list.result.data', [])}
              pagination={{
                hideOnSinglePage: false,
                responsive: true,
                showLessItems: true,
                pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                pageSize: pageSize,
                onChange: (page, pageSize) => {
                  changeMutilUrlParams({ pageSize, page });
                  setPageSize(pageSize);
                  setPage(page);
                },
                current: Number(page),
                showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                showSizeChanger: true,
              }}
              renderItem={(item) => (
                <List.Item key={item.bieu_mau_id}>
                  <Row className="full">
                    <Col xs={1} xl={1}>
                      <Checkbox checked={state.checkedList.includes(item.bieu_mau_id)} value={item.bieu_mau_id} onChange={() => onChangeCheck(item.bieu_mau_id)} />
                    </Col>
                    <Col xs={5} xl={5} className="text-left">
                      {item.ten_bieu_mau}
                      <div className="actions">
                        {shouldHaveAccessPermission('bieu_mau', 'bieu_mau/sua') && (
                          <Link className="edit act" to={`/form/edit/${item.bieu_mau_id}?table=${item.bang_du_lieu}`}>
                            {getLangText('GLOBAL.EDIT')}
                          </Link>
                        )}
                        {shouldHaveAccessPermission('bieu_mau', 'bieu_mau/xoa') && item.trang_thai === 'inactive' && (
                          <Button className="delete act" type="link" onClick={() => deleteForm(item)}>
                            Xóa
                          </Button>
                        )}
                        {shouldHaveAccessPermission('bieu_mau', 'bieu_mau/sua') && item.trang_thai === 'active' && (
                          <Button className="delete act" type="link" onClick={() => blockForm(item)}>
                            Khóa
                          </Button>
                        )}
                      </div>
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      {item.mo_ta}
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      {renderCategory(item.nhom_bieu_mau_id)}
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      <W5dStatusTag status={item.trang_thai} />
                    </Col>
                    <Col xs={3} xl={4} className="text-left">
                      {moment(item.ngay_sua).isValid() ? moment(item.ngay_sua).utc(0).format(config.DATE_FORMAT) : '-'}
                    </Col>
                    <Col xs={3} xl={4} className="text-left">
                      {moment(item.ngay_tao).isValid() ? moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT) : '-'}
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.form,
    config: state.config,
  };
};

export default connect(mapStateToProps)(FormList);
