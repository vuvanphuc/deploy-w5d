// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { get } from 'lodash';
import { Button, Form, Input, Space, Row, Col, Select, Modal, Checkbox, Radio, notification } from 'antd';
import { UnorderedListOutlined, PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';

// import internal libs
import App from 'App';
import { config } from 'config';
import constants from 'constants/global.constants';
import { getLangText } from 'helpers/language.helper';
import { getUserInformation, shouldHaveAccessPermission, recursiveCates, encodeHTML, decodeHTML } from 'helpers/common.helper';
import * as questionAction from 'redux/actions/question';
import * as examAction from 'redux/actions/exam';
import { TextEditorWidget } from 'containers/form/components/widgets';
import Loading from 'components/loading/Loading';

const { Option } = Select;
const { TextArea } = Input;

function QuestionForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const examId = get(props, 'match.params.examId', '');

  const defaultQuestion = {
    tieu_de: '',
    loi_giai: '',
    chu_de_id: '',
    de_thi_id: examId ? examId : '',
    loai_cau_hoi: '',
    kieu_cau_hoi: 'LUA_CHON',
    bo_mon: '',
    dap_an_dung: '',
    nhom_cau_hoi_id: '',
    trang_thai: 'active',
    dap_an: [
      { tieu_de: '', label: 'Đáp án A', key: 'A' },
      { tieu_de: '', label: 'Đáp án B', key: 'B' },
      { tieu_de: '', label: 'Đáp án C', key: 'C' },
      { tieu_de: '', label: 'Đáp án D', key: 'D' },
    ],
  };

  const defaultForm = {
    ...defaultQuestion,
    uploadLoading: false,
    isChanged: false,
    openMediaLibrary: false,
    insertMedia: {
      open: false,
      editor: null,
    },
  };

  const isEdit = props.match && props.match.params && props.match.params.id;
  const openMediaLibrary = () => setState((state) => ({ ...state, openMediaLibrary: true }));

  const handleSubmit = (values) => {
    const dap_an = {
      dap_an_a: values.dap_an[0].tieu_de,
      dap_an_b: values.dap_an[1].tieu_de,
      dap_an_c: values.dap_an[2].tieu_de,
      dap_an_d: values.dap_an[3].tieu_de,
    };
    if (props.match.params.id) {
      // edit a question
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Sửa câu hỏi thành công',
          });
          form.resetFields();
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
          if (isEdit) {
            props.dispatch(
              questionAction.getQuestion({
                cau_hoi_id: props.match.params.id,
              })
            );
          }
        }
      };
      const newFormData = {
        ...state,
        ...values,
        dap_an,
      };
      props.dispatch(questionAction.editQuestion(newFormData, callback));
    } else {
      //add a new question
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Tạo mới câu hỏi thành công',
          });
          // setState((state) => ({
          //   ...state,
          //   ...values,
          //   isChanged: false,
          //   dap_an: encodeHTML(JSON.stringify(values.dap_an)),
          // }));
          // form.resetFields();

          let routeLink = shouldHaveAccessPermission('cau_hoi', 'cau_hoi/sua') ? `/question/edit/${res.data.cau_hoi_id}` : '/questions';
          if (examId) {
            routeLink = `/question/add/${examId}`;
            const newBody = { ...state, ...defaultForm, de_thi_id: examId };
            form.setFieldsValue(newBody);
            setState(newBody);
          }
          history.push(routeLink);
        } else {
          notification.error({
            message: res.error,
          });
        }
      };
      props.dispatch(questionAction.createQuestion({ ...state, ...values, dap_an }, callback));
    }
  };

  const renderParents = () => {
    let options = [];
    if (props.question.listCates.result && props.question.listCates.result.data) {
      const cates = recursiveCates(get(props, 'question.listCates.result.data', []), get(props, 'question.listCates.result.data', []), 'nhom_cau_hoi_id', 'nhom_cha_id');
      options = cates.map((cate) => (
        <Option key={cate.nhom_cau_hoi_id} value={cate.nhom_cau_hoi_id}>
          {cate.titleLevel}
        </Option>
      ));
      return (
        <Select
          showSearch={false}
          value={state.nhom_cha_id}
          loading={props.question.listCates.loading}
          onChange={(nhom_cha_id) => setState({ ...state, nhom_cha_id, isChanged: true })}
          placeholder="Chọn nhóm cha"
        >
          {options}
        </Select>
      );
    }
  };

  // const renderTopics = () => {
  //   let options = [];
  //   if (props.exam.listTopics.result && props.exam.listTopics.result.data) {
  //     const cates = recursiveCates(get(props, 'exam.listTopics.result.data', []), get(props, 'exam.listTopics.result.data', []), 'chu_de_id', 'nhom_cha_id');
  //     options = cates.map((cate) => (
  //       <Option key={cate.chu_de_id} value={cate.chu_de_id}>
  //         {cate.titleLevel}
  //       </Option>
  //     ));
  //     return (
  //       <Select
  //         showSearch={false}
  //         value={state.chu_de_id}
  //         loading={props.exam.listTopics.loading}
  //         onChange={(chu_de_id) => setState({ ...state, chu_de_id, isChanged: true })}
  //         placeholder="Chọn chủ đề"
  //       >
  //         {options}
  //       </Select>
  //     );
  //   }
  // };

  const renderExams = () => {
    let options = [];
    if (props.exam.list.result && props.exam.list.result.data) {
      const cates = get(props, 'exam.list.result.data', []);
      options = cates.map((cate) => (
        <Option key={cate.de_thi_id} value={cate.de_thi_id}>
          {cate.tieu_de}
        </Option>
      ));
      return (
        <Select
          disabled={true}
          showSearch={false}
          value={state.de_thi_id}
          loading={props.exam.list.loading}
          onChange={(de_thi_id) => setState({ ...state, de_thi_id, isChanged: true })}
          placeholder="Chọn đề thi"
        >
          {options}
        </Select>
      );
    }
  };

  const renderQuestionStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select defaultValue={state.trang_thai} value={state.trang_thai} onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })} placeholder={getLangText('GLOBAL.SELECT_STATUS')}>
        {options}
      </Select>
    );
  };
  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      anh_dai_dien: file.tep_tin_url,
    }));
  };

  const onInsertImage = (file) => {
    state.insertMedia.editor.insertContent('<img className="image-editor" src="' + `${config.UPLOAD_API_URL}/${file.imgUrl}` + '"/>');
    setState((state) => ({
      ...state,
      isChanged: true,
      insertMedia: {
        open: false,
      },
    }));
  };

  const onResetForm = () => {
    if (props.question.item.result && isEdit) {
      form.setFieldsValue(props.question.item.result.data);
      setState((state) => ({ ...state, ...props.question.item.result.data }));
    } else {
      form.resetFields();
      setState((state) => ({ ...state, ...defaultForm }));
    }
  };

  const [state, setState] = useState(defaultForm);
  useEffect(() => {
    // props.dispatch(questionAction.getQuestionCates());
    // props.dispatch(examAction.getExamTopics());
    props.dispatch(examAction.getExams({ loading: true }));
    if (isEdit) {
      props.dispatch(
        questionAction.getQuestion({
          cau_hoi_id: props.match.params.id,
        })
      );
    } else {
      form.setFieldsValue(defaultQuestion);
    }
    return () => (window.onbeforeunload = null);
  }, []);

  useEffect(() => {
    const body = get(props, 'question.item.result.data', {});

    if (isEdit && body) {
      try {
        form.setFieldsValue({ ...body });
        setState((state) => ({ ...state, ...body }));
      } catch (error) {
        console.log(error);
      }
    }
    window.onbeforeunload = null;
  }, [props.question.item.result, examId]);
  console.log(2222, state);
  return (
    <App>
      <Helmet>
        <title>{isEdit ? `Sửa câu hỏi` : 'Thêm mới câu hỏi'}</title>
      </Helmet>
      {props.question.item.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <div className="w5d-form">
            <Form layout="vertical" className="" onFinish={handleSubmit} form={form} initialValues={defaultQuestion}>
              <Row gutter={25}>
                <Col xl={18} sm={16} xs={24}>
                  <h2 className="header-form-title">{isEdit ? 'Cập nhật câu hỏi' : 'Thêm mới câu hỏi'}</h2>
                </Col>
                <Col xl={6} sm={8} xs={24}>
                  <Col xl={4} sm={24} xs={24}></Col>
                  <Col xl={20} sm={24} xs={24}>
                    <Link to={examId || isEdit ? `/questions?de_thi_id=${state.de_thi_id}` : '/questions'} target="_blank">
                      <Button block shape="round" type="primary" icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />} className="btn-create-todo">
                        Danh sách câu hỏi cùng đề
                      </Button>
                    </Link>
                  </Col>
                </Col>
                <Col xl={18} sm={24} xs={24} className="left-content">
                  <div className="border-box">
                    <Row gutter={25}>
                      <Col xl={24} sm={24} xs={24}>
                        <Form.Item
                          className="input-col"
                          label="Tiêu đề"
                          name="tieu_de"
                          rules={[
                            {
                              required: true,
                              message: 'Tiêu đề là trường bắt buộc',
                            },
                          ]}
                        >
                          <TextEditorWidget value={state.tieu_de} placeholder="Nội dung câu hỏi" onChange={(val) => setState({ ...state, tieu_de: val })} height={200} />
                        </Form.Item>
                        <Form.Item className="input-col" label="Lời giải" name="loi_giai" rules={[]}>
                          <TextEditorWidget value={state.loi_giai} placeholder="Nội dung lời giải" onChange={(val) => setState({ ...state, loi_giai: val })} height={200} />
                        </Form.Item>
                        <Form.Item
                          className="input-col"
                          label="Loại câu hỏi"
                          name="loai_cau_hoi"
                          rules={[
                            {
                              required: true,
                              message: 'Loại câu hỏi là trường bắt buộc',
                            },
                          ]}
                        >
                          <Radio.Group options={constants.QUESTIONS_TYPES} />
                        </Form.Item>
                        <Form.Item
                          className="input-col"
                          label="Bộ môn"
                          name="bo_mon"
                          rules={[
                            {
                              required: true,
                              message: 'Bộ môn là trường bắt buộc',
                            },
                          ]}
                        >
                          <Radio.Group options={constants.SUBJECT_LIST} />
                        </Form.Item>
                        <Form.Item className="input-col hidden" label="Kiểu câu hỏi" name="kieu_cau_hoi" rules={[]}>
                          <Radio.Group options={constants.QUESTIONS_FORMATS} />
                        </Form.Item>
                        <Form.Item
                          className="input-col"
                          label="Đáp án đúng"
                          name="dap_an_dung"
                          rules={[
                            {
                              required: true,
                              message: 'Đáp án đúng là trường bắt buộc',
                            },
                          ]}
                        >
                          <Radio.Group options={constants.ANSWER_OPTIONS} />
                        </Form.Item>
                        <Form.List name="dap_an">
                          {(fields, { add, remove }) => (
                            <div className="group-answers">
                              {fields.map(({ key, name, ...restField }) => (
                                <Row key={key}>
                                  <Col xl={20}>
                                    <Form.Item {...restField} rules={[]} className="label">
                                      {state.dap_an[key].label}
                                    </Form.Item>
                                  </Col>
                                  {/* <Col xl={4} className="txt-right">
                                    <Form.Item {...restField} name={[name, 'dap_an_dung']} valuePropName="checked" rules={[]}>
                                      <Checkbox>Đáp án đúng</Checkbox>
                                    </Form.Item>
                                  </Col> */}
                                  {/* <Col xl={4}>
                                    <MinusCircleOutlined onClick={() => remove(name)} />
                                  </Col> */}
                                  <Col xl={24}>
                                    <Form.Item {...restField} name={[name, 'tieu_de']} rules={[{ required: true, message: 'Bạn chưa nhập nội dung' }]}>
                                      <TextEditorWidget
                                        value={state.dap_an[key].tieu_de}
                                        placeholder="Nội dung đáp án"
                                        onChange={(val) => {
                                          let dap_an = [...state.dap_an];
                                          dap_an[key] = {
                                            ...dap_an[key],
                                            tieu_de: val,
                                          };
                                          setState({ ...state, dap_an });
                                        }}
                                        height={200}
                                      />
                                    </Form.Item>
                                  </Col>
                                </Row>
                              ))}
                              {/* <Form.Item>
                                <Button type="dashed" onClick={() => add()} icon={<PlusOutlined />}>
                                  Thêm đáp án
                                </Button>
                              </Form.Item> */}
                            </div>
                          )}
                        </Form.List>
                      </Col>
                    </Row>
                  </div>
                </Col>
                <Col xl={6} sm={24} xs={24} className="right-content">
                  <div className="box">
                    <div className="box-body">
                      <div className="border-box hideMobile">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              {/* */}
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                      {/* <div
                        className="border-box"
                        style={{
                          display: !shouldHaveAccessPermission('cau_hoi', 'cau_hoi/sua') ? 'none' : 'block',
                        }}
                      >
                        <h2>{getLangText('GLOBAL.CATEGORIES')}</h2>
                        <Form.Item
                          className="input-col"
                          name="nhom_cau_hoi_id"
                          rules={[
                            {
                              required: true,
                              message: 'Nhóm câu hỏi là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderParents()}
                        </Form.Item>
                      </div> */}
                      {/* <div className="border-box">
                        <h2>Chủ đề</h2>
                        <Form.Item
                          className="input-col"
                          name="chu_de_id"
                          rules={[
                            {
                              required: true,
                              message: 'Chủ đề là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderTopics()}
                        </Form.Item>
                      </div> */}
                      <div className="border-box">
                        <h2>Đề thi</h2>
                        <Form.Item
                          className="input-col"
                          name="de_thi_id"
                          rules={[
                            {
                              required: true,
                              message: 'Đề thi là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderExams()}
                        </Form.Item>
                      </div>
                      <div
                        className="border-box"
                        style={{
                          display: !shouldHaveAccessPermission('cau_hoi', 'cau_hoi/sua') ? 'none' : 'block',
                        }}
                      >
                        <h2>{getLangText('GLOBAL.STATUS')}</h2>
                        <Form.Item
                          className="input-col"
                          name="trang_thai"
                          rules={[
                            {
                              required: true,
                              message: 'Trạng thái là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderQuestionStatus()}
                        </Form.Item>
                      </div>

                      <div className="border-box hideDesktop">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" htmlType="reset" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>
    </App>
  );
}
const mapStateToProps = (state) => {
  return {
    question: state.question,
    exam: state.exam,
  };
};
export default connect(mapStateToProps)(QuestionForm);
