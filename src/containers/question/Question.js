// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { get } from 'lodash';
import { Row, Col, List, Avatar, Button, Checkbox, notification, Menu, Dropdown, Space, Modal, Upload, Image, Switch, Form } from 'antd';
import { DownOutlined, PlusOutlined, LockOutlined, DeleteOutlined, UnlockOutlined, QuestionCircleOutlined, DownloadOutlined, UploadOutlined, FolderOutlined } from '@ant-design/icons';
import { io as socketIOClient } from 'socket.io-client';

// import internal libs
import * as questionAction from 'redux/actions/question';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl, recursiveCates } from 'helpers/common.helper';
import { config } from 'config';
import templateFile from 'assets/templates/User_temp.xlsx';
import App from 'App';
import constants from '../../constants/global.constants';
import AppFilter from 'components/AppFilter';
import W5dStatusTag from 'components/status/W5dStatusTag';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import { ExportToExcel } from 'helpers/ExportToExcel';
import W5dImageMinIO from 'components/W5dImageMinIO';

const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';

function Question(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);
  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };
  const searchQuestions = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.question.list.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(questionAction.getQuestions(searchParams));
    //form.setFieldsValue(state.body);
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.question.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });
  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };
  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
      checkAll: 0,
      checkedList: [],
    }));
    changeUrlParams(field, value);
  };
  const onDelete = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Block câu hỏi thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa câu hỏi thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        searchQuestions();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(questionAction.deleteQuestion({ id }, callback));
      },
    });
  };

  const onSwitch = (body) => {
    const callback = (res) => {
      if (res.success) {
        searchQuestions();
        notification.success({
          message: 'Đổi trạng thái thành công',
        });
      }
    };
    props.dispatch(questionAction.editQuestion(body, callback));
  };
  const allOptions = get(props, 'question.list.result.data', []).map((item) => item.cau_hoi_id);

  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (cau_hoi_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(cau_hoi_id) ? state.checkedList.filter((it) => it !== cau_hoi_id) : state.checkedList.concat([cau_hoi_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  function deleteListQuestions(e) {
    const callback = (res) => {
      if (res.success) {
        searchQuestions();
        notification.success({
          message: 'Xóa nhiều câu hỏi thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các câu hỏi được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(questionAction.deleteQuestions({ listIds: state.checkedList }, callback));
      },
    });
  }
  //Active thanh vien theo list => active
  function activeListQuestions(e) {
    const callback = (res) => {
      if (res.success) {
        searchQuestions();
        notification.success({
          message: 'Kích hoạt câu hỏi thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các câu hỏi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(questionAction.editStatusQuestion({ listIds: state.checkedList, status: 'active' }, callback));
        },
      });
    }
  }
  //Khoa thanh vien theo list => inactive
  function blockListQuestions(e) {
    const callback = (res) => {
      if (res.success) {
        searchQuestions();
        notification.success({
          message: 'Khóa câu hỏi thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn chắc chắn muốn khóa những câu hỏi được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            questionAction.editStatusQuestion(
              {
                listIds: state.checkedList,
                status: 'inactive',
              },
              callback
            )
          );
        },
      });
    }
  }

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListQuestions} disabled={!shouldHaveAccessPermission('cau_hoi', 'cau_hoi/xoa') || state.checkedList.length <= 0}>
        Xóa câu hỏi
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeListQuestions}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('cau_hoi', 'cau_hoi/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt câu hỏi
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockListQuestions}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('cau_hoi', 'cau_hoi/sua') || state.checkedList.length <= 0}
      >
        Khóa câu hỏi
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.question.list.search,
      ...urlQuery,
      loading: true,
    };
    props.dispatch(questionAction.getQuestions(searchParams));
  }, [props.location]);

  const findCate = (id) => {
    const cates = get(props, 'question.listCates.result.data', []);
    const cate = cates.find((ct) => ct.nhom_cau_hoi_id === id);
    return cate ? cate.ten_nhom : '';
  };

  const findType = (type) => {
    const cates = get(constants, 'QUESTIONS_TYPES', []);
    const cate = cates.find((ct) => ct.value === type);
    return cate ? cate.label : '---';
  };

  const findSubject = (type) => {
    const cates = get(constants, 'SUBJECT_LIST', []);
    const cate = cates.find((ct) => ct.value === type);
    return cate ? cate.label : '---';
  };

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.question.list.search,
      ...urlQuery,
      loading: false,
    };
    props.dispatch(questionAction.getQuestionCates());
    const socket = socketIOClient(config.API_URL, config.SOCKET_CONFIG);
    socket.on('event://questions-changed', () => {
      props.dispatch(questionAction.getQuestions(searchParams));
    });
  }, []);
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const saveFile = () => {
    FileSaver.saveAs(templateFile, 'tempUser.xlsx');
  };

  function processExcel(data) {
    const workbook = XLSX.read(data, { type: 'binary' });
    const firstSheet = workbook.SheetNames[0];
    const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    const onSuccess = () => {
      notification.success({
        message: 'Import biểu mẫu thành công',
      });
    };
    const onError = () => {
      const urlQuery = queryString.parse(props.location.search);
      const searchParams = {
        ...props.question.list.search,
        ...urlQuery,
        loading: false,
      };
      props.dispatch(questionAction.getQuestions(searchParams));
    };
    //props.dispatch(questionAction.importQuestions({ listUsers: excelRows }, onSuccess, onError));
  }

  const customRequest = async ({ file }) => {
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      if (reader.readAsBinaryString) {
        reader.onload = (e) => {
          processExcel(reader.result);
        };
        reader.readAsBinaryString(file);
      }
    } else {
      notification.error({
        message: 'Không đọc được file',
      });
    }
  };

  const menuImport = (
    <Menu>
      <Menu.Item key="1" icon={<DownloadOutlined />} onClick={saveFile}>
        Tải xuống file mẫu
      </Menu.Item>
      <Menu.Item key="2" icon={<UploadOutlined />}>
        <Upload id="uploadFile" multiple={false} accept=".xls,.xlsx" showUploadList={false} customRequest={(data) => customRequest(data)}>
          Đẩy lên dữ liệu
        </Upload>
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý câu hỏi</title>
      </Helmet>
      {props.question.list.loading && <Loading />}
      <Row className="app-main">
        <Col xl={24} className="body-content">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                title="Ngân hàng câu hỏi"
                isShowCategories={true}
                isShowStatus={true}
                isShowSearchBox={true}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                categories={recursiveCates(get(props, 'question.listCates.result.data', []), get(props, 'question.listCates.result.data', []), 'nhom_cau_hoi_id', 'nhom_cha_id').map((cate) => ({
                  name: cate.titleLevel,
                  id: cate.nhom_cau_hoi_id,
                }))}
                search={state.search}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group" gutter={[8, 8]}>
            <Col xl={12} sm={12} xs={24}>
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
            </Col>
            <Col xl={12} sm={12} xs={24} className="right-actions">
              {/* <Space wrap>
                <Dropdown overlay={menuImport} trigger="click">
                  <Button>
                    Import file dữ liệu
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space> */}
              {/* <ExportToExcel
                apiData={get(props, 'question.list.result.data', [])
                  .map((item) => {
                    return {
                      'Mã câu hỏi': item.cau_hoi_id,
                      'Tên câu hỏi': item.tieu_de,
                      'Mô tả': item.mo_ta,
                      'Nội dung': item.noi_dung,
                      'Mã nhóm': item.nhom_cau_hoi_id,
                      'Tên nhóm': findCate(item.nhom_cau_hoi_id),
                      'Bài viết mới': item.tin_moi,
                      'Bài viết nổi bật': item.loai_cau_hoi,
                      'Ngày tạo': moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT),
                    };
                  })
                  .sort((a, b) => (a.Nhom > b.Nhom ? 1 : -1))}
                fileName="DanhSach"
              /> */}
              {/* <Button shape="round" type="primary" icon={<FolderOutlined />} className="btn-action" onClick={() => history.push('question_category')}>
                Danh mục câu hỏi
              </Button> */}
              {/* {shouldHaveAccessPermission('cau_hoi', 'cau_hoi/them') && (
                <Link to="/question/add">
                  <Button shape="round" type="primary" icon={<PlusOutlined />} className="btn-action">
                    Thêm mới câu hỏi
                  </Button>
                </Link>
              )} */}
            </Col>
          </Row>

          <div className="w5d-list">
            <List
              locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
              header={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={9} xl={9} className="text-left">
                    Tiêu đề
                    <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Bộ môn
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Loại câu hỏi
                    <Sorting urlQuery={urlQuery} field={'loai_cau_hoi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              footer={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={9} xl={9} className="text-left">
                    Tiêu đề
                    <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Bộ môn
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Loại câu hỏi
                    <Sorting urlQuery={urlQuery} field={'loai_cau_hoi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              dataSource={get(props, 'question.list.result.data', [])}
              pagination={{
                hideOnSinglePage: false,
                responsive: true,
                showLessItems: true,
                pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                pageSize: pageSize,
                onChange: (page, pageSize) => {
                  changeMutilUrlParams({ pageSize, page });
                  setPageSize(pageSize);
                  setPage(page);
                },
                current: Number(page),
                showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                showSizeChanger: true,
              }}
              renderItem={(item) => (
                <List.Item key={item.cau_hoi_id} id="listUsers">
                  <Row className="full">
                    <Col xs={1} xl={1}>
                      <Checkbox checked={state.checkedList.includes(item.cau_hoi_id)} value={item.cau_hoi_id} onChange={() => onChangeCheck(item.cau_hoi_id)} />
                    </Col>
                    <Col xs={9} xl={9} className="text-left question-title">
                      <Link className="edit" to={shouldHaveAccessPermission('cau_hoi', 'cau_hoi/sua') ? `/question/edit/${item.cau_hoi_id}` : '#'}>
                        <div dangerouslySetInnerHTML={{ __html: item.tieu_de }}></div>
                      </Link>
                      <div className="actions">
                        {shouldHaveAccessPermission('cau_hoi', 'cau_hoi/sua') && (
                          <Link className="edit act" to={`/question/edit/${item.cau_hoi_id}`}>
                            {getLangText('GLOBAL.EDIT')}
                          </Link>
                        )}
                        {shouldHaveAccessPermission('cau_hoi', 'cau_hoi/xoa') && (
                          <Button
                            className="delete act"
                            type="link"
                            onClick={() => {
                              onDelete(item.cau_hoi_id, item.trang_thai);
                            }}
                          >
                            {item.trang_thai === 'active' ? 'Khóa' : 'Xóa'}
                          </Button>
                        )}
                        {/* {(shouldHaveAccessPermission('cau_hoi', 'cau_hoi/chi_tiet') &&
                            <Link className="view act" to={`/question/detail/${item.cau_hoi_id}`}>
                              {getLangText('GLOBAL.VIEW')}
                            </Link>
                          )} */}
                      </div>
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      {findSubject(item.bo_mon)}
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      {findType(item.loai_cau_hoi)}
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      <W5dStatusTag status={item.trang_thai} />
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    question: state.question,
  };
};
export default connect(mapStateToProps)(Question);
