import React, { useEffect } from 'react';
import get from 'lodash/get';
import { connect } from 'react-redux';

import * as userAction from 'redux/actions/user';
import { Pie } from 'react-chartjs-2';

function ChartUserGroup(props) {
  useEffect(() => {
    props.dispatch(userAction.getUserCates());
  }, []);

  const getDataSource = (cates, max) => {
    const backgroundColor = ['#ff6600', '#faad14', '#1890ff', '#3ce8e8', '#c350c7', '#92c5d2'];
    const dataObj = {
      data: [],
      labels: [],
      backgroundColor: [],
    };
    if (cates.length > max - 1) {
      // sắp xếp theo số lượng giảm dần
      cates.sort((a, b) => (a.soluong < b.soluong ? 1 : b.soluong < a.soluong ? -1 : 0));
      let countItem = 0;
      let otherValue = 0;
      cates.forEach((cate, idx) => {
        countItem++;
        if (idx < max - 1) {
          dataObj.data.push(cate.soluong);
          dataObj.labels.push(cate.ten_nhom);
          dataObj.backgroundColor.push(backgroundColor[idx]);
        } else {
          otherValue += cate.soluong;
        }
      });
      dataObj.data.push(otherValue);
      dataObj.labels.push('Nhóm khác');
      dataObj.backgroundColor.push(backgroundColor[backgroundColor.length - 1]);
      return dataObj;
    }

    cates.forEach((cate, idx) => {
      dataObj.data.push(cate.soluong);
      dataObj.labels.push(cate.ten_nhom);
      dataObj.backgroundColor.push(backgroundColor[idx]);
    });

    return dataObj;
  };

  let userCates = get(props, 'user.listCates.result.data', []);
  const dataSource = getDataSource(userCates, 6);

  const option = {
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          const dataset = data.datasets[tooltipItem.datasetIndex];
          const meta = dataset._meta[Object.keys(dataset._meta)[0]];
          const total = meta.total;
          const currentValue = dataset.data[tooltipItem.index];
          const percentage = parseFloat(((currentValue / total) * 100).toFixed(1));
          return currentValue + ' (' + percentage + '%)';
        },
        title: (tooltipItem, data) => {
          return data.labels[tooltipItem[0].index];
        },
      },
    },
  };
  return (
    <Pie
      data={{
        datasets: [
          {
            backgroundColor: dataSource.backgroundColor,
            data: dataSource.data,
          },
        ],
        labels: dataSource.labels,
        hoverOffset: 4,
      }}
      options={option}
    />
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps)(ChartUserGroup);
