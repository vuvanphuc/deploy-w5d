// import external libs
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import get from 'lodash/get';
import { Link } from 'react-router-dom';
import { Col, Row, Table } from 'antd';
import { FileTextOutlined, TeamOutlined, FileSearchOutlined, RightOutlined, AppstoreOutlined, DatabaseOutlined, LoadingOutlined, SnippetsOutlined } from '@ant-design/icons';

// import internal libs
import App from 'App';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission } from 'helpers/common.helper';
import Loading from 'components/loading/Loading';
import constants from 'constants/global.constants';
import W5dImageMinIO from 'components/W5dImageMinIO';

function Home(props) {
  const statistics = get(props, 'statistic.global', []);
  const configList = get(props, 'config.list.result.data', []);

  const commonConfig = configList.find((item) => item.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
  const commonData = commonConfig ? JSON.parse(commonConfig.du_lieu) : {};

  const welcomeTitle = commonData.site_welcome;

  return (
    <App>
      <Helmet>
        <title>{welcomeTitle}</title>
      </Helmet>
      {props.statistic.loading && <Loading />}
      <Row className="app-main home-screen" gutter={[30, 30]}>
        <Col span={24}>
          <div className="border-box-widget">
            <h2>{getLangText('DASHBOARD.STATISTICS')}</h2>
            <div className="border-box-body">
              <h1 className="welcome-text">{welcomeTitle}</h1>
              <Row gutter={[16, 16]}>
                {shouldHaveAccessPermission('nhan_vien', 'nhan_vien/xem') && (
                  <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                    <div className="dashboard-stat stat-user">
                      <div className="visual">
                        <TeamOutlined />
                      </div>
                      <div className="detail">
                        <div className="number">
                          <span>{get(statistics, 'result.data.users', 0)}</span>
                        </div>
                        <Link to="/users?status=" className="dashboard-stat stat-user">
                          <div className="desc">
                            Tài khoản <RightOutlined />
                          </div>
                        </Link>
                      </div>
                    </div>
                  </Col>
                )}
                {shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/xem') && (
                  <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                    <Link to="/courses" className="dashboard-stat stat-image">
                      <div className="visual">
                        <AppstoreOutlined />
                      </div>
                      <div className="detail">
                        <div className="number">
                          <span>{get(statistics, 'result.data.courses', 0)}</span>
                        </div>
                        <div className="desc">
                          Khóa học <RightOutlined />
                        </div>
                      </div>
                    </Link>
                  </Col>
                )}
                {shouldHaveAccessPermission('de_thi', 'de_thi/xem') && (
                  <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                    <div to="/exams" className="dashboard-stat stat-table">
                      <div className="visual">
                        <FileSearchOutlined />
                      </div>
                      <div className="detail">
                        <div className="number">
                          <span>{get(statistics, 'result.data.exams', 0)}</span>
                        </div>
                        <Link to="/exams" className="dashboard-stat stat-table">
                          <div className="desc">
                            Đề thi <RightOutlined />
                          </div>
                        </Link>
                      </div>
                    </div>
                  </Col>
                )}
                {shouldHaveAccessPermission('tin_tuc', 'tin_tuc/xem') && (
                  <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                    <div className="dashboard-stat stat-product">
                      <div className="visual">
                        <FileTextOutlined />
                      </div>
                      <div className="detail">
                        <div className="number">
                          <span>{get(statistics, 'result.data.posts', 0)}</span>
                        </div>
                        <Link to="/posts" className="dashboard-stat stat-product">
                          <div className="desc">
                            Tin tức <RightOutlined />
                          </div>
                        </Link>
                      </div>
                    </div>
                  </Col>
                )}
                {shouldHaveAccessPermission('thuc_don', 'thuc_don/xem') && (
                  <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                    <Link to="/cooks" className="dashboard-stat stat-image">
                      <div className="visual">
                        <SnippetsOutlined />
                      </div>
                      <div className="detail">
                        <div className="number">
                          <span>{get(statistics, 'result.data.cooks', 0)}</span>
                        </div>
                        <div className="desc">
                          Thực đơn <RightOutlined />
                        </div>
                        <div className="desc-group">
                          Nhóm thực đơn
                          <span>{get(statistics, 'result.data.cooksCate', 0)}</span>
                          <RightOutlined />
                        </div>
                      </div>
                    </Link>
                  </Col>
                )}
              </Row>
            </div>
          </div>
        </Col>
        <Col xs={24} sm={24} md={24} xl={12} xxl={12}>
          <div className="border-box-widget">
            <h2>Tài khoản mới</h2>
            <div className="border-box-body">
              <Table
                size="small"
                columns={[
                  {
                    title: 'Ảnh đại diện',
                    dataIndex: 'anh_dai_dien',
                    render: (anh_dai_dien, row) => {
                      return <W5dImageMinIO image={anh_dai_dien} shape="square" size={68} />;
                    },
                  },
                  {
                    title: 'Họ và tên',
                    dataIndex: 'ten_nhan_vien',
                    render: (val, row) => {
                      return <Link to={`/user/edit/${row.nhan_vien_id}`}>{val}</Link>;
                    },
                  },
                  {
                    title: 'E-mail',
                    dataIndex: 'email',
                    render: (val, row) => {
                      return <a href={`mailto:${val}`}>{val}</a>;
                    },
                  },
                ]}
                dataSource={get(statistics, 'result.data.NEW_USERS', []).map((item, index) => ({ ...item, key: index }))}
                pagination={{ pageSize: 5 }}
              />
            </div>
          </div>
        </Col>
        <Col xs={24} sm={24} md={24} xl={12} xxl={12}>
          <div className="border-box-widget">
            <h2>Tin tức mới</h2>
            <div className="border-box-body">
              <Table
                size="small"
                columns={[
                  {
                    title: 'Ảnh đại diện',
                    dataIndex: 'anh_dai_dien',
                    width: '20%',
                    render: (anh_dai_dien, row) => {
                      return <W5dImageMinIO image={anh_dai_dien} shape="square" size={68} />;
                    },
                  },
                  {
                    title: 'Tiêu đề',
                    dataIndex: 'tieu_de',
                    render: (val, row) => {
                      return <Link to={`/post/edit/${row.tin_tuc_id}`}>{val}</Link>;
                    },
                  },
                ]}
                dataSource={get(statistics, 'result.data.NEW_POSTS', []).map((item, index) => ({ ...item, key: index }))}
                pagination={{ pageSize: 5 }}
              />
            </div>
          </div>
        </Col>
        <Col xs={24} sm={24} md={24} xl={12} xxl={12}>
          <div className="border-box-widget">
            <h2>Khóa học mới</h2>
            <div className="border-box-body">
              <Table
                size="small"
                columns={[
                  {
                    title: 'Ảnh đại diện',
                    dataIndex: 'anh_dai_dien',
                    width: '20%',
                    render: (anh_dai_dien, row) => {
                      return <W5dImageMinIO image={anh_dai_dien} shape="square" size={68} />;
                    },
                  },
                  {
                    title: 'Tiêu đề',
                    dataIndex: 'tieu_de',
                    render: (val, row) => {
                      return <Link to={`/course/edit/${row.khoa_hoc_id}`}>{val}</Link>;
                    },
                  },
                ]}
                dataSource={get(statistics, 'result.data.NEW_COURSES', []).map((item, index) => ({ ...item, key: index }))}
                pagination={{ pageSize: 5 }}
              />
            </div>
          </div>
        </Col>
        <Col xs={24} sm={24} md={24} xl={12} xxl={12}>
          <div className="border-box-widget">
            <h2>Đề thi mới</h2>
            <div className="border-box-body">
              <Table
                size="small"
                columns={[
                  {
                    title: 'Ảnh đại diện',
                    dataIndex: 'anh_dai_dien',
                    width: '20%',
                    render: (anh_dai_dien, row) => {
                      return <W5dImageMinIO image={anh_dai_dien} shape="square" size={68} />;
                    },
                  },
                  {
                    title: 'Tiêu đề',
                    dataIndex: 'tieu_de',
                    render: (val, row) => {
                      return <Link to={`/exam/edit/${row.de_thi_id}`}>{val}</Link>;
                    },
                  },
                ]}
                dataSource={get(statistics, 'result.data.NEW_EXAMS', []).map((item, index) => ({ ...item, key: index }))}
                pagination={{ pageSize: 5 }}
              />
            </div>
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    statistic: state.statistic,
    config: state.config,
    user: state.user,
    log: state.log,
  };
};

export default connect(mapStateToProps)(Home);
