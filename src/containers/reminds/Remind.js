// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import moment from 'moment';
import { Link } from 'react-router-dom';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { Row, Col, Form, Input, List, Button, Checkbox, notification, Space, Dropdown, Menu, Modal, Avatar, TimePicker, Select } from 'antd';
import { DeleteOutlined, DownOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { get } from 'lodash';
// import internal libs
import * as remindAction from 'redux/actions/remind';
import { config } from 'config';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import App from 'App';
import AppFilter from 'components/AppFilter';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import W5dStatusTag from '../../components/status/W5dStatusTag';
import defaultImage from 'assets/images/default.jpg';
import W5dImageMinIO from 'components/W5dImageMinIO';

const dateFormat = 'DD-MM-YYYY';
const { TextArea } = Input;
const { Option } = Select;
const { confirm } = Modal;

function Remind(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const defaultForm = {
    nhom_nhac_nho_id: '',
    thoi_gian: '',
    trang_thai: '',
  };
  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    openMediaLibrary: false,
    body: defaultForm,
    search: {
      ...props.remind.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });
  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const changeMutilUrlParams = (fields) => {
    //const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const searchReminds = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.remind.list.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(remindAction.getReminds(searchParams));
    form.setFieldsValue(state.body);
  };

  useEffect(() => {
    searchReminds();
  }, [props.location]);

  useEffect(() => {
    props.dispatch(remindAction.getRemindCates());
  }, []);

  useEffect(() => {
    window.onbeforeunload = null;
  }, [props]);

  const openMediaLibrary = () => setState((state) => ({ ...state, openMediaLibrary: true }));

  // list functions
  const allOptions = get(props, 'remind.list.result.data', []).map((item) => item.nhac_nho_id);
  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (nhac_nho_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(nhac_nho_id) ? state.checkedList.filter((it) => it !== nhac_nho_id) : state.checkedList.concat([nhac_nho_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      body: {
        ...state.body,
        anh_dai_dien: file.tep_tin_url,
      },
    }));
  };

  const onEditCategory = (item) => {
    setState((state) => ({
      ...state,
      isEdit: true,
      body: item,
    }));
    form.setFieldsValue(item);
  };

  const onDeleteCategory = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Khóa nhắc nhở thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa nhắc nhở thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        searchReminds();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(remindAction.deleteRemind({ id }, callback));
      },
    });
  };

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };

  function onDeleteReminds() {
    const callback = (res) => {
      if (res.success) {
        searchReminds();
        setState((state) => ({
          ...state,
          checkedList: [],
        }));
        notification.success({
          message: 'Xóa nhiều nhắc nhở thành công',
        });
      } else {
        notification.error({
          message: res.error,
        });
      }
    };
    confirm({
      title: 'Không thể xóa nhắc nhở nếu có nhắc nhở thuộc nhóm đó, bạn chắc chắn muốn xóa?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(remindAction.deleteReminds({ listIds: state.checkedList }, callback));
      },
    });
  }

  const removeImage = () => setState((state) => ({ ...state, body: { ...state.body, anh_dai_dien: '' } }));

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
  };

  const shoudlDisableFrom = () => {
    if (state.isEdit && shouldHaveAccessPermission('nhac_nho', 'nhac_nho/sua')) return false;
    if (!state.isEdit && shouldHaveAccessPermission('nhac_nho', 'nhac_nho/them')) return false;
    return true;
  };

  const handleSubmit = (values) => {
    if (state.isEdit) {
      // edit a category
      const callback = (result) => {
        searchReminds();
        changeUrlParams('edit', '');
        notification.success({
          message: 'Cập nhật nhắc nhở thành công',
        });
        setState((state) => ({
          ...state,
          body: result.data,
        }));
      };
      props.dispatch(remindAction.editRemind({ ...state.body, ...values }, callback));
    } else {
      //add a new category
      const callback = () => {
        searchReminds();
        notification.success({
          message: 'Tạo mới nhắc nhở thành công',
        });
        setState((state) => ({
          ...state,
          isChanged: false,
        }));
      };
      props.dispatch(remindAction.createRemind({ ...state.body, ...values }, callback));
    }
    setState((state) => ({
      ...state,
      body: defaultForm,
      isEdit: false,
      isChanged: false,
    }));
    form.resetFields();
  };

  const onResetForm = () => {
    setState((state) => ({
      ...state,
      body: defaultForm,
      isEdit: false,
    }));
    form.resetFields();
  };

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };

  const renderCates = () => {
    const defaultCate = urlQuery.categories ? urlQuery.categories.split('_') : [];
    const cates = get(props, 'remind.listCates.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.nhom_nhac_nho_id}>
          {cate.ten_nhom}
        </Option>
      );
    });
    return (
      <Select
        disabled={shoudlDisableFrom()}
        value={defaultCate}
        onChange={(value) => {
          if (!state.isChanged) {
            setState((state) => ({
              ...state,
              isChanged: true,
            }));
          }
        }}
        placeholder="Chọn nhóm"
      >
        {catesOpts}
      </Select>
    );
  };

  const renderStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select
        disabled={shoudlDisableFrom()}
        defaultValue={state.trang_thai}
        value={state.trang_thai}
        onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })}
        placeholder={getLangText('GLOBAL.SELECT_STATUS')}
      >
        {options}
      </Select>
    );
  };

  const menu = (
    <Menu>
      {shouldHaveAccessPermission('nhac_nho', 'nhac_nho/xoa') && (
        <Menu.Item key="1" disabled={state.checkedList.length === 0 ? true : false} icon={<DeleteOutlined />} onClick={onDeleteReminds}>
          Xóa danh mục nhắc nhở
        </Menu.Item>
      )}
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>{getLangText('FORM_BUILDER.FORM_BUILDERS_CATEGORIES')}</title>
      </Helmet>
      {props.remind.list.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={8} sm={8} xs={24} className="cate-form-block">
              <h2>Thêm mới</h2>
              <Form layout="vertical" className="category-remind" form={form} onFinish={handleSubmit}>
                <Form.Item
                  className="input-col"
                  label="Nhóm nhắc nhở"
                  name="nhom_nhac_nho_id"
                  rules={[
                    {
                      required: true,
                      message: 'Nhóm nhắc nhở là trường bắt buộc',
                    },
                  ]}
                >
                  {renderCates()}
                </Form.Item>
                <Form.Item
                  className="input-col"
                  label="Thời gian"
                  name="thoi_gian"
                  rules={[
                    {
                      required: true,
                      message: 'Thời gian là trường bắt buộc',
                    },
                  ]}
                >
                  <Input
                    disabled={shoudlDisableFrom()}
                    placeholder="Thời gian"
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                {/* <Form.Item
                  className="input-col"
                  label="Thời gian"
                  name="thoi_gian"
                  rules={[
                    {
                      required: true,
                      message: 'Thời gian là trường bắt buộc',
                    },
                  ]}
                >
                  <TimePicker
                    disabled={shoudlDisableFrom()}
                    showNow={false}
                    locale={locale}
                    style={{ width: '100%' }}
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                    format="HH:mm"
                    placeholder="Thời gian"
                  />
                </Form.Item> */}
                <Form.Item
                  label="Trạng thái"
                  className="input-col"
                  name="trang_thai"
                  rules={[
                    {
                      required: true,
                      message: 'Trạng thái là trường bắt buộc.',
                    },
                  ]}
                >
                  {renderStatus()}
                </Form.Item>
                <Form.Item className="button-col">
                  <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableFrom()}>
                    {state.isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.ADD_NEW')}
                  </Button>
                  {state.isEdit && (
                    <Button shape="round" type="danger" onClick={() => onResetForm()}>
                      {getLangText('FORM.CANCEL')}
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
            <Col span={1}></Col>
            <Col xl={15} sm={15} xs={24}>
              <div className="w5d-list">
                <AppFilter
                  isShowCategories={false}
                  isShowStatus={false}
                  isShowSearchBox={true}
                  isShowDatePicker={true}
                  isRangeDatePicker={true}
                  title="Danh sách"
                  search={state.search}
                  onDateChange={(dates) => onDateChange(dates)}
                  onFilterChange={(field, value) => onFilterChange(field, value)}
                />
                <Row className="select-action-group-cate">
                  <Space wrap>
                    <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                      <Button>
                        Chọn hành động
                        <DownOutlined />
                      </Button>
                    </Dropdown>
                  </Space>
                </Row>
                <List
                  locale={{
                    emptyText: getLangText('GLOBAL.NO_ITEMS'),
                  }}
                  header={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={6} className="text-left">
                        Nhóm
                      </Col>
                      <Col span={6} className="text-left">
                        Ảnh đại diện
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4} className="text-left">
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4} className="text-left">
                        {getLangText('GLOBAL.UPDATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_sua'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={6} className="text-left">
                        Nhóm
                      </Col>
                      <Col span={6} className="text-left">
                        Ảnh đại diện
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4} className="text-left">
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4} className="text-left">
                        {getLangText('GLOBAL.UPDATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_sua'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  dataSource={get(props, 'remind.list.result.data', [])}
                  pagination={{
                    hideOnSinglePage: false,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.nhac_nho_id}>
                      <Row className="full">
                        <Col span={1}>
                          <Checkbox checked={state.checkedList.includes(item.nhac_nho_id)} value={item.nhac_nho_id} onChange={() => onChangeCheck(item.nhac_nho_id)} />
                        </Col>
                        <Col span={6} className="text-left">
                          <Link className="edit" to={`/reminds?categories=${item.nhac_nho}`}>
                            {item.nhom.ten_nhom} lúc {item.thoi_gian}
                          </Link>

                          <div className="actions">
                            {shouldHaveAccessPermission('nhac_nho', 'nhac_nho/sua') && (
                              <Button className="edit act" type="link" onClick={() => onEditCategory(item)}>
                                {getLangText('GLOBAL.EDIT')}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('nhac_nho', 'nhac_nho/xoa') && (
                              <Button className="delete act" type="link" onClick={() => onDeleteCategory(item.nhac_nho_id, item.trang_thai)}>
                                {item.trang_thai === 'active' ? 'Khóa' : 'Xóa'}
                              </Button>
                            )}
                          </div>
                        </Col>

                        <Col span={6} className="text-left">
                          <W5dImageMinIO image={item.nhom.anh_dai_dien} />
                        </Col>
                        <Col span={3} className="text-left">
                          <W5dStatusTag status={item.trang_thai} />
                        </Col>
                        <Col span={4} className="text-left">
                          {moment(item.ngay_tao).isValid() ? moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT) : '-'}
                        </Col>
                        <Col span={4} className="text-left">
                          {moment(item.ngay_sua).isValid() ? moment(item.ngay_sua).utc(0).format(config.DATE_FORMAT) : '-'}
                        </Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={state.openMediaLibrary}
        onCancel={() =>
          setState((state) => ({
            ...state,
            openMediaLibrary: false,
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    remind: state.remind,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Remind);
