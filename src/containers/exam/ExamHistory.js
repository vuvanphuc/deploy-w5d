// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
// import InfiniteScroll from 'react-infinite-scroller';
import { get } from 'lodash';
import { Row, Col, List, Avatar, Button, Checkbox, notification, Menu, Dropdown, Space, Modal, Upload, Image, Switch, Form } from 'antd';
import { DownOutlined, LockOutlined, DeleteOutlined, UnlockOutlined, QuestionCircleOutlined, DownloadOutlined, UploadOutlined, FolderOutlined } from '@ant-design/icons';

// import internal libs
import * as examAction from 'redux/actions/exam';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl, getUserInformation } from 'helpers/common.helper';
import { config } from 'config';
import templateFile from 'assets/templates/User_temp.xlsx';
import App from 'App';
import constants from 'constants/global.constants';
import AppFilter from 'components/AppFilter';
import W5dStatusTag from 'components/status/W5dStatusTag';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import { ExportToExcel } from 'helpers/ExportToExcel';
import W5dImageMinIO from 'components/W5dImageMinIO';

const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';

function ExamHistory(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);
  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };
  const searchExamHistories = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.exam.listHistories.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(examAction.getExamHistories(searchParams));
    //form.setFieldsValue(state.body);
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.exam.listHistories.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });
  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };
  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
      checkAll: 0,
      checkedList: [],
    }));
    changeUrlParams(field, value);
  };
  const onDelete = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Block lịch sử thi thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa lịch sử thi thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        searchExamHistories();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(examAction.deleteExamHistory({ id }, callback));
      },
    });
  };

  const allOptions = get(props, 'exam.listHistories.result.data', []).map((item) => item.lich_su_thi_id);

  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (lich_su_thi_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(lich_su_thi_id) ? state.checkedList.filter((it) => it !== lich_su_thi_id) : state.checkedList.concat([lich_su_thi_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  function deleteListExamHistories(e) {
    const callback = (res) => {
      if (res.success) {
        searchExamHistories();
        notification.success({
          message: 'Xóa nhiều lịch sử thi thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các lịch sử thi được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(examAction.deleteExamHistories({ listIds: state.checkedList }, callback));
      },
    });
  }
  //Active thanh vien theo list => active
  function activeListExamHistories(e) {
    const callback = (res) => {
      if (res.success) {
        searchExamHistories();
        notification.success({
          message: 'Kích hoạt lịch sử thi thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các lịch sử thi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          // props.dispatch(examAction.editStatusExamHistory({ listIds: state.checkedList, status: 'active' }, callback));
        },
      });
    }
  }
  //Khoa thanh vien theo list => inactive
  function blockListExamHistories(e) {
    const callback = (res) => {
      if (res.success) {
        searchExamHistories();
        notification.success({
          message: 'Khóa lịch sử thi thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn chắc chắn muốn khóa những lịch sử thi được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          // props.dispatch(
          //   examAction.editStatusExamHistory(
          //     {
          //       listIds: state.checkedList,
          //       status: 'inactive',
          //     },
          //     callback
          //   )
          // );
        },
      });
    }
  }

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListExamHistories} disabled={!shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/xoa') || state.checkedList.length <= 0}>
        Xóa lịch sử thi
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeListExamHistories}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt lịch sử thi
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockListExamHistories}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/sua') || state.checkedList.length <= 0}
      >
        Khóa lịch sử thi
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.exam.listHistories.search,
      ...urlQuery,
      loading: true,
    };
    props.dispatch(examAction.getExamHistories(searchParams));
  }, [props.location]);

  const findCate = (id) => {
    const cates = get(props, 'exam.listHistoriesCates.result.data', []);
    const cate = cates.find((ct) => ct.nhom_lich_su_thi_id === id);
    return cate ? cate.ten_nhom : '';
  };
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const saveFile = () => {
    FileSaver.saveAs(templateFile, 'tempUser.xlsx');
  };

  function processExcel(data) {
    const workbook = XLSX.read(data, { type: 'binary' });
    const firstSheet = workbook.SheetNames[0];
    const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    const onSuccess = () => {
      notification.success({
        message: 'Import biểu mẫu thành công',
      });
    };
    const onError = () => {
      const urlQuery = queryString.parse(props.location.search);
      const searchParams = {
        ...props.exam.listHistories.search,
        ...urlQuery,
        loading: false,
      };
      props.dispatch(examAction.getExamHistories(searchParams));
    };
    //props.dispatch(postAction.importPosts({ listUsers: excelRows }, onSuccess, onError));
  }

  const customRequest = async ({ file }) => {
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      if (reader.readAsBinaryString) {
        reader.onload = (e) => {
          processExcel(reader.result);
        };
        reader.readAsBinaryString(file);
      }
    } else {
      notification.error({
        message: 'Không đọc được file',
      });
    }
  };

  const findVersion = (type) => {
    const cates = get(constants, 'EXAMS_VERSIONS', []);
    const cate = cates.find((ct) => ct.value === type);
    return cate ? cate.label : '---';
  };

  const menuImport = (
    <Menu>
      <Menu.Item key="1" icon={<DownloadOutlined />} onClick={saveFile}>
        Tải xuống file mẫu
      </Menu.Item>
      <Menu.Item key="2" icon={<UploadOutlined />}>
        <Upload id="uploadFile" multiple={false} accept=".xls,.xlsx" showUploadList={false} customRequest={(data) => customRequest(data)}>
          Đẩy lên dữ liệu
        </Upload>
      </Menu.Item>
    </Menu>
  );

  return (
    <App>
      <Helmet>
        <title>Quản lý lịch sử thi</title>
      </Helmet>
      {props.exam.listHistories.loading && <Loading />}
      <Row className="app-main">
        <Col xl={24} className="body-content">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                title="Quản lý lịch sử thi"
                isShowCategories={false}
                isShowStatus={true}
                isShowSearchBox={true}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                categories={get(props, 'exam.listHistoriesCates.result.data', []).map((cate) => ({
                  name: cate.ten_nhom,
                  id: cate.nhom_lich_su_thi_id,
                }))}
                search={state.search}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group" gutter={[8, 8]}>
            <Col xl={12} sm={12} xs={24}>
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
            </Col>
            <Col xl={12} sm={12} xs={24} className="right-actions"></Col>
          </Row>

          <div className="w5d-list">
            <List
              locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
              header={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={2} xl={2}>
                    Ảnh đại diện
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Họ và tên
                    <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Đề thi
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Điểm
                    <Sorting urlQuery={urlQuery} field={'tin_noi_bat'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Kết quả chi tiết
                    <Sorting urlQuery={urlQuery} field={'tin_moi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={2} xl={2} className="text-left">
                    Thời gian làm
                    <Sorting urlQuery={urlQuery} field={'tin_moi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={2} xl={2} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={2} xl={2} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              footer={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={2} xl={2}>
                    Ảnh đại diện
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Họ và tên
                    <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Đề thi
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Điểm
                    <Sorting urlQuery={urlQuery} field={'tin_noi_bat'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Kết quả chi tiết
                    <Sorting urlQuery={urlQuery} field={'tin_moi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={2} xl={2} className="text-left">
                    Thời gian làm
                    <Sorting urlQuery={urlQuery} field={'tin_moi'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={2} xl={2} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={2} xl={2} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              dataSource={get(props, 'exam.listHistories.result.data', [])}
              pagination={{
                hideOnSinglePage: false,
                responsive: true,
                showLessItems: true,
                pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                pageSize: pageSize,
                onChange: (page, pageSize) => {
                  changeMutilUrlParams({ pageSize, page });
                  setPageSize(pageSize);
                  setPage(page);
                },
                current: Number(page),
                showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                showSizeChanger: true,
              }}
              renderItem={(item) => (
                <List.Item key={item.lich_su_thi_id} id="listUsers">
                  <Row className="full">
                    <Col xs={1} xl={1}>
                      <Checkbox checked={state.checkedList.includes(item.lich_su_thi_id)} value={item.lich_su_thi_id} onChange={() => onChangeCheck(item.lich_su_thi_id)} />
                    </Col>
                    <Col xs={2} xl={2}>
                      <Link className="view" to={shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/chi_tiet') ? `/exam/detail/${item.lich_su_thi_id}` : '#'}>
                        <W5dImageMinIO image={item.anh_dai_dien} />
                      </Link>
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      <Link className="edit" to="#">
                        {get(item, 'nguoi_tao', '')}
                      </Link>
                      <div className="actions">
                        {shouldHaveAccessPermission('lich_su_thi', 'lich_su_thi/xem') && (
                          <Button className="view act" type="link">
                            Xem
                          </Button>
                        )}
                      </div>
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      {get(item, 'de_thi_id', '')}
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      {get(item, 'diem', 0).toFixed(2)} / 10
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      Số câu đúng: {item.so_cau_dung || 0} <br />
                      Số câu sai: {item.so_cau_sai || 0} <br />
                      Số câu không làm: {item.so_cau_khong_lam || 0} <br />
                    </Col>
                    <Col xs={2} xl={2} className="text-left">
                      {Math.floor(item.thoi_gian / 60)}:{item.thoi_gian % 60}
                    </Col>
                    <Col xs={2} xl={2} className="text-left">
                      <W5dStatusTag status={item.trang_thai} />
                    </Col>
                    <Col xs={2} xl={2} className="text-left">
                      {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    exam: state.exam,
  };
};
export default connect(mapStateToProps)(ExamHistory);
