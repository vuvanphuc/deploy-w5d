// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Button, Form, Input, InputNumber, Row, Col, Select, Modal, Tabs, Radio, Steps, Result, List, notification } from 'antd';
import { RightOutlined, LeftOutlined, SaveOutlined, CloseOutlined } from '@ant-design/icons';
import moment from 'moment';
// import internal libs
import App from 'App';
import { config } from 'config';
import constants from 'constants/global.constants';
import { getLangText } from 'helpers/language.helper';
import { getUserInformation, shouldHaveAccessPermission, recursiveUserCates, recursiveCates } from 'helpers/common.helper';
import * as questionAction from 'redux/actions/question';
import * as examAction from 'redux/actions/exam';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import Loading from 'components/loading/Loading';
import defaultImage from 'assets/images/default.jpg';
import nottickImg from 'assets/images/ic_tick_disabled.png';
import tickImg from 'assets/images/ic_tick.png';
import TextEditor from 'containers/form/components/widgets/TextEditor/TextEditor';
import './ExamForm.css';

const { Option } = Select;
const { TextArea } = Input;
const { TabPane } = Tabs;
const { Step } = Steps;

const defaultExam = {
  tieu_de: '',
  chu_de_id: '',
  mo_ta: '',
  noi_dung: '',
  hinh_thuc: 'TU_LUYEN',
  anh_dai_dien: '',
  nhom_de_thi_id: '',
  trang_thai: 'inactive',
  so_luong_cau_hoi: 150,
  thoi_gian: 150,
};

const defaultQuestion = {
  tieu_de: '',
  loi_giai: '',
  chu_de_id: '',
  de_thi_id: '',
  loai_cau_hoi: '',
  kieu_cau_hoi: 'LUA_CHON',
  bo_mon: '',
  dap_an_dung: '',
  kieu_hien_thi_dap_an: 'HANG_1_COT',
  nhom_cau_hoi_id: '',
  trang_thai: 'active',
  dap_an: [
    { tieu_de: '', label: 'Đáp án A', key: 'A' },
    { tieu_de: '', label: 'Đáp án B', key: 'B' },
    { tieu_de: '', label: 'Đáp án C', key: 'C' },
    { tieu_de: '', label: 'Đáp án D', key: 'D' },
  ],
};

const defaultForm = {
  ...defaultExam,
  cau_hoi: [],
  uploadLoading: false,
  isChanged: false,
  openMediaLibrary: false,
  insertMedia: {
    open: false,
    editor: null,
  },
};

function ExamForm(props) {
  const [form] = Form.useForm();
  const [questionForm] = Form.useForm();
  const history = useHistory();
  const examId = get(props, 'match.params.id', '');
  const exam = get(props, 'exam.item.result.data', {});
  const [currentStep, setCurrentStep] = useState(0);
  const [currentQuestion, setCurrentQuestion] = useState(defaultQuestion);
  const [questions, setQuestions] = useState([]);
  const [state, setState] = useState(defaultForm);
  const [isOpenEditor, setIsOpenEditor] = useState(false);

  const next = async () => {
    if (currentStep === 0) {
      try {
        await form.validateFields();
        setCurrentStep(1);
        form.submit();
      } catch (error) {
        console.log('Error', error);
      }
    } else if (currentStep === 1) {
      if (state.so_luong_cau_hoi > questions.length) {
        Modal.warning({
          title: 'Thông báo',
          content: 'Bạn chưa nhập đủ số lượng câu hỏi.',
        });
      } else if (state.so_luong_cau_hoi < questions.length) {
        Modal.warning({
          title: 'Thông báo',
          content: 'Bạn chưa nhập quá số lượng câu hỏi.',
        });
      } else {
        setCurrentStep(2);
      }
    } else {
      setCurrentStep(2);
    }
  };

  const save = () => {
    if (state.isChanged) {
      form.submit();
    }
  };

  const isEdit = props.match && props.match.params && props.match.params.id;
  const openMediaLibrary = () => setState((state) => ({ ...state, openMediaLibrary: true }));

  const handleSaveExam = (values, force = false) => {
    if (props.match.params.id && (state.isChanged || force)) {
      const callback = (res) => {
        if (res.success) {
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
          notification.success({
            message: 'Cập nhật đề thi thành công.',
          });
        }
      };
      const newFormData = {
        ...state,
        ...values,
      };
      setState((state) => ({
        ...state,
        ...newFormData,
      }));
      props.dispatch(examAction.editExam(newFormData, callback));
    }
  };

  const removeImage = () => setState((state) => ({ ...state, anh_dai_dien: '' }));

  const renderExamCategories = () => {
    let options = [];
    if (props.exam.listCates.result && props.exam.listCates.result.data) {
      options = props.exam.listCates.result.data.map((cate) => (
        <Option key={cate.nhom_de_thi_id} value={cate.nhom_de_thi_id}>
          {cate.ten_nhom}
        </Option>
      ));
      return (
        <Select
          showSearch={false}
          value={state.nhom_de_thi_id}
          loading={props.exam.listCates.loading}
          onChange={(nhom_de_thi_id) => setState({ ...state, nhom_de_thi_id, isChanged: true })}
          placeholder={getLangText('GLOBAL.SELECT_CATEGORIES')}
        >
          {options}
        </Select>
      );
    }
  };

  const renderExamStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.value === 'active' ? 'Đã hoàn thiện' : 'Chưa hoàn thiện'}
      </Option>
    ));
    return (
      <Select defaultValue={state.trang_thai} value={state.trang_thai} onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })} placeholder={getLangText('GLOBAL.SELECT_STATUS')}>
        {options}
      </Select>
    );
  };

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      anh_dai_dien: file.tep_tin_url,
    }));
  };

  const onInsertImage = (file) => {
    state.insertMedia.editor.insertContent('<img className="image-editor" src="' + `${config.UPLOAD_API_URL}/${file.imgUrl}` + '"/>');
    setState((state) => ({
      ...state,
      isChanged: true,
      insertMedia: {
        open: false,
      },
    }));
  };

  const onResetForm = () => {
    if (props.exam.item.result && isEdit) {
      form.setFieldsValue(props.exam.item.result.data);
      setState((state) => ({ ...state, ...props.exam.item.result.data, isChanged: false }));
    } else {
      form.resetFields();
      setState((state) => ({ ...state, ...defaultForm, isChanged: false }));
    }
  };

  function mysql_real_escape_string(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
      switch (char) {
        case '\0':
          return '\\0';
        case '\x08':
          return '\\b';
        case '\x09':
          return '\\t';
        case '\x1a':
          return '\\z';
        case '\n':
          return '\\n';
        case '\r':
          return '\\r';
        case '"':
        case "'":
        case '\\':
        case '%':
          return '\\' + char; // prepends a backslash to backslash, percent,
        // and double/single quotes
        default:
          return char;
      }
    });
  }

  const handleSaveQuestion = (values) => {
    const dap_an = {
      dap_an_a: mysql_real_escape_string(values.dap_an[0].tieu_de),
      dap_an_b: mysql_real_escape_string(values.dap_an[1].tieu_de),
      dap_an_c: mysql_real_escape_string(values.dap_an[2].tieu_de),
      dap_an_d: mysql_real_escape_string(values.dap_an[3].tieu_de),
    };

    const newFormData = {
      ...currentQuestion,
      ...values,
      dap_an,
      tieu_de: mysql_real_escape_string(values.tieu_de),
      loi_giai: mysql_real_escape_string(values.loi_giai),
      de_thi_id: examId,
    };

    if (currentQuestion.cau_hoi_id) {
      // edit a question
      const callback = (res) => {
        if (res.success) {
          if (isEdit) {
            const newQuestions = questions.map((question) => {
              return question.cau_hoi_id === currentQuestion.cau_hoi_id ? { ...res.data } : question;
            });
            setQuestions(newQuestions);
            setCurrentQuestion(defaultQuestion);
            questionForm.setFieldsValue(defaultQuestion);
            notification.success({
              message: 'Cập nhật câu hỏi thành công.',
            });
          }
        }
      };

      props.dispatch(questionAction.editQuestion(newFormData, callback));
    } else {
      //add a new question
      const callback = (res) => {
        if (res.success) {
          setQuestions([...questions, { ...res.data }]);
          setCurrentQuestion(defaultQuestion);
          questionForm.setFieldsValue(defaultQuestion);
          notification.success({
            message: 'Thêm mới câu hỏi thành công.',
          });
        }
      };
      props.dispatch(questionAction.createQuestion(newFormData, callback));
    }
  };

  const handleRemoveQuestion = (cau_hoi_id) => {
    if (cau_hoi_id) {
      const callback = (res) => {
        if (res.success) {
          const newQuestions = questions.filter((question) => question.cau_hoi_id !== cau_hoi_id);
          setQuestions(newQuestions);
          setCurrentQuestion(defaultQuestion);
          questionForm.setFieldsValue(defaultQuestion);
        }
      };
      props.dispatch(questionAction.deleteQuestion({ id: cau_hoi_id }, callback));
    }
  };

  const renderQuestions = () => {
    const pointPerQuestion = (10 / exam.so_luong_cau_hoi).toFixed(1);
    const questionArr = questions.map((question, index) => {
      return (
        <div
          className={`${currentQuestion.cau_hoi_id === question.cau_hoi_id ? 'active' : ''} item`}
          key={index}
          onClick={() => {
            setCurrentQuestion(question);
            questionForm.setFieldsValue(question);
          }}
        >
          {state.trang_thai !== 'active' && (
            <CloseOutlined
              title="Xóa câu hỏi"
              onClick={() => {
                questionForm.setFieldsValue(defaultQuestion);
                Modal.confirm({
                  title: 'Xóa câu hỏi',
                  content: 'Bạn có chắc chắn muốn xóa câu hỏi này không?',
                  okText: 'Có',
                  cancelText: 'Không',
                  onOk: () => handleRemoveQuestion(question.cau_hoi_id),
                });
              }}
            />
          )}

          <div className="header-question">
            Câu {index + 1} <span className="point">[{pointPerQuestion} điểm]</span>
          </div>
          <div className="body-question">
            {question.dap_an.map((item, idx) => {
              return (
                <div className="answer-detail" key={idx}>
                  {question.dap_an_dung === item.key && <img src={tickImg} />}
                  {question.dap_an_dung !== item.key && <img src={nottickImg} />}
                  <div className="answer-position">{item.key}</div>
                </div>
              );
            })}
          </div>
        </div>
      );
    });
    return <div className="question-items">{questionArr}</div>;
  };

  useEffect(() => {
    const questionArr = get(props, 'question.list.result.data', []);
    setQuestions(questionArr);
  }, [props.question.list.result]);

  useEffect(() => {
    if (isEdit && props.exam.item.result) {
      form.setFieldsValue(props.exam.item.result.data);
      setState((state) => ({ ...state, ...props.exam.item.result.data, isChanged: false }));
    }
    window.onbeforeunload = null;
  }, [props.exam.item.result]);

  useEffect(() => {
    props.dispatch(examAction.getExamCates());
    if (isEdit) {
      props.dispatch(
        examAction.getExam({
          de_thi_id: examId,
        })
      );
      props.dispatch(questionAction.getQuestions({ de_thi_id: examId, status: 'active' }));
      props.dispatch(examAction.getExamHistories({ keyword: examId }));
    }
  }, []);

  console.log('state', state);

  return (
    <App>
      <Helmet>
        <title>{isEdit ? `Sửa đề thi` : 'Thêm mới đề thi'}</title>
      </Helmet>
      {props.exam.item.loading && <Loading />}
      <div className="exam-page">
        <Row className="app-main">
          <Col span={24} className="body-content">
            <div className={`w5d-form ${isOpenEditor ? 'open-editor' : ''}`}>
              <Row gutter={25}>
                <Col xs={24}>
                  <Steps current={currentStep}>
                    <Step title="Thông tin chung" />
                    <Step title="Danh sách câu hỏi" />
                    {state.trang_thai === 'active' ? <Step title="Kết quả" /> : <Step title="Hoàn thành" />}
                  </Steps>
                </Col>
              </Row>
              <Tabs activeKey={`step_${currentStep}`}>
                <TabPane tab="Thông tin" key="step_0">
                  <Form layout="vertical" className="ExamForm" onFinish={handleSaveExam} form={form} initialValues={defaultExam}>
                    <Row gutter={25}>
                      <Col xl={18} sm={24} xs={24} className="left-content">
                        <div className="border-box">
                          <Row gutter={25}>
                            <Col xl={24} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Tên gọi đề thi"
                                name="tieu_de"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên gọi đề thi là trường bắt buộc',
                                  },
                                ]}
                              >
                                <Input
                                  placeholder="Tên gọi đề thi"
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                          <Form.Item className="input-col" label="Mô tả" name="mo_ta" rules={[]}>
                            <TextEditor
                              value={state.mo_ta}
                              placeholder="Mô tả là trường bắt buộc"
                              onChange={(val) => {
                                if (val !== state.mo_ta) {
                                  setState({ ...state, mo_ta: val, isChanged: true });
                                }
                              }}
                              isSimple={true}
                            />
                          </Form.Item>
                          <Form.Item className="input-col" label="Nội dung chi tiết" name="noi_dung" rules={[]}>
                            <TextEditor
                              value={state.mo_ta}
                              placeholder="Nội dung chi tiết"
                              onChange={(val) => {
                                if (val !== state.noi_dung) {
                                  setState({ ...state, noi_dung: val, isChanged: true });
                                }
                              }}
                              isSimple={true}
                            />
                          </Form.Item>
                        </div>
                        <div className="border-box">
                          <Row gutter={25}>
                            <Col xl={24} sm={24} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Hình thức kiểm tra"
                                name="hinh_thuc"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Hình thức kiểm tra là trường bắt buộc',
                                  },
                                ]}
                              >
                                <Radio.Group options={constants.EXAMS_VERSIONS} optionType="button" buttonStyle="solid" />
                              </Form.Item>
                            </Col>
                            <Col xl={12} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Số lượng câu hỏi"
                                name="so_luong_cau_hoi"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Số lượng câu hỏi là trường bắt buộc',
                                  },
                                ]}
                              >
                                <InputNumber
                                  min={0}
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                />
                              </Form.Item>
                            </Col>
                            <Col xl={12} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Thời gian (phút)"
                                name="thoi_gian"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Thời gian là trường bắt buộc',
                                  },
                                ]}
                              >
                                <InputNumber
                                  min={0}
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col xl={6} sm={24} xs={24} className="right-content">
                        <div className="box">
                          <div className="box-body">
                            <div
                              className="border-box"
                              style={{
                                display: !shouldHaveAccessPermission('de_thi', 'de_thi/sua') ? 'none' : 'block',
                              }}
                            >
                              <h2>
                                <span style={{ color: '#ff4d4f' }}>*</span> {getLangText('GLOBAL.CATEGORIES')}
                              </h2>
                              <Form.Item
                                className="input-col"
                                name="nhom_de_thi_id"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Nhóm đề thi là trường bắt buộc.',
                                  },
                                ]}
                              >
                                {renderExamCategories()}
                              </Form.Item>
                            </div>
                            <W5dMediaBrowser
                              image={state.anh_dai_dien ? `${state.anh_dai_dien}` : defaultImage}
                              field="image"
                              setImageLabel={getLangText('GLOBAL.SET_AVATAR')}
                              removeImageLabel={getLangText('GLOBAL.REMOVE_AVATAR')}
                              title={getLangText('GLOBAL.AVATAR')}
                              openMediaLibrary={() => openMediaLibrary()}
                              removeImage={() => removeImage()}
                            />
                            <div className="border-box hideDesktop">
                              <h2>{getLangText('FORM.PUBLISH')}</h2>
                              <Form.Item className="input-col">
                                <Row>
                                  <Col span="11">
                                    <Button block type="danger" htmlType="reset" onClick={() => onResetForm()}>
                                      {getLangText('FORM.RESET')}
                                    </Button>
                                  </Col>
                                  <Col span="2"></Col>
                                  <Col span="11">
                                    <Button block type="primary" htmlType="submit">
                                      {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </Form>
                </TabPane>
                <TabPane tab="Tạo câu hỏi" key="step_1">
                  <Form layout="vertical" className="QuestionForm" onFinish={handleSaveQuestion} form={questionForm} initialValues={defaultQuestion}>
                    <Row gutter={25}>
                      <Col xl={18} sm={24} xs={24} className="left-content">
                        <div className="border-box question-content">
                          <Row gutter={0}>
                            <Col xl={24} sm={24} xs={24}>
                              <Row>
                                <Col xl={4}>
                                  <Form.Item className="label">
                                    <span style={{ color: '#ff4d4f' }}>*</span>Nội dung câu hỏi
                                  </Form.Item>
                                </Col>
                                <Col xl={20}>
                                  <Form.Item
                                    className="input-col"
                                    label=""
                                    name="tieu_de"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Nội dung câu hỏi là trường bắt buộc',
                                      },
                                    ]}
                                  >
                                    <TextEditor
                                      disabled={state.trang_thai === 'active'}
                                      value={currentQuestion.tieu_de}
                                      placeholder="Thêm nội dung câu hỏi"
                                      onChange={(val) => setCurrentQuestion({ ...currentQuestion, tieu_de: val })}
                                      openEditor={(val) => setIsOpenEditor(val)}
                                      isSimple={true}
                                    />
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Row>
                                <Col xl={4}>
                                  <Form.Item className="label">
                                    <span style={{ color: '#ff4d4f' }}>*</span>Loại câu hỏi
                                  </Form.Item>
                                </Col>
                                <Col xl={20}>
                                  <Form.Item
                                    className="input-col"
                                    label=""
                                    name="loai_cau_hoi"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Loại câu hỏi là trường bắt buộc',
                                      },
                                    ]}
                                  >
                                    <Radio.Group options={constants.QUESTIONS_TYPES} disabled={state.trang_thai === 'active'} optionType="button" buttonStyle="solid" />
                                  </Form.Item>
                                </Col>
                              </Row>

                              <Row>
                                <Col xl={4}>
                                  <Form.Item className="label">
                                    <span style={{ color: '#ff4d4f' }}>*</span>Lĩnh vực/môn học
                                  </Form.Item>
                                </Col>
                                <Col xl={20}>
                                  <Form.Item
                                    className="input-col"
                                    label=""
                                    name="bo_mon"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Lĩnh vực/môn học là trường bắt buộc',
                                      },
                                    ]}
                                  >
                                    <Radio.Group options={constants.SUBJECT_LIST} disabled={state.trang_thai === 'active'} optionType="button" buttonStyle="solid" />
                                  </Form.Item>
                                  <Form.Item className="input-col hidden" label="Kiểu câu hỏi" name="kieu_cau_hoi" rules={[]}>
                                    <Radio.Group options={constants.QUESTIONS_FORMATS} disabled={state.trang_thai === 'active'} optionType="button" buttonStyle="solid" />
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Row>
                                <Col xl={4}>
                                  <Form.Item className="label">
                                    <span style={{ color: '#ff4d4f' }}>*</span>Đáp án đúng
                                  </Form.Item>
                                </Col>
                                <Col xl={20}>
                                  <Form.Item
                                    className="input-col"
                                    label=""
                                    name="dap_an_dung"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Đáp án đúng là trường bắt buộc',
                                      },
                                    ]}
                                  >
                                    <Radio.Group options={constants.ANSWER_OPTIONS} disabled={state.trang_thai === 'active'} optionType="button" buttonStyle="solid" />
                                  </Form.Item>
                                </Col>
                              </Row>

                              <Form.List name="dap_an">
                                {(fields, { add, remove }) => (
                                  <div className="group-answers">
                                    {fields.map(({ key, name, ...restField }) => (
                                      <Row key={key}>
                                        <Col xl={4}>
                                          <Form.Item {...restField} rules={[]} className="label">
                                            <span style={{ color: '#ff4d4f' }}>*</span> {currentQuestion.dap_an[key].label}
                                          </Form.Item>
                                        </Col>
                                        <Col xl={20}>
                                          <Form.Item {...restField} name={[name, 'tieu_de']} rules={[{ required: true, message: 'Bạn chưa nhập nội dung đáp án' }]}>
                                            <TextEditor
                                              disabled={state.trang_thai === 'active'}
                                              value={currentQuestion.dap_an[key].tieu_de}
                                              placeholder="Thêm nội dung đáp án"
                                              onChange={(val) => {
                                                let dap_an = [...currentQuestion.dap_an];
                                                dap_an[key] = {
                                                  ...dap_an[key],
                                                  tieu_de: val,
                                                };
                                                setCurrentQuestion({ ...currentQuestion, dap_an });
                                              }}
                                              isSimple={true}
                                              openEditor={(val) => setIsOpenEditor(val)}
                                            />
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    ))}
                                  </div>
                                )}
                              </Form.List>
                              <Row>
                              <Col xl={4}>
                                  <Form.Item className="label">Hiển thị đáp án</Form.Item>
                                </Col>
                                <Col xl={20}>
                                  <Form.Item
                                    className="input-col"
                                    label=""
                                    name="kieu_hien_thi_dap_an"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Hiển thị đáp án',
                                      },
                                    ]}
                                  >
                                    <Radio.Group options={constants.EXAM_ANSWER_VIEW_LIST} disabled={state.trang_thai === 'active'} optionType="button" buttonStyle="solid" />
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Row>
                                <Col xl={4}>
                                  <Form.Item className="label">Lời giải</Form.Item>
                                </Col>
                                <Col xl={20}>
                                  <Form.Item className="input-col" label="" name="loi_giai" rules={[]}>
                                    <TextEditor
                                      disabled={state.trang_thai === 'active'}
                                      value={currentQuestion.loi_giai}
                                      placeholder="Thêm nội dung lời giải"
                                      onChange={(val) => setCurrentQuestion({ ...currentQuestion, loi_giai: val })}
                                      openEditor={(val) => setIsOpenEditor(val)}
                                      isSimple={true}
                                    />
                                  </Form.Item>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </div>
                        {state.trang_thai !== 'active' && (
                          <div className="footer-question">
                            <Button
                              type="dash"
                              danger
                              onClick={() => {
                                questionForm.setFieldsValue(defaultQuestion);
                                setCurrentQuestion(defaultQuestion);
                                setIsOpenEditor(false);
                              }}
                              size="large"
                            >
                              {currentQuestion.cau_hoi_id ? 'Hủy bỏ' : 'Làm lại'}
                            </Button>
                            <Button
                              type="primary"
                              onClick={() => {
                                setIsOpenEditor(false);
                                setTimeout(() => {
                                  questionForm.submit();
                                }, 600);
                              }}
                              size="large"
                            >
                              {currentQuestion.cau_hoi_id ? 'Cập nhật' : 'Thêm mới'}
                            </Button>
                          </div>
                        )}
                      </Col>
                      <Col xl={6} sm={24} xs={24} className="right-content ">
                        <div className="box ">
                          <div className="box-body ">
                            <div className="border-box question-list ">
                              <h2>
                                Danh sách câu hỏi{' '}
                                <span className="counter">
                                  {questions.length}/{exam.so_luong_cau_hoi}
                                </span>
                              </h2>
                              {renderQuestions()}
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </Form>
                </TabPane>
                <TabPane tab="Tạo câu" key="step_2">
                  <Row gutter={25}>
                    <Col xl={24} sm={24} xs={24} className="left-content">
                      {state.trang_thai !== 'active' && (
                        <Result
                          status="success"
                          title="Tạo đề thi mới thành công."
                          subTitle="Hãy kiểm tra kỹ trước khi tiến hành xuất bản đề thi, đề thi sau khi xuất bản sẽ không thể được cập nhật thêm."
                          extra={[
                            <Button onClick={() => setCurrentStep(1)}> Kiểm tra lại</Button>,
                            <Button
                              type="primary"
                              onClick={() =>
                                Modal.confirm({
                                  title: 'Xuất bản đề thi',
                                  content: 'Bạn có chắc chắn muốn xuất bản đề thi này không?',
                                  okText: 'Có',
                                  cancelText: 'Không',
                                  onOk: () => {
                                    handleSaveExam({ ...state, trang_thai: 'active' }, true);
                                    setCurrentStep(0);
                                  },
                                })
                              }
                            >
                              Xuất bản
                            </Button>,
                          ]}
                        />
                      )}
                      {state.trang_thai === 'active' && (
                        <div className="w5d-list">
                          <h2 className="text-center">Danh sách kết quả thi</h2>
                          <List
                            locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
                            header={
                              <Row>
                                <Col xs={6} className="text-left">
                                  Họ và tên
                                </Col>
                                <Col xs={4} className="text-left">
                                  Điểm
                                </Col>
                                <Col xs={6} className="text-left">
                                  Kết quả chi tiết
                                </Col>
                                <Col xs={4} className="text-left">
                                  Thời gian làm
                                </Col>
                                <Col xs={4} className="text-left">
                                  {getLangText('GLOBAL.CREATE_DATE')}
                                </Col>
                              </Row>
                            }
                            footer={null}
                            dataSource={get(props, 'exam.listHistories.result.data', [])}
                            renderItem={(item) => (
                              <List.Item key={item.lich_su_thi_id} id="listUsers">
                                <Row className="full">
                                  <Col xs={6} className="text-left">
                                    {item.hoc_sinh_lam_bai.ten_tai_khoan} - {item.hoc_sinh_lam_bai.ten_nhan_vien}
                                  </Col>
                                  <Col xs={4} className="text-left">
                                    {get(item, 'diem', 0).toFixed(2)} / 10
                                  </Col>
                                  <Col xs={6} className="text-left">
                                    Số câu đúng: {item.so_cau_dung || 0} <br />
                                    Số câu sai: {item.so_cau_sai || 0} <br />
                                    Số câu không làm: {item.so_cau_khong_lam || 0} <br />
                                  </Col>
                                  <Col xs={4} className="text-left">
                                    {Math.floor(item.thoi_gian / 60)}:{item.thoi_gian % 60}
                                  </Col>
                                  <Col xs={4} className="text-left">
                                    {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                                  </Col>
                                </Row>
                              </List.Item>
                            )}
                          />
                        </div>
                      )}
                    </Col>
                  </Row>
                </TabPane>
              </Tabs>
            </div>
          </Col>
        </Row>
        <footer className="footer-exam">
          <div className="footer-action">
            {currentStep === 0 && (
              <>
                <Button type="dash" onClick={() => save()} size="large" disabled={!state.isChanged}>
                  Cập nhật <SaveOutlined />
                </Button>
                <Button type="primary" onClick={() => next()} size="large">
                  {state.trang_thai === 'active' ? 'Danh sách câu hỏi' : 'Thêm câu hỏi'} <RightOutlined />
                </Button>
              </>
            )}
            {currentStep === 1 && (
              <>
                <Button type="dash" onClick={() => setCurrentStep(0)} size="large">
                  <LeftOutlined /> Quay lại
                </Button>
                <Button type="primary" onClick={() => next()} size="large">
                  Tiếp theo <RightOutlined />
                </Button>
              </>
            )}
            {currentStep === 2 && (
              <>
                <Button type="dash" onClick={() => setCurrentStep(1)} size="large">
                  <LeftOutlined /> Quay lại
                </Button>
              </>
            )}
          </div>
        </footer>
      </div>
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={state.openMediaLibrary}
        onCancel={() =>
          setState((state) => ({
            ...state,
            openMediaLibrary: false,
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>
      <Modal
        wrapClassName="w5d-modal-large"
        visible={state.insertMedia.open}
        onCancel={() =>
          setState((state) => ({
            ...state,
            insertMedia: {
              ...state.insertMedia,
              open: false,
            },
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary onSelectImage={(file) => onInsertImage(file)} />
      </Modal>
    </App>
  );
}
const mapStateToProps = (state) => {
  return {
    exam: state.exam,
    question: state.question,
  };
};
export default connect(mapStateToProps)(ExamForm);
