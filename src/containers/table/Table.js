// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Row, Col, Form, Input, List, Badge, Button, Checkbox, notification, Modal, Menu, Space, Dropdown, Radio } from 'antd';
import { DeleteOutlined, DownOutlined, QuestionCircleOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons';
import { get } from 'lodash';
// import internal libs
import * as tableAction from 'redux/actions/table';
import { config } from 'config';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import App from 'App';
import AppFilter from 'components/AppFilter';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import W5dStatusTag from '../../components/status/W5dStatusTag';
import { ExportToExcel } from 'helpers/ExportToExcel';
const { TextArea } = Input;
const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';
function Table(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  const defaultTable = {
    tenbang: '',
    csdl_tenbang: '',
    loai_bang: 'table_view',
    trang_thai: 'active',
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    openPermission: false,
    table: defaultTable,
    search: {
      ...props.table.listCates.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
    gensql: {
      visible: false,
      bangid: '',
      tenbang: '',
      tablesql: '',
      csdl_tenbang: '',
    },
  });

  const searchTables = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.table.listCates.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(tableAction.getTables(searchParams));
    form.setFieldsValue(state.table);
  };

  useEffect(() => {
    searchTables();
  }, [props.location]);

  useEffect(() => {
    window.onbeforeunload = null;
  }, [props]);

  // list functions
  const allOptions = get(props, 'table.listCates.result.data', []).map((item) => item.bangid);
  const onCheckAllChange = (e) => {
    setState((state) => ({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    }));
  };
  const onChangeCheck = (bangid) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(bangid) ? state.checkedList.filter((it) => it !== bangid) : state.checkedList.concat([bangid]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const onEditCategory = (item) => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    setState((state) => ({
      ...state,
      isEdit: true,
      table: item,
    }));
    form.setFieldsValue(item);
  };

  const showDropTableForm = (item) => {
    setState((state) => ({
      ...state,
      gensql: {
        visible: true,
        bangid: item.bangid,
        tenbang: item.tenbang,
        csdl_tenbang: item.csdl_tenbang,
        isTableExisted: item.isTableExisted,
      },
    }));
  };
  const showGenTableSQLForm = (item) => {
    const callback = (res) => {
      if (res.data.tablesql !== '') {
        setState((state) => ({
          ...state,
          gensql: {
            visible: true,
            bangid: item.bangid,
            tenbang: item.tenbang,
            csdl_tenbang: item.csdl_tenbang,
            isTableExisted: item.isTableExisted,
          },
        }));
      }
    };
    props.dispatch(tableAction.genTableSql({ id: item.bangid }, callback));
  };
  const callGenTable = async (e) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Tạo bảng thành công.',
        });
        setState((state) => ({
          ...state,
          gensql: {
            visible: false,
          },
        }));
        searchTables();
      }
    };
    props.dispatch(tableAction.genTable({ id: state.gensql.bangid, csdl_tenbang: state.gensql.csdl_tenbang }, callback));
  };
  const callDropTable = async (e) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Xóa bảng thành công.',
        });
        setState((state) => ({
          ...state,
          gensql: {
            visible: false,
          },
        }));
        searchTables();
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa bảng dữ liệu được chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.dropTable({ id: state.gensql.bangid, csdl_tenbang: state.gensql.csdl_tenbang }, callback));
      },
    });
  };
  const cancelGenSQLModal = (e) => {
    setState((state) => ({
      ...state,
      gensql: {
        visible: false,
      },
    }));
  };

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
  };
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const shoudlDisableFrom = () => {
    if (state.isEdit && shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/sua')) return false;
    if (!state.isEdit && shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/them')) return false;
    return true;
  };

  const handleSubmit = (values) => {
    if (state.isEdit) {
      // edit a table
      const callback = (result) => {
        searchTables();
        changeUrlParams('edit', '');
        notification.success({
          message: 'Cập nhật tên bảng thành công.',
        });
        setState((state) => ({
          ...state,
          table: result.data,
        }));
      };
      props.dispatch(tableAction.editTable({ ...state.table, ...values }, callback));
    } else {
      //add a new table
      const callback = () => {
        searchTables();
        notification.success({
          message: 'Thêm mới bảng thành công.',
        });
        setState((state) => ({
          ...state,
          isChanged: false,
        }));
      };
      props.dispatch(tableAction.createTable({ ...state.table, ...values }, callback));
    }
    setState((state) => ({
      ...state,
      table: defaultTable,
      isEdit: false,
      isChanged: false,
    }));
    form.resetFields();
  };

  const onResetTable = () => {
    setState((state) => ({
      ...state,
      table: defaultTable,
      isEdit: false,
    }));
    form.resetFields();
  };

  const renderTableSQL = (tablesql) => {
    if (tablesql) {
      const arrLines = tablesql.split(/(?:\r\n|\r|\n)/g);
      const lines = arrLines.map((item, idx) => {
        return (
          <div>
            {item}
            <br />
          </div>
        );
      });
      return <div>{lines}</div>;
    }
  };

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }

  const deleteTable = (item) => {
    const callback = (res) => {
      if (res.success) {
        searchTables();
        notification.success({
          message: `Xóa bảng '${item.tenbang}' thành công!`,
        });
      }
    };
    confirm({
      title: `Bạn chắc chắn muốn xóa bảng '${item.tenbang}' không ?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.deleteTable({ id: item.bangid }, callback));
      },
    });
  };

  function deleteTables() {
    const callback = (res) => {
      if (res.success) {
        searchTables();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Xóa bảng dữ liệu thành công',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa những bảng dữ liệu được chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.deleteTables({ listIds: state.checkedList }, callback));
      },
    });
  }

  const blockTable = (item) => {
    const callback = () => {
      searchTables();
      notification.success({
        message: `Khóa bảng '${item.tenbang}' thành công`,
      });
    };
    confirm({
      title: `Bạn chắc chắn muốn khóa bảng '${item.tenbang}' không ?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(
          tableAction.editTablesStatus(
            {
              listIds: [item.bangid],
              status: 'inactive',
            },
            callback
          )
        );
      },
    });
  };

  //Active theo list => active
  function activeTables(e) {
    const callback = (res) => {
      if (res.success) {
        searchTables();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Kích hoạt nhiều bảng dữ liệu thành công',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các bảng dữ liệu được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            tableAction.editTablesStatus(
              {
                listIds: state.checkedList,
                status: 'active',
              },
              callback
            )
          );
        },
      });
    }
  }

  //Khoa theo list => inactive
  function blockTables(e) {
    const callback = (res) => {
      if (res.success) {
        searchTables();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Khóa nhiều bảng dữ liệu thành công',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn khóa các bảng dữ liệu được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            tableAction.editTablesStatus(
              {
                listIds: state.checkedList,
                status: 'inactive',
              },
              callback
            )
          );
        },
      });
    }
  }

  const menu = (
    <Menu>
      <Menu.Item key="1" disabled={!shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xoa') || state.checkedList.length <= 0} icon={<DeleteOutlined />} onClick={deleteTables}>
        Xóa bảng dữ liệu
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeTables}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt bảng dữ liệu
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockTables}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/sua') || state.checkedList.length <= 0}
      >
        Khóa bảng dữ liệu
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý bảng dữ liệu </title>
      </Helmet>
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={6} sm={24} xs={24}>
              <h2>
                {!state.isEdit && 'Thêm mới'}
                {state.isEdit && 'Cập nhật'}
              </h2>
              <Form layout="vertical" className="category-table" form={form} onFinish={handleSubmit}>
                <Form.Item
                  className="input-col"
                  label="Tên bảng"
                  name="tenbang"
                  rules={[
                    {
                      required: true,
                      message: 'Tên bảng là trường bắt buộc.',
                    },
                  ]}
                >
                  <TextArea
                    disabled={shoudlDisableFrom()}
                    placeholder="Ví dụ: Bảng siêu âm"
                    rows={2}
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item
                  className="input-col"
                  label="Tên bảng trong CSDL"
                  name="csdl_tenbang"
                  rules={[
                    {
                      required: true,
                      message: 'Tên bảng trong CSDL là trường bắt buộc',
                    },
                  ]}
                >
                  <Input
                    disabled={shoudlDisableFrom()}
                    placeholder="Ví dụ: uttg_sieuam"
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item
                  className="input-col"
                  label="Là bảng danh mục hay không?"
                  name="loai_bang"
                  rules={[
                    {
                      required: true,
                      message: 'Bắt buộc phải chọn là bảng danh mục hay không?',
                    },
                  ]}
                >
                  <Radio.Group
                    options={[
                      { label: 'Không', value: 'table_view' },
                      { label: 'Có', value: 'category' },
                    ]}
                    optionType="button"
                    buttonStyle="solid"
                  />
                </Form.Item>
                <Form.Item className="button-col">
                  <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableFrom()}>
                    {state.isEdit ? 'Cập nhật tên bảng' : 'Thêm tên bảng'}
                  </Button>
                  {state.isEdit && (
                    <Button shape="round" type="danger" onClick={() => onResetTable()}>
                      Hủy bỏ
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
            <Col xl={18} sm={24} xs={24} className="table-cates">
              <Row>
                <Col xl={20} sm={16} xs={24}>
                  <AppFilter
                    isShowCategories={false}
                    isShowStatus={true}
                    isShowSearchBox={true}
                    isShowDatePicker={true}
                    isRangeDatePicker={true}
                    title="Bảng dữ liệu"
                    search={state.search}
                    onDateChange={(dates) => onDateChange(dates)}
                    onFilterChange={(field, value) => onFilterChange(field, value)}
                  />
                </Col>
              </Row>
              <Row className="select-action-group-cate">
                <Space wrap>
                  <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                    <Button>
                      Chọn hành động
                      <DownOutlined />
                    </Button>
                  </Dropdown>
                </Space>
              </Row>

              <div className="w5d-list">
                {props.table.listCates.loading && <Loading />}
                <List
                  locale={{
                    emptyText: getLangText('GLOBAL.NO_ITEMS'),
                  }}
                  header={
                    <Row>
                      <Col xs={1} xl={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col xs={14} xl={12} className="text-left">
                        Tên bảng
                        <Sorting urlQuery={urlQuery} field={'tenbang'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col xs={4} xl={4} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col xs={4} xl={4} className="text-left">
                        Loại bảng
                        <Sorting urlQuery={urlQuery} field={'loai_bang'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col xs={3} xl={3} className="text-left">
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={
                    <Row>
                      <Col xs={1} xl={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col xs={14} xl={12} className="text-left">
                        Tên bảng
                        <Sorting urlQuery={urlQuery} field={'tenbang'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col xs={3} xl={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>

                      <Col xs={4} xl={4} className="text-left">
                        Loại bảng
                        <Sorting urlQuery={urlQuery} field={'loai_bang'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col xs={4} xl={4} className="text-left">
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  dataSource={get(props, 'table.listCates.result.data', [])}
                  pagination={{
                    hideOnSinglePage: false,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.bangid}>
                      <Row className="full">
                        <Col xs={1} xl={1}>
                          <Checkbox checked={state.checkedList.includes(item.bangid)} value={item.bangid} onChange={() => onChangeCheck(item.bangid)} />
                        </Col>
                        <Col xs={14} xl={12} className="text-left">
                          <Button type="link">
                            <Badge color={item.isTableExisted ? 'green' : 'grey'} />
                          </Button>
                          <Link className="edit" to={`/columns?categories=${item.bangid}`}>
                            {item.tenbang} {item.soluong === 0 ? '' : `(${item.soluong})`} ({item.csdl_tenbang})
                          </Link>
                          <div className="actions">
                            {shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/sua') && (
                              <Button className="edit act" type="link" onClick={() => onEditCategory(item)}>
                                {getLangText('GLOBAL.EDIT')}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xoa') && item.trang_thai === 'inactive' && (
                              <Button className="delete act" type="link" onClick={() => deleteTable(item)}>
                                Xóa
                              </Button>
                            )}
                            {shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/sua') && item.trang_thai === 'active' && (
                              <Button className="delete act" type="link" onClick={() => blockTable(item)}>
                                Khóa
                              </Button>
                            )}
                            {shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xoa_bang_csdl') && item.isTableExisted && item.trang_thai === 'active' && (
                              <Button className="gensql act" type="link" onClick={() => showDropTableForm(item)}>
                                Xóa bảng
                              </Button>
                            )}
                            {shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/tao_bang_csdl') && !item.isTableExisted && item.trang_thai === 'active' && (
                              <Button className="gensql act" type="link" onClick={() => showGenTableSQLForm(item)}>
                                Tạo bảng
                              </Button>
                            )}
                            {shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/add_column') && item.trang_thai === 'active' && (
                              <Link className="add_column act" to={`/columns/select-columns/${item.bangid}`}>
                                Thêm trường từ bảng khác
                              </Link>
                            )}
                            {shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/export_table') && item.trang_thai === 'active' && <ExportToExcel />}
                          </div>
                        </Col>
                        <Col xs={4} xl={4} className="text-left">
                          <W5dStatusTag status={item.trang_thai} />
                        </Col>
                        <Col xs={4} xl={4} className="text-left">
                          {item.loai_bang}
                        </Col>
                        <Col xs={3} xl={3} className="text-left">
                          {moment(item.ngay_tao).isValid() ? moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT_SHORT) : '-'}
                        </Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        title={state.gensql.isTableExisted ? `Bảng "${state.gensql.tenbang}" đã tồn tại trong CSDL. Thực hiện xóa bảng?` : `Thực hiện tạo bảng "${state.gensql.tenbang}" trong CSDL?`}
        onCancel={cancelGenSQLModal}
        visible={state.gensql.visible}
        footer={[
          <Button key="back" onClick={() => cancelGenSQLModal()}>
            Đóng
          </Button>,
          <Button key="submit" type="primary" onClick={() => callGenTable()} disabled={state.gensql.isTableExisted || !shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/tao_bang_csdl')}>
            Tạo bảng
          </Button>,
          <Button key="del" type="danger" onClick={() => callDropTable()} disabled={!state.gensql.isTableExisted || !shouldHaveAccessPermission('bang_nghiep_vu', 'bang_nghiep_vu/xoa_bang_csdl')}>
            Xóa bảng
          </Button>,
        ]}
        width={700}
      >
        {state.gensql.isTableExisted ? 'Hãy chắc chắc là bạn muốn xóa bảng này. Bảng này sẽ bị xóa đi mà không có bản phục hồi.' : renderTableSQL(get(props, 'table.item.result.tablesql', ''))}
      </Modal>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    table: state.table,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Table);
