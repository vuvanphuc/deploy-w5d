// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory, useParams } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { Row, Col, Form, Input, List, Button, Checkbox, Select, Radio, InputNumber, notification, Modal, Menu, Space, Dropdown } from 'antd';
import { DeleteOutlined, DownOutlined, QuestionCircleOutlined, DatabaseOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons';
import { get } from 'lodash';
import moment from 'moment';
// import internal libs
import * as tableAction from '../../redux/actions/table';
import { config } from '../../config';
import { getLangText } from '../../helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from '../../helpers/common.helper';
import App from '../../App';
import AppFilter from '../../components/AppFilter';
import constants from '../../constants/global.constants';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import W5dStatusTag from '../../components/status/W5dStatusTag';

const { Option } = Select;
const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';
function Column(props) {
  const { bangid } = useParams();
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const [table, setTable] = useState(undefined);

  const defaultColumn = {
    csdl_tentruong: '',
    tentruong: '',
    kieudulieu: '',
    dulieu_dodai: 200,
    dulieu_khoachinh: 0,
    dulieu_tudongtang: 0,
    dulieu_batbuoc: 0,
    dulieu_duynhat: 0,
    dulieu_timkiem: 0,
    ghichu: '',
    thutu_hienthi: 0,
    trang_thai: 'active',
    bangid: undefined,
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    openPermission: false,
    column: defaultColumn,
    search: {
      ...props.table.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });

  const searchColumns = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.table.list.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(tableAction.getColumns(searchParams));
    form.setFieldsValue(state.column);
  };

  useEffect(() => {
    searchColumns();
  }, [props.location]);
  useEffect(() => {
    props.dispatch(tableAction.getTables());
  }, []);
  useEffect(() => {
    window.onbeforeunload = null;
  }, [props]);

  // list functions
  const allOptions = get(props, 'table.list.result.data', []).map((item) => item.truongid);

  const onCheckAllChange = (e) => {
    setState((state) => ({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    }));
  };

  const onChangeCheck = (truongid) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(truongid) ? state.checkedList.filter((it) => it !== truongid) : state.checkedList.concat([truongid]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const editColumn = (item) => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    setState((state) => ({
      ...state,
      isEdit: true,
      column: item,
    }));
    form.setFieldsValue(item);
  };

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
  };
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const shoudlDisableFrom = () => {
    if (state.isEdit && shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/sua')) return false;
    if (!state.isEdit && shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/them')) return false;
    return true;
  };

  const handleSubmit = (values) => {
    if (state.isEdit) {
      // edit a column
      const callback = (result) => {
        searchColumns();
        changeUrlParams('edit', '');
        notification.success({
          message: 'Cập nhật trường dữ liệu thành công.',
        });
        setState((state) => ({
          ...state,
          column: result.data,
        }));
      };
      props.dispatch(tableAction.editColumn({ ...state.column, ...values }, callback));
    } else {
      //add a new column
      const callback = () => {
        searchColumns();
        notification.success({
          message: 'Thêm mới trường dữ liệu thành công.',
        });
        setState((state) => ({
          ...state,
          isChanged: false,
        }));
      };
      props.dispatch(tableAction.createColumn({ ...state.column, ...values }, callback));
    }
    setState((state) => ({
      ...state,
      column: defaultColumn,
      isEdit: false,
      isChanged: false,
    }));
    form.resetFields();
  };

  const onResetDBColumn = () => {
    setState((state) => ({
      ...state,
      column: defaultColumn,
      isEdit: false,
    }));
    form.resetFields();
  };

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }

  const findCate = (id) => {
    const cates = get(props, 'table.listCates.result.data', []);
    const cate = cates.find((ct) => ct.bangid === id);
    return cate ? cate.tenbang : '';
  };

  const renderCates = () => {
    const defaultCate = urlQuery.categories ? urlQuery.categories.split('_') : [];
    const cates = get(props, 'table.listCates.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.bangid}>
          {cate.tenbang}
        </Option>
      );
    });
    return (
      <Select
        disabled={shoudlDisableFrom()}
        value={defaultCate}
        onChange={(value) => {
          setTable(value);
          if (!state.isChanged) {
            setState((state) => ({
              ...state,
              isChanged: true,
            }));
          }
        }}
        placeholder="Chọn bảng dữ liệu"
      >
        {catesOpts}
      </Select>
    );
  };

  const renderRelatedTable = () => {
    const cates = get(props, 'table.listCates.result.data', []).filter((cate) => cate.bangid !== table);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.bangid}>
          {cate.tenbang}
        </Option>
      );
    });
    return (
      <Select
        defaultValue=""
        disabled={shoudlDisableFrom() || !form.getFieldValue('bangid')}
        onChange={() => {
          if (!state.isChanged) {
            setState((state) => ({
              ...state,
              isChanged: true,
            }));
          }
        }}
        placeholder="Chọn bảng liên kết"
      >
        <Option value="">Trường này không có liên kết</Option>
        {catesOpts}
      </Select>
    );
  };
  const addColToTable = () => {
    if (state.checkedList.length === 0) {
      notification.warning({
        message: 'Bạn chưa chọn trường dữ liệu nào!',
      });
      return;
    }
    const callback = (res) => {
      if (res.success) {
        searchColumns();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Thêm trường dữ liệu thành công!',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn thêm những trường dữ liệu được chọn vào bảng ' + findCate(bangid) + '?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.addColumnsToTable({ listIds: state.checkedList, bangid: bangid }, callback));
      },
    });
  };
  const renderStatus = () => {
    const types = constants.COLUMN_TYPE;
    return types.map((label) => (
      <Option key={label.value} value={label.value}>
        {label.title}
      </Option>
    ));
  };

  const deleteColumn = (id, tentruong) => {
    const callback = (res) => {
      if (res.success) {
        searchColumns();
        notification.success({
          message: `Xóa trường dữ liệu ${tentruong} thành công.`,
        });
      }
    };
    confirm({
      title: `Bạn chắc chắn muốn xóa ${tentruong} không ?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.deleteColumn({ id }, callback));
      },
    });
  };

  const deleteColumns = () => {
    const callback = (res) => {
      if (res.success) {
        searchColumns();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Xóa trường dữ liệu thành công',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa những trường dữ liệu được chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.deleteColumns({ listIds: state.checkedList }, callback));
      },
    });
  };

  const blockColumn = (id, tentruong) => {
    const callback = (res) => {
      if (res.success) {
        searchColumns();
        notification.success({
          message: `Khóa trường dữ liệu ${tentruong} thành công.`,
        });
      }
    };
    confirm({
      title: `Bạn chắc chắn muốn khóa ${tentruong} không ?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.editColumnsStatus({ listIds: [id], status: 'inactive' }, callback));
      },
    });
  };

  const blockColumns = () => {
    const callback = (res) => {
      if (res.success) {
        searchColumns();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Khóa trường dữ liệu thành công',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn khóa những trường dữ liệu được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.editColumnsStatus({ listIds: state.checkedList, status: 'inactive' }, callback));
      },
    });
  };

  const activeColumns = () => {
    const callback = (res) => {
      if (res.success) {
        searchColumns();
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Kích hoạt trường dữ liệu thành công',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn kích hoạt những trường dữ liệu được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(tableAction.editColumnsStatus({ listIds: state.checkedList, status: 'active' }, callback));
      },
    });
  };

  const menu = (
    <Menu>
      <Menu.Item key="1" disabled={!shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/xoa') || state.checkedList.length <= 0} icon={<DeleteOutlined />} onClick={deleteColumns}>
        Xóa trường dữ liệu
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeColumns}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt trường dữ liệu
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockColumns}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/sua') || state.checkedList.length <= 0}
      >
        Khóa trường dữ liệu
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý trường dữ liệu </title>
      </Helmet>
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            {!bangid && (
              <Col xl={6} sm={24} xs={24}>
                <h2>Thêm mới </h2>
                <Form layout="vertical" className="category-table" form={form} onFinish={handleSubmit}>
                  <Row gutter={[16]}>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Tên trường"
                        name="tentruong"
                        rules={[
                          {
                            required: true,
                            message: 'Tên trường là trường bắt buộc.',
                          },
                        ]}
                      >
                        <Input
                          disabled={shoudlDisableFrom()}
                          placeholder=" Ví dụ: Họ và tên"
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Tên trường trong CSDL"
                        name="csdl_tentruong"
                        rules={[
                          {
                            required: true,
                            message: 'Tên trường trong CSDL là trường bắt buộc',
                          },
                        ]}
                      >
                        <Input
                          disabled={shoudlDisableFrom()}
                          placeholder=" Ví dụ: hovaten"
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={[16]}>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Tên bảng dữ liệu"
                        name="bangid"
                        rules={[
                          {
                            required: true,
                            message: 'Bảng là trường bắt buộc.',
                          },
                        ]}
                      >
                        {renderCates()}
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item className="input-col" label="Tên bảng liên kết" name="dulieu_banglienket" rules={[]}>
                        {renderRelatedTable()}
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row gutter={[16]}>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Kiểu dữ liệu"
                        name="kieudulieu"
                        rules={[
                          {
                            required: true,
                            message: 'Kiểu dữ liệu là trường bắt buộc',
                          },
                        ]}
                      >
                        <Select placeholder="Kiểu dữ liệu">{renderStatus()}</Select>
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item className="input-col" label="Độ dài" name="dulieu_dodai" rules={[]}>
                        <InputNumber
                          style={{ width: '100%' }}
                          disabled={shoudlDisableFrom()}
                          min={0}
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={[16]}>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item className="input-col" label="Ghi chú" name="ghichu">
                        <Input
                          disabled={shoudlDisableFrom()}
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Thứ tự hiển thị"
                        name="thutu_hienthi"
                        rules={[
                          {
                            required: true,
                            message: 'Thứ tự hiển thị là trường bắt buộc.',
                          },
                        ]}
                      >
                        <InputNumber
                          style={{ width: '100%' }}
                          disabled={shoudlDisableFrom()}
                          min={0}
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Là khóa chính không?"
                        name="dulieu_khoachinh"
                        rules={[
                          {
                            required: true,
                            message: 'Khóa chính là trường bắt buộc.',
                          },
                        ]}
                      >
                        <Radio.Group
                          options={[
                            { label: 'Không', value: 0 },
                            { label: 'Có', value: 1 },
                          ]}
                          optionType="button"
                          buttonStyle="solid"
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Tự động tăng không?"
                        name="dulieu_tudongtang"
                        rules={[
                          {
                            required: true,
                            message: 'Tự động tăng là trường bắt buộc.',
                          },
                        ]}
                      >
                        <Radio.Group
                          options={[
                            { label: 'Không', value: 0 },
                            { label: 'Có', value: 1 },
                          ]}
                          optionType="button"
                          buttonStyle="solid"
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Bắt buộc không?"
                        name="dulieu_batbuoc"
                        rules={[
                          {
                            required: true,
                            message: 'Là trường bắt buộc.',
                          },
                        ]}
                      >
                        <Radio.Group
                          options={[
                            { label: 'Không', value: 0 },
                            { label: 'Có', value: 1 },
                          ]}
                          optionType="button"
                          buttonStyle="solid"
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Dữ liệu có duy nhất không?"
                        name="dulieu_duynhat"
                        rules={[
                          {
                            required: true,
                            message: 'Là trường bắt buộc.',
                          },
                        ]}
                      >
                        <Radio.Group
                          options={[
                            { label: 'Không', value: 0 },
                            { label: 'Có', value: 1 },
                          ]}
                          optionType="button"
                          buttonStyle="solid"
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col xl={12} sm={12} xs={24}>
                      <Form.Item
                        className="input-col"
                        label="Có thể tìm kiếm không?"
                        name="dulieu_timkiem"
                        rules={[
                          {
                            required: true,
                            message: 'Là trường bắt buộc.',
                          },
                        ]}
                      >
                        <Radio.Group
                          options={[
                            { label: 'Không', value: 0 },
                            { label: 'Có', value: 1 },
                          ]}
                          optionType="button"
                          buttonStyle="solid"
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Form.Item className="button-col">
                    <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableFrom()}>
                      {state.isEdit ? 'Cập nhật trường dữ liệu' : 'Thêm trường dữ liệu'}
                    </Button>
                    {state.isEdit && (
                      <Button shape="round" type="danger" onClick={() => onResetDBColumn()}>
                        Hủy bỏ
                      </Button>
                    )}
                  </Form.Item>
                </Form>
              </Col>
            )}
            {bangid && (
              <Col xl={8} sm={24} xs={24}>
                <h2> Thêm trường dữ liệu từ các bảng khác </h2>
                <Row gutter={[16]}>
                  <Col xl={24} sm={12} xs={24}>
                    <div className="profile-basic-info">
                      <div className="info-item">
                        <DatabaseOutlined /> Tên bảng dữ liệu: {findCate(bangid)}
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <br />
                </Row>
                <Button shape="round" type="primary" onClick={() => addColToTable()}>
                  Thêm trường dữ liệu đã chọn
                </Button>
              </Col>
            )}
            <Col xl={18} sm={24} xs={24} className="table-cates">
              <Row>
                <Col xl={24} sm={16} xs={24}>
                  <AppFilter
                    isShowCategories={true}
                    isShowStatus={true}
                    isShowSearchBox={true}
                    title="Trường dữ liệu"
                    categories={get(props, 'table.listCates.result.data', []).map((cate) => ({
                      name: cate.tenbang,
                      id: cate.bangid,
                    }))}
                    isShowDatePicker={true}
                    isRangeDatePicker={true}
                    search={state.search}
                    onDateChange={(dates) => onDateChange(dates)}
                    onFilterChange={(field, value) => onFilterChange(field, value)}
                  />
                </Col>
              </Row>
              {!bangid && (
                <Row className="select-action-group-cate">
                  <Space wrap>
                    <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                      <Button>
                        Chọn hành động
                        <DownOutlined />
                      </Button>
                    </Dropdown>
                  </Space>
                </Row>
              )}
              <div className="w5d-list">
                {props.table.list.loading && <Loading />}
                <List
                  locale={{
                    emptyText: getLangText('GLOBAL.NO_ITEMS'),
                  }}
                  header={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={5} className="text-left">
                        Tên bảng
                        <Sorting urlQuery={urlQuery} field={'tenbang'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={5} className="text-left">
                        Tên trường
                        <Sorting urlQuery={urlQuery} field={'tentruong'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={5} className="text-left">
                        Tên trường CSDL
                        <Sorting urlQuery={urlQuery} field={'csdl_tentruong'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={3} className="text-left">
                        Kiểu dữ liệu
                        <Sorting urlQuery={urlQuery} field={'kieudulieu'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={2} className="text-left">
                        Thứ tự
                        <Sorting urlQuery={urlQuery} field={'thutu_hienthi'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={5} className="text-left">
                        Tên bảng
                        <Sorting urlQuery={urlQuery} field={'tenbang'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={5} className="text-left">
                        Tên trường
                        <Sorting urlQuery={urlQuery} field={'tentruong'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={5} className="text-left">
                        Tên trường CSDL
                        <Sorting urlQuery={urlQuery} field={'csdl_tentruong'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={3} className="text-left">
                        Kiểu dữ liệu
                        <Sorting urlQuery={urlQuery} field={'kieudulieu'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={2} className="text-left">
                        Thứ tự
                        <Sorting urlQuery={urlQuery} field={'thutu_hienthi'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  dataSource={get(props, 'table.list.result.data', [])}
                  pagination={{
                    hideOnSinglePage: false,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.truongid}>
                      <Row className="full">
                        <Col span={1}>
                          <Checkbox checked={state.checkedList.includes(item.truongid)} onChange={() => onChangeCheck(item.truongid)} />
                        </Col>
                        <Col span={5} className="text-left">
                          {findCate(item.bangid)}
                        </Col>
                        <Col span={5} className="text-left">
                          <Link className="edit" to={`#`}>
                            {item.tentruong}
                          </Link>
                          <div className="actions">
                            {shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/sua') && (
                              <Button className="edit act" type="link" onClick={() => editColumn(item)}>
                                {getLangText('GLOBAL.EDIT')}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/xoa') && item.trang_thai === 'inactive' && (
                              <Button className="delete act" type="link" onClick={() => deleteColumn(item.truongid, item.tentruong)}>
                                Xóa
                              </Button>
                            )}
                            {shouldHaveAccessPermission('truong_du_lieu', 'truong_du_lieu/xoa') && item.trang_thai === 'active' && (
                              <Button className="delete act" type="link" onClick={() => blockColumn(item.truongid, item.tentruong)}>
                                Khóa
                              </Button>
                            )}
                          </div>
                        </Col>
                        <Col span={5} className="text-left">
                          {item.csdl_tentruong}
                        </Col>
                        <Col span={3} className="text-left">
                          <W5dStatusTag status={item.trang_thai} />
                        </Col>
                        <Col span={3} className="text-left">
                          {item.kieudulieu}
                        </Col>
                        <Col span={2} className="text-left">
                          {item.thutu_hienthi}
                        </Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    table: state.table,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Column);
