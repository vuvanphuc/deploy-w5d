// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Helmet } from 'react-helmet';
import { Row, Col, Upload, Modal, Button, notification, Checkbox, Select, List } from 'antd';
import { UploadOutlined, DeleteOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import FolderTree from 'react-folder-tree';
import { FaRegTrashAlt } from 'react-icons/fa';

// import internal libs
import * as mediaAction from 'redux/actions/media';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl, getMediaInfo } from 'helpers/common.helper';
import App from 'App';
import Loading from 'components/loading/Loading';
import W5dPreviewImage from 'components/W5dPreviewImage';
import constants from 'constants/global.constants';

const { confirm } = Modal;
const { Dragger } = Upload;
const { Option } = Select;

function MediaLibrary(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);
  const [state, setState] = useState({
    previewVisible: false,
    previewImage: '',
    fileList: props.media.list.result,
    selected: [],
    listIds: [],
    nhom_tep_tin_id: null,
    checkedList: [],
  });
  const [appConfig, setAppConfig] = useState({});

  const [folders, setFolders] = useState({
    name: 'Thư viện ảnh',
    checked: 0,
    isOpen: true,
    children: [],
  });
  const [folder, setFolder] = useState(urlQuery.folder);

  const [isPreviewVisible, setIsPreviewVisible] = useState(false);
  const [imagePreview, setImagePreview] = useState({});
  const [page, setPage] = useState(props.location.search.page ? props.location.search : 1);
  const [pageSize, setPageSize] = useState(props.location.search.pageSize ? props.location.search.pageSize : 40);

  const handleCancel = () => setState({ previewVisible: false });

  const handlePreview = async (file) => {
    setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
    });
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onChangeFolder = (defaultOnClick, nodeData) => {
    //defaultOnClick();
    const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
    setState((state) => ({
      ...state,
      selected: [],
      nhom_tep_tin_id: '',
    }));
    if (nodeData.id) {
      params.category = nodeData.id;
      setState((state) => ({
        ...state,
        selected: [],
        listIds: [],
        nhom_tep_tin_id: nodeData.id,
      }));
      setFolder(nodeData.id);
      setFolders({
        ...folders,
        children: folders.children.map((item) => ({
          ...item,
          isOpen: item.id === nodeData.id,
          checked: item.id === nodeData.id,
        })),
      });
      changeMutilUrlParams({ folder: nodeData.id });
    } else {
      changeMutilUrlParams({ folder: '' });
      setFolder('');
      setState((state) => ({
        ...state,
        nhom_tep_tin_id: null,
      }));
    }

    props.dispatch(mediaAction.getMedias(params));
  };
  const onChangeFolderInfo = (newFolders) => {
    const checkedList = newFolders.children.filter((item) => item.checked === 1);
    setState({ ...state, checkedList });
    if (newFolders.children.length > folders.children.length) {
      const callback = (res) => {
        if (res.success) {
          const params = {
            category: res.data.nhom_tep_tin_id,
            server: appConfig.site_upload_server,
            bucket: appConfig.site_minio_bucket,
          };
          setFolder(res.data.nhom_tep_tin_id);
          props.dispatch(mediaAction.getMediaCates());
          props.dispatch(mediaAction.getMedias(params));
        }
      };
      props.dispatch(mediaAction.createMediaCate({ ten_nhom: 'new folder' }, callback));
    } else if (newFolders.children.length === folders.children.length) {
      for (const newNode of newFolders.children) {
        let isFinded = false;
        for (const oldNode of folders.children) {
          if (newNode.name === oldNode.name) {
            isFinded = true;
          }
        }
        if (!isFinded) {
          const callback = (res) => {
            if (res.success) {
              const params = {
                category: res.data.nhom_tep_tin_id,
                server: appConfig.site_upload_server,
                bucket: appConfig.site_minio_bucket,
              };
              setFolder(res.data.nhom_tep_tin_id);
              props.dispatch(mediaAction.getMediaCates());
              props.dispatch(mediaAction.getMedias(params));
            }
          };
          props.dispatch(mediaAction.editMediaCate({ nhom_tep_tin_id: newNode.id, ten_nhom: newNode.name }, callback));
        }
      }
    } else {
      console.log('Delete folder');
    }
  };

  const handleRemove = async (file) => {
    const result = window.confirm(getLangText('GLOBAL.ARE_YOUR_SURE_TO_DELETE'));
    if (result) {
      const callback = () => {
        props.dispatch(mediaAction.getMedias({ server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket }));
        notification.success({
          message: getLangText('MESSAGE.DELETE_MEDIA_SUCCESS'),
        });
      };
      props.dispatch(mediaAction.deleteMedia({ id: file.tep_tin_id }, callback));
    }
  };

  const customRequest = async ({ onSuccess, onError, file }) => {
    if (file.name.length < 50) {
      const callback = () => {
        const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
        if (folder) {
          params.category = folder;
        }
        props.dispatch(mediaAction.getMedias(params));
        // notification.success({
        //   message: getLangText('MESSAGE.CREATE_MEDIA_SUCCESS'),
        // });
      };
      props.dispatch(mediaAction.createMedia({ fileUpload: file, folder, server: appConfig.site_upload_server, minio_bucket: appConfig.site_minio_bucket }, callback));
    } else
      notification.error({
        message: 'Tên file ảnh có độ dài lớn hơn 50 ký tự, không thể upload ảnh',
      });
  };
  const selectImage = (image) => {
    if (props.type === 'single') {
      setState((state) => ({
        ...state,
        selected: [image.tep_tin_url],
        previewImage: image,
        listIds: [image.tep_tin_id],
      }));
    } else {
      setState((state) => ({
        ...state,
        previewImage: image,
        selected: state.selected && state.selected.includes(image.tep_tin_url) ? state.selected.filter((img) => img !== image.tep_tin_url) : state.selected.concat([image.tep_tin_url]),
        listIds: state.listIds && state.listIds.includes(image.tep_tin_id) ? state.listIds.filter((img) => img !== image.tep_tin_id) : state.listIds.concat([image.tep_tin_id]),
      }));
    }
  };
  const renderImage = () => {
    return (
      <List
        header={null}
        footer={null}
        pagination={{
          hideOnSinglePage: false,
          responsive: true,
          showLessItems: true,
          pageSize: pageSize,
          showSizeChanger: false,
          showQuickJumper: false,
          pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
          onChange: (page, pageSize) => {
            changeMutilUrlParams({ page, pageSize });
            setPageSize(pageSize);
            setPage(page);
          },
          current: Number(page),
          showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
          showSizeChanger: true,
        }}
        dataSource={get(props, 'media.list.result.data', [])}
        renderItem={(image, idx) => {
          const imgInfo = getMediaInfo(image);
          let imgSrc = imgInfo.src;
          if (appConfig.site_upload_server === 'minio') {
            imgSrc = image.minioUrl;
          }

          return (
            <div className="image-item" key={idx} title={image.tieu_de}>
              <Checkbox checked={state.listIds && state.listIds.includes(image.tep_tin_id)} onChange={() => selectImage(image)} />
              <div
                className="attachment-preview"
                onClick={() => {
                  setIsPreviewVisible(true);
                  setImagePreview(image);
                }}
              >
                <div className="thumbnail">
                  <div className="centered">
                    {' '}
                    <img src={imgSrc} title={image.tieu_de} alt={image.tieu_de} />
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      />
    );
  };

  const { previewVisible, previewImage } = state;

  const DeleteIcon = ({ onClick: defaultOnClick, nodeData }) => {
    const { name } = nodeData;
    const handleClick = () => {
      const callback = (res) => {
        if (res.success) {
          props.dispatch(mediaAction.getMedias({ server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket }));
          props.dispatch(mediaAction.getMediaCates());
          setFolder('');
          notification.success({
            message: 'Thực hiện xóa thư mục ảnh thành công.',
          });
        }
      };
      confirm({
        title: 'Bạn chắc chắn muốn xóa thư mục ' + name + ' không?',
        content: '',
        onOk() {
          props.dispatch(mediaAction.deleteMediaCate({ id: nodeData.id }, callback));
          defaultOnClick();
        },
      });
    };

    return <FaRegTrashAlt className="delete-folder-icon" onClick={handleClick} />;
  };

  const iconComponents = {
    DeleteIcon,
  };

  function deleteListMedias(e) {
    const callback = (res) => {
      if (res.success) {
        setState((state) => ({
          ...state,
          selected: [],
        }));
        const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
        if (folder) {
          params.category = folder;
        }
        props.dispatch(mediaAction.getMedias(params));
        notification.success({
          message: 'Xóa nhiều hình ảnh thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các ảnh được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(mediaAction.deleteMedias({ listIds: state.listIds }, callback));
      },
    });
  }

  function moveListMedias(nhom_tep_tin_id) {
    const callback = (res) => {
      if (res.success) {
        setState((state) => ({
          ...state,
          isChanged: false,
          selected: [],
          listIds: [],
          nhom_tep_tin_id: null,
        }));
        const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
        if (nhom_tep_tin_id) {
          params.category = nhom_tep_tin_id;
        }
        props.dispatch(mediaAction.getMedias(params));
        changeMutilUrlParams({ folder: nhom_tep_tin_id });
        notification.success({
          message: 'Chuyển file hình ảnh sang thư mục mới thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn chuyển các ảnh được chọn sang thư mục được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(mediaAction.moveMedias({ listIds: state.listIds, nhom_tep_tin_id }, callback));
      },
    });
  }

  useEffect(() => {
    const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
    if (folder) {
      params.category = folder;
    }
    if (appConfig.site_upload_server) {
      props.dispatch(mediaAction.getMedias(params));
      props.dispatch(mediaAction.getMediaCates());
    }
  }, [appConfig]);

  useEffect(() => {
    const cates = get(props, 'media.listCates.result.data', []);
    const subFolders = cates
      .filter((item) => item.nhom_cha_id !== 'MODULE_FILE_PRIVATE')
      .map((cate) => ({
        id: cate.nhom_tep_tin_id,
        parent: cate.nhom_cha_id,
        checked: 0,
        isOpen: false,
        name: cate.ten_nhom,
        children: [],
      }));
    // const fol = recursiveFolders(subFolders, subFolders)
    // console.log('fol', fol);

    setFolders({ ...folders, children: subFolders });
  }, [props.media.listCates.result.data]);

  const renderSelect = () => {
    const Cates = get(props, 'media.listCates.result.data', []);
    const listCates = Cates.map((cates, idx) => {
      return (
        <Option key={idx} value={cates.nhom_tep_tin_id}>
          {cates.ten_nhom}
        </Option>
      );
    });
    return (
      <Select
        disabled={state.selected.length === 0 ? true : false}
        style={{ width: 200 }}
        placeholder="Chọn thư mục cần chuyển"
        optionFilterProp="children"
        value={state.nhom_tep_tin_id}
        onChange={(nhom_tep_tin_id) => {
          setState({ ...state, isChanged: true });
          moveListMedias(nhom_tep_tin_id);
        }}
        className="items-margin"
      >
        {listCates}
      </Select>
    );
  };
  const onDeleteListForders = () => {
    let mediaCateIds = [];
    for (const [index, element] of state.checkedList.entries()) mediaCateIds.push(element.id);
    const callback = (res) => {
      if (res.success) {
        const params = { server: appConfig.site_upload_server, bucket: appConfig.site_minio_bucket };
        props.dispatch(mediaAction.getMedias(params));
        props.dispatch(mediaAction.getMediaCates());
        setFolder('');
        notification.success({
          message: 'Xóa nhiều thư mục ảnh thành công',
        });
      }
    };
    confirm({
      title: 'Bạn có chắc chắn xóa những thư mục ảnh đã chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(mediaAction.deleteMediaCates({ listIds: mediaCateIds }, callback));
      },
    });
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    const dataConfig = JSON.parse(config.du_lieu);
    setAppConfig(dataConfig);
  }, [props.config]);
  return (
    <App>
      <Helmet>
        <title>{getLangText('GLOBAL.MEDIA_LIBRARY')}</title>
      </Helmet>
      {(props.media.list.loading || props.media.cate.loading || props.media.listCates.loading) && <Loading />}
      <Row className="app-main app-library">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={5} sm={24} xs={24} className="folder-tree-sidebar">
              <Dragger
                listType="picture"
                multiple={true}
                customRequest={(data) => customRequest(data)}
                fileList={[]}
                onPreview={(file) => handlePreview(file)}
                onRemove={(file) => handleRemove(file)}
                className="upload-list-inline"
                showUploadList={{
                  showRemoveIcon: shouldHaveAccessPermission('thu_vien', 'thu_vien/xoa'),
                }}
                disabled={!shouldHaveAccessPermission('thu_vien', 'thu_vien/them')}
              >
                <p className="ant-upload-drag-icon">
                  <UploadOutlined />
                </p>
                <p className="ant-upload-text">Click hoặc kéo thả một hoặc nhiều ảnh vào đây</p>
              </Dragger>
              {/* <div className="folder-creator">
                <Input className="nameFolder" placeholder="Tên thư mục" size="small" />
                <Select className="parentFolder" defaultValue="lucy" size="small">
                  <Option value="jack">Jack</Option>
                  <Option value="lucy">Lucy</Option>
                  <Option value="disabled" disabled>
                    Disabled
                  </Option>
                  <Option value="Yiminghe">yiminghe</Option>
                </Select>
                <Button className="submitFolder" size="small" type="primary" icon={<DownloadOutlined />} />
              </div> */}
              <Button shape="round" type="danger" disabled={state.checkedList.length !== 0 ? false : true} onClick={onDeleteListForders}>
                <DeleteOutlined /> Xóa nhiều thư mục
              </Button>
              <FolderTree
                data={folders}
                showCheckbox={true}
                initCheckedStatus="custom"
                initOpenStatus="custom"
                iconComponents={iconComponents}
                onChange={(info) => onChangeFolderInfo(info)}
                onNameClick={(defaultOnClick, nodeData) => onChangeFolder(defaultOnClick, nodeData)}
              />
            </Col>
            <Col xl={19} sm={24} xs={24}>
              <Row>
                <Col xl={15} sm={8} xs={8}>
                  <Button shape="round" type="danger" onClick={deleteListMedias} disabled={state.selected.length === 0 ? true : false} className="items-margin">
                    <DeleteOutlined /> Xóa hình ảnh
                  </Button>
                  {renderSelect()}
                </Col>
              </Row>
              <br />
              <Row className="library-image-list" gutter={24}>
                {renderImage()}
              </Row>
              <Modal visible={previewVisible} footer={null} onCancel={() => handleCancel()}>
                <img alt={getLangText('GLOBAL.PREVIEW_IMAGE')} style={{ width: '100%' }} src={previewImage} />
              </Modal>
            </Col>
          </Row>
        </Col>
      </Row>

      <W5dPreviewImage isPreviewVisible={isPreviewVisible} imagePreview={imagePreview} folder={folder} appConfig={appConfig} setIsPreviewVisible={(value) => setIsPreviewVisible(value)} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    media: state.media,
    config: state.config,
  };
};

export default connect(mapStateToProps)(MediaLibrary);
