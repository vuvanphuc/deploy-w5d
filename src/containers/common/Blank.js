// import external libs
import React from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Row, Col } from 'antd';

// import internal libs
import App from 'App';

function Blank() {
  return (
    <App>
      <Helmet>
        <title>Blank page</title>
      </Helmet>
      <Row className="app-main">
        <Col span={12}>blank page</Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    post: state.post,
    todo: state.todo,
  };
};

export default connect(mapStateToProps)(Blank);
