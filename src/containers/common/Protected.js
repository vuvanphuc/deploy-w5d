// import external libs
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Result, Button } from 'antd';

// import internal libs
import App from 'App';

function Protected() {
  return (
    <App>
      <Helmet>
        <title>Bạn không có quyền truy cập - 403</title>
      </Helmet>
      <Result
        status="403"
        title="403"
        subTitle="Bạn không có quyền truy cập trang này!"
        extra={
          <Link to="/">
            <Button type="primary">Về trang chủ</Button>
          </Link>
        }
      />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    config: state.config,
  };
};

export default connect(mapStateToProps)(Protected);
