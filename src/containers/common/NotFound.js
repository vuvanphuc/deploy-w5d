// import external libs
import React from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Result, Button } from 'antd';
import Auth from 'containers/auth/Authenticate';

function NotFound() {
  return (
    <Auth>
      <Helmet>
        <title>Không tìm thấy trang - 404</title>
      </Helmet>
      <Result
        status="404"
        title="404"
        subTitle="Chúng tôi không tìm thấy trang bạn cần!"
        extra={
          <Link to="/auth/login">
            <Button type="primary">Đăng nhập</Button>
          </Link>
        }
      />
    </Auth>
  );
}

const mapStateToProps = (state) => {
  return {
    config: state.config,
  };
};

export default connect(mapStateToProps)(NotFound);
