// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import moment from 'moment';
import { Row, Col, Form, Button, Tabs, Input, DatePicker, Modal, notification } from 'antd';
// import internal libs
import { config } from 'config';
import * as configAction from 'redux/actions/configure';
import { getLangText } from 'helpers/language.helper';
import App from 'App';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import Loading from 'components/loading/Loading';
import constants from 'constants/global.constants';
import { TextEditorWidget } from 'containers/form/components/widgets';
import defaultImage from 'assets/images/default.jpg';

const { TextArea } = Input;
const { TabPane } = Tabs;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_MOBILE;
const dateFormat = 'DD-MM-YYYY';
const dbDateFormat = 'YYYY-MM-DD';
function ConfigMobile(props) {
  const [form] = Form.useForm();
  const defaultForm = {
    patient_app_name: '',
    patient_app_slogan: '',
    patient_version: '',
    patient_release_date: moment(new Date(), dbDateFormat).utc(7),
    patient_terms_policies: '',
    patient_hotline: '',
    patient_email: '',
    patient_copyright: '',
    patient_website: '',
    patient_logo: '',
    patient_background: '',
    patient_facebook: '',
    doctor_version: '',
    doctor_release_date: moment(new Date(), dbDateFormat).utc(7),
    doctor_terms_policies: '',
    doctor_hotline: '',
    doctor_email: '',
    doctor_copyright: '',
    doctor_website: '',
    doctor_logo: '',
    doctor_background: '',
    patient_intro: '',
    doctor_intro: '',
  };
  const [state, setState] = useState({
    openMediaLibrary: false,
    mediaField: '',
    form: defaultForm,
    isEdit: false,
  });
  const patientConfigSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Cấu hình thành công.',
        });
      }
    };
    const data = JSON.stringify({ ...state.form, ...values });
    props.dispatch(configAction.createConfig({ key: CONFIG_KEY, values: data }, callback));
  };
  const doctorConfigSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Cấu hình thành công.',
        });
      }
    };
    // props.dispatch(configAction.createConfig({ key: CONFIG_KEY2, values: data }, callback));
  };
  const removeImage = (mediaField) =>
    setState((state) => ({
      ...state,
      form: {
        ...state.form,
        [mediaField]: '',
      },
    }));

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      form: {
        ...state.form,
        [state.mediaField]: file.tep_tin_url,
      },
    }));
  };

  const openMediaLibrary = (mediaField) => setState((state) => ({ ...state, openMediaLibrary: true, mediaField }));

  const onResetForm = () => {
    if (props.config.list.result && props.config.list.result.data && props.config.list.result.data.length > 0) {
      form.setFieldsValue(props.config.list.result.data[0]);
    } else {
      form.setFieldsValue(defaultForm);
    }
  };
  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        isEdit: true,
        form: {
          ...data,
          patient_release_date: moment(data.patient_release_date, dbDateFormat).utc(7),
          doctor_release_date: moment(data.doctor_release_date, dbDateFormat).utc(7),
        },
        // form:data,
      }));
      form.setFieldsValue(data);
    } else {
      setState((state) => ({
        ...state,
        isEdit: false,
        form: defaultForm,
      }));
    }
    onResetForm();
  }, [props]);
  return (
    <App>
      <Helmet>
        <title>Quản lý cấu hình Mobile App</title>
      </Helmet>
      {(props.config.list.loading || props.config.item.loading) && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row gutter={25}>
            <Col span={18}>
              <h2 className="header-form-title">Cấu hình Mobile App</h2>
            </Col>
            <Col span={6}>
              <Row>
                <Col span={12}></Col>
                <Col span={12}></Col>
              </Row>
            </Col>
            <Col span={24}>
              <div className="border-box-widget profile-more">
                <div className="border-box-body">
                  <div className="w5d-form configs-box">
                    <Tabs defaultActiveKey="PATIENT_CONFIG" size="large">
                      <TabPane tab="Cấu hình cho bệnh nhân" key="PATIENT_CONFIG">
                        <Form id="PatientConfig" layout="vertical" form={form} initialValues={defaultForm} onFinish={(e) => patientConfigSubmit(e)}>
                          <Row gutter={25}>
                            <Col xl={18} sm={18} xs={18} className="left-content">
                              <Form.Item
                                className="input-col"
                                label="Tên ứng dụng"
                                name="patient_app_name"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên ứng dụng là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Khẩu hiệu của ứng dụng"
                                name="patient_app_slogan"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Khẩu hiệu của ứng dụng là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Bản quyền"
                                name="patient_copyright"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Bản quyền là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Bản quyền"
                                name="patient_copyright"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Bản quyền là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Phiên bản"
                                name="patient_version"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Phiên bản là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Ngày phát hành"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Ngày phát hành là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <DatePicker
                                  placeholder="Ngày phát hành"
                                  value={state.form.patient_release_date ? moment(state.form.patient_release_date, dateFormat).utc(7) : ''}
                                  onChange={(value) => {
                                    if (value) {
                                      setState({
                                        ...state,
                                        form: { ...state.form, patient_release_date: moment(value, dateFormat).utc(7) },
                                      });
                                    }
                                  }}
                                  style={{ width: '50%' }}
                                  format={dateFormat}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label={'Giới thiệu ứng dụng'} name="patient_intro" rules={[]}>
                                <TextEditorWidget
                                  theme="snow"
                                  value={state.form.patient_intro}
                                  placeholder="Giới thiệu ứng dụng"
                                  onChange={(val) => setState({ ...state, form: { ...state.form, patient_intro: val } })}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label={'Điều khoản và chính sách'} name="patient_terms_policies" rules={[]}>
                                <TextEditorWidget
                                  theme="snow"
                                  value={state.form.patient_terms_policies}
                                  placeholder="Điều khoản và chính sách"
                                  onChange={(val) => setState({ ...state, form: { ...state.form, patient_terms_policies: val } })}
                                />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </Col>
                            <Col xl={6} sm={24} xs={24} className="left-content">
                              <W5dMediaBrowser
                                image={`${state.form.patient_logo}`}
                                field="patient_logo"
                                title={getLangText('CONFIG.LOGO')}
                                setImageLabel={getLangText('CONFIG.CHOOSE_LOGO')}
                                removeImageLabel={getLangText('CONFIG.REMOVE_LOGO')}
                                openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                                removeImage={(mediaField) => removeImage(mediaField)}
                                style={{ height: 100 }}
                              />
                              <W5dMediaBrowser
                                image={`${state.form.patient_background}`}
                                field="patient_background"
                                title={getLangText('CONFIG.BACKGROUND')}
                                setImageLabel={getLangText('CONFIG.CHOOSE_BACKGROUND')}
                                removeImageLabel={getLangText('CONFIG.REMOVE_BACKGROUND')}
                                openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                                removeImage={(mediaField) => removeImage(mediaField)}
                              />
                              <div className="border-box">
                                <h2>Thông tin liên hệ</h2>
                                <Form.Item className="input-col" name="patient_hotline" rules={[]}>
                                  <Input placeholder="Hotline" />
                                </Form.Item>
                                <Form.Item className="input-col" name="patient_email" rules={[]}>
                                  <Input placeholder="Email" />
                                </Form.Item>
                                <Form.Item className="input-col" name="patient_facebook" rules={[]}>
                                  <Input placeholder="Facebook" />
                                </Form.Item>
                                <Form.Item className="input-col" name="patient_website" rules={[]}>
                                  <Input placeholder="Website" />
                                </Form.Item>
                              </div>
                            </Col>
                          </Row>
                        </Form>
                        <Modal
                          wrapClassName="w5d-modal-media-library"
                          visible={state.openMediaLibrary}
                          onCancel={() =>
                            setState((state) => ({
                              ...state,
                              openMediaLibrary: false,
                            }))
                          }
                          footer={null}
                        >
                          <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
                        </Modal>
                      </TabPane>
                      <TabPane tab="Cấu hình cho bác sĩ" key="DOCTOR_CONFIG">
                        <Form id="DoctorConfig" layout="vertical" form={form} initialValues={defaultForm} onFinish={(e) => patientConfigSubmit(e)}>
                          <Row gutter={25}>
                            <Col xl={18} sm={18} xs={18} className="left-content">
                              <Form.Item
                                className="input-col"
                                label="Bản quyền"
                                name="doctor_copyright"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Bản quyền là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Phiên bản"
                                name="doctor_version"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Phiên bản là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Ngày phát hành"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Ngày phát hành là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <DatePicker
                                  placeholder="Ngày phát hành"
                                  value={state.form.doctor_release_date ? moment(state.form.doctor_release_date, dateFormat).utc(7) : ''}
                                  onChange={(value) => {
                                    if (value) {
                                      setState({
                                        ...state,
                                        form: { ...state.form, doctor_release_date: moment(value, dateFormat).utc(7) },
                                      });
                                    }
                                  }}
                                  style={{ width: '50%' }}
                                  format={dateFormat}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label={'Giới thiệu ứng dụng'} name="doctor_intro" rules={[]}>
                                <TextEditorWidget
                                  theme="snow"
                                  value={state.form.doctor_intro}
                                  placeholder="Giới thiệu ứng dụng"
                                  onChange={(val) => setState({ ...state, form: { ...state.form, doctor_intro: val } })}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label={'Điều khoản và chính sách'} name="doctor_terms_policies" rules={[]}>
                                <TextEditorWidget
                                  theme="snow"
                                  value={state.form.doctor_terms_policies}
                                  placeholder="Điều khoản và chính sách"
                                  onChange={(val) => setState({ ...state, form: { ...state.form, doctor_terms_policies: val } })}
                                />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </Col>
                            <Col xl={6} sm={24} xs={24} className="left-content">
                              <W5dMediaBrowser
                                image={`${state.form.doctor_logo}`}
                                field="doctor_logo"
                                title={getLangText('CONFIG.LOGO')}
                                setImageLabel={getLangText('CONFIG.CHOOSE_LOGO')}
                                removeImageLabel={getLangText('CONFIG.REMOVE_LOGO')}
                                openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                                removeImage={(mediaField) => removeImage(mediaField)}
                              />
                              <W5dMediaBrowser
                                image={`${state.form.doctor_background}`}
                                field="doctor_background"
                                title={getLangText('CONFIG.BACKGROUND')}
                                setImageLabel={getLangText('CONFIG.CHOOSE_BACKGROUND')}
                                removeImageLabel={getLangText('CONFIG.REMOVE_BACKGROUND')}
                                openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                                removeImage={(mediaField) => removeImage(mediaField)}
                              />
                            </Col>
                          </Row>
                        </Form>
                      </TabPane>
                    </Tabs>
                    {/* <Form.Item className="input-col">
                      <Row className="btn-groups">
                        <Col span={24}>
                          <Button type="primary" htmlType="submit">
                            Lưu lại
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item> */}
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ConfigMobile);
