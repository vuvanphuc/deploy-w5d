// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Row, Col, Form, Button, Tabs, Input, notification } from 'antd';
// import internal libs
import * as configAction from 'redux/actions/configure';
import { getLangText } from 'helpers/language.helper';
import App from 'App';
import Loading from 'components/loading/Loading';
import constants from 'constants/global.constants';
const { TextArea } = Input;
const { TabPane } = Tabs;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_EMAIL;

function ConfigEmail(props) {
  const [form] = Form.useForm();
  const defaultForm = {
    email_sender_name: '',
    email_sender: '',
    email_port: 465,
    email_host: 'smtp.gmail.com',
    email_encryption: 'ssl',
    email_username: '',
    email_password: '',
  };
  const [state, setState] = useState({
    openMediaLibrary: false,
    mediaField: '',
    form: defaultForm,
    isEdit: false,
  });

  const handleSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Lưu thông tin cấu hình thành công.',
        });
      }
    };
    const data = JSON.stringify({ ...state.form, ...values });
    props.dispatch(configAction.createConfig({ key: CONFIG_KEY, values: data }, callback));
  };
  const sendEmailSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Gửi email thành công.',
        });
      }
    };
    props.dispatch(configAction.sendTestEmail({ sender: state.form, values }, callback));
  };

  const onResetForm = () => {
    if (props.config.list.result && props.config.list.result.data && props.config.list.result.data.length > 0) {
      form.setFieldsValue(props.config.list.result.data[0]);
    } else {
      form.setFieldsValue(defaultForm);
    }
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        isEdit: true,
        form: data,
      }));
      form.setFieldsValue(data);
    } else {
      setState((state) => ({
        ...state,
        isEdit: false,
        form: defaultForm,
      }));
    }
    onResetForm();
  }, [props]);

  return (
    <App>
      <Helmet>
        <title>Quản lý cấu hình E-mail</title>
      </Helmet>
      {(props.config.list.loading || props.config.item.loading) && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row gutter={25}>
            <Col span={18}>
              <h2 className="header-form-title">{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</h2>
            </Col>
            <Col span={6}>
              <Row>
                <Col span={12}></Col>
                <Col span={12}></Col>
              </Row>
            </Col>
            <Col span={24}>
              <div className="border-box-widget profile-more">
                <div className="border-box-body">
                  <div className="w5d-form configs-box">
                    <Row gutter={25}>
                      <Col xl={24} sm={24} xs={24} className="left-content">
                        <Tabs defaultActiveKey="DEFAULT_EMAIL" size="large">
                          <TabPane tab={getLangText('CONFIG.EMAIL')} key="DEFAULT_EMAIL">
                            <Form id="configEmail" layout="vertical" form={form} onFinish={(e) => handleSubmit(e)}>
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.SENDER_NAME')}
                                name="email_sender_name"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Người gửi là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.SENDER_EMAIL')}
                                name="email_sender"
                                rules={[
                                  {
                                    required: true,
                                    message: 'E-mail người gửi là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.HOST')}
                                name="email_host"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Host là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.PORT')}
                                name="email_port"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Port là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.ENCRYPTION')}
                                name="email_encryption"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Mã hóa là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.USERNAME')}
                                name="email_username"
                                rules={[
                                  {
                                    required: true,
                                    message: 'E-mail gửi là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                name="email_password"
                                label={getLangText('CONFIG.PASSWORD')}
                                rules={[
                                  {
                                    required: true,
                                    message: 'Mật khẩu của e-mail là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input.Password />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </Form>
                          </TabPane>
                          <TabPane tab={getLangText('CONFIG.TEST_EMAIL')} key="TEST_SEND_EMAIL">
                            <Form id="sendTestEmail" layout="vertical" onFinish={(e) => sendEmailSubmit(e)}>
                              <Row>
                                <Col xl={11}>
                                  <Form.Item
                                    className="input-col"
                                    label={getLangText('CONFIG.RECEIVE_EMAIL')}
                                    name="test_receive_email"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Người nhận là trường bắt buộc.',
                                      },
                                    ]}
                                  >
                                    <Input />
                                  </Form.Item>
                                </Col>
                                <Col span="2"></Col>
                                <Col xl={11}>
                                  <Form.Item
                                    className="input-col"
                                    label={getLangText('CONFIG.RECEIVE_EMAIL_SUBJECT')}
                                    name="test_email_subject"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Tiêu đề là trường bắt buộc.',
                                      },
                                    ]}
                                  >
                                    <Input />
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Form.Item
                                className="input-col"
                                name="test_email_content"
                                label={getLangText('CONFIG.EMAIL_CONTENT')}
                                rules={[
                                  {
                                    required: true,
                                    message: 'Nội dung là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <TextArea rows={4} />
                              </Form.Item>
                              <Row className="btn-groups">
                                <Col span={24}>
                                  <Button type="primary" htmlType="submit">
                                    Gửi thử email
                                  </Button>
                                </Col>
                              </Row>
                            </Form>
                          </TabPane>
                        </Tabs>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ConfigEmail);
