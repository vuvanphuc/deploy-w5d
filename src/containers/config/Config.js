// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Row, Col, Form, Button, Tabs, Input, Modal, Radio, notification } from 'antd';

// import internal libs
import { config } from 'config';
import * as configAction from 'redux/actions/configure';
import * as userAction from 'redux/actions/user';
import { getLangText } from 'helpers/language.helper';
import App from 'App';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import Loading from 'components/loading/Loading';
import defaultImage from 'assets/images/default.jpg';
import constants from 'constants/global.constants';
import defaultBackground from 'assets/images/bg_main.png';
const { TabPane } = Tabs;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_COMMON;

function Config(props) {
  const [form] = Form.useForm();
  const defaultForm = {
    site_name: '',
    site_email: '',
    site_slogan: '',
    site_description: '',
    site_address: '',
    site_website: '',
    site_theme: 'THEME_BLUE',
    site_welcome: '',
    site_logo: '',
    site_minio_bucket: '',
    site_upload_server: '',
    enable_reg: false,
    default_role: '',
    site_favicon: '',
    site_language: '',
    site_background: '',
    social_facebook: '',
    social_youtube: '',
    social_twitter: '',
  };
  const [state, setState] = useState({
    openMediaLibrary: false,
    mediaField: '',
    form: defaultForm,
    isEdit: false,
    isDisabledUserGroup: true,
  });

  const handleSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Lưu thông tin cấu hình thành công.',
        });
      }
    };
    const data = JSON.stringify({ ...state.form, ...values });
    props.dispatch(configAction.createConfig({ key: CONFIG_KEY, values: data }, callback));
  };
  const removeImage = (mediaField) =>
    setState((state) => ({
      ...state,
      form: {
        ...state.form,
        [mediaField]: '',
      },
    }));

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      form: {
        ...state.form,
        [state.mediaField]: file.tep_tin_url,
      },
    }));
  };

  const openMediaLibrary = (mediaField) => setState((state) => ({ ...state, openMediaLibrary: true, mediaField }));

  const onResetForm = () => {
    if (props.config.list.result && props.config.list.result.data && props.config.list.result.data.length > 0) {
      form.setFieldsValue(props.config.list.result.data[0]);
    } else {
      form.setFieldsValue(defaultForm);
    }
  };
  const onChange = () => {
    setState((state) => ({
      ...state,
      isDisabledUserGroup: !state.isDisabledUserGroup,
    }));
  };
  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        isEdit: true,
        form: data,
        isDisabledUserGroup: !data.enable_reg,
      }));
      form.setFieldsValue(data);
    } else {
      setState((state) => ({
        ...state,
        isEdit: false,
        form: defaultForm,
        isDisabledUserGroup: true,
      }));
    }
    onResetForm();
  }, [props]);
  useEffect(() => {
    props.dispatch(userAction.getUserCates());
  }, []);

  return (
    <App>
      <Helmet>
        <title>{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</title>
      </Helmet>
      {(props.config.list.loading || props.config.item.loading) && <Loading />}
      <Form layout="vertical" form={form} onFinish={(e) => handleSubmit(e)}>
        <Row className="app-main">
          <Col span={24} className="body-content">
            <Row gutter={25}>
              <Col span={18}>
                <h2 className="header-form-title">{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</h2>
              </Col>
              <Col span={6}>
                <Row>
                  <Col span={12}></Col>
                  <Col span={12}></Col>
                </Row>
              </Col>
              <Col span={24}>
                <div className="border-box-widget profile-more">
                  <div className="border-box-body">
                    <div className="w5d-form configs-box">
                      <Row gutter={25}>
                        <Col xl={18} sm={24} xs={24} className="left-content">
                          <Tabs defaultActiveKey="1" size="large">
                            <TabPane tab={getLangText('CONFIG.GENERAL')} key="1">
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.ADMIN_EMAIL')}
                                name="site_email"
                                rules={[
                                  {
                                    required: true,
                                    message: 'E-mail quản trị là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label={getLangText('CONFIG.NAME')}
                                name="site_name"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên website là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item className="input-col" label={getLangText('CONFIG.SLOGAN')} name="site_slogan" rules={[]}>
                                <Input />
                              </Form.Item>
                              <Form.Item className="input-col" label={getLangText('CONFIG.DESCRIPTION')} name="site_description" rules={[]}>
                                <Input />
                              </Form.Item>
                              <Form.Item className="input-col" label={getLangText('CONFIG.ADDRESS')} name="site_address" rules={[]}>
                                <Input />
                              </Form.Item>
                              <Form.Item className="input-col" label={getLangText('CONFIG.WEBSITE')} name="site_website" rules={[]}>
                                <Input />
                              </Form.Item>
                              <Form.Item className="input-col" label="Tông màu trang quản trị" name="site_theme" rules={[]}>
                                <Radio.Group options={constants.THEME_TYPES} optionType="button" buttonStyle="solid" />
                              </Form.Item>
                              <br />
                              <Form.Item className="input-col" label="Chào mừng quản trị viên" name="site_welcome" rules={[]}>
                                <Input placeholder="Câu chào mừng trong trang quản trị" />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </TabPane>
                          </Tabs>
                        </Col>
                        <Col xl={6} sm={24} xs={24} className="left-content">
                          <div className="border-box">
                            <h2>Hình ảnh</h2>
                            <Form.Item className="input-col" label="Server hình ảnh" name="site_upload_server" rules={[]}>
                              <Radio.Group options={constants.UPLOAD_TYPES} optionType="button" buttonStyle="solid" />
                            </Form.Item>
                            <Form.Item className="input-col" name="site_minio_bucket" rules={[]} label="MinIO bucket">
                              <Input placeholder="Nhập minio bucket" />
                            </Form.Item>
                          </div>
                          <div className="border-box">
                            <h2>{getLangText('CONFIG.SOCIAL')}</h2>
                            <Form.Item className="input-col" name="social_facebook" rules={[]}>
                              <Input placeholder={getLangText('CONFIG.FACEBOOK')} />
                            </Form.Item>
                            <Form.Item className="input-col" name="social_youtube" rules={[]}>
                              <Input placeholder={getLangText('CONFIG.YOUTUBE')} />
                            </Form.Item>
                            <Form.Item className="input-col" name="social_twitter" rules={[]}>
                              <Input placeholder={getLangText('CONFIG.TWITTER')} />
                            </Form.Item>
                          </div>
                          <W5dMediaBrowser
                            image={state.form.site_logo ? `${state.form.site_logo}` : defaultImage}
                            field="site_logo"
                            title={getLangText('CONFIG.LOGO')}
                            setImageLabel={getLangText('CONFIG.CHOOSE_LOGO')}
                            removeImageLabel={getLangText('CONFIG.REMOVE_LOGO')}
                            openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                            removeImage={(mediaField) => removeImage(mediaField)}
                          />
                          <W5dMediaBrowser
                            image={state.form.site_favicon ? `${state.form.site_favicon}` : defaultImage}
                            field="site_favicon"
                            title={getLangText('CONFIG.FAVICON')}
                            setImageLabel={getLangText('CONFIG.CHOOSE_FAVICON')}
                            removeImageLabel={getLangText('CONFIG.REMOVE_FAVICON')}
                            openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                            removeImage={(mediaField) => removeImage(mediaField)}
                          />
                          <W5dMediaBrowser
                            image={state.form.site_background ? `${state.form.site_background}` : defaultBackground}
                            field="site_background"
                            title={getLangText('CONFIG.BACKGROUND')}
                            setImageLabel={getLangText('CONFIG.CHOOSE_BACKGROUND')}
                            removeImageLabel={getLangText('CONFIG.REMOVE_BACKGROUND')}
                            openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                            removeImage={(mediaField) => removeImage(mediaField)}
                          />
                        </Col>
                      </Row>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={state.openMediaLibrary}
        onCancel={() =>
          setState((state) => ({
            ...state,
            openMediaLibrary: false,
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Config);
