// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Row, Col, Form, Button, Input, Select, InputNumber, List, Radio, Space, notification } from 'antd';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

// import internal libs
import * as configAction from 'redux/actions/configure';
import * as tableAction from 'redux/actions/table';
import * as formAction from 'redux/actions/form';
import { getLangText } from 'helpers/language.helper';
import App from 'App';
import Loading from 'components/loading/Loading';
import constants from 'constants/global.constants';
import Icon from 'components/icon/Icon';

const { Option } = Select;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_CATEGORY;

function ConfigCategoryModules(props) {
  const [form] = Form.useForm();
  const defaultForm = {
    module_name: '',
    module_id: '',
    module_icon: '',
    module_form: '',
    module_table_key: '',
    module_fields: [],
  };
  const [state, setState] = useState({
    form: defaultForm,
    isEdit: false,
    modules: [],
  });
  const [table, setTable] = useState();
  const [fields, setFields] = useState([]);

  const handleSubmit = (values) => {
    if (state.isEdit) {
      const body = { ...state.form, ...values };
      const modules = state.modules.map((mode) => (mode.module_id === body.module_id ? { ...mode, ...body } : mode));
      setState((state) => ({
        ...state,
        isEdit: false,
        modules,
      }));
      form.setFieldsValue(defaultForm);
      window.scrollTo({
        top: 200,
        behavior: 'smooth',
      });
    } else {
      const body = { ...state.form, ...values };
      // check existed
      const moduleItem = state.modules.find((mode) => mode.module_id === body.module_id);
      if (moduleItem) {
        return notification.error({
          message: 'Mô đun ID đã tồn tại.',
        });
      }
      setState((state) => ({
        ...state,
        modules: state.modules.concat([body]),
      }));
      form.setFieldsValue(defaultForm);
    }
  };

  const onSaveModules = () => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Lưu thông tin cấu hình thành công.',
        });
      }
    };
    props.dispatch(configAction.createConfig({ key: CONFIG_KEY, values: JSON.stringify({ modules: state.modules }) }, callback));
  };
  const handleOnDragEnd = (result) => {
    if (!result.destination) return;
    const moduleData = form.getFieldsValue();
    const items = Array.from(moduleData.module_fields);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);
    form.setFieldsValue({
      ...moduleData,
      module_fields: items,
    });
  };

  const onChangePrimaryKey = (key) => {
    const moduleData = form.getFieldsValue();
    let items = Array.from(moduleData.module_fields);
    items = items.map((item, idx) => (idx === key ? { ...item, key: true } : { ...item, key: false }));
    form.setFieldsValue({
      ...moduleData,
      module_fields: items,
    });
  };
  const onChangeIcon = (value) => {
    setState({ ...state, form: { ...state.form, ...form.getFieldsValue(), module_icon: value } });
    form.setFieldsValue({ ...state.form, ...form.getFieldsValue(), module_icon: value });
  };
  const onResetForm = () => {
    if (props.config.list.result && props.config.list.result.data && props.config.list.result.data.length > 0) {
      form.setFieldsValue(props.config.list.result.data[0]);
    } else {
      form.setFieldsValue(defaultForm);
    }
  };

  const onReset = () => {
    setState((state) => ({
      ...state,
      form: defaultForm,
      isEdit: false,
    }));
    form.resetFields();
  };

  const renderColumns = () => {
    const columns = get(props, 'table.list.result.data', []).filter((table) => table.bangid === form.getFieldValue('module_table'));
    const catesOpts = columns.map((cate) => {
      return (
        <Option key={cate.truongid} label={cate.tentruong} value={cate.csdl_tentruong}>
          {cate.tentruong}
        </Option>
      );
    });
    return (
      <Select showSearch optionFilterProp="children" placeholder="Chọn cột dữ liệu">
        {catesOpts}
      </Select>
    );
  };

  const renderFormats = () => {
    const catesOpts = constants.FORMAT_TYPES.map((type) => {
      return (
        <Option key={type.value} label={type.title} value={type.value}>
          {type.title}
        </Option>
      );
    });
    return (
      <Select showSearch optionFilterProp="children" placeholder="Chọn kiểu hiển thị" onChange={(val) => setFields(form.getFieldValue('module_fields'))}>
        {catesOpts}
      </Select>
    );
  };

  const onEditCategory = (item) => {
    setState((state) => ({
      ...state,
      isEdit: true,
      form: item,
    }));
    form.setFieldsValue(item);
    window.scrollTo({
      top: 660,
      behavior: 'smooth',
    });
  };

  const onRemove = (item) => {
    const modules = state.modules.filter((mod) => mod.module_id !== item.module_id);
    setState((state) => ({
      ...state,
      modules,
    }));
  };

  const renderTable = (module_table) => {
    const tableObj = get(props, 'table.listCates.result.data', []).find((tbl) => tbl.bangid === module_table);
    return tableObj ? tableObj.tenbang : '';
  };

  const renderForm = (bieu_mau_id) => {
    const formObj = get(props, 'form.list.result.data', []).find((cate) => cate.bieu_mau_id === bieu_mau_id);
    return formObj ? formObj.ten_bieu_mau : '';
  };

  const renderForms = () => {
    const cates = get(props, 'form.list.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.bieu_mau_id}>
          {cate.ten_bieu_mau}
        </Option>
      );
    });
    return (
      <Select
        placeholder="Chọn bảng dữ liệu"
        onChange={(value) => {
          setTable(value);
        }}
      >
        {catesOpts}
      </Select>
    );
  };
  const renderTables = () => {
    const cates = get(props, 'table.listCates.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.bangid}>
          {cate.tenbang}
        </Option>
      );
    });
    return (
      <Select
        placeholder="Chọn bảng dữ liệu"
        onChange={(value) => {
          setTable(value);
        }}
      >
        {catesOpts}
      </Select>
    );
  };

  useEffect(() => {
    props.dispatch(tableAction.getColumns());
    props.dispatch(tableAction.getTables());
    props.dispatch(formAction.getForms());
  }, []);

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        modules: data.modules,
      }));
    } else {
      setState((state) => ({
        ...state,
        form: defaultForm,
      }));
    }
    onResetForm();
  }, [props.config.list.result.data]);

  return (
    <App>
      <Helmet>
        <title>Quản lý cấu hình mô đun danh mục</title>
      </Helmet>
      {(props.config.list.loading || props.config.item.loading) && <Loading />}
      <Form layout="vertical" form={form} onFinish={(e) => handleSubmit(e)} name="edit-config">
        <Row className="app-main">
          <Col span={24} className="body-content">
            <Row gutter={25}>
              <Col span={18}>
                <h2 className="header-form-title">{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</h2>
              </Col>
              <Col span={6}>
                <Row>
                  <Col span={12}></Col>
                  <Col span={12}></Col>
                </Row>
              </Col>
              <Col span={24}>
                <div className="border-box-widget profile-more">
                  <div className="border-box-body">
                    <div className="w5d-form configs-box">
                      <Row gutter={25}>
                        <Col span={24}>
                          <h2>Quản lý mô đun danh mục</h2>
                        </Col>
                        <Col span={24} className="left-content">
                          <div className="w5d-list">
                            <List
                              locale={{
                                emptyText: getLangText('GLOBAL.NO_ITEMS'),
                              }}
                              header={
                                <Row>
                                  <Col span={8} className="text-left">
                                    Tên mô đun
                                  </Col>
                                  <Col span={7} className="text-left">
                                    Tên bảng trong CSDL
                                  </Col>
                                  <Col span={4} className="text-left">
                                    Mô đun ID
                                  </Col>
                                  <Col span={4} className="text-left">
                                    Biểu mẫu
                                  </Col>
                                </Row>
                              }
                              dataSource={get(state, 'modules', [])}
                              renderItem={(item) => (
                                <List.Item key={item._id}>
                                  <Row className="full">
                                    <Col span={8} className="text-left">
                                      <Link className="edit" to={`#`}>
                                        {item.module_name}
                                        <div className="actions">
                                          <Button className="edit act" type="link" onClick={() => onEditCategory(item)}>
                                            {getLangText('GLOBAL.EDIT')}
                                          </Button>
                                          <Button className="delete act" type="link" onClick={() => onRemove(item)}>
                                            {getLangText('GLOBAL.DELETE')}
                                          </Button>
                                        </div>
                                      </Link>
                                    </Col>
                                    <Col span={7} className="text-left">
                                      {renderTable(item.module_table)}
                                    </Col>
                                    <Col span={4} className="text-left">
                                      {item.module_id}
                                    </Col>
                                    <Col span={4} className="text-left">
                                      {renderForm(item.module_form)}
                                    </Col>
                                  </Row>
                                </List.Item>
                              )}
                            />
                          </div>
                          <Row>
                            <Col span="6">
                              <br />
                              <Form.Item>
                                <Button block type="primary" onClick={() => onSaveModules()}>
                                  {true ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                                </Button>
                              </Form.Item>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} className="left-content">
                          <h3>Thông tin chung</h3>
                          <Row gutter={[16, 16]}>
                            <Col xl={12} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Tên mô đun"
                                name="module_name"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên mô đun là trường bắt buộc',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Mô đun ID"
                                name="module_id"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Mô đun ID là trường bắt buộc',
                                  },
                                ]}
                              >
                                <Input placeholder="Ví dụ: patient" disabled={state.isEdit} />
                              </Form.Item>
                            </Col>
                            <Col xl={12} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Biểu mẫu"
                                name="module_form"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Biểu mẫu là trường bắt buộc',
                                  },
                                ]}
                              >
                                {renderForms()}
                              </Form.Item>
                              <Row gutter={[16, 16]}>
                                <Col xl={12} sm={12} xs={24}>
                                  <Form.Item
                                    className="input-col"
                                    label="Bảng dữ liệu"
                                    name="module_table"
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Bảng dữ liệu là trường bắt buộc',
                                      },
                                    ]}
                                  >
                                    {renderTables()}
                                  </Form.Item>
                                </Col>
                                <Col xl={12} sm={12} xs={24}>
                                  <Form.Item className="input-col" label="Khóa chính" name="module_table_key" rules={[]}>
                                    {renderColumns()}
                                  </Form.Item>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <h3>Hiển thị dữ liệu</h3>
                          <Row gutter={[16, 16]}>
                            <Col xl={7} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Hiển thị ô tìm kiếm"
                                name="module_show_search_box"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Hiển thị ô tìm kiếm là trường bắt buộc',
                                  },
                                ]}
                              >
                                <Radio.Group
                                  options={[
                                    { label: 'Không', value: false },
                                    { label: 'Có', value: true },
                                  ]}
                                  optionType="button"
                                  buttonStyle="solid"
                                />
                              </Form.Item>
                            </Col>
                            <Col xl={6} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Lọc theo trạng thái"
                                name="module_show_filter_status"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Hiển thị ô lọc trạng thái là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Radio.Group
                                  options={[
                                    { label: 'Không', value: false },
                                    { label: 'Có', value: true },
                                  ]}
                                  optionType="button"
                                  buttonStyle="solid"
                                />
                              </Form.Item>
                            </Col>
                            <Col xl={4} sm={12} xs={24}>
                              <Form.Item className="input-col" label="Icon" name="module_icon" rules={[]}>
                                <Icon value={state.module_icon} onChangeIcon={(values) => onChangeIcon(values)} />
                              </Form.Item>
                            </Col>
                          </Row>
                          <DragDropContext onDragEnd={(result) => handleOnDragEnd(result)}>
                            <Droppable droppableId="fieldId">
                              {(provided) => (
                                <div className="characters" {...provided.droppableProps} ref={provided.innerRef}>
                                  {form.getFieldValue('module_table') && (
                                    <Form.List name="module_fields">
                                      {(fields, { add, remove }) => (
                                        <div className="custom-view-layout">
                                          {fields.map(({ key, name, fieldKey, ...restField }) => (
                                            <div key={key}>
                                              <Draggable draggableId={`${key}`} index={key} key={`${key}_${name}`}>
                                                {(provided) => (
                                                  <Row gutter={[16, 16]}>
                                                    <Col xl={1} sm={0} xs={0} className="text-left">
                                                      <span className="nbr" ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                        {key + 1}
                                                      </span>
                                                    </Col>
                                                    <Col xl={3} sm={12} xs={24}>
                                                      <Form.Item
                                                        className="input-col"
                                                        label="Khóa chính"
                                                        name={[name, 'key']}
                                                        fieldKey={[fieldKey, 'key']}
                                                        rules={[
                                                          {
                                                            required: true,
                                                            message: 'Khóa chính là trường bắt buộc',
                                                          },
                                                        ]}
                                                      >
                                                        <Radio.Group
                                                          onChange={() => {
                                                            onChangePrimaryKey(key);
                                                          }}
                                                          options={[
                                                            { label: 'Không', value: false },
                                                            { label: 'Có', value: true },
                                                          ]}
                                                          optionType="button"
                                                          buttonStyle="solid"
                                                        />
                                                      </Form.Item>
                                                    </Col>
                                                    <Col xl={4} sm={12} xs={24}>
                                                      <Form.Item
                                                        className="input-col"
                                                        label="Tiêu đề cột"
                                                        name={[name, 'title']}
                                                        fieldKey={[fieldKey, 'title']}
                                                        rules={[
                                                          {
                                                            required: true,
                                                            message: 'Tên trường là bắt buộc.',
                                                          },
                                                        ]}
                                                      >
                                                        <Input placeholder="Tên cột dữ liệu" />
                                                      </Form.Item>
                                                    </Col>
                                                    <Col xl={4} sm={12} xs={24}>
                                                      {' '}
                                                      <Form.Item
                                                        className="input-col"
                                                        label="Cột dữ liệu"
                                                        name={[name, 'column']}
                                                        fieldKey={[fieldKey, 'column']}
                                                        rules={[
                                                          {
                                                            required: true,
                                                            message: 'Cột dữ liệu trường bắt buộc.',
                                                          },
                                                        ]}
                                                      >
                                                        {renderColumns()}
                                                      </Form.Item>
                                                    </Col>
                                                    <Col xl={4} sm={12} xs={24}>
                                                      <Form.Item
                                                        className="input-col"
                                                        label="Kiểu hiển thị"
                                                        name={[name, 'format']}
                                                        fieldKey={[fieldKey, 'format']}
                                                        rules={[
                                                          {
                                                            required: true,
                                                            message: 'Kiểu hiển thị là bắt buộc.',
                                                          },
                                                        ]}
                                                      >
                                                        {renderFormats()}
                                                      </Form.Item>
                                                      {form.getFieldValue('module_fields')[key] && form.getFieldValue('module_fields')[key].format === 'custom' && (
                                                        <Form.List name={[name, 'options']} fieldKey={[fieldKey, 'options']}>
                                                          {(subFields, subProps) => (
                                                            <>
                                                              {subFields.map(({ key, name, fieldKey, ...restField }) => (
                                                                <Space key={key} style={{ display: 'flex', marginBottom: 0 }} align="baseline">
                                                                  <Form.Item
                                                                    {...restField}
                                                                    name={[name, 'input']}
                                                                    fieldKey={[fieldKey, 'input']}
                                                                    rules={[
                                                                      {
                                                                        required: true,
                                                                        message: 'Giá trị thực là trường bắt buộc',
                                                                      },
                                                                    ]}
                                                                  >
                                                                    <Input size="small" placeholder="Giá trị thực" />
                                                                  </Form.Item>
                                                                  <Form.Item
                                                                    {...restField}
                                                                    name={[name, 'output']}
                                                                    fieldKey={[fieldKey, 'output']}
                                                                    rules={[
                                                                      {
                                                                        required: true,
                                                                        message: 'Giá trị hiển thị là trường bắt buộc',
                                                                      },
                                                                    ]}
                                                                  >
                                                                    <Input size="small" placeholder="Giá trị hiển thị" />
                                                                  </Form.Item>
                                                                  <DeleteOutlined onClick={() => subProps.remove(name)} />
                                                                </Space>
                                                              ))}
                                                              <Form.Item>
                                                                <Button type="dashed" size="small" onClick={() => subProps.add()} icon={<PlusOutlined />}>
                                                                  Thêm
                                                                </Button>
                                                              </Form.Item>
                                                            </>
                                                          )}
                                                        </Form.List>
                                                      )}
                                                    </Col>
                                                    <Col xl={4} sm={12} xs={24}>
                                                      {' '}
                                                      <Form.Item
                                                        className="input-col"
                                                        label="Độ rộng(tối đa 12)"
                                                        name={[name, 'width']}
                                                        fieldKey={[fieldKey, 'width']}
                                                        rules={[
                                                          {
                                                            required: true,
                                                            message: 'Độ rộng là trường bắt buộc',
                                                          },
                                                        ]}
                                                      >
                                                        <InputNumber style={{ width: '100%' }} placeholder="Độ rộng" min={0} max={12} value={4} />
                                                      </Form.Item>
                                                    </Col>

                                                    <Col xl={3} sm={12} xs={24}>
                                                      {' '}
                                                      <Form.Item
                                                        className="input-col"
                                                        label="Sắp xếp?"
                                                        name={[name, 'isSortable']}
                                                        fieldKey={[fieldKey, 'isSortable']}
                                                        rules={[
                                                          {
                                                            required: true,
                                                            message: 'Có thể sắp xếp là trường bắt buộc',
                                                          },
                                                        ]}
                                                      >
                                                        <Radio.Group
                                                          options={[
                                                            { label: 'Không', value: false },
                                                            { label: 'Có', value: true },
                                                          ]}
                                                          optionType="button"
                                                          buttonStyle="solid"
                                                        />
                                                      </Form.Item>
                                                    </Col>
                                                    <Button onClick={() => remove(name)} danger type="link" className="btn-remove-condition">
                                                      <DeleteOutlined />
                                                    </Button>
                                                  </Row>
                                                )}
                                              </Draggable>
                                            </div>
                                          ))}
                                          <Row>
                                            <Col xs={6}>
                                              <Form.Item>
                                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                                  Thêm cột dữ liệu
                                                </Button>
                                              </Form.Item>
                                            </Col>
                                          </Row>
                                        </div>
                                      )}
                                    </Form.List>
                                  )}
                                </div>
                              )}
                            </Droppable>
                          </DragDropContext>
                          <Form.Item className="button-col">
                            <Button shape="round" type="primary" htmlType="submit">
                              {state.isEdit ? 'Cập nhật mô đun' : 'Thêm mô đun'}
                            </Button>
                            {state.isEdit && (
                              <Button shape="round" type="danger" onClick={() => onReset()}>
                                Hủy bỏ
                              </Button>
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
    table: state.table,
    form: state.form,
  };
};

export default connect(mapStateToProps)(ConfigCategoryModules);
