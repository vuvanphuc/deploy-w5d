import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { constant, get } from 'lodash';
import { Row, Col, Form, Button, Tabs, Input, Switch, Select, notification } from 'antd';

// import internal libs
import * as configAction from 'redux/actions/configure';
import * as userAction from 'redux/actions/user';
import { getLangText } from 'helpers/language.helper';
import App from 'App';
import constants from 'constants/global.constants';
import Loading from 'components/loading/Loading';
const { TabPane } = Tabs;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_CRONJOB;

function ConfigCronjob(props) {
  const [form] = Form.useForm();
  const defaultForm = {
    enable_history: false,
    default_role: '',
    enable_report: false,
    enable_backupdb: false,
  };
  const [state, setState] = useState({
    form: defaultForm,
    isEdit: false,
    isDisableHistory: true,
    isDisableReport: true,
    isDisableIdGoogle: true,
    isDisableBackupdb: true,
  });

  const handleSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Lưu thông tin cấu hình thành công.',
        });
      }
    };
    const data = JSON.stringify({ ...state.form, ...values });
    props.dispatch(configAction.createConfig({ key: CONFIG_KEY, values: data }, callback));
  };

  const onResetForm = () => {
    if (props.config.list.result && props.config.list.result.data && props.config.list.result.data.length > 0) {
      form.setFieldsValue(props.config.list.result.data[0]);
    } else {
      form.setFieldsValue(defaultForm);
    }
  };
  const onChangeEnableHistory = () => {
    setState((state) => ({
      ...state,
      isDisableHistory: !state.isDisableHistory,
    }));
  };
  const onChangeEnableReport = () => {
    setState((state) => ({
      ...state,
      isDisableReport: !state.isDisableReport,
    }));
  };
  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        isEdit: true,
        form: data,
        isDisableHistory: !data.enable_history,
        isDisableReport: !data.enable_report,
        isDisableIdGoogle: !data.enable_logingg,
        isDisableBackupdb: !data.enable_backupdb,
      }));
      form.setFieldsValue(data);
    } else {
      setState((state) => ({
        ...state,
        isEdit: false,
        form: defaultForm,
        isDisableHistory: true,
        isDisableReport: true,
        isDisableIdGoogle: true,
      }));
    }
    onResetForm();
  }, [props]);
  useEffect(() => {
    props.dispatch(userAction.getUserCates());
  }, []);
  return (
    <App>
      <Helmet>
        <title>{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</title>
      </Helmet>
      {(props.config.list.loading || props.config.item.loading) && <Loading />}
      <Form layout="vertical" form={form} onFinish={(e) => handleSubmit(e)}>
        <Row className="app-main">
          <Col span={24} className="body-content">
            <Row gutter={25}>
              <Col span={18}>
                <h2 className="header-form-title">{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</h2>
              </Col>
              <Col span={6}>
                <Row>
                  <Col span={12}></Col>
                  <Col span={12}></Col>
                </Row>
              </Col>
              <Col span={24}>
                <div className="border-box-widget profile-more">
                  <div className="border-box-body">
                    <div className="w5d-form configs-box">
                      <Row gutter={25}>
                        <Col xl={24} sm={24} xs={24} className="left-content">
                          <Tabs defaultActiveKey="1" size="large">
                            <TabPane tab="CẤU HÌNH CRON JOB" key="1">
                              <Row style={{ paddingBottom: 10 }}>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Cho phép chạy xóa history" name="enable_history" rules={[]} valuePropName="checked">
                                    <Switch checkedChildren="Có" unCheckedChildren="Không" value={state.form.enable_history ? true : false} onChange={onChangeEnableHistory} />
                                  </Form.Item>
                                </Col>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Đặt lịch chạy" name="fixTime_history">
                                    <Input style={{ width: '70%' }} disabled={state.isDisableHistory ? true : false}></Input>
                                  </Form.Item>
                                </Col>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Số ngày" name="maxTime_history">
                                    <Input style={{ width: '50%' }} disabled={state.isDisableHistory ? true : false}></Input>
                                  </Form.Item>
                                </Col>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Số bản ghi tối đa" name="maxRecord_history">
                                    <Input style={{ width: '50%' }} disabled={state.isDisableHistory ? true : false}></Input>
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Row>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Cho phép chạy báo cáo" name="enable_report" rules={[]} valuePropName="checked">
                                    <Switch checkedChildren="Có" unCheckedChildren="Không" value={state.form.enable_report ? true : false} onChange={onChangeEnableReport} />
                                  </Form.Item>
                                </Col>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Đặt lịch chạy" name="fixTime_report">
                                    <Input style={{ width: '70%' }} disabled={state.isDisableReport ? true : false}></Input>
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Row>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Cho phép backup cơ sở dữ liệu" name="enable_backupdb" rules={[]} valuePropName="checked">
                                    <Switch
                                      checkedChildren="Có"
                                      unCheckedChildren="Không"
                                      value={state.form.enable_backupdb ? true : false}
                                      onChange={() => {
                                        setState({ ...state, isDisableBackupdb: !state.isDisableBackupdb });
                                      }}
                                    />
                                  </Form.Item>
                                </Col>
                                <Col xl={6} sm={24} xs={24}>
                                  <Form.Item className="input-col" label="Đặt lịch chạy" name="fixTime_backupdb">
                                    <Input style={{ width: '70%' }} disabled={state.isDisableBackupdb ? true : false}></Input>
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </TabPane>
                          </Tabs>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ConfigCronjob);
