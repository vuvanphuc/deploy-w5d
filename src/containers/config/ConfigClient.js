// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import moment from 'moment';
import { Row, Col, Form, Button, Tabs, Input, DatePicker, Modal, notification } from 'antd';
// import internal libs
import { config } from 'config';
import * as configAction from 'redux/actions/configure';
import { getLangText } from 'helpers/language.helper';
import App from 'App';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import Loading from 'components/loading/Loading';
import constants from 'constants/global.constants';
import { TextEditorWidget } from 'containers/form/components/widgets';
import TextEditor from 'containers/form/components/widgets/TextEditor/TextEditor';
import defaultImage from 'assets/images/default.jpg';

const { TextArea } = Input;
const { TabPane } = Tabs;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_CLIENT;
const dateFormat = 'DD-MM-YYYY';
const dbDateFormat = 'YYYY-MM-DD';
function ConfigClient(props) {
  const [form] = Form.useForm();
  const defaultForm = {
    client_app_name: '',
    client_app_slogan: '',
    client_version: '',
    client_release_date: moment(new Date(), dbDateFormat).utc(7),
    client_terms_policies: '',
    client_intro: '',
    client_hotline: '',
    client_email: '',
    client_copyright: '',
    client_website: '',
    client_logo: '',
    client_background: '',
    client_facebook: '',
  };
  const [state, setState] = useState({
    openMediaLibrary: false,
    mediaField: '',
    form: defaultForm,
    isEdit: false,
  });
  const clientConfigSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Cấu hình thành công.',
        });
      }
    };
    const data = JSON.stringify({ ...state.form, ...values });
    props.dispatch(configAction.createConfig({ key: CONFIG_KEY, values: data }, callback));
  };

  const removeImage = (mediaField) =>
    setState((state) => ({
      ...state,
      form: {
        ...state.form,
        [mediaField]: '',
      },
    }));

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      form: {
        ...state.form,
        [state.mediaField]: file.tep_tin_url,
      },
    }));
  };

  const openMediaLibrary = (mediaField) => setState((state) => ({ ...state, openMediaLibrary: true, mediaField }));

  const onResetForm = () => {
    if (props.config.list.result && props.config.list.result.data && props.config.list.result.data.length > 0) {
      form.setFieldsValue(props.config.list.result.data[0]);
    } else {
      form.setFieldsValue(defaultForm);
    }
  };
  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      try {
        const data = JSON.parse(config.du_lieu);
        setState((state) => ({
          ...state,
          isEdit: true,
          form: {
            ...data,
            client_release_date: moment(data.client_release_date, dbDateFormat).utc(7),
            doctor_release_date: moment(data.doctor_release_date, dbDateFormat).utc(7),
          },
          // form:data,
        }));
        form.setFieldsValue(data);
      } catch (error) {
        console.log('error', error);
      }
    } else {
      setState((state) => ({
        ...state,
        isEdit: false,
        form: defaultForm,
      }));
    }
    onResetForm();
  }, [props]);
  return (
    <App>
      <Helmet>
        <title>Quản lý cấu hình trang khách</title>
      </Helmet>
      {(props.config.list.loading || props.config.item.loading) && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row gutter={25}>
            <Col span={18}>
              <h2 className="header-form-title">Cấu hình trang khách</h2>
            </Col>
            <Col span={6}>
              <Row>
                <Col span={12}></Col>
                <Col span={12}></Col>
              </Row>
            </Col>
            <Col span={24}>
              <div className="border-box-widget profile-more">
                <div className="border-box-body">
                  <div className="w5d-form configs-box">
                    <Tabs defaultActiveKey="CLIENT_CONFIG" size="large">
                      <TabPane tab="Cấu hình chung" key="CLIENT_CONFIG">
                        <Form id="ClientConfig" layout="vertical" form={form} initialValues={defaultForm} onFinish={(e) => clientConfigSubmit(e)}>
                          <Row gutter={25}>
                            <Col xl={18} sm={18} xs={18} className="left-content">
                              <Form.Item
                                className="input-col"
                                label="Tên website"
                                name="client_app_name"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên website là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Khẩu hiệu của website"
                                name="client_app_slogan"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Khẩu hiệu của website là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Bản quyền"
                                name="client_copyright"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Bản quyền là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Phiên bản"
                                name="client_version"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Phiên bản là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Ngày phát hành"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Ngày phát hành là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <DatePicker
                                  placeholder="Ngày phát hành"
                                  value={state.form.client_release_date ? moment(state.form.client_release_date, dateFormat).utc(7) : ''}
                                  onChange={(value) => {
                                    if (value) {
                                      setState({
                                        ...state,
                                        form: { ...state.form, client_release_date: moment(value, dateFormat).utc(7) },
                                      });
                                    }
                                  }}
                                  style={{ width: '50%' }}
                                  format={dateFormat}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label={'Giới thiệu'} name="client_intro" rules={[]}>
                                <TextEditor
                                  value={state.form.client_intro}
                                  placeholder="Giới thiệu"
                                  onChange={(val) => setState({ ...state, form: { ...state.form, client_intro: val } })}
                                  showToolbar={true}
                                  isMinHeight300={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label={'Điều khoản và chính sách'} name="client_terms_policies" rules={[]}>
                                <TextEditor
                                  value={state.form.client_terms_policies}
                                  placeholder="Điều khoản và chính sách"
                                  onChange={(val) => setState({ ...state, form: { ...state.form, client_terms_policies: val } })}
                                  showToolbar={true}
                                  isMinHeight300={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </Col>
                            <Col xl={6} sm={24} xs={24} className="left-content">
                              <W5dMediaBrowser
                                image={`${state.form.client_logo}`}
                                field="client_logo"
                                title={getLangText('CONFIG.LOGO')}
                                setImageLabel={getLangText('CONFIG.CHOOSE_LOGO')}
                                removeImageLabel={getLangText('CONFIG.REMOVE_LOGO')}
                                openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                                removeImage={(mediaField) => removeImage(mediaField)}
                                style={{ height: 100 }}
                              />
                              <W5dMediaBrowser
                                image={`${state.form.client_background}`}
                                field="client_background"
                                title={getLangText('CONFIG.BACKGROUND')}
                                setImageLabel={getLangText('CONFIG.CHOOSE_BACKGROUND')}
                                removeImageLabel={getLangText('CONFIG.REMOVE_BACKGROUND')}
                                openMediaLibrary={(mediaField) => openMediaLibrary(mediaField)}
                                removeImage={(mediaField) => removeImage(mediaField)}
                              />
                              <div className="border-box">
                                <h2>Thông tin liên hệ</h2>
                                <Form.Item className="input-col" name="client_hotline" rules={[]}>
                                  <Input placeholder="Hotline" />
                                </Form.Item>
                                <Form.Item className="input-col" name="client_email" rules={[]}>
                                  <Input placeholder="Email" />
                                </Form.Item>
                                <Form.Item className="input-col" name="client_facebook" rules={[]}>
                                  <Input placeholder="Facebook" />
                                </Form.Item>
                                <Form.Item className="input-col" name="client_website" rules={[]}>
                                  <Input placeholder="Website" />
                                </Form.Item>
                              </div>
                            </Col>
                          </Row>
                        </Form>
                        <Modal
                          wrapClassName="w5d-modal-media-library"
                          visible={state.openMediaLibrary}
                          onCancel={() =>
                            setState((state) => ({
                              ...state,
                              openMediaLibrary: false,
                            }))
                          }
                          footer={null}
                        >
                          <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
                        </Modal>
                      </TabPane>
                      <TabPane tab="Cấu hình menu" key="CLIENT_MENU_CONFIG">
                        <Form layout="vertical" form={form} initialValues={defaultForm} onFinish={(e) => clientConfigSubmit(e)}>
                          <Row gutter={25}>
                            <Col xl={18} sm={18} xs={18} className="left-content">
                              <Form.Item
                                className="input-col"
                                label="Tên website"
                                name="client_app_name"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên website là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </Col>
                          </Row>
                        </Form>
                      </TabPane>
                      <TabPane tab="Cấu hình sidebar" key="CLIENT_SIDEBAR_CONFIG">
                        <Form layout="vertical" form={form} initialValues={defaultForm} onFinish={(e) => clientConfigSubmit(e)}>
                          <Row gutter={25}>
                            <Col xl={18} sm={18} xs={18} className="left-content">
                              <Form.Item
                                className="input-col"
                                label="Tên website"
                                name="client_app_name"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên website là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </Col>
                          </Row>
                        </Form>
                      </TabPane>
                    </Tabs>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ConfigClient);
