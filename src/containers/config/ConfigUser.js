import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Row, Col, Form, Button, Tabs, Input, Switch, Select, notification } from 'antd';

// import internal libs
import * as configAction from 'redux/actions/configure';
import * as userAction from 'redux/actions/user';
import { getLangText } from 'helpers/language.helper';
import { recursiveUserCates } from 'helpers/common.helper';
import constants from 'constants/global.constants';
import App from 'App';
import Loading from 'components/loading/Loading';
const { TabPane } = Tabs;
const { Option } = Select;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_USER;

function ConfigUser(props) {
  const [form] = Form.useForm();
  const defaultForm = {
    enable_reg: false,
    default_role: '',
    enable_loginfb: false,
    enable_logingg: false,
    app_facebook_key: '',
    app_google_key: '',
  };
  const [state, setState] = useState({
    form: defaultForm,
    isEdit: false,
    isDisabledUserGroup: true,
    isDisableIdFacebook: true,
    isDisableIdGoogle: true,
  });

  const handleSubmit = (values) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Lưu thông tin cấu hình thành công.',
        });
      }
    };
    const data = JSON.stringify({ ...state.form, ...values });
    props.dispatch(configAction.createConfig({ key: CONFIG_KEY, values: data }, callback));
  };

  const onResetForm = () => {
    if (props.config.list.result && props.config.list.result.data && props.config.list.result.data.length > 0) {
      form.setFieldsValue(props.config.list.result.data[0]);
    } else {
      form.setFieldsValue(defaultForm);
    }
  };
  const onChangeUserGroup = () => {
    setState((state) => ({
      ...state,
      isDisabledUserGroup: !state.isDisabledUserGroup,
    }));
  };
  const onChangeLoginFacebook = () => {
    setState((state) => ({
      ...state,
      isDisableIdFacebook: !state.isDisableIdFacebook,
    }));
  };
  const onChangeLoginGoogle = () => {
    setState((state) => ({
      ...state,
      isDisableIdGoogle: !state.isDisableIdGoogle,
    }));
  };
  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        isEdit: true,
        form: data,
        isDisabledUserGroup: !data.enable_reg,
        isDisableIdFacebook: !data.enable_loginfb,
        isDisableIdGoogle: !data.enable_logingg,
      }));
      form.setFieldsValue(data);
    } else {
      setState((state) => ({
        ...state,
        isEdit: false,
        form: defaultForm,
        isDisabledUserGroup: true,
        isDisableIdFacebook: true,
        isDisableIdGoogle: true,
      }));
    }
    onResetForm();
  }, [props]);
  useEffect(() => {
    props.dispatch(userAction.getUserCates());
  }, []);

  const renderUserCategories = () => {
    const userCates = get(props, 'user.listCates.result.data', []);
    const cates = recursiveUserCates(userCates, userCates);
    const listCates = cates.map((cates) => {
      return <Option value={cates.nhom_nhan_vien_id}>{cates.titleLevel}</Option>;
    });
    return (
      <Select style={{ width: 150 }} disabled={state.isDisabledUserGroup ? true : false}>
        {listCates}
      </Select>
    );
  };

  return (
    <App>
      <Helmet>
        <title>{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</title>
      </Helmet>
      {(props.config.list.loading || props.config.item.loading) && <Loading />}
      <Form layout="vertical" form={form} onFinish={(e) => handleSubmit(e)}>
        <Row className="app-main">
          <Col span={24} className="body-content">
            <Row gutter={25}>
              <Col span={18}>
                <h2 className="header-form-title">{getLangText('GLOBAL.CONFIG_MANAGEMENT')}</h2>
              </Col>
              <Col span={6}>
                <Row>
                  <Col span={12}></Col>
                  <Col span={12}></Col>
                </Row>
              </Col>
              <Col span={24}>
                <div className="border-box-widget profile-more">
                  <div className="border-box-body">
                    <div className="w5d-form configs-box">
                      <Row gutter={25}>
                        <Col xl={18} sm={24} xs={24} className="left-content">
                          <Tabs defaultActiveKey="1" size="large">
                            <TabPane tab="CẤU HÌNH THÀNH VIÊN" key="1">
                              <Form.Item className="input-col" label="Cho phép đăng ký thành viên mới" name="enable_reg" rules={[]} valuePropName="checked">
                                <Switch checkedChildren="Có" unCheckedChildren="Không" value={state.form.enable_reg ? true : false} onChange={onChangeUserGroup} />
                              </Form.Item>

                              <Form.Item className="input-col" label="Vai trò mặc định" name="default_role" rules={[]}>
                                {renderUserCategories()}
                              </Form.Item>

                              <Form.Item className="input-col" label="Cho phép đăng nhập bằng facebook" name="enable_loginfb" valuePropName="checked">
                                <Switch checkedChildren="Có" unCheckedChildren="Không" value={state.form.enable_loginfb ? true : false} onChange={onChangeLoginFacebook} />
                              </Form.Item>
                              <Form.Item className="input-col" label="App key facebook" name="app_facebook_key">
                                <Input disabled={state.isDisableIdFacebook ? true : false} />
                              </Form.Item>

                              <Form.Item className="input-col" label="Cho phép đăng nhập bằng google" name="enable_logingg" valuePropName="checked">
                                <Switch checkedChildren="Có" unCheckedChildren="Không" value={state.form.enable_logingg ? true : false} onChange={onChangeLoginGoogle} />
                              </Form.Item>
                              <Form.Item className="input-col" label="App key google" name="app_google_key">
                                <Input disabled={state.isDisableIdGoogle ? true : false} />
                              </Form.Item>
                              <Form.Item className="input-col">
                                <Row className="btn-groups">
                                  <Col span={24}>
                                    <Button type="primary" htmlType="submit">
                                      Lưu lại
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </TabPane>
                          </Tabs>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ConfigUser);
