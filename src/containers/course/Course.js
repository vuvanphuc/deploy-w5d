// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
// import InfiniteScroll from 'react-infinite-scroller';
import { get } from 'lodash';
import { Row, Col, List, Avatar, Button, Checkbox, notification, Menu, Dropdown, Space, Modal, Upload, Tabs, Switch, Form } from 'antd';
import { EditOutlined, PlusOutlined, LockOutlined, DeleteOutlined, UnlockOutlined, QuestionCircleOutlined, DownloadOutlined, UploadOutlined, FolderOutlined, DownOutlined } from '@ant-design/icons';

// import internal libs
import * as courseAction from 'redux/actions/course';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl, getUserInformation } from 'helpers/common.helper';
import { config } from 'config';
import templateFile from 'assets/templates/User_temp.xlsx';
import App from 'App';
import constants from 'constants/global.constants';
import AppFilter from 'components/AppFilter';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import W5dImageMinIO from 'components/W5dImageMinIO';

const { confirm } = Modal;
const { TabPane } = Tabs;
const dateFormat = 'DD-MM-YYYY';

function Course(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);
  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };
  const searchCourses = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.course.list.search,
      ...urlQuery,
    };
    props.dispatch(courseAction.getCourses(searchParams));
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.course.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });
  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };
  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
      checkAll: 0,
      checkedList: [],
    }));
    changeUrlParams(field, value);
  };
  const onDelete = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Block khóa học thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa khóa học thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        searchCourses();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(courseAction.deleteCourse({ id }, callback));
      },
    });
  };

  const onSwitch = (body) => {
    const callback = (res) => {
      if (res.success) {
        searchCourses();
        notification.success({
          message: 'Đổi trạng thái thành công',
        });
      }
    };
    props.dispatch(courseAction.editCourse(body, callback));
  };
  const allOptions = get(props, 'course.list.result.data', []).map((item) => item.khoa_hoc_id);

  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (khoa_hoc_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(khoa_hoc_id) ? state.checkedList.filter((it) => it !== khoa_hoc_id) : state.checkedList.concat([khoa_hoc_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  function deleteListCourses(e) {
    const callback = (res) => {
      if (res.success) {
        searchCourses();
        notification.success({
          message: 'Xóa nhiều khóa học thành công.',
        });
        setState({ ...state, checkedList: [], checkAll: false });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa toàn bộ bài học, lịch sử thi của các khóa học được chọn không?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(courseAction.deleteCourses({ listIds: state.checkedList }, callback));
      },
    });
  }

  const createCourse = () => {
    //add a new course
    const callback = (res) => {
      if (res.success) {
        const routeLink = shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/sua') ? `/course/edit/${res.data.khoa_hoc_id}` : '/courses';
        history.push(routeLink);
      } else {
        notification.error({
          message: 'Không tạo được khóa học mới',
        });
      }
    };
    props.dispatch(
      courseAction.createCourse(
        {
          tieu_de: `Khóa học mới ${moment().format('DD-MM-YYYY')}`,
          trang_thai: 'inactive',
        },
        callback
      )
    );
  };

  //Active thanh vien theo list => active
  function activeListCourses(e) {
    const callback = (res) => {
      if (res.success) {
        searchCourses();
        notification.success({
          message: 'Kích hoạt khóa học thành công.',
        });
        setState({ ...state, checkedList: [], checkAll: false });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các khóa học được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(courseAction.editStatusCourse({ listIds: state.checkedList, status: 'active' }, callback));
        },
      });
    }
  }
  //Khoa thanh vien theo list => inactive
  function blockListCourses(e) {
    const callback = (res) => {
      if (res.success) {
        searchCourses();
        notification.success({
          message: 'Khóa khóa học thành công.',
        });
        setState({ ...state, checkedList: [], checkAll: false });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn chắc chắn muốn khóa những khóa học được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            courseAction.editStatusCourse(
              {
                listIds: state.checkedList,
                status: 'inactive',
              },
              callback
            )
          );
        },
      });
    }
  }

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListCourses} disabled={!shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/xoa') || state.checkedList.length <= 0}>
        Xóa khóa học
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeListCourses}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/sua') || state.checkedList.length <= 0}
      >
        Xuất bản khóa học
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockListCourses}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/sua') || state.checkedList.length <= 0}
      >
        Khóa khóa học
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.course.list.search,
      ...urlQuery,
      loading: true,
    };
    props.dispatch(courseAction.getCourses(searchParams));
  }, [props.location]);

  const findCate = (id) => {
    const cates = get(props, 'course.listCates.result.data', []);
    const cate = cates.find((ct) => ct.nhom_khoa_hoc_id === id);
    return cate ? cate.ten_nhom : '';
  };
  useEffect(() => {
    props.dispatch(courseAction.getCourseCates());
  }, []);
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const saveFile = () => {
    FileSaver.saveAs(templateFile, 'tempUser.xlsx');
  };

  function processExcel(data) {
    const workbook = XLSX.read(data, { type: 'binary' });
    const firstSheet = workbook.SheetNames[0];
    const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    const onSuccess = () => {
      notification.success({
        message: 'Import biểu mẫu thành công',
      });
    };
    const onError = () => {
      const urlQuery = queryString.parse(props.location.search);
      const searchParams = {
        ...props.course.list.search,
        ...urlQuery,
        loading: false,
      };
      props.dispatch(courseAction.getCourses(searchParams));
    };
    //props.dispatch(postAction.importPosts({ listUsers: excelRows }, onSuccess, onError));
  }

  const customRequest = async ({ file }) => {
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      if (reader.readAsBinaryString) {
        reader.onload = (e) => {
          processExcel(reader.result);
        };
        reader.readAsBinaryString(file);
      }
    } else {
      notification.error({
        message: 'Không đọc được file',
      });
    }
  };

  return (
    <App>
      <Helmet>
        <title>Quản lý khóa học</title>
      </Helmet>
      {props.course.list.loading && <Loading />}
      <Row className="app-main">
        <Col xl={24} className="body-content tabs-layout-admin">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                title="Quản lý khóa học"
                isShowCategories={true}
                isShowStatus={true}
                isShowSearchBox={true}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                categories={get(props, 'course.listCates.result.data', []).map((cate) => ({
                  name: cate.ten_nhom,
                  id: cate.nhom_khoa_hoc_id,
                }))}
                search={state.search}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group" gutter={[8, 8]}>
            <Col xl={12} sm={12} xs={24}></Col>
            <Col xl={12} sm={12} xs={24} className="right-actions">
              <Button shape="round" type="primary" icon={<FolderOutlined />} className="btn-action" onClick={() => history.push('course_category')}>
                Danh mục khóa học
              </Button>
              {shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/them') && (
                <Button shape="round" type="primary" icon={<PlusOutlined />} className="btn-action" onClick={() => createCourse()}>
                  Thêm mới khóa học
                </Button>
              )}
            </Col>
          </Row>
          <Tabs
            defaultActiveKey="1"
            type="card"
            onChange={() => {
              setState({ ...state, checkedList: [], checkAll: false });
            }}
          >
            <TabPane tab="Khóa học đã hoàn thiện" key="1">
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
              <div className="w5d-list">
                <List
                  locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
                  header={
                    <Row>
                      <Col xs={1} xl={1}>
                        {/* <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} /> */}
                      </Col>

                      <Col xs={3}>Ảnh đại diện</Col>
                      <Col xs={8} className="text-left">
                        Tên gọi khóa học
                        <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col xs={4} className="text-left">
                        Giảng viên
                      </Col>
                      <Col xs={4} className="text-left">
                        Nhóm khóa học
                      </Col>
                      <Col xs={4} className="text-left">
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={null}
                  dataSource={get(props, 'course.list.result.data', []).filter((item) => item.trang_thai === 'active')}
                  pagination={{
                    hideOnSinglePage: true,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.khoa_hoc_id} id="listUsers">
                      <Row className="full">
                        <Col xs={1} xl={1}>
                          <Checkbox checked={state.checkedList.includes(item.khoa_hoc_id)} value={item.khoa_hoc_id} onChange={() => onChangeCheck(item.khoa_hoc_id)} />
                        </Col>

                        <Col xs={3}>
                          <Link className="view" to={shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/chi_tiet') ? `/course/detail/${item.khoa_hoc_id}` : '#'}>
                            <W5dImageMinIO image={item.anh_dai_dien} />
                          </Link>
                        </Col>
                        <Col xs={8} className="text-left">
                          <Link className="edit" to={shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/sua') ? `/course/edit/${item.khoa_hoc_id}` : '#'}>
                            {item.tieu_de}
                          </Link>
                        </Col>
                        <Col xs={4} className="text-left">
                          <Link className="view" to={`/user/detail/${item.giang_vien_id}`}>
                             {item.giang_vien.ten_nhan_vien}
                          </Link>
                        </Col>
                        <Col xs={4} className="text-left">
                          {findCate(item.nhom_khoa_hoc_id)}
                        </Col>
                        <Col xs={4} className="text-left">
                          {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                        </Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </TabPane>
            <TabPane tab="Khóa học đang làm" key="2">
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
              <div className="w5d-list">
                <List
                  locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
                  header={
                    <Row>
                      <Col xs={1} xl={1}>
                        {/* <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} /> */}
                      </Col>
                      <Col xs={3}>Ảnh đại diện</Col>
                      <Col xs={8} className="text-left">
                        Tên gọi khóa học
                        <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col xs={4} className="text-left">
                        Giảng viên
                      </Col>
                      <Col xs={4} className="text-left">
                        Nhóm khóa học
                      </Col>
                      <Col xs={4} className="text-left">
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={null}
                  dataSource={get(props, 'course.list.result.data', []).filter((item) => item.trang_thai !== 'active')}
                  pagination={{
                    hideOnSinglePage: true,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.khoa_hoc_id} id="listUsers">
                      <Row className="full">
                        <Col xs={1} xl={1}>
                          <Checkbox checked={state.checkedList.includes(item.khoa_hoc_id)} value={item.khoa_hoc_id} onChange={() => onChangeCheck(item.khoa_hoc_id)} />
                        </Col>
                        <Col xs={3}>
                          <Link className="view" to={shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/chi_tiet') ? `/course/detail/${item.khoa_hoc_id}` : '#'}>
                            <W5dImageMinIO image={item.anh_dai_dien} />
                          </Link>
                        </Col>
                        <Col xs={8} className="text-left">
                          <Link className="edit" to={shouldHaveAccessPermission('khoa_hoc', 'khoa_hoc/sua') ? `/course/edit/${item.khoa_hoc_id}` : '#'}>
                            {item.tieu_de} <EditOutlined />
                          </Link>
                        </Col>
                        <Col xs={4} className="text-left">
                          <Link className="view" to={`/user/detail/${item.giang_vien_id}`}>
                           {item.giang_vien.ten_nhan_vien}
                          </Link>
                        </Col>
                        <Col xs={4} className="text-left">
                          {findCate(item.nhom_khoa_hoc_id)}
                        </Col>
                        <Col xs={4} className="text-left">
                          {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                        </Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    course: state.course,
  };
};
export default connect(mapStateToProps)(Course);
