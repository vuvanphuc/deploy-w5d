// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { get } from 'lodash';
import { Button, Form, Input, InputNumber, Row, Col, Select, Modal, Tabs, Popover, Steps, Result, Table, Collapse, Empty, Radio, notification } from 'antd';
import {
  RightOutlined,
  LeftOutlined,
  SaveOutlined,
  CloseOutlined,
  DeleteOutlined,
  PlusOutlined,
  CheckOutlined,
  FormOutlined,
  LockOutlined,
  SafetyOutlined,
  VideoCameraOutlined,
  FileTextOutlined,
} from '@ant-design/icons';
import AutoLaTeX from 'react-autolatex';
import moment from 'moment';
// import internal libs
import App from 'App';
import { config } from 'config';
import constants from 'constants/global.constants';
import { getLangText } from 'helpers/language.helper';
import { getUserInformation, shouldHaveAccessPermission, recursiveUserCates, recursiveCates } from 'helpers/common.helper';
import * as lessonAction from 'redux/actions/lesson';
import * as courseAction from 'redux/actions/course';
import * as userAction from 'redux/actions/user';
import * as examAction from 'redux/actions/exam';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import Loading from 'components/loading/Loading';
import defaultImage from 'assets/images/default.jpg';
import nottickImg from 'assets/images/ic_tick_disabled.png';
import tickImg from 'assets/images/ic_tick.png';
import TextEditor from 'containers/form/components/widgets/TextEditor/TextEditor';
import './CourseForm.css';

const { Option } = Select;
const { TextArea } = Input;
const { TabPane } = Tabs;
const { Panel } = Collapse;
const { Step } = Steps;

const defaultCourse = {
  tieu_de: '',
  chu_de_id: '',
  hinh_thuc: 'OFFLINE',
  mo_ta: '',
  noi_dung: '',
  muc_tieu: '',
  doi_tuong: '',
  hinh_thuc_dao_tao: '',
  hoc_phi_dao_tao: '',
  xep_lop_dao_tao: '',
  hoat_dong_thay_tro: '',
  hoat_dong_ngoai_khoa: '',
  tai_nguyen: '',
  chung_nhan: '',
  giang_vien_id: '',
  anh_dai_dien: '',
  nhom_khoa_hoc_id: '',
  video_gioi_thieu: '',
  loi_ich: '',
  trang_thai: 'inactive',
  gia_goc: null,
  gia_ban: null,
};

const defaultLesson = {
  tieu_de: 'Bài ',
  khoa_hoc_id: '',
  nhom_bai_hoc_id: '',
  noi_dung: '',
  bai_hoc_mien_phi: 0,
  thoi_luong: 600,
  de_thi_id: '',
  link_video: '',
  video_server: 'youtube',
  trang_thai: 'active',
};

const defaultForm = {
  ...defaultCourse,
  chuong: [],
  uploadLoading: false,
  isChanged: false,
  openMediaLibrary: false,
  insertMedia: {
    open: false,
    editor: null,
  },
};

function CourseForm(props) {
  const [form] = Form.useForm();
  const [lessonForm] = Form.useForm();
  const history = useHistory();
  const courseId = get(props, 'match.params.id', '');
  const course = get(props, 'course.item.result.data', {});
  const [currentStep, setCurrentStep] = useState(0);
  const [currentLesson, setCurrentLesson] = useState(defaultLesson);
  const [lessons, setLessons] = useState([]);
  const [state, setState] = useState(defaultForm);
  const [isOpenEditor, setIsOpenEditor] = useState(false);
  const [newChapter, setNewChapter] = useState({
    visible: false,
    content: '',
    nhom_bai_hoc_id: null,
  });
  const [newLesson, setNewLesson] = useState({
    visible: false,
    ...defaultLesson,
  });

  const next = async () => {
    if (currentStep === 0) {
      try {
        await form.validateFields();
        setCurrentStep(1);
        form.submit();
      } catch (error) {
        console.log('Error', error);
      }
    } else if (currentStep === 1) {
      setCurrentStep(2);
    } else {
      setCurrentStep(2);
    }
  };

  const save = () => {
    if (state.isChanged) {
      form.submit();
    }
  };
  const secondsToMinutes = (time) => {
    let min = Math.floor(time / 60);
    min = min < 10 ? `0${min}` : min;
    let second = Math.floor(time % 60);
    second = second < 10 ? `0${second}` : second;
    return min + ':' + second;
  };

  const isEdit = props.match && props.match.params && props.match.params.id;
  const openMediaLibrary = () => setState((state) => ({ ...state, openMediaLibrary: true }));

  const handleSaveCourse = (values, force = false) => {
    if (props.match.params.id && (state.isChanged || force)) {
      const callback = (res) => {
        if (res.success) {
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
          notification.success({
            message: 'Cập nhật khóa học thành công.',
          });
        }
      };
      let newFormData = {
        ...state,
        ...values,
      };
      newFormData = {
        ...newFormData,
        mo_ta: mysql_real_escape_string(newFormData.mo_ta),
        noi_dung: mysql_real_escape_string(newFormData.noi_dung),
        loi_ich: mysql_real_escape_string(newFormData.loi_ich),
      };
      setState((state) => ({
        ...state,
        ...newFormData,
      }));
      props.dispatch(courseAction.editCourse(newFormData, callback));
    }
  };

  const removeImage = () => setState((state) => ({ ...state, anh_dai_dien: '' }));

  const renderCourseCategories = () => {
    let options = [];
    if (props.course.listCates.result && props.course.listCates.result.data) {
      const cates = recursiveCates(get(props, 'course.listCates.result.data', []), get(props, 'course.listCates.result.data', []), 'nhom_khoa_hoc_id', 'nhom_cha_id');
      options = cates.map((cate) => (
        <Option key={cate.nhom_khoa_hoc_id} value={cate.nhom_khoa_hoc_id}>
          {cate.titleLevel}
        </Option>
      ));
      return (
        <Select
          showSearch={false}
          value={state.nhom_cha_id}
          loading={props.course.listCates.loading}
          onChange={(nhom_khoa_hoc_id) => setState({ ...state, nhom_khoa_hoc_id, isChanged: true })}
          placeholder={getLangText('GLOBAL.SELECT_CATEGORIES')}
        >
          <Option value={0} disabled>Danh mục khóa học</Option>
          {options}
        </Select>
      );
    }
  };

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      anh_dai_dien: file.tep_tin_url,
    }));
  };

  const onInsertImage = (file) => {
    state.insertMedia.editor.insertContent('<img className="image-editor" src="' + `${config.UPLOAD_API_URL}/${file.imgUrl}` + '"/>');
    setState((state) => ({
      ...state,
      isChanged: true,
      insertMedia: {
        open: false,
      },
    }));
  };

  const onResetForm = () => {
    if (props.course.item.result && isEdit) {
      form.setFieldsValue(props.course.item.result.data);
      setState((state) => ({ ...state, ...props.course.item.result.data, isChanged: false }));
    } else {
      form.resetFields();
      setState((state) => ({ ...state, ...defaultForm, isChanged: false }));
    }
  };

  function mysql_real_escape_string(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
      switch (char) {
        case '\0':
          return '\\0';
        case '\x08':
          return '\\b';
        case '\x09':
          return '\\t';
        case '\x1a':
          return '\\z';
        case '\n':
          return '\\n';
        case '\r':
          return '\\r';
        case '"':
        case "'":
        case '\\':
        case '%':
          return '\\' + char; // prepends a backslash to backslash, percent,
        // and double/single quotes
        default:
          return char;
      }
    });
  }

  const removeLesson = (lesson) => {
    if (lesson.bai_hoc_id) {
      const callback = (res) => {
        if (res.success) {
          const chuong = state.chuong.map((chap) => {
            if (chap.nhom_bai_hoc_id === lesson.nhom_bai_hoc_id) {
              return {
                ...chap,
                bai_hoc: chap.bai_hoc.filter((less) => less.bai_hoc_id !== lesson.bai_hoc_id),
              };
            }
            return chap;
          });
          setState({
            ...state,
            chuong,
          });
          notification.success({
            message: 'Xóa bài học thành công.',
          });
        }
      };
      props.dispatch(lessonAction.deleteLesson({ id: lesson.bai_hoc_id }, callback));
    }
  };

  const removeChapter = (nhom_bai_hoc_id) => {
    if (nhom_bai_hoc_id) {
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Xóa chương thành công.',
          });
          const chuong = get(state, 'chuong', []).filter((chapter) => chapter.nhom_bai_hoc_id !== nhom_bai_hoc_id);
          setState({
            ...state,
            chuong,
          });
        }
      };
      props.dispatch(lessonAction.deleteLessonCate({ id: nhom_bai_hoc_id }, callback));
    }
  };

  const saveChapter = () => {
    if (newChapter.nhom_bai_hoc_id) {
      // edit a lesson
      const callback = (res) => {
        if (res.success) {
          if (isEdit) {
            const chuong = state.chuong.map((chap) => (chap.nhom_bai_hoc_id === newChapter.nhom_bai_hoc_id ? { ...chap, ten_nhom: newChapter.content } : chap));
            setState({
              ...state,
              chuong,
            });
            setNewChapter({
              visible: false,
              content: '',
              nhom_bai_hoc_id: null,
            });
            notification.success({
              message: 'Cập nhật chương thành công.',
            });
          }
        }
      };

      props.dispatch(lessonAction.editLessonCate({ ten_nhom: newChapter.content, khoa_hoc_id: courseId, nhom_bai_hoc_id: newChapter.nhom_bai_hoc_id }, callback));
    } else {
      //add a new chapter
      const callback = (res) => {
        if (res.success) {
          setState({
            ...state,
            chuong: [...state.chuong, res.data],
          });
          setNewChapter({
            visible: false,
            content: '',
            nhom_bai_hoc_id: null,
          });
          notification.success({
            message: 'Thêm chương mới thành công.',
          });
        }
      };
      props.dispatch(lessonAction.createLessonCate({ ten_nhom: newChapter.content, khoa_hoc_id: courseId }, callback));
    }
  };

  const saveLesson = () => {
    if (newLesson.bai_hoc_id) {
      // edit a lesson
      const callback = (res) => {
        if (res.success) {
          if (isEdit) {
            const chuong = state.chuong.map((chap) => {
              if (chap.nhom_bai_hoc_id === newLesson.nhom_bai_hoc_id) {
                return {
                  ...chap,
                  bai_hoc: chap.bai_hoc.map((less) => (less.bai_hoc_id === newLesson.bai_hoc_id ? res.data : less)),
                };
              }
              return chap;
            });
            setState({
              ...state,
              chuong,
            });
            setNewLesson({
              ...defaultLesson,
              visible: false,
            });
            notification.success({
              message: 'Cập nhật bài học thành công.',
            });
          }
        }
      };

      props.dispatch(lessonAction.editLesson({ ...newLesson, noi_dung: mysql_real_escape_string(newLesson.noi_dung) }, callback));
    } else {
      //add a new chapter
      const callback = (res) => {
        if (res.success) {
          const chuong = state.chuong.map((chap) => {
            if (chap.nhom_bai_hoc_id === newLesson.nhom_bai_hoc_id) {
              return {
                ...chap,
                bai_hoc: [...chap.bai_hoc, res.data],
              };
            }
            return chap;
          });
          setState({
            ...state,
            chuong,
          });
          setNewLesson({
            visible: false,
            content: '',
            bai_hoc_id: null,
            nhom_bai_hoc_id: null,
          });
          notification.success({
            message: 'Thêm bài học mới thành công.',
          });
        }
      };

      props.dispatch(lessonAction.createLesson({ ...newLesson, khoa_hoc_id: courseId, noi_dung: mysql_real_escape_string(newLesson.noi_dung) }, callback));
    }
  };

  const genExtra = (chapter, index) => (
    <div className="chapter-actions">
      <Button type="primary" title="Thêm bài học mới">
        <PlusOutlined
          onClick={(event) => {
            setNewLesson({
              ...newLesson,
              visible: true,
              content: `Bài `,
              bai_hoc_id: null,
              nhom_bai_hoc_id: chapter.nhom_bai_hoc_id,
            });
            event.stopPropagation();
          }}
        />
      </Button>

      <Button danger title="Xóa chương">
        <DeleteOutlined
          onClick={(event) =>
            Modal.confirm({
              title: 'Thông báo',
              content: 'Bạn có chắc chắn muốn xóa chương và các bài học thuộc chương này không?',
              okText: 'Có',
              cancelText: 'Không',
              onOk: () => {
                removeChapter(chapter.nhom_bai_hoc_id);
                event.stopPropagation();
              },
            })
          }
        />
      </Button>
    </div>
  );

  const renderTeachers = () => {
    let options = [];
    const userCates = get(props, 'user.list.result.data', []);
    if (userCates) {
      const cates = userCates;
      options = cates.map((cate) => (
        <Option key={cate.nhan_vien_id} value={cate.nhan_vien_id}>
          {cate.ten_tai_khoan} - {cate.ten_nhan_vien}
        </Option>
      ));
      return (
        <Select
          showSearch={true}
          value={state.giang_vien_id}
          loading={props.user.list.loading}
          onChange={(giang_vien_id) => setState({ ...state, giang_vien_id, isChanged: true })}
          placeholder="Chọn giảng viên"
        >
          {options}
        </Select>
      );
    }
  };

  const renderExams = () => {
    let options = [];
    const exams = get(props, 'exam.list.result.data', []);
    if (exams) {
      options = exams.map((cate) => (
        <Option key={cate.de_thi_id} value={cate.de_thi_id}>
          {cate.tieu_de}
        </Option>
      ));
      return (
        <Select
          style={{ width: '100%' }}
          showSearch={true}
          value={newLesson.de_thi_id}
          loading={props.exam.list.loading}
          onChange={(de_thi_id) => setNewLesson({ ...newLesson, de_thi_id })}
          placeholder="Chọn đề thi"
        >
          {options}
        </Select>
      );
    }
  };

  useEffect(() => {
    const lessonArr = get(props, 'lesson.list.result.data', []);
    setLessons(lessonArr);
  }, [props.lesson.list.result]);

  useEffect(() => {
    if (isEdit && props.course.item.result) {
      form.setFieldsValue(props.course.item.result.data);
      setState((state) => ({ ...state, ...props.course.item.result.data, isChanged: false }));
    }
    window.onbeforeunload = null;
  }, [props.course.item.result]);

  useEffect(() => {
    props.dispatch(
      userAction.getUsers({
        groupCode: 'TEACHER_GROUP',
      })
    );
    props.dispatch(
      examAction.getExams({
        status: 'active',
      })
    );
    props.dispatch(courseAction.getCourseCates());
    if (isEdit) {
      props.dispatch(
        courseAction.getCourse({
          khoa_hoc_id: courseId,
        })
      );
      props.dispatch(lessonAction.getLessons({ khoa_hoc_id: courseId, status: 'active' }));
    }
  }, []);

  console.log('state', state);

  return (
    <App>
      <Helmet>
        <title>{isEdit ? `Sửa khóa học` : 'Thêm mới khóa học'}</title>
      </Helmet>
      {props.course.item.loading && <Loading />}
      <div className="course-page">
        <Row className="app-main">
          <Col span={24} className="body-content">
            <div className={`w5d-form ${isOpenEditor ? 'open-editor' : ''}`}>
              <Row gutter={25}>
                <Col xs={24}>
                  <Steps current={currentStep}>
                    <Step title="Thông tin chung" />
                    <Step title="Danh sách bài học" />
                    {state.trang_thai === 'active' ? <Step title="Kết quả" /> : <Step title="Hoàn thành" />}
                  </Steps>
                </Col>
              </Row>
              <Tabs activeKey={`step_${currentStep}`}>
                <TabPane tab="Thông tin" key="step_0">
                  <Form layout="vertical" className="CourseForm" onFinish={handleSaveCourse} form={form} initialValues={defaultCourse}>
                    <Row gutter={25}>
                      <Col xl={18} sm={24} xs={24} className="left-content">
                        <div className="border-box">
                          <Row gutter={25}>
                            <Col xl={24} sm={12} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Tên gọi khóa học"
                                name="tieu_de"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Tên gọi khóa học là trường bắt buộc',
                                  },
                                ]}
                              >
                                <Input
                                  placeholder="Tên gọi khóa học"
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Link video giới thiệu"
                                name="video_gioi_thieu"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Video giới thiệu là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <Input
                                  placeholder="Ví dụ: https://www.youtube.com/watch?v=-zgrlgpG5g8"
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Hình thức học"
                                name="hinh_thuc"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Hình thức học là trường bắt buộc',
                                  },
                                ]}
                              >
                                <Radio.Group
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                  options={constants.COURSES_TYPES}
                                  optionType="button"
                                  buttonStyle="solid"
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row className="course-content-tabs">
                            <Col xl={24} sm={24} xs={24}>
                              <Form.Item
                                className="input-col"
                                label="Giới thiệu khóa học"
                                name="mo_ta"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Giới thiệu khóa học là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <TextEditor
                                  value={state.mo_ta}
                                  placeholder="Giới thiệu khóa học là trường bắt buộc"
                                  onChange={(val) => {
                                    if (val !== state.mo_ta) {
                                      setState({ ...state, mo_ta: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Mục tiêu & cam kết khóa học" name="muc_tieu" rules={[]}>
                                <TextEditor
                                  value={state.muc_tieu}
                                  placeholder="Mục tiêu khóa học"
                                  onChange={(val) => {
                                    if (val !== state.muc_tieu) {
                                      setState({ ...state, muc_tieu: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Đối tượng đào tạo" name="doi_tuong" rules={[]}>
                                <TextEditor
                                  value={state.doi_tuong}
                                  placeholder="Đối tượng đào tạo"
                                  onChange={(val) => {
                                    if (val !== state.doi_tuong) {
                                      setState({ ...state, doi_tuong: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Nội dung chi tiết khóa học"
                                name="noi_dung"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Nội dung chi tiết khóa học là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <TextEditor
                                  value={state.mo_ta}
                                  placeholder="Nội dung chi tiết"
                                  onChange={(val) => {
                                    if (val !== state.noi_dung) {
                                      setState({ ...state, noi_dung: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item
                                className="input-col"
                                label="Lợi ích"
                                name="loi_ich"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Lợi ích là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <TextEditor
                                  value={state.mo_ta}
                                  placeholder="Lợi ích khóa học"
                                  onChange={(val) => {
                                    if (val !== state.loi_ich) {
                                      setState({ ...state, loi_ich: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Hình thức đào tạo" name="hinh_thuc_dao_tao" rules={[]}>
                                <TextEditor
                                  value={state.hinh_thuc_dao_tao}
                                  placeholder="Hình thức đào tạo"
                                  onChange={(val) => {
                                    if (val !== state.hinh_thuc_dao_tao) {
                                      setState({ ...state, hinh_thuc_dao_tao: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Chi tiết học phí" name="hoc_phi_dao_tao" rules={[]}>
                                <TextEditor
                                  value={state.hoc_phi_dao_tao}
                                  placeholder="Học phí"
                                  onChange={(val) => {
                                    if (val !== state.hoc_phi_dao_tao) {
                                      setState({ ...state, hoc_phi_dao_tao: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Xếp lớp và thời gian đào tạo" name="xep_lop_dao_tao" rules={[]}>
                                <TextEditor
                                  value={state.xep_lop_dao_tao}
                                  placeholder="Xếp lớp và thời gian đào tạo"
                                  onChange={(val) => {
                                    if (val !== state.xep_lop_dao_tao) {
                                      setState({ ...state, xep_lop_dao_tao: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Các hoạt động tương tác thầy trò" name="hoat_dong_thay_tro" rules={[]}>
                                <TextEditor
                                  value={state.hoat_dong_thay_tro}
                                  placeholder="Các hoạt động tương tác"
                                  onChange={(val) => {
                                    if (val !== state.hoat_dong_thay_tro) {
                                      setState({ ...state, hoat_dong_thay_tro: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Các hoạt động ngoại khóa" name="hoat_dong_ngoai_khoa" rules={[]}>
                                <TextEditor
                                  value={state.hoat_dong_ngoai_khoa}
                                  placeholder="Các hoạt động tương tác"
                                  onChange={(val) => {
                                    if (val !== state.hoat_dong_ngoai_khoa) {
                                      setState({ ...state, hoat_dong_ngoai_khoa: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Tài nguyên hỗ trợ" name="tai_nguyen" rules={[]}>
                                <TextEditor
                                  value={state.tai_nguyen}
                                  placeholder="Tài nguyên hỗ trợ"
                                  onChange={(val) => {
                                    if (val !== state.tai_nguyen) {
                                      setState({ ...state, tai_nguyen: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" label="Chứng nhận sau khóa học" name="chung_nhan" rules={[]}>
                                <TextEditor
                                  value={state.chung_nhan}
                                  placeholder="Chứng nhận sau khóa học"
                                  onChange={(val) => {
                                    if (val !== state.chung_nhan) {
                                      setState({ ...state, chung_nhan: val, isChanged: true });
                                    }
                                  }}
                                  showToolbar={true}
                                  isMinHeight200={true}
                                  isSimple={true}
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col xl={6} sm={24} xs={24} className="right-content">
                        <div className="box">
                          <div className="box-body">
                            <div className="border-box">
                              <h2>
                                <span style={{ color: '#ff4d4f' }}>*</span>Giảng viên
                              </h2>

                              <Form.Item className="input-col" name="giang_vien_id">
                                {renderTeachers()}
                              </Form.Item>
                            </div>
                            <div
                              className="border-box"
                              style={{
                                display: !shouldHaveAccessPermission('de_thi', 'de_thi/sua') ? 'none' : 'block',
                              }}
                            >
                              <h2>
                                <span style={{ color: '#ff4d4f' }}>*</span> {getLangText('GLOBAL.CATEGORIES')}
                              </h2>
                              <Form.Item
                                className="input-col"
                                name="nhom_khoa_hoc_id"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Nhóm khóa học là trường bắt buộc.',
                                  },
                                ]}
                              >
                                {renderCourseCategories()}
                              </Form.Item>
                            </div>
                            <div className="border-box">
                              <h2>
                                <span style={{ color: '#ff4d4f' }}>*</span> Học phí (vnđ)
                              </h2>
                              <Form.Item
                                className="input-col"
                                name="gia_goc"
                                rules={[
                                  {
                                    required: true,
                                    message: 'Giá gốc là trường bắt buộc.',
                                  },
                                ]}
                              >
                                <InputNumber
                                  style={{ width: '100%' }}
                                  placeholder="Giá gốc của khóa học"
                                  min={0}
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                />
                              </Form.Item>
                              <Form.Item className="input-col" name="gia_ban" rules={[]}>
                                <InputNumber
                                  style={{ width: '100%' }}
                                  placeholder="Giá khuyến mại"
                                  min={0}
                                  onChange={() => {
                                    if (!state.isChanged) {
                                      setState((state) => ({
                                        ...state,
                                        isChanged: true,
                                      }));
                                    }
                                  }}
                                />
                              </Form.Item>
                            </div>
                            <W5dMediaBrowser
                              image={state.anh_dai_dien ? `${state.anh_dai_dien}` : defaultImage}
                              field="image"
                              setImageLabel={getLangText('GLOBAL.SET_AVATAR')}
                              removeImageLabel={getLangText('GLOBAL.REMOVE_AVATAR')}
                              title={getLangText('GLOBAL.AVATAR')}
                              openMediaLibrary={() => openMediaLibrary()}
                              removeImage={() => removeImage()}
                            />
                            <div className="border-box hideDesktop">
                              <h2>{getLangText('FORM.PUBLISH')}</h2>
                              <Form.Item className="input-col">
                                <Row>
                                  <Col span="11">
                                    <Button block type="danger" htmlType="reset" onClick={() => onResetForm()}>
                                      {getLangText('FORM.RESET')}
                                    </Button>
                                  </Col>
                                  <Col span="2"></Col>
                                  <Col span="11">
                                    <Button block type="primary" htmlType="submit">
                                      {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                                    </Button>
                                  </Col>
                                </Row>
                              </Form.Item>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </Form>
                </TabPane>
                <TabPane tab="Tạo bài học" key="step_1">
                  <Form layout="vertical" className="LessonForm">
                    <Row gutter={25}>
                      <Col xl={24} sm={24} xs={24} className="left-content">
                        <div className="border-box lesson-content">
                          <div className="header-lesson">
                            <Button
                              type="primary"
                              onClick={() => {
                                setNewChapter({
                                  ...newChapter,
                                  visible: true,
                                  content: `Chương `,
                                  nhom_bai_hoc_id: null,
                                });
                              }}
                            >
                              <PlusOutlined /> Thêm chương mới
                            </Button>
                          </div>
                          {get(state, 'chuong', []).length > 0 && (
                            <Collapse defaultActiveKey={[0]} className="course-collapse">
                              {get(state, 'chuong', []).map((chapter, k) => {
                                return (
                                  <Panel
                                    header={
                                      <span
                                        className="chapter-title"
                                        title={chapter.ten_nhom}
                                        onClick={(event) => {
                                          setNewChapter({
                                            visible: true,
                                            content: chapter.ten_nhom,
                                            nhom_bai_hoc_id: chapter.nhom_bai_hoc_id,
                                          });
                                          event.stopPropagation();
                                        }}
                                      >
                                        {chapter.ten_nhom} <FormOutlined />
                                      </span>
                                    }
                                    key={k}
                                    extra={genExtra(chapter, k)}
                                  >
                                    <Row gutter={[0]} key={k} className="chapter-row">
                                      <Col span={24} className="chapter-col">
                                        {get(chapter, 'bai_hoc', []).length > 0 && (
                                          <Table
                                            bordered
                                            size="small"
                                            columns={[
                                              {
                                                title: 'Tên bài học',
                                                dataIndex: 'tieu_de',
                                                key: 'bai_hoc_id',
                                                width: '50%',
                                                className: 'txt-left',
                                                render: (text) => <a>{text}</a>,
                                              },
                                              {
                                                title: 'Học thử',
                                                dataIndex: 'bai_hoc_mien_phi',
                                                key: 'bai_hoc_id',
                                                width: '10%',
                                                render: (isFree) => (isFree === 1 ? <CheckOutlined className="free-lesson" /> : <LockOutlined className="eco-lesson" />),
                                              },
                                              {
                                                title: 'Thời lượng',
                                                dataIndex: 'thoi_luong',
                                                key: 'bai_hoc_id',
                                                width: '10%',
                                                render: (text) => <a>{secondsToMinutes(text)}</a>,
                                              },
                                              {
                                                title: 'Nội dung',
                                                dataIndex: 'noi_dung',
                                                key: 'bai_hoc_id',
                                                width: '20%',
                                                render: (text, row) => (
                                                  <div className="lesson-content-viewer">
                                                    {row.link_video && (
                                                      <a href={row.link_video} target="_blank">
                                                        <Button type="link" title="Video bài giảng">
                                                          <VideoCameraOutlined />
                                                        </Button>
                                                      </a>
                                                    )}

                                                    {row.noi_dung && (
                                                      <Popover content={<AutoLaTeX>{row.noi_dung}</AutoLaTeX>} title="Bài giảng văn bản" placement="left">
                                                        <Button type="link" title="Bài giảng văn bản">
                                                          <FileTextOutlined />
                                                        </Button>
                                                      </Popover>
                                                    )}

                                                    {row.de_thi_id && (
                                                      <Button type="link" title="Đề thi" onClick={() => window.open(`/exams?keyword=${row.de_thi_id}`, '_blank')}>
                                                        <SafetyOutlined />
                                                      </Button>
                                                    )}
                                                  </div>
                                                ),
                                              },
                                              {
                                                title: '',
                                                dataIndex: '',
                                                key: 'x',
                                                width: '10%',
                                                render: (text, row) => (
                                                  <div className="lesson-actions">
                                                    <Button type="link" title="Cập nhật bài học">
                                                      <FormOutlined
                                                        onClick={(event) => {
                                                          setNewLesson({
                                                            ...row,
                                                            visible: true,
                                                          });
                                                          event.stopPropagation();
                                                        }}
                                                      />
                                                    </Button>

                                                    <Button type="link" danger title="Xóa bài học">
                                                      <DeleteOutlined
                                                        onClick={(event) => {
                                                          Modal.confirm({
                                                            title: 'Thông báo',
                                                            content: 'Bạn có chắc chắn muốn xóa bài học này không?',
                                                            okText: 'Có',
                                                            cancelText: 'Không',
                                                            onOk: () => {
                                                              removeLesson(row);
                                                              event.stopPropagation();
                                                            },
                                                          });
                                                        }}
                                                      />
                                                    </Button>
                                                  </div>
                                                ),
                                              },
                                            ]}
                                            pagination={false}
                                            dataSource={get(chapter, 'bai_hoc', []).map((less, index) => ({ ...less, key: index }))}
                                          />
                                        )}
                                        {get(chapter, 'bai_hoc', []).length <= 0 && <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={<span>Chưa có bài học nào.</span>} />}
                                      </Col>
                                    </Row>
                                  </Panel>
                                );
                              })}
                            </Collapse>
                          )}

                          {get(state, 'chuong', []).length <= 0 && (
                            <div className="empty-chapters">
                              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={<span>Chưa có chương nào.</span>} />
                            </div>
                          )}
                        </div>
                      </Col>
                    </Row>
                  </Form>
                </TabPane>
                <TabPane tab="Tạo câu" key="step_2">
                  <Row gutter={25}>
                    <Col xl={24} sm={24} xs={24} className="left-content">
                      {state.trang_thai === 'inactive' && (
                        <Result
                          status="success"
                          title="Khóa học đã sẵn sàng để xuất bản."
                          subTitle="Hãy kiểm tra kỹ trước khi tiến hành xuất bản khóa học."
                          extra={[
                            <Button onClick={() => setCurrentStep(0)}> Kiểm tra lại</Button>,
                            <Button
                              type="primary"
                              onClick={() =>
                                Modal.confirm({
                                  title: 'Xuất bản khóa học',
                                  content: 'Bạn có chắc chắn muốn xuất bản khóa học này không?',
                                  okText: 'Có',
                                  cancelText: 'Không',
                                  onOk: () => {
                                    handleSaveCourse({ ...state, trang_thai: 'active' }, true);
                                    setCurrentStep(0);
                                  },
                                })
                              }
                            >
                              Xuất bản
                            </Button>,
                          ]}
                        />
                      )}

                      {state.trang_thai === 'active' && (
                        <Result
                          status="warning"
                          title="Khóa học đã xuất bản thành công."
                          subTitle="Nếu bạn muốn chỉnh sửa khóa học, hãy tạm dừng xuất bản."
                          extra={[
                            <Button
                              type="primary"
                              onClick={() =>
                                Modal.confirm({
                                  title: 'Tạm dừng xuất bản',
                                  content: 'Bạn có chắc chắn muốn tạm dừng xuất bản khóa học này không?',
                                  okText: 'Có',
                                  cancelText: 'Không',
                                  onOk: () => {
                                    handleSaveCourse({ ...state, trang_thai: 'inactive' }, true);
                                    setCurrentStep(0);
                                  },
                                })
                              }
                            >
                              Dừng xuất bản
                            </Button>,
                          ]}
                        />
                      )}
                    </Col>
                  </Row>
                </TabPane>
              </Tabs>
            </div>
          </Col>
        </Row>
        <footer className="footer-course">
          <div className="footer-action">
            {currentStep === 0 && (
              <>
                <Button type="dash" onClick={() => save()} size="large" disabled={!state.isChanged}>
                  Cập nhật <SaveOutlined />
                </Button>
                <Button type="primary" onClick={() => next()} size="large">
                  {state.trang_thai === 'active' ? 'Danh sách bài học' : 'Thêm bài học'} <RightOutlined />
                </Button>
              </>
            )}
            {currentStep === 1 && (
              <>
                <Button type="dash" onClick={() => setCurrentStep(0)} size="large">
                  <LeftOutlined /> Quay lại
                </Button>
                <Button type="primary" onClick={() => next()} size="large">
                  Tiếp theo <RightOutlined />
                </Button>
              </>
            )}
            {currentStep === 2 && (
              <>
                <Button type="dash" onClick={() => setCurrentStep(1)} size="large">
                  <LeftOutlined /> Quay lại
                </Button>
              </>
            )}
          </div>
        </footer>
      </div>
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={state.openMediaLibrary}
        onCancel={() =>
          setState((state) => ({
            ...state,
            openMediaLibrary: false,
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>
      <Modal
        wrapClassName="w5d-modal-large"
        visible={state.insertMedia.open}
        onCancel={() =>
          setState((state) => ({
            ...state,
            insertMedia: {
              ...state.insertMedia,
              open: false,
            },
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary onSelectImage={(file) => onInsertImage(file)} />
      </Modal>

      <Modal
        title={newChapter.nhom_bai_hoc_id ? 'Cập nhật chương' : 'Thêm chương mới'}
        visible={newChapter.visible}
        onCancel={() =>
          setNewChapter({
            visible: false,
            content: '',
            nhom_bai_hoc_id: null,
          })
        }
        onOk={() => saveChapter()}
        okText={newChapter.nhom_bai_hoc_id ? 'Cập nhật' : 'Thêm mới'}
        cancelText="Hủy bỏ"
        width={480}
      >
        <Form.Item className="input-col" label="" rules={[]}>
          <TextArea
            value={newChapter.content}
            maxLength={200}
            placeholder="Tên chương"
            onChange={(e) => {
              setNewChapter({ ...newChapter, content: e.target.value });
            }}
            showCount
            allowClear
            autoSize
          />
        </Form.Item>
      </Modal>

      <Modal
        title={newLesson.bai_hoc_id ? 'Cập nhật bài học' : 'Thêm bài học mới'}
        visible={newLesson.visible}
        onCancel={() =>
          setNewLesson({
            ...defaultLesson,
            visible: false,
          })
        }
        onOk={() => saveLesson()}
        okText={newLesson.bai_hoc_id ? 'Cập nhật' : 'Thêm mới'}
        cancelText="Hủy bỏ"
        width={880}
      >
        <Row gutter={[12, 12]}>
          <Col xs={24}>
            <TextArea
              value={newLesson.tieu_de}
              maxLength={200}
              placeholder="Tên chương"
              onChange={(e) => {
                setNewLesson({ ...newLesson, tieu_de: e.target.value });
              }}
              showCount
              allowClear
              autoSize
            />
          </Col>
          <Col xs={24}>
            <Radio.Group
              options={[
                { label: 'Không học thử', value: 0 },
                { label: 'Học thử', value: 1 },
              ]}
              optionType="button"
              buttonStyle="solid"
              value={newLesson.bai_hoc_mien_phi}
              onChange={(e) => {
                setNewLesson({ ...newLesson, bai_hoc_mien_phi: e.target.value });
              }}
            />
          </Col>
        </Row>
        <Tabs defaultActiveKey="1">
          <TabPane tab="Video bài giảng" key="1">
            <Input.Group compact>
              <Input
                style={{ width: '60%' }}
                value={newLesson.link_video}
                placeholder="Link video bài giảng"
                onChange={(e) => {
                  setNewLesson({ ...newLesson, link_video: e.target.value });
                }}
              />
              <Select
                value={newLesson.video_server}
                style={{ width: '20%' }}
                onChange={(val) => {
                  setNewLesson({ ...newLesson, video_server: val });
                }}
              >
                <Option value="youtube">Youtube</Option>
                <Option value="private-server">Server</Option>
              </Select>
              <InputNumber
                min={0}
                max={36000}
                value={newLesson.thoi_luong}
                placeholder="Thời gian video(giây)"
                style={{ width: '20%' }}
                onChange={(val) => {
                  setNewLesson({ ...newLesson, thoi_luong: val });
                }}
              />
            </Input.Group>
          </TabPane>
          <TabPane tab="Văn bản" key="2">
            <TextEditor
              value={newLesson.noi_dung}
              placeholder="Nội dung bài giảng"
              onChange={(val) => {
                setNewLesson({ ...newLesson, noi_dung: val });
              }}
              showToolbar={true}
              isMinHeight200={true}
              isSimple={true}
            />
          </TabPane>
          <TabPane tab="Bài kiểm tra" key="3">
            {renderExams()}
          </TabPane>
        </Tabs>
      </Modal>
    </App>
  );
}
const mapStateToProps = (state) => {
  return {
    course: state.course,
    lesson: state.lesson,
    user: state.user,
    exam: state.exam,
  };
};
export default connect(mapStateToProps)(CourseForm);
