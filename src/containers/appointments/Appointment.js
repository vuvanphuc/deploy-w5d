// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import moment from 'moment';
import { Link } from 'react-router-dom';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { Row, Col, Form, Input, List, Button, Checkbox, notification, Space, Dropdown, Menu, Modal, DatePicker, TimePicker, Select, Tag } from 'antd';
import { DeleteOutlined, DownOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { get } from 'lodash';
// import internal libs
import * as appointmentAction from 'redux/actions/appointment';
import * as userAction from 'redux/actions/user';
import { config } from 'config';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import App from 'App';
import AppFilter from 'components/AppFilter';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import W5dMediaLibrary from 'components/W5dMediaLibrary';

const dateFormat = 'DD-MM-YYYY';
const { TextArea } = Input;
const { Option } = Select;
const { confirm } = Modal;

function Appointment(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const defaultForm = {
    lich_hen_id: '',
    bac_si_id: '',
    benh_nhan_id: '',
    benh_vien: '',
    ghi_chu: '',
    khoa: '',
    ly_do_huy: '',
    bat_dau: null,
    ket_thuc: null,
    ket_thuc_du_kien: null,
    trang_thai: '',
  };
  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    openMediaLibrary: false,
    body: defaultForm,
    search: {
      ...props.appointment.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });
  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const changeMutilUrlParams = (fields) => {
    //const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const handleChangeDate = (field, value) => setState({ ...state, body: { ...state.body, [field]: value }, isChanged: true });

  const searchAppointments = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.appointment.list.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(appointmentAction.getAppointments(searchParams));
    form.setFieldsValue(state.body);
  };

  useEffect(() => {
    searchAppointments();
    props.dispatch(userAction.getUsers({ loading: false }));
  }, [props.location]);

  useEffect(() => {
    window.onbeforeunload = null;
  }, [props]);

  const openMediaLibrary = () => setState((state) => ({ ...state, openMediaLibrary: true }));

  // list functions
  const allOptions = get(props, 'appointment.list.result.data', []).map((item) => item.lich_hen_id);

  const onCheckAllChange = (e) => {
    setState((state) => ({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    }));
  };

  const onChangeCheck = (lich_hen_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(lich_hen_id) ? state.checkedList.filter((it) => it !== lich_hen_id) : state.checkedList.concat([lich_hen_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      body: {
        ...state.body,
        anh_dai_dien: file.tep_tin_url,
      },
    }));
  };

  const onEditCategory = (item) => {
    setState((state) => ({
      ...state,
      isEdit: true,
      body: item,
    }));
    form.setFieldsValue(item);
  };

  const onDeleteCategory = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Khóa lịch hẹn thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa lịch hẹn thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        searchAppointments();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(appointmentAction.deleteAppointment({ id }, callback));
      },
    });
  };

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };

  function onDeleteAppointments() {
    const callback = (res) => {
      if (res.success) {
        searchAppointments();
        setState((state) => ({
          ...state,
          checkedList: [],
        }));
        notification.success({
          message: 'Xóa lịch hẹn thành công',
        });
      } else {
        notification.error({
          message: res.error,
        });
      }
    };
    confirm({
      title: 'Không thể xóa lịch hẹn nếu có lịch hẹn thuộc nhóm đó, bạn chắc chắn muốn xóa?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(appointmentAction.deleteAppointments({ listIds: state.checkedList }, callback));
      },
    });
  }

  function StatusTag(status) {
    if (status === 'active') {
      return <Tag color="#87d068">{constants.APPOINMENT_STATUS[1].title}</Tag>;
    } else if (status === 'pending') {
      return <Tag color="orange">{constants.APPOINMENT_STATUS[0].title}</Tag>;
    } else if (status === 'completed') {
      return <Tag color="#2FA2C1">{constants.APPOINMENT_STATUS[2].title}</Tag>;
    } else {
      return <Tag color="red">{constants.APPOINMENT_STATUS[3].title}</Tag>;
    }
  }
  const removeImage = () => setState((state) => ({ ...state, body: { ...state.body, anh_dai_dien: '' } }));

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
  };

  const shoudlDisableFrom = () => {
    if (state.isEdit && shouldHaveAccessPermission('lich_hen', 'lich_hen/sua')) return false;
    if (!state.isEdit && shouldHaveAccessPermission('lich_hen', 'lich_hen/them')) return false;
    return true;
  };

  const handleSubmit = (values) => {
    if (state.isEdit) {
      // edit a category
      const callback = (result) => {
        searchAppointments();
        changeUrlParams('edit', '');
        onResetForm();
        notification.success({
          message: 'Cập nhật lịch hẹn thành công',
        });
        setState((state) => ({
          ...state,
          body: result.data,
        }));
      };
      props.dispatch(appointmentAction.editAppointment({ ...state.body, ...values }, callback));
    } else {
      //add a new category
      const callback = () => {
        searchAppointments();
        notification.success({
          message: 'Tạo mới lịch hẹn thành công',
        });
        setState((state) => ({
          ...state,
          isChanged: false,
        }));
      };
      props.dispatch(appointmentAction.createAppointment({ ...state.body, ...values }, callback));
    }
    setState((state) => ({
      ...state,
      body: defaultForm,
      isEdit: false,
      isChanged: false,
    }));
    form.resetFields();
  };

  const onResetForm = () => {
    setState((state) => ({
      ...state,
      body: defaultForm,
      isEdit: false,
    }));
    form.resetFields();
  };

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };

  const renderPatients = () => {
    const cates = get(props, 'user.list.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.nhan_vien_id}>
          {cate.ten_nhan_vien}
        </Option>
      );
    });
    return (
      <Select
        disabled={shoudlDisableFrom()}
        onChange={(value) => {
          if (!state.isChanged) {
            setState((state) => ({
              ...state,
              isChanged: true,
            }));
          }
        }}
        placeholder="Chọn bác sĩ"
      >
        {catesOpts}
      </Select>
    );
  };
  const renderDoctors = () => {
    const cates = get(props, 'user.list.result.data', []);
    const catesOpts = cates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.nhan_vien_id}>
          {cate.ten_nhan_vien}
        </Option>
      );
    });
    return (
      <Select
        disabled={shoudlDisableFrom()}
        onChange={(value) => {
          if (!state.isChanged) {
            setState((state) => ({
              ...state,
              isChanged: true,
            }));
          }
        }}
        placeholder="Chọn bác sĩ"
      >
        {catesOpts}
      </Select>
    );
  };

  const renderStatus = () => {
    const options = constants.APPOINMENT_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select
        disabled={shoudlDisableFrom()}
        defaultValue={state.trang_thai}
        value={state.trang_thai}
        onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })}
        placeholder="Trạng thái"
      >
        {options}
      </Select>
    );
  };

  const menu = (
    <Menu>
      {shouldHaveAccessPermission('lich_hen', 'lich_hen/xoa') && (
        <Menu.Item key="1" disabled={state.checkedList.length === 0 ? true : false} icon={<DeleteOutlined />} onClick={onDeleteAppointments}>
          Xóa danh mục lịch hẹn
        </Menu.Item>
      )}
    </Menu>
  );
  console.log('state', state);
  return (
    <App>
      <Helmet>
        <title>{getLangText('FORM_BUILDER.FORM_BUILDERS_CATEGORIES')}</title>
      </Helmet>
      {props.appointment.list.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={8} sm={8} xs={24} className="cate-form-block">
              <h2>Thêm mới</h2>
              <Form layout="vertical" className="category-appointment" form={form} onFinish={handleSubmit}>
                <Form.Item
                  className="input-col"
                  label="Bác sĩ"
                  name="bac_si_id"
                  rules={[
                    {
                      required: true,
                      message: 'Bác sĩ là trường bắt buộc',
                    },
                  ]}
                >
                  {renderDoctors()}
                </Form.Item>
                <Form.Item
                  className="input-col"
                  label="Bệnh nhân"
                  name="benh_nhan_id"
                  rules={[
                    {
                      required: true,
                      message: 'Bệnh nhân là trường bắt buộc',
                    },
                  ]}
                >
                  {renderPatients()}
                </Form.Item>
                <Form.Item
                  className="input-col"
                  label="Thời gian bắt đầu"
                  rules={[
                    {
                      required: true,
                      message: 'Thời gian bắt đầu là trường bắt buộc',
                    },
                  ]}
                >
                  <DatePicker
                    disabledDate={(current) => current && current < moment()}
                    value={state.body.bat_dau ? moment(state.body.bat_dau) : null}
                    showToday={false}
                    style={{ width: '100%' }}
                    onChange={(value) => {
                      handleChangeDate('bat_dau', value);
                    }}
                    format="DD/MM/yyyy HH:mm"
                    max
                    placeholder="Bắt đầu"
                  />
                </Form.Item>
                <Form.Item
                  className="input-col"
                  label="Thời gian kết thúc dự kiến"
                  rules={[
                    {
                      required: true,
                      message: 'Thời gian kết thúc dự kiến là trường bắt buộc',
                    },
                  ]}
                >
                  <DatePicker
                    disabledDate={(current) => current && current < moment()}
                    value={state.body.ket_thuc_du_kien ? moment(state.body.ket_thuc_du_kien) : null}
                    showToday={true}
                    style={{ width: '100%' }}
                    onChange={(value) => handleChangeDate('ket_thuc_du_kien', value)}
                    format="DD/MM/yyyy HH:mm"
                    max
                    placeholder="Kết thúc dự kiến"
                  />
                </Form.Item>
                <Form.Item
                  className="input-col"
                  label="Thời gian kết thúc"
                  rules={[
                    {
                      required: true,
                      message: 'Thời gian kết thúc là trường bắt buộc',
                    },
                  ]}
                >
                  <DatePicker
                    disabledDate={(current) => current && current < moment()}
                    value={state.body.ket_thuc ? moment(state.body.ket_thuc) : null}
                    showToday={true}
                    style={{ width: '100%' }}
                    onChange={(value) => handleChangeDate('ket_thuc', value)}
                    format="DD/MM/yyyy HH:mm"
                    max
                    placeholder="Kết thúc"
                  />
                </Form.Item>
                <Form.Item
                  label="Bệnh viện"
                  className="input-col"
                  name="benh_vien"
                  rules={[
                    {
                      required: true,
                      message: 'Bệnh viện là trường bắt buộc.',
                    },
                  ]}
                >
                  <Input
                    disabled={shoudlDisableFrom()}
                    placeholder="Bệnh viện"
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item label="Khoa" className="input-col" name="khoa" rules={[{}]}>
                  <Input
                    disabled={shoudlDisableFrom()}
                    placeholder="Khoa"
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item label="Ghi chú" className="input-col" name="ghi_chu" rules={[{}]}>
                  <TextArea
                    disabled={shoudlDisableFrom()}
                    placeholder=""
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item
                  label="Trạng thái"
                  className="input-col"
                  name="trang_thai"
                  rules={[
                    {
                      required: true,
                      message: 'Trạng thái là trường bắt buộc.',
                    },
                  ]}
                >
                  {renderStatus()}
                </Form.Item>
                <Form.Item className="input-col" label="Lý do hủy" name="ly_do_huy" rules={[]}>
                  <TextArea
                    disabled={shoudlDisableFrom()}
                    placeholder=""
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item className="button-col">
                  <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableFrom()}>
                    {state.isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.ADD_NEW')}
                  </Button>
                  {state.isEdit && (
                    <Button shape="round" type="danger" onClick={() => onResetForm()}>
                      {getLangText('FORM.CANCEL')}
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
            <Col span={1}></Col>
            <Col xl={15} sm={15} xs={24}>
              <div className="w5d-list">
                <AppFilter
                  isShowCategories={false}
                  isShowStatus={false}
                  isShowSearchBox={true}
                  isShowDatePicker={true}
                  isRangeDatePicker={true}
                  title="Danh sách"
                  search={state.search}
                  onDateChange={(dates) => onDateChange(dates)}
                  onFilterChange={(field, value) => onFilterChange(field, value)}
                />
                <Row className="select-action-group-cate">
                  <Space wrap>
                    <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                      <Button>
                        Chọn hành động
                        <DownOutlined />
                      </Button>
                    </Dropdown>
                  </Space>
                </Row>
                <List
                  locale={{
                    emptyText: getLangText('GLOBAL.NO_ITEMS'),
                  }}
                  header={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={6} className="text-left">
                        Bác sĩ
                      </Col>
                      <Col span={6} className="text-left">
                        Bệnh nhân
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.UPDATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_sua'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={6} className="text-left">
                        Bác sĩ
                      </Col>
                      <Col span={6} className="text-left">
                        Bệnh nhân
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.UPDATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_sua'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  dataSource={get(props, 'appointment.list.result.data', [])}
                  pagination={{
                    hideOnSinglePage: false,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.lich_hen_id}>
                      <Row className="full">
                        <Col span={1}>
                          <Checkbox checked={state.checkedList.includes(item.lich_hen_id)} value={item.lich_hen_id} onChange={() => onChangeCheck(item.lich_hen_id)} />
                        </Col>
                        <Col span={6} className="text-left">
                          <Link className="edit" to={`#`}>
                            {item.bac_si.ho_ten}
                          </Link>

                          <div className="actions">
                            {shouldHaveAccessPermission('lich_hen', 'lich_hen/sua') && (
                              <Button className="edit act" type="link" onClick={() => onEditCategory(item)}>
                                {getLangText('GLOBAL.EDIT')}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('lich_hen', 'lich_hen/xoa') && (
                              <Button className="delete act" type="link" onClick={() => onDeleteCategory(item.lich_hen_id, item.trang_thai)}>
                                {item.trang_thai === 'active' ? 'Khóa' : 'Xóa'}
                              </Button>
                            )}
                          </div>
                        </Col>

                        <Col span={6} className="text-left">
                          {item.benh_nhan.ho_ten}
                        </Col>
                        <Col span={3} className="text-left">
                          {StatusTag(item.trang_thai)}
                        </Col>
                        <Col span={4}>{moment(item.ngay_tao).isValid() ? moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT) : '-'}</Col>
                        <Col span={4}>{moment(item.ngay_sua).isValid() ? moment(item.ngay_sua).utc(0).format(config.DATE_FORMAT) : '-'}</Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={state.openMediaLibrary}
        onCancel={() =>
          setState((state) => ({
            ...state,
            openMediaLibrary: false,
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    appointment: state.appointment,
    config: state.config,
    user: state.user,
  };
};

export default connect(mapStateToProps)(Appointment);
