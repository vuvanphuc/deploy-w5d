// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory, useParams } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Button, Form, Tabs, Row, Col, Modal, notification, Select } from 'antd';
import { UnorderedListOutlined, PlusOutlined, RollbackOutlined } from '@ant-design/icons';

// import internal libs
import App from 'App';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import * as moduleAction from 'redux/actions/moduleApp';
import constants from 'constants/global.constants';
import Loading from 'components/loading/Loading';
import FormBuilderRender from 'containers/form/FormBuilderRender';
import ModuleViewList from 'containers/module/components/ModuleViewList';
import ModuleFormModal from 'containers/module/components/ModuleFormModal';

const { TabPane } = Tabs;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_MODULE;
const { Option } = Select;
function ModuleForm(props) {
  const params = useParams();
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const defaultForm = {
    trang_thai: true,
    isChanged: false,
  };
  const [state, setState] = useState(defaultForm);
  const [module, setModule] = useState({});
  const [modules, setModules] = useState([]);
  const [body, setBody] = useState({});
  const [isSub, setIsSub] = useState(false);
  const [moduleLevel, setModuleLevel] = useState();
  const [tab, setTab] = useState(urlQuery.tab || 'LIST_ITEMS');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [moduleModalData, setModuleModalData] = useState();

  const isEdit = props.match && props.match.params && props.match.params.module_action === 'edit';

  const showModal = (subModule) => {
    setIsModalVisible(true);
    setModuleModalData(subModule);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleSubmit = (values) => {
    let formatValues = {};
    for (const [key, value] of Object.entries(values)) {
      const newVal = Array.isArray(value) ? JSON.stringify(value) : value;
      formatValues = { ...formatValues, [key]: newVal };
    }
    if (props.match.params.id) {
      // edit a data
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Cập nhật dữ liệu thành công.',
          });
          form.resetFields();
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
          if (urlQuery.returnLink) {
            history.push(atob(urlQuery.returnLink));
          } else {
            props.dispatch(moduleAction.getModule({ id: props.match.params.id, module_id: module.module_id }));
          }
        }
      };
      const newFormData = {
        data: {
          ...body,
          ...formatValues,
          urlParams: params,
          trang_thai: 'active',
        },
        id: props.match.params.id,
        module,
      };
      props.dispatch(moduleAction.editModule(newFormData, callback));
    } else {
      //add a new data
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Thêm mới dữ liệu thành công',
          });
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
          const keyField = module.module_fields.find((item) => item.key).column;
          if (urlQuery.returnLink) {
            history.push(atob(urlQuery.returnLink));
          } else {
            if (isSub) {
              history.push(`/module/sub/${props.match.params.module_parent}/${props.match.params.module_parent_data_id}/${props.match.params.module_id}/edit/${res.data[keyField]}?tab=FORM_ITEM`);
            } else {
              history.push(`/module/${props.match.params.module_id}/edit/${res.data[keyField]}`);
            }
          }
        }
      };

      let defaultForm = {};
      if (urlQuery.defaultForm) {
        defaultForm = JSON.parse(atob(urlQuery.defaultForm));
      }
      const newFormData = {
        data: {
          ...body,
          ...formatValues,
          ...defaultForm,
          urlParams: params,
          trang_thai: 'active',
        },
        module,
      };

      props.dispatch(moduleAction.createModule(newFormData, callback));
    }
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onResetForm = () => {
    if (props.module.item.result && isEdit && props.match.params.id) {
      form.setFieldsValue(props.module.item.result.data);
      setBody(props.module.item.result.data);
    } else {
      form.resetFields();
    }
  };

  const onChangeModule = (module_id) => {
    const [parentModule, currentModule] = module_id.split('@_@');
    const dataId = isSub ? get(props, 'match.params.module_parent_data_id') : get(props, 'match.params.id');
    if (parentModule === currentModule) {
      history.push(`/module/${parentModule}/edit/${dataId}`);
    } else {
      const newModule = modules.find((modu) => modu.module_id === currentModule);
      if (newModule.module_show_view_page) {
        history.push(`/module/sub/${parentModule}/${dataId}/${currentModule}/edit?tab=LIST_ITEMS`);
      } else {
        history.push(`/module/sub/${parentModule}/${dataId}/${currentModule}/edit?tab=FORM_ITEM`);
      }
    }
  };

  const renderSubModuleLinks = (modu) => {
    if (!isSub || !props.match.params.id) return null;
    const parentModule = modules.filter((mode) => mode.module_id === modu.module_parent);
    const subModules = modules.filter((mode) => mode.module_parent === modu.module_id);
    const subModuleItems = subModules.map((subModule, idx) => {
      return (
        <Button key={idx} shape="round" size="small" type="primary" onClick={() => showModal(subModule)}>
          {subModule.module_name}
        </Button>
      );
    });
    return (
      <div className="module-grant-btns">
        {subModuleItems}
        {moduleModalData && (
          <Modal title={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} wrapClassName="module-modal" centered={true} maskClosable={false} header={null} footer={null}>
            <ModuleFormModal moduleModalData={moduleModalData} handleSubmit={handleSubmit} location={props.location} match={props.match} isModalVisible={isModalVisible} />
          </Modal>
        )}
      </div>
    );
  };

  const renderContentModule = (parentModuleId) => {
    if (!parentModuleId) return null;
    if (isEdit) {
      let parentModule = modules.find((mode) => mode.module_id === parentModuleId);
      if (parentModule && parentModule.module_parent !== 'root') {
        parentModule = modules.find((mode) => mode.module_id === parentModule.module_parent);
      }
      const subModules = modules.filter((mode) => mode.module_parent === parentModule.module_id);
      const subModuleItems = [parentModule, ...subModules].map((mode) => {
        return (
          <TabPane
            className="module-main-tab"
            tab={
              <span>
                <UnorderedListOutlined />
                {mode.module_name}
              </span>
            }
            key={`${parentModule.module_id}@_@${mode.module_id}`}
          >
            {!isSub && (
              <FormBuilderRender
                formId={module.module_form}
                table={module.module_table}
                formConfig={module.module_table ? props.form.item[module.module_table] : {}}
                formObj={form}
                body={body}
                isEdit={!!isEdit}
              />
            )}

            {isSub && !mode.module_show_view_page && (
              <FormBuilderRender
                formId={module.module_form}
                table={module.module_table}
                formConfig={module.module_table ? props.form.item[module.module_table] : {}}
                formObj={form}
                body={body}
                isEdit={!!isEdit}
              />
            )}

            {isSub && mode.module_show_view_page && (
              <div className="sub-module-tabs">
                {renderSubModuleLinks(mode)}
                <Tabs
                  defaultActiveKey={tab}
                  tabPosition="top"
                  onChange={(tab) => {
                    changeMutilUrlParams({ tab });
                    setTab(tab);
                    const dataId = get(props, 'match.params.module_parent_data_id');
                    history.push(`/module/sub/${parentModule.module_id}/${dataId}/${mode.module_id}/edit?tab=${tab}`);
                    if (tab === 'FORM_ITEM') {
                      window.location.reload();
                      form.resetFields();
                    }
                  }}
                >
                  {shouldHaveAccessPermission(props.match.params.module_id, `${props.match.params.module_id}/xem`) && (
                    <TabPane tab="Danh sách" key="LIST_ITEMS">
                      <ModuleViewList location={props.location} match={props.match} moduleInfo={mode} parentModule={parentModule} parentDataId={props.match.params.module_parent_data_id} />
                    </TabPane>
                  )}
                  {(params.id || (!params.id && shouldHaveAccessPermission(props.match.params.module_id, `${props.match.params.module_id}/them`))) && (
                    <TabPane tab={`${params.id ? 'Chỉnh sửa' : 'Thêm mới'}`} key="FORM_ITEM">
                      <FormBuilderRender
                        formId={module.module_form}
                        table={module.module_table}
                        formConfig={module.module_table ? props.form.item[module.module_table] : {}}
                        formObj={form}
                        body={body}
                        isEdit={!!isEdit}
                      />
                    </TabPane>
                  )}
                </Tabs>
              </div>
            )}
          </TabPane>
        );
      });
      return <>{subModuleItems}</>;
    }
    return (
      <FormBuilderRender
        formId={module.module_form}
        table={module.module_table}
        formConfig={module.module_table ? props.form.item[module.module_table] : {}}
        formObj={form}
        body={body}
        isEdit={!!isEdit}
      />
    );
  };

  const renderModuleTabs = () => {
    const parentModule = module.module_parent === 'root' ? module.module_id : module.module_parent;
    const activeKey = `${parentModule}@_@${module.module_id}`;
    return (
      <Tabs type="card" onChange={(mode) => onChangeModule(mode)} activeKey={activeKey} className="main-mudule-app">
        {renderContentModule(parentModule)}
      </Tabs>
    );
  };
  const renderStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select value={state.trang_thai} onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })} placeholder={getLangText('GLOBAL.SELECT_STATUS')}>
        {options}
      </Select>
    );
  };
  useEffect(() => {
    if (isEdit && props.module.item.result) {
      const values = get(props, 'module.item.result.data', {});
      setBody(values);
      form.setFieldsValue(values);
    }
    window.onbeforeunload = null;
  }, [props.module.item.result]);

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      const currentModuleId = get(props, 'match.params.module_id');
      const dataId = get(props, 'match.params.id');
      setModules(data.modules);
      if (currentModuleId) {
        const currentModule = data.modules.find((module) => module.module_id === currentModuleId);
        setModule(currentModule);
        setIsSub(currentModule.module_parent !== 'root');
        if (dataId) {
          props.dispatch(
            moduleAction.getModule({
              id: dataId,
              module_id: currentModuleId,
            })
          );
        }
      }
    }
    form.resetFields();
  }, [props.config.list.result.data, props.match.params]);

  useEffect(() => {
    const params = props.match.params;
    if (params.module_grant && params.module_grant_data_id && params.module_parent && params.module_parent_data_id) {
      setModuleLevel('grant');
    } else if (params.module_parent && params.module_parent_data_id) {
      setModuleLevel('sub');
    } else {
      setModuleLevel('root');
    }
  }, [props.match.params]);

  return (
    <App>
      <Helmet>
        <title>{isEdit ? `Chỉnh sửa` : `Thêm mới`}</title>
      </Helmet>
      {props.module.item.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <div className="w5d-form">
            <Form className="" onFinish={handleSubmit} form={form} layout={get(props, `form.item.${module.module_table}.kieu_giao_dien`, 'vertical')}>
              <Row gutter={25}>
                <Col xl={18} sm={18} xs={24}>
                  <h2 className="header-form-title">
                    {urlQuery.returnLink && (
                      <Button
                        type="link"
                        icon={<RollbackOutlined />}
                        onClick={() => {
                          history.push(atob(urlQuery.returnLink));
                        }}
                      >
                        quay lại
                      </Button>
                    )}
                    {isEdit ? 'Chỉnh sửa' : 'Thêm mới'}
                  </h2>
                </Col>
                <Col xl={6} sm={6} xs={0}>
                  {/* {isEdit &&
                    shouldHaveAccessPermission(
                      props.match.params.module_id,
                      `${props.match.params.module_id}/them`
                    ) && (
                      <Button
                        href={
                          isSub
                            ? `/module/sub/${props.match.params.module_parent}/${props.match.params.module_parent_data_id}/${props.match.params.module_id}/edit?tab=FORM_ITEM`
                            : `/module/${module.module_id}/add`
                        }
                        block
                        shape="round"
                        type="primary"
                        icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />}
                        className="btn-create-todo"
                      >
                        Thêm mới
                      </Button>
                    )}
                  {!isEdit && (
                    <Button
                      href={isSub ? `/module/${module.module_id}/add` : `/module/${module.module_id}`}
                      block
                      shape="round"
                      type="primary"
                      icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />}
                      className="btn-create-todo"
                    >
                      {isSub ? 'Thêm mới' : ' Danh sách'}
                    </Button>
                  )} */}
                </Col>
              </Row>
              <Row gutter={25}>
                <Col xl={18} sm={18} xs={18} className="left-content">
                  <div className="border-box module-form-tab">{renderModuleTabs()}</div>
                </Col>
                <Col xl={6} sm={6} xs={6} className="right-content">
                  <div className="box">
                    {urlQuery.tab !== 'LIST_ITEMS' && (
                      <div className="box-body">
                        <div className="border-box hideMobile">
                          <h2>{getLangText('FORM.PUBLISH')}</h2>
                          <Form.Item className="input-col">
                            {((shouldHaveAccessPermission(props.match.params.module_id, `${props.match.params.module_id}/them`) && isEdit && isSub) ||
                              (shouldHaveAccessPermission(props.match.params.module_id, `${props.match.params.module_id}/them`) && !isEdit && !isSub) ||
                              (shouldHaveAccessPermission(props.match.params.module_id, `${props.match.params.module_id}/sua`) && isEdit)) && (
                              <Row>
                                <Col span="11">
                                  <Button block type="danger" onClick={() => onResetForm()}>
                                    {getLangText('FORM.RESET')}
                                  </Button>
                                </Col>
                                <Col span="2"></Col>
                                <Col span="11">
                                  {/* */}
                                  <Button block type="primary" htmlType="submit">
                                    {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                                  </Button>
                                </Col>
                              </Row>
                            )}
                          </Form.Item>
                        </div>
                      </div>
                    )}
                  </div>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    module: state.module,
    config: state.config,
    form: state.form,
  };
};

export default connect(mapStateToProps)(ModuleForm);
