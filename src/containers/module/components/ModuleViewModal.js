// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Row, Col, List, Button, Modal, notification, Checkbox, Menu, Dropdown, Space } from 'antd';
import { DownOutlined, DeleteOutlined, QuestionCircleOutlined } from '@ant-design/icons';

// import internal libs
import * as moduleAction from 'redux/actions/moduleApp';
import { getLangText } from 'helpers/language.helper';
import { buildUrl } from 'helpers/common.helper';
import { config } from 'config';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';

import { renderDataFormatted } from 'helpers/module.helper';

const { confirm } = Modal;

function ModuleViewList(props) {
  const { moduleInfo } = props;
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  //chứa danh sách id
  let allOptions = [];

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: props.module.list.search,
  });

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
    props.dispatch(
      moduleAction.getModules({
        ...state.search,
        ...fields,
        module_id: props.moduleInfo.module_id,
        filter_key: props.moduleInfo.module_related_key,
        filter_value: props.parentDataId,
        isFullData: true,
      })
    );
  };

  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(id) ? state.checkedList.filter((it) => it !== id) : state.checkedList.concat([id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onDelete = (id) => {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Xóa dữ liệu thành công.',
        });
        searchData(module.module_id);
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa không?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(moduleAction.deleteModule({ id, module_id: module.module_id }, callback));
      },
    });
  };

  function deleteListModules() {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Xóa nhiều bản ghi thành công.',
        });
        searchData(module.module_id);
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các bản ghi được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(moduleAction.deleteModules({ listIds: state.checkedList, module_id: module.module_id }, callback));
      },
    });
  }

  const renderHeaderFooterColumns = () => {
    if (module && module.module_fields) {
      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} className="text-left">
            {item.title}
            {item.isSortable && <Sorting urlQuery={urlQuery} field={item.column} onSortChange={(fields) => onSortChange(fields)} />}
          </Col>
        );
      });
      return (
        <Row>
          <Col span={1} className="text-left">
            <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
          </Col>
          {columns}
        </Row>
      );
    }
  };

  const renderRows = (data) => {
    if (module && module.module_fields) {
      //lấy id của từng bản ghi, đẩy vào allOptions
      allOptions.push(data[module.module_fields.find((field) => field.key === true).column]);

      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} className="text-left">
            {renderDataFormatted(data, item)}
            {item.key && (
              <div className="actions">
                <Button
                  type="link"
                  onClick={() => {
                    props.onEdit({ ...data, keyField: data[item.column] });
                  }}
                  className="edit act"
                >
                  {getLangText('GLOBAL.EDIT')}
                </Button>
                <Button className="delete act" type="link" onClick={() => onDelete(data[item.column])}>
                  {getLangText('GLOBAL.DELETE')}
                </Button>
              </div>
            )}
          </Col>
        );
      });
      return (
        <Row className="full">
          <Col span={1} className="text-left">
            <Checkbox
              checked={state.checkedList.includes(data[module.module_fields.find((field) => field.key === true).column])}
              value={data[module.module_fields.find((field) => field.key === true).column]}
              onChange={() => onChangeCheck(data[module.module_fields.find((field) => field.key === true).column])}
            />
          </Col>
          {columns}
        </Row>
      );
    }
  };

  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListModules} disabled={state.checkedList.length > 0 ? false : true}>
        Xóa bản ghi
      </Menu.Item>
    </Menu>
  );

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const [module, setModule] = useState({});

  const searchData = (module_id) => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.module.list.search,
      ...urlQuery,
      module_id,
      filter_key: props.moduleInfo.module_related_key,
      filter_value: props.match.params.id,
      isFullData: true,
    };
    delete searchParams.page;
    props.dispatch(moduleAction.getModules(searchParams));
  };

  useEffect(() => {
    if (moduleInfo && moduleInfo.module_id) {
      searchData(moduleInfo.module_id);
      setModule(moduleInfo);
    }
  }, [props.moduleInfo, props.tab]);

  return (
    <Col>
      <Row className="select-action-group">
        <Space wrap>
          <Dropdown overlay={menu} trigger="click">
            <Button>
              Chọn hành động
              <DownOutlined />
            </Button>
          </Dropdown>
        </Space>
        <br />
      </Row>
      <div className="w5d-list">
        {props.module.list.loading && <Loading />}
        <List
          locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
          header={renderHeaderFooterColumns()}
          dataSource={get(props, 'module.list.result.data', [])}
          pagination={{
            hideOnSinglePage: false,
            responsive: true,
            showLessItems: true,
            pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
            pageSize: pageSize,
            onChange: (page, pageSize) => {
              changeMutilUrlParams({ pageSize, page });
              setPageSize(pageSize);
              setPage(page);
            },
            current: Number(page),
            showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
            showSizeChanger: true,
          }}
          renderItem={(item, idx) => <List.Item key={idx}>{renderRows(item)}</List.Item>}
        />
      </div>
    </Col>
  );
}

const mapStateToProps = (state) => {
  return {
    module: state.module,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ModuleViewList);
