// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Button, Form, Tabs, Row, Col, notification } from 'antd';

// import internal libs
import { getLangText } from 'helpers/language.helper';
import * as moduleAction from 'redux/actions/moduleApp';

import FormBuilderRender from 'containers/form/FormBuilderRender';
import ModuleViewModal from 'containers/module/components/ModuleViewModal';

const { TabPane } = Tabs;

function ModuleFormModal(props) {
  const { moduleModalData } = props;
  const [tab, setTab] = useState('LIST_ITEMS');
  const [isEdit, setIsEdit] = useState(false);
  const [form] = Form.useForm();
  const [body, setBody] = useState({});

  const onEdit = (values) => {
    setIsEdit(true);
    setBody(values);
    setTab('FORM_ITEM');
    form.setFieldsValue(values);
  };

  const onResetForm = () => {
    if (isEdit) {
      form.setFieldsValue(body);
    } else {
      form.resetFields();
    }
  };

  const handleSubmit = (values) => {
    if (isEdit) {
      // edit a data
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Cập nhật dữ liệu thành công.',
          });
          form.resetFields();
          setIsEdit(false);
          setBody({});
          setTab('LIST_ITEMS');
          form.setFieldsValue({});
          form.resetFields();
        }
      };
      const newFormData = {
        data: {
          ...body,
          ...values,
        },
        id: body.keyField,
        module: moduleModalData,
      };
      props.dispatch(moduleAction.editModule(newFormData, callback));
    } else {
      //add a new data
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Thêm mới dữ liệu thành công',
          });
          form.resetFields();
          setIsEdit(false);
          setBody({});
          setTab('LIST_ITEMS');
          form.setFieldsValue({});
        }
      };
      const newFormData = {
        data: {
          ...body,
          ...values,
        },
        module: moduleModalData,
      };
      props.dispatch(moduleAction.createModule(newFormData, callback));
    }
  };

  useEffect(() => {
    setTab('LIST_ITEMS');
  }, [props.isModalVisible]);
  return (
    <div className="form-modal-module">
      <Form onFinish={handleSubmit} form={form} layout={get(props, `form.item.${moduleModalData.module_table}.kieu_giao_dien`, 'vertical').trim()}>
        {tab === 'FORM_ITEM' && (
          <Row>
            <Col xs={24} className="module-submit-btn">
              <div className="border-submit">
                <Form.Item className="input-col">
                  <Row>
                    <Col span="24">
                      <Button type="danger" onClick={onResetForm}>
                        {getLangText('FORM.RESET')}
                      </Button>
                      <Button type="primary" htmlType="submit">
                        {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                      </Button>
                    </Col>
                  </Row>
                </Form.Item>
              </div>
            </Col>
          </Row>
        )}

        {!moduleModalData.module_show_view_page && (
          <FormBuilderRender
            formId={moduleModalData.module_form}
            table={moduleModalData.module_table}
            formConfig={moduleModalData.module_table ? props.form.item[moduleModalData.module_table] : {}}
            formObj={form}
            body={body}
            isEdit={isEdit}
          />
        )}
        {moduleModalData.module_show_view_page && (
          <Tabs
            activeKey={tab}
            tabPosition="left"
            onChange={(tab) => {
              setTab(tab);
              setIsEdit(false);
              form.resetFields();
              form.setFieldsValue({});
              setBody({});
            }}
          >
            <TabPane tab="Danh sách" key="LIST_ITEMS">
              <ModuleViewModal location={props.location} match={props.match} moduleInfo={moduleModalData} onEdit={onEdit} tab={tab} />
            </TabPane>
            <TabPane tab={isEdit ? 'Chỉnh sửa' : 'Thêm mới'} key="FORM_ITEM">
              <FormBuilderRender
                formId={moduleModalData.module_form}
                table={moduleModalData.module_table}
                formConfig={moduleModalData.module_table ? props.form.item[moduleModalData.module_table] : {}}
                formObj={form}
                body={body}
                isEdit={isEdit}
              />
            </TabPane>
          </Tabs>
        )}
      </Form>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    module: state.module,
    config: state.config,
    form: state.form,
  };
};

export default connect(mapStateToProps)(ModuleFormModal);
