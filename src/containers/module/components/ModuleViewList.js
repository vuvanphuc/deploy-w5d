// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { get } from 'lodash';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, List, Button, Modal, notification, Checkbox, Menu, Dropdown, Space } from 'antd';
import { DownOutlined, DeleteOutlined, QuestionCircleOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons';

// import internal libs
import * as moduleAction from 'redux/actions/moduleApp';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import { config } from 'config';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import { renderDataFormatted } from 'helpers/module.helper';
import ExportToWordData from 'components/ExportToWordData';
import constants from 'constants/global.constants';
import W5dStatusTag from '..//../../components/status/W5dStatusTag';

const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_MODULE;
const { confirm } = Modal;

function ModuleViewList(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  //chứa danh sách id
  let allOptions = [];

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: props.module.list.search,
  });

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
    props.dispatch(
      moduleAction.getModules({
        ...state.search,
        ...fields,
        module_id: props.moduleInfo.module_id,
        filter_key: props.moduleInfo.module_related_key,
        filter_value: props.parentDataId,
      })
    );
  };

  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(id) ? state.checkedList.filter((it) => it !== id) : state.checkedList.concat([id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const deleteModule = (id, trang_thai) => {
    if (module.module_show_filter_status && trang_thai === 'inactive') {
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Xóa dữ liệu thành công.',
          });
          searchData(module.module_id);
        }
      };
      confirm({
        title: `Bạn chắc chắn muốn xóa bản ghi ${id} không?`,
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(moduleAction.deleteModule({ id, module_id: module.module_id }, callback));
        },
      });
    } else {
      if (module.module_show_filter_status) {
        blockModule(id, 'Khóa');
      } else {
        blockModule(id, 'Xóa');
      }
    }
  };

  function deleteModules() {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Xóa nhiều bản ghi thành công.',
        });
        searchData(module.module_id);
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các bản ghi được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(moduleAction.deleteModules({ listIds: state.checkedList, module_id: module.module_id }, callback));
      },
    });
  }

  const blockModule = (item, actionTitle) => {
    const callback = () => {
      searchData(module.module_id);
      notification.success({
        message: `${actionTitle} bản ghi '${item}' thành công!`,
      });
    };
    confirm({
      title: `Bạn chắc chắn muốn ${actionTitle} bản ghi '${item}' không?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(moduleAction.editModulesStatus({ data: { listIds: [item], status: 'inactive' }, module_id: module.module_id }, callback));
      },
    });
  };

  //Active theo list => active
  function activeModules(e) {
    const callback = (res) => {
      if (res.success) {
        searchData(module.module_id);
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Kích hoạt nhiều bản ghi thành công!',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các bản ghi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(moduleAction.editModulesStatus({ data: { listIds: state.checkedList, status: 'active' }, module_id: module.module_id }, callback));
        },
      });
    }
  }

  //Khoa theo list => inactive
  function blockModules(e) {
    const callback = (res) => {
      if (res.success) {
        searchData(module.module_id);
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Khóa nhiều bản ghi thành công!',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn khóa các bản ghi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(moduleAction.editModulesStatus({ data: { listIds: state.checkedList, status: 'inactive' }, module_id: module.module_id }, callback));
        },
      });
    }
  }

  const renderHeaderFooterColumns = () => {
    if (module && module.module_fields) {
      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} className="text-left">
            {item.title}
            {item.isSortable && <Sorting urlQuery={urlQuery} field={item.column} onSortChange={(fields) => onSortChange(fields)} />}
          </Col>
        );
      });
      return (
        <Row>
          <Col span={1} className="text-left">
            <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
          </Col>
          {columns}
          {module.module_show_filter_status && (
            <Col xs={3} xl={4}>
              {getLangText('GLOBAL.STATUS')}
              <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
            </Col>
          )}
        </Row>
      );
    }
  };

  const gotoDetail = (data) => {
    if (module && module.module_fields && (shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) || shouldHaveAccessPermission(module.module_id, `${module.module_id}/chi_tiet`))) {
      const keyField = module.module_fields.find((item) => item.key);
      history.push(`/module/sub/${props.parentModule.module_id}/${props.parentDataId}/${props.moduleInfo.module_id}/edit/${data[keyField.column]}?tab=FORM_ITEM`);
    }
  };

  const renderRows = (data) => {
    if (module && module.module_fields) {
      //lấy id của từng bản ghi, đẩy vào allOptions
      allOptions.push(data[module.module_fields.find((field) => field.key === true).column]);

      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} className="text-left">
            {item.key && (shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) || shouldHaveAccessPermission(module.module_id, `${module.module_id}/chi_tiet`)) ? (
              <Button type="link" href={`/module/sub/${props.parentModule.module_id}/${props.parentDataId}/${props.moduleInfo.module_id}/edit/${data[item.column]}?tab=FORM_ITEM`}>
                {renderDataFormatted(data, item)}
              </Button>
            ) : (
              renderDataFormatted(data, item)
            )}
            {item.key && (
              <div className="actions">
                {shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) && (
                  <Button
                    type="link"
                    href={`/module/sub/${props.parentModule.module_id}/${props.parentDataId}/${props.moduleInfo.module_id}/edit/${data[item.column]}?tab=FORM_ITEM`}
                    className="edit act"
                  >
                    {getLangText('GLOBAL.EDIT')}
                  </Button>
                )}
                {shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) && (
                  <Button className="delete act" type="link" onClick={() => deleteModule(data[item.column], data.trang_thai)}>
                    {module.module_show_filter_status && data.trang_thai === 'active' ? getLangText('GLOBAL.BLOCK') : getLangText('GLOBAL.DELETE')}
                  </Button>
                )}
                {module.module_export_data && <ExportToWordData moduleExport={module} dataId={data[item.column]} />}
              </div>
            )}
          </Col>
        );
      });
      return (
        <Row className="full" onDoubleClick={() => gotoDetail(data)}>
          <Col span={1} className="text-left">
            <Checkbox
              checked={state.checkedList.includes(data[module.module_fields.find((field) => field.key === true).column])}
              value={data[module.module_fields.find((field) => field.key === true).column]}
              onChange={() => onChangeCheck(data[module.module_fields.find((field) => field.key === true).column])}
            />
          </Col>
          {columns}
          {module.module_show_filter_status && (
            <Col xs={3} xl={4}>
              <W5dStatusTag status={data.trang_thai} />
            </Col>
          )}
        </Row>
      );
    }
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const [module, setModule] = useState({});

  const searchData = (module_id) => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.module.list.search,
      ...urlQuery,
      module_id,
      filter_key: props.moduleInfo.module_related_key,
      filter_value: props.parentDataId,
    };
    delete searchParams.page;
    props.dispatch(moduleAction.getModules(searchParams));
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      const currentModuleId = get(props, 'match.params.module_id');
      if (currentModuleId) {
        const currentModule = data.modules.find((module) => module.module_id === currentModuleId);
        setModule(currentModule);
        searchData(currentModule.module_id);
      }
    }
  }, [props.config.list.result.data, props.match.params.module_id]);

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteModules} disabled={!shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) || state.checkedList.length <= 0}>
        Xóa bản ghi
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeModules}
        hidden={!module.module_show_filter_status}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) || state.checkedList.length <= 0}
      >
        Kích hoạt bản ghi
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockModules}
        hidden={!module.module_show_filter_status}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) || state.checkedList.length <= 0}
      >
        Khóa bản ghi
      </Menu.Item>
    </Menu>
  );

  return (
    <Col>
      <Row className="select-action-group">
        <Space wrap>
          <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
            <Button>
              Chọn hành động
              <DownOutlined />
            </Button>
          </Dropdown>
        </Space>
        <br />
      </Row>
      <div className="w5d-list">
        {(props.module.list.loading || props.module.export.loading) && <Loading />}
        <List
          locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
          header={renderHeaderFooterColumns()}
          dataSource={get(props, 'module.list.result.data', [])}
          pagination={{
            hideOnSinglePage: false,
            responsive: true,
            showLessItems: true,
            pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
            pageSize: pageSize,
            onChange: (page, pageSize) => {
              changeMutilUrlParams({ pageSize, page });
              setPageSize(pageSize);
              setPage(page);
            },
            current: Number(page),
            showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
            showSizeChanger: true,
          }}
          renderItem={(item, idx) => <List.Item key={idx}>{renderRows(item)}</List.Item>}
        />
      </div>
    </Col>
  );
}

const mapStateToProps = (state) => {
  return {
    module: state.module,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ModuleViewList);
