// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { get } from 'lodash';
import { useHistory, useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Row, Col, List, Button, notification } from 'antd';
import { DeploymentUnitOutlined } from '@ant-design/icons';

// import internal libs
import { validateRuleset, deductionResultFilter } from './validations/rule-validation';
import * as deductionAction from 'redux/actions/deduction';
import * as rulesetAction from 'redux/actions/ruleset';
import { getLangText } from 'helpers/language.helper';
import { buildUrl } from 'helpers/common.helper';
import { config } from 'config';
import App from 'App';
import AppFilter from 'components/AppFilter';
import Loading from 'components/loading/Loading';
import ProgressBar from 'components/progress/Progress';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';

import { renderDataFormatted } from 'helpers/module.helper';

const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_DEDUCE;

function Deduction(props) {
  const params = useParams();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const [state, setState] = useState({
    search: props.deduction.deductioninfos.search,
  });
  const [ruleset, setRuleset] = useState({});
  const [ruleAction, setRuleAction] = useState({ loading: false });
  const [percent, setPercent] = useState(0);

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
    props.dispatch(deductionAction.getDeductionInfos({ ...state.search, [field]: value, module_id: module.module_id }));
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
    props.dispatch(deductionAction.getDeductionInfos({ ...state.search, ...fields, module_id: module.module_id }));
  };

  const renderHeaderFooterColumns = () => {
    if (module && module.module_fields) {
      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} hidden={!item.isShow} className="text-left">
            {item.title}
            {item.isSortable && <Sorting urlQuery={urlQuery} field={item.column} onSortChange={(fields) => onSortChange(fields)} />}
          </Col>
        );
      });
      return <Row>{columns}</Row>;
    }
  };

  const gotoDetail = (data) => {
    if (module && module.module_fields) {
      const keyField = module.module_fields.find((item) => item.key);
      history.push(`/deduction/${module.module_id}/detail/${data[keyField.column]}`);
    }
  };

  const renderRows = (data) => {
    if (module && module.module_fields) {
      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col span={item.width} key={idx} hidden={!item.isShow} className="text-left">
            {item.key ? <Link to={`/deduction/${module.module_id}/detail/${data[item.column]}`}>{renderDataFormatted(data, item)}</Link> : renderDataFormatted(data, item)}
          </Col>
        );
      });
      return (
        <Row className="full" onDoubleClick={() => gotoDetail(data)}>
          {columns}
        </Row>
      );
    }
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const [module, setModule] = useState({});

  const searchData = (module_id) => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.deduction.deductioninfos.search,
      ...urlQuery,
      module_id,
    };
    delete searchParams.page;
    props.dispatch(deductionAction.getDeductionInfos(searchParams));
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      const currentModuleId = params.module_id;
      if (currentModuleId) {
        const currentModule = data.modules.find((module) => module.module_id === currentModuleId);
        setModule(currentModule);
        searchData(currentModule.module_id);
        if (currentModule.isDeduceItems) {
          props.dispatch(rulesetAction.getRuleset({ id: currentModule.module_ruleset }));
        }
      }
    }
  }, [props.config.list.result.data, params]);

  useEffect(() => {
    if (module.isDeduceItems) {
      if (props.ruleset.item.result.data) {
        const data = get(props.ruleset.item.result, 'data', []);
        const ruleInfo = JSON.parse(data.dulieu);
        setRuleset({
          ...ruleInfo,
          tapluatid: data.tapluatid,
        });
      }
      setRuleAction({ loading: false });
    }
  }, [props.ruleset.item.result.data]);

  const getOutcomes = (items) => {
    const filter = deductionResultFilter(items);
    const columnList = module.module_fields.filter((col) => col.isDeductionResult === true);
    const resultList = columnList.map((col) => {
      return {
        [col['column']]: filter
          .filter((item) => item.type.toLowerCase() === col['column'].toLowerCase())
          .map((it) => get(it, 'params.result', it.type))
          .join('; '),
      };
    });
    let resultObj = {};
    for (const item of resultList) {
      resultObj = { ...resultObj, ...item };
    }
    return resultObj;
  };

  const updateDeductionResult = (outcomes, id) => {
    const ketqua = getOutcomes(outcomes);
    const newFormData = {
      data: ketqua,
      id,
      module,
    };
    props.dispatch(deductionAction.editDeductionResult(newFormData));
  };

  const validateList = (data, attributes, decisions, progress) => {
    for (const [idx, item] of data.entries()) {
      console.log(idx, item);
      let facts = {};
      for (const [key, value] of Object.entries(item)) {
        const attrProps = attributes.find((attr) => attr.name === key);
        if (attrProps === undefined) continue;
        if (attrProps.type === 'number') {
          facts[key] = value === null ? null : Number(value);
        } else if (value && value.indexOf(',') > -1) {
          facts[key] = value.split(',');
        } else {
          facts[key] = value;
        }
      }
      validateRuleset(facts, decisions)
        .then((outcomes) => {
          updateDeductionResult(outcomes, item[module.module_fields.find((it) => it.key === true).column]);
          progress(idx);
        })
        .catch((e) => {
          progress(idx);
        });
    }
  };

  const deduceItems = () => {
    //suy diễn
    const { decisions, attributes } = ruleset;
    setRuleAction({ loading: true });
    setPercent(0);

    const data = get(props, 'deduction.deductioninfos.result.data', []);

    const finish = (idx) => {
      setPercent(Math.round(((idx + 1) * 100) / data.length));
      if (idx + 1 == data.length) {
        setRuleAction({ loading: false });
        notification.success({
          message: `Hoàn thành suy diễn ${idx + 1} bản ghi dữ liệu.`,
        });
        searchData(module.module_id);
      }
    };

    validateList(data, attributes, decisions, finish);
  };

  console.log('loading', ruleAction.loading);
  console.log('percent', percent);

  return (
    <App>
      <Helmet>
        <title>{module.module_name}</title>
      </Helmet>
      {props.deduction.deductioninfos.loading && <Loading />}
      {ruleAction.loading && <ProgressBar percent={percent} />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Col span={24} className="body-content">
            <Row>
              <Col xl={20} sm={24} xs={24}>
                <AppFilter
                  title={module.module_name}
                  search={state.search}
                  isShowStatus={module.module_show_filter_status}
                  isShowSearchBox={module.module_show_search_box}
                  onFilterChange={(field, value) => onFilterChange(field, value)}
                />
              </Col>
              <Col xl={4} sm={8} xs={24} className="right-actions">
                <Button shape="round" type="primary" icon={<DeploymentUnitOutlined />} hidden={!module.isDeduceItems} className="btn-create-todo" onClick={() => deduceItems()}>
                  Suy diễn
                </Button>
              </Col>
            </Row>
            <div className="w5d-list">
              <List
                locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
                header={renderHeaderFooterColumns()}
                footer={renderHeaderFooterColumns()}
                dataSource={get(props, 'deduction.deductioninfos.result.data', [])}
                pagination={{
                  hideOnSinglePage: false,
                  responsive: true,
                  showLessItems: true,
                  pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                  pageSize: pageSize,
                  onChange: (page, pageSize) => {
                    changeMutilUrlParams({ pageSize, page });
                    setPageSize(pageSize);
                    setPage(page);
                  },
                  current: Number(page),
                  showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                  showSizeChanger: true,
                }}
                renderItem={(item, idx) => <List.Item key={idx}>{renderRows(item)}</List.Item>}
              />
            </div>
          </Col>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    deduction: state.deduction,
    config: state.config,
    ruleset: state.ruleset,
  };
};

export default connect(mapStateToProps)(Deduction);
