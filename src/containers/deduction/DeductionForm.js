// import external libs
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Button, Form, Row, Col, notification } from 'antd';

// import internal libs
import App from 'App';
import { getLangText } from 'helpers/language.helper';
import * as deductionAction from 'redux/actions/deduction';
import * as rulesetAction from 'redux/actions/ruleset';
import { validateRuleset, deductionResultFilter } from './validations/rule-validation';

import Loading from 'components/loading/Loading';
import FormBuilderRender from 'containers/form/FormBuilderRender';
import constants from 'constants/global.constants';

const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_DEDUCE;

function DeductionForm(props) {
  const params = useParams();
  const deduceOne = params.id ? false : true;
  const [deductionForm] = Form.useForm();
  const [infoForm] = Form.useForm();

  const [module, setModule] = useState({});
  const [infoBody, setInfoBody] = useState({});
  const [deductionBody, setDeductionBody] = useState({});
  const [ruleset, setRuleset] = useState({});
  const [ruleResults, setRuleResults] = useState({});
  const [action, setAction] = useState({});

  // xử lý khi submit form suy diễn
  const handleSubmit = (values) => {
    if (action.action === 'edit') {
      //lưu thông tin suy diễn vào CSDL
      let formatValues = {};
      for (const [key, value] of Object.entries(values)) {
        const newVal = Array.isArray(value) ? JSON.stringify(value) : value;
        formatValues = { ...formatValues, [key]: newVal };
      }

      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Cập nhật dữ liệu suy diễn thành công.',
          });
          deductionForm.resetFields();
          props.dispatch(deductionAction.getDeduction({ id: params.id, module_id: module.module_id }));
        }
      };
      const newFormData = {
        data: {
          ...deductionBody,
          ...formatValues,
          urlParams: params,
        },
        id: params.id,
        module,
      };
      props.dispatch(deductionAction.editDeduction(newFormData, callback));
    } else if (action.action === 'deduce') {
      //suy diễn
      const { decisions, attributes } = ruleset;

      setRuleResults({ loading: true });

      let facts = {};
      for (const [key, value] of Object.entries(values)) {
        const attrProps = attributes.find((attr) => attr.name === key);
        if (attrProps === undefined) continue;
        if (attrProps.type === 'number') {
          facts[key] = value === null ? null : Number(value);
        } else if (value && value.indexOf(',') > -1) {
          facts[key] = value.split(',');
        } else {
          facts[key] = value;
        }
      }
      validateRuleset(facts, decisions)
        .then((outcomes) => {
          setRuleResults({
            loading: false,
            outcomes,
            result: true,
            error: false,
            errorMessage: '',
          });
        })
        .catch((e) => {
          setRuleResults({
            loading: false,
            error: true,
            errorMessage: e.error,
            result: true,
          });
        });
    }
  };

  const onResetForm = () => {
    const values = deductionForm.getFieldsValue();
    let newValues = {};
    for (const [key] of Object.entries(values)) {
      newValues[key] = null;
    }
    deductionForm.setFieldsValue(newValues);
  };

  const renderDeductionInfo = () => {
    if (!module.module_id) return null;
    return (
      <FormBuilderRender formId={module.module_form} table={module.module_table} formConfig={module.module_table ? props.form.list[module.module_table] : {}} formObj={infoForm} body={infoBody} />
    );
  };

  const renderDeduction = () => {
    if (!module.module_id) return null;
    return (
      <FormBuilderRender
        formId={module.module_rule_form}
        table={module.module_update_table}
        formConfig={module.module_update_table ? props.form.list[module.module_update_table] : {}}
        formObj={deductionForm}
        body={deductionBody}
      />
    );
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    //lấy config của tập module suy diễn
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      const currentModuleId = params.module_id; //module_id của form đang gọi
      const dataId = params.id; //id của bản ghi
      const currentModule = data.modules.find((module) => module.module_id === currentModuleId);
      setModule(currentModule);
      if (!deduceOne) {
        props.dispatch(deductionAction.getDeductionInfo({ id: dataId, module_id: currentModuleId }));
        props.dispatch(deductionAction.getDeduction({ id: dataId, module_id: currentModuleId }));
      }
      props.dispatch(rulesetAction.getRuleset({ id: currentModule.module_ruleset }));
    }
    if (!deduceOne) {
      infoForm.resetFields();
    }
    deductionForm.resetFields();
  }, [props.config.list.result.data, params]);

  useEffect(() => {
    if (!deduceOne) {
      if (props.deduction.deductioninfo.result) {
        const values = get(props, 'deduction.deductioninfo.result.data', {});
        setInfoBody(values);
        infoForm.setFieldsValue(values);
      }
      window.onbeforeunload = null;
    }
  }, [props.deduction.deductioninfo.result]);

  useEffect(() => {
    if (!deduceOne) {
      if (props.deduction.deduction.result) {
        const values = get(props, 'deduction.deduction.result.data', {});
        setDeductionBody(values);
        deductionForm.setFieldsValue(values);
      }
      window.onbeforeunload = null;
    }
  }, [props.deduction.deduction.result]);

  useEffect(() => {
    if (props.ruleset.item.result.data) {
      const data = get(props.ruleset.item.result, 'data', []);
      const ruleInfo = JSON.parse(data.dulieu);
      setRuleset({
        ...ruleInfo,
        tapluatid: data.tapluatid,
      });
    }
    setRuleResults({
      message: '',
      loading: false,
      outcomes: [],
      error: false,
    });
  }, [props.ruleset.item.result.data]);

  const ViewOutcomes = ({ items }) => {
    const filter = deductionResultFilter(items);
    return filter.map((item, ind) => (
      <div key={ind} className="view-body">
        {item.type}
        <b>{get(item, 'params.result', item.type)}</b>
      </div>
    ));
  };

  const attributeItems = () => {
    const { loading, outcomes, result, error, errorMessage } = ruleResults;
    let message;
    if (result) {
      if (error) {
        message = <div className="form-error">Có vấn đề khi xử lý luật, nguyên nhân là {errorMessage}</div>;
      } else if (outcomes && outcomes.length < 1) {
        message = <div>Không tìm thấy kết quả</div>;
      } else if (outcomes && outcomes.length > 0) {
        message = <ViewOutcomes items={outcomes} />;
      } else {
        message = undefined;
      }
    }
    return (
      <Row>
        {loading && <Loading />}
        <div className="view-attribute">{!loading && message}</div>
      </Row>
    );
  };

  return (
    <App>
      <Helmet>
        <title>{module.module_name}</title>
      </Helmet>
      {props.deduction.deduction.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          {!deduceOne && (
            <div className="w5d-form">
              <Form className="" form={infoForm} layout={get(props, `form.item.${module.module_table}.kieu_giao_dien`, 'vertical')}>
                <Row>
                  <Col xl={24} sm={24} xs={24}>
                    <h1>THÔNG TIN CHUNG</h1>
                  </Col>
                </Row>
                <Row gutter={32}>
                  <Col xl={24} sm={24} xs={24} className="left-content">
                    <div className="border-box module-form-tab">{renderDeductionInfo()}</div>
                  </Col>
                </Row>
              </Form>
            </div>
          )}
          <div className="w5d-form">
            <Form className="" onFinish={handleSubmit} form={deductionForm} layout={get(props, `form.item.${module.module_update_table}.kieu_giao_dien`, 'vertical')}>
              <Row>
                <Col xl={24} sm={24} xs={24}>
                  <h1>THÔNG TIN SUY DIỄN</h1>
                </Col>
              </Row>
              <Row gutter={32}>
                <Col xl={24} sm={24} xs={24} className="left-content">
                  <div className="border-box module-form-tab">{renderDeduction()}</div>
                </Col>
                <Col xl={24} xs={24}>
                  <div className="border-submit">
                    <Form.Item className="button-col">
                      <Row>
                        <Col span={9} />
                        <Col>
                          <Button type="danger" onClick={() => onResetForm()}>
                            {getLangText('FORM.RESET')}
                          </Button>
                        </Col>
                        <Col span={1} />
                        <Col>
                          <Button type="primary" htmlType="submit" onClick={() => setAction({ action: 'deduce' })}>
                            {getLangText('FORM.DEDUCE')}
                          </Button>
                        </Col>
                        <Col span={1} />
                        {!deduceOne && (
                          <Col>
                            <Button type="primary" style={{ background: 'DarkOrange', borderColor: 'DarkOrange' }} htmlType="submit" onClick={() => setAction({ action: 'edit' })}>
                              {getLangText('FORM.UPDATE')}
                            </Button>
                          </Col>
                        )}
                      </Row>
                    </Form.Item>
                  </div>
                </Col>
              </Row>
            </Form>
          </div>
          <div className="w5d-form">
            <Form>
              <br />
              <Row>
                <Col xl={24} sm={24} xs={24}>
                  <h1>KẾT QUẢ SUY DIỄN</h1>
                </Col>
              </Row>
              {attributeItems()}
            </Form>
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    deduction: state.deduction,
    ruleset: state.ruleset,
    config: state.config,
    form: state.form,
  };
};

export default connect(mapStateToProps)(DeductionForm);
