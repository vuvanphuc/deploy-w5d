import { Engine } from 'json-rules-engine';
import { get } from 'lodash';

export const processEngine = (fact, conditions) => {
  const engine = new Engine(conditions);

  return engine
    .run(fact)
    .then((results) => {
      return results.events;
    })
    .catch((e) => {
      console.error('Xảy ra lỗi khi suy diễn tập luật', e);
      return Promise.reject({ error: e.message });
    });
};

export const validateRuleset = async (facts, conditions) => {
  const result = await processEngine(facts, conditions);
  return result;
};

export const deductionResultFilter = (items) => {
  let filter = [];

  for (const [idx, item] of items.entries()) {
    // Trong list filter đã có phần tử cùng type
    const index = filter.findIndex((i) => i.type === item.type);
    if (index >= 0) {
      //  độ ưu tiên nhỏ hơn thì thêm kết quả mới, bỏ kết quả cùng type đó.
      if (filter.findIndex((i) => i.type === item.type && get(i, 'params.priority', 0) < get(item, 'params.priority', 0)) >= 0) {
        filter.splice(index, 1);
        filter.push(item);
      }
      // độ ưu tiên lớn hơn thì bỏ qua kết quả mới, không thêm kết quả mới vào.
      // độ ưu tiên ngang bằng thì thêm phần tử mới vào
      else if (filter.findIndex((i) => i.type === item.type && get(i, 'params.priority', 0) === get(item, 'params.priority', 0)) >= 0) {
        filter.push(item);
      }
    } // Trường hợp khác, chưa có type đó trong filter, đẩy phần tử mới vào
    else {
      filter.push(item);
    }
  }
  return filter;
};
