// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Form, Input, Button, Avatar, notification } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { get } from 'lodash';
import { useHistory } from 'react-router-dom';
// import internal libs
import 'assets/style/auth.css';
import Loading from 'components/loading/Loading';
import defaultLogo from 'assets/images/logo.png';
import Auth from 'containers/auth/Authenticate';
import * as configAction from 'redux/actions/configure';
import * as userAction from 'redux/actions/user';
import { getLangText } from 'helpers/language.helper';
import { getImagePath } from 'helpers/common.helper';
import constants from 'constants/global.constants';
import { config } from 'config';
import { getFileMinio } from 'redux/services/api';
import logoImage from 'assets/images/logo.png';

function RegisterForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const [state, setState] = useState({
    configUserData: {},
    configData: {},
    confirmDirty: false,
    submit: false,
  });

  const [images, setImages] = useState({});

  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  const cf_pros = get(props, 'config.list.result.data', []);
  const cm_config = cf_pros.find((item) => item.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);

  const logo_img = cm_config && cm_config.du_lieu ? `${config.UPLOAD_API_URL}/${JSON.parse(cm_config.du_lieu).site_logo}` : defaultLogo;

  useEffect(() => {
    props.dispatch(configAction.getGlobal());
    const configs = get(props, 'config.list.result.data', []);
    if (configs.length <= 0) {
      props.dispatch(configAction.getConfigs());
    }
  }, []);

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const configUser = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_USER);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    if (configUser) {
      const data = JSON.parse(configUser.du_lieu);
      if (!data.enable_reg) {
        history.push('/auth/login');
      }
      setState((state) => ({
        ...state,
        configUserData: data,
      }));
    }
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
      }));
    }
  }, [props]);
  const onSubmit = (values) => {
    const callback = () => {
      notification.success({
        message: getLangText('MESSAGE.USER_REGISTER_SUCCESS'),
      });
      setState((state) => ({ ...state, submit: true }));
      props.history.push('/auth/registed');
    };
    const data = {
      ...values,
      nhom_nhan_vien_id: state.configUserData.default_role,
      trang_thai: 'active',
    };
    props.dispatch(userAction.registerUser(data, callback));
  };

  const compareToFirstPassword = (rule, value, callback) => {
    if (value) {
      if (value !== form.getFieldValue('mat_khau')) {
        callback(getLangText('MESSAGE.PASSWORD_NOT_MATCH'));
      } else {
        callback();
      }
    }
  };

  useEffect(() => {
    if (state.configData.site_upload_server === 'minio') {
      const getData = async () => {
        const logo = await getImage(state.configData.site_logo, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, logo: logo.data });
      };
      getData();
    }
  }, [state.configData]);

  return (
    <Auth>
      <Helmet>
        <title>{getLangText('FORM.REGISTER_TITLE')}</title>
      </Helmet>
      <div className="logo">
        <Link to="/">
          <Avatar shape="square" size={130} src={getImagePath(state.configData.site_upload_server, state.configData.site_logo, images.logo, logoImage)} />
        </Link>
      </div>
      {props.user.item.loading && <Loading />}
      <Form className="login-form" onFinish={onSubmit} form={form}>
        <Form.Item name="ten_tai_khoan">
          <Input size="normal" prefix={<UserOutlined />} placeholder={getLangText('GLOBAL.USERNAME')} />
        </Form.Item>
        <Form.Item name="ten_nhan_vien">
          <Input size="normal" prefix={<UserOutlined />} placeholder={getLangText('GLOBAL.FULLNAME')} />
        </Form.Item>
        <Form.Item
          name="email"
          rules={[
            { required: true, message: getLangText('GLOBAL.EMAIL_REQUIRE') },
            { type: 'email', message: getLangText('GLOBAL.EMAIL_VALIDATE') },
          ]}
        >
          <Input size="normal" prefix={<MailOutlined />} placeholder={getLangText('GLOBAL.EMAIL')} />
        </Form.Item>
        <Form.Item
          name="mat_khau"
          rules={[
            {
              required: true,
              message: getLangText('GLOBAL.PASSWORD_REQUIRE'),
            },
            { min: 6, message: getLangText('GLOBAL.PASSWORD_VALIDATE_MIN') },
          ]}
        >
          <Input.Password size="normal" prefix={<LockOutlined />} placeholder={getLangText('GLOBAL.PASSWORD')} iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
        </Form.Item>
        <Form.Item
          name="xac_nhan_mat_khau"
          rules={[
            {
              required: true,
              message: getLangText('GLOBAL.CONFIRM_PASSWORD_REQUIRE'),
            },
            {
              validator: compareToFirstPassword,
            },
          ]}
        >
          <Input.Password
            size="normal"
            prefix={<LockOutlined />}
            placeholder={getLangText('GLOBAL.CONFIRM_PASSWORD')}
            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" size="normal" shape="round">
            {getLangText('FORM.REGISTER')}
          </Button>{' '}
          {getLangText('GLOBAL.OR')} <Link to="/auth/login">{getLangText('FORM.LOGIN')}</Link> |{' '}
          <Link className="login-form-forgot" to="/auth/forgot-password">
            {getLangText('FORM.FORGOT_PASSWORD')}
          </Link>
        </Form.Item>
      </Form>
    </Auth>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(RegisterForm);
