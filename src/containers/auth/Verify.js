// import external libs
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Avatar, Button, Col, Row, Spin } from 'antd';
import Icon from '@ant-design/icons';

// import internal libs
import { getLangText } from 'helpers/language.helper';
import { cryptoUnHash } from 'helpers/common.helper';
import 'assets/style/auth.css';
import logoImage from 'assets/images/logo.png';
import verifyImage from 'assets/images/verify.png';
import * as userAction from 'redux/actions/user';
import Auth from 'containers/auth/Authenticate';

function Verify(props) {
  const success = () => (
    <Row>
      <Col xs={{ span: 24 }} md={{ span: 16 }}>
        <h2>Hi {props.user.verify.result && props.user.verify.result.success ? props.user.verify.result.user.email : ''}, </h2>
        <p>{getLangText('GLOBAL.THANKS_FOR_CONFIRMATION')}</p>
        <Link to="/auth/login">
          <Button type="link">
            <Icon type="arrow-right" /> {getLangText('GLOBAL.LOGIN_NOW')}
          </Button>
        </Link>
      </Col>
      <Col xs={{ span: 24 }} md={{ span: 8 }}>
        <Avatar size={100} src={verifyImage} />
      </Col>
    </Row>
  );

  const error = () => (
    <Row>
      <Col xs={{ span: 24 }} md={{ span: 24 }}>
        <h2>{getLangText('GLOBAL.THERE_IS_ERROR')}!</h2>
        <p>{getLangText('GLOBAL.CAN_NOT_VERIFY')}</p>
        <Link to="/">
          <Button type="link">
            <Icon type="arrow-right" /> {getLangText('GLOBAL.BACK_TO_HOME')}
          </Button>
        </Link>
      </Col>
    </Row>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const currentTime = new Date().getTime();
    const expireDate = cryptoUnHash(urlQuery.keyCode);
    if (currentTime < Number(expireDate)) {
      props.dispatch(userAction.verifyUser({ id: urlQuery.userId }));
    }
  }, []);

  return (
    <Auth>
      <Helmet>
        <title>{getLangText('FORM.CONFIRM_TITLE')}</title>
      </Helmet>
      <div className="logo">
        <Link to="/">
          <Avatar shape="square" size={130} src={logoImage} />
        </Link>
      </div>
      {props.user.verify.loading ? (
        <div className="loading-spin">
          {' '}
          <Spin />
        </div>
      ) : (
        <div className="auth-block">{props.user.verify && props.user.verify.result && props.user.verify.result.success ? success() : error()}</div>
      )}
    </Auth>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Verify);
