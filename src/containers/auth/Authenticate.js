// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Layout, Row, Col } from 'antd';
import { io as socketIOClient } from 'socket.io-client';
import Helmet from 'react-helmet';
// import internal libs
import 'assets/style/auth.css';
import { get } from 'lodash';
import * as configAction from 'redux/actions/configure';
import { config } from 'config';
import constants from 'constants/global.constants';
import defaultBackground from 'assets/images/bg_main.png';
import { getImagePath } from 'helpers/common.helper';
import { getFileMinio } from 'redux/services/api';

const defaultLogo = require('assets/images/logo.png');
const { Content } = Layout;

function Auth(props) {
  const [state, setState] = useState({
    bgImage: `${defaultBackground}`,
    configData: {},
  });

  const [images, setImages] = useState({});
  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  const cf_pros = get(props, 'config.list.result.data', []);
  const cm_config = cf_pros.find((item) => item.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);

  const favicon_img = cm_config && cm_config.du_lieu ? `${config.UPLOAD_API_URL}/${JSON.parse(cm_config.du_lieu).site_favicon}` : defaultLogo;

  useEffect(() => {
    const configs = get(props, 'config.list.result', []);
    if (configs && configs.data && configs.data.length > 0 && configs.success) {
      localStorage.setItem('configInfo', JSON.stringify(configs.data));
    }
  }, [props.config]);

  useEffect(() => {
    try {
      const configs = get(props, 'config.list.result.data', []);
      if (configs.length <= 0) {
        props.dispatch(configAction.getConfigs());
      }
      const socket = socketIOClient(config.UPLOAD_API_URL, config.SOCKET_CONFIG);
      socket.on('event://configs-changed', () => {
        props.dispatch(configAction.getConfigs());
      });
      return () => {
        socket.disconnect();
      };
    } catch (error) {
      console.log(error);
    }
  }, []);

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const configData = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    if (configData) {
      const data = JSON.parse(configData.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
        bgImage: `${config.UPLOAD_API_URL}/${data.site_background}`,
      }));
    }
  }, [props]);

  useEffect(() => {
    if (state.configData.site_upload_server === 'minio') {
      const getData = async () => {
        const siteBackground = await getImage(state.configData.site_background, state.configData.site_upload_server, state.configData.site_minio_bucket);
        const siteFavicon = await getImage(state.configData.site_favicon, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, siteFavicon: siteFavicon.data, siteBackground: siteBackground.data });
      };
      getData();
    }
  }, [state.configData]);

  return (
    <Layout
      className="app-auth"
      style={{
        background: `url(${getImagePath(state.configData.site_upload_server, state.configData.site_background, images.siteBackground, '')})`,
      }}
    >
      <Helmet>
        <link rel="icon" href={getImagePath(state.configData.site_upload_server, state.configData.site_favicon, images.siteFavicon, '')} />
      </Helmet>
      <Content>
        <Row>
          <Col xs={{ span: 22, offset: 1 }} lg={{ span: 6, offset: 9 }}>
            {props.children}
          </Col>
        </Row>
      </Content>
    </Layout>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Auth);
