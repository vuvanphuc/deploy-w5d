// import external libs
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Avatar, notification } from 'antd';
import { LockOutlined, UserOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { get } from 'lodash';

// import internal libs
import { config } from 'config';
import W5dSocialLogin from 'components/W5dSocialLogin';
import 'assets/style/auth.css';
import logoImage from 'assets/images/logo.png';
import Auth from 'containers/auth/Authenticate';
import { getLangText } from 'helpers/language.helper';
import * as userAction from 'redux/actions/user';
import Loading from 'components/loading/Loading';
import constants from 'constants/global.constants';
import { getFileMinio } from 'redux/services/api';
import { getImagePath } from 'helpers/common.helper';

function LoginForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();

  const [images, setImages] = useState({});

  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  const onSubmit = (values) => {
    const callback = () => {
      notification.success({
        message: getLangText('MESSAGE.USER_LOGIN_SUCCESS'),
      });
      window.location.reload();
    };
    props.dispatch(userAction.loginUser(values, callback));
  };
  const [state, setState] = useState({
    configData: {},
    isDisableReg: true,
  });

  useEffect(() => {
    const token = localStorage.getItem('userToken');
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    const configUser = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_USER);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
      }));
    } else {
      setState((state) => ({
        ...state,
        configData: {},
      }));
    }
    if (configUser) {
      const dataUser = JSON.parse(configUser.du_lieu);
      setState((state) => ({
        ...state,
        isDisableReg: !dataUser.enable_reg,
      }));
    }
    if (token) {
      history.push('/');
    }
  }, [props]);

  useEffect(() => {
    if (state.configData.site_upload_server === 'minio') {
      const getData = async () => {
        const logo = await getImage(state.configData.site_logo, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, logo: logo.data });
      };
      getData();
    }
  }, [state.configData]);

  return (
    <Auth>
      <Helmet>
        <title>Phần mềm quản trị hệ thống</title>
      </Helmet>
      {props.user.login.loading && <Loading />}
      <div className="logo">
        <Link to="/">
          <Avatar shape="square" size={130} src={getImagePath(state.configData.site_upload_server, state.configData.site_logo, images.logo, logoImage)} />
        </Link>
      </div>
      <Form className="login-form" onFinish={onSubmit} form={form}>
        <Form.Item name="ten_tai_khoan" rules={[{ required: true, message: 'Bạn chưa nhập tên tài khoản' }]}>
          <Input size="normal" prefix={<UserOutlined />} placeholder="Tên tài khoản" />
        </Form.Item>
        <Form.Item name="mat_khau" rules={[{ required: true, message: getLangText('GLOBAL.PASSWORD_REQUIRE') }]}>
          <Input.Password size="normal" prefix={<LockOutlined />} placeholder={getLangText('GLOBAL.PASSWORD')} iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
        </Form.Item>
        <Form.Item className='center-buttons'>
          <Button type="primary" htmlType="submit" size="normal" shape="round">
            {getLangText('FORM.LOGIN')}
          </Button>
          <W5dSocialLogin />
          <div className="other-links">
            <Link className="login-form-forgot" to="/auth/register" disabled={state.isDisableReg ? true : false}>
              {getLangText('FORM.REGISTER')}
            </Link>{' '}
            |{' '}
            <Link className="login-form-forgot" to="/auth/forgot-password">
              {getLangText('FORM.FORGOT_PASSWORD')}
            </Link>{' '}
          </div>
        </Form.Item>
      </Form>
    </Auth>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(LoginForm);
