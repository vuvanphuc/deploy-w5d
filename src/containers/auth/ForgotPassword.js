import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Form, Input, Button, Avatar, Result, notification } from 'antd';
import Icon from '@ant-design/icons';
import { LockOutlined } from '@ant-design/icons';
// import internal libs
import { config } from 'config';
import 'assets/style/auth.css';
import logoImage from 'assets/images/logo.png';
import { get } from 'lodash';
import Auth from 'containers/auth/Authenticate';
import * as userAction from 'redux/actions/user';
import * as configAction from 'redux/actions/configure';
import { getLangText } from 'helpers/language.helper';
import { cryptoUnHash, getImagePath } from 'helpers/common.helper';
import Loading from 'components/loading/Loading';
import constants from 'constants/global.constants';
import { getFileMinio } from 'redux/services/api';

function ForgotForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();

  const [images, setImages] = useState({});

  const getImage = async (path, server, bucket) => {
    const res = await getFileMinio(`${config.UPLOAD_API_URL}/api/media-minio?server=${server}&bucket=${bucket}&tep_tin_url=${path}`);
    return res.data;
  };

  const [state, setState] = useState({
    confirmDirty: false,
    submit: false,
    configData: {},
  });
  useEffect(() => {
    const configs = get(props, 'config.list.result.data', []);
    if (configs.length <= 0) {
      props.dispatch(configAction.getConfigs());
    }
  }, []);
  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === constants.CONFIG_LIST.CONFIG_COMMON);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      setState((state) => ({
        ...state,
        configData: data,
      }));
    }
  }, [props]);
  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('password')) {
      callback(getLangText('MESSAGE.PASSWORD_NOT_MATCH'));
    } else {
      callback();
    }
  };

  const validateToNextPassword = (rule, value, callback) => {
    if (value && state.confirmDirty) {
      form.validateFields(['confirm-pass'], { force: true });
    }
    callback();
  };

  const handleConfirmBlur = (e) => {
    const { value } = e.target;
    setState((state) => ({
      ...state,
      confirmDirty: state.confirmDirty || !!value,
    }));
  };
  const onChangePassword = (values) => {
    const callback = (result) => {
      if (result.msg === 'ok') {
        notification.success({
          message: getLangText('MESSAGE.CHANGE_PASSWORD_SUCCESS'),
        });
        history.push('/auth/login');
      } else {
        notification.success({
          message: getLangText('MESSAGE.CHANGE_PASSWORD_FAIL'),
        });
      }
    };
    const urlQuery = queryString.parse(props.location.search);
    props.dispatch(userAction.changePassword({ mat_khau: values.password, nhan_vien_id: urlQuery.userId }, callback));
  };

  const onSubmit = (values) => {
    const callback = (result) => {
      if (result.success) {
        history.push('/auth/forgot-password?submit=success');
      } else {
        history.push('/auth/forgot-password?submit=error');
      }
    };
    props.dispatch(userAction.forgotPassword({ email: values.email }, callback));
  };

  const renderEmailForm = () => {
    return (
      <Form className="login-form" form={form} onFinish={onSubmit}>
        <p className="alert">{getLangText('GLOBAL.FORGOT_PASSWORD_HELP_TEXT')}</p>
        <Form.Item
          name="email"
          rules={[
            { required: true, message: getLangText('GLOBAL.EMAIL_REQUIRE') },
            { type: 'email', message: getLangText('GLOBAL.EMAIL_VALIDATE') },
          ]}
        >
          <Input size="normal" prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={getLangText('GLOBAL.EMAIL')} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button" size="normal" shape="round">
            {getLangText('FORM.RESET_PASSWORD')}
          </Button>
          <br />
          <div className="other-links">
            {' '}
            <Link className="login-form-forgot" to="/auth/login">
              {getLangText('FORM.LOGIN')}
            </Link>{' '}
          </div>
        </Form.Item>
      </Form>
    );
  };

  const renderChangePasswordForm = () => {
    return (
      <Form className="login-form" form={form} onFinish={onChangePassword}>
        <p className="alert">{getLangText('GLOBAL.FORGOT_PASSWORD_CHANGE_PASS_HELP_TEXT')}</p>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: getLangText('GLOBAL.PASSWORD_REQUIRE'),
            },
            { min: 6, message: getLangText('GLOBAL.PASSWORD_VALIDATE_MIN') },
            { validator: validateToNextPassword },
          ]}
        >
          <Input prefix={<LockOutlined />} type="password" placeholder={getLangText('GLOBAL.PASSWORD')} />
        </Form.Item>
        <Form.Item
          name="confirm-pass"
          rules={[
            {
              required: true,
              message: getLangText('GLOBAL.CONFIRM_PASSWORD_REQUIRE'),
            },
            {
              validator: compareToFirstPassword,
            },
          ]}
        >
          <Input prefix={<LockOutlined />} type="password" onBlur={handleConfirmBlur} placeholder={getLangText('GLOBAL.CONFIRM_PASSWORD')} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button" onClick={() => onChangePassword(form.getFieldsValue())}>
            {getLangText('FORM.CHANGE_PASSWORD')}
          </Button>
          <br />
          <div className="other-links">
            <Link to="/auth/register">{getLangText('FORM.REGISTER')}</Link> |{' '}
            <Link className="login-form-forgot" to="/auth/login">
              {getLangText('FORM.LOGIN')}
            </Link>
          </div>
        </Form.Item>
      </Form>
    );
  };

  const renderRequestSuccess = () => {
    return (
      <div className="login-form">
        <Result
          status="success"
          extra={[
            <Link to="/auth/login">
              <Button>{getLangText('GLOBAL.LOGIN_NOW')}</Button>
            </Link>,
          ]}
          title={getLangText('GLOBAL.PLEASE_CHECK_EMAIL')}
          subTitle={getLangText('GLOBAL.WE_SENT_EMAIL_FORGOT_PASSWORD')}
        />
      </div>
    );
  };

  const renderRequestFail = () => {
    return (
      <div className="login-form">
        <Result
          status="warning"
          extra={[
            <Link to="/auth/forgot-password">
              <Button>{getLangText('GLOBAL.GO_BACK')}</Button>
            </Link>,
          ]}
          title={getLangText('GLOBAL.SENT_MAIL_FORGOT_PASSWORD_FAIL')}
          subTitle={getLangText('GLOBAL.THERE_IS_AN_ERROR_WHEN_SENT_MAIL_FORGOT_PASSWORD')}
        />
      </div>
    );
  };

  const renderRequestExpire = () => {
    return (
      <div className="login-form">
        <Result
          status="warning"
          extra={[
            <Link to="/auth/forgot-password">
              <Button>gửi lại</Button>
            </Link>,
          ]}
          title="Link đã hết hạn"
          subTitle="Link lấy lại mật khẩu đã hết hạn, bạn vui lòng gửi lại email."
        />
      </div>
    );
  };

  const renderForgotPassword = () => {
    const urlQuery = queryString.parse(props.location.search);

    if (urlQuery.submit && urlQuery.submit === 'success') {
      return renderRequestSuccess();
    } else if (urlQuery.submit && urlQuery.submit === 'error') return renderRequestFail();
    else if (urlQuery.userId && urlQuery.keyCode) {
      const currentTime = new Date().getTime();
      const expireDate = atob(urlQuery.keyCode);
      if (currentTime < Number(expireDate)) return renderChangePasswordForm();
      else return renderRequestExpire();
    } else return renderEmailForm();
  };

  useEffect(() => {
    if (state.configData.site_upload_server === 'minio') {
      const getData = async () => {
        const logo = await getImage(state.configData.site_logo, state.configData.site_upload_server, state.configData.site_minio_bucket);
        setImages({ ...images, logo: logo.data });
      };
      getData();
    }
  }, [state.configData]);
  return (
    <Auth>
      <Helmet>
        <title>{getLangText('FORM.FORGOT_PASSWORD')}</title>
      </Helmet>
      {(props.user.forgotPassword.loading || props.user.changePassword.loading) && <Loading />}
      <div className="logo">
        <Link to="/">
          <Avatar shape="square" size={130} src={getImagePath(state.configData.site_upload_server, state.configData.site_logo, images.logo, logoImage)} />
        </Link>
      </div>
      {renderForgotPassword()}
    </Auth>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ForgotForm);
