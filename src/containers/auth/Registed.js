import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Result, Button } from 'antd';
import Auth from 'containers/auth/Authenticate';

import { getLangText } from 'helpers/language.helper';

function Registed() {
  return (
    <Auth>
      <Helmet>
        <title>{getLangText('FORM.REGISTED_TITLE')}</title>
      </Helmet>
      <div className="auth-block">
        <Result
          status="success"
          title={getLangText('GLOBAL.REGISTED_USER')}
          subTitle={getLangText('GLOBAL.CHECK_FOR_VERIFY_MAIL')}
          extra={[
            <Link key="login" to="/auth/login">
              <Button type="link">{getLangText('GLOBAL.LOGIN_NOW')}</Button>
            </Link>,
          ]}
        />
      </div>
    </Auth>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Registed);
