// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { get } from 'lodash';
import ReactQuill from 'react-quill';
import moment from 'moment';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { Button, Form, Input, DatePicker, Row, Col, Select, Modal, Switch, Radio, notification } from 'antd';
import { UnorderedListOutlined, PlusOutlined } from '@ant-design/icons';

// import internal libs
import App from 'App';
import { config } from 'config';
import constants from 'constants/global.constants';
import { getLangText } from 'helpers/language.helper';
import { getUserInformation, shouldHaveAccessPermission, recursiveUserCates } from 'helpers/common.helper';
import * as userAction from 'redux/actions/user';
import * as cookAction from 'redux/actions/cook';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import Loading from 'components/loading/Loading';
import defaultImage from 'assets/images/default.jpg';
import { TextEditorWidget } from 'containers/form/components/widgets';
import { ifStatement } from '@babel/types';

const { Option } = Select;
const { TextArea } = Input;

function CookForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();

  const defaultCook = {
    tieu_de: '',
    mo_ta: '',
    noi_dung: '',
    anh_dai_dien: '',
    nhom_thuc_don_id: '',
    trang_thai: '',
    tin_moi: '',
    tin_noi_bat: '',
  };

  const defaultForm = {
    ...defaultCook,
    uploadLoading: false,
    isChanged: false,
    openMediaLibrary: false,
    insertMedia: {
      open: false,
      editor: null,
    },
  };

  const isEdit = props.match && props.match.params && props.match.params.id;
  const openMediaLibrary = () => setState((state) => ({ ...state, openMediaLibrary: true }));
  //const handleChangeDate = (date) => setState({ ...state, ngay_sinh: date, isChanged: true });

  const handleSubmit = (values) => {
    if (props.match.params.id) {
      // edit a cook
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Sửa thực đơn thành công',
          });
          form.resetFields();
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
          if (isEdit) {
            props.dispatch(
              cookAction.getCook({
                thuc_don_id: props.match.params.id,
              })
            );
          }
        }
      };
      const newFormData = {
        ...state,
        ...values,
      };
      props.dispatch(cookAction.editCook(newFormData, callback));
    } else {
      //add a new cook
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Tạo mới thực đơn thành công',
          });
          setState((state) => ({
            ...state,
            ...values,
            isChanged: false,
          }));
          const routeLink = shouldHaveAccessPermission('thuc_don', 'thuc_don/sua') ? `/cook/edit/${res.data.thuc_don_id}` : '/cooks';
          history.push(routeLink);
        } else {
          notification.error({
            message: res.error,
          });
        }
      };
      props.dispatch(cookAction.createCook({ ...state, ...values, nhom_thuc_don_id: values.nhom_thuc_don_id ? values.nhom_thuc_don_id : '1' }, callback));
    }
  };

  const showField = (field) => (field ? field : '');

  const removeImage = () => setState((state) => ({ ...state, anh_dai_dien: '' }));

  const renderCookCategories = () => {
    let options = [];
    if (props.cook.listCates.result && props.cook.listCates.result.data) {
      options = props.cook.listCates.result.data.map((cate) => (
        <Option key={cate.nhom_thuc_don_id} value={cate.nhom_thuc_don_id}>
          {cate.ten_nhom}
        </Option>
      ));
      return (
        <Select
          showSearch={false}
          value={state.nhom_thuc_don_id}
          loading={props.cook.listCates.loading}
          onChange={(nhom_thuc_don_id) => setState({ ...state, nhom_thuc_don_id, isChanged: true })}
          placeholder={getLangText('GLOBAL.SELECT_CATEGORIES')}
        >
          {options}
        </Select>
      );
    }
  };

  const renderCookStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select defaultValue={state.trang_thai} value={state.trang_thai} onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })} placeholder={getLangText('GLOBAL.SELECT_STATUS')}>
        {options}
      </Select>
    );
  };
  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      anh_dai_dien: file.tep_tin_url,
    }));
  };

  const onInsertImage = (file) => {
    state.insertMedia.editor.insertContent('<img className="image-editor" src="' + `${config.UPLOAD_API_URL}/${file.imgUrl}` + '"/>');
    setState((state) => ({
      ...state,
      isChanged: true,
      insertMedia: {
        open: false,
      },
    }));
  };

  const onResetForm = () => {
    if (props.cook.item.result && isEdit) {
      form.setFieldsValue(props.cook.item.result.data);
      setState((state) => ({ ...state, ...props.cook.item.result.data }));
    } else {
      form.resetFields();
      setState((state) => ({ ...state, ...defaultForm }));
    }
  };

  const [state, setState] = useState(defaultForm);
  useEffect(() => {
    props.dispatch(cookAction.getCookCates());
    if (isEdit) {
      props.dispatch(
        cookAction.getCook({
          thuc_don_id: props.match.params.id,
        })
      );
    } else {
      form.setFieldsValue(defaultCook);
    }
    return () => (window.onbeforeunload = null);
  }, []);

  useEffect(() => {
    if (isEdit && props.cook.item.result) {
      form.setFieldsValue(props.cook.item.result.data);
      setState((state) => ({ ...state, ...props.cook.item.result.data }));
    }
    window.onbeforeunload = null;
  }, [props.cook.item.result]);
  //${showField(state.tieu_de)}
  return (
    <App>
      <Helmet>
        <title>{isEdit ? `Sửa thực đơn` : 'Thêm mới thực đơn'}</title>
      </Helmet>
      {props.cook.item.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <div className="w5d-form">
            <Form layout="vertical" className="" onFinish={handleSubmit} form={form} initialValues={defaultCook}>
              <Row gutter={25}>
                <Col xl={18} sm={16} xs={24}>
                  <h2 className="header-form-title">{isEdit ? 'Cập nhật thực đơn' : 'Thêm mới thực đơn'}</h2>
                </Col>
                <Col xl={6} sm={8} xs={24}>
                  <Col xl={4} sm={24} xs={24}></Col>
                  <Col xl={20} sm={24} xs={24}>
                    <Link to={isEdit ? '/cook/add' : '/cooks'}>
                      <Button block shape="round" type="primary" icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />} className="btn-create-todo">
                        {isEdit ? 'Thêm mới thực đơn' : 'Danh sách thực đơn'}
                      </Button>
                    </Link>
                  </Col>
                </Col>
                <Col xl={18} sm={24} xs={24} className="left-content">
                  <div className="border-box">
                    <Row gutter={25}>
                      <Col xl={24} sm={12} xs={24}>
                        <Form.Item
                          className="input-col"
                          label="Tiêu đề"
                          name="tieu_de"
                          rules={[
                            {
                              required: true,
                              message: 'Tiêu đề là trường bắt buộc',
                            },
                          ]}
                        >
                          <Input
                            placeholder="Tiêu đề"
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                        <Form.Item className="input-col" label="Mô tả" name="mo_ta" rules={[]}>
                          <TextArea
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                            placeholder="Mô tả"
                          />
                        </Form.Item>
                      </Col>
                      <Col xl={12} sm={12} xs={24}></Col>
                    </Row>
                    <Form.Item className="input-col" label="Nội dung" name="noi_dung" rules={[]}>
                      <TextEditorWidget theme="snow" value={state.noi_dung} placeholder="Nội dung là trường bắt buộc" onChange={(val) => setState({ ...state, noi_dung: val })} />
                    </Form.Item>
                  </div>
                </Col>
                <Col xl={6} sm={24} xs={24} className="right-content">
                  <div className="box">
                    <div className="box-body">
                      <div className="border-box hideMobile">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              {/* */}
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                      <div
                        className="border-box"
                        style={{
                          display: !shouldHaveAccessPermission('thuc_don', 'thuc_don/sua') ? 'none' : 'block',
                        }}
                      >
                        <h2>{getLangText('GLOBAL.CATEGORIES')}</h2>
                        <Form.Item
                          className="input-col"
                          name="nhom_thuc_don_id"
                          rules={[
                            {
                              required: true,
                              message: 'Nhóm thực đơn là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderCookCategories()}
                        </Form.Item>
                      </div>
                      <div
                        className="border-box"
                        style={{
                          display: !shouldHaveAccessPermission('thuc_don', 'thuc_don/sua') ? 'none' : 'block',
                        }}
                      >
                        <h2>{getLangText('GLOBAL.STATUS')}</h2>
                        <Form.Item
                          className="input-col"
                          name="trang_thai"
                          rules={[
                            {
                              required: true,
                              message: 'Trạng thái là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderCookStatus()}
                        </Form.Item>
                      </div>
                      <W5dMediaBrowser
                        image={state.anh_dai_dien}
                        field="image"
                        setImageLabel={getLangText('GLOBAL.SET_AVATAR')}
                        removeImageLabel={getLangText('GLOBAL.REMOVE_AVATAR')}
                        title={getLangText('GLOBAL.AVATAR')}
                        openMediaLibrary={() => openMediaLibrary()}
                        removeImage={() => removeImage()}
                      />
                      <div className="border-box hideDesktop">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" htmlType="reset" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={state.openMediaLibrary}
        onCancel={() =>
          setState((state) => ({
            ...state,
            openMediaLibrary: false,
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>
      <Modal
        wrapClassName="w5d-modal-large"
        visible={state.insertMedia.open}
        onCancel={() =>
          setState((state) => ({
            ...state,
            insertMedia: {
              ...state.insertMedia,
              open: false,
            },
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary onSelectImage={(file) => onInsertImage(file)} />
      </Modal>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}
const mapStateToProps = (state) => {
  return {
    cook: state.cook,
  };
};
export default connect(mapStateToProps)(CookForm);
