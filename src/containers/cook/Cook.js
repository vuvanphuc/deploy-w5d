// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
// import InfiniteScroll from 'react-infinite-scroller';
import { get } from 'lodash';
import { Row, Col, List, Avatar, Button, Checkbox, notification, Menu, Dropdown, Space, Modal, Upload, Image } from 'antd';
import { DownOutlined, PlusOutlined, LockOutlined, DeleteOutlined, UnlockOutlined, QuestionCircleOutlined, DownloadOutlined, UploadOutlined } from '@ant-design/icons';

// import internal libs
import * as cookAction from 'redux/actions/cook';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl, getUserInformation } from 'helpers/common.helper';
import { config } from 'config';
import userDefaultImage from 'assets/images/defaultUser.jpg';
import newNews from 'assets/images/ico_news.png';
import hotNews from 'assets/images/ico_hot.png';
import templateFile from 'assets/templates/User_temp.xlsx';
import App from 'App';
import constants from '../../constants/global.constants';
import AppFilter from 'components/AppFilter';
import W5dStatusTag from 'components/status/W5dStatusTag';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import { ExportToExcel } from 'helpers/ExportToExcel';
import W5dImageMinIO from 'components/W5dImageMinIO';
const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';

function Cook(props) {
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);
  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };
  const searchCooks = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.cook.list.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(cookAction.getCooks(searchParams));
    //form.setFieldsValue(state.body);
  };
  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.cook.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
      checkAll: 0,
      checkedList: [],
    }));
    changeUrlParams(field, value);
  };
  const onDelete = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Block thực đơn thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa thực đơn thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        searchCooks();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(cookAction.deleteCook({ id }, callback));
      },
    });
  };

  const allOptions = get(props, 'cook.list.result.data', []).map((item) => item.thuc_don_id);
  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions.filter() : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (thuc_don_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(thuc_don_id) ? state.checkedList.filter((it) => it !== thuc_don_id) : state.checkedList.concat([thuc_don_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length - 1,
      };
    });
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  function deleteListCooks(e) {
    const callback = (res) => {
      if (res.success) {
        searchCooks();
        notification.success({
          message: 'Xóa nhiều thực đơn thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các thực đơn được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(cookAction.deleteCooks({ listIds: state.checkedList }, callback));
      },
    });
  }
  //Active thanh vien theo list => active
  function activeListCooks(e) {
    const callback = (res) => {
      if (res.success) {
        searchCooks();
        notification.success({
          message: 'Kích hoạt thực đơn thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các thực đơn được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(cookAction.editStatusCook({ listIds: state.checkedList, status: 'active' }, callback));
        },
      });
    }
  }
  //Khoa thanh vien theo list => inactive
  function blockListCooks(e) {
    const callback = (res) => {
      if (res.success) {
        searchCooks();
        notification.success({
          message: 'Khóa thực đơn thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn chắc chắn muốn khóa những thực đơn được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            cookAction.editStatusCook(
              {
                listIds: state.checkedList,
                status: 'inactive',
              },
              callback
            )
          );
        },
      });
    }
  }

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListCooks} disabled={!shouldHaveAccessPermission('thuc_don', 'thuc_don/xoa') || state.checkedList.length <= 0}>
        Xóa thực đơn
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeListCooks}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('thuc_don', 'thuc_don/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt thực đơn
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockListCooks}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('thuc_don', 'thuc_don/sua') || state.checkedList.length <= 0}
      >
        Khóa thực đơn
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.cook.list.search,
      ...urlQuery,
      loading: true,
    };
    props.dispatch(cookAction.getCooks(searchParams));
  }, [props.location]);
  const findCate = (id) => {
    const cates = get(props, 'cook.listCates.result.data', []);
    const cate = cates.find((ct) => ct.nhom_thuc_don_id === id);
    return cate ? cate.ten_nhom : '';
  };
  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.cook.list.search,
      ...urlQuery,
      loading: false,
    };
    props.dispatch(cookAction.getCookCates());
  }, []);
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const saveFile = () => {
    FileSaver.saveAs(templateFile, 'tempUser.xlsx');
  };

  function processExcel(data) {
    const workbook = XLSX.read(data, { type: 'binary' });
    const firstSheet = workbook.SheetNames[0];
    const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    const onSuccess = () => {
      notification.success({
        message: 'Import biểu mẫu thành công',
      });
    };
    const onError = () => {
      const urlQuery = queryString.parse(props.location.search);
      const searchParams = {
        ...props.cook.list.search,
        ...urlQuery,
        loading: false,
      };
      props.dispatch(cookAction.getCooks(searchParams));
    };
    //props.dispatch(cookAction.importCooks({ listUsers: excelRows }, onSuccess, onError));
  }

  const customRequest = async ({ file }) => {
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      if (reader.readAsBinaryString) {
        reader.onload = (e) => {
          processExcel(reader.result);
        };
        reader.readAsBinaryString(file);
      }
    } else {
      notification.error({
        message: 'Không đọc được file',
      });
    }
  };

  const menuImport = (
    <Menu>
      <Menu.Item key="1" icon={<DownloadOutlined />} onClick={saveFile}>
        Tải xuống file mẫu
      </Menu.Item>
      <Menu.Item key="2" icon={<UploadOutlined />}>
        <Upload id="uploadFile" multiple={false} accept=".xls,.xlsx" showUploadList={false} customRequest={(data) => customRequest(data)}>
          Đẩy lên dữ liệu
        </Upload>
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý thực đơn</title>
      </Helmet>
      {props.cook.list.loading && <Loading />}
      <Row className="app-main">
        <Col xl={24} className="body-content">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                title="Thực đơn"
                isShowCategories={true}
                isShowStatus={true}
                isShowSearchBox={true}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                categories={get(props, 'cook.listCates.result.data', []).map((cate) => ({
                  name: cate.ten_nhom,
                  id: cate.nhom_thuc_don_id,
                }))}
                search={state.search}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group" gutter={[8, 8]}>
            <Col xl={12} sm={12} xs={24}>
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
            </Col>
            <Col xl={12} sm={12} xs={24} className="right-actions">
              <Space wrap>
                <Dropdown overlay={menuImport} trigger="click">
                  <Button>
                    Import file dữ liệu
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
              <ExportToExcel
                apiData={get(props, 'cook.list.result.data', [])
                  .map((item) => {
                    return {
                      'Mã thực đơn': item.thuc_don_id,
                      'Tên thực đơn': item.tieu_de,
                      'Mô tả': item.mo_ta,
                      'Trạng thái': item.trang_thai,
                      'Ảnh đại diện': item.anh_dai_dien,
                      'Mã nhóm': item.nhom_thuc_don_id,
                      'Tên nhóm': findCate(item.nhom_thuc_don_id),
                      'Ngày tạo': moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT),
                    };
                  })
                  .sort((a, b) => (a.Nhom > b.Nhom ? 1 : -1))}
                fileName="DanhSach"
              />
              {shouldHaveAccessPermission('thuc_don', 'thuc_don/them') && (
                <Link to="/cook/add">
                  <Button shape="round" type="primary" icon={<PlusOutlined />} className="btn-action">
                    Thêm mới thực đơn
                  </Button>
                </Link>
              )}
            </Col>
          </Row>

          <div className="w5d-list">
            <List
              locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
              header={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={3} xl={3}>
                    Ảnh thực đơn
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Tiêu đề
                    <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Nhóm thực đơn
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={7} xl={7} className="text-left">
                    Mô tả
                    <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Trạng thái
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              footer={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={3} xl={3}>
                    Ảnh thực đơn
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    Tiêu đề
                    <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Nhóm thực đơn
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={7} xl={7} className="text-left">
                    Mô tả
                    <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    Trạng thái
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              dataSource={get(props, 'cook.list.result.data', [])}
              pagination={{
                hideOnSinglePage: false,
                responsive: true,
                showLessItems: true,
                pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                pageSize: pageSize,
                onChange: (page, pageSize) => {
                  changeMutilUrlParams({ pageSize, page });
                  setPageSize(pageSize);
                  setPage(page);
                },
                current: Number(page),
                showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                showSizeChanger: true,
              }}
              renderItem={(item) => (
                <List.Item key={item.thuc_don_id} id="listUsers">
                  <Row className="full">
                    <Col xs={1} xl={1}>
                      <Checkbox checked={state.checkedList.includes(item.thuc_don_id)} value={item.thuc_don_id} onChange={() => onChangeCheck(item.thuc_don_id)} />
                    </Col>
                    <Col xs={3} xl={3}>
                      <Link className="view" to={shouldHaveAccessPermission('thuc_don', 'thuc_don/chi_tiet') ? `/cook/detail/${item.thuc_don_id}` : '#'}>
                        <W5dImageMinIO image={item.anh_dai_dien} />
                      </Link>
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      <Link className="edit" to={shouldHaveAccessPermission('thuc_don', 'thuc_don/sua') ? `/cook/edit/${item.thuc_don_id}` : '#'}>
                        {item.tieu_de}
                      </Link>
                      <div className="actions">
                        {shouldHaveAccessPermission('thuc_don', 'thuc_don/sua') && (
                          <Link className="edit act" to={`/cook/edit/${item.thuc_don_id}`}>
                            {getLangText('GLOBAL.EDIT')}
                          </Link>
                        )}
                        {shouldHaveAccessPermission('thuc_don', 'thuc_don/xoa') && (
                          <Button
                            className="delete act"
                            type="link"
                            onClick={() => {
                              onDelete(item.thuc_don_id, item.trang_thai);
                            }}
                          >
                            {item.trang_thai === 'active' ? 'Khóa' : 'Xóa'}
                          </Button>
                        )}
                      </div>
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      {findCate(item.nhom_thuc_don_id)}
                    </Col>
                    <Col xs={7} xl={7} className="text-left">
                      {item.mo_ta}
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      <W5dStatusTag status={item.trang_thai} />
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    cook: state.cook,
  };
};
export default connect(mapStateToProps)(Cook);
