// import external libs
import React from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Col } from 'antd';

import { config } from 'config';

// import internal libs
import App from 'App';

function RuleEditor() {
  const token = localStorage.getItem('userToken');
  return (
    <App>
      <Helmet>
        <title>Quản lý tập luật</title>
      </Helmet>
      <div className="iframe-main">
        <Col span={24}>
          <iframe src={`${config.RULE_SET_APP}?token=${token}`} className="iframe-rule-engine" />
        </Col>
      </div>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    post: state.post,
    todo: state.todo,
  };
};

export default connect(mapStateToProps)(RuleEditor);
