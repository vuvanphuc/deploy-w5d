// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Row, Col, Form, Input, List, Button, Checkbox, notification, Modal, Menu, Space, Dropdown, Select } from 'antd';
import { DeleteOutlined, DownOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { get } from 'lodash';
// import internal libs
import * as notificationAction from '../../redux/actions/notification';
import { config } from '../../config';
import { getLangText } from '../../helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from '../../helpers/common.helper';
import App from '../../App';
import AppFilter from '../../components/AppFilter';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import W5dStatusTag from '../../components/status/W5dStatusTag';

const { TextArea } = Input;
const { confirm } = Modal;
const { Option } = Select;

function NotificationCategory(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  const defaultForm = {
    tieu_de: '',
    mo_ta: '',
    ngay_tao: '',
    trang_thai: 'active',
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    form: defaultForm,
    search: props.notification.listCates.search,
  });

  const searchNotificationCategory = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.notification.listCates.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(notificationAction.getNotificationCates(searchParams));
    form.setFieldsValue(state.form);
  };

  useEffect(() => {
    searchNotificationCategory();
  }, []);

  useEffect(() => {
    window.onbeforeunload = null;
  }, [props]);

  const renderStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select
        disabled={shoudlDisableFrom()}
        defaultValue={state.form.trang_thai}
        value={state.form.trang_thai}
        onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })}
        placeholder={getLangText('GLOBAL.SELECT_STATUS')}
      >
        {options}
      </Select>
    );
  };
  // list functions
  const allOptions = get(props, 'notification.listCates.result.data', []).map((item) => item.nhom_thong_bao_id);
  const onCheckAllChange = (e) => {
    setState((state) => ({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    }));
  };
  const onChangeCheck = (nhom_thong_bao_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(nhom_thong_bao_id) ? state.checkedList.filter((it) => it !== nhom_thong_bao_id) : state.checkedList.concat([nhom_thong_bao_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
    props.dispatch(notificationAction.getNotificationCates({ ...state.search, ...fields }));
  };

  const onEditCategory = (item) => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    setState((state) => ({
      ...state,
      isEdit: true,
      form: item,
    }));
    form.setFieldsValue(item);
  };

  const onDeleteCategory = (id) => {
    const callback = (res) => {
      if (res.success) {
        searchNotificationCategory();
        notification.success({
          message: 'Xóa nhóm thông báo thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa không ?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(notificationAction.deleteNotificationCate({ id }, callback));
      },
    });
  };

  function onDeleteCatelist() {
    const callback = (res) => {
      if (res.success) {
        searchNotificationCategory();
        setState((state) => ({
          ...state,
          checkedList: [],
        }));
        notification.success({
          message: 'Xóa nhóm thông báo thành công',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa những nhóm thông báo được chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(notificationAction.deleteNotificationCates({ listIds: state.checkedList }, callback));
      },
    });
  }

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
    props.dispatch(notificationAction.getNotificationCates({ ...state.search, [field]: value }));
  };

  const shoudlDisableFrom = () => {
    if (state.isEdit && shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/sua')) return false;
    if (!state.isEdit && shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/them')) return false;
    return true;
  };

  const handleSubmit = (values) => {
    if (state.isEdit) {
      // edit a notification category
      const callback = (result) => {
        searchNotificationCategory();
        changeUrlParams('edit', '');
        notification.success({
          message: 'Cập nhật nhóm thông báo thành công.',
        });
        form.resetFields();
        setState((state) => ({
          ...state,
          form: defaultForm,
        }));
      };
      props.dispatch(notificationAction.editNotificationCate({ ...state.form, ...values }, callback));
    } else {
      //add a new notification category
      const callback = () => {
        searchNotificationCategory();
        notification.success({
          message: 'Thêm mới nhóm thông báo thành công.',
        });
        setState((state) => ({
          ...state,
          isChanged: false,
        }));
      };
      props.dispatch(notificationAction.createNotificationCate({ ...state.form, ...values }, callback));
    }
    setState((state) => ({
      ...state,
      form: defaultForm,
      isEdit: false,
      isChanged: false,
    }));
    form.resetFields();
  };

  const onResetNotificationCate = () => {
    setState((state) => ({
      ...state,
      form: defaultForm,
      isEdit: false,
    }));
    form.resetFields();
  };

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }
  const menu = (
    <Menu>
      <Menu.Item key="1" disabled={state.checkedList.length === 0 ? true : false} icon={<DeleteOutlined />} onClick={onDeleteCatelist}>
        Xóa nhóm thông báo
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý bảng dữ liệu </title>
      </Helmet>
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={6} sm={24} xs={24} className="cate-form-block">
              <h2>Thêm mới </h2>
              <Form layout="vertical" className="category-form" form={form} onFinish={handleSubmit}>
                <Form.Item
                  className="input-col"
                  label="Tiêu đề"
                  name="tieu_de"
                  rules={[
                    {
                      required: true,
                      message: 'Tiêu đề là trường bắt buộc.',
                    },
                  ]}
                >
                  <Input
                    disabled={shoudlDisableFrom()}
                    placeholder=""
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item className="input-col" label="Mô tả" name="mo_ta" rules={[]}>
                  <TextArea
                    disabled={shoudlDisableFrom()}
                    placeholder=""
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item
                  label="Trạng thái"
                  className="input-col"
                  name="trang_thai"
                  rules={[
                    {
                      required: true,
                      message: 'Trạng thái là trường bắt buộc.',
                    },
                  ]}
                >
                  {renderStatus()}
                </Form.Item>
                <Form.Item className="button-col">
                  <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableFrom()}>
                    {state.isEdit ? 'Cập nhật nhóm thông báo' : 'Thêm nhóm thông báo '}
                  </Button>
                  {state.isEdit && (
                    <Button shape="round" type="danger" onClick={() => onResetNotificationCate()}>
                      Hủy bỏ
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
            <Col xl={18} sm={24} xs={24}>
              <Row>
                <Col xl={20} sm={16} xs={24}>
                  <AppFilter
                    isShowCategories={false}
                    isShowStatus={false}
                    isShowSearchBox={true}
                    title="Nhóm thông báo"
                    search={state.search}
                    onFilterChange={(field, value) => onFilterChange(field, value)}
                  />
                </Col>
              </Row>
              <Row className="select-action-group">
                <Space wrap>
                  <Dropdown overlay={menu} trigger="click">
                    <Button>
                      Chọn hành động
                      <DownOutlined />
                    </Button>
                  </Dropdown>
                </Space>
              </Row>

              <div className="w5d-list">
                {props.notification.listCates.loading && <Loading />}
                <List
                  locale={{
                    emptyText: getLangText('GLOBAL.NO_ITEMS'),
                  }}
                  header={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={6} className="text-left">
                        Tiêu đề
                        <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={6} className="text-left">
                        Mô tả
                        <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4} className="text-left">
                        Trạng thái
                        <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={6} className="text-left">
                        Tiêu đề
                        <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={6} className="text-left">
                        Mô tả
                        <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4} className="text-left">
                        Trạng thái
                        <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  dataSource={get(props, 'notification.listCates.result.data', [])}
                  pagination={{
                    hideOnSinglePage: false,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.nhom_thong_bao_id}>
                      <Row className="full">
                        <Col span={1}>
                          <Checkbox checked={state.checkedList.includes(item.nhom_thong_bao_id)} value={item.nhom_thong_bao_id} onChange={() => onChangeCheck(item.nhom_thong_bao_id)} />
                        </Col>
                        <Col span={6} className="text-left">
                          <Link className="edit" to={`#`}>
                            {item.tieu_de}
                          </Link>
                          <div className="actions">
                            {shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/sua') && (
                              <Button className="edit act" type="link" onClick={() => onEditCategory(item)}>
                                {getLangText('GLOBAL.EDIT')}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/xoa') && (
                              <Button className="delete act" type="link" onClick={() => onDeleteCategory(item.nhom_thong_bao_id)}>
                                {getLangText('GLOBAL.DELETE')}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('nhom_thong_bao', 'nhom_thong_bao/xem') && (
                              <Link className="view act" to={`/notifications?categories=${item.nhom_thong_bao_id}`}>
                                {getLangText('GLOBAL.VIEW')}
                              </Link>
                            )}
                          </div>
                        </Col>

                        <Col span={6} className="text-left">
                          {item.mo_ta}
                        </Col>
                        <Col span={4} className="text-left">
                          <W5dStatusTag status={item.trang_thai} />
                        </Col>
                        <Col span={4}>{moment(item.ngay_tao).isValid() ? moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT_SHORT) : '-'}</Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>

      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    notification: state.notification,
    config: state.config,
  };
};

export default connect(mapStateToProps)(NotificationCategory);
