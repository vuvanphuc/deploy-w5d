// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { Prompt, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { get } from 'lodash';
import { Row, Col, List, Button, Checkbox, notification, Menu, Input, Form, Select, Dropdown, Space, Modal, Radio, Tabs } from 'antd';
import { DownOutlined, DeleteOutlined, QuestionCircleOutlined, BellOutlined, OrderedListOutlined, FormOutlined } from '@ant-design/icons';
import { io as socketIOClient } from 'socket.io-client';

// import internal libs
import * as notificationAction from 'redux/actions/notification';
import * as userAction from 'redux/actions/user';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from 'helpers/common.helper';
import { config } from 'config';
import App from 'App';
import AppFilter from 'components/AppFilter';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import { useLocation } from 'react-router-dom';
import TextEditor from 'containers/form/components/widgets/TextEditor/TextEditor';
import AutoLaTeX from 'react-autolatex';
const { confirm } = Modal;
const { Option } = Select;
const { TextArea } = Input;
const { TabPane } = Tabs;

function Notification(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const location = useLocation();
  const urlQuery = queryString.parse(props.location.search);

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const isViewOnly = location.pathname === '/notifications/view';

  const defaultForm = {
    tieu_de: '',
    mo_ta: '',
    nhom_thong_bao_id: '',
    receiver: '',
    listUserCate: [],
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    tab: 'NOTIFICATION_LIST',
    openPermission: false,
    form: defaultForm,
    search: props.notification.list.search,
  });

  const searchNotifications = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.notification.list.search,
      ...urlQuery,
    };
    // delete searchParams.page;
    props.dispatch(notificationAction.getNotifications(searchParams));
    const socket = socketIOClient(config.API_URL, config.SOCKET_CONFIG);
    socket.on('event://notifications-changed', () => {
      props.dispatch(notificationAction.getNotifications(searchParams));
    });
    form.setFieldsValue(state.form);
  };
  useEffect(() => {
    searchNotifications();
    props.dispatch(notificationAction.getNotificationCates());
  }, []);

  useEffect(() => {
    props.dispatch(userAction.getUsers({ loading: false }));
    props.dispatch(userAction.getUserCates());
    setState((state) => ({
      ...state,
      usersorCates: get(props, 'user.list.result.data', []),
    }));
  }, []);

  useEffect(() => {
    window.onbeforeunload = null;
  }, [props]);

  // list functions
  const allOptions = get(props, 'notification.list.result.data', []).map((item) => item.thong_bao_id);
  const onCheckAllChange = (e) => {
    setState((state) => ({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    }));
  };
  const onChangeCheck = (thong_bao_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(thong_bao_id) ? state.checkedList.filter((it) => it !== thong_bao_id) : state.checkedList.concat([thong_bao_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
    props.dispatch(notificationAction.getNotifications({ ...state.search, ...fields }));
  };

  const onEditNotification = (item) => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    let receiver = 'user';
    const listReceiver = JSON.parse(item.nguoi_nhan);
    let listUserCate = get(listReceiver, 'user', []);
    if (get(listReceiver, 'user', []).length > 0) {
      setState((state) => ({
        ...state,
        form: {
          ...state.form,
          receiver: 'user',
        },
      }));
    } else if (get(listReceiver, 'user_cate', []).length > 0) {
      listUserCate = get(listReceiver, 'user_cate', []);
      receiver = 'user_cate';
      setState((state) => ({
        ...state,
        form: {
          ...state.form,
          receiver: 'user_cate',
        },
      }));
    }
    setState((state) => ({
      ...state,
      isEdit: true,
      tab: 'NOTIFICATION_FORM',
      form: {
        ...item,
        receiver,
        listUserCate,
      },
    }));
    form.setFieldsValue({ ...item, receiver, listUserCate });
  };

  const onDeleteCategory = (id) => {
    const callback = (res) => {
      if (res.success) {
        searchNotifications();
        notification.success({
          message: 'Xóa nhóm thông báo thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa không ?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(notificationAction.deleteNotification({ id }, callback));
      },
    });
  };

  function onDeleteNotificationlist() {
    const callback = (res) => {
      if (res.success) {
        searchNotifications();
        setState((state) => ({
          ...state,
          checkedList: [],
        }));
        notification.success({
          message: 'Xóa thông báo thành công',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa những thông báo được chọn',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(notificationAction.deleteNotifications({ listIds: state.checkedList }, callback));
      },
    });
  }

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
    props.dispatch(notificationAction.getNotifications({ ...state.search, [field]: value }));
  };

  const shoudlDisableFrom = () => {
    if (state.isEdit && shouldHaveAccessPermission('thong_bao', 'thong_bao/sua')) return false;
    if (!state.isEdit && shouldHaveAccessPermission('thong_bao', 'thong_bao/them')) return false;
    return true;
  };

  const handleSubmit = (values) => {
    if (state.isEdit) {
      // edit a notification category
      const nguoi_nhan = JSON.stringify({
        user: values.receiver === 'user' ? values.listUserCate : [],
        user_cate: values.receiver === 'user_cate' ? values.listUserCate : [],
      });
      const callback = (result) => {
        searchNotifications();
        //changeUrlParams('edit', '');
        notification.success({
          message: 'Cập nhật thông báo thành công.',
        });
        setState((state) => ({
          ...state,
          form: {},
          isEdit: false,
          isChanged: false,
          tab: 'NOTIFICATION_LIST',
        }));
        form.resetFields();
      };
      props.dispatch(notificationAction.editNotification({ ...state.form, ...values, nguoi_nhan }, callback));
    } else {
      //add a new notification category
      const callback = () => {
        searchNotifications();
        notification.success({
          message: 'Thêm mới thông báo thành công.',
        });
        setState((state) => ({
          ...state,
          form: {},
          isEdit: false,
          isChanged: false,
          tab: 'NOTIFICATION_LIST',
        }));
        form.resetFields();
      };

      const nguoi_nhan = JSON.stringify({
        user: state.form.receiver === 'user' ? state.form.listUserCate : [],
        user_cate: state.form.receiver === 'user_cate' ? state.form.listUserCate : [],
      });
      const newFormData = {
        ...state,
        ...values,
        nguoi_nhan,
      };
      props.dispatch(notificationAction.createNotification(newFormData, callback));
    }
    setState((state) => ({
      ...state,
      form: {
        tieu_de: '',
        mo_ta: '',
        nhom_thong_bao_id: '',
        receiver: '',
        listUserCate: [],
      },
      isEdit: false,
      isChanged: false,
    }));
    form.resetFields();
  };

  const onResetNotification = () => {
    setState((state) => ({
      ...state,
      form: {
        tieu_de: '',
        mo_ta: '',
        nhom_thong_bao_id: '',
        receiver: '',
        listUserCate: [],
      },
      isEdit: false,
    }));
    form.resetFields();
  };
  const getListReceiver = (values) => {
    try {
      let stringReceiver = ``;
      const Receiver = JSON.parse(values);
      const listUser = Receiver.user;
      const allUser = get(props, 'user.list.result.data', []);
      const allCate = get(props, 'user.listCates.result.data', []);
      const listCate = Receiver.user_cate;
      if (Receiver.user_cate.length !== 0) {
        for (const [idx, item] of allCate.entries()) {
          if (listCate.includes(item.nhom_nhan_vien_id)) {
            stringReceiver += `${item.ten_nhom}; `;
          }
        }
      } else if (Receiver.user.length !== 0) {
        for (const [idx, item] of allUser.entries()) {
          if (listUser.includes(item.nhan_vien_id)) {
            stringReceiver += `${item.ten_tai_khoan}; `;
          }
        }
      }
      return stringReceiver; //values;
    } catch (error) {
      console.log('error', error);
    }
  };

  const renderMenu = () => {
    let allUsercate;
    if (state.form.receiver === 'user') {
      const usersorCates = get(props, 'user.list.result.data', []);
      allUsercate = usersorCates.map((cate, idx) => {
        return (
          <Option key={idx} value={cate.nhan_vien_id}>
            {cate.ten_tai_khoan}
          </Option>
        );
      });
    } else if (state.form.receiver === 'user_cate') {
      const usersorCates = get(props, 'user.listCates.result.data', []);
      allUsercate = usersorCates.map((cate, idx) => {
        return (
          <Option key={idx} value={cate.nhom_nhan_vien_id}>
            {cate.ten_nhom}
          </Option>
        );
      });
    }
    return (
      <Select
        style={{ width: '100%' }}
        size="large"
        mode="multiple"
        disabled={shoudlDisableFrom()}
        placeholder={state.form.receiver === 'user' ? `Chọn tài khoản` : '`Chọn nhóm tài khoản`'}
        defaultValue={[]}
        value={state.listUserCate}
        onChange={(listUserCate) => setState({ ...state, form: { ...state.form, listUserCate } })}
      >
        {allUsercate}
      </Select>
    );
  };

  const renderMenuCate = () => {
    const notificationCates = get(props, 'notification.listCates.result.data', []);
    const listCates = notificationCates.map((cate, idx) => {
      return (
        <Option key={idx} value={cate.nhom_thong_bao_id}>
          {cate.tieu_de}
        </Option>
      );
    });
    return (
      <Select
        style={{ width: 245 }}
        disabled={shoudlDisableFrom()}
        placeholder="Danh mục"
        defaultValue={[]}
        value={state.nhom_thong_bao_id}
        onChange={(nhom_thong_bao_id) => setState({ ...state, nhom_thong_bao_id, isChanged: true })}
      >
        {listCates}
      </Select>
    );
  };

  const onChange = (e) => {
    setState((state) => ({
      ...state,
      form: {
        ...state.form,
        receiver: e.target.value,
      },
    }));
  };
  const menu = (
    <Menu>
      <Menu.Item key="1" disabled={state.checkedList.length === 0 ? true : false} icon={<DeleteOutlined />} onClick={onDeleteNotificationlist}>
        Xóa thông báo
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý bảng dữ liệu </title>
      </Helmet>
      <Row className="app-main">
        <Col span={24} className="body-content tabs-layout-admin">
          <Tabs
            activeKey={state.tab}
            type="card"
            onChange={(activeTab) => {
              setState({ ...state, tab: activeTab });
              onResetNotification();
            }}
          >
            <TabPane
              key="NOTIFICATION_LIST"
              tab={
                <span>
                  <OrderedListOutlined />
                  Danh sách thông báo
                </span>
              }
            >
              <Row gutter={[30, 30]}>
                <Col xs={24}>
                  <AppFilter
                    isShowCategories={false}
                    isShowStatus={false}
                    isShowSearchBox={true}
                    isShowDatePicker={true}
                    isRangeDatePicker={true}
                    title="Thông báo"
                    search={state.search}
                    onFilterChange={(field, value) => onFilterChange(field, value)}
                    dateWidth={8}
                    searchWidth={16}
                  />

                  {!isViewOnly && (
                    <Space wrap>
                      <Dropdown overlay={menu} trigger="click">
                        <Button>
                          Chọn hành động
                          <DownOutlined />
                        </Button>
                      </Dropdown>
                    </Space>
                  )}

                  <div className={`${isViewOnly ? 'full-text' : ''} w5d-list`}>
                    {props.notification.list.loading && <Loading />}
                    <List
                      locale={{
                        emptyText: getLangText('GLOBAL.NO_ITEMS'),
                      }}
                      header={
                        <Row>
                          <Col span={isViewOnly ? 0 : 1}>
                            <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                          </Col>
                          <Col span={isViewOnly ? 8 : 13} className="text-left">
                            Tiêu đề
                            <Sorting urlQuery={urlQuery} field={'tieu_de'} onSortChange={(fields) => onSortChange(fields)} />
                          </Col>
                          <Col span={isViewOnly ? 16 : 0} className="text-left">
                            Nội dung
                            <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                          </Col>
                          <Col span={isViewOnly ? 0 : 6} className="text-left">
                            Người nhận
                            <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                          </Col>
                          <Col span={isViewOnly ? 0 : 4} className="text-left">
                            {getLangText('GLOBAL.CREATE_DATE')}
                            <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                          </Col>
                        </Row>
                      }
                      footer={null}
                      dataSource={get(props, 'notification.list.result.data', [])}
                      pagination={{
                        hideOnSinglePage: false,
                        responsive: true,
                        showLessItems: true,
                        pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                        pageSize: pageSize,
                        onChange: (page, pageSize) => {
                          changeMutilUrlParams({ pageSize, page });
                          setPageSize(pageSize);
                          setPage(page);
                        },
                        current: Number(page),
                        showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                        showSizeChanger: true,
                      }}
                      renderItem={(item) => (
                        <List.Item key={item.thong_bao_id}>
                          <Row className="full">
                            <Col span={isViewOnly ? 0 : 1}>
                              <Checkbox checked={state.checkedList.includes(item.thong_bao_id)} value={item.thong_bao_id} onChange={() => onChangeCheck(item.thong_bao_id)} />
                            </Col>
                            <Col span={isViewOnly ? 8 : 13} className="text-left">
                              <Link className="edit" to={`#`}>
                                <BellOutlined /> {item.tieu_de}
                              </Link>
                              {!isViewOnly && (
                                <div className="actions">
                                  {shouldHaveAccessPermission('thong_bao', 'thong_bao/sua') && (
                                    <Button className="edit act" type="link" onClick={() => onEditNotification(item)}>
                                      {getLangText('GLOBAL.EDIT')}
                                    </Button>
                                  )}
                                  {shouldHaveAccessPermission('thong_bao', 'thong_bao/xoa') && (
                                    <Button className="delete act" type="link" onClick={() => onDeleteCategory(item.thong_bao_id)}>
                                      {getLangText('GLOBAL.DELETE')}
                                    </Button>
                                  )}
                                </div>
                              )}
                            </Col>
                            <Col span={isViewOnly ? 16 : 0} className="text-left">
                              <div className="viewable-content">
                                <AutoLaTeX>{item.mo_ta}</AutoLaTeX>
                              </div>
                            </Col>
                            <Col span={isViewOnly ? 0 : 6} className="text-left">
                              {getListReceiver(item.nguoi_nhan)}
                            </Col>
                            <Col span={isViewOnly ? 0 : 4} className="text-left">
                              {moment(item.ngay_tao).isValid() ? moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT_SHORT) : '-'}
                            </Col>
                          </Row>
                        </List.Item>
                      )}
                    />
                  </div>
                </Col>
              </Row>
            </TabPane>
            <TabPane
              key="NOTIFICATION_FORM"
              tab={
                <span>
                  <FormOutlined />
                  {state.isEdit ? 'Cập nhật' : 'Thêm mới'}
                </span>
              }
            >
              <Row>
                <Col xl={24} sm={24} xs={24} className="cate-form-block">
                  <h2>Thêm mới </h2>
                  <Form layout="vertical" className="category-table" form={form} onFinish={handleSubmit}>
                    <Form.Item
                      className="input-col"
                      label="Tiêu đề"
                      name="tieu_de"
                      rules={[
                        {
                          required: true,
                          message: 'Tiêu đề là trường bắt buộc.',
                        },
                      ]}
                    >
                      <Input
                        disabled={shoudlDisableFrom()}
                        placeholder="Nhập tiêu đề thông báo"
                        onChange={() => {
                          if (!state.isChanged) {
                            setState((state) => ({
                              ...state,
                              isChanged: true,
                            }));
                          }
                        }}
                      />
                    </Form.Item>
                    <Form.Item className="input-col" label="Nội dung" name="mo_ta" rules={[]}>
                      <TextEditor
                        disabled={shoudlDisableFrom()}
                        value={state.form.mo_ta}
                        placeholder="Nhập nội dung thông báo"
                        onChange={(val) => {
                          if (val !== state.form.mo_ta) {
                            setState({ ...state, form: { ...state.form, mo_ta: val }, isChanged: true });
                          }
                        }}
                        showToolbar={true}
                        isMinHeight300={true}
                        isSimple={true}
                      />
                    </Form.Item>
                    <Form.Item className="input-col" label="Nhóm thông báo" name="nhom_thong_bao_id" rules={[]}>
                      {renderMenuCate()}
                    </Form.Item>
                    <Form.Item
                      className="input-col"
                      label="Gửi tới"
                      name="receiver"
                      rules={[
                        {
                          required: true,
                          message: 'Người nhận trường bắt buộc.',
                        },
                      ]}
                    >
                      <Radio.Group
                        options={[
                          { label: 'Người dùng', value: 'user' },
                          { label: 'Nhóm người dùng', value: 'user_cate' },
                        ]}
                        optionType="button"
                        buttonStyle="solid"
                        disabled={shoudlDisableFrom()}
                        value={state.form.receiver}
                        onChange={(e) => onChange(e)}
                      />
                    </Form.Item>
                    {state.form.receiver && (
                      <Form.Item
                        className="input-col"
                        name="listUserCate"
                        rules={[
                          {
                            required: true,
                            message: 'Người nhận trường bắt buộc.',
                          },
                        ]}
                      >
                        {renderMenu()}
                      </Form.Item>
                    )}

                    <Form.Item className="button-col">
                      <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableFrom()}>
                        {state.isEdit ? 'Cập nhật thông báo' : 'Thêm thông báo '}
                      </Button>
                      {state.isEdit && (
                        <Button shape="round" type="danger" onClick={() => onResetNotification()}>
                          Hủy bỏ
                        </Button>
                      )}
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    notification: state.notification,
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(Notification);
