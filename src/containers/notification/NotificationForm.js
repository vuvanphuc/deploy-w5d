// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import moment from 'moment';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { Button, Form, Input, DatePicker, Row, Col, Select, Modal, Switch, notification, message } from 'antd';
import { UnorderedListOutlined, PlusOutlined } from '@ant-design/icons';

// import internal libs
import App from 'App';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission } from 'helpers/common.helper';
import * as notificationAction from 'redux/actions/notification';
import Loading from 'components/loading/Loading';

const { Option } = Select;

function NotificationForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();

  const defaultNotification = {
    tieu_de: '',
    mo_ta: '',
    ngay_tao: '',
  };

  const defaultForm = {
    ...defaultNotification,
    uploadLoading: false,
    isChanged: false,
    openMediaLibrary: false,
    insertMedia: {
      open: false,
      editor: null,
    },
  };

  const isEdit = props.match && props.match.params && props.match.params.id;

  const handleSubmit = (values) => {
    if (props.match.params.id) {
      // edit a notification
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: getLangText('MESSAGE.UPDATE_NOTIFICATION_SUCCESS'),
          });
          form.resetFields();
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
          if (isEdit) {
            props.dispatch(
              notificationAction.getNotification({
                thong_bao_id: props.match.params.id,
              })
            );
          }
        }
      };
      const newFormData = {
        ...state,
        ...values,
      };
      props.dispatch(notificationAction.editNotification(newFormData, callback));
    } else {
      //add a new user
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: getLangText('MESSAGE.CREATE_NOTIFICATION_SUCCESS'),
          });
          setState((state) => ({
            ...state,
            ...values,
            isChanged: false,
          }));
          const routeLink = shouldHaveAccessPermission('thong_bao', 'thong_bao/sua') ? `/notification/edit/${res.data.thong_bao_id}` : '/notifications';
          history.push(routeLink);
        }
      };
      props.dispatch(notificationAction.createNotification({ ...state, ...values, nhom_thong_bao_id: values.nhom_thong_bao_id ? values.nhom_thong_bao_id : '1' }, callback));
    }
  };

  const showField = (field) => (field ? field : '');

  const renderNotificationCategories = () => {
    let options = [];
    if (props.notification.listCates.result && props.notification.listCates.result.data) {
      options = props.notification.listCates.result.data.map((cate) => (
        <Option key={cate.nhom_thong_bao_id} value={cate.nhom_thong_bao_id}>
          {cate.tieu_de}
        </Option>
      ));
      return (
        <Select
          showSearch={false}
          value={state.nhom_thong_bao_id}
          loading={props.notification.listCates.loading}
          onChange={(nhom_thong_bao_id) => setState({ ...state, nhom_thong_bao_id, isChanged: true })}
          placeholder={getLangText('GLOBAL.SELECT_CATEGORIES')}
        >
          {options}
        </Select>
      );
    }
  };

  const [state, setState] = useState(defaultForm);

  const onResetForm = () => {
    if (props.notification.item.result && isEdit) {
      form.setFieldsValue(props.notification.item.result.data);
      setState((state) => ({ ...state, ...props.notification.item.result.data }));
    } else {
      form.resetFields();
      setState((state) => ({ ...state, ...defaultForm }));
    }
  };

  useEffect(() => {
    props.dispatch(notificationAction.getNotificationCates());
    if (isEdit) {
      props.dispatch(
        notificationAction.getNotification({
          id: props.match.params.id,
        })
      );
    } else {
      form.setFieldsValue(defaultNotification);
    }
    return () => (window.onbeforeunload = null);
  }, []);

  useEffect(() => {
    if (isEdit && props.notification.item.result) {
      form.setFieldsValue(props.notification.item.result.data);
      setState((state) => ({ ...state, ...props.notification.item.result.data }));
    }
    window.onbeforeunload = null;
  }, [props.notification.item.result]);

  return (
    <App>
      <Helmet>
        <title>{isEdit ? `${getLangText('NOTIFICATION.EDIT_NOTIFICATION')}: ${showField(state.fullname)}` : getLangText('NOTIFICATION.ADD_NEW_NOTIFICATION')}</title>
      </Helmet>
      {props.notification.item.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <div className="w5d-form">
            <Form layout="vertical" className="" onFinish={handleSubmit} form={form} initialValues={defaultNotification}>
              <Row gutter={25}>
                <Col xl={18} sm={16} xs={24}>
                  <h2 className="header-form-title">{isEdit ? getLangText('NOTIFICATION.EDIT_NOTIFICATION') : getLangText('NOTIFICATION.ADD_NEW_NOTIFICATION')}</h2>
                </Col>
                <Col xl={6} sm={8} xs={24}>
                  <Col xl={4} sm={24} xs={24}></Col>
                  <Col xl={20} sm={24} xs={24}>
                    <Link to={isEdit ? '/notification/add' : '/notifications'}>
                      <Button block shape="round" type="primary" icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />} className="btn-create-todo">
                        {isEdit ? getLangText('NOTIFICATION.ADD_NEW_NOTIFICATION') : getLangText('NOTIFICATION.LIST_NOTIFICATIONS')}
                      </Button>
                    </Link>
                  </Col>
                </Col>
                <Col xl={18} sm={24} xs={24} className="left-content">
                  <div className="border-box">
                    <Row gutter={25}>
                      <Col xl={12} sm={12} xs={24}>
                        <Form.Item
                          className="input-col"
                          label={getLangText('GLOBAL.TITLE')}
                          name="tieu_de"
                          rules={[
                            {
                              required: true,
                              message: 'Tiêu đề là trường bắt buộc',
                            },
                          ]}
                        >
                          <Input
                            placeholder={getLangText('GLOBAL.TITLE')}
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                        <Form.Item
                          className="input-col"
                          label={getLangText('GLOBAL.DESCRIPTION')}
                          name="mo_ta"
                          rules={[
                            {
                              required: true,
                              message: 'Mô tả là trường bắt buộc',
                            },
                          ]}
                        >
                          <Input
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                            placeholder={'Mô tả'}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </div>
                </Col>
                <Col xl={6} sm={24} xs={24} className="right-content">
                  <div className="box">
                    <div className="box-body">
                      <div className="border-box hideMobile">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              {/* */}
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                      <div className="border-box">
                        <h2>{getLangText('GLOBAL.CATEGORIES')}</h2>
                        <Form.Item
                          className="input-col"
                          name="nhom_thong_bao_id"
                          rules={[
                            {
                              required: true,
                              message: 'Nhóm thông báo là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderNotificationCategories()}
                        </Form.Item>
                      </div>

                      <div className="border-box hideDesktop">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" htmlType="reset" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>

      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    notification: state.notification,
    todo: state.todo,
  };
};

export default connect(mapStateToProps)(NotificationForm);
