import React from 'react';
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend } from 'chart.js';
import { connect } from 'react-redux';
import { Line } from 'react-chartjs-2';
import { get } from 'lodash';
import moment from 'moment';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);

const ChartUser = (props) => {
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
    },
  };

  const getDataSourceGlucose = () => {
    let items = get(props, 'module.list.result.data', []);
    items = [...items].sort((d1, d2) => new Date(d1.ngay_thuc_hien).getTime() - new Date(d2.ngay_thuc_hien).getTime());
    const itemType1 = items.map((it) => (it.thoi_diem === 1 ? it.gia_tri : null));
    const itemType2 = items.map((it) => (it.thoi_diem === 2 ? it.gia_tri : null));
    const itemType3 = items.map((it) => (it.thoi_diem === 3 ? it.gia_tri : null));
    const itemType4 = items.map((it) => (it.thoi_diem === 4 ? it.gia_tri : null));
    const labels = items.map((it) => moment(it.ngay_thuc_hien).format('ha DD/MM'));

    const dataSource = {
      labels,
      datasets: [
        {
          label: 'Trước ăn sáng',
          data: itemType1,
          borderColor: '#32c5d2',
          backgroundColor: '#32c5d2',
        },
        {
          label: 'Trước ăn trưa',
          data: itemType2,
          borderColor: '#8e44ad',
          backgroundColor: '#8e44ad',
        },
        {
          label: 'Trước ăn tối',
          data: itemType3,
          borderColor: '#faad14',
          backgroundColor: '#faad14',
        },
        {
          label: 'Trước khi ngủ(21h)',
          data: itemType4,
          borderColor: '#333',
          backgroundColor: '#333',
        },
      ],
    };
    return dataSource;
  };

  const getDataSourcePressure = () => {
    let items = get(props, 'module.list.result.data', []);
    items = [...items].sort((d1, d2) => new Date(d1.ngay_thuc_hien).getTime() - new Date(d2.ngay_thuc_hien).getTime());
    const itemType1 = items.map((it) => it.tam_thu);
    const itemType2 = items.map((it) => it.tam_truong);
    const labels = items.map((it) => moment(it.ngay_thuc_hien).format('ha DD/MM'));

    const dataSource = {
      labels,
      datasets: [
        {
          label: 'Tâm thu',
          data: itemType1,
          borderColor: '#32c5d2',
          backgroundColor: '#32c5d2',
        },
        {
          label: 'Tâm trương',
          data: itemType2,
          borderColor: '#8e44ad',
          backgroundColor: '#8e44ad',
        },
      ],
    };
    return dataSource;
  };

  const getDataSourceWeight = () => {
    let items = get(props, 'module.list.result.data', []);
    items = [...items].sort((d1, d2) => new Date(d1.ngay_thuc_hien).getTime() - new Date(d2.ngay_thuc_hien).getTime());
    const itemType1 = items.map((it) => it.gia_tri);
    // const labels = items
    //   .map((it) => moment(it.ngay_thuc_hien).format('DD/MM'))
    //   .filter(function (value, index, array) {
    //     return array.indexOf(value) === index;
    //   });

    const labels = items.map((it) => moment(it.ngay_thuc_hien).format('ha DD/MM'));

    const dataSource = {
      labels,
      datasets: [
        {
          label: 'Cân nặng',
          data: itemType1,
          borderColor: '#32c5d2',
          backgroundColor: '#32c5d2',
        },
      ],
    };
    return dataSource;
  };

  const getDataSource = () => {
    if (props.module_id === 'glucose') {
      return getDataSourceGlucose();
    } else if (props.module_id === 'pressure') {
      return getDataSourcePressure();
    } else if (props.module_id === 'weight') {
      return getDataSourceWeight();
    } else {
      return {
        labels: [],
        datasets: [],
      };
    }
  };

  return <Line options={options} data={getDataSource()} />;
};

const mapStateToProps = (state) => {
  return {
    module: state.module,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ChartUser);
