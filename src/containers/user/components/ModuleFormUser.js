// import external libs
import React, { useState, useEffect } from 'react';
import { useHistory, useParams, useLocation } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Button, Form, Tabs, Row, Col, notification, Select } from 'antd';

// import internal libs
import App from 'App';
import { getLangText } from 'helpers/language.helper';
import * as moduleAction from 'redux/actions/moduleApp';
import constants from 'constants/global.constants';
import Loading from 'components/loading/Loading';
import FormBuilderRender from 'containers/form/FormBuilderRender';

function ModuleFormUser(props) {
  const params = useParams();
  const [form] = Form.useForm();
  const history = useHistory();
  const location = useLocation();
  const urlQuery = queryString.parse(location.search);

  const defaultForm = {
    trang_thai: true,
    isChanged: false,
  };
  const [state, setState] = useState(defaultForm);
  const [body, setBody] = useState({});
  const [isSub, setIsSub] = useState(false);

  const searchData = () => {
    const searchParams = {
      module_id: props.module_id,
      filter_key: 'benh_nhan_id',
      filter_value: props.user_id,
    };
    props.dispatch(moduleAction.getModules(searchParams));
  };

  const handleSubmit = (values) => {
    let formatValues = {};
    for (const [key, value] of Object.entries(values)) {
      const newVal = Array.isArray(value) ? JSON.stringify(value) : value;
      formatValues = { ...formatValues, [key]: newVal };
    }
    if (props.isEdit) {
      // edit a data
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Cập nhật dữ liệu thành công.',
          });
          form.resetFields();
          searchData();
        }
      };
      const newFormData = {
        data: {
          ...body,
          ...formatValues,
          urlParams: {
            id: body[props.moduleConfig.module_related_key],
            module_action: 'edit',
            module_id: props.module_id,
          },
        },
        id: body[props.moduleConfig.module_related_key],
        module: props.moduleConfig,
      };
      props.dispatch(moduleAction.editModule(newFormData, callback));
    }
  };

  const onResetForm = () => {
    if (props.module.item.result && props.isEdit && params.id) {
      form.setFieldsValue(props.module.item.result.data);
      setBody(props.module.item.result.data);
    } else {
      form.resetFields();
    }
  };

  useEffect(() => {
    if (props.isEdit && props.module.item.result) {
      const values = get(props, 'module.list.result.data', []);
      setBody(values[0]);
      form.setFieldsValue(values[0]);
    }
  }, [props.module.list.result]);

  useEffect(() => {
    if (props.subTab === props.module_id) {
      searchData();
    }
  }, [props.config.list.result.data, location, props.subTab, props.module_id, props.user_id]);
  if (!props.moduleConfig) return null;
  return (
    <div className="w5d-form">
      {props.module.list.loading && <Loading />}
      <Form className="" onFinish={handleSubmit} form={form} layout={get(props, `form.item.${props.moduleConfig.module_table}.kieu_giao_dien`, 'vertical')}>
        <Row gutter={25}>
          <Col xl={18} sm={18} xs={18} className="left-content">
            <div className="border-box module-form-tab">
              <FormBuilderRender
                formId={props.moduleConfig.module_form}
                table={props.moduleConfig.module_table}
                formConfig={props.moduleConfig.module_table ? props.form.item[props.moduleConfig.module_table] : {}}
                formObj={form}
                body={body}
                isEdit={props.isEdit}
              />
            </div>
          </Col>
          <Col xl={6} sm={6} xs={6} className="right-content">
            <div className="box">
              {urlQuery.tab !== 'LIST_ITEMS' && (
                <div className="box-body">
                  <div className="border-box hideMobile">
                    <h2>{getLangText('FORM.PUBLISH')}</h2>
                    <Form.Item className="input-col">
                      <Row>
                        <Col span="11">
                          <Button block type="danger" onClick={() => onResetForm()}>
                            {getLangText('FORM.RESET')}
                          </Button>
                        </Col>
                        <Col span="2"></Col>
                        <Col span="11">
                          <Button block type="primary" htmlType="submit">
                            {props.isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>
                  </div>
                </div>
              )}
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    module: state.module,
    config: state.config,
    form: state.form,
  };
};

export default connect(mapStateToProps)(ModuleFormUser);
