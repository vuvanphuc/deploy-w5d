// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { get } from 'lodash';
import { useHistory, Link, useLocation } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Row, Col, List, Button, Menu, Checkbox, Dropdown, Space, Modal, notification } from 'antd';
import { PlusOutlined, DownOutlined, DeleteOutlined, QuestionCircleOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons';
import moment from 'moment';
// import internal libs
import * as moduleAction from 'redux/actions/moduleApp';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl } from '../../../helpers/common.helper';
import { config } from 'config';
import App from 'App';
import AppFilter from 'components/AppFilter';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import constants from 'constants/global.constants';
import { renderDataFormatted } from 'helpers/module.helper';
import W5dStatusTag from '../../../components/status/W5dStatusTag';

const { confirm } = Modal;
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_MODULE;
const dateFormat = 'DD-MM-YYYY';

function ModuleUser(props) {
  const history = useHistory();
  const location = useLocation();
  const urlQuery = queryString.parse(location.search);
  let allOptions = [];

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.module.list,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });
  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    });
  };
  const onChangeCheck = (id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(id) ? state.checkedList.filter((it) => it !== id) : state.checkedList.concat([id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(location.search);

    let url = location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(location.pathname, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(location.search);
    let url = location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
  };
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const deleteModule = (id, trang_thai) => {
    if (module.module_show_filter_status && trang_thai === 'inactive') {
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: 'Xóa dữ liệu thành công.',
          });
          searchData(module.module_id);
        }
      };
      confirm({
        title: `Bạn chắc chắn muốn xóa bản ghi ${id} không?`,
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(moduleAction.deleteModule({ id, module_id: module.module_id }, callback));
        },
      });
    } else {
      if (module.module_show_filter_status) {
        blockModule(id, 'Khóa');
      } else {
        blockModule(id, 'Xóa');
      }
    }
  };

  function deleteModules() {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Xóa nhiều bản ghi thành công.',
        });
        searchData(module.module_id);
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các bản ghi được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(moduleAction.deleteModules({ listIds: state.checkedList, module_id: module.module_id }, callback));
      },
    });
  }

  const blockModule = (item, actionTitle) => {
    const callback = () => {
      searchData(module.module_id);
      notification.success({
        message: `${actionTitle} bản ghi '${item}' thành công!`,
      });
    };
    confirm({
      title: `Bạn chắc chắn muốn ${actionTitle} bản ghi '${item}' không?`,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(moduleAction.editModulesStatus({ data: { listIds: [item], status: 'inactive' }, module_id: module.module_id }, callback));
      },
    });
  };

  //Active theo list => active
  function activeModules(e) {
    const callback = (res) => {
      if (res.success) {
        searchData(module.module_id);
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Kích hoạt nhiều bản ghi thành công!',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các bản ghi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(moduleAction.editModulesStatus({ data: { listIds: state.checkedList, status: 'active' }, module_id: module.module_id }, callback));
        },
      });
    }
  }

  //Khoa theo list => inactive
  function blockModules(e) {
    const callback = (res) => {
      if (res.success) {
        searchData(module.module_id);
        setState((state) => ({
          ...state,
          checkedList: [],
          checkAll: false,
        }));
        notification.success({
          message: 'Khóa nhiều bản ghi thành công!',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn khóa các bản ghi được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(moduleAction.editModulesStatus({ data: { listIds: state.checkedList, status: 'inactive' }, module_id: module.module_id }, callback));
        },
      });
    }
  }
  const renderHeaderFooterColumns = () => {
    if (module && module.module_fields) {
      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col xs={item.width} xl={item.width} key={idx} className="text-left">
            {item.title} {item.isSortable && <Sorting urlQuery={urlQuery} field={item.column} onSortChange={(fields) => onSortChange(fields)} />}
          </Col>
        );
      });
      return (
        <Row>
          <Col xs={1} xl={1} className="text-left">
            <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
          </Col>
          {columns}
          {module.module_show_filter_status && (
            <Col xs={3} xl={4}>
              {getLangText('GLOBAL.STATUS')}
              <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
            </Col>
          )}
        </Row>
      );
    }
  };

  const gotoDetail = (data) => {
    if (module && module.module_fields && (shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) || shouldHaveAccessPermission(module.module_id, `${module.module_id}/chi_tiet`))) {
      const keyField = module.module_fields.find((item) => item.key);
      history.push(`/module/${module.module_id}/edit/${data[keyField.column]}`);
    }
  };
  const renderRows = (data) => {
    if (module && module.module_fields) {
      //lấy id của từng bản ghi, đẩy vào allOptions
      allOptions.push(data[module.module_fields.find((field) => field.key === true)]);
      const columns = module.module_fields.map((item, idx) => {
        return (
          <Col xs={item.width} xl={item.width} key={idx} className="text-left">
            {item.key && (shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) || shouldHaveAccessPermission(module.module_id, `${module.module_id}/chi_tiet`)) ? (
              <Link to={`/module/${module.module_id}/edit/${data[item.column]}?returnLink=${btoa(location.pathname)}`}>{renderDataFormatted(data, item)}</Link>
            ) : (
              renderDataFormatted(data, item)
            )}
            {item.key && (
              <div className="actions">
                {shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) && (
                  <Link className="edit act" to={`/module/${module.module_id}/edit/${data[item.column]}?returnLink=${btoa(location.pathname)}`}>
                    {getLangText('GLOBAL.EDIT')}
                  </Link>
                )}
                {shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) && (
                  <Button className="delete act" type="link" onClick={() => deleteModule(data[item.column], 'inactive')}>
                    {getLangText('GLOBAL.DELETE')}
                  </Button>
                )}
              </div>
            )}
          </Col>
        );
      });
      return (
        <Row className="full" onDoubleClick={() => gotoDetail(data)}>
          <Col xs={1} xl={1} className="text-left">
            <Checkbox
              checked={state.checkedList.includes(data[module.module_fields.find((field) => field.key === true)])}
              value={data[module.module_fields.find((field) => field.key === true)]}
              onChange={() => onChangeCheck(data[module.module_fields.find((field) => field.key === true).column])}
            />
          </Col>
          {columns}
          {module.module_show_filter_status && (
            <Col xs={3} xl={4}>
              <W5dStatusTag status={data.trang_thai} />
            </Col>
          )}
        </Row>
      );
    }
  };

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);
  const [module, setModule] = useState({});

  const searchData = (module_id) => {
    const urlQuery = queryString.parse(location.search);
    const searchParams = {
      ...props.module.list.search,
      pageSize: pageSize,
      ...urlQuery,
      module_id,
      filter_key: 'benh_nhan_id',
      filter_value: props.user_id,
    };
    props.dispatch(moduleAction.getModules(searchParams));
  };

  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      const currentModuleId = get(props, 'module_id', []);
      if (currentModuleId) {
        const currentModule = data.modules.find((module) => module.module_id === currentModuleId);
        setModule(currentModule);
        if (props.subTab === props.module_id) {
          searchData(currentModule.module_id);
        }
      }
    }
  }, [props.config.list.result.data, location, props.subTab, props.module_id]);

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteModules} disabled={!shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) || state.checkedList.length <= 0}>
        Xóa bản ghi
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeModules}
        hidden={!module.module_show_filter_status}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission(module.module_id, `${module.module_id}/sua`) || state.checkedList.length <= 0}
      >
        Kích hoạt bản ghi
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockModules}
        hidden={!module.module_show_filter_status}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission(module.module_id, `${module.module_id}/xoa`) || state.checkedList.length <= 0}
      >
        Khóa bản ghi
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      {props.module.list.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                search={state.search}
                isShowStatus={module.module_show_filter_status}
                isShowSearchBox={module.module_show_search_box}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group-cate">
            <Col xl={16} sm={16} xs={24}>
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
            </Col>
            <Col xl={8} sm={8} xs={24} className="right-actions">
              <Link to={`/module/${module.module_id}/add?returnLink=${btoa(location.pathname)}&defaultForm=${btoa(JSON.stringify({ benh_nhan_id: props.user_id }))}`}>
                <Button shape="round" type="primary" icon={<PlusOutlined />} disabled={!shouldHaveAccessPermission(module.module_id, `${module.module_id}/them`)} className="btn-create-todo">
                  Thêm mới
                </Button>
              </Link>
            </Col>
          </Row>
          <div className="w5d-list">
            <List
              locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
              header={renderHeaderFooterColumns()}
              footer={renderHeaderFooterColumns()}
              dataSource={get(props, 'module.list.result.data', [])}
              pagination={{
                hideOnSinglePage: false,
                total: get(props, 'module.list.result.meta.totalItem', 0),
                responsive: true,
                showLessItems: true,
                pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                pageSize: pageSize,
                onChange: (page, pageSize) => {
                  changeMutilUrlParams({ pageSize, page });
                  setPageSize(pageSize);
                  setPage(page);
                },
                current: Number(page),
                showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                showSizeChanger: true,
              }}
              renderItem={(item, idx) => <List.Item key={idx}>{renderRows(item)}</List.Item>}
            />
          </div>
        </Col>
      </Row>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    module: state.module,
    config: state.config,
  };
};

export default connect(mapStateToProps)(ModuleUser);
