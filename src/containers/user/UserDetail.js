// import external libs
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { get } from 'lodash';
import { Row, Col, Avatar, Button, Tabs } from 'antd';
import { TeamOutlined, TagsOutlined, MailOutlined, PhoneOutlined, SolutionOutlined, UsergroupAddOutlined, UserAddOutlined, StarFilled } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
// import internal libs
import { config } from 'config';
import * as userAction from 'redux/actions/user';
import * as logAction from 'redux/actions/log';
import { getLangText } from 'helpers/language.helper';
import userImage from 'assets/images/defaultUser.jpg';
import App from 'App';
import Loading from 'components/loading/Loading';
import * as moduleAction from '../../redux/actions/moduleApp';
import * as statisticAction from 'redux/actions/statistic';
import queryString from 'query-string';
import constants from '../../constants/global.constants';
import { shouldHaveAccessPermission, buildUrl, getUserInformation } from '../../helpers/common.helper';
import W5dImageMinIO from 'components/W5dImageMinIO';
import ChartUser from './components/ChartUser';
import ModuleUser from './components/ModuleUser';
import ModuleFormUser from './components/ModuleFormUser';

import './UserDetail.css';

const { TabPane } = Tabs;

const dateFormat = 'DD-MM-YYYY';
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_MODULE;
function UserDetail(props) {
  const history = useHistory();
  const inforUser = getUserInformation();
  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.module.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });

  const [module, setModule] = useState({});
  const [subTab, setSubTab] = useState(sessionStorage.getItem('module_sub_tab'));

  const searchData = (module_id) => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.module.list.search,
      ...urlQuery,
      module_id,
      filter_key: 'benh_nhan_id',
      filter_value: props.match.params.id,
    };
    props.dispatch(moduleAction.getModules(searchParams));
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const changeTab = (module_id) => {
    sessionStorage.setItem('module_main_tab', module_id);
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      if (module_id) {
        const currentModule = data.modules.find((module) => module.module_id === module_id);
        setModule(currentModule);
        let url = props.location.pathname;
        history.push(url);
      }
    }
  };
  const filterGender = () => {
    const gender = get(props, 'user.item.result.data.gioi_tinh', '');
    if (gender === 1) {
      return 'Nam';
    } else return 'Nữ';
  };

  const bloodPressureStatistic = get(props, 'statistic.latest.result.data.dtdcc_huyetap', []);
  const weightStatistic = get(props, 'statistic.latest.result.data.dtdcc_cannang', []);
  const heightStatistic = get(props, 'statistic.latest.result.data.dtdcc_chieucao', []);
  const bloodSugarStatistic = get(props, 'statistic.edge.result.data', []);

  const bloodSugarData = [bloodSugarStatistic.average, bloodSugarStatistic.max, bloodSugarStatistic.min];
  const [average, max, min] = bloodSugarData;
  const averageFixed = average ? average.toFixed(1) : '--';

  const weightStatisticData = weightStatistic.gia_tri !== undefined ? weightStatistic.gia_tri : 0;
  const heightBMI = heightStatistic.gia_tri !== undefined ? heightStatistic.gia_tri / 100 : 0;

  const bmiData = weightStatisticData && heightBMI ? (weightStatisticData / (heightBMI * heightBMI)).toFixed(2) : '---';


  useEffect(() => {
    props.dispatch(userAction.getUserCates());
    if (props.match.params.id) {
      props.dispatch(
        userAction.getUser({
          nhan_vien_id: props.match.params.id,
        })
      );
      props.dispatch(
        logAction.getLogs({
          nhan_vien_id: props.match.params.id,
        })
      );
    }
  }, []);
  useEffect(() => {
    const configList = get(props, 'config.list.result.data', []);
    const config = configList.find((cfg) => cfg.ma_cau_hinh === CONFIG_KEY);
    if (config) {
      const data = JSON.parse(config.du_lieu);
      if (subTab) {
        const currentModule = data.modules.find((module) => module.module_id === subTab);
        if (currentModule) {
          setModule(currentModule);
          searchData(currentModule.module_id);
        }
      }
    }
  }, [props.config.list.result.data, props.location]);

  return (
    <App>
      <Helmet>
        <title>{getLangText('USER.USER_PROFILE')}</title>
      </Helmet>
      {(props.user.item.loading || props.module.list.loading) && <Loading />}
      <Row className="app-main detail-user-page">
        <Col span={24} className="body-content">
          <Row gutter={25}>
            <Col span={24}>
              <h2 className="header-form-title">{getLangText('USER.USER_PROFILE')}</h2>
            </Col>
            <Col xl={6} md={6} xs={24} className="profile-left">
              <div className="border-box-widget">
                <h2>
                  {getLangText('USER.BASIC_INFORMATION')}{' '}
                  {get(props, 'user.item.result.data.ma_gioi_thieu', '') === inforUser.ten_tai_khoan && <StarFilled title="Bệnh nhân của tôi" className="my-patient" />}
                </h2>
                <div className="border-box-body profile-basic">
                  <Avatar
                    className="my-avatar"
                    shape="square"
                    src={
                      props.user.item.result && props.user.item.result.data && props.user.item.result.data.anh_dai_dien ? (
                        <W5dImageMinIO image={props.user.item.result.data.anh_dai_dien} size={200} />
                      ) : (
                        userImage
                      )
                    }
                  />

                  <h1>{get(props, 'user.item.result.data.ten_nhan_vien', '')}</h1>
                  <Link to={props.user.item.result && props.user.item.result.data ? `/user/edit/${props.user.item.result.data.nhan_vien_id}` : ''}>
                    <Button shape="round" type="danger" block>
                      {getLangText('USER.EDIT_USER')}
                    </Button>
                  </Link>

                  <div className="profile-basic-info">
                    <div className="info-item">
                      <div>
                        <TeamOutlined /> Nhóm tài khoản
                      </div>
                      <p className="info-value">{get(props, 'user.item.result.data.nhom.ten_nhom', '')}</p>
                    </div>
                    <div className="info-item">
                      <div>
                        <UserAddOutlined /> Bác sĩ quản lý
                      </div>
                      <p className="info-value">{get(props, 'user.item.result.data.ma_gioi_thieu', '')}</p>
                    </div>

                    <div className="info-item">
                      <div>
                        <UsergroupAddOutlined /> Giới tính
                      </div>
                      <p className="info-value">{filterGender()}</p>
                    </div>

                    <div className="info-item">
                      <div>
                        <PhoneOutlined /> Số điện thoại
                      </div>
                      <p className="info-value">{get(props, 'user.item.result.data.so_dien_thoai', '')}</p>
                    </div>

                    <div className="info-item">
                      <div>
                        <MailOutlined /> E-mail
                      </div>
                      <p className="info-value">{get(props, 'user.item.result.data.email', '')}</p>
                    </div>
                    <div className="info-item">
                      <div>
                        <TagsOutlined /> Địa chỉ
                      </div>
                      <p className="info-value">{get(props, 'user.item.result.data.dia_chi', '')}</p>
                    </div>
                    <div className="info-item">
                      <div>
                        <SolutionOutlined /> Giới thiệu
                      </div>
                      <p
                        className="introduction"
                        dangerouslySetInnerHTML={{
                          __html: props.user.item.result && props.user.item.result.data ? ` ${props.user.item.result.data.gioi_thieu}` : '',
                        }}
                      ></p>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col xl={18} md={18} xs={24} className="profile-right">
              <div className="border-box-widget profile-more">
                <div className="border-box-body">
                  {get(props, 'user.item.result.data.nhom.ma_nhom', '') === 'PATIENT_GROUP' && (
                    <>
                      <Tabs
                        defaultActiveKey="ibm"
                        activeKey={subTab}
                        onChange={(activeKey) => {
                          setSubTab(activeKey);
                          changeTab(activeKey);
                          sessionStorage.setItem('module_sub_tab', activeKey);
                          let url = props.location.pathname;
                          history.push(url);
                        }}
                      >
                        <TabPane tab="Tổng quan" key="ibm">
                          <div className="timelines">
                            <div className="overview-items">
                              <h3>Chỉ số BMI của bệnh nhân là </h3>
                              <h3 className="overview-bold">{`${bmiData} kg/m2`}</h3>
                            </div>
                            {bmiData < 16 ? <h3 className="overview-bold">Chỉ số BMI ở trên cho thấy bệnh nhân gầy độ III</h3> : ''}
                            {bmiData >= 17 && bmiData < 18.5 ? <h3 className="overview-bold">Chỉ số BMI ở trên cho thấy bệnh nhân gầy độ I</h3> : ''}
                            {bmiData > 18.5 && bmiData < 24.9 ? <h3 className="overview-bold">Chỉ số BMI ở trên cho thấy bệnh nhân bình thường</h3> : ''}
                            {bmiData > 25 && bmiData < 30 ? <h3 className="overview-bold">Chỉ số BMI ở trên cho thấy bệnh nhân đang thừa cân</h3> : ''}
                            {bmiData > 30 && bmiData < 34.9 ? <h3 className="overview-bold">Chỉ số BMI ở trên cho thấy bệnh nhân đang béo phì</h3> : ''}
                            <div className="overview-items">
                              <h3>Chỉ số Huyết áp tâm trương </h3>
                              <h3 className="overview-bold">{`${bloodPressureStatistic && bloodPressureStatistic.tam_truong !== undefined ? bloodPressureStatistic.tam_truong : '---'} mmHg`}</h3>
                            </div>
                            <div className="overview-items">
                              <h3>Chỉ số Huyết áp tâm thu </h3>
                              <h3 className="overview-bold">{`${bloodPressureStatistic && bloodPressureStatistic.tam_thu !== undefined ? bloodPressureStatistic.tam_thu : '---'} mmHg`}</h3>
                            </div>
                            <div className="overview-items">
                              <h3>Chỉ số Đường huyết trung bình </h3>
                              <h3 className="overview-bold">{`${bloodSugarStatistic && bloodSugarStatistic.average !== null ? averageFixed : '---'} mmol/l`}</h3>
                            </div>
                            <div className="overview-items">
                              <h3>Chỉ số Đường huyết lớn nhất </h3>
                              <h3 className="overview-bold">{`${bloodSugarStatistic && bloodSugarStatistic.max !== null ? max : '---'} mmol/l`}</h3>
                            </div>
                            <div className="overview-items">
                              <h3>Chỉ số Đường huyết nhỏ nhất </h3>
                              <h3 className="overview-bold">{`${bloodSugarStatistic && bloodSugarStatistic.min !== null ? min : '---'} mmol/l`}</h3>
                            </div>
                          </div>
                        </TabPane>
                        <TabPane tab="Đường huyết" key="glucose">
                          <div className="chart-user">
                            <ChartUser module_id={'glucose'} user_id={props.match.params.id} subTab={subTab} />
                            <ModuleUser module_id={'glucose'} user_id={props.match.params.id} subTab={subTab} />
                          </div>
                        </TabPane>
                        <TabPane tab="Huyết áp" key="pressure">
                          <div className="chart-user">
                            <ChartUser module_id={'pressure'} user_id={props.match.params.id} subTab={subTab} />
                            <ModuleUser module_id={'pressure'} user_id={props.match.params.id} subTab={subTab} />
                          </div>
                        </TabPane>
                        <TabPane tab="Thuốc" key="drugs">
                          <ModuleUser module_id={'drugs'} user_id={props.match.params.id} subTab={subTab} />
                        </TabPane>
                        <TabPane tab="Xét nghiệm" key="tests">
                          <ModuleUser module_id={'tests'} user_id={props.match.params.id} subTab={subTab} />
                        </TabPane>
                        <TabPane tab="Bệnh lý" key="pathological">
                          <ModuleFormUser module_id={'pathological'} user_id={props.match.params.id} subTab={subTab} moduleConfig={module} isEdit={true} />
                        </TabPane>
                        <TabPane tab="Tiền sử ĐTĐ" key="patient">
                          <ModuleFormUser module_id={'patient'} user_id={props.match.params.id} subTab={subTab} moduleConfig={module} isEdit={true} />
                        </TabPane>
                        <TabPane tab="Tiền sử gia đình" key="family">
                          <ModuleFormUser module_id={'family'} user_id={props.match.params.id} subTab={subTab} moduleConfig={module} isEdit={true} />
                        </TabPane>
                        <TabPane tab="Căn nặng" key="weight">
                          <div className="chart-user">
                            <ChartUser module_id={'weight'} user_id={props.match.params.id} subTab={subTab} />
                            <ModuleUser module_id={'weight'} user_id={props.match.params.id} subTab={subTab} />
                          </div>
                        </TabPane>
                        <TabPane tab="Chiều cao" key="height">
                          <div className="chart-user">
                            <ModuleUser module_id={'height'} user_id={props.match.params.id} subTab={subTab} />
                          </div>
                        </TabPane>
                      </Tabs>
                    </>
                  )}
                </div>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    log: state.log,
    config: state.config,
    module: state.module,
    statistic: state.statistic,
  };
};

export default connect(mapStateToProps)(UserDetail);
