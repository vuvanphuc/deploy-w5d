// import external libs
import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import { useHistory, useLocation } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
// import InfiniteScroll from 'react-infinite-scroller';
import { get } from 'lodash';
import { Row, Col, List, Tag , Button, Checkbox, notification, Menu, Dropdown, Space, Modal, Upload } from 'antd';
import { DownOutlined, PlusOutlined, LockOutlined, DeleteOutlined, UnlockOutlined, QuestionCircleOutlined, DownloadOutlined, UploadOutlined, StarFilled } from '@ant-design/icons';
import { io as socketIOClient } from 'socket.io-client';

// import internal libs
import * as userAction from 'redux/actions/user';
import { getLangText } from 'helpers/language.helper';
import { shouldHaveAccessPermission, buildUrl, getUserInformation } from 'helpers/common.helper';
import { config } from 'config';
import userDefaultImage from 'assets/images/defaultUser.jpg';
import templateFile from 'assets/templates/User_temp.xlsx';
import App from 'App';
import constants from '../../constants/global.constants';
import AppFilter from 'components/AppFilter';
import W5dStatusTag from 'components/status/W5dStatusTag';
import W5dImageMinIO from 'components/W5dImageMinIO';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import { ExportToExcel } from 'helpers/ExportToExcel';

const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';
const oldKeys = ['Tên thành viên', 'Tài khoản'];
const newKeys = ['ten_nhan_vien', 'ten_tai_khoan'];

function User(props) {
  const inforUser = getUserInformation();
  const history = useHistory();
  const location = useLocation();
  let groupCode = '';
  if (location.pathname.includes('users/patients')) {
    groupCode = 'PATIENT_GROUP';
  } else if (location.pathname.includes('users/doctors')) {
    groupCode = 'DOCTOR_GROUP';
  }
  const urlQuery = queryString.parse(props.location.search);
  urlQuery.groupCode = groupCode;
  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };

  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
      checkAll: 0,
      checkedList: [],
    }));
    changeUrlParams(field, value);
  };
  const onDelete = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Block thành viên thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa thành viên thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(userAction.deleteUser({ id }, callback));
      },
    });
  };

  const allOptions = get(props, 'user.list.result.data', []).map((item) => item.nhan_vien_id);
  const onCheckAllChange = (e) => {
    setState({
      ...state,
      checkedList: e.target.checked ? allOptions.filter((nhan_vien_id) => nhan_vien_id !== 'superuserA') : [],
      checkAll: e.target.checked,
    });
  };

  const onChangeCheck = (nhan_vien_id) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(nhan_vien_id) ? state.checkedList.filter((it) => it !== nhan_vien_id) : state.checkedList.concat([nhan_vien_id]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length - 1,
      };
    });
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    search: {
      ...props.user.list.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
  });

  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  function deleteListUsers(e) {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Xóa nhiều thành viên thành công.',
        });
      }
    };
    confirm({
      title: 'Bạn chắc chắn muốn xóa các thành viên được chọn?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(userAction.deleteUsers({ listIds: state.checkedList }, callback));
      },
    });
  }

  //Active thanh vien theo list => active
  function activeListUsers(e) {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Kích hoạt thành viên thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn có chắc chắn muốn kích hoạt các thành viên được chọn?',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(userAction.editStatusUser({ listIds: state.checkedList, status: 'active' }, callback));
        },
      });
    }
  }

  //Khoa thanh vien theo list => inactive
  function blockListUsers(e) {
    const callback = (res) => {
      if (res.success) {
        notification.success({
          message: 'Khóa thành viên thành công.',
        });
      }
    };
    if (state.checkedList.length > 0) {
      confirm({
        title: 'Bạn chắc chắn muốn khóa những thành viên được chọn',
        icon: <QuestionCircleOutlined />,
        content: '',
        onOk() {
          props.dispatch(
            userAction.editStatusUser(
              {
                listIds: state.checkedList,
                status: 'inactive',
              },
              callback
            )
          );
        },
      });
    }
  }

  //lam dropdown button
  const menu = (
    <Menu>
      <Menu.Item key="1" icon={<DeleteOutlined />} onClick={deleteListUsers} disabled={!shouldHaveAccessPermission('nhan_vien', 'nhan_vien/xoa') || state.checkedList.length <= 0}>
        Xóa thành viên
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={activeListUsers}
        disabled={urlQuery.status === 'active' || !shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') || state.checkedList.length <= 0}
      >
        Kích hoạt thành viên
      </Menu.Item>
      <Menu.Item
        key="3"
        icon={<LockOutlined />}
        onClick={blockListUsers}
        disabled={urlQuery.status === 'inactive' || !shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') || state.checkedList.length <= 0}
      >
        Khóa thành viên
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    urlQuery.groupCode = groupCode;
    const searchParams = {
      ...props.user.list.search,
      ...urlQuery,
      loading: true,
    };
    props.dispatch(userAction.getUsers(searchParams));
  }, [props.location]);

  const findCate = (id) => {
    const cates = get(props, 'user.listCates.result.data', []);
    const cate = cates.find((ct) => ct.nhom_nhan_vien_id === id);
    if (get(cate, 'ma_nhom', '') === 'STUDENT_GROUP') return <Tag color="#2db7f5">{cate.ten_nhom}</Tag>;
    else if (get(cate, 'ma_nhom', '') === 'TEACHER_GROUP') return <Tag color="#d3adf7">{cate.ten_nhom}</Tag>;
    return <Tag color="#f50">{cate?.ten_nhom}</Tag>;
  };
  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    urlQuery.groupCode = groupCode;
    const searchParams = {
      ...props.user.list.search,
      ...urlQuery,
      loading: false,
    };
    props.dispatch(userAction.getUserCates());
    const socket = socketIOClient(config.API_URL, config.SOCKET_CONFIG);
    socket.on('event://users-changed', () => {
      props.dispatch(userAction.getUsers(searchParams));
    });
  }, []);
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  const saveFile = () => {
    FileSaver.saveAs(templateFile, 'tempUser.xlsx');
  };

  function processExcel(data) {
    const workbook = XLSX.read(data, { type: 'binary' });
    const firstSheet = workbook.SheetNames[0];
    const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    const onSuccess = () => {
      notification.success({
        message: 'Import biểu mẫu thành công',
      });
    };
    const onError = () => {
      const urlQuery = queryString.parse(props.location.search);
      urlQuery.groupCode = groupCode;
      const searchParams = {
        ...props.user.list.search,
        ...urlQuery,
        loading: false,
      };

      props.dispatch(userAction.getUsers(searchParams));
    };
    props.dispatch(userAction.importUsers({ listUsers: excelRows }, onSuccess, onError));
  }

  const customRequest = async ({ file }) => {
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      if (reader.readAsBinaryString) {
        reader.onload = (e) => {
          processExcel(reader.result);
        };
        reader.readAsBinaryString(file);
      }
    } else {
      notification.error({
        message: 'Không đọc được file',
      });
    }
  };

  const menuImport = (
    <Menu>
      <Menu.Item key="1" icon={<DownloadOutlined />} onClick={saveFile}>
        Tải xuống file mẫu
      </Menu.Item>
      <Menu.Item key="2" icon={<UploadOutlined />}>
        <Upload id="uploadFile" multiple={false} accept=".xls,.xlsx" showUploadList={false} customRequest={(data) => customRequest(data)}>
          Đẩy lên dữ liệu
        </Upload>
      </Menu.Item>
    </Menu>
  );
  return (
    <App>
      <Helmet>
        <title>Quản lý nhân viên</title>
      </Helmet>
      {props.user.list.loading && <Loading />}
      <Row className="app-main">
        <Col xl={24} className="body-content">
          <Row>
            <Col xl={24} sm={24} xs={24}>
              <AppFilter
                title={getLangText('USER.USERS')}
                isShowCategories={true}
                isDisableCategories={groupCode !== ''}
                isShowStatus={true}
                isShowSearchBox={true}
                isShowDatePicker={true}
                isRangeDatePicker={true}
                categories={get(props, 'user.listCates.result.data', []).map((cate) => ({
                  name: cate.ten_nhom,
                  id: cate.nhom_nhan_vien_id,
                }))}
                search={state.search}
                onDateChange={(dates) => onDateChange(dates)}
                onFilterChange={(field, value) => onFilterChange(field, value)}
              />
            </Col>
          </Row>
          <Row className="select-action-group" gutter={[8, 8]}>
            <Col xl={12} sm={12} xs={24}>
              <Space wrap>
                <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                  <Button>
                    Chọn hành động
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
            </Col>
            <Col xl={12} sm={12} xs={24} className="right-actions">
              {/* <Space wrap>
                <Dropdown overlay={menuImport} trigger="click" className="btn-action">
                  <Button>
                    Import file dữ liệu
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </Space>
              <ExportToExcel
                apiData={get(props, 'user.list.result.data', [])
                  .map((item) => {
                    return {
                      'Mã nhân viên': item.nhan_vien_id,
                      'Tên thành viên': item.ten_nhan_vien,
                      'Tên tài khoản': item.ten_tai_khoan,
                      'Chức vụ': item.chuc_vu,
                      'Đơn vị': item.don_vi,
                      'Mã nhóm': item.nhom_nhan_vien_id,
                      'Tên nhóm': findCate(item.nhom_nhan_vien_id),
                      Email: item.email,
                      'Số điện thoại': item.so_dien_thoai,
                      'Địa chỉ': item.dia_chi,
                      'Giới thiệu': item.gioi_thieu,
                      'Ngày sinh': moment(item.ngay_sinh).utc(0).format(config.DATE_FORMAT),
                      'Ngày tạo': moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT),
                    };
                  })
                  .sort((a, b) => (a.Nhom > b.Nhom ? 1 : -1))}
                fileName="DanhSach"
              /> */}
              {shouldHaveAccessPermission('nhan_vien', 'nhan_vien/them') && (
                <Link to="/user/add">
                  <Button shape="round" type="primary" icon={<PlusOutlined />} className=" btn-action">
                    {getLangText('USER.ADD_NEW_USER')}
                  </Button>
                </Link>
              )}
            </Col>
          </Row>

          <div className="w5d-list">
            <List
              locale={{ emptyText: getLangText('GLOBAL.NO_ITEMS') }}
              header={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={2} xl={2}>
                    {getLangText('GLOBAL.AVATAR')}
                  </Col>
                  <Col xs={5} xl={5} className="text-left">
                    {getLangText('GLOBAL.EMAIL')}
                    <Sorting urlQuery={urlQuery} field={'email'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-center">
                    {getLangText('GLOBAL.GROUP_USER')}
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    {getLangText('GLOBAL.USERNAME')}
                    <Sorting urlQuery={urlQuery} field={'ten_tai_khoan'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.FULLNAME')}
                    <Sorting urlQuery={urlQuery} field={'ten_nhan_vien'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              footer={
                <Row>
                  <Col xs={1} xl={1}>
                    <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                  </Col>
                  <Col xs={2} xl={2}>
                    {getLangText('GLOBAL.AVATAR')}
                  </Col>
                  <Col xs={5} xl={5} className="text-left">
                    {getLangText('GLOBAL.EMAIL')}
                    <Sorting urlQuery={urlQuery} field={'email'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-center">
                    {getLangText('GLOBAL.GROUP_USER')}
                    <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={4} xl={4} className="text-left">
                    {getLangText('GLOBAL.USERNAME')}
                    <Sorting urlQuery={urlQuery} field={'ten_tai_khoan'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.FULLNAME')}
                    <Sorting urlQuery={urlQuery} field={'ten_nhan_vien'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.STATUS')}
                    <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                  <Col xs={3} xl={3} className="text-left">
                    {getLangText('GLOBAL.CREATE_DATE')}
                    <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                  </Col>
                </Row>
              }
              dataSource={get(props, 'user.list.result.data', [])}
              pagination={{
                hideOnSinglePage: false,
                responsive: true,
                showLessItems: true,
                pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                pageSize: pageSize,
                onChange: (page, pageSize) => {
                  changeMutilUrlParams({ pageSize, page });
                  setPageSize(pageSize);
                  setPage(page);
                },
                current: Number(page),
                showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                showSizeChanger: true,
              }}
              renderItem={(item) => (
                <List.Item key={item._id} id="listUsers">
                  <Row className="full">
                    <Col xs={1} xl={1}>
                      {item.nhan_vien_id !== 'superuserA' && <Checkbox checked={state.checkedList.includes(item.nhan_vien_id)} value={item._id} onChange={() => onChangeCheck(item.nhan_vien_id)} />}
                    </Col>
                    <Col xs={2} xl={2}>
                      <Link
                        className="view"
                        to={shouldHaveAccessPermission('nhan_vien', 'nhan_vien/chi_tiet') || item.nhan_vien_id === inforUser.nhan_vien_id ? `/user/detail/${item.nhan_vien_id}` : '#'}
                      >
                        <W5dImageMinIO image={item.anh_dai_dien} />
                      </Link>
                    </Col>
                    <Col xs={5} xl={5} className="text-left">
                      <Link className="edit" to={shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') || item.nhan_vien_id === inforUser.nhan_vien_id ? `/user/edit/${item.nhan_vien_id}` : '#'}>
                        {item.email}
                      </Link>
                      <div className="actions">
                        {(shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') || item.nhan_vien_id === inforUser.nhan_vien_id) && (
                          <Link className="edit act" to={`/user/edit/${item.nhan_vien_id}`}>
                            {getLangText('GLOBAL.EDIT')}
                          </Link>
                        )}
                        {shouldHaveAccessPermission('nhan_vien', 'nhan_vien/xoa') && (
                          <Button
                            className="delete act"
                            type="link"
                            onClick={() => {
                              onDelete(item.nhan_vien_id, item.trang_thai);
                            }}
                          >
                            {item.trang_thai === 'active' ? 'Khóa' : 'Xóa'}
                          </Button>
                        )}
                        {(shouldHaveAccessPermission('nhan_vien', 'nhan_vien/chi_tiet') || item.nhan_vien_id === inforUser.nhan_vien_id) && (
                          <Link className="view act" to={`/user/detail/${item.nhan_vien_id}`}>
                            {getLangText('GLOBAL.VIEW')}
                          </Link>
                        )}
                      </div>
                    </Col>
                    <Col xs={3} xl={3} className="text-center">
                      {findCate(item.nhom_nhan_vien_id)}
                      {item.ma_gioi_thieu === inforUser.ten_tai_khoan && <StarFilled title="Bệnh nhân của tôi" className="my-patient" />}
                    </Col>
                    <Col xs={4} xl={4} className="text-left">
                      {item.ten_tai_khoan}
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      {item.ten_nhan_vien}
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      <W5dStatusTag status={item.trang_thai} />
                    </Col>
                    <Col xs={3} xl={3} className="text-left">
                      {moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT)}
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </div>
        </Col>
      </Row>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};
export default connect(mapStateToProps)(User);
