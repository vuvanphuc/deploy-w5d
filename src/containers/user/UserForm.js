// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { get } from 'lodash';
import moment from 'moment';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { Button, Form, Input, DatePicker, Row, Col, Select, Modal, Switch, Radio, notification } from 'antd';
import { UnorderedListOutlined, PlusOutlined } from '@ant-design/icons';

// import internal libs
import App from 'App';
import { config } from 'config';
import constants from 'constants/global.constants';
import { getLangText } from 'helpers/language.helper';
import { getUserInformation, shouldHaveAccessPermission, recursiveUserCates } from 'helpers/common.helper';
import * as userAction from 'redux/actions/user';
import W5dMediaLibrary from 'components/W5dMediaLibrary';
import W5dMediaBrowser from 'components/W5dMediaBrowser';
import Loading from 'components/loading/Loading';
import defaultImage from 'assets/images/default.jpg';

const { Option } = Select;
const { TextArea } = Input;

function UserForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const userInfo = getUserInformation();

  const defaultUser = {
    ten_nhan_vien: '',
    ten_tai_khoan: '',
    email: '',
    so_dien_thoai: '',
    anh_dai_dien: '',
    mat_khau: '',
    chuc_vu: '',
    don_vi: '',
    nhom_nhan_vien_id: '',
    trang_thai: 'active',
    dia_chi: '',
    ngay_sinh: '',
    gioi_thieu: '',
    gioi_tinh: 1,
    ma_gioi_thieu: '',
  };

  const defaultForm = {
    ...defaultUser,
    uploadLoading: false,
    isChanged: false,
    isChangePass: false,
    openMediaLibrary: false,
    insertMedia: {
      open: false,
      editor: null,
    },
  };

  const isEdit = props.match && props.match.params && props.match.params.id;
  const openMediaLibrary = () => setState((state) => ({ ...state, openMediaLibrary: true }));
  const handleChangeDate = (date) => setState({ ...state, ngay_sinh: date, isChanged: true });

  const handleSubmit = (values) => {
    if (props.match.params.id) {
      // edit a user
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: getLangText('MESSAGE.UPDATE_USER_SUCCESS'),
          });
          if (userInfo.nhan_vien_id === res.data.nhan_vien_id) {
            props.dispatch(
              userAction.getUser({
                nhan_vien_id: userInfo.nhan_vien_id,
                isUpdateStorage: true,
              })
            );
          }
          form.resetFields();
          setState((state) => ({
            ...state,
            isChanged: false,
            isChangePass: false,
          }));
          if (isEdit) {
            props.dispatch(
              userAction.getUser({
                nhan_vien_id: props.match.params.id,
              })
            );
          }
        }
      };
      const newFormData = {
        ...state,
        ...values,
        password: values.newPassword,
      };
      props.dispatch(userAction.editUser(newFormData, callback));
    } else {
      //add a new user
      const callback = (res) => {
        if (res.success) {
          notification.success({
            message: getLangText('MESSAGE.CREATE_USER_SUCCESS'),
          });
          setState((state) => ({
            ...state,
            ...values,
            isChanged: false,
          }));
          const routeLink = shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') ? `/user/edit/${res.data.nhan_vien_id}` : '/users';
          history.push(routeLink);
        } else {
          notification.error({
            message: res.error,
          });
        }
      };
      props.dispatch(userAction.createUser({ ...state, ...values, nhom_nhan_vien_id: values.nhom_nhan_vien_id ? values.nhom_nhan_vien_id : 'LiXNoEiT4' }, callback));
    }
  };

  const showField = (field) => (field ? field : '');

  const removeImage = () => setState((state) => ({ ...state, anh_dai_dien: '' }));

  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('mat_khau_moi')) {
      callback(getLangText('MESSAGE.PASSWORD_NOT_MATCH'));
    } else {
      callback();
    }
  };

  const validateToNextPassword = (rule, value, callback) => {
    if (value && state.confirmDirty) {
      form.validateFields(['confirm-pass'], { force: true });
    }
    callback();
  };

  const renderUserCategories = () => {
    let options = [];
    const userCates = get(props, 'user.listCates.result.data', []);
    if (userCates) {
      const cates = recursiveUserCates(userCates, userCates);
      options = cates.map((cate) => (
        <Option key={cate.nhom_nhan_vien_id} value={cate.nhom_nhan_vien_id}>
          {cate.titleLevel}
        </Option>
      ));
      return (
        <Select
          showSearch={false}
          value={state.nhom_nhan_vien_id}
          loading={props.user.listCates.loading}
          onChange={(nhom_nhan_vien_id) => setState({ ...state, nhom_nhan_vien_id, isChanged: true })}
          placeholder={getLangText('GLOBAL.SELECT_CATEGORIES')}
        >
          {options}
        </Select>
      );
    }
  };

  const renderDoctors = () => {
    let options = [];
    const userCates = get(props, 'user.list.result.data', []);
    if (userCates) {
      const cates = userCates;
      options = cates.map((cate) => (
        <Option key={cate.nhan_vien_id} value={cate.ten_tai_khoan}>
          {cate.ten_tai_khoan} - {cate.ten_nhan_vien}
        </Option>
      ));
      return (
        <Select
          disabled={get(userInfo, 'ma_nhom_nhan_vien', '') === 'PATIENT_GROUP'}
          showSearch={false}
          value={state.ma_gioi_thieu}
          loading={props.user.list.loading}
          onChange={(ma_gioi_thieu) => setState({ ...state, ma_gioi_thieu, isChanged: true })}
          placeholder="Chọn bác sĩ"
        >
          {options}
        </Select>
      );
    }
  };

  const renderUserStatus = () => {
    const options = constants.COMMON_STATUS.map((status) => (
      <Option key={status.value} value={status.value}>
        {status.title}
      </Option>
    ));
    return (
      <Select value={state.trang_thai} onChange={(trang_thai) => setState({ ...state, trang_thai, isChanged: true })} placeholder={getLangText('GLOBAL.SELECT_STATUS')}>
        {options}
      </Select>
    );
  };

  const [state, setState] = useState(defaultForm);

  const onSelectImage = (file) => {
    setState((state) => ({
      ...state,
      openMediaLibrary: false,
      isChanged: true,
      anh_dai_dien: file.tep_tin_url,
    }));
  };

  const onInsertImage = (file) => {
    state.insertMedia.editor.insertContent('<img className="image-editor" src="' + `${config.UPLOAD_API_URL}/${file.imgUrl}` + '"/>');
    setState((state) => ({
      ...state,
      isChanged: true,
      insertMedia: {
        open: false,
      },
    }));
  };

  const onResetForm = () => {
    if (props.user.item.result && isEdit) {
      form.setFieldsValue(props.user.item.result.data);
      setState((state) => ({ ...state, ...props.user.item.result.data }));
    } else {
      form.resetFields();
      setState((state) => ({ ...state, ...defaultForm }));
    }
  };

  useEffect(() => {
    props.dispatch(userAction.getUserCates());
    if (isEdit) {
      props.dispatch(
        userAction.getUser({
          nhan_vien_id: props.match.params.id,
        })
      );
      props.dispatch(
        userAction.getUsers({
          groupCode: 'DOCTOR_GROUP',
        })
      );
    } else {
      form.setFieldsValue(defaultUser);
    }
    return () => (window.onbeforeunload = null);
  }, []);

  useEffect(() => {
    if (isEdit && props.user.item.result) {
      form.setFieldsValue(props.user.item.result.data);
      setState((state) => ({ ...state, ...props.user.item.result.data }));
    }
    window.onbeforeunload = null;
  }, [props.user.item.result]);

  return (
    <App>
      <Helmet>
        <title>{isEdit ? `${getLangText('USER.EDIT_USER')}: ${showField(state.fullname)}` : getLangText('USER.ADD_NEW_USER')}</title>
      </Helmet>
      {props.user.item.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <div className="w5d-form">
            <Form layout="vertical" className="" onFinish={handleSubmit} form={form} initialValues={defaultUser}>
              <Row gutter={25}>
                <Col xl={18} sm={16} xs={24}>
                  <h2 className="header-form-title">{isEdit ? getLangText('USER.EDIT_USER') : getLangText('USER.ADD_NEW_USER')}</h2>
                </Col>
                <Col xl={6} sm={8} xs={24}>
                  <Col xl={4} sm={24} xs={24}></Col>
                  <Col xl={20} sm={24} xs={24}>
                    <Link to={isEdit ? '/user/add' : '/users'}>
                      <Button block shape="round" type="primary" icon={isEdit ? <PlusOutlined /> : <UnorderedListOutlined />} className="btn-create-todo">
                        {isEdit ? getLangText('USER.ADD_NEW_USER') : getLangText('USER.LIST_USERS')}
                      </Button>
                    </Link>
                  </Col>
                </Col>
                <Col xl={18} sm={24} xs={24} className="left-content">
                  <div className="border-box">
                    <Row gutter={25}>
                      <Col xl={12} sm={12} xs={24}>
                        <Form.Item
                          className="input-col"
                          label={getLangText('GLOBAL.FULLNAME')}
                          name="ten_nhan_vien"
                          rules={[
                            {
                              required: true,
                              message: getLangText('GLOBAL.FULLNAME_REQUIRE'),
                            },
                          ]}
                        >
                          <Input
                            placeholder={getLangText('GLOBAL.FULLNAME')}
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                        <Form.Item
                          className="input-col"
                          label={getLangText('GLOBAL.USERNAME')}
                          name="ten_tai_khoan"
                          rules={[
                            {
                              required: true,
                              message: getLangText('GLOBAL.USERNAME_REQUIRE'),
                            },
                          ]}
                        >
                          <Input
                            disabled={isEdit}
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                            placeholder={getLangText('GLOBAL.USERNAME')}
                          />
                        </Form.Item>
                        <Form.Item
                          className="input-col"
                          label={getLangText('GLOBAL.EMAIL')}
                          name="email"
                          rules={[
                            {
                              required: true,
                              message: getLangText('GLOBAL.EMAIL_REQUIRE'),
                            },
                            { type: 'email', message: getLangText('GLOBAL.EMAIL_VALIDATE') },
                          ]}
                        >
                          <Input
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                            placeholder={getLangText('GLOBAL.EMAIL')}
                          />
                        </Form.Item>
                        <Form.Item className="input-col" label={getLangText('GLOBAL.PHONE')} name="so_dien_thoai" rules={[]}>
                          <Input
                            placeholder={getLangText('GLOBAL.PHONE')}
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>
                      <Col xl={12} sm={12} xs={24}>
                        <Form.Item className="input-col" label="Nơi làm việc" name="don_vi" rules={[]}>
                          <Input
                            placeholder="Nhập nơi làm việc"
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                        <Form.Item className="input-col" label="Công việc" name="chuc_vu" rules={[]}>
                          <Input
                            placeholder="Nhập công việc"
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                        <Form.Item className="input-col" label={getLangText('GLOBAL.ADDRESS')} name="dia_chi" rules={[]}>
                          <Input placeholder={getLangText('GLOBAL.ADDRESS')} />
                        </Form.Item>
                        <Form.Item className="input-col" label="Giới tính" name="gioi_tinh" rules={[]}>
                          <Radio.Group
                            options={[
                              { label: 'Nam', value: 1 },
                              { label: 'Nữ', value: 0 },
                            ]}
                            optionType="button"
                            buttonStyle="solid"
                          />
                        </Form.Item>
                      </Col>
                    </Row>

                    {isEdit && (
                      <Form.Item name="isChangePass" label="Đổi mật khẩu" valuePropName="checked">
                        <Switch
                          onClick={() => {
                            setState((state) => ({
                              ...state,
                              isChanged: true,
                              isChangePass: !state.isChangePass,
                            }));
                          }}
                        />
                      </Form.Item>
                    )}
                    {isEdit && state.isChangePass && (
                      <>
                        <Form.Item
                          className="input-col"
                          name="mat_khau_moi"
                          label={getLangText('GLOBAL.PASSWORD')}
                          rules={[
                            {
                              required: true,
                              message: getLangText('GLOBAL.PASSWORD_REQUIRE'),
                            },
                            {
                              min: 6,
                              message: getLangText('GLOBAL.PASSWORD_VALIDATE_MIN'),
                            },
                            { validator: validateToNextPassword },
                          ]}
                        >
                          <Input.Password
                            value=""
                            minLength={6}
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                        <Form.Item
                          className="input-col"
                          name="confirm-pass"
                          label={getLangText('GLOBAL.CONFIRM_PASSWORD')}
                          rules={[
                            {
                              required: true,
                              message: getLangText('GLOBAL.CONFIRM_PASSWORD_REQUIRE'),
                            },
                            {
                              validator: compareToFirstPassword,
                            },
                          ]}
                        >
                          <Input.Password
                            onChange={() => {
                              if (!state.isChanged) {
                                setState((state) => ({
                                  ...state,
                                  isChanged: true,
                                }));
                              }
                            }}
                          />
                        </Form.Item>
                      </>
                    )}
                    {!isEdit && (
                      <Form.Item
                        className="input-col"
                        label={getLangText('GLOBAL.PASSWORD')}
                        name="mat_khau"
                        rules={[
                          {
                            required: true,
                            message: getLangText('GLOBAL.PASSWORD_REQUIRE'),
                          },
                          {
                            min: 6,
                            message: getLangText('GLOBAL.PASSWORD_VALIDATE_MIN'),
                          },
                        ]}
                      >
                        <Input.Password
                          onChange={() => {
                            if (!state.isChanged) {
                              setState((state) => ({
                                ...state,
                                isChanged: true,
                              }));
                            }
                          }}
                        />
                      </Form.Item>
                    )}

                    <Form.Item className="input-col" name="gioi_thieu" label={getLangText('GLOBAL.INTRODUCTION')}>
                      <TextArea
                        rows={6}
                        onChange={() => {
                          if (!state.isChanged) {
                            setState((state) => ({
                              ...state,
                              isChanged: true,
                            }));
                          }
                        }}
                        placeholder={getLangText('GLOBAL.USERNAME')}
                      />
                    </Form.Item>
                  </div>
                </Col>
                <Col xl={6} sm={24} xs={24} className="right-content">
                  <div className="box">
                    <div className="box-body">
                      <div className="border-box hideMobile">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              {/* */}
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                      <div className="border-box">
                        <h2>{getLangText('GLOBAL.BIRTHDAY')}</h2>
                        <Form.Item className="input-col">
                          <DatePicker
                            disabledDate={(current) => current && current > moment()}
                            value={state.ngay_sinh ? moment(state.ngay_sinh) : null}
                            locale={locale}
                            showToday={true}
                            style={{ width: '100%' }}
                            onChange={handleChangeDate}
                            format="DD/MM/yyyy"
                            max
                            placeholder={getLangText('GLOBAL.BIRTHDAY')}
                          />
                        </Form.Item>
                      </div>
                      <div
                        className="border-box"
                        style={{
                          display: !shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') ? 'none' : 'block',
                        }}
                      >
                        <h2>{getLangText('GLOBAL.CATEGORIES')}</h2>

                        <Form.Item
                          className="input-col"
                          name="nhom_nhan_vien_id"
                          rules={[
                            {
                              required: true,
                              message: 'Nhóm thành viên là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderUserCategories()}
                        </Form.Item>
                      </div>
                      <div
                        className="border-box"
                        style={{
                          display: !shouldHaveAccessPermission('nhan_vien', 'nhan_vien/sua') ? 'none' : 'block',
                        }}
                      >
                        <h2>{getLangText('GLOBAL.STATUS')}</h2>
                        <Form.Item
                          className="input-col"
                          name="trang_thai"
                          rules={[
                            {
                              required: true,
                              message: 'Trạng thái là trường bắt buộc.',
                            },
                          ]}
                        >
                          {renderUserStatus()}
                        </Form.Item>
                      </div>
                      <W5dMediaBrowser
                        image={state.anh_dai_dien ? `${state.anh_dai_dien}` : ''}
                        field="image"
                        setImageLabel={getLangText('GLOBAL.SET_AVATAR')}
                        removeImageLabel={getLangText('GLOBAL.REMOVE_AVATAR')}
                        title={getLangText('GLOBAL.AVATAR')}
                        openMediaLibrary={() => openMediaLibrary()}
                        removeImage={() => removeImage()}
                      />
                      <div className="border-box hideDesktop">
                        <h2>{getLangText('FORM.PUBLISH')}</h2>
                        <Form.Item className="input-col">
                          <Row>
                            <Col span="11">
                              <Button block type="danger" htmlType="reset" onClick={() => onResetForm()}>
                                {getLangText('FORM.RESET')}
                              </Button>
                            </Col>
                            <Col span="2"></Col>
                            <Col span="11">
                              <Button block type="primary" htmlType="submit">
                                {isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.PUBLISH')}
                              </Button>
                            </Col>
                          </Row>
                        </Form.Item>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>
      <Modal
        wrapClassName="w5d-modal-media-library"
        visible={state.openMediaLibrary}
        onCancel={() =>
          setState((state) => ({
            ...state,
            openMediaLibrary: false,
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary type="single" onSelectImage={(file) => onSelectImage(file)} />
      </Modal>

      <Modal
        wrapClassName="w5d-modal-large"
        visible={state.insertMedia.open}
        onCancel={() =>
          setState((state) => ({
            ...state,
            insertMedia: {
              ...state.insertMedia,
              open: false,
            },
          }))
        }
        footer={null}
      >
        <W5dMediaLibrary onSelectImage={(file) => onInsertImage(file)} />
      </Modal>
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    todo: state.todo,
  };
};

export default connect(mapStateToProps)(UserForm);
