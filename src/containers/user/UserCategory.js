// import external libs
import React, { useState, useEffect } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { get } from 'lodash';
import { Row, Col, Form, Input, List, Button, Checkbox, Select, Modal, notification, Menu, Space, Dropdown, Radio } from 'antd';
import { DownOutlined, DeleteOutlined, QuestionCircleOutlined } from '@ant-design/icons';

// import internal libs
import * as userAction from 'redux/actions/user';
import { config } from 'config';
import constants from 'constants/global.constants';
import { getLangText } from 'helpers/language.helper';
import { getUserInformation, shouldHaveAccessPermission, buildUrl, recursiveUserCates } from 'helpers/common.helper';
import App from 'App';
import AppFilter from 'components/AppFilter';
import W5dPermission from 'components/W5dPermission';
import Loading from 'components/loading/Loading';
import Sorting from 'components/sorting/Sorting';
import W5dStatusTag from '../../components/status/W5dStatusTag';

const { TextArea } = Input;
const { Option } = Select;
const { confirm } = Modal;
const dateFormat = 'DD-MM-YYYY';
const CONFIG_KEY = constants.CONFIG_LIST.CONFIG_MODULE;
const CONFIG_DEDUCE_KEY = constants.CONFIG_LIST.CONFIG_DEDUCE;
const CONFIG_CATEGORY_KEY = constants.CONFIG_LIST.CONFIG_CATEGORY;

function UserCategory(props) {
  const [form] = Form.useForm();
  const history = useHistory();
  const urlQuery = queryString.parse(props.location.search);

  const configProps = get(props, 'config.list.result.data', []);

  const modulesConfig = configProps.find((item) => item.ma_cau_hinh === CONFIG_KEY);
  const modulesData = modulesConfig ? JSON.parse(modulesConfig.du_lieu) : {};

  const deductionConfig = configProps.find((item) => item.ma_cau_hinh === CONFIG_DEDUCE_KEY);
  const deductionData = deductionConfig ? JSON.parse(deductionConfig.du_lieu) : {};

  const categoryConfig = configProps.find((item) => item.ma_cau_hinh === CONFIG_CATEGORY_KEY);
  const categoryData = categoryConfig ? JSON.parse(categoryConfig.du_lieu) : {};

  const defaultForm = {
    ten_nhom: '',
    mo_ta: '',
    nhomnhanviencha_id: '',
    phan_quyen: '',
    ma_nhom: '',
    trang_thai: 'active',
  };

  const [state, setState] = useState({
    checkedList: [],
    checkAll: false,
    isEdit: false,
    isChanged: false,
    openPermission: false,
    form: defaultForm,
    search: {
      ...props.user.listCates.search,
      date: moment(new Date(), 'DD-MM-YYYY').format(dateFormat),
    },
    modules: [],
  });
  const changeMutilUrlParams = (fields) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    const params = { ...urlQuery, ...fields };
    url = buildUrl(url, params);
    history.push(url);
  };

  const onSortChange = (fields) => {
    changeMutilUrlParams(fields);
    setState((state) => ({
      ...state,
      search: { ...state.search, ...fields },
    }));
  };

  const getListCates = () => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.user.listCates.search,
      ...urlQuery,
    };
    props.dispatch(userAction.getUserCates(searchParams));
  };
  const [pageSize, setPageSize] = useState(urlQuery.pageSize ? urlQuery.pageSize : config.PAGE_SIZE);
  const [page, setPage] = useState(urlQuery.page ? urlQuery.page : 1);

  // list functions
  const allOptions = get(props, 'user.listCates.result.data', []).map((item) => item.nhom_nhan_vien_id);

  const onCheckAllChange = (e) => {
    setState((state) => ({
      ...state,
      checkedList: e.target.checked ? allOptions : [],
      checkAll: e.target.checked,
    }));
  };

  const onChangeCheck = (item) => {
    setState((state) => {
      const checkedList = state.checkedList.includes(item) ? state.checkedList.filter((it) => it !== item) : state.checkedList.concat([item]);
      return {
        ...state,
        checkedList,
        checkAll: checkedList.length === allOptions.length,
      };
    });
  };

  const onEditCategory = (item) => {
    setState((state) => ({
      ...state,
      isEdit: true,
      form: item,
    }));
    form.setFieldsValue(item);
  };

  const onOpenPermissionModal = (selectedCategory) => {
    setState((state) => ({
      ...state,
      openPermission: true,
      form: selectedCategory,
      modules: (modulesData ? modulesData.modules : [])
        .map((item) => {
          return {
            name: item.module_name,
            key: item.module_id,
            subs: [
              {
                name: 'Thêm mới',
                key: 'them',
              },
              {
                name: 'Cập nhật',
                key: 'sua',
              },
              {
                name: 'Xem chi tiết',
                key: 'chi_tiet',
              },
              {
                name: 'Xem danh sách',
                key: 'xem',
              },
              {
                name: 'Xem tất cả',
                key: 'xem_tat_ca',
              },
              {
                name: 'Xóa',
                key: 'xoa',
              },
            ],
          };
        })
        .concat(
          (deductionData ? deductionData.modules : []).map((item) => {
            return {
              name: item.module_name,
              key: item.module_id,
              subs: [
                {
                  name: 'Xem danh sách',
                  key: 'xem',
                },
              ],
            };
          })
        )
        .concat(
          (categoryConfig && categoryData ? categoryData.modules : []).map((item) => {
            return {
              name: item.module_name,
              key: item.module_id,
              subs: [
                {
                  name: 'Thêm mới',
                  key: 'them',
                },
                {
                  name: 'Cập nhật',
                  key: 'sua',
                },
                {
                  name: 'Xem danh sách',
                  key: 'xem',
                },
                {
                  name: 'Xóa',
                  key: 'xoa',
                },
              ],
            };
          })
        ),
    }));
  };

  const onDeleteCategory = (id, status) => {
    let msg = '';
    let title = '';
    if (status === 'active') {
      msg = 'Block nhóm thành viên thành công';
      title = 'Bạn chắc chắn muốn khóa không ?';
    } else {
      msg = 'Xóa nhóm thành viên thành công';
      title = 'Bạn chắc chắn muốn xóa không ?';
    }
    const callback = (res) => {
      if (res.success) {
        getListCates();
        notification.success({
          message: msg,
        });
      }
    };
    confirm({
      title: title,
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(userAction.deleteUserCate({ id }, callback));
      },
    });
  };
  const onDeleteCategories = () => {
    const callback = (res) => {
      if (res.success) {
        getListCates();
        setState((state) => ({
          ...state,
          checkedList: [],
        }));
        notification.success({
          message: 'Xóa nhóm thành viên thành công',
        });
      } else {
        notification.error({
          message: res.error,
        });
      }
    };
    confirm({
      title: 'Không thể xóa nhóm thành viên khi có thành viên, bạn chắc chắn xóa những nhóm thành viên được chọn ?',
      icon: <QuestionCircleOutlined />,
      content: '',
      onOk() {
        props.dispatch(userAction.deleteUserCates({ listIds: state.checkedList }, callback));
      },
    });
  };

  const changeUrlParams = (field, value) => {
    const urlQuery = queryString.parse(props.location.search);
    let url = props.location.pathname;
    if (Object.keys(urlQuery).length === 0) {
      url += `?${field}=${value}`;
    } else {
      urlQuery[field] = value;
      url = buildUrl(url, urlQuery);
    }
    if (field === 'date' && value === null) {
      delete urlQuery.date;
      url = buildUrl(props.location.pathname, urlQuery);
    }
    history.push(url);
  };

  const onFilterChange = (field, value) => {
    setState((state) => ({
      ...state,
      search: { ...state.search, [field]: value },
    }));
    changeUrlParams(field, value);
  };

  const shoudlDisableForm = () => {
    if (state.isEdit && shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/sua')) return false;
    if (!state.isEdit && shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/them')) return false;
    return true;
  };

  const handleSubmit = (values) => {
    if (state.isEdit || state.openPermission) {
      // edit a category
      const callback = (result) => {
        notification.success({
          message: getLangText('MESSAGE.UPDATE_USER_CATE_SUCCESS'),
        });
        const userInfo = getUserInformation();
        props.dispatch(
          userAction.getUser({
            nhan_vien_id: userInfo.nhan_vien_id,
            isUpdateStorage: true,
          })
        );
        setState((state) => ({
          ...state,
          form: result.data,
        }));
      };
      props.dispatch(userAction.editUserCate({ ...state.form, ...values }, callback));
    } else {
      //add a new category
      const callback = (res) => {
        if (res.success) {
          getListCates();
          notification.success({
            message: getLangText('MESSAGE.CREATE_USER_CATE_SUCCESS'),
          });
          setState((state) => ({
            ...state,
            isChanged: false,
          }));
        }
      };
      props.dispatch(userAction.createUserCate({ ...state.form, ...values }, callback));
    }
    setState((state) => ({
      ...state,
      form: defaultForm,
      isEdit: false,
      isChanged: false,
    }));
    form.resetFields();
  };

  const renderUserCategories = () => {
    let options = [];
    if (props.user.listCates.result && props.user.listCates.result.data) {
      const cates = recursiveUserCates(get(props, 'user.listCates.result.data', []), get(props, 'user.listCates.result.data', []));
      options = cates.map((cate) => (
        <Option key={cate.nhom_nhan_vien_id} value={cate.nhom_nhan_vien_id}>
          {cate.titleLevel}
        </Option>
      ));
      return (
        <Select
          showSearch={false}
          value={state.nhom_nhan_vien_id}
          loading={props.user.listCates.loading}
          onChange={(nhom_nhan_vien_id) => setState({ ...state, nhom_nhan_vien_id, isChanged: true })}
          placeholder="Chọn nhóm cha"
        >
          <Option value={''}>Root</Option>
          {options}
        </Select>
      );
    }
  };

  const onResetForm = () => {
    setState((state) => ({
      ...state,
      form: defaultForm,
      isEdit: false,
    }));
    form.resetFields();
  };

  if (state.isChanged) {
    window.onbeforeunload = (e) => {
      return getLangText('GLOBAL.CONFIRM_LEAVE_PAGE');
    };
  }

  useEffect(() => {
    form.setFieldsValue(state.form);
  }, []);

  useEffect(() => {
    window.onbeforeunload = null;
  }, [props]);
  useEffect(() => {
    const urlQuery = queryString.parse(props.location.search);
    const searchParams = {
      ...props.user.list.search,
      ...urlQuery,
      loading: true,
    };
    props.dispatch(userAction.getUserCates(searchParams));
  }, [props.location]);
  const menu = (
    <Menu>
      {shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/xoa') && (
        <Menu.Item key="1" disabled={state.checkedList.length === 0 ? true : false} icon={<DeleteOutlined />} onClick={onDeleteCategories}>
          Xóa danh mục thành viên
        </Menu.Item>
      )}
    </Menu>
  );
  const onDateChange = (dates = []) => {
    changeMutilUrlParams({
      startDate: dates && dates[0] ? moment(dates[0]).format(dateFormat) : '',
      endDate: dates && dates[1] ? moment(dates[1]).format(dateFormat) : '',
      page: 1,
    });
  };
  return (
    <App>
      <Helmet>
        <title>{getLangText('USER.USERS_CATEGORIES')}</title>
      </Helmet>
      {props.user.listCates.loading && <Loading />}
      <Row className="app-main">
        <Col span={24} className="body-content">
          <Row>
            <Col xl={6} sm={24} xs={24} className="cate-form-block">
              <h2>{getLangText('USER.ADD_NEW_CATEGORY')}</h2>
              <Form layout="vertical" className="category-form" form={form} onFinish={handleSubmit}>
                <Form.Item
                  className="input-col"
                  label={getLangText('GLOBAL.TITLE')}
                  name="ten_nhom"
                  rules={[
                    {
                      required: true,
                      message: getLangText('GLOBAL.TITLE_REQUIRE'),
                    },
                  ]}
                >
                  <Input
                    disabled={shoudlDisableForm()}
                    placeholder={getLangText('GLOBAL.TITLE')}
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item label="Mã nhóm" className="input-col" name="ma_nhom" rules={[{}]}>
                  <Input
                    disabled={shoudlDisableForm() || state.isEdit}
                    placeholder="Mã nhóm"
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                  />
                </Form.Item>
                <Form.Item className="input-col" label="Nhóm nhân viên cha" name="nhomnhanviencha_id" rules={[]}>
                  {renderUserCategories()}
                </Form.Item>
                <Form.Item className="input-col" label={getLangText('GLOBAL.DESCRIPTION')} name="mo_ta" rules={[]}>
                  <TextArea
                    disabled={shoudlDisableForm()}
                    rows={2}
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                    placeholder={getLangText('GLOBAL.DESCRIPTION')}
                  />
                </Form.Item>
                <Form.Item className="input-col" name="hien_thi_menu_con" label="Hiển thị menu con">
                  <Radio.Group
                    onChange={() => {
                      if (!state.isChanged) {
                        setState((state) => ({
                          ...state,
                          isChanged: true,
                        }));
                      }
                    }}
                    options={[
                      { label: 'Không', value: false },
                      { label: 'Có', value: true },
                    ]}
                    optionType="button"
                    buttonStyle="solid"
                  />
                </Form.Item>
                <Form.Item className="button-col">
                  <Button shape="round" type="primary" htmlType="submit" disabled={shoudlDisableForm()}>
                    {state.isEdit ? getLangText('FORM.UPDATE') : getLangText('FORM.ADD_NEW')}
                  </Button>
                  {state.isEdit && (
                    <Button shape="round" type="danger" onClick={() => onResetForm()}>
                      {getLangText('FORM.CANCEL')}
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
            <Col xl={18} sm={24} xs={24} className="table-cates">
              <div className="w5d-list">
                <AppFilter
                  isShowSearchBox={true}
                  isShowCategories={false}
                  isShowStatus={false}
                  isShowDatePicker={true}
                  isRangeDatePicker={true}
                  title="Nhóm thành viên"
                  search={state.search}
                  onDateChange={(dates) => onDateChange(dates)}
                  onFilterChange={(field, value) => onFilterChange(field, value)}
                />
                <Row className="select-action-group-cate">
                  <Space wrap>
                    <Dropdown overlay={menu} trigger="click" disabled={state.checkedList.length <= 0}>
                      <Button>
                        Chọn hành động
                        <DownOutlined />
                      </Button>
                    </Dropdown>
                  </Space>
                </Row>
                <List
                  locale={{
                    emptyText: getLangText('GLOBAL.NO_ITEMS'),
                  }}
                  header={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={10} className="text-left">
                        {getLangText('GLOBAL.TITLE')}
                        <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={6} className="text-left">
                        {getLangText('GLOBAL.DESCRIPTION')}
                        <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  footer={
                    <Row>
                      <Col span={1}>
                        <Checkbox onChange={(e) => onCheckAllChange(e)} checked={state.checkAll} />
                      </Col>
                      <Col span={10} className="text-left">
                        {getLangText('GLOBAL.TITLE')}
                        <Sorting urlQuery={urlQuery} field={'ten_nhom'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={6} className="text-left">
                        {getLangText('GLOBAL.DESCRIPTION')}
                        <Sorting urlQuery={urlQuery} field={'mo_ta'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={3} className="text-left">
                        {getLangText('GLOBAL.STATUS')}
                        <Sorting urlQuery={urlQuery} field={'trang_thai'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                      <Col span={4}>
                        {getLangText('GLOBAL.CREATE_DATE')}
                        <Sorting urlQuery={urlQuery} field={'ngay_tao'} onSortChange={(fields) => onSortChange(fields)} />
                      </Col>
                    </Row>
                  }
                  // dataSource={get(props, 'user.listCates.result.data', [])}
                  dataSource={recursiveUserCates(get(props, 'user.listCates.result.data', []), get(props, 'user.listCates.result.data', []))}
                  pagination={{
                    hideOnSinglePage: false,
                    responsive: true,
                    showLessItems: true,
                    pageSizeOptions: constants.PAGE_SIZE_OPTIONS,
                    pageSize: pageSize,
                    onChange: (page, pageSize) => {
                      changeMutilUrlParams({ pageSize, page });
                      setPageSize(pageSize);
                      setPage(page);
                    },
                    current: Number(page),
                    showTotal: (total) => `${getLangText('GLOBAL.TOTAL')}: ${total}`,
                    showSizeChanger: true,
                  }}
                  renderItem={(item) => (
                    <List.Item key={item.nhom_nhan_vien_id}>
                      <Row className="full">
                        <Col span={1}>
                          <Checkbox checked={state.checkedList.includes(item.nhom_nhan_vien_id)} value={item.nhom_nhan_vien_id} onChange={() => onChangeCheck(item.nhom_nhan_vien_id)} />
                        </Col>
                        <Col span={10} className="text-left">
                          <Link className="edit" to={`/users?categories=${item.nhom_nhan_vien_id}`}>
                            {item.titleLevel} {item.soluong === 0 ? '' : `(${item.soluong})`}
                          </Link>
                          <div className="actions">
                            {shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/sua') && (
                              <Button className="edit act" type="link" onClick={() => onEditCategory(item)}>
                                {getLangText('GLOBAL.EDIT')}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/xoa') && (
                              <Button className="delete act" type="link" onClick={() => onDeleteCategory(item.nhom_nhan_vien_id, item.trang_thai)}>
                                {item.trang_thai === 'active' ? 'Khóa' : 'Xóa'}
                              </Button>
                            )}
                            {shouldHaveAccessPermission('nhom_nhan_vien', 'nhom_nhan_vien/phan_quyen') && (
                              <Button className="edit act" type="link" onClick={() => onOpenPermissionModal(item)}>
                                {getLangText('GLOBAL.SET_PERMISSION')}
                              </Button>
                            )}
                          </div>
                        </Col>
                        <Col span={6} className="text-left">
                          {item.mo_ta}
                        </Col>
                        <Col span={3} className="text-left">
                          <W5dStatusTag status={item.trang_thai} />
                        </Col>
                        <Col span={4}>{moment(item.ngay_tao).isValid() ? moment(item.ngay_tao).utc(0).format(config.DATE_FORMAT) : '-'}</Col>
                      </Row>
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal wrapClassName="w5d-modal-permission w5d-modal-large" visible={state.openPermission} closable={false} footer={null} header={null}>
        <W5dPermission
          onCancel={() =>
            setState((state) => ({
              ...state,
              openPermission: false,
            }))
          }
          selectedCategory={state.form}
          modules={state.modules}
          onEditCategory={(category) => handleSubmit(category)}
        />
      </Modal>
      <Prompt when={state.isChanged} message={getLangText('GLOBAL.CONFIRM_LEAVE_PAGE')} />
    </App>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    config: state.config,
  };
};

export default connect(mapStateToProps)(UserCategory);
