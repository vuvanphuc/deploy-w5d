import * as ruleActions from '../actions/ruleset';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      status: '',
    },
  },
};

export default function rulesetReducer(state = initialState, action) {
  switch (action.type) {
    case ruleActions.GET_RULESET:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case ruleActions.GET_RULESET_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          result: action.item,
        },
      };
    case ruleActions.GET_RULESET_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case ruleActions.GET_RULE_SETS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case ruleActions.GET_RULE_SETS_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          result: action.list,
        },
      };
    case ruleActions.GET_RULE_SETS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
