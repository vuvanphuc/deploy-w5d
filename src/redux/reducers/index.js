import { combineReducers } from 'redux';
// IMPORT REDUCERS
import category from './category';
import notification from './notification';
import moduleApp from './moduleApp';
import deduction from './deduction';
import form from './form';
import user from './user';
import media from './media';
import log from './log';
import config from './configure';
import statistic from './statistic';
import table from './table';
import ruleset from './ruleset';
import installer from './installer';
import post from './post';
import exam from './exam';
import question from './question';
import cook from './cook';
import remind from './remind';
import appointment from './appointment';
import course from './course';
import lesson from './lesson';

export default combineReducers({
  // EXPORT REDUCERS
  category,
  notification,
  media,
  log,
  module: moduleApp,
  deduction,
  form,
  user,
  post,
  exam,
  question,
  cook,
  config,
  statistic,
  table,
  ruleset,
  installer,
  remind,
  appointment,
  course,
  lesson,
});
