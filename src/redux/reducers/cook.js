import * as cookActions from '../actions/cook';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function cookReducer(state = initialState, action) {
  switch (action.type) {
    case cookActions.GET_COOK:
      return {
        ...state,
        item: { ...state.item, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case cookActions.GET_COOK_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case cookActions.GET_COOK_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case cookActions.SET_SEARCH_COOK:
      return {
        ...state,
        list: { ...state.list, search: action.params },
      };
    case cookActions.GET_COOKS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case cookActions.GET_COOKS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case cookActions.GET_COOKS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case cookActions.DELETE_COOK:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case cookActions.DELETE_COOK_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case cookActions.DELETE_COOK_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case cookActions.DELETE_COOKS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case cookActions.DELETE_COOKS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case cookActions.DELETE_COOKS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case cookActions.CREATE_COOK:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case cookActions.CREATE_COOK_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case cookActions.CREATE_COOK_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case cookActions.EDIT_COOK:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case cookActions.EDIT_COOK_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case cookActions.EDIT_COOK_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case cookActions.EDIT_STATUS_COOKS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case cookActions.EDIT_STATUS_COOKS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case cookActions.EDIT_STATUS_COOKS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case cookActions.GET_COOK_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case cookActions.GET_COOK_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case cookActions.GET_COOK_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case cookActions.EDIT_COOK_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case cookActions.EDIT_COOK_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case cookActions.EDIT_COOK_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case cookActions.CREATE_COOK_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case cookActions.CREATE_COOK_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case cookActions.CREATE_COOK_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case cookActions.DELETE_COOK_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case cookActions.DELETE_COOK_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case cookActions.DELETE_COOK_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case cookActions.DELETE_COOK_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case cookActions.DELETE_COOK_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case cookActions.DELETE_COOK_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case cookActions.GET_COOK_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case cookActions.GET_COOK_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case cookActions.GET_COOK_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
