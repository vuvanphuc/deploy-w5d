import * as logActions from '../actions/log';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
    },
  },
};

export default function logReducer(state = initialState, action) {
  switch (action.type) {
    case logActions.GET_LOGS:
      return {
        ...state,
        loading: true,
      };
    case logActions.GET_LOGS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case logActions.GET_LOGS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case logActions.CREATE_LOG:
      return {
        ...state,
        loading: true,
      };
    case logActions.CREATE_LOG_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case logActions.CREATE_LOG_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
