import * as lessonActions from '../actions/lesson';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function lessonReducer(state = initialState, action) {
  switch (action.type) {
    case lessonActions.GET_LESSON:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case lessonActions.GET_LESSON_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case lessonActions.GET_LESSON_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case lessonActions.SET_SEARCH_LESSON:
      return {
        ...state,
        list: { ...state.list, search: action.params },
      };
    case lessonActions.GET_LESSONS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case lessonActions.GET_LESSONS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case lessonActions.GET_LESSONS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case lessonActions.DELETE_LESSON:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case lessonActions.DELETE_LESSON_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case lessonActions.DELETE_LESSON_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case lessonActions.DELETE_LESSONS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case lessonActions.DELETE_LESSONS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case lessonActions.DELETE_LESSONS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case lessonActions.CREATE_LESSON:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case lessonActions.CREATE_LESSON_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case lessonActions.CREATE_LESSON_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case lessonActions.EDIT_LESSON:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case lessonActions.EDIT_LESSON_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case lessonActions.EDIT_LESSON_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case lessonActions.EDIT_STATUS_LESSONS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case lessonActions.EDIT_STATUS_LESSONS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case lessonActions.EDIT_STATUS_LESSONS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case lessonActions.GET_LESSON_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case lessonActions.GET_LESSON_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case lessonActions.GET_LESSON_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case lessonActions.EDIT_LESSON_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case lessonActions.EDIT_LESSON_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case lessonActions.EDIT_LESSON_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case lessonActions.CREATE_LESSON_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case lessonActions.CREATE_LESSON_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case lessonActions.CREATE_LESSON_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case lessonActions.DELETE_LESSON_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case lessonActions.DELETE_LESSON_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case lessonActions.DELETE_LESSON_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case lessonActions.DELETE_LESSON_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case lessonActions.DELETE_LESSON_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case lessonActions.DELETE_LESSON_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case lessonActions.GET_LESSON_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case lessonActions.GET_LESSON_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case lessonActions.GET_LESSON_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
