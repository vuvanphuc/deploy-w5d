import * as userActions from '../actions/user';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  login: {
    loading: false,
    result: {},
  },
  logout: {
    loading: false,
    result: {},
  },
  verify: {
    loading: false,
    result: {},
  },
  forgotPassword: {
    loading: false,
    result: {},
  },
  changePassword: {
    loading: false,
    result: {},
  },
  statistics: {
    loading: false,
    result: {},
  },
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case userActions.GET_USER:
      return {
        ...state,
        item: { ...state.item, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case userActions.GET_USER_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case userActions.GET_USER_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case userActions.SET_SEARCH_USER:
      return {
        ...state,
        list: { ...state.list, search: action.params },
      };
    case userActions.GET_USERS:
      return {
        ...state,
        list: { ...state.list, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case userActions.GET_USERS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case userActions.GET_USERS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case userActions.DELETE_USER:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case userActions.DELETE_USER_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case userActions.DELETE_USER_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case userActions.DELETE_USERS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case userActions.DELETE_USERS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case userActions.DELETE_USERS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case userActions.CREATE_USER:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case userActions.CREATE_USER_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case userActions.CREATE_USER_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case userActions.IMPORT_USERS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case userActions.IMPORT_USERS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case userActions.IMPORT_USERS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case userActions.REGISTER_USER:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case userActions.REGISTER_USER_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.email },
      };
    case userActions.REGISTER_USER_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case userActions.EDIT_USER:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case userActions.EDIT_USER_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case userActions.EDIT_USER_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case userActions.EDIT_STATUS_USERS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case userActions.EDIT_STATUS_USERS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case userActions.EDIT_STATUS_USERS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case userActions.GET_USER_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case userActions.GET_USER_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case userActions.GET_USER_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case userActions.EDIT_USER_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case userActions.EDIT_USER_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case userActions.EDIT_USER_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case userActions.CREATE_USER_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case userActions.CREATE_USER_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case userActions.CREATE_USER_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case userActions.DELETE_USER_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case userActions.DELETE_USER_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case userActions.DELETE_USER_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case userActions.DELETE_USER_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case userActions.DELETE_USER_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case userActions.DELETE_USER_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case userActions.GET_USER_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case userActions.GET_USER_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case userActions.GET_USER_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case userActions.LOGIN_USER:
      return {
        ...state,
        login: { ...state.login, loading: true },
      };
    case userActions.LOGIN_USER_SUCCESS:
      return {
        ...state,
        login: { ...state.login, loading: false, result: action.result },
      };
    case userActions.LOGIN_USER_FAILED:
      return {
        ...state,
        login: { ...state.login, loading: false, result: action.error },
      };
    case userActions.LOGOUT_USER:
      return {
        ...state,
        logout: { ...state.logout, loading: true },
      };
    case userActions.LOGOUT_USER_SUCCESS:
      return {
        ...state,
        logout: { ...state.logout, loading: false, result: action.result },
      };
    case userActions.LOGOUT_USER_FAILED:
      return {
        ...state,
        logout: { ...state.logout, loading: false, result: action.error },
      };
    case userActions.VERIFY_USER:
      return {
        ...state,
        verify: { ...state.verify, loading: true },
      };
    case userActions.VERIFY_USER_SUCCESS:
      return {
        ...state,
        verify: { ...state.verify, loading: false, result: action.result },
      };
    case userActions.VERIFY_USER_FAILED:
      return {
        ...state,
        verify: { ...state.verify, loading: false, result: action.error },
      };
    case userActions.FORGOT_PASSWORD:
      return {
        ...state,
        forgotPassword: { ...state.forgotPassword, loading: true },
      };
    case userActions.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        forgotPassword: {
          ...state.forgotPassword,
          loading: false,
          result: action.result,
        },
      };
    case userActions.FORGOT_PASSWORD_FAILED:
      return {
        ...state,
        forgotPassword: {
          ...state.forgotPassword,
          loading: false,
          result: action.error,
        },
      };

    case userActions.CHANGE_PASSWORD:
      return {
        ...state,
        changePassword: { ...state.changePassword, loading: true },
      };
    case userActions.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        changePassword: {
          ...state.changePassword,
          loading: false,
          result: action.result,
        },
      };
    case userActions.CHANGE_PASSWORD_FAILED:
      return {
        ...state,
        changePassword: {
          ...state.changePassword,
          loading: false,
          result: action.error,
        },
      };
    case userActions.GET_USER_STATISTICS:
      return {
        ...state,
        statistics: { ...state.statistics, loading: true },
      };
    case userActions.GET_USER_STATISTICS_SUCCESS:
      return {
        ...state,
        statistics: {
          ...state.statistics,
          loading: false,
          result: action.result,
        },
      };
    case userActions.GET_USER_STATISTICS_FAILED:
      return {
        ...state,
        statistics: {
          ...state.statistics,
          loading: false,
          result: action.error,
        },
      };
    default:
      return state;
  }
}
