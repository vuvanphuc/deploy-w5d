import * as courseActions from '../actions/course';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function courseReducer(state = initialState, action) {
  switch (action.type) {
    case courseActions.GET_COURSE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case courseActions.GET_COURSE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case courseActions.GET_COURSE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case courseActions.SET_SEARCH_COURSE:
      return {
        ...state,
        list: { ...state.list, search: action.params },
      };
    case courseActions.GET_COURSES:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case courseActions.GET_COURSES_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case courseActions.GET_COURSES_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case courseActions.DELETE_COURSE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case courseActions.DELETE_COURSE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case courseActions.DELETE_COURSE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case courseActions.DELETE_COURSES:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case courseActions.DELETE_COURSES_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case courseActions.DELETE_COURSES_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case courseActions.CREATE_COURSE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case courseActions.CREATE_COURSE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case courseActions.CREATE_COURSE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case courseActions.EDIT_COURSE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case courseActions.EDIT_COURSE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case courseActions.EDIT_COURSE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case courseActions.EDIT_STATUS_COURSES:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case courseActions.EDIT_STATUS_COURSES_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case courseActions.EDIT_STATUS_COURSES_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case courseActions.GET_COURSE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case courseActions.GET_COURSE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case courseActions.GET_COURSE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case courseActions.EDIT_COURSE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case courseActions.EDIT_COURSE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case courseActions.EDIT_COURSE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case courseActions.CREATE_COURSE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case courseActions.CREATE_COURSE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case courseActions.CREATE_COURSE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case courseActions.DELETE_COURSE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case courseActions.DELETE_COURSE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case courseActions.DELETE_COURSE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case courseActions.DELETE_COURSE_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case courseActions.DELETE_COURSE_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case courseActions.DELETE_COURSE_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case courseActions.GET_COURSE_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case courseActions.GET_COURSE_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case courseActions.GET_COURSE_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
