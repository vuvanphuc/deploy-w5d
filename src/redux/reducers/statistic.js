import * as statisticActions from '../actions/statistic';

const initialState = {
  global: {
    loading: false,
    result: {},
  },
  project: {
    loading: false,
    result: {},
  },
  latest: {
    loading: false,
    result: {},
  },
  edge: {
    loading: false,
    result: {},
  },
};

export default function statisticReducer(state = initialState, action) {
  switch (action.type) {
    case statisticActions.GET_GLOBAL_STATISTICS:
      return {
        ...state,
        global: { ...state.global, loading: true },
      };
    case statisticActions.GET_GLOBAL_STATISTICS_SUCCESS:
      return {
        ...state,
        global: {
          ...state.global,
          loading: false,
          result: action.result,
        },
      };
    case statisticActions.GET_GLOBAL_STATISTICS_FAILED:
      return {
        ...state,
        global: {
          ...state.global,
          loading: false,
          result: action.error,
        },
      };
    case statisticActions.GET_MODULE_STATISTICS:
      return {
        ...state,
        module: { ...state.module, loading: true },
      };
    case statisticActions.GET_MODULE_STATISTICS_SUCCESS:
      return {
        ...state,
        module: {
          ...state.module,
          loading: false,
          result: action.result,
        },
      };
    case statisticActions.GET_MODULE_STATISTICS_FAILED:
      return {
        ...state,
        module: {
          ...state.module,
          loading: false,
          result: action.error,
        },
      };
    case statisticActions.GET_PROJECT_STATISTICS:
      return {
        ...state,
        project: { ...state.project, loading: true },
      };
    case statisticActions.GET_PROJECT_STATISTICS_SUCCESS:
      return {
        ...state,
        project: {
          ...state.project,
          loading: false,
          result: action.result,
        },
      };
    case statisticActions.GET_PROJECT_STATISTICS_FAILED:
      return {
        ...state,
        project: {
          ...state.project,
          loading: false,
          result: action.error,
        },
      };
    case statisticActions.GET_MODULE_LATEST_RECORD:
      return {
        ...state,
        latest: { ...state.module, loading: true },
      };
    case statisticActions.GET_MODULE_LATEST_RECORD_SUCCESS:
      return {
        ...state,
        latest: {
          ...state.module,
          loading: false,
          result: action.result,
        },
      };
    case statisticActions.GET_MODULE_LATEST_RECORD_FAILED:
      return {
        ...state,
        latest: {
          ...state.module,
          loading: false,
          result: action.error,
        },
      };
    case statisticActions.GET_MODULE_EDGE_RECORD:
      return {
        ...state,
        edge: { ...state.module, loading: true },
      };
    case statisticActions.GET_MODULE_EDGE_RECORD_SUCCESS:
      return {
        ...state,
        edge: {
          ...state.module,
          loading: false,
          result: action.result,
        },
      };
    case statisticActions.GET_MODULE_EDGE_RECORD_FAILED:
      return {
        ...state,
        edge: {
          ...state.module,
          loading: false,
          result: action.error,
        },
      };
    default:
      return state;
  }
}
