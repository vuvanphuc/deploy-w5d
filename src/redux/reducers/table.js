import * as tableActions from '../actions/table';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      status: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function tableReducer(state = initialState, action) {
  switch (action.type) {
    case tableActions.GET_COLUMNS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case tableActions.GET_COLUMNS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case tableActions.GET_COLUMNS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case tableActions.GET_COLUMN:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.GET_COLUMN_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          result: action.item.data,
        },
      };
    case tableActions.GET_COLUMN_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case tableActions.CREATE_COLUMN:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.CREATE_COLUMN_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case tableActions.CREATE_COLUMN_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case tableActions.EDIT_COLUMN:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.EDIT_COLUMN_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          result: action.item,
        },
      };
    case tableActions.EDIT_COLUMN_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case tableActions.DELETE_COLUMN:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.DELETE_COLUMN_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case tableActions.DELETE_COLUMN_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case tableActions.DELETE_COLUMNS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.DELETE_COLUMNS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false },
      };
    case tableActions.DELETE_COLUMNS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false },
      };
    case tableActions.EDIT_COLUMNS_STATUS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.EDIT_COLUMNS_STATUS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false },
      };
    case tableActions.EDIT_COLUMNS_STATUS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false },
      };
    case tableActions.GET_TABLE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case tableActions.GET_TABLE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case tableActions.GET_TABLE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case tableActions.EDIT_TABLE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case tableActions.EDIT_TABLE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case tableActions.EDIT_TABLE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case tableActions.CREATE_TABLE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case tableActions.CREATE_TABLE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case tableActions.CREATE_TABLE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case tableActions.DELETE_TABLE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case tableActions.DELETE_TABLE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case tableActions.DELETE_TABLE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case tableActions.DROP_TABLE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case tableActions.DROP_TABLE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case tableActions.DROP_TABLE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case tableActions.DELETE_TABLES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case tableActions.DELETE_TABLES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case tableActions.DELETE_TABLES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case tableActions.EDIT_TABLES_STATUS:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case tableActions.EDIT_TABLES_STATUS_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false },
      };
    case tableActions.EDIT_TABLES_STATUS_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false },
      };
    case tableActions.GET_TABLES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };
    case tableActions.GET_TABLES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.listCates,
        },
      };
    case tableActions.GET_TABLES_FAILED:
      return {
        ...state,
        listCates: { ...state.listCates, loading: false, result: action.error },
      };
    case tableActions.GEN_TABLE_SQL:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.GEN_TABLE_SQL_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          result: action.item.data,
        },
      };
    case tableActions.GEN_TABLE_SQL_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case tableActions.GEN_TABLE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.GEN_TABLE_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          result: action.item.data,
        },
      };
    case tableActions.GEN_TABLE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case tableActions.ADD_COLUMNS_TO_TABLE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case tableActions.ADD_COLUMNS_TO_TABLE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false },
      };
    case tableActions.ADD_COLUMNS_TO_TABLE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false },
      };
    case tableActions.GET_VIEW:
      return {
        ...state,
        item: { ...state.item, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    default:
      return state;
  }
}
