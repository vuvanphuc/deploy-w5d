import * as installerActions from '../actions/installer';

const initialState = {
  check: {
    loading: false,
    result: {},
  },
  checkConnection: {
    loading: false,
    result: {},
  },
  initData: {
    loading: false,
    result: {},
  },
};

export default function installerReducer(state = initialState, action) {
  switch (action.type) {
    case installerActions.CHECK_INSTALLER:
      return {
        ...state,
        check: { ...state.check, loading: true },
      };
    case installerActions.CHECK_INSTALLER_SUCCESS:
      return {
        ...state,
        check: { ...state.check, loading: false, result: action.result },
      };
    case installerActions.CHECK_INSTALLER_FAILED:
      return {
        ...state,
        check: { ...state.check, loading: false, result: action.error },
      };

    case installerActions.CHECK_CONNECTION:
      return {
        ...state,
        checkConnection: { ...state.checkConnection, loading: true },
      };
    case installerActions.CHECK_CONNECTION_SUCCESS:
      return {
        ...state,
        checkConnection: { ...state.checkConnection, loading: false, result: action.result },
      };
    case installerActions.CHECK_CONNECTION_FAILED:
      return {
        ...state,
        checkConnection: { ...state.checkConnection, loading: false, result: action.error },
      };

    case installerActions.INIT_DATA:
      return {
        ...state,
        initData: { ...state.initData, loading: true },
      };
    case installerActions.INIT_DATA_SUCCESS:
      return {
        ...state,
        initData: { ...state.initData, loading: false, result: action.result },
      };
    case installerActions.INIT_DATA_FAILED:
      return {
        ...state,
        initData: { ...state.initData, loading: false, result: action.error },
      };

    default:
      return state;
  }
}
