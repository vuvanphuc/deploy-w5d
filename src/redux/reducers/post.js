import * as postActions from '../actions/post';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function postReducer(state = initialState, action) {
  switch (action.type) {
    case postActions.GET_POST:
      return {
        ...state,
        item: { ...state.item, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case postActions.GET_POST_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case postActions.GET_POST_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case postActions.SET_SEARCH_POST:
      return {
        ...state,
        list: { ...state.list, search: action.params },
      };
    case postActions.GET_POSTS:
      return {
        ...state,
        list: { ...state.list, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case postActions.GET_POSTS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case postActions.GET_POSTS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case postActions.DELETE_POST:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case postActions.DELETE_POST_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case postActions.DELETE_POST_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case postActions.DELETE_POSTS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case postActions.DELETE_POSTS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case postActions.DELETE_POSTS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case postActions.CREATE_POST:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case postActions.CREATE_POST_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case postActions.CREATE_POST_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case postActions.EDIT_POST:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case postActions.EDIT_POST_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case postActions.EDIT_POST_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case postActions.EDIT_STATUS_POSTS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case postActions.EDIT_STATUS_POSTS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case postActions.EDIT_STATUS_POSTS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case postActions.GET_POST_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case postActions.GET_POST_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case postActions.GET_POST_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case postActions.EDIT_POST_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case postActions.EDIT_POST_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case postActions.EDIT_POST_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case postActions.CREATE_POST_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case postActions.CREATE_POST_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case postActions.CREATE_POST_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case postActions.DELETE_POST_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case postActions.DELETE_POST_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case postActions.DELETE_POST_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case postActions.DELETE_POST_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case postActions.DELETE_POST_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case postActions.DELETE_POST_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case postActions.GET_POST_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case postActions.GET_POST_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case postActions.GET_POST_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
