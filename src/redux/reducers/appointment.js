import * as appointmentActions from '../actions/appointment';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function appointmentReducer(state = initialState, action) {
  switch (action.type) {
    case appointmentActions.GET_APPOINTMENT:
      return {
        ...state,
        item: { ...state.item, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case appointmentActions.GET_APPOINTMENT_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case appointmentActions.GET_APPOINTMENT_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case appointmentActions.GET_APPOINTMENTS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case appointmentActions.GET_APPOINTMENTS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case appointmentActions.GET_APPOINTMENTS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case appointmentActions.DELETE_APPOINTMENT:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case appointmentActions.DELETE_APPOINTMENT_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case appointmentActions.DELETE_APPOINTMENT_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case appointmentActions.DELETE_APPOINTMENTS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case appointmentActions.DELETE_APPOINTMENTS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case appointmentActions.DELETE_APPOINTMENTS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case appointmentActions.CREATE_APPOINTMENT:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case appointmentActions.CREATE_APPOINTMENT_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case appointmentActions.CREATE_APPOINTMENT_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case appointmentActions.IMPORT_APPOINTMENTS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case appointmentActions.IMPORT_APPOINTMENTS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case appointmentActions.IMPORT_APPOINTMENTS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case appointmentActions.REGISTER_APPOINTMENT:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case appointmentActions.REGISTER_APPOINTMENT_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.email },
      };
    case appointmentActions.REGISTER_APPOINTMENT_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case appointmentActions.EDIT_APPOINTMENT:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case appointmentActions.EDIT_APPOINTMENT_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case appointmentActions.EDIT_APPOINTMENT_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case appointmentActions.EDIT_STATUS_APPOINTMENTS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case appointmentActions.EDIT_STATUS_APPOINTMENTS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case appointmentActions.EDIT_STATUS_APPOINTMENTS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case appointmentActions.GET_APPOINTMENT_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case appointmentActions.GET_APPOINTMENT_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case appointmentActions.GET_APPOINTMENT_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case appointmentActions.EDIT_APPOINTMENT_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case appointmentActions.EDIT_APPOINTMENT_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case appointmentActions.EDIT_APPOINTMENT_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case appointmentActions.CREATE_APPOINTMENT_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case appointmentActions.CREATE_APPOINTMENT_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case appointmentActions.CREATE_APPOINTMENT_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case appointmentActions.DELETE_APPOINTMENT_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case appointmentActions.DELETE_APPOINTMENT_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case appointmentActions.DELETE_APPOINTMENT_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case appointmentActions.DELETE_APPOINTMENT_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case appointmentActions.DELETE_APPOINTMENT_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case appointmentActions.DELETE_APPOINTMENT_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case appointmentActions.GET_APPOINTMENT_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case appointmentActions.GET_APPOINTMENT_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case appointmentActions.GET_APPOINTMENT_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
