import * as notificationActions from '../actions/notification';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      status: 'active',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      status: 'active',
    },
  },
};

export default function notificationReducer(state = initialState, action) {
  switch (action.type) {
    case notificationActions.GET_NOTIFICATIONS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case notificationActions.GET_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.notifications },
      };
    case notificationActions.GET_NOTIFICATIONS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case notificationActions.GET_NOTIFICATION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case notificationActions.GET_NOTIFICATION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case notificationActions.GET_NOTIFICATION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case notificationActions.CREATE_NOTIFICATION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case notificationActions.CREATE_NOTIFICATION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case notificationActions.CREATE_NOTIFICATION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case notificationActions.EDIT_NOTIFICATION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case notificationActions.EDIT_NOTIFICATION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case notificationActions.EDIT_NOTIFICATION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case notificationActions.DELETE_NOTIFICATION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case notificationActions.DELETE_NOTIFICATION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case notificationActions.DELETE_NOTIFICATION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case notificationActions.DELETE_NOTIFICATIONS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case notificationActions.DELETE_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.notifications },
      };
    case notificationActions.DELETE_NOTIFICATIONS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case notificationActions.GET_NOTIFICATION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };

    case notificationActions.GET_NOTIFICATION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case notificationActions.GET_NOTIFICATION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case notificationActions.EDIT_NOTIFICATION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case notificationActions.EDIT_NOTIFICATION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case notificationActions.EDIT_NOTIFICATION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case notificationActions.CREATE_NOTIFICATION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case notificationActions.CREATE_NOTIFICATION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case notificationActions.CREATE_NOTIFICATION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case notificationActions.DELETE_NOTIFICATION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case notificationActions.DELETE_NOTIFICATION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case notificationActions.DELETE_NOTIFICATION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case notificationActions.DELETE_NOTIFICATION_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };
    case notificationActions.DELETE_NOTIFICATION_CATES_SUCCESS:
      return {
        ...state,
        listCates: { ...state.listCates, loading: false, result: action.listCates },
      };
    case notificationActions.DELETE_NOTIFICATION_CATES_FAILED:
      return {
        ...state,
        listCates: { ...state.listCates, loading: false, result: action.error },
      };
    case notificationActions.GET_NOTIFICATION_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };
    case notificationActions.GET_NOTIFICATION_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.listCates,
        },
      };
    case notificationActions.GET_NOTIFICATION_CATES_FAILED:
      return {
        ...state,
        listCates: { ...state.listCates, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
