import * as mediaActions from '../actions/media';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
    },
  },
};

export default function mediaReducer(state = initialState, action) {
  switch (action.type) {
    case mediaActions.GET_MEDIAS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case mediaActions.GET_MEDIAS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case mediaActions.GET_MEDIAS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case mediaActions.GET_MEDIA:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case mediaActions.GET_MEDIA_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case mediaActions.GET_MEDIA_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case mediaActions.GET_MEDIA_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case mediaActions.GET_MEDIA_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case mediaActions.GET_MEDIA_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case mediaActions.EDIT_MEDIA_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case mediaActions.EDIT_MEDIA_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case mediaActions.EDIT_MEDIA_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case mediaActions.CREATE_MEDIA_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case mediaActions.CREATE_MEDIA_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case mediaActions.CREATE_MEDIA_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case mediaActions.DELETE_MEDIA_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case mediaActions.DELETE_MEDIA_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case mediaActions.DELETE_MEDIA_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case mediaActions.DELETE_MEDIA_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case mediaActions.DELETE_MEDIA_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case mediaActions.DELETE_MEDIA_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case mediaActions.GET_MEDIA_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };
    case mediaActions.GET_MEDIA_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.listCates,
        },
      };
    case mediaActions.GET_MEDIA_CATES_FAILED:
      return {
        ...state,
        listCates: { ...state.listCates, loading: false, result: action.error },
      };

    default:
      return state;
  }
}
