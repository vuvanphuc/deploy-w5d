import * as deductionActions from '../actions/deduction';

const initialState = {
  deduction: {
    loading: false,
    result: {},
  },
  deductioninfos: {
    loading: false,
    executeTime: 0,
    result: {},
    search: {
      keyword: '',
      status: 'active',
    },
  },
  deductioninfo: {
    loading: false,
    result: {},
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      status: 'active',
    },
  },
};

export default function deductionReducer(state = initialState, action) {
  switch (action.type) {
    case deductionActions.GET_DEDUCTION_INFOS:
      return {
        ...state,
        deductioninfos: { ...state.deductioninfos, loading: true, executeTime: new Date().getTime() },
      };
    case deductionActions.GET_DEDUCTION_INFOS_SUCCESS:
      return {
        ...state,
        deductioninfos: {
          ...state.deductioninfos,
          loading: false,
          executeTime: new Date().getTime() - state.deductioninfos.executeTime,
          result: action.list,
        },
      };
    case deductionActions.GET_DEDUCTION_INFOS_FAILED:
      return {
        ...state,
        deductioninfos: { ...state.deductioninfos, loading: false, result: action.error },
      };
    case deductionActions.GET_DEDUCTION_INFO:
      return {
        ...state,
        deductioninfo: { ...state.deductioninfo, loading: true },
      };
    case deductionActions.GET_DEDUCTION_INFO_SUCCESS:
      return {
        ...state,
        deductioninfo: { ...state.deductioninfo, loading: false, result: action.item },
      };
    case deductionActions.GET_DEDUCTION_INFO_FAILED:
      return {
        ...state,
        deductioninfo: { ...state.deductioninfo, loading: false, result: action.error },
      };
    case deductionActions.GET_DEDUCTION:
      return {
        ...state,
        deduction: { ...state.deduction, loading: true },
      };
    case deductionActions.GET_DEDUCTION_SUCCESS:
      return {
        ...state,
        deduction: { ...state.deduction, loading: false, result: action.item },
      };
    case deductionActions.GET_DEDUCTION_FAILED:
      return {
        ...state,
        deduction: { ...state.deduction, loading: false, result: action.error },
      };
    case deductionActions.EDIT_DEDUCTION:
      return {
        ...state,
        deduction: { ...state.deduction, loading: true },
      };
    case deductionActions.EDIT_DEDUCTION_SUCCESS:
      return {
        ...state,
        deduction: { ...state.deduction, loading: false, result: action.item },
      };
    case deductionActions.EDIT_DEDUCTION_FAILED:
      return {
        ...state,
        deduction: { ...state.deduction, loading: false, result: action.error },
      };
    case deductionActions.EDIT_DEDUCTION_RESULT:
      return {
        ...state,
        deduction: { ...state.deduction, loading: true },
      };
    case deductionActions.EDIT_DEDUCTION_RESULT_SUCCESS:
      return {
        ...state,
        deduction: { ...state.deduction, loading: false, result: action.item },
      };
    case deductionActions.EDIT_DEDUCTION_RESULT_FAILED:
      return {
        ...state,
        deduction: { ...state.deduction, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
