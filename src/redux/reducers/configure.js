import * as configActions from '../actions/configure';
import { getCurrentLanguage } from '../../helpers/language.helper';

const configInfo = JSON.parse(localStorage.getItem('configInfo'));
const defaultData = configInfo && Array.isArray(configInfo) && configInfo.length > 0 ? configInfo : [];

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: { data: defaultData },
    search: {
      keyword: '',
      status: '',
    },
  },
  email: {
    from: '',
    sender: '',
    smtp: {},
    loading: false,
  },
  theme: {},
  language: getCurrentLanguage(),
};

export default function configReducer(state = initialState, action) {
  switch (action.type) {
    case configActions.GET_GLOBAL:
      return state;
    case configActions.EDIT_GLOBAL:
      return {
        ...state,
        [action.key]: action.value,
      };
    case configActions.GET_CONFIGS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case configActions.GET_CONFIGS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case configActions.GET_CONFIGS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case configActions.GET_CONFIG:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case configActions.GET_CONFIG_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case configActions.GET_CONFIG_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case configActions.CREATE_CONFIG:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case configActions.CREATE_CONFIG_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case configActions.CREATE_CONFIG_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case configActions.SEND_TEST_EMAIL:
      return {
        ...state,
        email: { ...state.email, loading: true },
      };
    case configActions.SEND_TEST_EMAIL_SUCCESS:
      return {
        ...state,
        email: { ...state.email, loading: false, result: action.email },
      };
    case configActions.SEND_TEST_EMAIL_FAILED:
      return {
        ...state,
        email: { ...state.email, loading: false, result: action.error },
      };
    case configActions.EDIT_CONFIG:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case configActions.EDIT_CONFIG_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case configActions.EDIT_CONFIG_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case configActions.DELETE_CONFIG:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case configActions.DELETE_CONFIG_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case configActions.DELETE_CONFIG_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
