import * as categoryActions from '../actions/category';
const categoryInfo = JSON.parse(localStorage.getItem('categoryInfo'));
const defaultCategories = categoryInfo && categoryInfo.constructor === Object && Object.keys(categoryInfo).length > 0 ? categoryInfo : {};

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    executeTime: 0,
    result: {},
    search: {
      keyword: '',
      status: 'active',
    },
  },
  categoryData: { loading: false, executeTime: 0, result: { data: defaultCategories } },
};

export default function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case categoryActions.GET_CATEGORIES:
      return {
        ...state,
        list: { ...state.list, loading: true, executeTime: new Date().getTime() },
      };
    case categoryActions.GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          executeTime: new Date().getTime() - state.list.executeTime,
          result: action.modules,
        },
      };
    case categoryActions.GET_CATEGORIES_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case categoryActions.GET_CATEGORY:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case categoryActions.GET_CATEGORY_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case categoryActions.GET_CATEGORY_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case categoryActions.CREATE_CATEGORY:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case categoryActions.CREATE_CATEGORY_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case categoryActions.CREATE_CATEGORY_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case categoryActions.EDIT_CATEGORY:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case categoryActions.EDIT_CATEGORY_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case categoryActions.EDIT_CATEGORY_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case categoryActions.EDIT_CATEGORIES_STATUS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case categoryActions.EDIT_CATEGORIES_STATUS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case categoryActions.EDIT_CATEGORIES_STATUS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case categoryActions.DELETE_CATEGORY:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case categoryActions.DELETE_CATEGORY_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case categoryActions.DELETE_CATEGORY_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case categoryActions.DELETE_CATEGORIES:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case categoryActions.DELETE_CATEGORIES_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.modules },
      };
    case categoryActions.DELETE_CATEGORIES_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case categoryActions.GET_CATEGORY_DATA:
      return {
        ...state,
        categoryData: { ...state.categoryData, loading: true, executeTime: new Date().getTime() },
      };
    case categoryActions.GET_CATEGORY_DATA_SUCCESS:
      return {
        ...state,
        categoryData: {
          ...state.categoryData,
          loading: false,
          executeTime: new Date().getTime() - state.list.executeTime,
          result: action.data,
        },
      };
    case categoryActions.GET_ALL_CATEGORY_DATA_FAILED:
      return {
        ...state,
        categoryData: { ...state.categoryData, loading: false, result: action.error },
      };
    case categoryActions.GET_ALL_CATEGORY_DATA:
      return {
        ...state,
        categoryData: { ...state.categoryData, loading: true, executeTime: new Date().getTime() },
      };
    case categoryActions.GET_ALL_CATEGORY_DATA_SUCCESS:
      return {
        ...state,
        categoryData: {
          ...state.categoryData,
          loading: false,
          executeTime: new Date().getTime() - state.list.executeTime,
          result: action.data,
        },
      };
    case categoryActions.GET_ALL_CATEGORY_DATA_FAILED:
      return {
        ...state,
        categoryData: { ...state.categoryData, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
