import * as questionActions from '../actions/question';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function questionReducer(state = initialState, action) {
  switch (action.type) {
    case questionActions.GET_QUESTION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case questionActions.GET_QUESTION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case questionActions.GET_QUESTION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case questionActions.SET_SEARCH_QUESTION:
      return {
        ...state,
        list: { ...state.list, search: action.params },
      };
    case questionActions.GET_QUESTIONS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case questionActions.GET_QUESTIONS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case questionActions.GET_QUESTIONS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case questionActions.DELETE_QUESTION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case questionActions.DELETE_QUESTION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case questionActions.DELETE_QUESTION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case questionActions.DELETE_QUESTIONS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case questionActions.DELETE_QUESTIONS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case questionActions.DELETE_QUESTIONS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case questionActions.CREATE_QUESTION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case questionActions.CREATE_QUESTION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case questionActions.CREATE_QUESTION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case questionActions.EDIT_QUESTION:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case questionActions.EDIT_QUESTION_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case questionActions.EDIT_QUESTION_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case questionActions.EDIT_STATUS_QUESTIONS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case questionActions.EDIT_STATUS_QUESTIONS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case questionActions.EDIT_STATUS_QUESTIONS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case questionActions.GET_QUESTION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case questionActions.GET_QUESTION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case questionActions.GET_QUESTION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case questionActions.EDIT_QUESTION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case questionActions.EDIT_QUESTION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case questionActions.EDIT_QUESTION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case questionActions.CREATE_QUESTION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case questionActions.CREATE_QUESTION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case questionActions.CREATE_QUESTION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case questionActions.DELETE_QUESTION_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case questionActions.DELETE_QUESTION_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case questionActions.DELETE_QUESTION_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case questionActions.DELETE_QUESTION_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case questionActions.DELETE_QUESTION_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case questionActions.DELETE_QUESTION_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case questionActions.GET_QUESTION_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case questionActions.GET_QUESTION_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case questionActions.GET_QUESTION_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
