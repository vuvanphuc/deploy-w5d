import * as remindActions from '../actions/remind';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function remindReducer(state = initialState, action) {
  switch (action.type) {
    case remindActions.GET_REMIND:
      return {
        ...state,
        item: { ...state.item, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case remindActions.GET_REMIND_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case remindActions.GET_REMIND_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case remindActions.GET_REMINDS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case remindActions.GET_REMINDS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case remindActions.GET_REMINDS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case remindActions.DELETE_REMIND:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case remindActions.DELETE_REMIND_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case remindActions.DELETE_REMIND_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case remindActions.DELETE_REMINDS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case remindActions.DELETE_REMINDS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case remindActions.DELETE_REMINDS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case remindActions.CREATE_REMIND:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case remindActions.CREATE_REMIND_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case remindActions.CREATE_REMIND_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case remindActions.IMPORT_REMINDS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case remindActions.IMPORT_REMINDS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case remindActions.IMPORT_REMINDS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case remindActions.REGISTER_REMIND:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case remindActions.REGISTER_REMIND_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.email },
      };
    case remindActions.REGISTER_REMIND_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case remindActions.EDIT_REMIND:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case remindActions.EDIT_REMIND_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case remindActions.EDIT_REMIND_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case remindActions.EDIT_STATUS_REMINDS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case remindActions.EDIT_STATUS_REMINDS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case remindActions.EDIT_STATUS_REMINDS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case remindActions.GET_REMIND_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case remindActions.GET_REMIND_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case remindActions.GET_REMIND_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case remindActions.EDIT_REMIND_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case remindActions.EDIT_REMIND_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case remindActions.EDIT_REMIND_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case remindActions.CREATE_REMIND_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case remindActions.CREATE_REMIND_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case remindActions.CREATE_REMIND_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case remindActions.DELETE_REMIND_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case remindActions.DELETE_REMIND_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case remindActions.DELETE_REMIND_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case remindActions.DELETE_REMIND_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case remindActions.DELETE_REMIND_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case remindActions.DELETE_REMIND_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case remindActions.GET_REMIND_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case remindActions.GET_REMIND_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case remindActions.GET_REMIND_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
