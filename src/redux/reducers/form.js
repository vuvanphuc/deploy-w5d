import * as formActions from '../actions/form';
const formInfo = JSON.parse(localStorage.getItem('formInfo'));
const defaultForms = formInfo && Array.isArray(formInfo) && formInfo.length > 0 ? formInfo : [];

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: { data: defaultForms },
    search: {
      keyword: '',
      status: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function formReducer(state = initialState, action) {
  switch (action.type) {
    case formActions.GET_FORMS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case formActions.GET_FORMS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.forms },
      };
    case formActions.GET_FORMS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case formActions.GET_FORM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case formActions.GET_FORM_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          [action.item.data.bang_du_lieu]: action.item.data,
        },
      };
    case formActions.GET_FORM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case formActions.CREATE_FORM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case formActions.CREATE_FORM_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case formActions.CREATE_FORM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case formActions.IMPORT_FORMS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case formActions.IMPORT_FORMS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.forms },
      };
    case formActions.IMPORT_FORMS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case formActions.EDIT_FORM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case formActions.EDIT_FORM_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          result: action.item,
          [action.table]: action.data,
        },
      };
    case formActions.EDIT_FORM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case formActions.EDIT_FORMS_STATUS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case formActions.EDIT_FORMS_STATUS_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          loading: false,
          result: action.item,
          [action.table]: action.data,
        },
      };
    case formActions.EDIT_FORMS_STATUS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case formActions.DELETE_FORM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case formActions.DELETE_FORM_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case formActions.DELETE_FORM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case formActions.DELETE_FORMS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case formActions.DELETE_FORMS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case formActions.DELETE_FORMS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case formActions.GET_FORM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case formActions.GET_FORM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case formActions.GET_FORM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case formActions.EDIT_FORM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case formActions.EDIT_FORM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case formActions.EDIT_FORM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case formActions.CREATE_FORM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case formActions.CREATE_FORM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case formActions.CREATE_FORM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case formActions.DELETE_FORM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case formActions.DELETE_FORM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case formActions.DELETE_FORM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case formActions.DELETE_FORM_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case formActions.DELETE_FORM_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case formActions.DELETE_FORM_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case formActions.GET_FORM_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };
    case formActions.GET_FORM_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.listCates,
        },
      };
    case formActions.GET_FORM_CATES_FAILED:
      return {
        ...state,
        listCates: { ...state.listCates, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
