import * as moduleActions from '../actions/moduleApp';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  export: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    executeTime: 0,
    result: {},
    search: {
      keyword: '',
      status: 'active',
      startDate: '',
      endDate: '',
      date: '',
      isPagination: true,
      page: 1,
      pageSize: 100,
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      keyword: '',
      status: '',
    },
  },
};

export default function moduleReducer(state = initialState, action) {
  switch (action.type) {
    case moduleActions.GET_MODULES:
      return {
        ...state,
        list: { ...state.list, loading: true, executeTime: new Date().getTime(), result: {} },
      };
    case moduleActions.GET_MODULES_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          executeTime: new Date().getTime() - state.list.executeTime,
          result: action.modules,
        },
      };
    case moduleActions.GET_MODULES_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case moduleActions.GET_MODULE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case moduleActions.GET_MODULE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case moduleActions.GET_MODULE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case moduleActions.EXPORT_MODULE:
      return {
        ...state,
        export: { ...state.export, loading: true },
      };
    case moduleActions.EXPORT_MODULE_SUCCESS:
      return {
        ...state,
        export: { ...state.export, loading: false, result: action.item },
      };
    case moduleActions.EXPORT_MODULE_FAILED:
      return {
        ...state,
        export: { ...state.export, loading: false, result: action.error },
      };
    case moduleActions.CREATE_MODULE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case moduleActions.CREATE_MODULE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case moduleActions.CREATE_MODULE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case moduleActions.EDIT_MODULE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case moduleActions.EDIT_MODULE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case moduleActions.EDIT_MODULE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case moduleActions.DELETE_MODULE:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case moduleActions.DELETE_MODULE_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.item },
      };
    case moduleActions.DELETE_MODULE_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case moduleActions.DELETE_MODULES:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case moduleActions.DELETE_MODULES_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.modules },
      };
    case moduleActions.DELETE_MODULES_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case moduleActions.EDIT_MODULES_STATUS:
      return {
        ...state,
        list: { ...state.list, loading: true },
      };
    case moduleActions.EDIT_MODULES_STATUS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.modules },
      };
    case moduleActions.EDIT_MODULES_STATUS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case moduleActions.GET_MODULE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case moduleActions.GET_MODULE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case moduleActions.GET_MODULE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case moduleActions.EDIT_MODULE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case moduleActions.EDIT_MODULE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case moduleActions.EDIT_MODULE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case moduleActions.CREATE_MODULE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case moduleActions.CREATE_MODULE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case moduleActions.CREATE_MODULE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case moduleActions.DELETE_MODULE_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case moduleActions.DELETE_MODULE_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case moduleActions.DELETE_MODULE_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case moduleActions.GET_MODULE_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };
    case moduleActions.GET_MODULE_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.listCates,
        },
      };
    case moduleActions.GET_MODULE_CATES_FAILED:
      return {
        ...state,
        listCates: { ...state.listCates, loading: false, result: action.error },
      };
    default:
      return state;
  }
}
