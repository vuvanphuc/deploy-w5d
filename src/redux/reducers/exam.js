import * as examActions from '../actions/exam';

const initialState = {
  item: {
    loading: false,
    result: {},
  },
  list: {
    loading: false,
    result: {},
    search: {
      status: '',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  cate: {
    loading: false,
    result: {},
  },
  listCates: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  topic: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  listTopics: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
  history: {
    loading: false,
    result: {},
  },
  listHistories: {
    loading: false,
    result: {},
    search: {
      status: 'active',
      keyword: '',
      startDate: '',
      endDate: '',
      date: '',
    },
  },
};

export default function examReducer(state = initialState, action) {
  switch (action.type) {
    case examActions.GET_EXAM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.GET_EXAM_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.GET_EXAM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.GET_EXAM_HISTORY:
      return {
        ...state,
        history: { ...state.history, loading: true },
      };
    case examActions.GET_EXAM_HISTORY_SUCCESS:
      return {
        ...state,
        history: { ...state.history, loading: false, result: action.result },
      };
    case examActions.GET_EXAM_HISTORY_FAILED:
      return {
        ...state,
        history: { ...state.history, loading: false, result: action.error },
      };
    case examActions.GET_EXAM_TOPIC:
      return {
        ...state,
        topic: { ...state.topic, loading: true },
      };
    case examActions.GET_EXAM_TOPIC_SUCCESS:
      return {
        ...state,
        topic: { ...state.topic, loading: false, result: action.result },
      };
    case examActions.GET_EXAM_TOPIC_FAILED:
      return {
        ...state,
        topic: { ...state.topic, loading: false, result: action.error },
      };
    case examActions.SET_SEARCH_EXAM:
      return {
        ...state,
        list: { ...state.list, search: action.params },
      };
    case examActions.GET_EXAMS:
      return {
        ...state,
        list: { ...state.list, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case examActions.GET_EXAMS_SUCCESS:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.result },
      };
    case examActions.GET_EXAMS_FAILED:
      return {
        ...state,
        list: { ...state.list, loading: false, result: action.error },
      };
    case examActions.GET_EXAM_HISTORIES:
      return {
        ...state,
        listHistories: { ...state.listHistories, loading: typeof action.params.loading !== undefined ? action.params.loading : true },
      };
    case examActions.GET_EXAM_HISTORIES_SUCCESS:
      return {
        ...state,
        listHistories: { ...state.listHistories, loading: false, result: action.result },
      };
    case examActions.GET_EXAM_HISTORIES_FAILED:
      return {
        ...state,
        listHistories: { ...state.listHistories, loading: false, result: action.error },
      };
    case examActions.GET_EXAM_TOPICS:
      return {
        ...state,
        listTopics: { ...state.listTopics, loading: true },
      };
    case examActions.GET_EXAM_TOPICS_SUCCESS:
      return {
        ...state,
        listTopics: { ...state.listTopics, loading: false, result: action.result },
      };
    case examActions.GET_EXAM_TOPICS_FAILED:
      return {
        ...state,
        listTopics: { ...state.listTopics, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.DELETE_EXAM_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.DELETE_EXAM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAM_HISTORY:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.DELETE_EXAM_HISTORY_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.DELETE_EXAM_HISTORY_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAM_TOPIC:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.DELETE_EXAM_TOPIC_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.DELETE_EXAM_TOPIC_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAMS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.DELETE_EXAMS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.DELETE_EXAMS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAM_HISTORIES:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.DELETE_EXAM_HISTORIES_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.DELETE_EXAM_HISTORIES_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAM_TOPICS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.DELETE_EXAM_TOPICS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.DELETE_EXAM_TOPICS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.CREATE_EXAM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.CREATE_EXAM_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.CREATE_EXAM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.CREATE_EXAM_HISTORY:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.CREATE_EXAM_HISTORY_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.CREATE_EXAM_HISTORY_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.CREATE_EXAM_TOPIC:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.CREATE_EXAM_TOPIC_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.CREATE_EXAM_TOPIC_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.EDIT_EXAM:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.EDIT_EXAM_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.EDIT_EXAM_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.EDIT_EXAM_HISTORY:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.EDIT_EXAM_HISTORY_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.EDIT_EXAM_HISTORY_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.EDIT_EXAM_TOPIC:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.EDIT_EXAM_TOPIC_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.EDIT_EXAM_TOPIC_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.EDIT_STATUS_EXAMS:
      return {
        ...state,
        item: { ...state.item, loading: true },
      };
    case examActions.EDIT_STATUS_EXAMS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.result },
      };
    case examActions.EDIT_STATUS_EXAMS_FAILED:
      return {
        ...state,
        item: { ...state.item, loading: false, result: action.error },
      };
    case examActions.GET_EXAM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case examActions.GET_EXAM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case examActions.GET_EXAM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.result },
      };
    case examActions.EDIT_EXAM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case examActions.EDIT_EXAM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case examActions.EDIT_EXAM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case examActions.CREATE_EXAM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case examActions.CREATE_EXAM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case examActions.CREATE_EXAM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAM_CATE:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case examActions.DELETE_EXAM_CATE_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case examActions.DELETE_EXAM_CATE_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case examActions.DELETE_EXAM_CATES:
      return {
        ...state,
        cate: { ...state.cate, loading: true },
      };
    case examActions.DELETE_EXAM_CATES_SUCCESS:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.cate },
      };
    case examActions.DELETE_EXAM_CATES_FAILED:
      return {
        ...state,
        cate: { ...state.cate, loading: false, result: action.error },
      };
    case examActions.GET_EXAM_CATES:
      return {
        ...state,
        listCates: { ...state.listCates, loading: true },
      };

    case examActions.GET_EXAM_CATES_SUCCESS:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    case examActions.GET_EXAM_CATES_FAILED:
      return {
        ...state,
        listCates: {
          ...state.listCates,
          loading: false,
          result: action.result,
        },
      };
    default:
      return state;
  }
}
