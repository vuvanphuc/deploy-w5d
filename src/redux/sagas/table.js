import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { get } from 'lodash';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';

export function* fetchColumn(payload) {
  try {
    const endpoint = `${config.API_URL}/api/column/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.table.GET_COLUMN_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.GET_COLUMN_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Lấy dữ liệu trường dữ liệu thất bại.'),
    });
  }
}

export function* fetchColumns(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/columns`, payload.params ? payload.params : state.table.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.table.GET_COLUMNS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.GET_COLUMNS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Lấy dữ liệu trường dữ liệu thất bại.'),
    });
  }
}

export function* deleteColumn(payload) {
  try {
    const endpoint = `${config.API_URL}/api/column/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.table.DELETE_COLUMN_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.DELETE_COLUMN_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa trường dữ liệu thất bại.'),
    });
  }
}

export function* deleteColumns(payload) {
  try {
    const endpoint = `${config.API_URL}/api/column/delete-columns`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.table.DELETE_COLUMNS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.DELETE_COLUMNS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa trường dữ liệu thất bại.'),
    });
  }
}

export function* editColumn(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/column/${payload.params.truongid}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
    });
    const data = yield response.data;
    yield put({ type: actions.table.EDIT_COLUMN_SUCCESS, result: data });
    yield put({ type: actions.table.GET_COLUMN_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.EDIT_COLUMN_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trường dữ liệu thất bại.'),
    });
  }
}

export function* editColumnsStatus(payload) {
  try {
    const endpoint = `${config.API_URL}/api/columns/update-columns-status`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.table.EDIT_COLUMNS_STATUS_SUCCESS,
      item: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.EDIT_COLUMNS_STATUS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không cập nhật được trạng thái của trường dữ liệu.'),
    });
  }
}

export function* createColumn(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/column`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
    });
    const data = yield response.data;
    yield put({ type: actions.table.CREATE_COLUMN_SUCCESS, result: data });
    yield put({ type: actions.table.GET_COLUMN_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.CREATE_COLUMN_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới trường dữ liệu thất bại.'),
    });
  }
}

export function* fetchTables(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/tables`, payload.params ? payload.params : state.table.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.table.GET_TABLES_SUCCESS,
      listCates: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.GET_TABLES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh sách bảng dữ liệu thất bại.'),
    });
  }
}

export function* createTable(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/table`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
    });
    const data = yield response.data;
    yield put({
      type: actions.table.CREATE_TABLE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.CREATE_TABLE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Thêm bảng dữ liệu thất bại.'),
    });
  }
}

export function* editTable(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/table/${payload.params.bangid}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
    });
    const data = yield response.data;
    yield put({
      type: actions.table.EDIT_TABLE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.EDIT_TABLE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật bảng dữ liệu thất bại.'),
    });
  }
}

export function* deleteTable(payload) {
  try {
    const endpoint = `${config.API_URL}/api/table/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({
      type: actions.table.DELETE_TABLE_SUCCESS,
      item: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.DELETE_TABLE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được bảng dữ liệu.'),
    });
  }
}
export function* dropTable(payload) {
  try {
    const endpoint = `${config.API_URL}/api/table/drop-table/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.table.DROP_TABLE_SUCCESS,
      item: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.DROP_TABLE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được bảng dữ liệu.'),
    });
  }
}

export function* deleteTables(payload) {
  try {
    const endpoint = `${config.API_URL}/api/tables/delete-tables`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.table.DELETE_TABLES_SUCCESS,
      item: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.DELETE_TABLES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được bảng dữ liệu.'),
    });
  }
}

export function* editTablesStatus(payload) {
  try {
    const endpoint = `${config.API_URL}/api/tables/update-tables-status`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.table.EDIT_TABLES_STATUS_SUCCESS,
      cate: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.EDIT_TABLES_STATUS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không cập nhật được trạng thái của bảng dữ liệu.'),
    });
  }
}

export function* fetchTableSql(payload) {
  try {
    const endpoint = `${config.API_URL}/api/tablesql/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.table.GEN_TABLE_SQL_SUCCESS,
      item: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.GEN_TABLE_SQL_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không sinh được câu lệnh SQL.'),
    });
  }
}

export function* genTable(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/gentable`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
    });
    const data = yield response.data;
    yield put({
      type: actions.table.GEN_TABLE_SUCCESS,
      item: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.GEN_TABLE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Lỗi phát sinh khi tạo bảng.'),
    });
  }
}

export function* addColumnsToTable(payload) {
  try {
    const endpoint = `${config.API_URL}/api/add-columns-to-table`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.table.ADD_COLUMNS_TO_TABLE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.table.ADD_COLUMNS_TO_TABLE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Thêm trường dữ liệu thất bại.'),
    });
  }
}

function* fetchView(payload) {
  try {
    const endpoint = buildUrl(`${config.API_URL}/api/findview`, payload.params ? payload.params : {});
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.table.GET_VIEW_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.table.GET_VIEW_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại'),
    });
  }
}

export function* loadView() {
  yield takeEvery(actions.table.GET_VIEW, fetchView);
}

export function* loadColumn() {
  yield takeEvery(actions.table.GET_COLUMN, fetchColumn);
}

export function* loadColumns() {
  yield takeEvery(actions.table.GET_COLUMNS, fetchColumns);
}
export function* loadDeleteColumn() {
  yield takeEvery(actions.table.DELETE_COLUMN, deleteColumn);
}

export function* loadDeleteColumns() {
  yield takeEvery(actions.table.DELETE_COLUMNS, deleteColumns);
}
export function* loadEditColumn() {
  yield takeEvery(actions.table.EDIT_COLUMN, editColumn);
}

export function* loadEditColumnsStatus() {
  yield takeEvery(actions.table.EDIT_COLUMNS_STATUS, editColumnsStatus);
}

export function* loadCreateColumn() {
  yield takeEvery(actions.table.CREATE_COLUMN, createColumn);
}

export function* loadTables() {
  yield takeEvery(actions.table.GET_TABLES, fetchTables);
}

export function* loadCreateTable() {
  yield takeEvery(actions.table.CREATE_TABLE, createTable);
}

export function* loadEditTable() {
  yield takeEvery(actions.table.EDIT_TABLE, editTable);
}

export function* loadDeleteTable() {
  yield takeEvery(actions.table.DELETE_TABLE, deleteTable);
}
export function* loadDropTable() {
  yield takeEvery(actions.table.DROP_TABLE, dropTable);
}

export function* loadTableSql() {
  yield takeEvery(actions.table.GEN_TABLE_SQL, fetchTableSql);
}

export function* loadGenTable() {
  yield takeEvery(actions.table.GEN_TABLE, genTable);
}
export function* loadDeleteTables() {
  yield takeEvery(actions.table.DELETE_TABLES, deleteTables);
}

export function* loadEditTablesStatus() {
  yield takeEvery(actions.table.EDIT_TABLES_STATUS, editTablesStatus);
}

export function* loadAddColumnsToTable() {
  yield takeEvery(actions.table.ADD_COLUMNS_TO_TABLE, addColumnsToTable);
}
