import { call, put, takeEvery } from 'redux-saga/effects';
import { get } from 'lodash';
import { notification } from 'antd';
import { config } from 'config';
import * as actions from '../actions';
import { getApi, postApi } from '../services/api';

function* checkInstaller(payload) {
  try {
    const endpoint = `${config.API_URL}/api/installer/check`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.installer.CHECK_INSTALLER_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.installer.CHECK_INSTALLER_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Lấy dữ liệu kiểm tra cài đặt CSDL thất bại.'),
    });
  }
}

function* checkConnection(payload) {
  try {
    const endpoint = `${config.API_URL}/api/installer/checkConnection`;
    const response = yield call(postApi, endpoint, payload.params.body);
    const data = yield response.data;
    yield put({ type: actions.installer.CHECK_CONNECTION_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.installer.CHECK_CONNECTION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Lấy dữ liệu kiểm tra kết nối CSDL thất bại.'),
    });
  }
}

function* initData(payload) {
  try {
    const endpoint = `${config.API_URL}/api/installer/initData`;
    const response = yield call(postApi, endpoint, payload.params.body);
    const data = yield response.data;
    yield put({ type: actions.installer.INIT_DATA_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.installer.INIT_DATA_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Khởi tạo dữ liệu trong CSDL thất bại.'),
    });
  }
}

export function* loadCheckInstaller() {
  yield takeEvery(actions.installer.CHECK_INSTALLER, checkInstaller);
}

export function* loadCheckConnection() {
  yield takeEvery(actions.installer.CHECK_CONNECTION, checkConnection);
}

export function* loadInitData() {
  yield takeEvery(actions.installer.INIT_DATA, initData);
}
