import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchQuestion(payload) {
  try {
    let endpoint = `${config.API_URL}/api/question/${payload.params.cau_hoi_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.question.GET_QUESTION_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.question.GET_QUESTION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải câu hỏi thất bại'),
    });
  }
}

function* fetchQuestions(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/questions`, payload.params ? payload.params : state.question.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.question.GET_QUESTIONS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.GET_QUESTIONS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách câu hỏi thất bại'),
    });
  }
}

function* deleteQuestion(payload) {
  try {
    const endpoint = `${config.API_URL}/api/question/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.question.DELETE_QUESTION_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.DELETE_QUESTION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa câu hỏi thất bại'),
    });
  }
}

function* deleteQuestions(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-questions`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.question.DELETE_QUESTIONS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.DELETE_QUESTIONS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều câu hỏi thất bại.'),
    });
  }
}
function* editQuestion(payload) {
  try {
    const endpoint = `${config.API_URL}/api/question/${payload.params.cau_hoi_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.question.EDIT_QUESTION_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.question.EDIT_QUESTION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật câu hỏi thất bại'),
    });
  }
}

function* editStatusQuestions(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-questions`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.question.EDIT_STATUS_QUESTIONS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.question.EDIT_STATUS_QUESTIONS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều câu hỏi thất bại.'),
    });
  }
}

function* createQuestion(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/question`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.question.CREATE_QUESTION_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.CREATE_QUESTION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới câu hỏi thất bại'),
    });
  }
}

function* fetchQuestionCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/question-categories`, payload.params ? payload.params : state.question.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.question.GET_QUESTION_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.GET_QUESTION_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục câu hỏi thất bại'),
    });
  }
}

function* createQuestionCate(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/question-category`;
    const params = {
      ten_nhom: payload.params.ten_nhom,
      nhom_cha_id: payload.params.nhom_cha_id,
      mo_ta: payload.params.mo_ta,
      anh_dai_dien: payload.params.anh_dai_dien,
      trang_thai: payload.params.trang_thai,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    };
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({ type: actions.question.CREATE_QUESTION_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.CREATE_QUESTION_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục câu hỏi thất bại'),
    });
  }
}

function* editQuestionCate(payload) {
  try {
    const userInfo = getUserInformation();
    const state = yield select();
    const endpoint = `${config.API_URL}/api/question-category/${payload.params.nhom_cau_hoi_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
      ngay_sua: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.question.EDIT_QUESTION_CATE_SUCCESS, result: data });
    yield put(actions.question.getQuestionCates(state.question.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.EDIT_QUESTION_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục câu hỏi thất bại'),
    });
  }
}

function* deleteQuestionCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/question-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.question.DELETE_QUESTION_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.DELETE_QUESTION_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục câu hỏi thất bại'),
    });
  }
}
function* deleteQuestionCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-question-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.question.DELETE_QUESTION_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.question.DELETE_QUESTION_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục câu hỏi.'),
    });
  }
}

export function* loadQuestion() {
  yield takeEvery(actions.question.GET_QUESTION, fetchQuestion);
}

export function* loadQuestions() {
  yield takeEvery(actions.question.GET_QUESTIONS, fetchQuestions);
}

export function* loadDeleteQuestion() {
  yield takeEvery(actions.question.DELETE_QUESTION, deleteQuestion);
}

export function* loadDeleteQuestions() {
  yield takeEvery(actions.question.DELETE_QUESTIONS, deleteQuestions);
}

export function* loadEditQuestion() {
  yield takeEvery(actions.question.EDIT_QUESTION, editQuestion);
}

export function* loadEditStatusQuestion() {
  yield takeEvery(actions.question.EDIT_STATUS_QUESTIONS, editStatusQuestions);
}

export function* loadCreateQuestion() {
  yield takeEvery(actions.question.CREATE_QUESTION, createQuestion);
}

export function* loadQuestionCates() {
  yield takeEvery(actions.question.GET_QUESTION_CATES, fetchQuestionCates);
}
export function* loadCreateQuestionCate() {
  yield takeEvery(actions.question.CREATE_QUESTION_CATE, createQuestionCate);
}

export function* loadEditQuestionCate() {
  yield takeEvery(actions.question.EDIT_QUESTION_CATE, editQuestionCate);
}

export function* loadDeleteQuestionCate() {
  yield takeEvery(actions.question.DELETE_QUESTION_CATE, deleteQuestionCate);
}

export function* loadDeleteQuestionCates() {
  yield takeEvery(actions.question.DELETE_QUESTION_CATES, deleteQuestionCates);
}
