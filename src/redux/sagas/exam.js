import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchExam(payload) {
  try {
    let endpoint = `${config.API_URL}/api/exam/${payload.params.de_thi_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.exam.GET_EXAM_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.exam.GET_EXAM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải bài thi thất bại'),
    });
  }
}

function* fetchExamHistory(payload) {
  try {
    let endpoint = `${config.API_URL}/api/exam-history/${payload.params.lich_su_thi_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.exam.GET_EXAM_HISTORY_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.exam.GET_EXAM_HISTORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải lịch sử thi thất bại'),
    });
  }
}

function* fetchExamTopic(payload) {
  try {
    let endpoint = `${config.API_URL}/api/exam-topic/${payload.params.chu_de_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.exam.GET_EXAM_TOPIC_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.exam.GET_EXAM_TOPIC_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải chủ đề thi thất bại'),
    });
  }
}

function* fetchExams(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/exams`, payload.params ? payload.params : state.exam.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.exam.GET_EXAMS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.GET_EXAMS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách đề thi thất bại'),
    });
  }
}

function* fetchExamHistories(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/exam-histories`, payload.params ? payload.params : state.exam.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.exam.GET_EXAM_HISTORIES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.GET_EXAM_HISTORIES_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách lịch sử thi thất bại'),
    });
  }
}

function* fetchExamTopics(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/exam-topics`, payload.params ? payload.params : state.exam.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.exam.GET_EXAM_TOPICS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.GET_EXAM_TOPICS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách chủ đề thi thất bại'),
    });
  }
}

function* deleteExam(payload) {
  try {
    const endpoint = `${config.API_URL}/api/exam/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAM_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa đề thi thất bại'),
    });
  }
}

function* deleteExamHistory(payload) {
  try {
    const endpoint = `${config.API_URL}/api/exam-history/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAM_HISTORY_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAM_HISTORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa lịch sử thi thất bại'),
    });
  }
}

function* deleteExamTopic(payload) {
  try {
    const endpoint = `${config.API_URL}/api/exam-topic/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAM_TOPIC_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAM_TOPIC_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa lịch sử chủ đề thi thất bại'),
    });
  }
}

function* deleteExams(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-exams`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAMS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAMS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều đề thất bại.'),
    });
  }
}

function* deleteExamHistories(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-exam-histories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAM_HISTORIES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAM_HISTORIES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều lịch sử thi thất bại.'),
    });
  }
}

function* deleteExamTopics(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-exam-topics`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAM_TOPICS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAM_TOPICS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều chủ đề thi thất bại.'),
    });
  }
}

function* editExam(payload) {
  try {
    const endpoint = `${config.API_URL}/api/exam/${payload.params.de_thi_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.exam.EDIT_EXAM_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.exam.EDIT_EXAM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật đề thi thất bại'),
    });
  }
}

function* editExamHistory(payload) {
  try {
    const endpoint = `${config.API_URL}/api/exam-history/${payload.params.lich_su_thi_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.exam.EDIT_EXAM_HISTORY_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.exam.EDIT_EXAM_HISTORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật lịch sử thi thất bại'),
    });
  }
}

function* editExamTopic(payload) {
  try {
    const endpoint = `${config.API_URL}/api/exam-topic/${payload.params.chu_de_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.exam.EDIT_EXAM_TOPIC_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.exam.EDIT_EXAM_TOPIC_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật chủ đề thi thất bại'),
    });
  }
}

function* editStatusExams(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-exams`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.exam.EDIT_STATUS_EXAMS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.exam.EDIT_STATUS_EXAMS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều đề thi thất bại.'),
    });
  }
}

function* createExam(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/exam`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.exam.CREATE_EXAM_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.CREATE_EXAM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới đề thi thất bại'),
    });
  }
}

function* createExamHistory(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/exam-history`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.exam.CREATE_EXAM_HISTORY_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.CREATE_EXAM_HISTORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới lịch sử thi thất bại'),
    });
  }
}

function* createExamTopic(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/exam-topic`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.exam.CREATE_EXAM_TOPIC_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.CREATE_EXAM_TOPIC_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới chủ đề thi thất bại'),
    });
  }
}

function* fetchExamCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/exam-categories`, payload.params ? payload.params : state.exam.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.exam.GET_EXAM_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.GET_EXAM_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục đề thi thất bại'),
    });
  }
}

function* createExamCate(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/exam-category`;
    const params = {
      ten_nhom: payload.params.ten_nhom,
      nhom_cha_id: payload.params.nhom_cha_id,
      mo_ta: payload.params.mo_ta,
      anh_dai_dien: payload.params.anh_dai_dien,
      trang_thai: payload.params.trang_thai,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    };
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({ type: actions.exam.CREATE_EXAM_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.CREATE_EXAM_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục đề thi thất bại'),
    });
  }
}

function* editExamCate(payload) {
  try {
    const userInfo = getUserInformation();
    const state = yield select();
    const endpoint = `${config.API_URL}/api/exam-category/${payload.params.nhom_de_thi_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
      ngay_sua: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.exam.EDIT_EXAM_CATE_SUCCESS, result: data });
    yield put(actions.exam.getExamCates(state.exam.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.EDIT_EXAM_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục đề thi thất bại'),
    });
  }
}

function* deleteExamCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/exam-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAM_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAM_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục đề thi thất bại'),
    });
  }
}
function* deleteExamCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-exam-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.exam.DELETE_EXAM_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.exam.DELETE_EXAM_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục đề thi.'),
    });
  }
}

export function* loadExam() {
  yield takeEvery(actions.exam.GET_EXAM, fetchExam);
}

export function* loadExamHistory() {
  yield takeEvery(actions.exam.GET_EXAM_HISTORY, fetchExamHistory);
}

export function* loadExamTopic() {
  yield takeEvery(actions.exam.GET_EXAM_TOPIC, fetchExamTopic);
}

export function* loadExams() {
  yield takeEvery(actions.exam.GET_EXAMS, fetchExams);
}

export function* loadExamHistories() {
  yield takeEvery(actions.exam.GET_EXAM_HISTORIES, fetchExamHistories);
}

export function* loadExamTopics() {
  yield takeEvery(actions.exam.GET_EXAM_TOPICS, fetchExamTopics);
}

export function* loadDeleteExam() {
  yield takeEvery(actions.exam.DELETE_EXAM, deleteExam);
}

export function* loadDeleteExamHistory() {
  yield takeEvery(actions.exam.DELETE_EXAM_HISTORY, deleteExamHistory);
}

export function* loadDeleteExamTopic() {
  yield takeEvery(actions.exam.DELETE_EXAM_TOPIC, deleteExamTopic);
}

export function* loadDeleteExams() {
  yield takeEvery(actions.exam.DELETE_EXAMS, deleteExams);
}

export function* loadDeleteExamHistories() {
  yield takeEvery(actions.exam.DELETE_EXAM_HISTORIES, deleteExamHistories);
}

export function* loadDeleteExamTopics() {
  yield takeEvery(actions.exam.DELETE_EXAM_TOPICS, deleteExamTopics);
}

export function* loadEditExam() {
  yield takeEvery(actions.exam.EDIT_EXAM, editExam);
}

export function* loadEditExamHistory() {
  yield takeEvery(actions.exam.EDIT_EXAM_HISTORY, editExamHistory);
}

export function* loadEditExamTopic() {
  yield takeEvery(actions.exam.EDIT_EXAM_TOPIC, editExamTopic);
}

export function* loadEditStatusExam() {
  yield takeEvery(actions.exam.EDIT_STATUS_EXAMS, editStatusExams);
}

export function* loadCreateExam() {
  yield takeEvery(actions.exam.CREATE_EXAM, createExam);
}

export function* loadCreateExamHistory() {
  yield takeEvery(actions.exam.CREATE_EXAM_HISTORY, createExamHistory);
}

export function* loadCreateExamTopic() {
  yield takeEvery(actions.exam.CREATE_EXAM_TOPIC, createExamTopic);
}

export function* loadExamCates() {
  yield takeEvery(actions.exam.GET_EXAM_CATES, fetchExamCates);
}
export function* loadCreateExamCate() {
  yield takeEvery(actions.exam.CREATE_EXAM_CATE, createExamCate);
}

export function* loadEditExamCate() {
  yield takeEvery(actions.exam.EDIT_EXAM_CATE, editExamCate);
}

export function* loadDeleteExamCate() {
  yield takeEvery(actions.exam.DELETE_EXAM_CATE, deleteExamCate);
}

export function* loadDeleteExamCates() {
  yield takeEvery(actions.exam.DELETE_EXAM_CATES, deleteExamCates);
}
