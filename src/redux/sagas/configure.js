import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl } from '../../helpers/common.helper';
import * as actions from '../actions/configure';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { getLangText } from '../../helpers/language.helper';

function* fetchConfig(payload) {
  try {
    const endpoint = `${config.API_URL}/api/config/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.GET_CONFIG_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.GET_CONFIG_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.GET_CONFI_FAIL'),
    });
  }
}

function* fetchConfigs(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/configs`, payload.params ? payload.params : state.config.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.GET_CONFIGS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({
      type: actions.GET_CONFIGS_FAILED,
      error: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.GET_CONFIGS_FAILED'),
    });
    notification.error({
      message: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.GET_CONFIGS_FAILED'),
    });
  }
}

function* deleteConfig(payload) {
  try {
    const state = yield select();
    const endpoint = `${config.API_URL}/api/config/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.DELETE_CONFIG_SUCCESS, result: data });
    yield put(actions.getConfigs(state.config.list.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.DELETE_CONFIG_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.DELETE_CONFIG_FAIL'),
    });
  }
}

function* editConfig(payload) {
  try {
    const endpoint = `${config.API_URL}/api/config/${payload.params._id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.EDIT_CONFIG_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.EDIT_CONFIG_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.UPDATE_CONFIG_FAIL'),
    });
  }
}

function* createConfig(payload) {
  try {
    const endpoint = `${config.API_URL}/api/config`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.CREATE_CONFIG_SUCCESS, result: data });
    yield put({ type: actions.GET_CONFIG_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({
      type: actions.CREATE_CONFIG_FAILED,
      error: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.CREATE_CONFIG_FAILED'),
    });
    notification.error({
      message: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.CREATE_CONFIG_FAILED'),
    });
  }
}
function* sendTestEmail(payload) {
  try {
    const endpoint = `${config.API_URL}/api/sendTestEmail`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.SEND_TEST_EMAIL_SUCCESS, result: data });
    //yield put({ type: actions.GET_CONFIG_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({
      type: actions.SEND_TEST_EMAIL_FAILED,
      error: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.SEND_TEST_EMAIL_FAILED'),
    });
    notification.error({
      message: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.SEND_TEST_EMAIL_FAILED'),
    });
  }
}
export function* loadConfig() {
  yield takeEvery(actions.GET_CONFIG, fetchConfig);
}

export function* loadConfigs() {
  yield takeEvery(actions.GET_CONFIGS, fetchConfigs);
}

export function* loadDeleteConfig() {
  yield takeEvery(actions.DELETE_CONFIG, deleteConfig);
}

export function* loadEditConfig() {
  yield takeEvery(actions.EDIT_CONFIG, editConfig);
}

export function* loadCreateConfig() {
  yield takeEvery(actions.CREATE_CONFIG, createConfig);
}
export function* loadSendTestEmail() {
  yield takeEvery(actions.SEND_TEST_EMAIL, sendTestEmail);
}
