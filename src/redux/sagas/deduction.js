import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl } from '../../helpers/common.helper';
import { get } from 'lodash';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, putApi } from '../services/api';

export function* fetchDeductionInfos(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/deduction/infos/${payload.params.module_id}`, payload.params ? payload.params : state.deduction.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.deduction.GET_DEDUCTION_INFOS_SUCCESS, list: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.deduction.GET_DEDUCTION_INFOS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_DEDUCTION_INFOS_FAIL')),
    });
  }
}

export function* fetchDeductionInfo(payload) {
  try {
    const endpoint = `${config.API_URL}/api/deduction/info/${payload.params.module_id}/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.deduction.GET_DEDUCTION_INFO_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.deduction.GET_DEDUCTION_INFO_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_DEDUCTION_INFO_FAIL')),
    });
  }
}

export function* fetchDeduction(payload) {
  try {
    const endpoint = `${config.API_URL}/api/deduction/${payload.params.module_id}/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.deduction.GET_DEDUCTION_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.deduction.GET_DEDUCTION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_DEDUCTION_FAIL')),
    });
  }
}

export function* editDeduction(payload) {
  try {
    const endpoint = `${config.API_URL}/api/deduction/${payload.params.module.module_id}/${payload.params.id}`;
    const response = yield call(putApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.deduction.EDIT_DEDUCTION_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.deduction.EDIT_DEDUCTION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.UPDATE_DEDUCTION_FAIL')),
    });
  }
}

export function* editDeductionResult(payload) {
  try {
    const endpoint = `${config.API_URL}/api/deduction/result/${payload.params.module.module_id}/${payload.params.id}`;
    const response = yield call(putApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.deduction.EDIT_DEDUCTION_RESULT_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.deduction.EDIT_DEDUCTION_RESULT_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.UPDATE_DEDUCTION_RESULT_FAIL')),
    });
  }
}

export function* loadDeductionInfos() {
  yield takeEvery(actions.deduction.GET_DEDUCTION_INFOS, fetchDeductionInfos);
}

export function* loadDeductionInfo() {
  yield takeEvery(actions.deduction.GET_DEDUCTION_INFO, fetchDeductionInfo);
}

export function* loadDeduction() {
  yield takeEvery(actions.deduction.GET_DEDUCTION, fetchDeduction);
}

export function* loadEditDeduction() {
  yield takeEvery(actions.deduction.EDIT_DEDUCTION, editDeduction);
}

export function* loadEditDeductionResult() {
  yield takeEvery(actions.deduction.EDIT_DEDUCTION_RESULT, editDeductionResult);
}
