import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { get } from 'lodash';
import { notification } from 'antd';
import { config } from 'config';
import { buildUrl, getUserInformation } from 'helpers/common.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';

export function* fetchNotification(payload) {
  try {
    const endpoint = `${config.API_URL}/api/notification/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.notification.GET_NOTIFICATION_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.GET_NOTIFICATION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thông báo thất bại'),
    });
  }
}

export function* fetchNotifications(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/notifications`, payload.params ? payload.params : state.notification.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.notification.GET_NOTIFICATIONS_SUCCESS, notifications: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.GET_NOTIFICATIONS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thông báo thất bại'),
    });
  }
}

export function* deleteNotification(payload) {
  try {
    const endpoint = `${config.API_URL}/api/notification/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.notification.DELETE_NOTIFICATION_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.DELETE_NOTIFICATION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa dữ liệu thông báo thất bại'),
    });
  }
}

function* deleteNotifications(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-notifications`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.notification.DELETE_NOTIFICATIONS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.DELETE_NOTIFICATIONS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều thông báo thất bại.'),
    });
  }
}

export function* editNotification(payload) {
  try {
    const endpoint = `${config.API_URL}/api/notification/${payload.params.thong_bao_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.notification.EDIT_NOTIFICATION_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.EDIT_NOTIFICATION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật dữ liệu thông báo thất bại'),
    });
  }
}

export function* createNotification(payload) {
  try {
    const endpoint = `${config.API_URL}/api/notification`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.notification.CREATE_NOTIFICATION_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.CREATE_NOTIFICATION_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới dữ liệu thông báo thất bại'),
    });
  }
}

export function* fetchNotificationCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/notification-categories`, payload.params ? payload.params : state.notification.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.notification.GET_NOTIFICATION_CATES_SUCCESS,
      listCates: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.GET_NOTIFICATION_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu nhóm thông báo thất bại'),
    });
  }
}

export function* createNotificationCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/notification-category`;
    const params = {
      tieu_de: payload.params.tieu_de,
      mo_ta: payload.params.mo_ta,
      trang_thai: payload.params.trang_thai,
      ngay_tao: moment().format(),
    };
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({
      type: actions.notification.CREATE_NOTIFICATION_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.CREATE_NOTIFICATION_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới dữ liệu nhóm thông báo thất bại'),
    });
  }
}

export function* editNotificationCate(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/notification-category/${payload.params.nhom_thong_bao_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_d,
      ngay_sua: moment().format(),
    });

    const data = yield response.data;
    yield put({
      type: actions.notification.EDIT_NOTIFICATION_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.EDIT_NOTIFICATION_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật dữ liệu nhóm thông báo thất bại'),
    });
  }
}

export function* deleteNotificationCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/notification-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({
      type: actions.notification.DELETE_NOTIFICATION_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.DELETE_NOTIFICATION_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa dữ liệu nhóm thông báo thất bại'),
    });
  }
}

export function* deleteNotificationCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-notification-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.notification.DELETE_NOTIFICATION_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.notification.DELETE_NOTIFICATION_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục thông báo.'),
    });
  }
}

export function* loadNotification() {
  yield takeEvery(actions.notification.GET_NOTIFICATION, fetchNotification);
}

export function* loadNotifications() {
  yield takeEvery(actions.notification.GET_NOTIFICATIONS, fetchNotifications);
}

export function* loadDeleteNotification() {
  yield takeEvery(actions.notification.DELETE_NOTIFICATION, deleteNotification);
}

export function* loadDeleteNotifications() {
  yield takeEvery(actions.notification.DELETE_NOTIFICATIONS, deleteNotifications);
}

export function* loadEditNotification() {
  yield takeEvery(actions.notification.EDIT_NOTIFICATION, editNotification);
}

export function* loadCreateNotification() {
  yield takeEvery(actions.notification.CREATE_NOTIFICATION, createNotification);
}

export function* loadNotificationCates() {
  yield takeEvery(actions.notification.GET_NOTIFICATION_CATES, fetchNotificationCates);
}

export function* loadCreateNotificationCate() {
  yield takeEvery(actions.notification.CREATE_NOTIFICATION_CATE, createNotificationCate);
}

export function* loadEditNotificationCate() {
  yield takeEvery(actions.notification.EDIT_NOTIFICATION_CATE, editNotificationCate);
}

export function* loadDeleteNotificationCate() {
  yield takeEvery(actions.notification.DELETE_NOTIFICATION_CATE, deleteNotificationCate);
}

export function* loadDeleteNotificationCates() {
  yield takeEvery(actions.notification.DELETE_NOTIFICATION_CATES, deleteNotificationCates);
}
