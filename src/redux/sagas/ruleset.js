import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { buildUrl } from '../../helpers/common.helper';
import { get } from 'lodash';
import { config } from '../../config';
import * as actions from '../actions';
import { getApi } from '../services/api';

export function* fetchRuleset(payload) {
  try {
    const endpoint = `${config.API_URL}/api/rule-set/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.ruleset.GET_RULESET_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.ruleset.GET_RULESET_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Lấy dữ liệu tập luật thất bại.'),
    });
  }
}

export function* fetchRulesets(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/rule-sets`, payload.params ? payload.params : state.ruleset.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.ruleset.GET_RULE_SETS_SUCCESS,
      list: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.ruleset.GET_RULE_SETS_FAILED, error });
    notification.error({
      message: 'Tải danh sách tập luật thất bại',
    });
  }
}

export function* loadRuleset() {
  yield takeEvery(actions.ruleset.GET_RULESET, fetchRuleset);
}

export function* loadRulesets() {
  yield takeEvery(actions.ruleset.GET_RULE_SETS, fetchRulesets);
}
