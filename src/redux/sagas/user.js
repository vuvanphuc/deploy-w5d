import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchUser(payload) {
  try {
    let endpoint = `${config.API_URL}/api/user/${payload.params.nhan_vien_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.user.GET_USER_SUCCESS, result });
    if (payload.params.isUpdateStorage) {
      localStorage.setItem('userInfo', JSON.stringify(result.data));
      window.location.reload();
    }
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.user.GET_USER_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thành viên thất bại'),
    });
  }
}

function* fetchUsers(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/users`, payload.params ? payload.params : state.user.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.user.GET_USERS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.GET_USERS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách thành viên thất bại'),
    });
  }
}

function* deleteUser(payload) {
  try {
    const endpoint = `${config.API_URL}/api/user/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.user.DELETE_USER_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.DELETE_USER_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa thành viên thất bại'),
    });
  }
}

function* deleteUsers(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-users`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.user.DELETE_USERS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.DELETE_USERS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều thành viên thất bại.'),
    });
  }
}
function* importUsers(payload) {
  try {
    const endpoint = `${config.API_URL}/api/import-users`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.user.IMPORT_USERS_SUCCESS, result: data });
    if (payload.onSuccess) {
      payload.onSuccess(data);
    }
  } catch (error) {
    yield put({ type: actions.user.IMPORT_USERS_FAILED, error });
    if (payload.onError) {
      payload.onError();
    }
    notification.error({
      message: get(error, 'response.data.error', 'Import nhiều thành viên thất bại.'),
    });
  }
}
function* editUser(payload) {
  try {
    const endpoint = `${config.API_URL}/api/user/${payload.params.nhan_vien_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.user.EDIT_USER_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.user.EDIT_USER_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật thành viên thất bại'),
    });
  }
}

function* editStatusUsers(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-users`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.user.EDIT_STATUS_USERS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.user.EDIT_STATUS_USERS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều thành viên thất bại.'),
    });
  }
}

function* createUser(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/user`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.user.CREATE_USER_SUCCESS, result: data });
    //yield put({ type: actions.user.GET_USER_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.CREATE_USER_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới thành viên thất bại'),
    });
  }
}

function* fetchUserCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/user-categories`, payload.params ? payload.params : state.user.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.user.GET_USER_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.GET_USER_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục thành viên thất bại'),
    });
  }
}
function* createUserCate(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/user-category`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      ten_nhom: payload.params.ten_nhom,
      nhomnhanviencha_id: payload.params.nhomnhanviencha_id,
      ma_nhom: payload.params.ma_nhom,
      mo_ta: payload.params.mo_ta,
      hien_thi_menu_con: payload.params.hien_thi_menu_con,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.user.CREATE_USER_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.CREATE_USER_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục thành viên thất bại'),
    });
  }
}

function* editUserCate(payload) {
  try {
    const userInfo = getUserInformation();
    const state = yield select();
    const endpoint = `${config.API_URL}/api/user-category/${payload.params.nhom_nhan_vien_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
      ngay_sua: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.user.EDIT_USER_CATE_SUCCESS, result: data });
    yield put(actions.user.getUserCates(state.user.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.EDIT_USER_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục thành viên thất bại'),
    });
  }
}

function* deleteUserCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/user-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.user.DELETE_USER_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.DELETE_USER_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục thành viên thất bại'),
    });
  }
}
function* deleteUserCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-user-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.user.DELETE_USER_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.DELETE_USER_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục thành viên.'),
    });
  }
}

function* login(payload) {
  try {
    const endpoint = config.API_URL + '/api/user/login';
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    if (data.success) {
      yield put({ type: actions.user.LOGIN_USER_SUCCESS, result: data });
      localStorage.setItem('userToken', data.token);
      localStorage.setItem('userInfo', JSON.stringify(data.data));
      if (payload.callback) {
        payload.callback(data);
      }
    } else {
      yield put({ type: actions.user.LOGIN_USER_FAILED, error: data.msg });
      notification.error({
        message: getLangText('MESSAGE.USER_LOGIN_FAIL'),
      });
    }
  } catch (error) {
    yield put({
      type: actions.user.LOGIN_USER_FAILED,
      error: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.USER_LOGIN_FAIL'),
    });
    notification.error({
      message: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.USER_LOGIN_FAIL'),
    });
  }
}

function* logout(payload) {
  localStorage.removeItem('userToken');
  localStorage.removeItem('userInfo');
  localStorage.removeItem('formInfo');
  localStorage.removeItem('configInfo');
  localStorage.removeItem('currentOpenKey');
  localStorage.removeItem('categoryInfo');
  try {
    const endpoint = config.API_URL + '/api/user/logout';
    const response = yield call(postApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.user.LOGOUT_USER_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.LOGOUT_USER_FAILED, error });
  }
}

function* verify(payload) {
  try {
    const endpoint = config.API_URL + '/api/user-confirmation/' + payload.params.id;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.user.VERIFY_USER_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.VERIFY_USER_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xác minh tài khoản thất bại'),
    });
  }
}

function* forgotPassword(payload) {
  try {
    const endpoint = config.API_URL + '/api/user/forgot-password';
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.user.FORGOT_PASSWORD_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({
      type: actions.user.FORGOT_PASSWORD_FAILED,
      error: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.FORGOT_PASSWORD_FAILED'),
    });
    notification.error({
      message: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.FORGOT_PASSWORD_FAILED'),
    });
  }
}

function* changePassword(payload) {
  try {
    const endpoint = config.API_URL + '/api/user/change-password';
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.user.CHANGE_PASSWORD_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.CHANGE_PASSWORD_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Thay đổi mật khẩu thất bại'),
    });
  }
}

function* getUserStatistics(payload) {
  try {
    const endpoint = config.API_URL + '/api/user-statistics';
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.user.GET_USER_STATISTICS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.user.GET_USER_STATISTICS_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.GET_USER_STATISTICS_FAILED'),
    });
  }
}
function* registerUser(payload) {
  try {
    const endpoint = `${config.API_URL}/api/user/regUser`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.user.REGISTER_USER_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({
      type: actions.user.REGISTER_USER_FAILED,
      error: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.REGISTER_USER_FAILED'),
    });
    notification.error({
      message: error.response.data.error ? error.response.data.error : getLangText('MESSAGE.REGISTER_USER_FAILED'),
    });
  }
}
export function* loadUser() {
  yield takeEvery(actions.user.GET_USER, fetchUser);
}

export function* loadUsers() {
  yield takeEvery(actions.user.GET_USERS, fetchUsers);
}

export function* loadDeleteUser() {
  yield takeEvery(actions.user.DELETE_USER, deleteUser);
}

export function* loadDeleteUsers() {
  yield takeEvery(actions.user.DELETE_USERS, deleteUsers);
}

export function* loadEditUser() {
  yield takeEvery(actions.user.EDIT_USER, editUser);
}

export function* loadEditStatusUser() {
  yield takeEvery(actions.user.EDIT_STATUS_USERS, editStatusUsers);
}

export function* loadCreateUser() {
  yield takeEvery(actions.user.CREATE_USER, createUser);
}

export function* loadUserCates() {
  yield takeEvery(actions.user.GET_USER_CATES, fetchUserCates);
}
export function* loadCreateUserCate() {
  yield takeEvery(actions.user.CREATE_USER_CATE, createUserCate);
}

export function* loadEditUserCate() {
  yield takeEvery(actions.user.EDIT_USER_CATE, editUserCate);
}

export function* loadDeleteUserCate() {
  yield takeEvery(actions.user.DELETE_USER_CATE, deleteUserCate);
}

export function* loadDeleteUserCates() {
  yield takeEvery(actions.user.DELETE_USER_CATES, deleteUserCates);
}

export function* loadLogin() {
  yield takeEvery(actions.user.LOGIN_USER, login);
}

export function* loadLogout() {
  yield takeEvery(actions.user.LOGOUT_USER, logout);
}

export function* loadVerify() {
  yield takeEvery(actions.user.VERIFY_USER, verify);
}

export function* loadForgotPassword() {
  yield takeEvery(actions.user.FORGOT_PASSWORD, forgotPassword);
}

export function* loadChangePassword() {
  yield takeEvery(actions.user.CHANGE_PASSWORD, changePassword);
}

export function* loadUserStatistics() {
  yield takeEvery(actions.user.GET_USER_STATISTICS, getUserStatistics);
}
export function* loadRegisterUser() {
  yield takeEvery(actions.user.REGISTER_USER, registerUser);
}
export function* loadImportUsers() {
  yield takeEvery(actions.user.IMPORT_USERS, importUsers);
}
