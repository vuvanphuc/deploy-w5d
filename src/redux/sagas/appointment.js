import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchAppointment(payload) {
  try {
    let endpoint = `${config.API_URL}/api/appointment/${payload.params.nhan_vien_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.appointment.GET_APPOINTMENT_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.appointment.GET_APPOINTMENT_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu lịch hẹn thất bại'),
    });
  }
}

function* fetchAppointments(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/appointments`, payload.params ? payload.params : state.appointment.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.appointment.GET_APPOINTMENTS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.GET_APPOINTMENTS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách lịch hẹn thất bại'),
    });
  }
}

function* deleteAppointment(payload) {
  try {
    const endpoint = `${config.API_URL}/api/appointment/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.appointment.DELETE_APPOINTMENT_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.DELETE_APPOINTMENT_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa lịch hẹn thất bại'),
    });
  }
}

function* deleteAppointments(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-appointments`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.appointment.DELETE_APPOINTMENTS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.DELETE_APPOINTMENTS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều lịch hẹn thất bại.'),
    });
  }
}
function* importAppointments(payload) {
  try {
    const endpoint = `${config.API_URL}/api/import-appointments`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.appointment.IMPORT_APPOINTMENTS_SUCCESS, result: data });
    if (payload.onSuccess) {
      payload.onSuccess(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.IMPORT_APPOINTMENTS_FAILED, error });
    if (payload.onError) {
      payload.onError();
    }
    notification.error({
      message: get(error, 'response.data.error', 'Import nhiều lịch hẹn thất bại.'),
    });
  }
}
function* editAppointment(payload) {
  try {
    const endpoint = `${config.API_URL}/api/appointment/${payload.params.lich_hen_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.appointment.EDIT_APPOINTMENT_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.appointment.EDIT_APPOINTMENT_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật lịch hẹn thất bại'),
    });
  }
}

function* editStatusAppointments(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-appointments`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.appointment.EDIT_STATUS_APPOINTMENTS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.appointment.EDIT_STATUS_APPOINTMENTS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều lịch hẹn thất bại.'),
    });
  }
}

function* createAppointment(payload) {
  try {
    const endpoint = `${config.API_URL}/api/appointment`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.appointment.CREATE_APPOINTMENT_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.CREATE_APPOINTMENT_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới lịch hẹn thất bại'),
    });
  }
}

function* fetchAppointmentCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/appointment-categories`, payload.params ? payload.params : state.appointment.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.appointment.GET_APPOINTMENT_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.GET_APPOINTMENT_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục lịch hẹn thất bại'),
    });
  }
}

function* createAppointmentCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/appointment-category`;
    const params = payload.params;
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({ type: actions.appointment.CREATE_APPOINTMENT_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.CREATE_APPOINTMENT_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục lịch hẹn thất bại'),
    });
  }
}

function* editAppointmentCate(payload) {
  try {
    const state = yield select();
    const endpoint = `${config.API_URL}/api/appointment-category/${payload.params.nhom_lich_hen_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.appointment.EDIT_APPOINTMENT_CATE_SUCCESS, result: data });
    yield put(actions.appointment.getAppointmentCates(state.appointment.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.EDIT_APPOINTMENT_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục lịch hẹn thất bại'),
    });
  }
}

function* deleteAppointmentCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/appointment-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.appointment.DELETE_APPOINTMENT_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.DELETE_APPOINTMENT_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục lịch hẹn thất bại'),
    });
  }
}
function* deleteAppointmentCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-appointment-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.appointment.DELETE_APPOINTMENT_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.appointment.DELETE_APPOINTMENT_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục lịch hẹn.'),
    });
  }
}

export function* loadAppointment() {
  yield takeEvery(actions.appointment.GET_APPOINTMENT, fetchAppointment);
}

export function* loadAppointments() {
  yield takeEvery(actions.appointment.GET_APPOINTMENTS, fetchAppointments);
}

export function* loadDeleteAppointment() {
  yield takeEvery(actions.appointment.DELETE_APPOINTMENT, deleteAppointment);
}

export function* loadDeleteAppointments() {
  yield takeEvery(actions.appointment.DELETE_APPOINTMENTS, deleteAppointments);
}

export function* loadEditAppointment() {
  yield takeEvery(actions.appointment.EDIT_APPOINTMENT, editAppointment);
}

export function* loadEditStatusAppointment() {
  yield takeEvery(actions.appointment.EDIT_STATUS_APPOINTMENTS, editStatusAppointments);
}

export function* loadCreateAppointment() {
  yield takeEvery(actions.appointment.CREATE_APPOINTMENT, createAppointment);
}

export function* loadAppointmentCates() {
  yield takeEvery(actions.appointment.GET_APPOINTMENT_CATES, fetchAppointmentCates);
}
export function* loadCreateAppointmentCate() {
  yield takeEvery(actions.appointment.CREATE_APPOINTMENT_CATE, createAppointmentCate);
}

export function* loadEditAppointmentCate() {
  yield takeEvery(actions.appointment.EDIT_APPOINTMENT_CATE, editAppointmentCate);
}

export function* loadDeleteAppointmentCate() {
  yield takeEvery(actions.appointment.DELETE_APPOINTMENT_CATE, deleteAppointmentCate);
}

export function* loadDeleteAppointmentCates() {
  yield takeEvery(actions.appointment.DELETE_APPOINTMENT_CATES, deleteAppointmentCates);
}

export function* loadImportAppointments() {
  yield takeEvery(actions.appointment.IMPORT_APPOINTMENTS, importAppointments);
}
