import { call, put, takeEvery } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi } from '../services/api';

function* getGlobalStatistics(payload) {
  try {
    const endpoint = config.API_URL + '/api/statistics';
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.statistic.GET_GLOBAL_STATISTICS_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.statistic.GET_GLOBAL_STATISTICS_SUCCESS, error });
    notification.error({
      message: 'Lấy dữ liệu thống kê thất bại.',
    });
  }
}

function* getModuleStatistics(payload) {
  try {
    const endpoint = config.API_URL + '/api/statistics/modules';
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.statistic.GET_MODULE_STATISTICS_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.statistic.GET_MODULE_STATISTICS_FAILED, error });
    notification.error({
      message: 'Lấy dữ liệu thống kê thất bại.',
    });
  }
}

function* getProjectStatistics(payload) {
  try {
    const endpoint = config.API_URL + '/api/statistic/project';
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.statistic.GET_PROJECT_STATISTICS_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.statistic.GET_PROJECT_STATISTICS_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.GET_PROJECT_STATISTICS_FAILED'),
    });
  }
}

function* getModuleLatestRecord(payload) {
  try {
    const endpoint = config.API_URL + '/api/statistics/modules/latest-record';
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.statistic.GET_MODULE_LATEST_RECORD_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.statistic.GET_MODULE_LATEST_RECORD_FAILED, error });
    notification.error({
      message: 'Lấy dữ liệu thống kê thất bại.',
    });
  }
}

function* getModuleEdgeRecord(payload) {
  try {
    const endpoint = config.API_URL + '/api/statistics/modules/edge-records';
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.statistic.GET_MODULE_EDGE_RECORD_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.statistic.GET_MODULE_EDGE_RECORD_FAILED, error });
    notification.error({
      message: 'Lấy dữ liệu thống kê thất bại.',
    });
  }
}

export function* loadGlobalStatistics() {
  yield takeEvery(actions.statistic.GET_GLOBAL_STATISTICS, getGlobalStatistics);
}

export function* loadModuleStatistics() {
  yield takeEvery(actions.statistic.GET_MODULE_STATISTICS, getModuleStatistics);
}

export function* loadProjectStatistics() {
  yield takeEvery(actions.statistic.GET_PROJECT_STATISTICS, getProjectStatistics);
}

export function* loadLatestStatistics() {
  yield takeEvery(actions.statistic.GET_MODULE_LATEST_RECORD, getModuleLatestRecord);
}

export function* loadEdgeStatistics() {
  yield takeEvery(actions.statistic.GET_MODULE_EDGE_RECORD, getModuleEdgeRecord);
}
