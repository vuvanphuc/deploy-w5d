import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchPost(payload) {
  try {
    let endpoint = `${config.API_URL}/api/post/${payload.params.tin_tuc_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.post.GET_POST_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.post.GET_POST_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải bài viết thất bại'),
    });
  }
}

function* fetchPosts(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/posts`, payload.params ? payload.params : state.post.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.post.GET_POSTS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.GET_POSTS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách bài viết thất bại'),
    });
  }
}

function* deletePost(payload) {
  try {
    const endpoint = `${config.API_URL}/api/post/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.post.DELETE_POST_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.DELETE_POST_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa bài viết thất bại'),
    });
  }
}

function* deletePosts(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-posts`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.post.DELETE_POSTS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.DELETE_POSTS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều bài viết thất bại.'),
    });
  }
}
function* editPost(payload) {
  try {
    const endpoint = `${config.API_URL}/api/post/${payload.params.tin_tuc_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.post.EDIT_POST_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.post.EDIT_POST_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật bài viết thất bại'),
    });
  }
}

function* editStatusPosts(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-posts`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.post.EDIT_STATUS_POSTS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.post.EDIT_STATUS_POSTS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều bài viết thất bại.'),
    });
  }
}

function* createPost(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/post`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.post.CREATE_POST_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.CREATE_POST_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới bài viết thất bại'),
    });
  }
}

function* fetchPostCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/post-categories`, payload.params ? payload.params : state.post.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.post.GET_POST_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.GET_POST_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục bài viết thất bại'),
    });
  }
}

function* createPostCate(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/post-category`;
    const params = {
      ten_nhom: payload.params.ten_nhom,
      nhom_cha_id: payload.params.nhom_cha_id,
      mo_ta: payload.params.mo_ta,
      anh_dai_dien: payload.params.anh_dai_dien,
      trang_thai: payload.params.trang_thai,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    };
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({ type: actions.post.CREATE_POST_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.CREATE_POST_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục bài viết thất bại'),
    });
  }
}

function* editPostCate(payload) {
  try {
    const userInfo = getUserInformation();
    const state = yield select();
    const endpoint = `${config.API_URL}/api/post-category/${payload.params.nhom_tin_tuc_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
      ngay_sua: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.post.EDIT_POST_CATE_SUCCESS, result: data });
    yield put(actions.post.getPostCates(state.post.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.EDIT_POST_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục bài viết thất bại'),
    });
  }
}

function* deletePostCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/post-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.post.DELETE_POST_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.DELETE_POST_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục bài viết thất bại'),
    });
  }
}
function* deletePostCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-post-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.post.DELETE_POST_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.post.DELETE_POST_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục bài viết.'),
    });
  }
}

export function* loadPost() {
  yield takeEvery(actions.post.GET_POST, fetchPost);
}

export function* loadPosts() {
  yield takeEvery(actions.post.GET_POSTS, fetchPosts);
}

export function* loadDeletePost() {
  yield takeEvery(actions.post.DELETE_POST, deletePost);
}

export function* loadDeletePosts() {
  yield takeEvery(actions.post.DELETE_POSTS, deletePosts);
}

export function* loadEditPost() {
  yield takeEvery(actions.post.EDIT_POST, editPost);
}

export function* loadEditStatusPost() {
  yield takeEvery(actions.post.EDIT_STATUS_POSTS, editStatusPosts);
}

export function* loadCreatePost() {
  yield takeEvery(actions.post.CREATE_POST, createPost);
}

export function* loadPostCates() {
  yield takeEvery(actions.post.GET_POST_CATES, fetchPostCates);
}
export function* loadCreatePostCate() {
  yield takeEvery(actions.post.CREATE_POST_CATE, createPostCate);
}

export function* loadEditPostCate() {
  yield takeEvery(actions.post.EDIT_POST_CATE, editPostCate);
}

export function* loadDeletePostCate() {
  yield takeEvery(actions.post.DELETE_POST_CATE, deletePostCate);
}

export function* loadDeletePostCates() {
  yield takeEvery(actions.post.DELETE_POST_CATES, deletePostCates);
}
