import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchCook(payload) {
  try {
    let endpoint = `${config.API_URL}/api/cook/${payload.params.thuc_don_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.cook.GET_COOK_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.cook.GET_COOK_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải thực đơn thất bại'),
    });
  }
}

function* fetchCooks(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/cooks`, payload.params ? payload.params : state.cook.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.cook.GET_COOKS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.GET_COOKS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách thực đơn thất bại'),
    });
  }
}

function* deleteCook(payload) {
  try {
    const endpoint = `${config.API_URL}/api/cook/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.cook.DELETE_COOK_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.DELETE_COOK_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa thực đơn thất bại'),
    });
  }
}

function* deleteCooks(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-cooks`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.cook.DELETE_COOKS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.DELETE_COOKS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều thực đơn thất bại.'),
    });
  }
}
function* editCook(payload) {
  try {
    const endpoint = `${config.API_URL}/api/cook/${payload.params.thuc_don_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.cook.EDIT_COOK_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.cook.EDIT_COOK_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật thực đơn thất bại'),
    });
  }
}

function* editStatusCooks(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-cooks`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.cook.EDIT_STATUS_COOKS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.cook.EDIT_STATUS_COOKS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều thực đơn thất bại.'),
    });
  }
}

function* createCook(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/cook`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.cook.CREATE_COOK_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.CREATE_COOK_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới thực đơn thất bại'),
    });
  }
}

function* fetchCookCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/cook-categories`, payload.params ? payload.params : state.cook.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.cook.GET_COOK_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.GET_COOK_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục thực đơn thất bại'),
    });
  }
}

function* createCookCate(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/cook-category`;
    const params = {
      ten_nhom: payload.params.ten_nhom,
      nhom_cha_id: payload.params.nhom_cha_id,
      mo_ta: payload.params.mo_ta,
      anh_dai_dien: payload.params.anh_dai_dien,
      trang_thai: payload.params.trang_thai,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    };
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({ type: actions.cook.CREATE_COOK_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.CREATE_COOK_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục thực đơn thất bại'),
    });
  }
}

function* editCookCate(payload) {
  try {
    const userInfo = getUserInformation();
    const state = yield select();
    const endpoint = `${config.API_URL}/api/cook-category/${payload.params.nhom_thuc_don_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
      ngay_sua: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.cook.EDIT_COOK_CATE_SUCCESS, result: data });
    yield put(actions.cook.getCookCates(state.cook.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.EDIT_COOK_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục thực đơn thất bại'),
    });
  }
}

function* deleteCookCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/cook-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.cook.DELETE_COOK_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.DELETE_COOK_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục thực đơn thất bại'),
    });
  }
}
function* deleteCookCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-cook-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.cook.DELETE_COOK_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.cook.DELETE_COOK_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục thực đơn.'),
    });
  }
}

export function* loadCook() {
  yield takeEvery(actions.cook.GET_COOK, fetchCook);
}

export function* loadCooks() {
  yield takeEvery(actions.cook.GET_COOKS, fetchCooks);
}

export function* loadDeleteCook() {
  yield takeEvery(actions.cook.DELETE_COOK, deleteCook);
}

export function* loadDeleteCooks() {
  yield takeEvery(actions.cook.DELETE_COOKS, deleteCooks);
}

export function* loadEditCook() {
  yield takeEvery(actions.cook.EDIT_COOK, editCook);
}

export function* loadEditStatusCook() {
  yield takeEvery(actions.cook.EDIT_STATUS_COOKS, editStatusCooks);
}

export function* loadCreateCook() {
  yield takeEvery(actions.cook.CREATE_COOK, createCook);
}

export function* loadCookCates() {
  yield takeEvery(actions.cook.GET_COOK_CATES, fetchCookCates);
}
export function* loadCreateCookCate() {
  yield takeEvery(actions.cook.CREATE_COOK_CATE, createCookCate);
}

export function* loadEditCookCate() {
  yield takeEvery(actions.cook.EDIT_COOK_CATE, editCookCate);
}

export function* loadDeleteCookCate() {
  yield takeEvery(actions.cook.DELETE_COOK_CATE, deleteCookCate);
}

export function* loadDeleteCookCates() {
  yield takeEvery(actions.cook.DELETE_COOK_CATES, deleteCookCates);
}
