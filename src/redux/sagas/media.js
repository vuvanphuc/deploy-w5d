import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from 'config';
import { buildUrl } from 'helpers/common.helper';
import { getLangText } from 'helpers/language.helper';
import * as actions from 'redux/actions';
import { getApi, uploadApi, deleteApi, putApi, postApi } from 'redux/services/api';

function* fetchMedia(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/media/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.media.GET_MEDIA_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.GET_MEDIA_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.GET_MEDIA_FAIL'),
    });
  }
}

function* fetchMedias(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.UPLOAD_API_URL}/api/medias`, payload.params ? payload.params : state.media.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.media.GET_MEDIAS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.GET_MEDIAS_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.GET_MEDIAS_FAIL'),
    });
  }
}

function* deleteMedia(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/media/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.media.DELETE_MEDIA_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.DELETE_MEDIA_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.DELETE_MEDIA_FAIL'),
    });
  }
}

function* deleteMedias(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/delete-medias`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.media.DELETE_MEDIAS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.DELETE_MEDIAS_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.DELETE_MEDIAS_FAIL'),
    });
  }
}

function* moveMedias(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/move-medias`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.media.MOVE_MEDIAS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.MOVE_MEDIAS_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.MOVE_MEDIAS_FAIL'),
    });
  }
}

function* editMedia(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/media/${payload.params.tep_tin_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.media.EDIT_MEDIA_SUCCESS, result: data });
    yield put({ type: actions.media.GET_MEDIA_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.EDIT_MEDIA_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.UPDATE_MEDIA_FAIL'),
    });
  }
}

function* createMedia(payload) {
  try {
    let endpoint = `${config.UPLOAD_API_URL}/api/media`;
    if (payload.params.server === 'minio') {
      endpoint += `/minio`;
    }
    const params = {
      fileUpload: payload.params.fileUpload,
      folder: payload.params.folder,
      bucket: payload.params.minio_bucket,
    };
    const response = yield call(uploadApi, endpoint, params);
    const data = yield response.data;
    yield put({ type: actions.media.CREATE_MEDIA_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.CREATE_MEDIA_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.CREATE_MEDIA_FAIL'),
    });
  }
}

export function* fetchMediaCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.UPLOAD_API_URL}/api/media-categories`, payload.params ? payload.params : state.media.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.media.GET_MEDIA_CATES_SUCCESS,
      listCates: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.GET_MEDIA_CATES_FAILED, error });
    notification.error({
      message: 'Tải danh sách danh mục biểu mẫu thất bại',
    });
  }
}

export function* createMediaCate(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/media-category`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.media.CREATE_MEDIA_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.CREATE_MEDIA_CATE_FAILED, error });
    notification.error({
      message: error.response.data.error,
    });
  }
}

export function* editMediaCate(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/media-category/${payload.params.nhom_tep_tin_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.media.EDIT_MEDIA_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.EDIT_MEDIA_CATE_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.UPDATE_MEDIA_CATE_FAIL'),
    });
  }
}

export function* deleteMediaCate(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/media-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({
      type: actions.media.DELETE_MEDIA_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.DELETE_MEDIA_CATE_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.DELETE_MEDIA_CATE_FAIL'),
    });
  }
}

export function* deleteMediaCates(payload) {
  try {
    const endpoint = `${config.UPLOAD_API_URL}/api/delete-media-categories`;

    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.media.DELETE_MEDIA_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.media.DELETE_MEDIA_CATES_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.DELETE_MEDIA_CATE_FAIL'),
    });
  }
}

export function* loadMedia() {
  yield takeEvery(actions.media.GET_MEDIA, fetchMedia);
}

export function* loadMedias() {
  yield takeEvery(actions.media.GET_MEDIAS, fetchMedias);
}

export function* loadDeleteMedia() {
  yield takeEvery(actions.media.DELETE_MEDIA, deleteMedia);
}
export function* loadDeleteMedias() {
  yield takeEvery(actions.media.DELETE_MEDIAS, deleteMedias);
}
export function* loadMoveMedias() {
  yield takeEvery(actions.media.MOVE_MEDIAS, moveMedias);
}

export function* loadEditMedia() {
  yield takeEvery(actions.media.EDIT_MEDIA, editMedia);
}

export function* loadCreateMedia() {
  yield takeEvery(actions.media.CREATE_MEDIA, createMedia);
}

export function* loadMediaCates() {
  yield takeEvery(actions.media.GET_MEDIA_CATES, fetchMediaCates);
}

export function* loadCreateMediaCate() {
  yield takeEvery(actions.media.CREATE_MEDIA_CATE, createMediaCate);
}

export function* loadEditMediaCate() {
  yield takeEvery(actions.media.EDIT_MEDIA_CATE, editMediaCate);
}

export function* loadDeleteMediaCate() {
  yield takeEvery(actions.media.DELETE_MEDIA_CATE, deleteMediaCate);
}
export function* loadDeleteMediaCates() {
  yield takeEvery(actions.media.DELETE_MEDIA_CATES, deleteMediaCates);
}
