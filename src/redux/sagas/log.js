import { call, put, takeEvery } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi } from '../services/api';

function* fetchLogs(payload) {
  try {
    let endpoint = `${config.API_URL}/api/logs?version=lastest`;
    if (payload.params.nhan_vien_id) {
      endpoint += `&nhan_vien_id=${payload.params.nhan_vien_id}`;
    }
    if (payload.params.bang_du_lieu) {
      endpoint += `&bang_du_lieu=${payload.params.bang_du_lieu}`;
    }
    if (payload.params.du_lieu_id) {
      endpoint += `&du_lieu_id=${payload.params.du_lieu_id}`;
    }
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.log.GET_LOGS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.log.GET_LOGS_FAILED, error });
    notification.error({
      message: getLangText('MESSAGE.GET_LOGS_FAIL'),
    });
  }
}

export function* loadLogs() {
  yield takeEvery(actions.log.GET_LOGS, fetchLogs);
}
