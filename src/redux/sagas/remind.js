import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchRemind(payload) {
  try {
    let endpoint = `${config.API_URL}/api/remind/${payload.params.nhan_vien_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.remind.GET_REMIND_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.remind.GET_REMIND_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu nhắc nhở thất bại'),
    });
  }
}

function* fetchReminds(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/reminds`, payload.params ? payload.params : state.remind.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.remind.GET_REMINDS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.GET_REMINDS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách nhắc nhở thất bại'),
    });
  }
}

function* deleteRemind(payload) {
  try {
    const endpoint = `${config.API_URL}/api/remind/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.remind.DELETE_REMIND_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.DELETE_REMIND_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhắc nhở thất bại'),
    });
  }
}

function* deleteReminds(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-reminds`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.remind.DELETE_REMINDS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.DELETE_REMINDS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều nhắc nhở thất bại.'),
    });
  }
}
function* importReminds(payload) {
  try {
    const endpoint = `${config.API_URL}/api/import-reminds`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.remind.IMPORT_REMINDS_SUCCESS, result: data });
    if (payload.onSuccess) {
      payload.onSuccess(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.IMPORT_REMINDS_FAILED, error });
    if (payload.onError) {
      payload.onError();
    }
    notification.error({
      message: get(error, 'response.data.error', 'Import nhiều nhắc nhở thất bại.'),
    });
  }
}
function* editRemind(payload) {
  try {
    const endpoint = `${config.API_URL}/api/remind/${payload.params.nhac_nho_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.remind.EDIT_REMIND_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.remind.EDIT_REMIND_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật nhắc nhở thất bại'),
    });
  }
}

function* editStatusReminds(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-reminds`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.remind.EDIT_STATUS_REMINDS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.remind.EDIT_STATUS_REMINDS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều nhắc nhở thất bại.'),
    });
  }
}

function* createRemind(payload) {
  try {
    const endpoint = `${config.API_URL}/api/remind`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.remind.CREATE_REMIND_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.CREATE_REMIND_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới nhắc nhở thất bại'),
    });
  }
}

function* fetchRemindCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/remind-categories`, payload.params ? payload.params : state.remind.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.remind.GET_REMIND_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.GET_REMIND_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục nhắc nhở thất bại'),
    });
  }
}

function* createRemindCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/remind-category`;
    const params = payload.params;
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({ type: actions.remind.CREATE_REMIND_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.CREATE_REMIND_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục nhắc nhở thất bại'),
    });
  }
}

function* editRemindCate(payload) {
  try {
    const state = yield select();
    const endpoint = `${config.API_URL}/api/remind-category/${payload.params.nhom_nhac_nho_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.remind.EDIT_REMIND_CATE_SUCCESS, result: data });
    yield put(actions.remind.getRemindCates(state.remind.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.EDIT_REMIND_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục nhắc nhở thất bại'),
    });
  }
}

function* deleteRemindCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/remind-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.remind.DELETE_REMIND_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.DELETE_REMIND_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục nhắc nhở thất bại'),
    });
  }
}
function* deleteRemindCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-remind-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.remind.DELETE_REMIND_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.remind.DELETE_REMIND_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục nhắc nhở.'),
    });
  }
}

export function* loadRemind() {
  yield takeEvery(actions.remind.GET_REMIND, fetchRemind);
}

export function* loadReminds() {
  yield takeEvery(actions.remind.GET_REMINDS, fetchReminds);
}

export function* loadDeleteRemind() {
  yield takeEvery(actions.remind.DELETE_REMIND, deleteRemind);
}

export function* loadDeleteReminds() {
  yield takeEvery(actions.remind.DELETE_REMINDS, deleteReminds);
}

export function* loadEditRemind() {
  yield takeEvery(actions.remind.EDIT_REMIND, editRemind);
}

export function* loadEditStatusRemind() {
  yield takeEvery(actions.remind.EDIT_STATUS_REMINDS, editStatusReminds);
}

export function* loadCreateRemind() {
  yield takeEvery(actions.remind.CREATE_REMIND, createRemind);
}

export function* loadRemindCates() {
  yield takeEvery(actions.remind.GET_REMIND_CATES, fetchRemindCates);
}
export function* loadCreateRemindCate() {
  yield takeEvery(actions.remind.CREATE_REMIND_CATE, createRemindCate);
}

export function* loadEditRemindCate() {
  yield takeEvery(actions.remind.EDIT_REMIND_CATE, editRemindCate);
}

export function* loadDeleteRemindCate() {
  yield takeEvery(actions.remind.DELETE_REMIND_CATE, deleteRemindCate);
}

export function* loadDeleteRemindCates() {
  yield takeEvery(actions.remind.DELETE_REMIND_CATES, deleteRemindCates);
}

export function* loadImportReminds() {
  yield takeEvery(actions.remind.IMPORT_REMINDS, importReminds);
}
