import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import { get } from 'lodash';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';

export function* fetchModule(payload) {
  try {
    const endpoint = `${config.API_URL}/api/module/${payload.params.module_id}/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.GET_MODULE_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.GET_MODULE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_MODULE_FAIL')),
    });
  }
}

export function* exportModule(payload) {
  try {
    const endpoint = `${config.API_URL}/api/module/export/${payload.params.module_id}/${payload.params.module_data_id}`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.EXPORT_MODULE_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.EXPORT_MODULE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_MODULE_FAIL')),
    });
  }
}

export function* fetchModules(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/modules/${payload.params.module_id}`, payload.params ? payload.params : state.module.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.GET_MODULES_SUCCESS, modules: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.GET_MODULES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_MODULES_FAIL')),
    });
  }
}

export function* deleteModule(payload) {
  try {
    const endpoint = `${config.API_URL}/api/module/${payload.params.module_id}/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.DELETE_MODULE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.DELETE_MODULE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.DELETE_MODULE_FAIL')),
    });
  }
}

export function* deleteModules(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-modules/${payload.params.module_id}`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.DELETE_MODULES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.DELETE_MODULES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.DELETE_MODULES_FAIL')),
    });
  }
}

export function* editModulesStatus(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-modules-status/${payload.params.module_id}`;
    const response = yield call(postApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.EDIT_MODULES_STATUS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.EDIT_MODULES_STATUS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.EDIT_MODULES_STATUS_FAILED')),
    });
  }
}

export function* editModule(payload) {
  try {
    const endpoint = `${config.API_URL}/api/module/${payload.params.module.module_id}/${payload.params.id}`;
    const response = yield call(putApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.EDIT_MODULE_SUCCESS, result: data });
    yield put({ type: actions.moduleApp.GET_MODULE_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.EDIT_MODULE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.UPDATE_MODULE_FAIL')),
    });
  }
}

export function* createModule(payload) {
  try {
    const endpoint = `${config.API_URL}/api/module/${payload.params.module.module_id}`;
    const response = yield call(postApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.moduleApp.CREATE_MODULE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.CREATE_MODULE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.CREATE_MODULE_FAIL')),
    });
  }
}

export function* fetchModuleCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/module-categories`, payload.params ? payload.params : state.module.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.moduleApp.GET_MODULE_CATES_SUCCESS,
      listCates: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.GET_MODULE_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_MODULE_CATES_FAIL')),
    });
  }
}

export function* createModuleCate(payload) {
  try {
    const state = yield select();
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/module-category`;
    const params = {
      title: payload.params.title,
      description: payload.params.description,
      author: userInfo._id,
      status: payload.params.status,
    };
    const response = yield call(postApi, endpoint, params);
    const data = yield response.data;
    yield put({
      type: actions.moduleApp.CREATE_MODULE_CATE_SUCCESS,
      result: data,
    });
    yield put(actions.moduleApp.getModuleCates(state.module.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.CREATE_MODULE_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.CREATE_MODULE_CATE_FAIL')),
    });
  }
}

export function* editModuleCate(payload) {
  try {
    const state = yield select();
    const endpoint = `${config.API_URL}/api/module-category/${payload.params._id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.moduleApp.EDIT_MODULE_CATE_SUCCESS,
      result: data,
    });
    yield put(actions.moduleApp.getModuleCates(state.module.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.EDIT_MODULE_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.UPDATE_MODULE_CATE_FAIL')),
    });
  }
}

export function* deleteModuleCate(payload) {
  try {
    const state = yield select();
    const endpoint = `${config.API_URL}/api/module-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({
      type: actions.moduleApp.DELETE_MODULE_CATE_SUCCESS,
      result: data,
    });
    yield put(actions.moduleApp.getModuleCates(state.module.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.moduleApp.DELETE_MODULE_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.DELETE_MODULE_CATE_FAIL')),
    });
  }
}

export function* loadModule() {
  yield takeEvery(actions.moduleApp.GET_MODULE, fetchModule);
}

export function* loadExportModule() {
  yield takeEvery(actions.moduleApp.EXPORT_MODULE, exportModule);
}

export function* loadModules() {
  yield takeEvery(actions.moduleApp.GET_MODULES, fetchModules);
}

export function* loadDeleteModule() {
  yield takeEvery(actions.moduleApp.DELETE_MODULE, deleteModule);
}

export function* loadDeleteModules() {
  yield takeEvery(actions.moduleApp.DELETE_MODULES, deleteModules);
}

export function* loadEditModulesStatus() {
  yield takeEvery(actions.moduleApp.EDIT_MODULES_STATUS, editModulesStatus);
}

export function* loadEditModule() {
  yield takeEvery(actions.moduleApp.EDIT_MODULE, editModule);
}

export function* loadCreateModule() {
  yield takeEvery(actions.moduleApp.CREATE_MODULE, createModule);
}

export function* loadModuleCates() {
  yield takeEvery(actions.moduleApp.GET_MODULE_CATES, fetchModuleCates);
}

export function* loadCreateModuleCate() {
  yield takeEvery(actions.moduleApp.CREATE_MODULE_CATE, createModuleCate);
}

export function* loadEditModuleCate() {
  yield takeEvery(actions.moduleApp.EDIT_MODULE_CATE, editModuleCate);
}

export function* loadDeleteModuleCate() {
  yield takeEvery(actions.moduleApp.DELETE_MODULE_CATE, deleteModuleCate);
}
