import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchLesson(payload) {
  try {
    let endpoint = `${config.API_URL}/api/lesson/${payload.params.bai_hoc_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.lesson.GET_LESSON_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.lesson.GET_LESSON_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải bài học thất bại'),
    });
  }
}

function* fetchLessons(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/lessons`, payload.params ? payload.params : state.lesson.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.lesson.GET_LESSONS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.GET_LESSONS_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách bài học thất bại'),
    });
  }
}

function* deleteLesson(payload) {
  try {
    const endpoint = `${config.API_URL}/api/lesson/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.lesson.DELETE_LESSON_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.DELETE_LESSON_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa bài học thất bại'),
    });
  }
}

function* deleteLessons(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-lessons`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.lesson.DELETE_LESSONS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.DELETE_LESSONS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều bài học thất bại.'),
    });
  }
}
function* editLesson(payload) {
  try {
    const endpoint = `${config.API_URL}/api/lesson/${payload.params.bai_hoc_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.lesson.EDIT_LESSON_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.lesson.EDIT_LESSON_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật bài học thất bại'),
    });
  }
}

function* editStatusLessons(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-lessons`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.lesson.EDIT_STATUS_LESSONS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.lesson.EDIT_STATUS_LESSONS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều bài học thất bại.'),
    });
  }
}

function* createLesson(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/lesson`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.lesson.CREATE_LESSON_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.CREATE_LESSON_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới bài học thất bại'),
    });
  }
}

function* fetchLessonCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/lesson-categories`, payload.params ? payload.params : state.lesson.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.lesson.GET_LESSON_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.GET_LESSON_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục bài học thất bại'),
    });
  }
}

function* createLessonCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/lesson-category`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.lesson.CREATE_LESSON_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.CREATE_LESSON_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục bài học thất bại'),
    });
  }
}

function* editLessonCate(payload) {
  try {
    const userInfo = getUserInformation();
    const state = yield select();
    const endpoint = `${config.API_URL}/api/lesson-category/${payload.params.nhom_bai_hoc_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
      ngay_sua: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.lesson.EDIT_LESSON_CATE_SUCCESS, result: data });
    yield put(actions.lesson.getLessonCates(state.lesson.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.EDIT_LESSON_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục bài học thất bại'),
    });
  }
}

function* deleteLessonCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/lesson-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.lesson.DELETE_LESSON_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.DELETE_LESSON_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục bài học thất bại'),
    });
  }
}
function* deleteLessonCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-lesson-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.lesson.DELETE_LESSON_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.lesson.DELETE_LESSON_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục bài học.'),
    });
  }
}

export function* loadLesson() {
  yield takeEvery(actions.lesson.GET_LESSON, fetchLesson);
}

export function* loadLessons() {
  yield takeEvery(actions.lesson.GET_LESSONS, fetchLessons);
}

export function* loadDeleteLesson() {
  yield takeEvery(actions.lesson.DELETE_LESSON, deleteLesson);
}

export function* loadDeleteLessons() {
  yield takeEvery(actions.lesson.DELETE_LESSONS, deleteLessons);
}

export function* loadEditLesson() {
  yield takeEvery(actions.lesson.EDIT_LESSON, editLesson);
}

export function* loadEditStatusLesson() {
  yield takeEvery(actions.lesson.EDIT_STATUS_LESSONS, editStatusLessons);
}

export function* loadCreateLesson() {
  yield takeEvery(actions.lesson.CREATE_LESSON, createLesson);
}

export function* loadLessonCates() {
  yield takeEvery(actions.lesson.GET_LESSON_CATES, fetchLessonCates);
}
export function* loadCreateLessonCate() {
  yield takeEvery(actions.lesson.CREATE_LESSON_CATE, createLessonCate);
}

export function* loadEditLessonCate() {
  yield takeEvery(actions.lesson.EDIT_LESSON_CATE, editLessonCate);
}

export function* loadDeleteLessonCate() {
  yield takeEvery(actions.lesson.DELETE_LESSON_CATE, deleteLessonCate);
}

export function* loadDeleteLessonCates() {
  yield takeEvery(actions.lesson.DELETE_LESSON_CATES, deleteLessonCates);
}
