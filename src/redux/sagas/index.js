import { all } from 'redux-saga/effects';
// IMPORT SAGAS

import {
  loadCategory,
  loadCategories,
  loadDeleteCategory,
  loadDeleteCategories,
  loadEditCategory,
  loadCreateCategory,
  loadCategoryData,
  loadAllCategoryData,
  loadEditCategoriesStatus,
} from './category';

import {
  loadNotification,
  loadNotifications,
  loadDeleteNotification,
  loadDeleteNotifications,
  loadCreateNotification,
  loadEditNotification,
  loadNotificationCates,
  loadCreateNotificationCate,
  loadEditNotificationCate,
  loadDeleteNotificationCate,
  loadDeleteNotificationCates,
} from './notification';

import {
  loadPost,
  loadPosts,
  loadDeletePost,
  loadDeletePosts,
  loadCreatePost,
  loadEditPost,
  loadEditStatusPost,
  loadPostCates,
  loadCreatePostCate,
  loadEditPostCate,
  loadDeletePostCate,
  loadDeletePostCates,
} from './post';

import {
  loadExam,
  loadExamHistory,
  loadExamTopic,
  loadExams,
  loadExamHistories,
  loadExamTopics,
  loadDeleteExam,
  loadDeleteExamHistory,
  loadDeleteExamTopic,
  loadDeleteExams,
  loadDeleteExamHistories,
  loadDeleteExamTopics,
  loadCreateExam,
  loadCreateExamHistory,
  loadCreateExamTopic,
  loadEditExam,
  loadEditExamHistory,
  loadEditExamTopic,
  loadEditStatusExam,
  loadExamCates,
  loadCreateExamCate,
  loadEditExamCate,
  loadDeleteExamCate,
  loadDeleteExamCates,
} from './exam';

import {
  loadQuestion,
  loadQuestions,
  loadDeleteQuestion,
  loadDeleteQuestions,
  loadCreateQuestion,
  loadEditQuestion,
  loadEditStatusQuestion,
  loadQuestionCates,
  loadCreateQuestionCate,
  loadEditQuestionCate,
  loadDeleteQuestionCate,
  loadDeleteQuestionCates,
} from './question';

import {
  loadCourse,
  loadCourses,
  loadDeleteCourse,
  loadDeleteCourses,
  loadCreateCourse,
  loadEditCourse,
  loadEditStatusCourse,
  loadCourseCates,
  loadCreateCourseCate,
  loadEditCourseCate,
  loadDeleteCourseCate,
  loadDeleteCourseCates,
} from './course';

import {
  loadLesson,
  loadLessons,
  loadDeleteLesson,
  loadDeleteLessons,
  loadCreateLesson,
  loadEditLesson,
  loadEditStatusLesson,
  loadLessonCates,
  loadCreateLessonCate,
  loadEditLessonCate,
  loadDeleteLessonCate,
  loadDeleteLessonCates,
} from './lesson';

import {
  loadCook,
  loadCooks,
  loadDeleteCook,
  loadDeleteCooks,
  loadCreateCook,
  loadEditCook,
  loadEditStatusCook,
  loadCookCates,
  loadCreateCookCate,
  loadEditCookCate,
  loadDeleteCookCate,
  loadDeleteCookCates,
} from './cook';

import {
  loadModule,
  loadExportModule,
  loadModules,
  loadDeleteModule,
  loadDeleteModules,
  loadCreateModule,
  loadEditModule,
  loadModuleCates,
  loadCreateModuleCate,
  loadEditModuleCate,
  loadDeleteModuleCate,
  loadEditModulesStatus,
} from './moduleApp';

import { loadDeductionInfo, loadDeductionInfos, loadDeduction, loadEditDeduction, loadEditDeductionResult } from './deduction';

import {
  loadForm,
  loadForms,
  loadDeleteForm,
  loadDeleteForms,
  loadCreateForm,
  loadEditForm,
  loadEditStatusForms,
  loadFormCates,
  loadCreateFormCate,
  loadEditFormCate,
  loadDeleteFormCate,
  loadDeleteFormCates,
  loadEditFormsStatus,
  loadImportForms,
} from './form';

import {
  loadUser,
  loadUsers,
  loadDeleteUser,
  loadDeleteUsers,
  loadCreateUser,
  loadEditUser,
  loadEditStatusUser,
  loadUserCates,
  loadCreateUserCate,
  loadEditUserCate,
  loadDeleteUserCate,
  loadDeleteUserCates,
  loadLogin,
  loadLogout,
  loadVerify,
  loadChangePassword,
  loadForgotPassword,
  loadRegisterUser,
  loadImportUsers,
} from './user';

import {
  loadRemind,
  loadReminds,
  loadDeleteRemind,
  loadDeleteReminds,
  loadCreateRemind,
  loadEditRemind,
  loadEditStatusRemind,
  loadRemindCates,
  loadCreateRemindCate,
  loadEditRemindCate,
  loadDeleteRemindCate,
  loadDeleteRemindCates,
  loadImportReminds,
} from './remind';

import { loadAppointment, loadAppointments, loadDeleteAppointment, loadDeleteAppointments, loadCreateAppointment, loadEditAppointment, loadEditStatusAppointment } from './appointment';

import { loadLogs } from './log';

import {
  loadMedia,
  loadMedias,
  loadDeleteMedia,
  loadDeleteMedias,
  loadCreateMedia,
  loadEditMedia,
  loadMediaCates,
  loadCreateMediaCate,
  loadEditMediaCate,
  loadDeleteMediaCate,
  loadDeleteMediaCates,
  loadMoveMedias,
} from './media';

import { loadConfig, loadConfigs, loadDeleteConfig, loadCreateConfig, loadEditConfig, loadSendTestEmail } from './configure';

import { loadGlobalStatistics, loadModuleStatistics, loadProjectStatistics, loadLatestStatistics, loadEdgeStatistics } from './statistic';

import {
  loadColumn,
  loadColumns,
  loadDeleteColumn,
  loadDeleteColumns,
  loadCreateColumn,
  loadEditColumn,
  loadTables,
  loadCreateTable,
  loadEditTable,
  loadDeleteTable,
  loadDropTable,
  loadDeleteTables,
  loadTableSql,
  loadGenTable,
  loadAddColumnsToTable,
  loadView,
  loadEditTablesStatus,
  loadEditColumnsStatus,
} from './table';

import { loadRuleset, loadRulesets } from './ruleset';
import { loadCheckInstaller, loadCheckConnection, loadInitData } from './installer';

export default function* rootSaga() {
  yield all([
    // EXPORT SAGAS

    // category
    loadCategory(),
    loadCategories(),
    loadDeleteCategory(),
    loadDeleteCategories(),
    loadEditCategory(),
    loadCreateCategory(),
    loadEditCategoriesStatus(),

    // category
    loadCategoryData(),
    loadAllCategoryData(),

    // installer
    loadCheckInstaller(),
    loadCheckConnection(),
    loadInitData(),

    // notification
    loadNotification(),
    loadNotifications(),
    loadDeleteNotification(),
    loadDeleteNotifications(),
    loadCreateNotification(),
    loadEditNotification(),
    loadNotificationCates(),
    loadCreateNotificationCate(),
    loadEditNotificationCate(),
    loadDeleteNotificationCates(),
    loadDeleteNotificationCate(),

    //ruleset
    loadRuleset(),
    loadRulesets(),

    // statistics
    loadGlobalStatistics(),
    loadModuleStatistics(),
    loadProjectStatistics(),
    loadLatestStatistics(),
    loadEdgeStatistics(),

    // module
    loadModule(),
    loadExportModule(),
    loadModules(),
    loadDeleteModule(),
    loadDeleteModules(),
    loadCreateModule(),
    loadEditModulesStatus(),
    loadEditModule(),
    loadModuleCates(),
    loadCreateModuleCate(),
    loadEditModuleCate(),
    loadDeleteModuleCate(),

    // deduction
    loadDeductionInfo(),
    loadDeductionInfos(),
    loadDeduction(),
    loadEditDeduction(),
    loadEditDeductionResult(),

    // form
    loadForm(),
    loadForms(),
    loadDeleteForm(),
    loadDeleteForms(),
    loadCreateForm(),
    loadEditForm(),
    loadEditFormsStatus(),
    loadFormCates(),
    loadCreateFormCate(),
    loadEditFormCate(),
    loadDeleteFormCate(),
    loadDeleteFormCates(),
    loadImportForms(),

    // user
    loadUser(),
    loadUsers(),
    loadDeleteUser(),
    loadDeleteUsers(),
    loadCreateUser(),
    loadImportUsers(),
    loadEditUser(),
    loadEditStatusUser(),
    loadUserCates(),
    loadCreateUserCate(),
    loadEditUserCate(),
    loadDeleteUserCate(),
    loadDeleteUserCates(),
    loadLogin(),
    loadLogout(),
    loadVerify(),
    loadForgotPassword(),
    loadChangePassword(),
    loadRegisterUser(),
    // post
    loadPost(),
    loadPosts(),
    loadDeletePost(),
    loadDeletePosts(),
    loadCreatePost(),
    loadEditPost(),
    loadEditStatusPost(),
    loadPostCates(),
    loadCreatePostCate(),
    loadEditPostCate(),
    loadDeletePostCate(),
    loadDeletePostCates(),

    // exam
    loadExam(),
    loadExamHistory(),
    loadExamTopic(),
    loadExams(),
    loadExamHistories(),
    loadExamTopics(),
    loadDeleteExam(),
    loadDeleteExamHistory(),
    loadDeleteExamTopic(),
    loadDeleteExams(),
    loadDeleteExamHistories(),
    loadDeleteExamTopics(),
    loadCreateExam(),
    loadCreateExamHistory(),
    loadCreateExamTopic(),

    loadEditExam(),
    loadEditExamHistory(),
    loadEditExamTopic(),
    loadEditStatusExam(),
    loadExamCates(),
    loadCreateExamCate(),
    loadEditExamCate(),
    loadDeleteExamCate(),
    loadDeleteExamCates(),

    // question
    loadQuestion(),
    loadQuestions(),
    loadDeleteQuestion(),
    loadDeleteQuestions(),
    loadCreateQuestion(),
    loadEditQuestion(),
    loadEditStatusQuestion(),
    loadQuestionCates(),
    loadCreateQuestionCate(),
    loadEditQuestionCate(),
    loadDeleteQuestionCate(),
    loadDeleteQuestionCates(),

    // course
    loadCourse(),
    loadCourses(),
    loadDeleteCourse(),
    loadDeleteCourses(),
    loadCreateCourse(),
    loadEditCourse(),
    loadEditStatusCourse(),
    loadCourseCates(),
    loadCreateCourseCate(),
    loadEditCourseCate(),
    loadDeleteCourseCate(),
    loadDeleteCourseCates(),

    // lesson
    loadLesson(),
    loadLessons(),
    loadDeleteLesson(),
    loadDeleteLessons(),
    loadCreateLesson(),
    loadEditLesson(),
    loadEditStatusLesson(),
    loadLessonCates(),
    loadCreateLessonCate(),
    loadEditLessonCate(),
    loadDeleteLessonCate(),
    loadDeleteLessonCates(),

    //cook
    loadCook(),
    loadCooks(),
    loadDeleteCook(),
    loadDeleteCooks(),
    loadCreateCook(),
    loadEditCook(),
    loadEditStatusCook(),
    loadCookCates(),
    loadCreateCookCate(),
    loadEditCookCate(),
    loadDeleteCookCate(),
    loadDeleteCookCates(),

    //reminds
    loadRemind(),
    loadReminds(),
    loadDeleteRemind(),
    loadDeleteReminds(),
    loadCreateRemind(),
    loadEditRemind(),
    loadEditStatusRemind(),
    loadRemindCates(),
    loadCreateRemindCate(),
    loadEditRemindCate(),
    loadDeleteRemindCate(),
    loadDeleteRemindCates(),
    loadImportReminds(),

    // appoinetment
    loadAppointment(),
    loadAppointments(),
    loadDeleteAppointment(),
    loadDeleteAppointments(),
    loadCreateAppointment(),
    loadEditAppointment(),
    loadEditStatusAppointment(),

    // media
    loadMedia(),
    loadMedias(),
    loadDeleteMedia(),
    loadDeleteMedias(),
    loadMoveMedias(),
    loadCreateMedia(),
    loadEditMedia(),
    loadMediaCates(),
    loadCreateMediaCate(),
    loadEditMediaCate(),
    loadDeleteMediaCate(),
    loadDeleteMediaCates(),
    // log
    loadLogs(),

    // config
    loadConfig(),
    loadConfigs(),
    loadDeleteConfig(),
    loadCreateConfig(),
    loadEditConfig(),
    loadSendTestEmail(),

    // column/table
    loadColumn(),
    loadColumns(),
    loadDeleteColumn(),
    loadDeleteColumns(),
    loadCreateColumn(),
    loadEditColumn(),
    loadEditColumnsStatus(),
    loadTables(),
    loadCreateTable(),
    loadEditTable(),
    loadDeleteTable(),
    loadDropTable(),
    loadTableSql(),
    loadGenTable(),
    loadDeleteTables(),
    loadEditTablesStatus(),
    loadAddColumnsToTable(),

    //View2Export
    loadView(),
  ]);
}
