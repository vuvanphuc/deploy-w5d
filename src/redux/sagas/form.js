import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl } from '../../helpers/common.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

export function* fetchForm(payload) {
  try {
    const endpoint = `${config.API_URL}/api/form/${payload.params.bieu_mau_id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.form.GET_FORM_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.GET_FORM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu biểu mẫu thất bại'),
    });
  }
}

export function* fetchForms(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/forms`, payload.params ? payload.params : state.form.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.form.GET_FORMS_SUCCESS, forms: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.GET_FORMS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh sách biểu mẫu thất bại'),
    });
  }
}

export function* deleteForm(payload) {
  try {
    const endpoint = `${config.API_URL}/api/form/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, { id: payload.params.id });
    const data = yield response.data;
    yield put({ type: actions.form.DELETE_FORM_SUCCESS, result: data });

    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.DELETE_FORM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa biểu mẫu thất bại'),
    });
  }
}

export function* deleteForms(payload) {
  console.log('Xem tham sô xóa mẫu', payload.params);
  try {
    const endpoint = `${config.API_URL}/api/delete-forms`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.form.DELETE_FORMS_SUCCESS, result: data });

    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.DELETE_FORMS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều biểu mẫu thất bại'),
    });
  }
}

export function* editForm(payload) {
  try {
    const endpoint = `${config.API_URL}/api/form/${payload.params.bieu_mau_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.form.EDIT_FORM_SUCCESS, result: data });
    yield put({ type: actions.form.GET_FORM_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.EDIT_FORM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật biểu mẫu thất bại'),
    });
  }
}

export function* editStatusForms(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-forms-status`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.form.EDIT_FORMS_STATUS_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.form.EDIT_FORMS_STATUS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái biểu mẫu thất bại'),
    });
  }
}

export function* createForm(payload) {
  try {
    const endpoint = `${config.API_URL}/api/form`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.form.CREATE_FORM_SUCCESS, result: data });
    yield put({ type: actions.form.GET_FORM_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.CREATE_FORM_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới biểu mẫu thất bại'),
    });
  }
}
function* importForms(payload) {
  try {
    const endpoint = `${config.API_URL}/api/import-forms`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.form.IMPORT_FORMS_SUCCESS, result: data });
    if (payload.onSuccess) {
      payload.onSuccess(data);
    }
  } catch (error) {
    yield put({ type: actions.form.IMPORT_FORMS_FAILED, error });
    if (payload.onError) {
      payload.onError();
    }
    notification.error({
      message: get(error, 'response.data.error', 'Import nhiều thành viên thất bại.'),
    });
  }
}
export function* fetchFormCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/form-categories`, payload.params ? payload.params : state.form.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.form.GET_FORM_CATES_SUCCESS,
      listCates: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.GET_FORM_CATES_FAILED, error });
    notification.error({
      message: 'Tải danh sách danh mục biểu mẫu thất bại',
    });
  }
}

export function* createFormCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/form-category`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.form.CREATE_FORM_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.CREATE_FORM_CATE_FAILED, error });
    notification.error({
      message: error.response.data.error,
    });
  }
}

export function* editFormCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/form-category/${payload.params.nhom_bieu_mau_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.form.EDIT_FORM_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.EDIT_FORM_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục biểu mẫu thất bại'),
    });
  }
}

export function* deleteFormCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/form-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({
      type: actions.form.DELETE_FORM_CATE_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.DELETE_FORM_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục biểu mẫu thất bại'),
    });
  }
}

export function* deleteFormCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-form-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({
      type: actions.form.DELETE_FORM_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.form.DELETE_FORM_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục biểu mẫu thất bại'),
    });
  }
}

export function* loadForm() {
  yield takeEvery(actions.form.GET_FORM, fetchForm);
}

export function* loadForms() {
  yield takeEvery(actions.form.GET_FORMS, fetchForms);
}

export function* loadDeleteForm() {
  yield takeEvery(actions.form.DELETE_FORM, deleteForm);
}

export function* loadDeleteForms() {
  yield takeEvery(actions.form.DELETE_FORMS, deleteForms);
}

export function* loadEditForm() {
  yield takeEvery(actions.form.EDIT_FORM, editForm);
}

export function* loadEditFormsStatus() {
  yield takeEvery(actions.form.EDIT_FORMS_STATUS, editStatusForms);
}

export function* loadCreateForm() {
  yield takeEvery(actions.form.CREATE_FORM, createForm);
}

export function* loadFormCates() {
  yield takeEvery(actions.form.GET_FORM_CATES, fetchFormCates);
}

export function* loadCreateFormCate() {
  yield takeEvery(actions.form.CREATE_FORM_CATE, createFormCate);
}

export function* loadEditFormCate() {
  yield takeEvery(actions.form.EDIT_FORM_CATE, editFormCate);
}

export function* loadDeleteFormCate() {
  yield takeEvery(actions.form.DELETE_FORM_CATE, deleteFormCate);
}

export function* loadDeleteFormCates() {
  yield takeEvery(actions.form.DELETE_FORM_CATES, deleteFormCates);
}
export function* loadImportForms() {
  yield takeEvery(actions.form.IMPORT_FORMS, importForms);
}
