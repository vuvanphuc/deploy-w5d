import { call, put, takeEvery, select } from 'redux-saga/effects';
import moment from 'moment';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl, getUserInformation } from '../../helpers/common.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';
import { get } from 'lodash';

function* fetchCourse(payload) {
  try {
    let endpoint = `${config.API_URL}/api/course/${payload.params.khoa_hoc_id}`;
    const response = yield call(getApi, endpoint);
    const result = yield response.data;
    yield put({ type: actions.course.GET_COURSE_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.course.GET_COURSE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải khóa học thất bại'),
    });
  }
}

function* fetchCourses(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/courses`, payload.params ? payload.params : state.course.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.course.GET_COURSES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.GET_COURSES_FAILED, error });
    notification.error({
      message: get(error, error.response.data.error, 'Tải danh sách khóa học thất bại'),
    });
  }
}

function* deleteCourse(payload) {
  try {
    const endpoint = `${config.API_URL}/api/course/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.course.DELETE_COURSE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.DELETE_COURSE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa khóa học thất bại'),
    });
  }
}

function* deleteCourses(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-courses`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.course.DELETE_COURSES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.DELETE_COURSES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa nhiều khóa học thất bại.'),
    });
  }
}
function* editCourse(payload) {
  try {
    const endpoint = `${config.API_URL}/api/course/${payload.params.khoa_hoc_id}`;
    const response = yield call(putApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.course.EDIT_COURSE_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.course.EDIT_COURSE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật khóa học thất bại'),
    });
  }
}

function* editStatusCourses(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-status-courses`;
    const response = yield call(postApi, endpoint, payload.params);
    const result = yield response.data;
    yield put({ type: actions.course.EDIT_STATUS_COURSES_SUCCESS, result });
    if (payload.callback) {
      payload.callback(result);
    }
  } catch (error) {
    yield put({ type: actions.course.EDIT_STATUS_COURSES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật trạng thái nhiều khóa học thất bại.'),
    });
  }
}

function* createCourse(payload) {
  try {
    const userInfo = getUserInformation();
    const endpoint = `${config.API_URL}/api/course`;
    const response = yield call(postApi, endpoint, {
      ...payload.params,
      nguoi_tao: userInfo.nhan_vien_id,
      ngay_tao: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.course.CREATE_COURSE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.CREATE_COURSE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo mới khóa học thất bại'),
    });
  }
}

function* fetchCourseCates(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/course-categories`, payload.params ? payload.params : state.course.listCates.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({
      type: actions.course.GET_COURSE_CATES_SUCCESS,
      result: data,
    });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.GET_COURSE_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải danh mục khóa học thất bại'),
    });
  }
}

function* createCourseCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/course-category`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.course.CREATE_COURSE_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.CREATE_COURSE_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tạo danh mục khóa học thất bại'),
    });
  }
}

function* editCourseCate(payload) {
  try {
    const userInfo = getUserInformation();
    const state = yield select();
    const endpoint = `${config.API_URL}/api/course-category/${payload.params.nhom_khoa_hoc_id}`;
    const response = yield call(putApi, endpoint, {
      ...payload.params,
      nguoi_sua: userInfo.nhan_vien_id,
      ngay_sua: moment().format(),
    });
    const data = yield response.data;
    yield put({ type: actions.course.EDIT_COURSE_CATE_SUCCESS, result: data });
    yield put(actions.course.getCourseCates(state.course.listCates.search));
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.EDIT_COURSE_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Cập nhật danh mục khóa học thất bại'),
    });
  }
}

function* deleteCourseCate(payload) {
  try {
    const endpoint = `${config.API_URL}/api/course-category/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.course.DELETE_COURSE_CATE_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.DELETE_COURSE_CATE_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Xóa danh mục khóa học thất bại'),
    });
  }
}
function* deleteCourseCates(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-course-categories`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.course.DELETE_COURSE_CATES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.course.DELETE_COURSE_CATES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Không xóa được danh mục khóa học.'),
    });
  }
}

export function* loadCourse() {
  yield takeEvery(actions.course.GET_COURSE, fetchCourse);
}

export function* loadCourses() {
  yield takeEvery(actions.course.GET_COURSES, fetchCourses);
}

export function* loadDeleteCourse() {
  yield takeEvery(actions.course.DELETE_COURSE, deleteCourse);
}

export function* loadDeleteCourses() {
  yield takeEvery(actions.course.DELETE_COURSES, deleteCourses);
}

export function* loadEditCourse() {
  yield takeEvery(actions.course.EDIT_COURSE, editCourse);
}

export function* loadEditStatusCourse() {
  yield takeEvery(actions.course.EDIT_STATUS_COURSES, editStatusCourses);
}

export function* loadCreateCourse() {
  yield takeEvery(actions.course.CREATE_COURSE, createCourse);
}

export function* loadCourseCates() {
  yield takeEvery(actions.course.GET_COURSE_CATES, fetchCourseCates);
}
export function* loadCreateCourseCate() {
  yield takeEvery(actions.course.CREATE_COURSE_CATE, createCourseCate);
}

export function* loadEditCourseCate() {
  yield takeEvery(actions.course.EDIT_COURSE_CATE, editCourseCate);
}

export function* loadDeleteCourseCate() {
  yield takeEvery(actions.course.DELETE_COURSE_CATE, deleteCourseCate);
}

export function* loadDeleteCourseCates() {
  yield takeEvery(actions.course.DELETE_COURSE_CATES, deleteCourseCates);
}
