import { call, put, takeEvery, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { config } from '../../config';
import { buildUrl } from '../../helpers/common.helper';
import { get } from 'lodash';
import { getLangText } from '../../helpers/language.helper';
import * as actions from '../actions';
import { getApi, postApi, deleteApi, putApi } from '../services/api';

export function* fetchCategory(payload) {
  try {
    const endpoint = `${config.API_URL}/api/category/${payload.params.module_id}/${payload.params.id}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.category.GET_CATEGORY_SUCCESS, item: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.GET_CATEGORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* fetchCategories(payload) {
  try {
    const state = yield select();
    const endpoint = buildUrl(`${config.API_URL}/api/categories/${payload.params.module_id}`, payload.params ? payload.params : state.category.list.search);
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.category.GET_CATEGORIES_SUCCESS, modules: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.GET_CATEGORIES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* deleteCategory(payload) {
  try {
    const endpoint = `${config.API_URL}/api/category/${payload.params.module_id}/${payload.params.id}`;
    const response = yield call(deleteApi, endpoint, payload.params.id);
    const data = yield response.data;
    yield put({ type: actions.category.DELETE_CATEGORY_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.DELETE_CATEGORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* deleteCategories(payload) {
  try {
    const endpoint = `${config.API_URL}/api/delete-categories/${payload.params.module_id}`;
    const response = yield call(postApi, endpoint, payload.params);
    const data = yield response.data;
    yield put({ type: actions.category.DELETE_CATEGORIES_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.DELETE_CATEGORIES_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* editCategory(payload) {
  try {
    const endpoint = `${config.API_URL}/api/category/${payload.params.module.module_id}/${payload.params.id}`;
    const response = yield call(putApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.category.EDIT_CATEGORY_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.EDIT_CATEGORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* editCategoriesStatus(payload) {
  try {
    const endpoint = `${config.API_URL}/api/update-categories-status/${payload.params.module_id}`;
    const response = yield call(postApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.category.EDIT_CATEGORIES_STATUS_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.EDIT_CATEGORIES_STATUS_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* createCategory(payload) {
  try {
    const endpoint = `${config.API_URL}/api/category/${payload.params.module.module_id}`;
    const response = yield call(postApi, endpoint, payload.params.data);
    const data = yield response.data;
    yield put({ type: actions.category.CREATE_CATEGORY_SUCCESS, result: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.CREATE_CATEGORY_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* fetchCategoryData(payload) {
  try {
    const endpoint = `${config.API_URL}/api/category-data/${payload.params.bangidlist}`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.category.GET_CATEGORY_DATA_SUCCESS, data: data });
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.GET_CATEGORY_DATA_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', 'Tải dữ liệu thất bại.'),
    });
  }
}

export function* fetchAllCategoryData(payload) {
  try {
    const endpoint = `${config.API_URL}/api/all-category-data`;
    const response = yield call(getApi, endpoint);
    const data = yield response.data;
    yield put({ type: actions.category.GET_ALL_CATEGORY_DATA_SUCCESS, data: data });
    if (payload && payload.params && payload.params.isUpdateStorage) {
      localStorage.setItem('categoryInfo', JSON.stringify(data.data));
      window.location.reload();
    }
    if (payload.callback) {
      payload.callback(data);
    }
  } catch (error) {
    yield put({ type: actions.category.GET_ALL_CATEGORY_DATA_FAILED, error });
    notification.error({
      message: get(error, 'response.data.error', getLangText('MESSAGE.GET_ALL_CATEGORY_DATA_FAIL')),
    });
  }
}

export function* loadCategoryData() {
  yield takeEvery(actions.category.GET_CATEGORY_DATA, fetchCategoryData);
}

export function* loadAllCategoryData() {
  yield takeEvery(actions.category.GET_ALL_CATEGORY_DATA, fetchAllCategoryData);
}

export function* loadCategory() {
  yield takeEvery(actions.category.GET_CATEGORY, fetchCategory);
}

export function* loadCategories() {
  yield takeEvery(actions.category.GET_CATEGORIES, fetchCategories);
}

export function* loadDeleteCategory() {
  yield takeEvery(actions.category.DELETE_CATEGORY, deleteCategory);
}

export function* loadDeleteCategories() {
  yield takeEvery(actions.category.DELETE_CATEGORIES, deleteCategories);
}

export function* loadEditCategory() {
  yield takeEvery(actions.category.EDIT_CATEGORY, editCategory);
}

export function* loadCreateCategory() {
  yield takeEvery(actions.category.CREATE_CATEGORY, createCategory);
}

export function* loadEditCategoriesStatus() {
  yield takeEvery(actions.category.EDIT_CATEGORIES_STATUS, editCategoriesStatus);
}
