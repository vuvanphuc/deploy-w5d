// get a rule set
export const GET_RULESET = 'GET_RULESET';
export const GET_RULESET_SUCCESS = 'GET_RULESET_SUCCESS';
export const GET_RULESET_FAILED = 'GET_RULESET_FAILED';

// get rulesets
export const GET_RULE_SETS = 'GET_RULE_SETS';
export const GET_RULE_SETS_SUCCESS = 'GET_RULE_SETS_SUCCESS';
export const GET_RULE_SETS_FAILED = 'GET_RULE_SETS_FAILED';

export function getRuleset(params, callback) {
  return {
    type: GET_RULESET,
    params,
    callback,
  };
}

export function getRulesets(params, callback) {
  return {
    type: GET_RULE_SETS,
    params,
    callback,
  };
}
