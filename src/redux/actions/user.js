// get a user
export const GET_USER = 'GET_USER';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const GET_USER_FAILED = 'GET_USER_FAILED';

//get a list of users
export const GET_USERS = 'GET_USERS';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAILED = 'GET_USERS_FAILED';

// delete a user
export const DELETE_USER = 'DELETE_USER';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const DELETE_USER_FAILED = 'DELETE_USER_FAILED';

// delete list user
export const DELETE_USERS = 'DELETE_USERS';
export const DELETE_USERS_SUCCESS = 'DELETE_USERS_SUCCESS';
export const DELETE_USERS_FAILED = 'DELETE_USERS_FAILED';

// edit a user
export const EDIT_USER = 'EDIT_USER';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';
export const EDIT_USER_FAILED = 'EDIT_USER_FAILED';

// edit status list user
export const EDIT_STATUS_USERS = 'EDIT_STATUS_USERS';
export const EDIT_STATUS_USERS_SUCCESS = 'EDIT_USERS_SUCCESS';
export const EDIT_STATUS_USERS_FAILED = 'EDIT_USERS_FAILED';

// create a user
export const CREATE_USER = 'CREATE_USER';
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS';
export const CREATE_USER_FAILED = 'CREATE_USER_FAILED';
//import list user
export const IMPORT_USERS = 'IMPORT_USERS';
export const IMPORT_USERS_SUCCESS = 'IMPORT_USERS_SUCCESS';
export const IMPORT_USERS_FAILED = 'IMPORT_USERS_FAILED';
// register member
export const REGISTER_USER = 'REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILED = 'REGISTER_USER_FAILED';
// get a user category
export const GET_USER_CATE = 'GET_USER_CATE';
export const GET_USER_CATE_SUCCESS = 'GET_USER_CATE_SUCCESS';
export const GET_USER_CATE_FAILED = 'GET_USER_CATE_FAILED';

//get a list of user categories
export const GET_USER_CATES = 'GET_USER_CATES';
export const GET_USER_CATES_SUCCESS = 'GET_USER_CATES_SUCCESS';
export const GET_USER_CATES_FAILED = 'GET_USER_CATES_FAILED';

//create a user category
export const CREATE_USER_CATE = 'CREATE_USER_CATE';
export const CREATE_USER_CATE_SUCCESS = 'CREATE_USER_CATE_SUCCESS';
export const CREATE_USER_CATE_FAILED = 'CREATE_USER_CATE_FAILED';

//edit a user category
export const EDIT_USER_CATE = 'EDIT_USER_CATE';
export const EDIT_USER_CATE_SUCCESS = 'EDIT_USER_CATE_SUCCESS';
export const EDIT_USER_CATE_FAILED = 'EDIT_USER_CATE_FAILED';

//delete a user category
export const DELETE_USER_CATE = 'DELETE_USER_CATE';
export const DELETE_USER_CATE_SUCCESS = 'DELETE_USER_CATE_SUCCESS';
export const DELETE_USER_CATE_FAILED = 'DELETE_USER_CATE_FAILED';

//delete a user categories
export const DELETE_USER_CATES = 'DELETE_USER_CATES';
export const DELETE_USER_CATES_SUCCESS = 'DELETE_USER_CATES_SUCCESS';
export const DELETE_USER_CATES_FAILED = 'DELETE_USER_CATES_FAILED';

// set user search params
export const SET_SEARCH_USER = 'SET_SEARCH_USER';

// login
export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILED = 'LOGIN_USER_FAILED';

// logout
export const LOGOUT_USER = 'LOGOUT_USER';
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS';
export const LOGOUT_USER_FAILED = 'LOGOUT_USER_FAILED';

// verify user
export const VERIFY_USER = 'VERIFY_USER';
export const VERIFY_USER_SUCCESS = 'VERIFY_USER_SUCCESS';
export const VERIFY_USER_FAILED = 'VERIFY_USER_FAILED';

// forgot password
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_FAILED = 'FORGOT_PASSWORD_FAILED';

// change password
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAILED = 'CHANGE_PASSWORD_FAILED';

// get user statistics
export const GET_USER_STATISTICS = 'GET_USER_STATISTICS';
export const GET_USER_STATISTICS_SUCCESS = 'GET_USER_STATISTICS_SUCCESS';
export const GET_USER_STATISTICS_FAILED = 'GET_USER_STATISTICS_FAILED';

export function getUser(params, callback) {
  return {
    type: GET_USER,
    params,
    callback,
  };
}

export function getUsers(params, callback) {
  return {
    type: GET_USERS,
    params,
    callback,
  };
}

export function deleteUser(params, callback) {
  return {
    type: DELETE_USER,
    params,
    callback,
  };
}

export function deleteUsers(params, callback) {
  return {
    type: DELETE_USERS,
    params,
    callback,
  };
}

export function editUser(params, callback) {
  return {
    type: EDIT_USER,
    params,
    callback,
  };
}

export function editStatusUser(params, callback) {
  return {
    type: EDIT_STATUS_USERS,
    params,
    callback,
  };
}

export function createUser(params, callback) {
  return {
    type: CREATE_USER,
    params,
    callback,
  };
}
export function importUsers(params, onSuccess, onError) {
  return {
    type: IMPORT_USERS,
    params,
    onSuccess,
    onError,
  };
}
export function getUserCate(id, callback) {
  return {
    type: GET_USER_CATE,
    id,
    callback,
  };
}

export function getUserCates(params, callback) {
  return {
    type: GET_USER_CATES,
    params,
    callback,
  };
}

export function createUserCate(params, callback) {
  return {
    type: CREATE_USER_CATE,
    params,
    callback,
  };
}

export function editUserCate(params, callback) {
  return {
    type: EDIT_USER_CATE,
    params,
    callback,
  };
}

export function deleteUserCate(params, callback) {
  return {
    type: DELETE_USER_CATE,
    params,
    callback,
  };
}

export function deleteUserCates(params, callback) {
  return {
    type: DELETE_USER_CATES,
    params,
    callback,
  };
}

export function setSearchParams(params) {
  return {
    type: SET_SEARCH_USER,
    params,
  };
}

export function loginUser(params, callback) {
  return {
    type: LOGIN_USER,
    params,
    callback,
  };
}

export function logoutUser(params, callback) {
  return {
    type: LOGOUT_USER,
    params,
    callback,
  };
}

export function verifyUser(params, callback) {
  return {
    type: VERIFY_USER,
    params,
    callback,
  };
}

export function forgotPassword(params, callback) {
  return {
    type: FORGOT_PASSWORD,
    params,
    callback,
  };
}

export function changePassword(params, callback) {
  return {
    type: CHANGE_PASSWORD,
    params,
    callback,
  };
}

export function getUserStatistic(params, callback) {
  return {
    type: GET_USER_STATISTICS,
    params,
    callback,
  };
}
export function registerUser(params, callback) {
  return {
    type: REGISTER_USER,
    params,
    callback,
  };
}
