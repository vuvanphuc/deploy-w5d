// get a CATEGORY
export const GET_CATEGORY = 'GET_CATEGORY';
export const GET_CATEGORY_SUCCESS = 'GET_CATEGORY_SUCCESS';
export const GET_CATEGORY_FAILED = 'GET_CATEGORY_FAILED';

// get a list of CATEGORYs
export const GET_CATEGORIES = 'GET_CATEGORIES';
export const GET_CATEGORIES_SUCCESS = 'GET_CATEGORIES_SUCCESS';
export const GET_CATEGORIES_FAILED = 'GET_CATEGORIES_FAILED';

// delete a CATEGORY
export const DELETE_CATEGORY = 'DELETE_CATEGORY';
export const DELETE_CATEGORY_SUCCESS = 'DELETE_CATEGORY_SUCCESS';
export const DELETE_CATEGORY_FAILED = 'DELETE_CATEGORY_FAILED';

// delete a list of CATEGORYs
export const DELETE_CATEGORIES = 'DELETE_CATEGORIES';
export const DELETE_CATEGORIES_SUCCESS = 'DELETE_CATEGORIES_SUCCESS';
export const DELETE_CATEGORIES_FAILED = 'DELETE_CATEGORIES_FAILED';

// edit a CATEGORY
export const EDIT_CATEGORY = 'EDIT_CATEGORY';
export const EDIT_CATEGORY_SUCCESS = 'EDIT_CATEGORY_SUCCESS';
export const EDIT_CATEGORY_FAILED = 'EDIT_CATEGORY_FAILED';

// edit CATEGORY STATUS
export const EDIT_CATEGORIES_STATUS = 'EDIT_CATEGORIES_STATUS';
export const EDIT_CATEGORIES_STATUS_SUCCESS = 'EDIT_CATEGORIES_STATUS_SUCCESS';
export const EDIT_CATEGORIES_STATUS_FAILED = 'EDIT_CATEGORIES_STATUS_FAILED';

// create a CATEGORY
export const CREATE_CATEGORY = 'CREATE_CATEGORY';
export const CREATE_CATEGORY_SUCCESS = 'CREATE_CATEGORY_SUCCESS';
export const CREATE_CATEGORY_FAILED = 'CREATE_CATEGORY_FAILED';

// get a list of data of records
export const GET_CATEGORY_DATA = 'GET_CATEGORY_DATA';
export const GET_CATEGORY_DATA_SUCCESS = 'GET_CATEGORY_DATA_SUCCESS';
export const GET_CATEGORY_DATA_FAILED = 'GET_CATEGORY_DATA_FAILED';

export const GET_ALL_CATEGORY_DATA = 'GET_ALL_CATEGORY_DATA';
export const GET_ALL_CATEGORY_DATA_SUCCESS = 'GET_ALL_CATEGORY_DATA_SUCCESS';
export const GET_ALL_CATEGORY_DATA_FAILED = 'GET_ALL_CATEGORY_DATA_FAILED';

export function getCategoryData(params, callback) {
  return {
    type: GET_CATEGORY_DATA,
    params,
    callback,
  };
}

export function getAllCategoryData(params, callback) {
  return {
    type: GET_ALL_CATEGORY_DATA,
    params,
    callback,
  };
}

export function getCategory(params, callback) {
  return {
    type: GET_CATEGORY,
    params,
    callback,
  };
}

export function getCategories(params, callback) {
  return {
    type: GET_CATEGORIES,
    params,
    callback,
  };
}

export function deleteCategory(params, callback) {
  return {
    type: DELETE_CATEGORY,
    params,
    callback,
  };
}

export function deleteCategories(params, callback) {
  return {
    type: DELETE_CATEGORIES,
    params,
    callback,
  };
}

export function editCategory(params, callback) {
  return {
    type: EDIT_CATEGORY,
    params,
    callback,
  };
}

export function editCategoriesStatus(params, callback) {
  return {
    type: EDIT_CATEGORIES_STATUS,
    params,
    callback,
  };
}

export function createCategory(params, callback) {
  return {
    type: CREATE_CATEGORY,
    params,
    callback,
  };
}
