// get global statistics
export const GET_GLOBAL_STATISTICS = 'GET_GLOBAL_STATISTICS';
export const GET_GLOBAL_STATISTICS_SUCCESS = 'GET_GLOBAL_STATISTICS_SUCCESS';
export const GET_GLOBAL_STATISTICS_FAILED = 'GET_GLOBAL_STATISTICS_FAILED';

//get number rows data of modules
export const GET_MODULE_STATISTICS = 'GET_MODULS_STATISTICS';
export const GET_MODULE_STATISTICS_SUCCESS = 'GET_MODULE_STATISTICS_SUCCESS';
export const GET_MODULE_STATISTICS_FAILED = 'GET_MODULE_STATISTICS_FAILED';

// get project statistics
export const GET_PROJECT_STATISTICS = 'GET_PROJECT_STATISTICS';
export const GET_PROJECT_STATISTICS_SUCCESS = 'GET_PROJECT_STATISTICS_SUCCESS';
export const GET_PROJECT_STATISTICS_FAILED = 'GET_PROJECT_STATISTICS_FAILED';

// get module lasted record
export const GET_MODULE_LATEST_RECORD = 'GET_MODULE_LATEST_RECORD';
export const GET_MODULE_LATEST_RECORD_SUCCESS = 'GET_MODULE_LATEST_RECORD_SUCCESS';
export const GET_MODULE_LATEST_RECORD_FAILED = 'GET_MODULE_LATEST_RECORD_FAILED';

// get module edge record
export const GET_MODULE_EDGE_RECORD = 'GET_MODULE_EDGE_RECORD';
export const GET_MODULE_EDGE_RECORD_SUCCESS = 'GET_MODULE_EDGE_RECORD_SUCCESS';
export const GET_MODULE_EDGE_RECORD_FAILED = 'GET_MODULE_EDGE_RECORD_FAILED';

export function getGlobalStatistic(params, callback) {
  return {
    type: GET_GLOBAL_STATISTICS,
    params,
    callback,
  };
}

export function getModuleStatistic(params, callback) {
  return {
    type: GET_MODULE_STATISTICS,
    params,
    callback,
  };
}

export function getProjectStatistic(params, callback) {
  return {
    type: GET_PROJECT_STATISTICS,
    params,
    callback,
  };
}

export function getModuleLatestRecord(params, callback) {
  return {
    type: GET_MODULE_LATEST_RECORD,
    params,
    callback,
  };
}
export function getModuleEdgeRecord(params, callback) {
  return {
    type: GET_MODULE_EDGE_RECORD,
    params,
    callback,
  };
}
