// get a column
export const GET_COLUMN = 'GET_COLUMN';
export const GET_COLUMN_SUCCESS = 'GET_COLUMN_SUCCESS';
export const GET_COLUMN_FAILED = 'GET_COLUMN_FAILED';

// get a list of columns
export const GET_COLUMNS = 'GET_COLUMNS';
export const GET_COLUMNS_SUCCESS = 'GET_COLUMNS_SUCCESS';
export const GET_COLUMNS_FAILED = 'GET_COLUMNS_FAILED';

// delete a column
export const DELETE_COLUMN = 'DELETE_COLUMN';
export const DELETE_COLUMN_SUCCESS = 'DELETE_COLUMN_SUCCESS';
export const DELETE_COLUMN_FAILED = 'DELETE_COLUMN_FAILED';

// delete a column
export const DELETE_COLUMNS = 'DELETE_COLUMNS';
export const DELETE_COLUMNS_SUCCESS = 'DELETE_COLUMNS_SUCCESS';
export const DELETE_COLUMNS_FAILED = 'DELETE_COLUMNS_FAILED';

// edit a column
export const EDIT_COLUMN = 'EDIT_COLUMN';
export const EDIT_COLUMN_SUCCESS = 'EDIT_COLUMN_SUCCESS';
export const EDIT_COLUMN_FAILED = 'EDIT_COLUMN_FAILED';

// create a column
export const CREATE_COLUMN = 'CREATE_COLUMN';
export const CREATE_COLUMN_SUCCESS = 'CREATE_COLUMN_SUCCESS';
export const CREATE_COLUMN_FAILED = 'CREATE_COLUMN_FAILED';

// edit status of columns
export const EDIT_COLUMNS_STATUS = 'EDIT_COLUMNS_STATUS';
export const EDIT_COLUMNS_STATUS_SUCCESS = 'EDIT_COLUMNS_STATUS_SUCCESS';
export const EDIT_COLUMNS_STATUS_FAILED = 'EDIT_COLUMNS_STATUS_FAILED';

// get a table
export const GET_TABLE = 'GET_TABLE';
export const GET_TABLE_SUCCESS = 'GET_TABLE_SUCCESS';
export const GET_TABLE_FAILED = 'GET_TABLE_FAILED';

// get a list of tables
export const GET_TABLES = 'GET_TABLES';
export const GET_TABLES_SUCCESS = 'GET_TABLES_SUCCESS';
export const GET_TABLES_FAILED = 'GET_TABLES_FAILED';

//create a table
export const CREATE_TABLE = 'CREATE_TABLE';
export const CREATE_TABLE_SUCCESS = 'CREATE_TABLE_SUCCESS';
export const CREATE_TABLE_FAILED = 'CREATE_TABLE_FAILED';

//edit a table
export const EDIT_TABLE = 'EDIT_TABLE';
export const EDIT_TABLE_SUCCESS = 'EDIT_TABLE_SUCCESS';
export const EDIT_TABLE_FAILED = 'EDIT_TABLE_FAILED';

//delete a table
export const DELETE_TABLE = 'DELETE_TABLE';
export const DELETE_TABLE_SUCCESS = 'DELETE_TABLE_SUCCESS';
export const DELETE_TABLE_FAILED = 'DELETE_TABLE_FAILED';

//drop a table
export const DROP_TABLE = 'DROP_TABLE';
export const DROP_TABLE_SUCCESS = 'DROP_TABLE_SUCCESS';
export const DROP_TABLE_FAILED = 'DROP_TABLE_FAILED';

// delete many table
export const DELETE_TABLES = 'DELETE_TABLES';
export const DELETE_TABLES_SUCCESS = 'DELETE_TABLES_SUCCESS';
export const DELETE_TABLES_FAILED = 'DELETE_TABLES_FAILED';

//generate table creating sql
export const GEN_TABLE_SQL = 'GEN_TABLE_SQL';
export const GEN_TABLE_SQL_SUCCESS = 'GEN_TABLE_SQL_SUCCESS';
export const GEN_TABLE_SQL_FAILED = 'GEN_TABLE_SQL_FAILED';

//generate table
export const GEN_TABLE = 'GEN_TABLE';
export const GEN_TABLE_SUCCESS = 'GEN_TABLE_SUCCESS';
export const GEN_TABLE_FAILED = 'GEN_TABLE_FAILED';

// get a view KHDT
export const GET_VIEW = 'GET_VIEW';
export const GET_VIEW_SUCCESS = 'GET_VIEW_SUCCESS';
export const GET_VIEW_FAILED = 'GET_VIEW_FAILED';

// add columns to a table
export const ADD_COLUMNS_TO_TABLE = 'ADD_COLUMNS_TO_TABLE';
export const ADD_COLUMNS_TO_TABLE_SUCCESS = 'ADD_COLUMNS_TO_TABLE_SUCCESS';
export const ADD_COLUMNS_TO_TABLE_FAILED = 'ADD_COLUMNS_TO_TABLE_FAILED';

// edit status of tables
export const EDIT_TABLES_STATUS = 'EDIT_TABLES_STATUS';
export const EDIT_TABLES_STATUS_SUCCESS = 'EDIT_TABLES_STATUS_SUCCESS';
export const EDIT_TABLES_STATUS_FAILED = 'EDIT_TABLES_STATUS_FAILED';

export function getView(params, callback) {
  return {
    type: GET_VIEW,
    params,
    callback,
  };
}

export function getColumn(params, callback) {
  return {
    type: GET_COLUMN,
    params,
    callback,
  };
}

export function getColumns(params, callback) {
  return {
    type: GET_COLUMNS,
    params,
    callback,
  };
}

export function deleteColumn(params, callback) {
  return {
    type: DELETE_COLUMN,
    params,
    callback,
  };
}

export function deleteColumns(params, callback) {
  return {
    type: DELETE_COLUMNS,
    params,
    callback,
  };
}

export function editColumn(params, callback) {
  return {
    type: EDIT_COLUMN,
    params,
    callback,
  };
}

export function createColumn(params, callback) {
  return {
    type: CREATE_COLUMN,
    params,
    callback,
  };
}

export function editColumnsStatus(params, callback) {
  return {
    type: EDIT_COLUMNS_STATUS,
    params,
    callback,
  };
}

export function getTable(id, callback) {
  return {
    type: GET_TABLE,
    id,
    callback,
  };
}

export function getTables(params, callback) {
  return {
    type: GET_TABLES,
    params,
    callback,
  };
}

export function createTable(params, callback) {
  return {
    type: CREATE_TABLE,
    params,
    callback,
  };
}

export function editTable(params, callback) {
  return {
    type: EDIT_TABLE,
    params,
    callback,
  };
}

export function deleteTable(params, callback) {
  return {
    type: DELETE_TABLE,
    params,
    callback,
  };
}

export function dropTable(params, callback) {
  return {
    type: DROP_TABLE,
    params,
    callback,
  };
}

export function deleteTables(params, callback) {
  return {
    type: DELETE_TABLES,
    params,
    callback,
  };
}

export function genTableSql(params, callback) {
  return {
    type: GEN_TABLE_SQL,
    params,
    callback,
  };
}

export function genTable(params, callback) {
  return {
    type: GEN_TABLE,
    params,
    callback,
  };
}

export function addColumnsToTable(params, callback) {
  return {
    type: ADD_COLUMNS_TO_TABLE,
    params,
    callback,
  };
}

export function editTablesStatus(params, callback) {
  return {
    type: EDIT_TABLES_STATUS,
    params,
    callback,
  };
}
