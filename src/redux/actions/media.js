// get a media
export const GET_MEDIA = 'GET_MEDIA';
export const GET_MEDIA_SUCCESS = 'GET_MEDIA_SUCCESS';
export const GET_MEDIA_FAILED = 'GET_MEDIA_FAILED';

// get a list of medias
export const GET_MEDIAS = 'GET_MEDIAS';
export const GET_MEDIAS_SUCCESS = 'GET_MEDIAS_SUCCESS';
export const GET_MEDIAS_FAILED = 'GET_MEDIAS_FAILED';

// delete a media
export const DELETE_MEDIA = 'DELETE_MEDIA';
export const DELETE_MEDIA_SUCCESS = 'DELETE_MEDIA_SUCCESS';
export const DELETE_MEDIA_FAILED = 'DELETE_MEDIA_FAILED';

// delete list medias
export const DELETE_MEDIAS = 'DELETE_MEDIAS';
export const DELETE_MEDIAS_SUCCESS = 'DELETE_MEDIAS_SUCCESS';
export const DELETE_MEDIAS_FAILED = 'DELETE_MEDIAS_FAILED';

// edit a media
export const EDIT_MEDIA = 'EDIT_MEDIA';
export const EDIT_MEDIA_SUCCESS = 'EDIT_MEDIA_SUCCESS';
export const EDIT_MEDIA_FAILED = 'EDIT_MEDIA_FAILED';

// create a media
export const CREATE_MEDIA = 'CREATE_MEDIA';
export const CREATE_MEDIA_SUCCESS = 'CREATE_MEDIA_SUCCESS';
export const CREATE_MEDIA_FAILED = 'CREATE_MEDIA_FAILED';

// move list medias
export const MOVE_MEDIAS = 'MOVE_MEDIAS';
export const MOVE_MEDIAS_SUCCESS = 'MOVE_MEDIAS_SUCCESS';
export const MOVE_MEDIAS_FAILED = 'MOVE_MEDIAS_FAILED';

// get a media category
export const GET_MEDIA_CATE = 'GET_MEDIA_CATE';
export const GET_MEDIA_CATE_SUCCESS = 'GET_MEDIA_CATE_SUCCESS';
export const GET_MEDIA_CATE_FAILED = 'GET_MEDIA_CATE_FAILED';

// get a list of media categories
export const GET_MEDIA_CATES = 'GET_MEDIA_CATES';
export const GET_MEDIA_CATES_SUCCESS = 'GET_MEDIA_CATES_SUCCESS';
export const GET_MEDIA_CATES_FAILED = 'GET_MEDIA_CATES_FAILED';

//create a media category
export const CREATE_MEDIA_CATE = 'CREATE_MEDIA_CATE';
export const CREATE_MEDIA_CATE_SUCCESS = 'CREATE_MEDIA_CATE_SUCCESS';
export const CREATE_MEDIA_CATE_FAILED = 'CREATE_MEDIA_CATE_FAILED';

//edit a media category
export const EDIT_MEDIA_CATE = 'EDIT_MEDIA_CATE';
export const EDIT_MEDIA_CATE_SUCCESS = 'EDIT_MEDIA_CATE_SUCCESS';
export const EDIT_MEDIA_CATE_FAILED = 'EDIT_MEDIA_CATE_FAILED';

//delete a media category
export const DELETE_MEDIA_CATE = 'DELETE_MEDIA_CATE';
export const DELETE_MEDIA_CATE_SUCCESS = 'DELETE_MEDIA_CATE_SUCCESS';
export const DELETE_MEDIA_CATE_FAILED = 'DELETE_MEDIA_CATE_FAILED';

//delete a media categories
export const DELETE_MEDIA_CATES = 'DELETE_MEDIA_CATES';
export const DELETE_MEDIA_CATES_SUCCESS = 'DELETE_MEDIA_CATES_SUCCESS';
export const DELETE_MEDIA_CATES_FAILED = 'DELETE_MEDIA_CATES_FAILED';

export function getMedia(params, callback) {
  return {
    type: GET_MEDIA,
    params,
    callback,
  };
}

export function getMedias(params, callback) {
  return {
    type: GET_MEDIAS,
    params,
    callback,
  };
}

export function deleteMedia(params, callback) {
  return {
    type: DELETE_MEDIA,
    params,
    callback,
  };
}

export function deleteMedias(params, callback) {
  return {
    type: DELETE_MEDIAS,
    params,
    callback,
  };
}

export function moveMedias(params, callback) {
  return {
    type: MOVE_MEDIAS,
    params,
    callback,
  };
}

export function editMedia(params, callback) {
  return {
    type: EDIT_MEDIA,
    params,
    callback,
  };
}

export function createMedia(params, callback) {
  return {
    type: CREATE_MEDIA,
    params,
    callback,
  };
}

export function getMediaCate(id, callback) {
  return {
    type: GET_MEDIA_CATE,
    id,
    callback,
  };
}

export function getMediaCates(params, callback) {
  return {
    type: GET_MEDIA_CATES,
    params,
    callback,
  };
}

export function createMediaCate(params, callback) {
  return {
    type: CREATE_MEDIA_CATE,
    params,
    callback,
  };
}

export function editMediaCate(params, callback) {
  return {
    type: EDIT_MEDIA_CATE,
    params,
    callback,
  };
}
export function deleteMediaCate(params, callback) {
  return {
    type: DELETE_MEDIA_CATE,
    params,
    callback,
  };
}
export function deleteMediaCates(params, callback) {
  return {
    type: DELETE_MEDIA_CATES,
    params,
    callback,
  };
}
