// get a list of logs
export const GET_LOGS = 'GET_LOGS';
export const GET_LOGS_SUCCESS = 'GET_LOGS_SUCCESS';
export const GET_LOGS_FAILED = 'GET_LOGS_FAILED';

// create a log
export const CREATE_LOG = 'CREATE_LOG';
export const CREATE_LOG_SUCCESS = 'CREATE_LOG_SUCCESS';
export const CREATE_LOG_FAILED = 'CREATE_LOG_FAILED';

export function getLogs(params, callback) {
  return {
    type: GET_LOGS,
    params,
    callback,
  };
}

export function createLog(params, callback) {
  return {
    type: CREATE_LOG,
    params,
    callback,
  };
}
