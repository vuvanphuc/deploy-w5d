// get a form
export const GET_FORM = 'GET_FORM';
export const GET_FORM_SUCCESS = 'GET_FORM_SUCCESS';
export const GET_FORM_FAILED = 'GET_FORM_FAILED';

// get a list of forms
export const GET_FORMS = 'GET_FORMS';
export const GET_FORMS_SUCCESS = 'GET_FORMS_SUCCESS';
export const GET_FORMS_FAILED = 'GET_FORMS_FAILED';

// delete a form
export const DELETE_FORM = 'DELETE_FORM';
export const DELETE_FORM_SUCCESS = 'DELETE_FORM_SUCCESS';
export const DELETE_FORM_FAILED = 'DELETE_FORM_FAILED';

// delete list form
export const DELETE_FORMS = 'DELETE_FORMS';
export const DELETE_FORMS_SUCCESS = 'DELETE_FORMS_SUCCESS';
export const DELETE_FORMS_FAILED = 'DELETE_FORMS_FAILED';

// edit a form
export const EDIT_FORM = 'EDIT_FORM';
export const EDIT_FORM_SUCCESS = 'EDIT_FORM_SUCCESS';
export const EDIT_FORM_FAILED = 'EDIT_FORM_FAILED';

// edit a form
export const EDIT_FORMS_STATUS = 'EDIT_FORMS_STATUS';
export const EDIT_FORMS_STATUS_SUCCESS = 'EDIT_FORMS_STATUS_SUCCESS';
export const EDIT_FORMS_STATUS_FAILED = 'EDIT_FORMS_STATUS_FAILED';

// create a form
export const CREATE_FORM = 'CREATE_FORM';
export const CREATE_FORM_SUCCESS = 'CREATE_FORM_SUCCESS';
export const CREATE_FORM_FAILED = 'CREATE_FORM_FAILED';
//import list form
export const IMPORT_FORMS = 'IMPORT_FORMS';
export const IMPORT_FORMS_SUCCESS = 'IMPORT_FORMS_SUCCESS';
export const IMPORT_FORMS_FAILED = 'IMPORT_FORMS_FAILED';
// get a form category
export const GET_FORM_CATE = 'GET_FORM_CATE';
export const GET_FORM_CATE_SUCCESS = 'GET_FORM_CATE_SUCCESS';
export const GET_FORM_CATE_FAILED = 'GET_FORM_CATE_FAILED';

// get a list of form categories
export const GET_FORM_CATES = 'GET_FORM_CATES';
export const GET_FORM_CATES_SUCCESS = 'GET_FORM_CATES_SUCCESS';
export const GET_FORM_CATES_FAILED = 'GET_FORM_CATES_FAILED';

//create a form category
export const CREATE_FORM_CATE = 'CREATE_FORM_CATE';
export const CREATE_FORM_CATE_SUCCESS = 'CREATE_FORM_CATE_SUCCESS';
export const CREATE_FORM_CATE_FAILED = 'CREATE_FORM_CATE_FAILED';

//edit a form category
export const EDIT_FORM_CATE = 'EDIT_FORM_CATE';
export const EDIT_FORM_CATE_SUCCESS = 'EDIT_FORM_CATE_SUCCESS';
export const EDIT_FORM_CATE_FAILED = 'EDIT_FORM_CATE_FAILED';

//delete a form category
export const DELETE_FORM_CATE = 'DELETE_FORM_CATE';
export const DELETE_FORM_CATE_SUCCESS = 'DELETE_FORM_CATE_SUCCESS';
export const DELETE_FORM_CATE_FAILED = 'DELETE_FORM_CATE_FAILED';

//delete a form categories
export const DELETE_FORM_CATES = 'DELETE_FORM_CATES';
export const DELETE_FORM_CATES_SUCCESS = 'DELETE_FORM_CATES_SUCCESS';
export const DELETE_FORM_CATES_FAILED = 'DELETE_FORM_CATES_FAILED';

export function getForm(params, callback) {
  return {
    type: GET_FORM,
    params,
    callback,
  };
}

export function getForms(params, callback) {
  return {
    type: GET_FORMS,
    params,
    callback,
  };
}

export function deleteForm(params, callback) {
  return {
    type: DELETE_FORM,
    params,
    callback,
  };
}

export function deleteForms(params, callback) {
  return {
    type: DELETE_FORMS,
    params,
    callback,
  };
}

export function editForm(params, callback) {
  return {
    type: EDIT_FORM,
    params,
    callback,
  };
}

export function editFormsStatus(params, callback) {
  return {
    type: EDIT_FORMS_STATUS,
    params,
    callback,
  };
}

export function createForm(params, callback) {
  return {
    type: CREATE_FORM,
    params,
    callback,
  };
}
export function importForms(params, onSuccess, onError) {
  return {
    type: IMPORT_FORMS,
    params,
    onSuccess,
    onError,
  };
}
export function getFormCate(id, callback) {
  return {
    type: GET_FORM_CATE,
    id,
    callback,
  };
}

export function getFormCates(params, callback) {
  return {
    type: GET_FORM_CATES,
    params,
    callback,
  };
}

export function createFormCate(params, callback) {
  return {
    type: CREATE_FORM_CATE,
    params,
    callback,
  };
}

export function editFormCate(params, callback) {
  return {
    type: EDIT_FORM_CATE,
    params,
    callback,
  };
}

export function deleteFormCate(params, callback) {
  return {
    type: DELETE_FORM_CATE,
    params,
    callback,
  };
}

export function deleteFormCates(params, callback) {
  return {
    type: DELETE_FORM_CATES,
    params,
    callback,
  };
}
