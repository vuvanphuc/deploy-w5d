// get a appointment
export const GET_APPOINTMENT = 'GET_APPOINTMENT';
export const GET_APPOINTMENT_SUCCESS = 'GET_APPOINTMENT_SUCCESS';
export const GET_APPOINTMENT_FAILED = 'GET_APPOINTMENT_FAILED';

//get a list of appointments
export const GET_APPOINTMENTS = 'GET_APPOINTMENTS';
export const GET_APPOINTMENTS_SUCCESS = 'GET_APPOINTMENTS_SUCCESS';
export const GET_APPOINTMENTS_FAILED = 'GET_APPOINTMENTS_FAILED';

// delete a appointment
export const DELETE_APPOINTMENT = 'DELETE_APPOINTMENT';
export const DELETE_APPOINTMENT_SUCCESS = 'DELETE_APPOINTMENT_SUCCESS';
export const DELETE_APPOINTMENT_FAILED = 'DELETE_APPOINTMENT_FAILED';

// delete list appointment
export const DELETE_APPOINTMENTS = 'DELETE_APPOINTMENTS';
export const DELETE_APPOINTMENTS_SUCCESS = 'DELETE_APPOINTMENTS_SUCCESS';
export const DELETE_APPOINTMENTS_FAILED = 'DELETE_APPOINTMENTS_FAILED';

// edit a appointment
export const EDIT_APPOINTMENT = 'EDIT_APPOINTMENT';
export const EDIT_APPOINTMENT_SUCCESS = 'EDIT_APPOINTMENT_SUCCESS';
export const EDIT_APPOINTMENT_FAILED = 'EDIT_APPOINTMENT_FAILED';

// edit status list appointment
export const EDIT_STATUS_APPOINTMENTS = 'EDIT_STATUS_APPOINTMENTS';
export const EDIT_STATUS_APPOINTMENTS_SUCCESS = 'EDIT_APPOINTMENTS_SUCCESS';
export const EDIT_STATUS_APPOINTMENTS_FAILED = 'EDIT_APPOINTMENTS_FAILED';

// create a appointment
export const CREATE_APPOINTMENT = 'CREATE_APPOINTMENT';
export const CREATE_APPOINTMENT_SUCCESS = 'CREATE_APPOINTMENT_SUCCESS';
export const CREATE_APPOINTMENT_FAILED = 'CREATE_APPOINTMENT_FAILED';

//import list appointment
export const IMPORT_APPOINTMENTS = 'IMPORT_APPOINTMENTS';
export const IMPORT_APPOINTMENTS_SUCCESS = 'IMPORT_APPOINTMENTS_SUCCESS';
export const IMPORT_APPOINTMENTS_FAILED = 'IMPORT_APPOINTMENTS_FAILED';

// register member
export const REGISTER_APPOINTMENT = 'REGISTER_APPOINTMENT';
export const REGISTER_APPOINTMENT_SUCCESS = 'REGISTER_APPOINTMENT_SUCCESS';
export const REGISTER_APPOINTMENT_FAILED = 'REGISTER_APPOINTMENT_FAILED';

// get a appointment category
export const GET_APPOINTMENT_CATE = 'GET_APPOINTMENT_CATE';
export const GET_APPOINTMENT_CATE_SUCCESS = 'GET_APPOINTMENT_CATE_SUCCESS';
export const GET_APPOINTMENT_CATE_FAILED = 'GET_APPOINTMENT_CATE_FAILED';

//get a list of appointment categories
export const GET_APPOINTMENT_CATES = 'GET_APPOINTMENT_CATES';
export const GET_APPOINTMENT_CATES_SUCCESS = 'GET_APPOINTMENT_CATES_SUCCESS';
export const GET_APPOINTMENT_CATES_FAILED = 'GET_APPOINTMENT_CATES_FAILED';

//create a appointment category
export const CREATE_APPOINTMENT_CATE = 'CREATE_APPOINTMENT_CATE';
export const CREATE_APPOINTMENT_CATE_SUCCESS = 'CREATE_APPOINTMENT_CATE_SUCCESS';
export const CREATE_APPOINTMENT_CATE_FAILED = 'CREATE_APPOINTMENT_CATE_FAILED';

//edit a appointment category
export const EDIT_APPOINTMENT_CATE = 'EDIT_APPOINTMENT_CATE';
export const EDIT_APPOINTMENT_CATE_SUCCESS = 'EDIT_APPOINTMENT_CATE_SUCCESS';
export const EDIT_APPOINTMENT_CATE_FAILED = 'EDIT_APPOINTMENT_CATE_FAILED';

//delete a appointment category
export const DELETE_APPOINTMENT_CATE = 'DELETE_APPOINTMENT_CATE';
export const DELETE_APPOINTMENT_CATE_SUCCESS = 'DELETE_APPOINTMENT_CATE_SUCCESS';
export const DELETE_APPOINTMENT_CATE_FAILED = 'DELETE_APPOINTMENT_CATE_FAILED';

//delete a appointment categories
export const DELETE_APPOINTMENT_CATES = 'DELETE_APPOINTMENT_CATES';
export const DELETE_APPOINTMENT_CATES_SUCCESS = 'DELETE_APPOINTMENT_CATES_SUCCESS';
export const DELETE_APPOINTMENT_CATES_FAILED = 'DELETE_APPOINTMENT_CATES_FAILED';

export function getAppointment(params, callback) {
  return {
    type: GET_APPOINTMENT,
    params,
    callback,
  };
}

export function getAppointments(params, callback) {
  return {
    type: GET_APPOINTMENTS,
    params,
    callback,
  };
}

export function deleteAppointment(params, callback) {
  return {
    type: DELETE_APPOINTMENT,
    params,
    callback,
  };
}

export function deleteAppointments(params, callback) {
  return {
    type: DELETE_APPOINTMENTS,
    params,
    callback,
  };
}

export function editAppointment(params, callback) {
  return {
    type: EDIT_APPOINTMENT,
    params,
    callback,
  };
}

export function editStatusAppointment(params, callback) {
  return {
    type: EDIT_STATUS_APPOINTMENTS,
    params,
    callback,
  };
}

export function createAppointment(params, callback) {
  return {
    type: CREATE_APPOINTMENT,
    params,
    callback,
  };
}
export function importAppointments(params, onSuccess, onError) {
  return {
    type: IMPORT_APPOINTMENTS,
    params,
    onSuccess,
    onError,
  };
}
export function getAppointmentCate(id, callback) {
  return {
    type: GET_APPOINTMENT_CATE,
    id,
    callback,
  };
}

export function getAppointmentCates(params, callback) {
  return {
    type: GET_APPOINTMENT_CATES,
    params,
    callback,
  };
}

export function createAppointmentCate(params, callback) {
  return {
    type: CREATE_APPOINTMENT_CATE,
    params,
    callback,
  };
}

export function editAppointmentCate(params, callback) {
  return {
    type: EDIT_APPOINTMENT_CATE,
    params,
    callback,
  };
}

export function deleteAppointmentCate(params, callback) {
  return {
    type: DELETE_APPOINTMENT_CATE,
    params,
    callback,
  };
}

export function deleteAppointmentCates(params, callback) {
  return {
    type: DELETE_APPOINTMENT_CATES,
    params,
    callback,
  };
}
