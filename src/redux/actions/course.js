// get a course
export const GET_COURSE = 'GET_COURSE';
export const GET_COURSE_SUCCESS = 'GET_COURSE_SUCCESS';
export const GET_COURSE_FAILED = 'GET_COURSE_FAILED';

//get a list of courses
export const GET_COURSES = 'GET_COURSES';
export const GET_COURSES_SUCCESS = 'GET_COURSES_SUCCESS';
export const GET_COURSES_FAILED = 'GET_COURSES_FAILED';

// delete a course
export const DELETE_COURSE = 'DELETE_COURSE';
export const DELETE_COURSE_SUCCESS = 'DELETE_COURSE_SUCCESS';
export const DELETE_COURSE_FAILED = 'DELETE_COURSE_FAILED';

// delete list course
export const DELETE_COURSES = 'DELETE_COURSES';
export const DELETE_COURSES_SUCCESS = 'DELETE_COURSES_SUCCESS';
export const DELETE_COURSES_FAILED = 'DELETE_COURSES_FAILED';

// edit a course
export const EDIT_COURSE = 'EDIT_COURSE';
export const EDIT_COURSE_SUCCESS = 'EDIT_COURSE_SUCCESS';
export const EDIT_COURSE_FAILED = 'EDIT_COURSE_FAILED';

// edit status list course
export const EDIT_STATUS_COURSES = 'EDIT_STATUS_COURSES';
export const EDIT_STATUS_COURSES_SUCCESS = 'EDIT_COURSES_SUCCESS';
export const EDIT_STATUS_COURSES_FAILED = 'EDIT_COURSES_FAILED';

// create a course
export const CREATE_COURSE = 'CREATE_COURSE';
export const CREATE_COURSE_SUCCESS = 'CREATE_COURSE_SUCCESS';
export const CREATE_COURSE_FAILED = 'CREATE_COURSE_FAILED';

// get a course category
export const GET_COURSE_CATE = 'GET_COURSE_CATE';
export const GET_COURSE_CATE_SUCCESS = 'GET_COURSE_CATE_SUCCESS';
export const GET_COURSE_CATE_FAILED = 'GET_COURSE_CATE_FAILED';

//get a list of course categories
export const GET_COURSE_CATES = 'GET_COURSE_CATES';
export const GET_COURSE_CATES_SUCCESS = 'GET_COURSE_CATES_SUCCESS';
export const GET_COURSE_CATES_FAILED = 'GET_COURSE_CATES_FAILED';

//create a course category
export const CREATE_COURSE_CATE = 'CREATE_COURSE_CATE';
export const CREATE_COURSE_CATE_SUCCESS = 'CREATE_COURSE_CATE_SUCCESS';
export const CREATE_COURSE_CATE_FAILED = 'CREATE_COURSE_CATE_FAILED';

//edit a course category
export const EDIT_COURSE_CATE = 'EDIT_COURSE_CATE';
export const EDIT_COURSE_CATE_SUCCESS = 'EDIT_COURSE_CATE_SUCCESS';
export const EDIT_COURSE_CATE_FAILED = 'EDIT_COURSE_CATE_FAILED';

//delete a course category
export const DELETE_COURSE_CATE = 'DELETE_COURSE_CATE';
export const DELETE_COURSE_CATE_SUCCESS = 'DELETE_COURSE_CATE_SUCCESS';
export const DELETE_COURSE_CATE_FAILED = 'DELETE_COURSE_CATE_FAILED';

//delete a course categories
export const DELETE_COURSE_CATES = 'DELETE_COURSE_CATES';
export const DELETE_COURSE_CATES_SUCCESS = 'DELETE_COURSE_CATES_SUCCESS';
export const DELETE_COURSE_CATES_FAILED = 'DELETE_COURSE_CATES_FAILED';

// set course search params
export const SET_SEARCH_COURSE = 'SET_SEARCH_COURSE';

export function getCourse(params, callback) {
  return {
    type: GET_COURSE,
    params,
    callback,
  };
}

export function getCourses(params, callback) {
  return {
    type: GET_COURSES,
    params,
    callback,
  };
}

export function deleteCourse(params, callback) {
  return {
    type: DELETE_COURSE,
    params,
    callback,
  };
}

export function deleteCourses(params, callback) {
  return {
    type: DELETE_COURSES,
    params,
    callback,
  };
}

export function editCourse(params, callback) {
  return {
    type: EDIT_COURSE,
    params,
    callback,
  };
}

export function editStatusCourse(params, callback) {
  return {
    type: EDIT_STATUS_COURSES,
    params,
    callback,
  };
}

export function createCourse(params, callback) {
  return {
    type: CREATE_COURSE,
    params,
    callback,
  };
}

export function getCourseCate(id, callback) {
  return {
    type: GET_COURSE_CATE,
    id,
    callback,
  };
}

export function getCourseCates(params, callback) {
  return {
    type: GET_COURSE_CATES,
    params,
    callback,
  };
}

export function createCourseCate(params, callback) {
  return {
    type: CREATE_COURSE_CATE,
    params,
    callback,
  };
}

export function editCourseCate(params, callback) {
  return {
    type: EDIT_COURSE_CATE,
    params,
    callback,
  };
}

export function deleteCourseCate(params, callback) {
  return {
    type: DELETE_COURSE_CATE,
    params,
    callback,
  };
}

export function deleteCourseCates(params, callback) {
  return {
    type: DELETE_COURSE_CATES,
    params,
    callback,
  };
}

export function setSearchParams(params) {
  return {
    type: SET_SEARCH_COURSE,
    params,
  };
}
