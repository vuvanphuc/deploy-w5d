export const GET_GLOBAL = 'GET_GLOBAL';
export const EDIT_GLOBAL = 'EDIT_GLOBAL';

export function getGlobal() {
  return {
    type: GET_GLOBAL,
  };
}

export function editGlobal(key, value) {
  return {
    type: EDIT_GLOBAL,
    key,
    value,
  };
}

// get a config
export const GET_CONFIG = 'GET_CONFIG';
export const GET_CONFIG_SUCCESS = 'GET_CONFIG_SUCCESS';
export const GET_CONFIG_FAILED = 'GET_CONFIG_FAILED';

// get a list of configs
export const GET_CONFIGS = 'GET_CONFIGS';
export const GET_CONFIGS_SUCCESS = 'GET_CONFIGS_SUCCESS';
export const GET_CONFIGS_FAILED = 'GET_CONFIGS_FAILED';
// send test email
export const SEND_TEST_EMAIL = 'SEND_TEST_EMAIL';
export const SEND_TEST_EMAIL_SUCCESS = 'SEND_TEST_EMAIL_SUCCESS';
export const SEND_TEST_EMAIL_FAILED = 'SEND_TEST_EMAIL_FAILED';
// delete a config
export const DELETE_CONFIG = 'DELETE_CONFIG';
export const DELETE_CONFIG_SUCCESS = 'DELETE_CONFIG_SUCCESS';
export const DELETE_CONFIG_FAILED = 'DELETE_CONFIG_FAILED';

// edit a config
export const EDIT_CONFIG = 'EDIT_CONFIG';
export const EDIT_CONFIG_SUCCESS = 'EDIT_CONFIG_SUCCESS';
export const EDIT_CONFIG_FAILED = 'EDIT_CONFIG_FAILED';

// create a config
export const CREATE_CONFIG = 'CREATE_CONFIG';
export const CREATE_CONFIG_SUCCESS = 'CREATE_CONFIG_SUCCESS';
export const CREATE_CONFIG_FAILED = 'CREATE_CONFIG_FAILED';

export function getConfig(params, callback) {
  return {
    type: GET_CONFIG,
    params,
    callback,
  };
}

export function getConfigs(params, callback) {
  return {
    type: GET_CONFIGS,
    params,
    callback,
  };
}

export function deleteConfig(params, callback) {
  return {
    type: DELETE_CONFIG,
    params,
    callback,
  };
}

export function editConfig(params, callback) {
  return {
    type: EDIT_CONFIG,
    params,
    callback,
  };
}

export function createConfig(params, callback) {
  return {
    type: CREATE_CONFIG,
    params,
    callback,
  };
}
export function sendTestEmail(params, callback) {
  return {
    type: SEND_TEST_EMAIL,
    params,
    callback,
  };
}
