// get a EXAM
export const GET_EXAM = 'GET_EXAM';
export const GET_EXAM_SUCCESS = 'GET_EXAM_SUCCESS';
export const GET_EXAM_FAILED = 'GET_EXAM_FAILED';

//get a list of exam
export const GET_EXAMS = 'GET_EXAMS';
export const GET_EXAMS_SUCCESS = 'GET_EXAMS_SUCCESS';
export const GET_EXAMS_FAILED = 'GET_EXAMS_FAILED';

//get exam history
export const GET_EXAM_HISTORY = 'GET_EXAM_HISTORY';
export const GET_EXAM_HISTORY_SUCCESS = 'GET_EXAM_HISTORY_SUCCESS';
export const GET_EXAM_HISTORY_FAILED = 'GET_EXAM_HISTORY_FAILED';

//get list of exam topic
export const GET_EXAM_TOPICS = 'GET_EXAM_TOPICS';
export const GET_EXAM_TOPICS_SUCCESS = 'GET_EXAM_TOPICS_SUCCESS';
export const GET_EXAM_TOPICS_FAILED = 'GET_EXAM_TOPICS_FAILED';

//get exam topic
export const GET_EXAM_TOPIC = 'GET_EXAM_TOPIC';
export const GET_EXAM_TOPIC_SUCCESS = 'GET_EXAM_TOPIC_SUCCESS';
export const GET_EXAM_TOPIC_FAILED = 'GET_EXAM_TOPIC_FAILED';

// get a list of exam histories
export const GET_EXAM_HISTORIES = 'GET_EXAM_HISTORIES';
export const GET_EXAM_HISTORIES_SUCCESS = 'GET_EXAM_HISTORIES_SUCCESS';
export const GET_EXAM_HISTORIES_FAILED = 'GET_EXAM_HISTORIES_FAILED';

// delete a exam
export const DELETE_EXAM = 'DELETE_EXAM';
export const DELETE_EXAM_SUCCESS = 'DELETE_EXAM_SUCCESS';
export const DELETE_EXAM_FAILED = 'DELETE_EXAM_FAILED';

// delete list exam
export const DELETE_EXAMS = 'DELETE_EXAMS';
export const DELETE_EXAMS_SUCCESS = 'DELETE_EXAMS_SUCCESS';
export const DELETE_EXAMS_FAILED = 'DELETE_EXAMS_FAILED';

// delete a exam history
export const DELETE_EXAM_HISTORY = 'DELETE_EXAM_HISTORY';
export const DELETE_EXAM_HISTORY_SUCCESS = 'DELETE_EXAM_HISTORY_SUCCESS';
export const DELETE_EXAM_HISTORY_FAILED = 'DELETE_EXAM_HISTORY_FAILED';

// delete list exam histories
export const DELETE_EXAM_HISTORIES = 'DELETE_EXAM_HISTORIES';
export const DELETE_EXAM_HISTORIES_SUCCESS = 'DELETE_EXAM_HISTORIES_SUCCESS';
export const DELETE_EXAM_HISTORIES_FAILED = 'DELETE_EXAM_HISTORIES_FAILED';

// delete a exam topic
export const DELETE_EXAM_TOPIC = 'DELETE_EXAM_TOPIC';
export const DELETE_EXAM_TOPIC_SUCCESS = 'DELETE_EXAM_TOPIC_SUCCESS';
export const DELETE_EXAM_TOPIC_FAILED = 'DELETE_EXAM_TOPIC_FAILED';

// delete list of exam topic
export const DELETE_EXAM_TOPICS = 'DELETE_EXAM_TOPICS';
export const DELETE_EXAM_TOPICS_SUCCESS = 'DELETE_EXAM_TOPICS_SUCCESS';
export const DELETE_EXAM_TOPICS_FAILED = 'DELETE_EXAM_TOPICS_FAILED';

// edit a EXAM
export const EDIT_EXAM = 'EDIT_EXAM';
export const EDIT_EXAM_SUCCESS = 'EDIT_EXAM_SUCCESS';
export const EDIT_EXAM_FAILED = 'EDIT_EXAM_FAILED';

// edit a exam history
export const EDIT_EXAM_HISTORY = 'EDIT_EXAM_HISTORY';
export const EDIT_EXAM_HISTORY_SUCCESS = 'EDIT_EXAM_HISTORY_SUCCESS';
export const EDIT_EXAM_HISTORY_FAILED = 'EDIT_EXAM_HISTORY_FAILED';

// edit a exam topic
export const EDIT_EXAM_TOPIC = 'EDIT_EXAM_TOPIC';
export const EDIT_EXAM_TOPIC_SUCCESS = 'EDIT_EXAM_TOPIC_SUCCESS';
export const EDIT_EXAM_TOPIC_FAILED = 'EDIT_EXAM_TOPIC_FAILED';

// edit status list EXAM
export const EDIT_STATUS_EXAMS = 'EDIT_STATUS_EXAMS';
export const EDIT_STATUS_EXAMS_SUCCESS = 'EDIT_EXAMS_SUCCESS';
export const EDIT_STATUS_EXAMS_FAILED = 'EDIT_EXAMS_FAILED';

// create a EXAM
export const CREATE_EXAM = 'CREATE_EXAM';
export const CREATE_EXAM_SUCCESS = 'CREATE_EXAM_SUCCESS';
export const CREATE_EXAM_FAILED = 'CREATE_EXAM_FAILED';

// create a exam history
export const CREATE_EXAM_HISTORY = 'CREATE_EXAM_HISTORY';
export const CREATE_EXAM_HISTORY_SUCCESS = 'CREATE_EXAM_HISTORY_SUCCESS';
export const CREATE_EXAM_HISTORY_FAILED = 'CREATE_EXAM_HISTORY_FAILED';

// create a exam topic
export const CREATE_EXAM_TOPIC = 'CREATE_EXAM_TOPIC';
export const CREATE_EXAM_TOPIC_SUCCESS = 'CREATE_EXAM_TOPIC_SUCCESS';
export const CREATE_EXAM_TOPIC_FAILED = 'CREATE_EXAM_TOPIC_FAILED';

// get a EXAM category
export const GET_EXAM_CATE = 'GET_EXAM_CATE';
export const GET_EXAM_CATE_SUCCESS = 'GET_EXAM_CATE_SUCCESS';
export const GET_EXAM_CATE_FAILED = 'GET_EXAM_CATE_FAILED';

//get a list of EXAM categories
export const GET_EXAM_CATES = 'GET_EXAM_CATES';
export const GET_EXAM_CATES_SUCCESS = 'GET_EXAM_CATES_SUCCESS';
export const GET_EXAM_CATES_FAILED = 'GET_EXAM_CATES_FAILED';

//create a EXAM category
export const CREATE_EXAM_CATE = 'CREATE_EXAM_CATE';
export const CREATE_EXAM_CATE_SUCCESS = 'CREATE_EXAM_CATE_SUCCESS';
export const CREATE_EXAM_CATE_FAILED = 'CREATE_EXAM_CATE_FAILED';

//edit a EXAM category
export const EDIT_EXAM_CATE = 'EDIT_EXAM_CATE';
export const EDIT_EXAM_CATE_SUCCESS = 'EDIT_EXAM_CATE_SUCCESS';
export const EDIT_EXAM_CATE_FAILED = 'EDIT_EXAM_CATE_FAILED';

//delete a EXAM category
export const DELETE_EXAM_CATE = 'DELETE_EXAM_CATE';
export const DELETE_EXAM_CATE_SUCCESS = 'DELETE_EXAM_CATE_SUCCESS';
export const DELETE_EXAM_CATE_FAILED = 'DELETE_EXAM_CATE_FAILED';

//delete a EXAM categories
export const DELETE_EXAM_CATES = 'DELETE_EXAM_CATES';
export const DELETE_EXAM_CATES_SUCCESS = 'DELETE_EXAM_CATES_SUCCESS';
export const DELETE_EXAM_CATES_FAILED = 'DELETE_EXAM_CATES_FAILED';

// set EXAM search params
export const SET_SEARCH_EXAM = 'SET_SEARCH_EXAM';

export function getExam(params, callback) {
  return {
    type: GET_EXAM,
    params,
    callback,
  };
}

export function getExams(params, callback) {
  return {
    type: GET_EXAMS,
    params,
    callback,
  };
}

export function getExamHistory(params, callback) {
  return {
    type: GET_EXAM_HISTORY,
    params,
    callback,
  };
}

export function getExamHistories(params, callback) {
  return {
    type: GET_EXAM_HISTORIES,
    params,
    callback,
  };
}

export function getExamTopic(params, callback) {
  return {
    type: GET_EXAM_TOPIC,
    params,
    callback,
  };
}

export function getExamTopics(params, callback) {
  return {
    type: GET_EXAM_TOPICS,
    params,
    callback,
  };
}

export function deleteExam(params, callback) {
  return {
    type: DELETE_EXAM,
    params,
    callback,
  };
}

export function deleteExams(params, callback) {
  return {
    type: DELETE_EXAMS,
    params,
    callback,
  };
}

export function deleteExamHistory(params, callback) {
  return {
    type: DELETE_EXAM_HISTORY,
    params,
    callback,
  };
}

export function deleteExamHistories(params, callback) {
  return {
    type: DELETE_EXAM_HISTORIES,
    params,
    callback,
  };
}

export function deleteExamTopic(params, callback) {
  return {
    type: DELETE_EXAM_TOPIC,
    params,
    callback,
  };
}

export function deleteExamTopics(params, callback) {
  return {
    type: DELETE_EXAM_TOPICS,
    params,
    callback,
  };
}

export function editExam(params, callback) {
  return {
    type: EDIT_EXAM,
    params,
    callback,
  };
}

export function editHistory(params, callback) {
  return {
    type: EDIT_EXAM_HISTORY,
    params,
    callback,
  };
}

export function editExamTopic(params, callback) {
  return {
    type: EDIT_EXAM_TOPIC,
    params,
    callback,
  };
}

export function editStatusExam(params, callback) {
  return {
    type: EDIT_STATUS_EXAMS,
    params,
    callback,
  };
}

export function createExam(params, callback) {
  return {
    type: CREATE_EXAM,
    params,
    callback,
  };
}

export function createExamHistory(params, callback) {
  return {
    type: CREATE_EXAM_HISTORY,
    params,
    callback,
  };
}

export function createExamTopic(params, callback) {
  return {
    type: CREATE_EXAM_TOPIC,
    params,
    callback,
  };
}

export function getExamCate(id, callback) {
  return {
    type: GET_EXAM_CATE,
    id,
    callback,
  };
}

export function getExamCates(params, callback) {
  return {
    type: GET_EXAM_CATES,
    params,
    callback,
  };
}

export function createExamCate(params, callback) {
  return {
    type: CREATE_EXAM_CATE,
    params,
    callback,
  };
}

export function editExamCate(params, callback) {
  return {
    type: EDIT_EXAM_CATE,
    params,
    callback,
  };
}

export function deleteExamCate(params, callback) {
  return {
    type: DELETE_EXAM_CATE,
    params,
    callback,
  };
}

export function deleteExamCates(params, callback) {
  return {
    type: DELETE_EXAM_CATES,
    params,
    callback,
  };
}

export function setSearchParams(params) {
  return {
    type: SET_SEARCH_EXAM,
    params,
  };
}
