// get a remind
export const GET_REMIND = 'GET_REMIND';
export const GET_REMIND_SUCCESS = 'GET_REMIND_SUCCESS';
export const GET_REMIND_FAILED = 'GET_REMIND_FAILED';

//get a list of reminds
export const GET_REMINDS = 'GET_REMINDS';
export const GET_REMINDS_SUCCESS = 'GET_REMINDS_SUCCESS';
export const GET_REMINDS_FAILED = 'GET_REMINDS_FAILED';

// delete a remind
export const DELETE_REMIND = 'DELETE_REMIND';
export const DELETE_REMIND_SUCCESS = 'DELETE_REMIND_SUCCESS';
export const DELETE_REMIND_FAILED = 'DELETE_REMIND_FAILED';

// delete list remind
export const DELETE_REMINDS = 'DELETE_REMINDS';
export const DELETE_REMINDS_SUCCESS = 'DELETE_REMINDS_SUCCESS';
export const DELETE_REMINDS_FAILED = 'DELETE_REMINDS_FAILED';

// edit a remind
export const EDIT_REMIND = 'EDIT_REMIND';
export const EDIT_REMIND_SUCCESS = 'EDIT_REMIND_SUCCESS';
export const EDIT_REMIND_FAILED = 'EDIT_REMIND_FAILED';

// edit status list remind
export const EDIT_STATUS_REMINDS = 'EDIT_STATUS_REMINDS';
export const EDIT_STATUS_REMINDS_SUCCESS = 'EDIT_REMINDS_SUCCESS';
export const EDIT_STATUS_REMINDS_FAILED = 'EDIT_REMINDS_FAILED';

// create a remind
export const CREATE_REMIND = 'CREATE_REMIND';
export const CREATE_REMIND_SUCCESS = 'CREATE_REMIND_SUCCESS';
export const CREATE_REMIND_FAILED = 'CREATE_REMIND_FAILED';

//import list remind
export const IMPORT_REMINDS = 'IMPORT_REMINDS';
export const IMPORT_REMINDS_SUCCESS = 'IMPORT_REMINDS_SUCCESS';
export const IMPORT_REMINDS_FAILED = 'IMPORT_REMINDS_FAILED';

// register member
export const REGISTER_REMIND = 'REGISTER_REMIND';
export const REGISTER_REMIND_SUCCESS = 'REGISTER_REMIND_SUCCESS';
export const REGISTER_REMIND_FAILED = 'REGISTER_REMIND_FAILED';

// get a remind category
export const GET_REMIND_CATE = 'GET_REMIND_CATE';
export const GET_REMIND_CATE_SUCCESS = 'GET_REMIND_CATE_SUCCESS';
export const GET_REMIND_CATE_FAILED = 'GET_REMIND_CATE_FAILED';

//get a list of remind categories
export const GET_REMIND_CATES = 'GET_REMIND_CATES';
export const GET_REMIND_CATES_SUCCESS = 'GET_REMIND_CATES_SUCCESS';
export const GET_REMIND_CATES_FAILED = 'GET_REMIND_CATES_FAILED';

//create a remind category
export const CREATE_REMIND_CATE = 'CREATE_REMIND_CATE';
export const CREATE_REMIND_CATE_SUCCESS = 'CREATE_REMIND_CATE_SUCCESS';
export const CREATE_REMIND_CATE_FAILED = 'CREATE_REMIND_CATE_FAILED';

//edit a remind category
export const EDIT_REMIND_CATE = 'EDIT_REMIND_CATE';
export const EDIT_REMIND_CATE_SUCCESS = 'EDIT_REMIND_CATE_SUCCESS';
export const EDIT_REMIND_CATE_FAILED = 'EDIT_REMIND_CATE_FAILED';

//delete a remind category
export const DELETE_REMIND_CATE = 'DELETE_REMIND_CATE';
export const DELETE_REMIND_CATE_SUCCESS = 'DELETE_REMIND_CATE_SUCCESS';
export const DELETE_REMIND_CATE_FAILED = 'DELETE_REMIND_CATE_FAILED';

//delete a remind categories
export const DELETE_REMIND_CATES = 'DELETE_REMIND_CATES';
export const DELETE_REMIND_CATES_SUCCESS = 'DELETE_REMIND_CATES_SUCCESS';
export const DELETE_REMIND_CATES_FAILED = 'DELETE_REMIND_CATES_FAILED';

export function getRemind(params, callback) {
  return {
    type: GET_REMIND,
    params,
    callback,
  };
}

export function getReminds(params, callback) {
  return {
    type: GET_REMINDS,
    params,
    callback,
  };
}

export function deleteRemind(params, callback) {
  return {
    type: DELETE_REMIND,
    params,
    callback,
  };
}

export function deleteReminds(params, callback) {
  return {
    type: DELETE_REMINDS,
    params,
    callback,
  };
}

export function editRemind(params, callback) {
  return {
    type: EDIT_REMIND,
    params,
    callback,
  };
}

export function editStatusRemind(params, callback) {
  return {
    type: EDIT_STATUS_REMINDS,
    params,
    callback,
  };
}

export function createRemind(params, callback) {
  return {
    type: CREATE_REMIND,
    params,
    callback,
  };
}
export function importReminds(params, onSuccess, onError) {
  return {
    type: IMPORT_REMINDS,
    params,
    onSuccess,
    onError,
  };
}
export function getRemindCate(id, callback) {
  return {
    type: GET_REMIND_CATE,
    id,
    callback,
  };
}

export function getRemindCates(params, callback) {
  return {
    type: GET_REMIND_CATES,
    params,
    callback,
  };
}

export function createRemindCate(params, callback) {
  return {
    type: CREATE_REMIND_CATE,
    params,
    callback,
  };
}

export function editRemindCate(params, callback) {
  return {
    type: EDIT_REMIND_CATE,
    params,
    callback,
  };
}

export function deleteRemindCate(params, callback) {
  return {
    type: DELETE_REMIND_CATE,
    params,
    callback,
  };
}

export function deleteRemindCates(params, callback) {
  return {
    type: DELETE_REMIND_CATES,
    params,
    callback,
  };
}
