// get a POST
export const GET_POST = 'GET_POST';
export const GET_POST_SUCCESS = 'GET_POST_SUCCESS';
export const GET_POST_FAILED = 'GET_POST_FAILED';

//get a list of POSTs
export const GET_POSTS = 'GET_POSTS';
export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS';
export const GET_POSTS_FAILED = 'GET_POSTS_FAILED';

// delete a POST
export const DELETE_POST = 'DELETE_POST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILED = 'DELETE_POST_FAILED';

// delete list POST
export const DELETE_POSTS = 'DELETE_POSTS';
export const DELETE_POSTS_SUCCESS = 'DELETE_POSTS_SUCCESS';
export const DELETE_POSTS_FAILED = 'DELETE_POSTS_FAILED';

// edit a POST
export const EDIT_POST = 'EDIT_POST';
export const EDIT_POST_SUCCESS = 'EDIT_POST_SUCCESS';
export const EDIT_POST_FAILED = 'EDIT_POST_FAILED';

// edit status list POST
export const EDIT_STATUS_POSTS = 'EDIT_STATUS_POSTS';
export const EDIT_STATUS_POSTS_SUCCESS = 'EDIT_POSTS_SUCCESS';
export const EDIT_STATUS_POSTS_FAILED = 'EDIT_POSTS_FAILED';

// create a POST
export const CREATE_POST = 'CREATE_POST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILED = 'CREATE_POST_FAILED';

// get a POST category
export const GET_POST_CATE = 'GET_POST_CATE';
export const GET_POST_CATE_SUCCESS = 'GET_POST_CATE_SUCCESS';
export const GET_POST_CATE_FAILED = 'GET_POST_CATE_FAILED';

//get a list of POST categories
export const GET_POST_CATES = 'GET_POST_CATES';
export const GET_POST_CATES_SUCCESS = 'GET_POST_CATES_SUCCESS';
export const GET_POST_CATES_FAILED = 'GET_POST_CATES_FAILED';

//create a POST category
export const CREATE_POST_CATE = 'CREATE_POST_CATE';
export const CREATE_POST_CATE_SUCCESS = 'CREATE_POST_CATE_SUCCESS';
export const CREATE_POST_CATE_FAILED = 'CREATE_POST_CATE_FAILED';

//edit a POST category
export const EDIT_POST_CATE = 'EDIT_POST_CATE';
export const EDIT_POST_CATE_SUCCESS = 'EDIT_POST_CATE_SUCCESS';
export const EDIT_POST_CATE_FAILED = 'EDIT_POST_CATE_FAILED';

//delete a POST category
export const DELETE_POST_CATE = 'DELETE_POST_CATE';
export const DELETE_POST_CATE_SUCCESS = 'DELETE_POST_CATE_SUCCESS';
export const DELETE_POST_CATE_FAILED = 'DELETE_POST_CATE_FAILED';

//delete a POST categories
export const DELETE_POST_CATES = 'DELETE_POST_CATES';
export const DELETE_POST_CATES_SUCCESS = 'DELETE_POST_CATES_SUCCESS';
export const DELETE_POST_CATES_FAILED = 'DELETE_POST_CATES_FAILED';

// set POST search params
export const SET_SEARCH_POST = 'SET_SEARCH_POST';

export function getPost(params, callback) {
  return {
    type: GET_POST,
    params,
    callback,
  };
}

export function getPosts(params, callback) {
  return {
    type: GET_POSTS,
    params,
    callback,
  };
}

export function deletePost(params, callback) {
  return {
    type: DELETE_POST,
    params,
    callback,
  };
}

export function deletePosts(params, callback) {
  return {
    type: DELETE_POSTS,
    params,
    callback,
  };
}

export function editPost(params, callback) {
  return {
    type: EDIT_POST,
    params,
    callback,
  };
}

export function editStatusPost(params, callback) {
  return {
    type: EDIT_STATUS_POSTS,
    params,
    callback,
  };
}

export function createPost(params, callback) {
  return {
    type: CREATE_POST,
    params,
    callback,
  };
}

export function getPostCate(id, callback) {
  return {
    type: GET_POST_CATE,
    id,
    callback,
  };
}

export function getPostCates(params, callback) {
  return {
    type: GET_POST_CATES,
    params,
    callback,
  };
}

export function createPostCate(params, callback) {
  return {
    type: CREATE_POST_CATE,
    params,
    callback,
  };
}

export function editPostCate(params, callback) {
  return {
    type: EDIT_POST_CATE,
    params,
    callback,
  };
}

export function deletePostCate(params, callback) {
  return {
    type: DELETE_POST_CATE,
    params,
    callback,
  };
}

export function deletePostCates(params, callback) {
  return {
    type: DELETE_POST_CATES,
    params,
    callback,
  };
}

export function setSearchParams(params) {
  return {
    type: SET_SEARCH_POST,
    params,
  };
}
