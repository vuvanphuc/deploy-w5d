// get a module
export const GET_MODULE = 'GET_MODULE';
export const GET_MODULE_SUCCESS = 'GET_MODULE_SUCCESS';
export const GET_MODULE_FAILED = 'GET_MODULE_FAILED';

// export a module
export const EXPORT_MODULE = 'EXPORT_MODULE';
export const EXPORT_MODULE_SUCCESS = 'EXPORT_MODULE_SUCCESS';
export const EXPORT_MODULE_FAILED = 'EXPORT_MODULE_FAILED';

// get a list of modules
export const GET_MODULES = 'GET_MODULES';
export const GET_MODULES_SUCCESS = 'GET_MODULES_SUCCESS';
export const GET_MODULES_FAILED = 'GET_MODULES_FAILED';

// delete a module
export const DELETE_MODULE = 'DELETE_MODULE';
export const DELETE_MODULE_SUCCESS = 'DELETE_MODULE_SUCCESS';
export const DELETE_MODULE_FAILED = 'DELETE_MODULE_FAILED';

// delete a list of modules
export const DELETE_MODULES = 'DELETE_MODULES';
export const DELETE_MODULES_SUCCESS = 'DELETE_MODULES_SUCCESS';
export const DELETE_MODULES_FAILED = 'DELETE_MODULES_FAILED';

// update status of a list of modules
export const EDIT_MODULES_STATUS = 'EDIT_MODULES_STATUS';
export const EDIT_MODULES_STATUS_SUCCESS = 'EDIT_MODULES_STATUS_SUCCESS';
export const EDIT_MODULES_STATUS_FAILED = 'EDIT_MODULES_STATUS_FAILED';

// edit a module
export const EDIT_MODULE = 'EDIT_MODULE';
export const EDIT_MODULE_SUCCESS = 'EDIT_MODULE_SUCCESS';
export const EDIT_MODULE_FAILED = 'EDIT_MODULE_FAILED';

// create a module
export const CREATE_MODULE = 'CREATE_MODULE';
export const CREATE_MODULE_SUCCESS = 'CREATE_MODULE_SUCCESS';
export const CREATE_MODULE_FAILED = 'CREATE_MODULE_FAILED';

// get a module category
export const GET_MODULE_CATE = 'GET_MODULE_CATE';
export const GET_MODULE_CATE_SUCCESS = 'GET_MODULE_CATE_SUCCESS';
export const GET_MODULE_CATE_FAILED = 'GET_MODULE_CATE_FAILED';

// get a list of module categories
export const GET_MODULE_CATES = 'GET_MODULE_CATES';
export const GET_MODULE_CATES_SUCCESS = 'GET_MODULE_CATES_SUCCESS';
export const GET_MODULE_CATES_FAILED = 'GET_MODULE_CATES_FAILED';

//create a module category
export const CREATE_MODULE_CATE = 'CREATE_MODULE_CATE';
export const CREATE_MODULE_CATE_SUCCESS = 'CREATE_MODULE_CATE_SUCCESS';
export const CREATE_MODULE_CATE_FAILED = 'CREATE_MODULE_CATE_FAILED';

//edit a module category
export const EDIT_MODULE_CATE = 'EDIT_MODULE_CATE';
export const EDIT_MODULE_CATE_SUCCESS = 'EDIT_MODULE_CATE_SUCCESS';
export const EDIT_MODULE_CATE_FAILED = 'EDIT_MODULE_CATE_FAILED';

//delete a module category
export const DELETE_MODULE_CATE = 'DELETE_MODULE_CATE';
export const DELETE_MODULE_CATE_SUCCESS = 'DELETE_MODULE_CATE_SUCCESS';
export const DELETE_MODULE_CATE_FAILED = 'DELETE_MODULE_CATE_FAILED';

export function getModule(params, callback) {
  return {
    type: GET_MODULE,
    params,
    callback,
  };
}

export function exportModule(params, callback) {
  return {
    type: EXPORT_MODULE,
    params,
    callback,
  };
}

export function getModules(params, callback) {
  return {
    type: GET_MODULES,
    params,
    callback,
  };
}

export function deleteModule(params, callback) {
  return {
    type: DELETE_MODULE,
    params,
    callback,
  };
}

export function deleteModules(params, callback) {
  return {
    type: DELETE_MODULES,
    params,
    callback,
  };
}

export function editModulesStatus(params, callback) {
  return {
    type: EDIT_MODULES_STATUS,
    params,
    callback,
  };
}

export function editModule(params, callback) {
  return {
    type: EDIT_MODULE,
    params,
    callback,
  };
}

export function createModule(params, callback) {
  return {
    type: CREATE_MODULE,
    params,
    callback,
  };
}

export function getModuleCate(id, callback) {
  return {
    type: GET_MODULE_CATE,
    id,
    callback,
  };
}

export function getModuleCates(params, callback) {
  return {
    type: GET_MODULE_CATES,
    params,
    callback,
  };
}

export function createModuleCate(params, callback) {
  return {
    type: CREATE_MODULE_CATE,
    params,
    callback,
  };
}

export function editModuleCate(params, callback) {
  return {
    type: EDIT_MODULE_CATE,
    params,
    callback,
  };
}

export function deleteModuleCate(params, callback) {
  return {
    type: DELETE_MODULE_CATE,
    params,
    callback,
  };
}
