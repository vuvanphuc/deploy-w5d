// get a COOK
export const GET_COOK = 'GET_COOK';
export const GET_COOK_SUCCESS = 'GET_COOK_SUCCESS';
export const GET_COOK_FAILED = 'GET_COOK_FAILED';

//get a list of COOKs
export const GET_COOKS = 'GET_COOKS';
export const GET_COOKS_SUCCESS = 'GET_COOKS_SUCCESS';
export const GET_COOKS_FAILED = 'GET_COOKS_FAILED';

// delete a COOK
export const DELETE_COOK = 'DELETE_COOK';
export const DELETE_COOK_SUCCESS = 'DELETE_COOK_SUCCESS';
export const DELETE_COOK_FAILED = 'DELETE_COOK_FAILED';

// delete list COOK
export const DELETE_COOKS = 'DELETE_COOKS';
export const DELETE_COOKS_SUCCESS = 'DELETE_COOKS_SUCCESS';
export const DELETE_COOKS_FAILED = 'DELETE_COOKS_FAILED';

// edit a COOK
export const EDIT_COOK = 'EDIT_COOK';
export const EDIT_COOK_SUCCESS = 'EDIT_COOK_SUCCESS';
export const EDIT_COOK_FAILED = 'EDIT_COOK_FAILED';

// edit status list COOK
export const EDIT_STATUS_COOKS = 'EDIT_STATUS_COOKS';
export const EDIT_STATUS_COOKS_SUCCESS = 'EDIT_COOKS_SUCCESS';
export const EDIT_STATUS_COOKS_FAILED = 'EDIT_COOKS_FAILED';

// create a COOK
export const CREATE_COOK = 'CREATE_COOK';
export const CREATE_COOK_SUCCESS = 'CREATE_COOK_SUCCESS';
export const CREATE_COOK_FAILED = 'CREATE_COOK_FAILED';

// get a COOK category
export const GET_COOK_CATE = 'GET_COOK_CATE';
export const GET_COOK_CATE_SUCCESS = 'GET_COOK_CATE_SUCCESS';
export const GET_COOK_CATE_FAILED = 'GET_COOK_CATE_FAILED';

//get a list of COOK categories
export const GET_COOK_CATES = 'GET_COOK_CATES';
export const GET_COOK_CATES_SUCCESS = 'GET_COOK_CATES_SUCCESS';
export const GET_COOK_CATES_FAILED = 'GET_COOK_CATES_FAILED';

//create a COOK category
export const CREATE_COOK_CATE = 'CREATE_COOK_CATE';
export const CREATE_COOK_CATE_SUCCESS = 'CREATE_COOK_CATE_SUCCESS';
export const CREATE_COOK_CATE_FAILED = 'CREATE_COOK_CATE_FAILED';

//edit a COOK category
export const EDIT_COOK_CATE = 'EDIT_COOK_CATE';
export const EDIT_COOK_CATE_SUCCESS = 'EDIT_COOK_CATE_SUCCESS';
export const EDIT_COOK_CATE_FAILED = 'EDIT_COOK_CATE_FAILED';

//delete a COOK category
export const DELETE_COOK_CATE = 'DELETE_COOK_CATE';
export const DELETE_COOK_CATE_SUCCESS = 'DELETE_COOK_CATE_SUCCESS';
export const DELETE_COOK_CATE_FAILED = 'DELETE_COOK_CATE_FAILED';

//delete a COOK categories
export const DELETE_COOK_CATES = 'DELETE_COOK_CATES';
export const DELETE_COOK_CATES_SUCCESS = 'DELETE_COOK_CATES_SUCCESS';
export const DELETE_COOK_CATES_FAILED = 'DELETE_COOK_CATES_FAILED';

// set COOK search params
export const SET_SEARCH_COOK = 'SET_SEARCH_COOK';

export function getCook(params, callback) {
  return {
    type: GET_COOK,
    params,
    callback,
  };
}

export function getCooks(params, callback) {
  return {
    type: GET_COOKS,
    params,
    callback,
  };
}

export function deleteCook(params, callback) {
  return {
    type: DELETE_COOK,
    params,
    callback,
  };
}

export function deleteCooks(params, callback) {
  return {
    type: DELETE_COOKS,
    params,
    callback,
  };
}

export function editCook(params, callback) {
  return {
    type: EDIT_COOK,
    params,
    callback,
  };
}

export function editStatusCook(params, callback) {
  return {
    type: EDIT_STATUS_COOKS,
    params,
    callback,
  };
}

export function createCook(params, callback) {
  return {
    type: CREATE_COOK,
    params,
    callback,
  };
}

export function getCookCate(id, callback) {
  return {
    type: GET_COOK_CATE,
    id,
    callback,
  };
}

export function getCookCates(params, callback) {
  return {
    type: GET_COOK_CATES,
    params,
    callback,
  };
}

export function createCookCate(params, callback) {
  return {
    type: CREATE_COOK_CATE,
    params,
    callback,
  };
}

export function editCookCate(params, callback) {
  return {
    type: EDIT_COOK_CATE,
    params,
    callback,
  };
}

export function deleteCookCate(params, callback) {
  return {
    type: DELETE_COOK_CATE,
    params,
    callback,
  };
}

export function deleteCookCates(params, callback) {
  return {
    type: DELETE_COOK_CATES,
    params,
    callback,
  };
}

export function setSearchParams(params) {
  return {
    type: SET_SEARCH_COOK,
    params,
  };
}
