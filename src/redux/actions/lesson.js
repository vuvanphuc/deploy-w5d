// get a lesson
export const GET_LESSON = 'GET_LESSON';
export const GET_LESSON_SUCCESS = 'GET_LESSON_SUCCESS';
export const GET_LESSON_FAILED = 'GET_LESSON_FAILED';

//get a list of lessons
export const GET_LESSONS = 'GET_LESSONS';
export const GET_LESSONS_SUCCESS = 'GET_LESSONS_SUCCESS';
export const GET_LESSONS_FAILED = 'GET_LESSONS_FAILED';

// delete a lesson
export const DELETE_LESSON = 'DELETE_LESSON';
export const DELETE_LESSON_SUCCESS = 'DELETE_LESSON_SUCCESS';
export const DELETE_LESSON_FAILED = 'DELETE_LESSON_FAILED';

// delete list lesson
export const DELETE_LESSONS = 'DELETE_LESSONS';
export const DELETE_LESSONS_SUCCESS = 'DELETE_LESSONS_SUCCESS';
export const DELETE_LESSONS_FAILED = 'DELETE_LESSONS_FAILED';

// edit a lesson
export const EDIT_LESSON = 'EDIT_LESSON';
export const EDIT_LESSON_SUCCESS = 'EDIT_LESSON_SUCCESS';
export const EDIT_LESSON_FAILED = 'EDIT_LESSON_FAILED';

// edit status list lesson
export const EDIT_STATUS_LESSONS = 'EDIT_STATUS_LESSONS';
export const EDIT_STATUS_LESSONS_SUCCESS = 'EDIT_LESSONS_SUCCESS';
export const EDIT_STATUS_LESSONS_FAILED = 'EDIT_LESSONS_FAILED';

// create a lesson
export const CREATE_LESSON = 'CREATE_LESSON';
export const CREATE_LESSON_SUCCESS = 'CREATE_LESSON_SUCCESS';
export const CREATE_LESSON_FAILED = 'CREATE_LESSON_FAILED';

// get a lesson category
export const GET_LESSON_CATE = 'GET_LESSON_CATE';
export const GET_LESSON_CATE_SUCCESS = 'GET_LESSON_CATE_SUCCESS';
export const GET_LESSON_CATE_FAILED = 'GET_LESSON_CATE_FAILED';

//get a list of lesson categories
export const GET_LESSON_CATES = 'GET_LESSON_CATES';
export const GET_LESSON_CATES_SUCCESS = 'GET_LESSON_CATES_SUCCESS';
export const GET_LESSON_CATES_FAILED = 'GET_LESSON_CATES_FAILED';

//create a lesson category
export const CREATE_LESSON_CATE = 'CREATE_LESSON_CATE';
export const CREATE_LESSON_CATE_SUCCESS = 'CREATE_LESSON_CATE_SUCCESS';
export const CREATE_LESSON_CATE_FAILED = 'CREATE_LESSON_CATE_FAILED';

//edit a lesson category
export const EDIT_LESSON_CATE = 'EDIT_LESSON_CATE';
export const EDIT_LESSON_CATE_SUCCESS = 'EDIT_LESSON_CATE_SUCCESS';
export const EDIT_LESSON_CATE_FAILED = 'EDIT_LESSON_CATE_FAILED';

//delete a lesson category
export const DELETE_LESSON_CATE = 'DELETE_LESSON_CATE';
export const DELETE_LESSON_CATE_SUCCESS = 'DELETE_LESSON_CATE_SUCCESS';
export const DELETE_LESSON_CATE_FAILED = 'DELETE_LESSON_CATE_FAILED';

//delete a lesson categories
export const DELETE_LESSON_CATES = 'DELETE_LESSON_CATES';
export const DELETE_LESSON_CATES_SUCCESS = 'DELETE_LESSON_CATES_SUCCESS';
export const DELETE_LESSON_CATES_FAILED = 'DELETE_LESSON_CATES_FAILED';

// set lesson search params
export const SET_SEARCH_LESSON = 'SET_SEARCH_LESSON';

export function getLesson(params, callback) {
  return {
    type: GET_LESSON,
    params,
    callback,
  };
}

export function getLessons(params, callback) {
  return {
    type: GET_LESSONS,
    params,
    callback,
  };
}

export function deleteLesson(params, callback) {
  return {
    type: DELETE_LESSON,
    params,
    callback,
  };
}

export function deleteLessons(params, callback) {
  return {
    type: DELETE_LESSONS,
    params,
    callback,
  };
}

export function editLesson(params, callback) {
  return {
    type: EDIT_LESSON,
    params,
    callback,
  };
}

export function editStatusLesson(params, callback) {
  return {
    type: EDIT_STATUS_LESSONS,
    params,
    callback,
  };
}

export function createLesson(params, callback) {
  return {
    type: CREATE_LESSON,
    params,
    callback,
  };
}

export function getLessonCate(id, callback) {
  return {
    type: GET_LESSON_CATE,
    id,
    callback,
  };
}

export function getLessonCates(params, callback) {
  return {
    type: GET_LESSON_CATES,
    params,
    callback,
  };
}

export function createLessonCate(params, callback) {
  return {
    type: CREATE_LESSON_CATE,
    params,
    callback,
  };
}

export function editLessonCate(params, callback) {
  return {
    type: EDIT_LESSON_CATE,
    params,
    callback,
  };
}

export function deleteLessonCate(params, callback) {
  return {
    type: DELETE_LESSON_CATE,
    params,
    callback,
  };
}

export function deleteLessonCates(params, callback) {
  return {
    type: DELETE_LESSON_CATES,
    params,
    callback,
  };
}

export function setSearchParams(params) {
  return {
    type: SET_SEARCH_LESSON,
    params,
  };
}
