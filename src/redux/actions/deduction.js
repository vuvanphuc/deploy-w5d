// get a deduction Info
export const GET_DEDUCTION_INFO = 'GET_DEDUCTION_INFO';
export const GET_DEDUCTION_INFO_SUCCESS = 'GET_DEDUCTION_INFO_SUCCESS';
export const GET_DEDUCTION_INFO_FAILED = 'GET_DEDUCTION_INFO_FAILED';

// get a list of deduction Info
export const GET_DEDUCTION_INFOS = 'GET_DEDUCTION_INFOS';
export const GET_DEDUCTION_INFOS_SUCCESS = 'GET_DEDUCTION_INFOS_SUCCESS';
export const GET_DEDUCTION_INFOS_FAILED = 'GET_DEDUCTION_INFOS_FAILED';

// get a deduction
export const GET_DEDUCTION = 'GET_DEDUCTION';
export const GET_DEDUCTION_SUCCESS = 'GET_DEDUCTION_SUCCESS';
export const GET_DEDUCTION_FAILED = 'GET_DEDUCTION_FAILED';

// edit a deduction
export const EDIT_DEDUCTION = 'EDIT_DEDUCTION';
export const EDIT_DEDUCTION_SUCCESS = 'EDIT_DEDUCTION_SUCCESS';
export const EDIT_DEDUCTION_FAILED = 'EDIT_DEDUCTION_FAILED';

// edit deduction result
export const EDIT_DEDUCTION_RESULT = 'EDIT_DEDUCTION_RESULT';
export const EDIT_DEDUCTION_RESULT_SUCCESS = 'EDIT_DEDUCTION_RESULT_SUCCESS';
export const EDIT_DEDUCTION_RESULT_FAILED = 'EDIT_DEDUCTION_RESULT_FAILED';

export function getDeductionInfo(params, callback) {
  return {
    type: GET_DEDUCTION_INFO,
    params,
    callback,
  };
}

export function getDeductionInfos(params, callback) {
  return {
    type: GET_DEDUCTION_INFOS,
    params,
    callback,
  };
}

export function getDeduction(params, callback) {
  return {
    type: GET_DEDUCTION,
    params,
    callback,
  };
}

export function editDeduction(params, callback) {
  return {
    type: EDIT_DEDUCTION,
    params,
    callback,
  };
}

export function editDeductionResult(params, callback) {
  return {
    type: EDIT_DEDUCTION_RESULT,
    params,
    callback,
  };
}
