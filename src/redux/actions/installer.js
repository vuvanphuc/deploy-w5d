// check web install or not
export const CHECK_INSTALLER = 'CHECK_INSTALLER';
export const CHECK_INSTALLER_SUCCESS = 'CHECK_INSTALLER_SUCCESS';
export const CHECK_INSTALLER_FAILED = 'CHECK_INSTALLER_FAILED';

// check connection
export const CHECK_CONNECTION = 'CHECK_CONNECTION';
export const CHECK_CONNECTION_SUCCESS = 'CHECK_CONNECTION_SUCCESS';
export const CHECK_CONNECTION_FAILED = 'CHECK_CONNECTION_FAILED';

// init data
export const INIT_DATA = 'INIT_DATA';
export const INIT_DATA_SUCCESS = 'INIT_DATA_SUCCESS';
export const INIT_DATA_FAILED = 'INIT_DATA_FAILED';

export function check(params, callback) {
  return {
    type: CHECK_INSTALLER,
    params,
    callback,
  };
}

export function checkConnection(params, callback) {
  return {
    type: CHECK_CONNECTION,
    params,
    callback,
  };
}

export function initData(params, callback) {
  return {
    type: INIT_DATA,
    params,
    callback,
  };
}
