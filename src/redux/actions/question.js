// get a question
export const GET_QUESTION = 'GET_QUESTION';
export const GET_QUESTION_SUCCESS = 'GET_QUESTION_SUCCESS';
export const GET_QUESTION_FAILED = 'GET_QUESTION_FAILED';

//get a list of questions
export const GET_QUESTIONS = 'GET_QUESTIONS';
export const GET_QUESTIONS_SUCCESS = 'GET_QUESTIONS_SUCCESS';
export const GET_QUESTIONS_FAILED = 'GET_QUESTIONS_FAILED';

// delete a question
export const DELETE_QUESTION = 'DELETE_QUESTION';
export const DELETE_QUESTION_SUCCESS = 'DELETE_QUESTION_SUCCESS';
export const DELETE_QUESTION_FAILED = 'DELETE_QUESTION_FAILED';

// delete list question
export const DELETE_QUESTIONS = 'DELETE_QUESTIONS';
export const DELETE_QUESTIONS_SUCCESS = 'DELETE_QUESTIONS_SUCCESS';
export const DELETE_QUESTIONS_FAILED = 'DELETE_QUESTIONS_FAILED';

// edit a question
export const EDIT_QUESTION = 'EDIT_QUESTION';
export const EDIT_QUESTION_SUCCESS = 'EDIT_QUESTION_SUCCESS';
export const EDIT_QUESTION_FAILED = 'EDIT_QUESTION_FAILED';

// edit status list question
export const EDIT_STATUS_QUESTIONS = 'EDIT_STATUS_QUESTIONS';
export const EDIT_STATUS_QUESTIONS_SUCCESS = 'EDIT_QUESTIONS_SUCCESS';
export const EDIT_STATUS_QUESTIONS_FAILED = 'EDIT_QUESTIONS_FAILED';

// create a question
export const CREATE_QUESTION = 'CREATE_QUESTION';
export const CREATE_QUESTION_SUCCESS = 'CREATE_QUESTION_SUCCESS';
export const CREATE_QUESTION_FAILED = 'CREATE_QUESTION_FAILED';

// get a question category
export const GET_QUESTION_CATE = 'GET_QUESTION_CATE';
export const GET_QUESTION_CATE_SUCCESS = 'GET_QUESTION_CATE_SUCCESS';
export const GET_QUESTION_CATE_FAILED = 'GET_QUESTION_CATE_FAILED';

//get a list of question categories
export const GET_QUESTION_CATES = 'GET_QUESTION_CATES';
export const GET_QUESTION_CATES_SUCCESS = 'GET_QUESTION_CATES_SUCCESS';
export const GET_QUESTION_CATES_FAILED = 'GET_QUESTION_CATES_FAILED';

//create a question category
export const CREATE_QUESTION_CATE = 'CREATE_QUESTION_CATE';
export const CREATE_QUESTION_CATE_SUCCESS = 'CREATE_QUESTION_CATE_SUCCESS';
export const CREATE_QUESTION_CATE_FAILED = 'CREATE_QUESTION_CATE_FAILED';

//edit a question category
export const EDIT_QUESTION_CATE = 'EDIT_QUESTION_CATE';
export const EDIT_QUESTION_CATE_SUCCESS = 'EDIT_QUESTION_CATE_SUCCESS';
export const EDIT_QUESTION_CATE_FAILED = 'EDIT_QUESTION_CATE_FAILED';

//delete a question category
export const DELETE_QUESTION_CATE = 'DELETE_QUESTION_CATE';
export const DELETE_QUESTION_CATE_SUCCESS = 'DELETE_QUESTION_CATE_SUCCESS';
export const DELETE_QUESTION_CATE_FAILED = 'DELETE_QUESTION_CATE_FAILED';

//delete a question categories
export const DELETE_QUESTION_CATES = 'DELETE_QUESTION_CATES';
export const DELETE_QUESTION_CATES_SUCCESS = 'DELETE_QUESTION_CATES_SUCCESS';
export const DELETE_QUESTION_CATES_FAILED = 'DELETE_QUESTION_CATES_FAILED';

// set question search params
export const SET_SEARCH_QUESTION = 'SET_SEARCH_QUESTION';

export function getQuestion(params, callback) {
  return {
    type: GET_QUESTION,
    params,
    callback,
  };
}

export function getQuestions(params, callback) {
  return {
    type: GET_QUESTIONS,
    params,
    callback,
  };
}

export function deleteQuestion(params, callback) {
  return {
    type: DELETE_QUESTION,
    params,
    callback,
  };
}

export function deleteQuestions(params, callback) {
  return {
    type: DELETE_QUESTIONS,
    params,
    callback,
  };
}

export function editQuestion(params, callback) {
  return {
    type: EDIT_QUESTION,
    params,
    callback,
  };
}

export function editStatusQuestion(params, callback) {
  return {
    type: EDIT_STATUS_QUESTIONS,
    params,
    callback,
  };
}

export function createQuestion(params, callback) {
  return {
    type: CREATE_QUESTION,
    params,
    callback,
  };
}

export function getQuestionCate(id, callback) {
  return {
    type: GET_QUESTION_CATE,
    id,
    callback,
  };
}

export function getQuestionCates(params, callback) {
  return {
    type: GET_QUESTION_CATES,
    params,
    callback,
  };
}

export function createQuestionCate(params, callback) {
  return {
    type: CREATE_QUESTION_CATE,
    params,
    callback,
  };
}

export function editQuestionCate(params, callback) {
  return {
    type: EDIT_QUESTION_CATE,
    params,
    callback,
  };
}

export function deleteQuestionCate(params, callback) {
  return {
    type: DELETE_QUESTION_CATE,
    params,
    callback,
  };
}

export function deleteQuestionCates(params, callback) {
  return {
    type: DELETE_QUESTION_CATES,
    params,
    callback,
  };
}

export function setSearchParams(params) {
  return {
    type: SET_SEARCH_QUESTION,
    params,
  };
}
