// get a notification
export const GET_NOTIFICATION = 'GET_NOTIFICATION';
export const GET_NOTIFICATION_SUCCESS = 'GET_NOTIFICATION_SUCCESS';
export const GET_NOTIFICATION_FAILED = 'GET_NOTIFICATION_FAILED';

// get a list of notifications
export const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS';
export const GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS';
export const GET_NOTIFICATIONS_FAILED = 'GET_NOTIFICATIONS_FAILED';

// delete a notification
export const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';
export const DELETE_NOTIFICATION_SUCCESS = 'DELETE_NOTIFICATION_SUCCESS';
export const DELETE_NOTIFICATION_FAILED = 'DELETE_NOTIFICATION_FAILED';

// delete notifications
export const DELETE_NOTIFICATIONS = 'DELETE_NOTIFICATIONS';
export const DELETE_NOTIFICATIONS_SUCCESS = 'DELETE_NOTIFICATIONS_SUCCESS';
export const DELETE_NOTIFICATIONS_FAILED = 'DELETE_NOTIFICATIONS_FAILED';

// edit a notification
export const EDIT_NOTIFICATION = 'EDIT_NOTIFICATION';
export const EDIT_NOTIFICATION_SUCCESS = 'EDIT_NOTIFICATION_SUCCESS';
export const EDIT_NOTIFICATION_FAILED = 'EDIT_NOTIFICATION_FAILED';

// create a notification
export const CREATE_NOTIFICATION = 'CREATE_NOTIFICATION';
export const CREATE_NOTIFICATION_SUCCESS = 'CREATE_NOTIFICATION_SUCCESS';
export const CREATE_NOTIFICATION_FAILED = 'CREATE_NOTIFICATION_FAILED';

// get a notification category
export const GET_NOTIFICATION_CATE = 'GET_NOTIFICATION_CATE';
export const GET_NOTIFICATION_CATE_SUCCESS = 'GET_NOTIFICATION_CATE_SUCCESS';
export const GET_NOTIFICATION_CATE_FAILED = 'GET_NOTIFICATION_CATE_FAILED';

// get a list of notification categories
export const GET_NOTIFICATION_CATES = 'GET_NOTIFICATION_CATES';
export const GET_NOTIFICATION_CATES_SUCCESS = 'GET_NOTIFICATION_CATES_SUCCESS';
export const GET_NOTIFICATION_CATES_FAILED = 'GET_NOTIFICATION_CATES_FAILED';

//create a notification category
export const CREATE_NOTIFICATION_CATE = 'CREATE_NOTIFICATION_CATE';
export const CREATE_NOTIFICATION_CATE_SUCCESS = 'CREATE_NOTIFICATION_CATE_SUCCESS';
export const CREATE_NOTIFICATION_CATE_FAILED = 'CREATE_NOTIFICATION_CATE_FAILED';

//edit a notification category
export const EDIT_NOTIFICATION_CATE = 'EDIT_NOTIFICATION_CATE';
export const EDIT_NOTIFICATION_CATE_SUCCESS = 'EDIT_NOTIFICATION_CATE_SUCCESS';
export const EDIT_NOTIFICATION_CATE_FAILED = 'EDIT_NOTIFICATION_CATE_FAILED';

//delete a notification category
export const DELETE_NOTIFICATION_CATE = 'DELETE_NOTIFICATION_CATE';
export const DELETE_NOTIFICATION_CATE_SUCCESS = 'DELETE_NOTIFICATION_CATE_SUCCESS';
export const DELETE_NOTIFICATION_CATE_FAILED = 'DELETE_NOTIFICATION_CATE_FAILED';

//delete notification categories
export const DELETE_NOTIFICATION_CATES = 'DELETE_NOTIFICATION_CATES';
export const DELETE_NOTIFICATION_CATES_SUCCESS = 'DELETE_NOTIFICATION_CATES_SUCCESS';
export const DELETE_NOTIFICATION_CATES_FAILED = 'DELETE_NOTIFICATION_CATES_FAILED';

export function getNotification(params, callback) {
  return {
    type: GET_NOTIFICATION,
    params,
    callback,
  };
}

export function getNotifications(params, callback) {
  return {
    type: GET_NOTIFICATIONS,
    params,
    callback,
  };
}

export function deleteNotification(params, callback) {
  return {
    type: DELETE_NOTIFICATION,
    params,
    callback,
  };
}

export function deleteNotifications(params, callback) {
  return {
    type: DELETE_NOTIFICATIONS,
    params,
    callback,
  };
}

export function editNotification(params, callback) {
  return {
    type: EDIT_NOTIFICATION,
    params,
    callback,
  };
}

export function createNotification(params, callback) {
  return {
    type: CREATE_NOTIFICATION,
    params,
    callback,
  };
}

export function getNotificationCate(id, callback) {
  return {
    type: GET_NOTIFICATION_CATE,
    id,
    callback,
  };
}

export function getNotificationCates(params, callback) {
  return {
    type: GET_NOTIFICATION_CATES,
    params,
    callback,
  };
}

export function createNotificationCate(params, callback) {
  return {
    type: CREATE_NOTIFICATION_CATE,
    params,
    callback,
  };
}

export function editNotificationCate(params, callback) {
  return {
    type: EDIT_NOTIFICATION_CATE,
    params,
    callback,
  };
}

export function deleteNotificationCate(params, callback) {
  return {
    type: DELETE_NOTIFICATION_CATE,
    params,
    callback,
  };
}

export function deleteNotificationCates(params, callback) {
  return {
    type: DELETE_NOTIFICATION_CATES,
    params,
    callback,
  };
}
