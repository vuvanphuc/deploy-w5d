// IMPORT ACTIONS
import * as category from './category';
import * as notification from './notification';
import * as moduleApp from './moduleApp';
import * as deduction from './deduction';
import * as form from './form';
import * as user from './user';
import * as media from './media';
import * as log from './log';
import * as config from './configure';
import * as statistic from './statistic';
import * as table from './table';
import * as ruleset from './ruleset';
import * as installer from './installer';
import * as post from './post';
import * as exam from './exam';
import * as question from './question';
import * as cook from './cook';
import * as remind from './remind';
import * as appointment from './appointment';
import * as course from './course';
import * as lesson from './lesson';

export {
  // EXPORT ACTIONS
  category,
  notification,
  media,
  log,
  moduleApp,
  deduction,
  form,
  user,
  post,
  exam,
  question,
  cook,
  config,
  statistic,
  table,
  ruleset,
  installer,
  remind,
  appointment,
  course,
  lesson
};
